﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using BatchService.STD.Daemon;
using System.Threading;

namespace BatchService
{
    public partial class BatchService : ServiceBase
    {
        public BatchService()
        {
            InitializeComponent();
        }

        protected override void OnStart(string[] args)
        {
            CSTDBatchDaemon daemon = new CSTDBatchDaemon(args);
            Thread daemonThread = new Thread(daemon.run);
            daemonThread.Start();
        }

        protected override void OnStop()
        {
        }
    }
}
