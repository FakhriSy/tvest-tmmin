﻿  
-- Create table
create table TB_M_BATCH
(
  BATCH_ID         VARCHAR(10) not null,
  PROJECT_CODE     CHAR(3) not null,
  BATCH_NAME       VARCHAR(100),
  PRIORITY_LEVEL   INT default 0,
  CONCURRENCY_FLAG INT default 1,
  GROUP_ID		   VARCHAR(20),
  RUNNING_COUNT    INT not null,
  RUN_AS           VARCHAR(10) not null,
  SHELL            VARCHAR(255) not null,
  CREATE_BY        VARCHAR(8),
  CREATE_DT        timestamp not null,
  UPDATE_BY        VARCHAR(8),
  UPDATE_DT        timestamp not null,
  SUPPORT_ID	   VARCHAR(10)
);

-- Create table
create table TB_R_BATCH_QUEUE
(
  QUEUE_NO     NUMBER(12),
  PROJECT_CODE char(3),
  BATCH_ID     VARCHAR(10) not null,
  REQUEST_ID   VARCHAR(8) not null,
  REQUEST_DATE timestamp not null,
  REQUEST_BY   VARCHAR(10) not null,
  SUPPORT_ID   VARCHAR(10),
  PARAMETER		VARCHAR(500),
  PRIMARY KEY(QUEUE_NO)
);

-- create table history
create table TB_L_BATCH_STATUS_LOG
(
  QUEUE_NO     NUMBER(12),
  PROJECT_CODE char(3),
  BATCH_ID     VARCHAR(10) not null,
  REQUEST_ID   VARCHAR(8) not null,
  REQUEST_DATE timestamp not null,
  REQUEST_BY   VARCHAR(10) not null,
  SUPPORT_ID   VARCHAR(10),
  PARAMETER		VARCHAR(500),
  CURRENT_STATUS	integer not null,
  BATCH_RESULT    VARCHAR(200) null,
  RUN_DATE   timestamp not null,
  PRIMARY KEY (QUEUE_NO)
);

  