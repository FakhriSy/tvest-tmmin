﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.Reflection;
using System.Data;
using Common.STD.DAO;
using Common.STD.DTO;
using System.Threading;
using System.IO;
using Common.STD.Util;
using System.Diagnostics;
using BatchHandler.CED.Handler;
using System.Threading.Tasks;

namespace BatchService.STD.Daemon
{
    public class CSTDBatchThread
    {
        public Thread Thread = null;
        public CSTDBatchDTO Dto = null;
        public Nullable<DateTime> StartDate = null;
    }

    public class CSTDBatchDaemon
    {
        //static int MAX_CONCURRENT_PROCESS = 10;
        static int MAX_CONCURRENT_PROCESS = 10; // request from Pak Pram
        static long DELAY = 500; //1000 = 1 second
        static ILog logger = LogManager.GetLogger(typeof(Program));
        protected static bool _BF_USE_TWS = false;
        protected static bool _BATCH_SEND_MAIL = false;
        protected static String _BATCH_MAIL_FROM = "";
        protected static String _BATCH_MAIL_TO = "";
        protected static String _TWS_TEMP_FOLDER = null;
        protected static String logFolder = "";
        protected static String environment = "";
        protected static Boolean isTWSStatus = false;
        protected static Boolean isTWSProblem = false;
        protected static double maxTWSHoursExec = 24;
        protected static Boolean _TWS_MONITOR = false;
        CSTDBatchThread[] HandlerThreads = new CSTDBatchThread[] { };

        public CSTDBatchDaemon(String[] args)
        {
            FileInfo log4netConfig = new FileInfo(CSTDStringUtil.GetApplicationResourcesPath + "Log4NetConfig\\Log4net.xml");
            XmlConfigurator.Configure(log4netConfig);
            if (args != null)
            {
                if (args.Length > 1)
                {
                    DELAY = Convert.ToInt32(args[1]);
                }
                else if (args.Length > 0)
                {
                    MAX_CONCURRENT_PROCESS = Convert.ToInt32(args[0]);
                }
            }
            HandlerThreads = new CSTDBatchThread[MAX_CONCURRENT_PROCESS];
            logger.Info("NSF Batch Service started on : " + DateTime.Now);
            logger.Info("MAX_CONCURRENT_PROCESS : " + MAX_CONCURRENT_PROCESS);
        }

        public void run()
        {
            CSTDProperties.PropertiesPath = CSTDStringUtil.GetApplicationResourcesPath;
            _BF_USE_TWS = "1".Equals(CSTDBatchDAO.GetTWSFlag, StringComparison.OrdinalIgnoreCase) ? true : false;
            _BATCH_SEND_MAIL = "1".Equals(CSTDBatchDAO.GetBatchMail, StringComparison.OrdinalIgnoreCase) ? true : false;
            _BATCH_MAIL_FROM = CSTDBatchDAO.GetBatchMailFrom;
            _BATCH_MAIL_TO = CSTDBatchDAO.GetBatchMailto;

            _TWS_TEMP_FOLDER = CSTDProperties.PropertiesPath;
            logFolder = CSTDBatchDAO.GetBatchLogLocation;
            String MaximumBatchRun = CSTDBatchDAO.GetMaximumBatchRun;
            if (!String.IsNullOrEmpty(MaximumBatchRun))
            {
                maxTWSHoursExec = double.Parse(MaximumBatchRun);
            }
            bool twsError = false;

            environment = CSTDSystemConfig.GetValue("ENVIRONTMENT");
            _TWS_MONITOR = "1".Equals(CSTDBatchDAO.GetTWSMonitor, StringComparison.OrdinalIgnoreCase) ? true : false;

            DateTime PrevTime = DateTime.Now;
            while (true)
            {
                //get queue
                try
                {
                    PrevTime = DateTime.Now;
                    //int runningProcess = CSTDBatchDAO.GetRunningProcesses;
                    //logger.Info("Number of running Process: " + runningProcess);

                    //if (runningProcess > -1 && runningProcess < MAX_CONCURRENT_PROCESS)
                    {
                        //Checking long Queueing
                        DataTable LongQueue = CSTDBatchDAO.GetLongQueueList();
                        if (LongQueue != null)
                        {
                            if (LongQueue.Rows.Count > 0)
                            {
                                if (LongQueue.Rows[0]["NUM_OF_LONG_QUEUE"] != null)
                                {
                                    Int16 LongQueueCount = Int16.Parse(LongQueue.Rows[0]["NUM_OF_LONG_QUEUE"].ToString());
                                    if ( LongQueueCount > 0)
                                    {
                                        String Message =
                                            "\nWarning : There are " + LongQueueCount + " batch queue that not execute in 2 seconds after registering." +
                                            "\nPlease check batch queue";
                                        logger.Warn(Message);

                                        if (_BATCH_SEND_MAIL)
                                        {
                                            CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                                            emailDTO.To = _BATCH_MAIL_TO;
                                            emailDTO.Subject = "Batch Warning";
                                            emailDTO.Content = Message;
                                            CSTDEmailUtil.SendEmail(emailDTO);
                                        }
                                    }
                                }
                            }
                        }

                        for (int threadIdx = 0; threadIdx < HandlerThreads.Length; threadIdx++)
                        {
                            if (HandlerThreads[threadIdx] != null)
                            {
                                if (HandlerThreads[threadIdx].Thread.IsAlive)
                                {
                                    DateTime FinishDateTime = DateTime.Now;
                                    TimeSpan duration = FinishDateTime - (DateTime)HandlerThreads[threadIdx].StartDate;
                                    double Minutes = duration.TotalMinutes;
                                    //Cecking long Execution
                                    if (Minutes > 5)
                                    {
                                        String Subject = "Batch : [" +
                                            HandlerThreads[threadIdx].Dto.BatchId + "]-[" +
                                            HandlerThreads[threadIdx].Dto.BatchName + "] not finish in 5 minutes";
                                        String Message =
                                            " Queue Code = " + HandlerThreads[threadIdx].Dto.QueueNo + "\n" +
                                            " Project Code = " + HandlerThreads[threadIdx].Dto.ProjectCode + "\n" +
                                            " Batch ID = " + HandlerThreads[threadIdx].Dto.BatchId + "\n" +
                                            " Batch Name = " + HandlerThreads[threadIdx].Dto.BatchName + "\n" +
                                            " Batch Parameter = " + HandlerThreads[threadIdx].Dto.Parameter + "\n" +
                                            " Request ID = " + HandlerThreads[threadIdx].Dto.RequestId + "\n" +
                                            " Request By = " + HandlerThreads[threadIdx].Dto.RequestBy + "\n" +
                                            " Started Time = " + ((DateTime)HandlerThreads[threadIdx].StartDate).ToString("dd-MM-yyy HH:mm:ss.fff") + "\n";

                                        if (_BATCH_SEND_MAIL)
                                        {
                                            CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                                            emailDTO.To = _BATCH_MAIL_TO;
                                            emailDTO.Subject = Subject;
                                            emailDTO.Content = Message;
                                            CSTDEmailUtil.SendEmail(emailDTO);
                                        }

                                        logger.Info(Subject);
                                        logger.Info(Message);
                                    }
                                }
                                else
                                {
                                    //Cecking Thread Not Alive but not end with stopped
                                    if (!HandlerThreads[threadIdx].Thread.ThreadState.ToString()
                                        .Equals(System.Threading.ThreadState.Stopped.ToString()))
                                    {
                                        String Message = "Queue Code = " + HandlerThreads[threadIdx].Dto.QueueNo + "\n" +
                                            " Project Code = " + HandlerThreads[threadIdx].Dto.ProjectCode + "\n" +
                                            " Batch ID = " + HandlerThreads[threadIdx].Dto.BatchId + "\n" +
                                            " Batch Name = " + HandlerThreads[threadIdx].Dto.BatchName + "\n" +
                                            " Batch Parameter = " + HandlerThreads[threadIdx].Dto.Parameter + "\n" +
                                            " Request ID = " + HandlerThreads[threadIdx].Dto.RequestId + "\n" +
                                            " Request By = " + HandlerThreads[threadIdx].Dto.RequestBy + "\n" +
                                            " Thread State = " + HandlerThreads[threadIdx].Thread.ThreadState.ToString() +
                                            " Started Time = " + ((DateTime)HandlerThreads[threadIdx].StartDate).ToString("dd-MM-yyy HH:mm:ss.fff") + "\n";

                                        if (_BATCH_SEND_MAIL)
                                        {
                                            CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                                            emailDTO.To = _BATCH_MAIL_TO;
                                            emailDTO.Subject = "Batch : [" +
                                            HandlerThreads[threadIdx].Dto.BatchId + "]-[" +
                                            HandlerThreads[threadIdx].Dto.BatchName + "] Not End Normally";
                                            emailDTO.Content = Message;
                                            CSTDEmailUtil.SendEmail(emailDTO);
                                        }
                                        logger.Error(Message);
                                    }
                                    HandlerThreads[threadIdx] = null; // Emptying Finish Thread
                                }
                            }
                        }

                        DataTable queue = CSTDBatchDAO.GetQueueList;
                        logger.Debug("Current queue: " + (queue == null ? 0 : queue.Rows.Count));
                        if (queue != null && queue.Rows.Count > 0)
                        {
                            int numQueue = queue.Rows.Count;
                            if (numQueue > 0)
                            {
                                logger.Info("Current number of queue: " + numQueue);
                            }
                            //for (int i = runningProcess, j = 0; i < MAX_CONCURRENT_PROCESS && j < numQueue; i++, j++)
                            for (int j = 0; j < numQueue; j++)
                            {
                                Boolean hasRun = false;
                                CSTDBatchDTO dto = new CSTDBatchDTO(queue.Rows[j]);
                                //logger.Info("Current queue: " + dto.QueueNo);

                                /** Remarked by FID)Praditha, 24/11/2014
                                 *  Because the Process has not been running will be always moved to history table 
                                 * **/
                                //CSTDBatchDAO.MoveQueueToHistory(dto.QueueNo);
                                #region For TWS
                                if (_BF_USE_TWS && dto.RequestBy.IndexOf(" ") == -1)
                                {
                                     /** Start Move by FID)Jumadi, 11/12/2014
                                     Should be check concurrency ( if concurrency flag equal '1' or 'Y'), before execute
                                     **/
                                    CSTDBatchDAO.MoveQueueToHistory(dto.QueueNo);
                                    /** End Move by FID)Jumadi, 11/12/2014 **/
                                    //create job param
                                    String fileName = CSTDJobUtil.CreateJobParam(_TWS_TEMP_FOLDER, dto);

                                    //submit param value
                                    String twsSubmitSts = "";
                                    String twsCommandError = "";
                                    if (File.Exists(logFolder + "SubmitToTWS.txt"))
                                    {
                                        File.Delete(logFolder + "SubmitToTWS.txt");
                                    }
                                    String retValue = CSTDJobUtil.RunProcess("cmd.exe", "/C composer replace \"" + _TWS_TEMP_FOLDER + fileName + "\" > " + logFolder + "SubmitToTWS.txt 2>&1 ");

                                    twsCommandError += " Command Executed : \n\n" +
                                                       " ===========================\n\n\n\r";

                                    twsCommandError += "        /C composer replace \"" + _TWS_TEMP_FOLDER + fileName + "\"\n\n";
                                    //delete job file
                                    StreamReader inputStream = new StreamReader(logFolder + "SubmitToTWS.txt");
                                    String line;
                                    bool SubmitTwsOke = false;
                                    do
                                    {
                                        line = inputStream.ReadLine();
                                        logger.Info(line);
                                        if (line.IndexOf("Total objects updated", StringComparison.OrdinalIgnoreCase) > -1)
                                        {
                                            SubmitTwsOke = true;
                                        }
                                        twsCommandError += line + "\n";
                                    } while (inputStream.Peek() != -1);
                                    inputStream.Close();

                                    if (SubmitTwsOke)
                                    {
                                        //submit job to scheduler
                                        String aliasName = dto.ProjectCode + DateTime.Now.ToString("ddMMyyHHmmss");
                                        Console.WriteLine("Alias name: " + aliasName);
                                        if (File.Exists(logFolder + "SubmitToTWS1.txt"))
                                        {
                                            File.Delete(logFolder + "SubmitToTWS1.txt");
                                        }
                                        retValue = CSTDJobUtil.RunProcess("cmd.exe", "/C conman sbs OBTEMPLATE alias=" + aliasName + " > " + logFolder + "SubmitToTWS1.txt 2>&1 ");

                                        twsCommandError += "\n\n\n\n        /C conman sbs OBTEMPLATE alias=" + aliasName + "\n\n";
                                        inputStream = new StreamReader(logFolder + "SubmitToTWS1.txt");
                                        do
                                        {
                                            line = inputStream.ReadLine();
                                            logger.Info(line);
                                            if (line.IndexOf("cannot be submitted", StringComparison.OrdinalIgnoreCase) > -1)
                                            {
                                                twsError = true;
                                            }
                                            twsCommandError += line + "\n";
                                        } while (inputStream.Peek() != -1);
                                        inputStream.Close();
                                    }
                                    else
                                    {
                                        twsError = true;
                                    }
                                    String fileContent = "";
                                    if (twsError)
                                    {
                                        twsSubmitSts = "Error while submitting job to CED " + environment + " TWS for :\n\r";
                                        twsSubmitSts += "    QueueNo   -> " + dto.QueueNo + "\n\r";
                                        twsSubmitSts += "    BatchId   -> " + dto.BatchId + "\n\r";
                                        twsSubmitSts += "    BatchName -> " + dto.BatchName + "\n\r";
                                        twsSubmitSts += "    RequestBy -> " + dto.RequestBy + "\n\r";

                                        inputStream = new StreamReader(_TWS_TEMP_FOLDER + fileName);
                                        do
                                        {
                                            line = inputStream.ReadLine();
                                            fileContent += line + "\n";
                                        } while (inputStream.Peek() != -1);
                                        inputStream.Close();
                                    }
                                    File.Delete(_TWS_TEMP_FOLDER + fileName);

                                    if (!twsError)
                                    {
                                        //check TWS batchman
                                        if (File.Exists(logFolder + "checkTWSStatus.txt"))
                                        {
                                            File.Delete(logFolder + "checkTWSStatus.txt");
                                        }
                                        retValue = CSTDJobUtil.RunProcessWithReturn("cmd.exe", "/C conman status > " + logFolder + "checkTWSStatus.txt 2>&1 ");


                                        inputStream = new StreamReader(logFolder + "checkTWSStatus.txt");

                                        do
                                        {
                                            line = inputStream.ReadLine();
                                            logger.Info(line);
                                            if (!"".Equals(line.Trim()) && line.IndexOf("Batchman down", StringComparison.OrdinalIgnoreCase) > -1)
                                            {
                                                logger.Info("error");
                                                twsError = true;
                                                twsSubmitSts = "Batchman down on CED " + environment + " TWS";
                                            }
                                            twsCommandError += line + "\n";
                                        } while (inputStream.Peek() != -1);
                                        inputStream.Close();

                                    }


                                    if (twsError)
                                    {
                                        //send email
                                        if (_BATCH_SEND_MAIL)
                                        {
                                            CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                                            emailDTO.To = _BATCH_MAIL_TO;
                                            emailDTO.Subject = environment + " TWS Error!!!";
                                            emailDTO.Content =
                                                "" + twsSubmitSts + ". \n\rTemporarily counter measure ODB will run without TWS. \n\r" +
                                                "Please fix TWS, then restart NFS Batch Service. \n\r" +
                                                "Thank You.\n\r\n\r\n\r" +
                                                " Note, \n\r\n\r" +
                                                " Content of " + _TWS_TEMP_FOLDER + fileName + " : \n\r" +
                                                " =====================================================\n\r" +
                                                fileContent +
                                                " \n\r\n\r" +
                                                twsCommandError;
                                            CSTDEmailUtil.SendEmail(emailDTO);
                                        }

                                        //temporary by pass tws
                                        _BF_USE_TWS = false;
                                        //temporary no need monitor TWS
                                        _TWS_MONITOR = false;
                                        //redirect batch to batch handler directly
                                        CSTDBatchHandler handler = new CSTDBatchHandler(dto);
                                        Thread batchHandlerThread = new Thread(handler.Run);
                                        batchHandlerThread.Start();
                                    }
                                }
                                #endregion
                                else
                                {
                                    //CSTDBatchHandler handler = new CSTDBatchHandler(dto);
                                    //Thread batchHandlerThread = new Thread(handler.Run);
                                    //batchHandlerThread.Start();
                                    if ("1".Equals(dto.ConCurrency))
                                    {
                                        for (int threadIdx = 0; threadIdx < HandlerThreads.Length; threadIdx++)
                                        {
                                            if (HandlerThreads[threadIdx] != null &&
                                                HandlerThreads[threadIdx].Thread.IsAlive)
                                            {
                                                if (dto.BatchId.Equals(HandlerThreads[threadIdx].Dto.BatchId))
                                                {
                                                    hasRun = true;
                                                }
                                            }
                                        }
                                    }

                                    for (int threadIdx = 0; threadIdx < HandlerThreads.Length; threadIdx++)
                                    {
                                        if (!hasRun)
                                        {
                                            logger.Info("Thread Number : " + threadIdx);
                                            if (HandlerThreads[threadIdx] == null ||
                                                !HandlerThreads[threadIdx].Thread.IsAlive)
                                            {
                                                HandlerThreads[threadIdx] = new CSTDBatchThread();
                                                HandlerThreads[threadIdx].Dto = dto;
                                                CSTDBatchHandler handler = new CSTDBatchHandler(dto);
                                                HandlerThreads[threadIdx].Thread = new Thread(handler.Run);
                                                HandlerThreads[threadIdx].StartDate = DateTime.Now;
                                                HandlerThreads[threadIdx].Thread.Start();
                                                hasRun = true;
                                                CSTDBatchDAO.MoveQueueToHistory(dto.QueueNo);
                                                String Message = "\n" +
                                                    " Queue No. = " + HandlerThreads[threadIdx].Dto.QueueNo + ",\n" +
                                                    " Project Code = " + HandlerThreads[threadIdx].Dto.ProjectCode + ",\n" +
                                                    " Batch ID = " + HandlerThreads[threadIdx].Dto.BatchId + ",\n" +
                                                    " Batch Name = " + HandlerThreads[threadIdx].Dto.BatchName + ",\n" +
                                                    " Batch Parameter = " + HandlerThreads[threadIdx].Dto.Parameter + ",\n" +
                                                    " Request ID = " + HandlerThreads[threadIdx].Dto.RequestId + ",\n" +
                                                    " Request By = " + HandlerThreads[threadIdx].Dto.RequestBy + ",\n" +
                                                    " Started Time = " + ((DateTime)HandlerThreads[threadIdx].StartDate).ToString("dd-MM-yyy HH:mm:ss.fff") + "\n";
                                                logger.Info(Message);
                                            }
                                        }
                                    }
                                }
                            }
                        }
                    }
                    if (_TWS_MONITOR)
                    {
                        MonitorTWSStatus();
                    }
                }
                catch (Exception e)
                {
                    String ExceptionMessage = e.Message;
                    String StackTrace = e.StackTrace;
                    String Message = "";
                    //logger.Error(e.StackTrace);
                    //logger.Error(e.Message);
                    Message += e.Message + "\n";
                    Exception InnerException = e.InnerException;
                    while (InnerException != null)
                    {
                        InnerException = InnerException.InnerException;
                        Message += InnerException.Message + "\n";
                    }

                    logger.Error("Batch Error : " + "\n" + Message + "\n\nStack Trace :\n" + StackTrace);

                    if (_BATCH_SEND_MAIL)
                    {
                        CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                        emailDTO.To = _BATCH_MAIL_TO;
                        emailDTO.Subject = "Batch Error : " + ExceptionMessage;
                        emailDTO.Content = Message + "\n\nStack Trace :\n" + StackTrace;
                        CSTDEmailUtil.SendEmail(emailDTO);
                    }
                }
                try
                {
                    int IdleThread = 0;
                    for (int threadIdx = 0; threadIdx < HandlerThreads.Length; threadIdx++)
                    {
                        if (HandlerThreads[threadIdx] == null ||
                            !HandlerThreads[threadIdx].Thread.IsAlive)
                        {
                            IdleThread++;
                        }
                    }
                    logger.Info("Thread Count : " + HandlerThreads.Length + ", Thread Idle : " + IdleThread);
                }
                catch (Exception e)
                {
                    logger.Info("Error when counting Idle Thread.");
                }
                //Console.WriteLine(DateTime.Now);
                //Thread.Sleep(DELAY);
                //Task.Delay(DELAY).Wait();
                long MillisecondDiff = Int64.Parse (Math.Floor((DateTime.Now - PrevTime).TotalMilliseconds).ToString());
                if (Math.Abs(DELAY - MillisecondDiff) > 0 && Math.Abs(DELAY - MillisecondDiff) <= DELAY)
                {
                    Thread.Sleep(new TimeSpan(0,0,0,0,(int)Math.Abs(DELAY - MillisecondDiff)));
                }
            }
        }


        private void MonitorTWSStatus()
        {
            if (File.Exists(logFolder + "TWSStatus.txt"))
            {
                File.Delete(logFolder + "TWSStatus.txt");
            }
            String retValue = CSTDJobUtil.RunProcessWithReturn("cmd.exe", "/C conman ss > " + logFolder + "TWSStatus.txt 2>&1 ");

            StreamReader inputStream = new StreamReader(logFolder + "TWSStatus.txt");
            List<String> stTwsStatus = new List<String>();
            List<String> stTwsStatusEcexDetail = new List<String>();
            List<String> stTwsStatusAbendDetail = new List<String>();
            List<String> stTwsStatusProblem = new List<String>();
            List<String> stTwsHeader = new List<String>();
            String line;
            isTWSStatus = false;
            isTWSProblem = false;
            do
            {
                line = inputStream.ReadLine();
                stTwsStatus.Add(line);
                if (line.IndexOf("Scheduled for") == 0)
                {
                    isTWSStatus = false;
                }
                if (isTWSStatus)
                {
                    if ("EXEC".Equals(line.Substring(46, 5).Trim()))
                    {

                        String startTime = line.Substring(55, 5).Trim();
                        String sparateTime = line.Substring(57, 1).Trim();

                        if ("/".Equals(sparateTime))
                        {
                            char[] delimiterChars = { '/' };
                            String[] stDate = startTime.Split(delimiterChars);
                            DateTime xmas = new DateTime(DateTime.Now.Year, Int32.Parse(stDate[0]), Int32.Parse(stDate[1]));
                            double daysUntilChristmas = DateTime.Today.Subtract(xmas).TotalDays;
                            if (daysUntilChristmas * 24 > maxTWSHoursExec)
                            {
                                String JobStream = line.Substring(17, 17).Trim();
                                //isTWSProblem = true;
                                stTwsStatusProblem.Add(line);
                                MonitorTWSStatus_Detail(stTwsStatusEcexDetail, stTwsStatusProblem, "EXEC", JobStream);
                                stTwsStatusProblem.Add("");
                            }
                        }
                        else
                            if (":".Equals(sparateTime))
                            {
                                DateTime t_1 = DateTime.Now;
                                DateTime t_2 = Convert.ToDateTime(startTime + ":00");
                                TimeSpan duration = t_1 - t_2;
                                double hours = duration.TotalHours;
                                if (hours > maxTWSHoursExec)
                                {
                                    String JobStream = line.Substring(17, 17).Trim();
                                    //isTWSProblem = true;
                                    stTwsStatusProblem.Add(line);
                                    MonitorTWSStatus_Detail(stTwsStatusEcexDetail, stTwsStatusProblem, "EXEC", JobStream);
                                    stTwsStatusProblem.Add("");
                                }
                            }
                    }
                    if ("ABEND".Equals(line.Substring(46, 5).Trim()))
                    {
                        isTWSProblem = true;
                        stTwsStatusProblem.Add(line);
                        String JobStream = line.Substring(17, 17).Trim();
                        MonitorTWSStatus_Detail(stTwsStatusAbendDetail, stTwsStatusProblem, "ABEND", JobStream);

                    }
                    if ("STUCK".Equals(line.Substring(46, 5).Trim()))
                    {
                        isTWSProblem = true;
                        stTwsStatusProblem.Add(line);
                        String JobStream = line.Substring(17, 17).Trim();
                        MonitorTWSStatus_Detail(stTwsStatusAbendDetail, stTwsStatusProblem, "STUCK", JobStream);

                    }
                }
                if (line.IndexOf("Workstation      ") == 0 && stTwsHeader.Count == 0)
                {
                    stTwsHeader.Add(line);
                    isTWSStatus = true;
                }
            } while (inputStream.Peek() != -1);

            if (isTWSProblem)
            {
                //temporary no need monitor TWS
                _TWS_MONITOR = false;
                String stTWSProblem = "";
                stTWSProblem += " " + environment + " TWS Monitoring, \n";
                stTWSProblem += " Please analysizing for TWS Status, \n";
                stTWSProblem += "\n";
                stTWSProblem += " Problem detected  of TWS Status :\n";
                stTWSProblem += " ( ABEND or EXEC more then " + maxTWSHoursExec + " hours )\n";
                stTWSProblem += " ================================================================= \n";
                stTWSProblem += "\n";
                foreach (String currTwsHeader in stTwsHeader)
                {
                    stTWSProblem += currTwsHeader + "\n";
                }
                foreach (String stCurrTWSProblem in stTwsStatusProblem)
                {
                    stTWSProblem += stCurrTWSProblem + "\n";
                }
                stTWSProblem += "\n";
                stTWSProblem += "\n";
                stTWSProblem += " NFS " + environment + " system will not monitor TWS again until restarted CED Batch Service.\n";
                stTWSProblem += " After fixed TWS please restart NFS Batch Service.\n";
                stTWSProblem += " Thank You.\n\n";
                stTWSProblem += " Best Regards,\n";
                stTWSProblem += " CED Support.";
                stTWSProblem += "\n\n";
                stTWSProblem += " Note,\n";
                stTWSProblem += " List of TWS Status :\n";
                stTWSProblem += " ================================= \n\n";

                foreach (String stCurrTwsStatus in stTwsStatus)
                {
                    stTWSProblem += stCurrTwsStatus + "\n";
                }
                stTWSProblem += "\n\n";
                stTWSProblem += " List of TWS EXEC Status Detail :\n";
                stTWSProblem += " ============================================ \n\n";
                foreach (String stCurrTwsStatusDetail in stTwsStatusEcexDetail)
                {
                    stTWSProblem += stCurrTwsStatusDetail + "\n";
                }
                stTWSProblem += "\n\n";
                stTWSProblem += " List of TWS ABEND Status Detail :\n";
                stTWSProblem += " ============================================ \n\n";
                foreach (String stCurrTwsStatusDetail in stTwsStatusAbendDetail)
                {
                    stTWSProblem += stCurrTwsStatusDetail + "\n";
                }

                if (_BATCH_SEND_MAIL)
                {
                    CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                    emailDTO.To = _BATCH_MAIL_TO;
                    emailDTO.Subject = environment + " TWS Monitoring";
                    emailDTO.Content = stTWSProblem;
                    CSTDEmailUtil.SendEmail(emailDTO);
                }
            }
            else
            {
                _TWS_MONITOR = true;
            }
            inputStream.Close();
        }

        private void MonitorTWSStatus_Detail(List<String> stTwsStatus, List<String> stTwsStatusProblem, String Status, String JobStream)
        {
            if (File.Exists(logFolder + "TWSStatusDetail.txt"))
            {
                File.Delete(logFolder + "TWSStatusDetail.txt");
            }
            String retValue = CSTDJobUtil.RunProcessWithReturn("cmd.exe", "/C conman sj=" + JobStream + " " + Status + " > " + logFolder + "TWSStatusDetail.txt 2>&1 ");

            StreamReader inputStream = new StreamReader(logFolder + "TWSStatusDetail.txt");
            String line;
            do
            {

                line = inputStream.ReadLine();
                stTwsStatus.Add(line);
                String noNeesCheck = "********************";
                int noNeesCheckLength = noNeesCheck.Length;
                if (line != null && line.Length > 101 &&
                    (!noNeesCheck.Equals(line.Substring(45, noNeesCheckLength).Trim())) &&
                    Status.Equals(line.Substring(86, 5).Trim()))
                {
                    String startTime = line.Substring(95, 5).Trim();
                    String sparateTime = line.Substring(97, 1).Trim();

                    if ("/".Equals(sparateTime))
                    {
                        char[] delimiterChars = { '/' };
                        String[] stDate = startTime.Split(delimiterChars);
                        DateTime xmas = new DateTime(DateTime.Now.Year, Int32.Parse(stDate[0]), Int32.Parse(stDate[1]));
                        double daysUntilChristmas = DateTime.Today.Subtract(xmas).TotalDays;
                        if (daysUntilChristmas * 24 > maxTWSHoursExec)
                        {
                            isTWSProblem = true;
                            stTwsStatusProblem.Add(line);
                        }
                    }
                    else
                        if (":".Equals(sparateTime))
                        {
                            DateTime t_1 = DateTime.Now;
                            DateTime t_2 = Convert.ToDateTime(startTime + ":00");
                            TimeSpan duration = t_1 - t_2;
                            double hours = duration.TotalHours;
                            if (hours > maxTWSHoursExec)
                            {
                                isTWSProblem = true;
                                stTwsStatusProblem.Add(line);
                            }
                        }
                }
            } while (inputStream.Peek() != -1);

            inputStream.Close();
        }

    }

}


