﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.ServiceProcess;
using System.Text;
using BatchService.STD.Daemon;
using Common.STD.Util;

namespace BatchService
{
    static class Program
    {
        /// <summary>
        /// The main entry point for the application.
        /// </summary>
        static void Main()
        {
            try
            {
                CSTDMapper.SqlMapConfigLocation = "Resources/IBatisNetConfig/SqlMap.config"; // Call from batch, from screen defined on global.asax

                CSTDMapper.SqlBatchMapConfigLocation = "Resources/IBatisNetConfig/SqlBatchMap.config"; // Call from batch, from screen defined on global.asax


                /*
                Reference : 
                knowledge : http://msdn.microsoft.com/en-us/library/zt39148a(v=vs.110).aspx
                to install : C:\Windows\Microsoft.NET\Framework64\v4.0.30319\installUtil.exe D:\FUJITSU\CED\SRC\BatchService\bin\Debug\BatchService.exe
                After Service was installed, set Log On property to Administrator User
                */

#if DEBUG
                CSTDBatchDaemon daemon = new CSTDBatchDaemon(null);
                daemon.run();
                System.Threading.Thread.Sleep(System.Threading.Timeout.Infinite);
#else

                 ServiceBase[] ServicesToRun;
                ServicesToRun = new ServiceBase[] 
                { 
                new BatchService() 
                };
                ServiceBase.Run(ServicesToRun);

#endif




                //CSTDBatchDaemon daemon = new CSTDBatchDaemon(null);
                //daemon.run();
            }
            catch (Exception e)
            {

            }
        }
    }
}
