﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using log4net;
using log4net.Config;
using System.Reflection;
using Common.STD.Util;
using System.IO;
using System.Threading;
using BatchHandler.CED.Handler;

namespace BatchHandler.CED.Daemon
{
    public class CSTDBatchDaemon
    {
        static ILog logger = LogManager.GetLogger(typeof(CSTDBatchDaemon));
        public static volatile bool Running = true;

        public CSTDBatchDaemon()
        {
            #region set config location
            Assembly assembly = typeof(Program).Assembly; // in the same assembly!
            CSTDProperties.PropertiesPath = CSTDStringUtil.GetApplicationResourcesPath;

            CSTDMapper.SqlMapConfigLocation = "Resources/IBatisNetConfig/SqlMapBatch.config";
            CSTDMapper.SqlBatchMapConfigLocation = "bin/Resources/IBatisNetConfig/SqlBatchMapOnline.config";

            FileInfo log4netConfig = new FileInfo(CSTDProperties.PropertiesPath + "Log4NetConfig/Log4net.xml");
            XmlConfigurator.Configure(log4netConfig);
            #endregion
            CSTDBatchDaemon.Running = true;
        }

        public void Run()
        {
            logger.Info("Retrieve batch info from system master");

            Dictionary<String, Thread> listThread = new Dictionary<string, Thread>();

            IList<String> listBatchId = CSTDMapper.Instance().QueryForList<String>("Batch.ListBatch", null);
            if (listBatchId == null || listBatchId.Count == 0)
            {
                logger.Fatal("Can't find batch info from system master. Daemon will exit now!");
                return;
            }

            logger.Info("Found " + listBatchId.Count + " batch configuration from system master.");
            logger.Info("Start loading each batch configuration as thread");
            foreach (String batchId in listBatchId)
            {
                logger.Info("===============================");
                logger.Info("Batch ID: " + batchId);
                logger.Info("Load and validate configuration");

                CSTDBatchHandler handler = new CSTDBatchHandler(batchId);
                IList<String> messages = handler.LoadBatchInfo();
                if (messages.Count == 0)
                {
                    Thread t = new Thread(handler.Run);
                    t.Start();
                    listThread[batchId] = t;
                    logger.Info("Load and validate completed successfully. Thread for batch started.");
                }
                else
                {
                    foreach (String message in messages)
                    {
                        logger.Error(message);
                    }
                    logger.Error("Load and validate completed with error.");
                }
                logger.Info("===============================");
            }
            logger.Info("CED Batch Service started on : " + CSTDDateUtil.GetCurrentDBDate());

            while (CSTDBatchDaemon.Running)
            {
                Thread.Sleep(1000);
            }
            logger.Info("Prepare to stop CED Batch Service");
            CSTDBatchHandler.Running = false;
            foreach (KeyValuePair<String, Thread> t in listThread)
            {
                logger.Info("Stoping thread for batch id: "+ t.Key);
                t.Value.Join();
                logger.Info("Thread for batch id: " + t.Key + " stopped successfully");
            }
            logger.Info("CED Batch Service stopped on : " + CSTDDateUtil.GetCurrentDBDate());

        }
    }
}
