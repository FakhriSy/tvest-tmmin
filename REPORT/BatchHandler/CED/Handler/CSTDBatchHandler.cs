﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using Common.STD.Util;
using Common.STD.DTO;
using log4net;
using log4net.Config;
using System.Threading;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using System.IO;
using Common.STD.Base;
using Common.STD.DAO;

namespace BatchHandler.CED.Handler
{
    public class CSTDBatchHandler
    {
        private const int SLEEP_INTERVAL = 60 * 1000; //1 minute sleep
        private const int EVERY_SPECIFIC_TIME = 1, MINUTELY = 2, HOURLY = 3, DAILY = 4;
        private const int SUCCESS = 0;

        private String _SystemCategory;
        public static volatile bool Running = true;
        //private Dictionary<String, String> _BatchDTO;
        public Dictionary<String, String> _BatchDTO;
        public String processId;
        public int processStatus;
        private int _ExecutionScheduleType;
        private IDictionary<int, TimeSpan> _ExecutionSpecificDate;
        private TimeSpan _ExecutionStart;
        private TimeSpan _ExecutionEnd;
        private int _ExecutionInterval;
        private TimeSpan _ExecutionDaily;
        static ILog logger = LogManager.GetLogger(typeof(CSTDBatchHandler));

        public CSTDBatchHandlerDTO _dto = new CSTDBatchHandlerDTO();
        private CSTDBatchDTO _queue = null;
        static String HISTORY = "2";

        public CSTDBatchHandler(CSTDBatchDTO queue)
        {
            this._queue = queue;
        }

        public void ExecuteHandler(CSTDBatchDAO dao)
        {
            String tmpLine = null;
            try
            {
                tmpLine = "Start processing [batch-id: " + _queue.BatchId + ", request-by: " + _queue.RequestBy + ", on " + _queue.RequestDate + "]";
                logger.Info(tmpLine);

                //update running count
                dao.UpdateRunningCount(1, _queue.BatchId, _queue.ProjectCode);

                //execute bat
                CSTDJobUtil.RunProcess(_queue.Shell, "\"" + _queue.Parameter.Replace("#:#", "\" \"") + "\"");
            }
            catch (IOException e)
            {
                Console.WriteLine(e.Message);
                Console.WriteLine(e.StackTrace);
                logger.Error(e.StackTrace);
            }
            finally
            {
                dao.UpdateQueueStatus(HISTORY, "Completed Successfully", _queue.QueueNo);
                tmpLine = "Execution [batch-id: " + _queue.BatchId + ", request-by: " + _queue.RequestBy + ", on " + _queue.RequestDate + "] completed successfully";
                logger.Info(tmpLine);
                dao.UpdateRunningCount(-1, _queue.BatchId, _queue.ProjectCode);
                logger.Info("End processing [batch-id: " + _queue.BatchId + ", request-by: " + _queue.RequestBy + ", on " + _queue.RequestDate + "]");
            }
        }


        public void Run()
        {

            CSTDProperties.PropertiesPath = CSTDStringUtil.GetApplicationResourcesPath;
            CSTDBatchDAO dao = new CSTDBatchDAO();
            ExecuteHandler(dao);

            #region form PGI for scheduling
            /*
            int counter = _ExecutionInterval;

            while (Running)
            {
                LoadBatchInfo();
                DateTime CurrentDate = DateTime.Now;
                switch (_ExecutionScheduleType)
                {
                    case EVERY_SPECIFIC_TIME:
                        if (_ExecutionSpecificDate.ContainsKey(CurrentDate.Day))
                        {
                            TimeSpan t1 = _ExecutionSpecificDate[CurrentDate.Day];
                            if (t1.Hours == CurrentDate.Hour && t1.Minutes == CurrentDate.Minute)
                            {
                                //process batch
                                ProcessBatch();
                            }
                        }
                        break;
                    case MINUTELY:
                    case HOURLY:
                        {
                            if (CurrentDate.TimeOfDay.CompareTo(_ExecutionStart) >= 0 && CurrentDate.TimeOfDay.CompareTo(_ExecutionEnd) <= 0)
                            {
                                if (counter == _ExecutionInterval)
                                {
                                    ProcessBatch();
                                    counter = 0;
                                }
                                counter++;
                            }
                            break;
                        }
                    case DAILY:
                        if (_ExecutionDaily.Hours == CurrentDate.Hour && _ExecutionDaily.Minutes == CurrentDate.Minute)
                        {
                            //process batch
                            ProcessBatch();
                        }
                        break;
                }
                if (Running)
                {
                    Thread.Sleep(SLEEP_INTERVAL);
                }
            }
             */
            #endregion
        }


        public CSTDBatchHandler(String systemCategory)
        {
            _SystemCategory = systemCategory;
            _BatchDTO = new Dictionary<String, String>();
            _ExecutionSpecificDate = new Dictionary<int, TimeSpan>();
        }

        public void ProcessBatch()
        {
            //check if it's upload or export process
            if (_BatchDTO.ContainsKey("INPUT_FOLDER") && !String.IsNullOrEmpty(_BatchDTO["INPUT_FOLDER"]))
            {
                try
                {
                    ProcessUpload();
                }
                catch (Exception e)
                {
                    processStatus = 2;
                    _dto.Message = e.Message;
                    logger.Error("[" + _SystemCategory + "] Unable to process upload. Error: " + e.Message);
                }
            }
            else if (_BatchDTO.ContainsKey("OUTPUT_FOLDER") && !String.IsNullOrEmpty(_BatchDTO["OUTPUT_FOLDER"]))
            {
                try
                {
                    ProcessExport();
                }
                catch (Exception e)
                {
                    processStatus = 2;
                    _dto.Message = e.Message;
                    logger.Error("[" + _SystemCategory + "] Unable to process export. Error: " + e.Message);
                }
            }
        }

        private void ProcessUpload()
        {
            #region Declare variable
            #endregion

            #region Scan for available files
            DirectoryInfo info = new DirectoryInfo(_BatchDTO["INPUT_FOLDER"]);
            FileInfo[] files = info.GetFiles().OrderByDescending(p => p.CreationTime).ToArray();
            FileInfo uploadedFile = null;
            foreach (FileInfo file in files)
            {
                uploadedFile = file;
                break;
            }
            #endregion

            if (uploadedFile == null) return;

            //Insert Start Process Status into TB_R_BATCH_STATUS
            _dto.FunctionId = _BatchDTO["FUNCTION_ID"];
            _dto.SequenceNo = CSTDMapper.Instance().QueryForObject<String>("Batch.DoInsertStatus", _BatchDTO["FUNCTION_ID"]);

            #region Call stored procedure
            using (SqlConnection con = CSTDDBUtil.GetNonPooledConnection)
            {
                using (SqlCommand cmd = new SqlCommand(_BatchDTO["SP_NAME"], con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter(CSTDBaseDAO.LeadingSQLParameter + "PROCESS_ID", SqlDbType.Decimal));
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_ID"].Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(new SqlParameter(CSTDBaseDAO.LeadingSQLParameter + "PROCESS_STS", SqlDbType.Int));
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_STS"].Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(new SqlParameter(CSTDBaseDAO.LeadingSQLParameter + "FILENAME", SqlDbType.VarChar));
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "FILENAME"].Value = uploadedFile.Name;
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "FILENAME"].Direction = ParameterDirection.Input;

                    con.Open();
                    cmd.ExecuteNonQuery();

                    processStatus = (int)cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_STS"].Value;
                    processId = ((Decimal)cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_ID"].Value).ToString();
                }
            }
            #endregion

            #region Check Result
            String archiveFolder = null;
            if (processStatus == SUCCESS)
            {
                logger.Info("[" + _SystemCategory + "] Upload process for file: " + uploadedFile.Name + " finish successfully with process id: " + processId);
                archiveFolder = _BatchDTO["ARCHIVE_SUCCESS_FOLDER"];
            }
            else
            {
                logger.Error("[" + _SystemCategory + "] Upload process for file: " + uploadedFile.Name + " finish with error with process id: " + processId);
                try
                {
                    SendEmail(processId);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    logger.Error("[" + _SystemCategory + "] Error when sending email: " + e.Message);
                }
                archiveFolder = _BatchDTO["ARCHIVE_FAIL_FOLDER"];
            }

            //do archiving
            logger.Info("[" + _SystemCategory + "] Move file: " + uploadedFile.Name + " from " + _BatchDTO["INPUT_FOLDER"] + " into " + archiveFolder);
            String fileDest = archiveFolder + @"\" + uploadedFile.Name;
            if (File.Exists(fileDest)) File.Delete(fileDest);

            uploadedFile.MoveTo(fileDest);

            #endregion

            //Update Process Status of TB_R_BATCH_STATUS with previously Inserted Sequence No.
            _dto.BatchStatus = (processStatus == 0 ? "0" : "1");
            CSTDMapper.Instance().Update("Batch.DoUpdateStatus", _dto);
        }

        private void ProcessExport()
        {
            #region Declare variable
            String fileName;
            #endregion

            //Insert Start Process Status into TB_R_BATCH_STATUS
            _dto.FunctionId = _BatchDTO["FUNCTION_ID"];
            _dto.SequenceNo = CSTDMapper.Instance().QueryForObject<String>("Batch.DoInsertStatus", _BatchDTO["FUNCTION_ID"]);

            #region Call stored procedure
            using (SqlConnection con = CSTDDBUtil.GetNonPooledConnection)
            {
                using (SqlCommand cmd = new SqlCommand(_BatchDTO["SP_NAME"], con))
                {
                    cmd.CommandType = CommandType.StoredProcedure;

                    cmd.Parameters.Add(new SqlParameter(CSTDBaseDAO.LeadingSQLParameter + "PROCESS_ID", SqlDbType.Decimal));
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_ID"].Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(new SqlParameter(CSTDBaseDAO.LeadingSQLParameter + "PROCESS_STS", SqlDbType.Int));
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_STS"].Direction = ParameterDirection.Output;

                    cmd.Parameters.Add(new SqlParameter(CSTDBaseDAO.LeadingSQLParameter + "FILENAME", SqlDbType.VarChar, 100));
                    cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "FILENAME"].Direction = ParameterDirection.Output;

                    con.Open();
                    cmd.ExecuteNonQuery();

                    processStatus = (int)cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_STS"].Value;
                    processId = ((Decimal)cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "PROCESS_ID"].Value).ToString();
                    fileName = (String)cmd.Parameters[CSTDBaseDAO.LeadingSQLParameter + "FILENAME"].Value;
                }
            }
            #endregion

            #region Check Result
            if (processStatus == SUCCESS)
            {
                logger.Info("[" + _SystemCategory + "] Export process finish successfully with process id: " + processId);
            }
            else
            {
                logger.Error("[" + _SystemCategory + "] Export process finish with error with process id: " + processId);
                SendEmail(processId);
            }

            #endregion

            //Update Process Status of TB_R_BATCH_STATUS with previously Inserted Sequence No.
            _dto.BatchStatus = (processStatus == 0 ? "0" : "1");
            CSTDMapper.Instance().Update("Batch.DoUpdateStatus", _dto);
        }

        private void SendEmail(String ProcessId)
        {
            CSTDEmailDTO emailDto = CSTDMapper.Instance().QueryForObject<CSTDEmailDTO>("Batch.GetEmail", ProcessId);
            if (emailDto != null)
            {

                logger.Error("[" + _SystemCategory + "] Start sending email for process id: " + ProcessId);
                CSTDEmailUtil.SendEmail(emailDto);
                logger.Error("[" + _SystemCategory + "] Sending email end succesfuuly for process id: " + ProcessId);
            }
            else
            {
                logger.Error("[" + _SystemCategory + "] Unable to get notification email for process id: " + ProcessId);
            }
        }


        public IList<String> LoadBatchInfo()
        {
            bool loadResult = true;
            IList<String> messages = new List<String>();

            #region Load Batch Info
            IList<Hashtable> lstBatchInfo = CSTDMapper.Instance().QueryForList<Hashtable>("Batch.GetBatchInfo", _SystemCategory);

            foreach (Hashtable systemMaster in lstBatchInfo)
            {
                _BatchDTO[systemMaster["SYS_CD"].ToString()] = systemMaster["SYS_VAL"].ToString();
            }



            //mandatory setting for upload process
            if (_BatchDTO.ContainsKey("INPUT_FOLDER"))
            {
                String[] mandatorySetting = new String[] { "ARCHIVE_SUCCESS_FOLDER", "ARCHIVE_FAIL_FOLDER", "SP_NAME", "EMAIL_SUBJECT", "EMAIL_TO" };
                foreach (String setting in mandatorySetting)
                {
                    if (!_BatchDTO.ContainsKey(setting))
                    {
                        messages.Add("Setting: " + setting + " not found.");
                        loadResult = false;
                    }
                }
                //check folder existence
                if (loadResult)
                {
                    String[] folders = new String[] { "INPUT_FOLDER", "ARCHIVE_SUCCESS_FOLDER", "ARCHIVE_FAIL_FOLDER" };
                    foreach (String folder in folders)
                    {
                        if (!Directory.Exists(_BatchDTO[folder]))
                        {
                            messages.Add("Folder: " + _BatchDTO[folder] + " as " + folder + " not exist.");
                            loadResult = false;
                        }
                    }
                }
            }
            else if (_BatchDTO.ContainsKey("OUTPUT_FOLDER"))
            {
                String[] mandatorySetting = new String[] { "SP_NAME", "EMAIL_SUBJECT", "EMAIL_TO" };
                foreach (String setting in mandatorySetting)
                {
                    if (!_BatchDTO.ContainsKey(setting))
                    {
                        messages.Add("Setting: " + setting + " not found.");
                        loadResult = false;
                    }
                }

                //check folder existence
                if (loadResult)
                {
                    String[] folders = new String[] { "OUTPUT_FOLDER" };
                    foreach (String folder in folders)
                    {
                        if (!Directory.Exists(_BatchDTO[folder]))
                        {
                            messages.Add("Folder: " + folder + " not exist.");
                            loadResult = false;
                        }
                    }
                }
            }
            else
            {
                messages.Add("Setting: INPUT_FOLDER or OUTPUT_FOLDER not found.");
                loadResult = false;
            }

            _ExecutionScheduleType = CSTDNumberUtil.GetNumber(_BatchDTO["EXECUTION_SCHEDULE"]);
            switch (_ExecutionScheduleType)
            {
                case EVERY_SPECIFIC_TIME:
                    for (int i = 0; i < 11; i++)
                    {
                        if (_BatchDTO.ContainsKey("EXECUTION_DATE" + (i == 0 ? "" : "" + i)))
                        {
                            int executionDate = CSTDNumberUtil.GetNumber(_BatchDTO["EXECUTION_DATE" + (i == 0 ? "" : "" + i)]);
                            if (_BatchDTO.ContainsKey("EXECUTION_TIME" + (i == 0 ? "" : "" + i)))
                            {
                                _ExecutionSpecificDate[executionDate] = GetTimeSpan(_BatchDTO["EXECUTION_TIME" + (i == 0 ? "" : "" + i)]);
                            }
                            else
                            {
                                messages.Add("Can't find EXECUTION TIME" + (i == 0 ? "" : "" + i) + " as pair for EXECUTION_DATE" + (i == 0 ? "" : "" + i));
                                loadResult = false;
                            }
                        }

                    }
                    break;
                case MINUTELY:
                case HOURLY:
                    {
                        if (_BatchDTO.ContainsKey("EXECUTION_START_TIME") && _BatchDTO.ContainsKey("EXECUTION_STOP_TIME") && _BatchDTO.ContainsKey("EXECUTION_INTERVAL"))
                        {
                            _ExecutionStart = GetTimeSpan(_BatchDTO["EXECUTION_START_TIME"]);
                            _ExecutionEnd = GetTimeSpan(_BatchDTO["EXECUTION_STOP_TIME"]);
                            _ExecutionInterval = CSTDNumberUtil.GetNumber(_BatchDTO["EXECUTION_INTERVAL"]);
                        }
                        else
                        {
                            messages.Add("Execution Start, End, and Interval are mandatory for execution schedule type " + _ExecutionScheduleType);
                            loadResult = false;
                        }
                    }
                    break;
                case DAILY:
                    if (_BatchDTO.ContainsKey("EXECUTION_TIME") && _BatchDTO.ContainsKey("EXECUTION_INTERVAL"))
                    {
                        _ExecutionDaily = GetTimeSpan(_BatchDTO["EXECUTION_TIME"]);
                        _ExecutionInterval = CSTDNumberUtil.GetNumber(_BatchDTO["EXECUTION_INTERVAL"]);
                    }
                    else
                    {
                        messages.Add("Execution Time and Interval are mandatory for execution schedule type " + _ExecutionScheduleType);
                        loadResult = false;
                    }
                    break;
                default:
                    messages.Add("Incorrect value EXECUTION_SCHEDULE: " + _ExecutionScheduleType + ".Value must be either 1,2,3, or 4.");
                    break;
            }

            if (loadResult)
            {
                //sleep will be in minute.
                switch (_ExecutionScheduleType)
                {
                    case HOURLY:
                        _ExecutionInterval = 60 * _ExecutionInterval;
                        break;
                    case DAILY:
                        _ExecutionInterval = 24 * 60 * _ExecutionInterval;
                        break;
                }
            }
            #endregion
            return messages;
        }

        private TimeSpan GetTimeSpan(String time)
        {
            String[] tmp = time.Split(':');
            TimeSpan t1 = new TimeSpan(CSTDNumberUtil.GetNumber(tmp[0]), CSTDNumberUtil.GetNumber(tmp[1]), 0);
            return t1;
        }
    }
}
