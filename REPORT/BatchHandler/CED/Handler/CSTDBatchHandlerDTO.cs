﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace BatchHandler.CED.Handler
{
    public class CSTDBatchHandlerDTO
    {
        public String FunctionId { get; set; }
        public String SequenceNo { get; set; }
        public String BatchStatus { get; set; }
        public String Message { get; set; }

        public CSTDBatchHandlerDTO()
        {
        }
    }
}
