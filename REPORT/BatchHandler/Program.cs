﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using Common.STD.Util;
using BatchHandler.CED.Daemon;
using System.Threading;
namespace BatchHandler
{
    class Program
    {
        static void Main(string[] args)
        {
            CSTDBatchDaemon daemon = new CSTDBatchDaemon();
            Thread t = new Thread(daemon.Run);
            t.Start();
        }
    }
}
