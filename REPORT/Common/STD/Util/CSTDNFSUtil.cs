﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data.SqlClient;
using System.Data;

namespace Common.STD.Util
{
    public class CSTDNFSUtil
    {
        public const String PAINT = "CPNT";
        
        //public static ArrayList GetCurrentShiftDate(DateTime currentTime)
        //{
        //    IList<WorkTimeScheduleDTO> workTimeScheduleList = new List<WorkTimeScheduleDTO>();
        //    IList<PatternDTO> patternList = new List<PatternDTO>();
        //    ArrayList currentShiftDate = new ArrayList();
        //    String currentShift = null;
        //    //bool flagWorkTime = false;

        //    //get date based on IAMI Calendar
        //    String startNightShift = String.Format("{0}:00", CSTDSystemMasterHelper.GetValue("MASTER", "START_TIME", "1"));
        //    String currShift = GetCurrentProdShift();
        //    TimeSpan currTime = currentTime.TimeOfDay;
        //    if (currTime < System.TimeSpan.Parse("24:00:00") && currTime >= System.TimeSpan.Parse(startNightShift) && currShift.Equals("1"))
        //    {
        //        currentTime = currentTime.AddDays(1);
        //    }

        //    workTimeScheduleList = GetWorkTimeScheduleList(currentTime);
            
        //    if (workTimeScheduleList.Count > 0)
        //    {
        //        foreach (var item in workTimeScheduleList)
        //        {
        //            PatternDTO p = new PatternDTO();
        //            IList<PatternDTO> tempPatternList = new List<PatternDTO>();
        //            tempPatternList = GetPatternList(item);
        //            if(tempPatternList.Count > 0)
        //                patternList.Add(tempPatternList[0]);
        //        }

        //        foreach (var item in patternList)
        //        {
        //            TimeSpan st = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
        //            TimeSpan st1 = new TimeSpan(item.EndTime.Hour, item.EndTime.Minute, item.EndTime.Second);
        //            bool isBetween = IsTimeOfDayBetween(currentTime, st, st1);
        //            if (isBetween)
        //            {
        //                var p = new FinalWorkTimeScheduleParamDTO();
        //                p.PatternCd = item.PatternCd;
        //                p.ProdDate = currentTime;
        //                currentShift = GetShift(p);

        //                /* remark by Fid dio 07/01/2015
        //                */
        //                //TimeSpan currentTimeSpan = new TimeSpan(currentTime.Hour, currentTime.Minute, currentTime.Second);
        //                //TimeSpan twelveOclock = new TimeSpan(23, 59, 59);

        //                //if ((currentTimeSpan >= st) && (currentTimeSpan <= twelveOclock) && (flagWorkTime == false) && (currentShift != "2"))
        //                //{
        //                //    currentTime = currentTime.AddDays(1);
        //                //    currentShift = GetFinalCurrentShiftCode(currentTime);

        //                //}

        //            }
        //        }
        //    }

        //    currentShiftDate.Add(currentShift);
        //    currentShiftDate.Add(currentTime.ToString());

        //    return currentShiftDate;
        //}

        public static string GetCurrentShiftCode(DateTime currentTime)
        {
            IList<WorkTimeScheduleDTO> workTimeScheduleList = new List<WorkTimeScheduleDTO>();
            IList<PatternDTO> patternList = new List<PatternDTO>();
            String currentShift = null;
            bool flagWorkTime = false;


            workTimeScheduleList = GetWorkTimeScheduleList(currentTime);
            if (workTimeScheduleList.Count == 0)
            {
                currentTime = currentTime.AddDays(1);
                workTimeScheduleList = null;
                workTimeScheduleList = new List<WorkTimeScheduleDTO>();
                workTimeScheduleList = GetWorkTimeScheduleList(currentTime);
                flagWorkTime = true;
            }

            if (workTimeScheduleList.Count > 0)
            {
                foreach (var item in workTimeScheduleList)
                {
                    PatternDTO p = new PatternDTO();
                    IList<PatternDTO> tempPatternList = new List<PatternDTO>();
                    tempPatternList = GetPatternList(item);
                    patternList.Add(tempPatternList[0]);
                }

                foreach (var item in patternList)
                {
                    TimeSpan st = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
                    TimeSpan st1 = new TimeSpan(item.EndTime.Hour, item.EndTime.Minute, item.EndTime.Second);
                    bool isBetween = IsTimeOfDayBetween(currentTime, st, st1);
                    if (isBetween)
                    {
                        var p = new FinalWorkTimeScheduleParamDTO();
                        p.PatternCd = item.PatternCd;
                        p.ProdDate = currentTime;
                        currentShift = GetShift(p);

                        TimeSpan currentTimeSpan = new TimeSpan(currentTime.Hour, currentTime.Minute, currentTime.Second);
                        TimeSpan twelveOclock = new TimeSpan(23, 59, 59);

                        if ((currentTimeSpan >= st) && (currentTimeSpan <= twelveOclock) && (flagWorkTime == false) && (currentShift != "2"))
                        {
                            currentTime = currentTime.AddDays(1);
                            currentShift = GetFinalCurrentShiftCode(currentTime);

                        }

                    }
                }
            }

            return currentShift;
        }

        //public static string GetCurrentShiftCode(DateTime currentTime, String ShopCd, String SeriesCd)
        //{
        //    IList<WorkTimeScheduleDTO> workTimeScheduleList = new List<WorkTimeScheduleDTO>();
        //    IList<PatternDTO> patternList = new List<PatternDTO>();
        //    String currentShift = null;
        //    bool flagWorkTime = false;
        //    var param = new WorkTimeScheduleParamDTO();

        //    //get date based on IAMI Calendar
        //    String startNightShift = String.Format("{0}:00", CSTDSystemMasterHelper.GetValue("MASTER", "START_TIME", "1"));
        //    String currShift = GetCurrentProdShift();
        //    TimeSpan currTime = currentTime.TimeOfDay;
        //    if (currTime < System.TimeSpan.Parse("24:00:00") && currTime >= System.TimeSpan.Parse(startNightShift) && currShift.Equals("1"))
        //    {
        //        currentTime = currentTime.AddDays(1);
        //    }

        //    param.ProdDate = currentTime.ToString();
        //    param.ShopCd = ShopCd;
        //    param.SeriesCd = SeriesCd;

        //    workTimeScheduleList = GetWorkTimeScheduleList(param);
        //    if (workTimeScheduleList.Count == 0)
        //    {
        //        currentTime = currentTime.AddDays(1);
        //        workTimeScheduleList = null;
        //        workTimeScheduleList = new List<WorkTimeScheduleDTO>();
        //        workTimeScheduleList = GetWorkTimeScheduleList(param);
        //        flagWorkTime = true;
        //    }

        //    if (workTimeScheduleList.Count > 0)
        //    {
        //        foreach (var item in workTimeScheduleList)
        //        {
        //            PatternDTO p = new PatternDTO();
        //            IList<PatternDTO> tempPatternList = new List<PatternDTO>();
        //            tempPatternList = GetPatternList(item);
        //            patternList.Add(tempPatternList[0]);
        //        }

        //        foreach (var item in patternList)
        //        {
        //            TimeSpan st = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
        //            TimeSpan st1 = new TimeSpan(item.EndTime.Hour, item.EndTime.Minute, item.EndTime.Second);
        //            bool isBetween = IsTimeOfDayBetween(currentTime, st, st1);
        //            if (isBetween)
        //            {
        //                var p = new FinalWorkTimeScheduleParamDTO();
        //                p.PatternCd = item.PatternCd;
        //                p.ProdDate = currentTime;
        //                p.ShopCd = ShopCd;
        //                p.SeriesCd = SeriesCd;
        //                currentShift = GetShift(p);

        //                /* remark by Fid dio 07/12/204
        //                */
        //                //TimeSpan currentTimeSpan = new TimeSpan(currentTime.Hour, currentTime.Minute, currentTime.Second);
        //                //TimeSpan twelveOclock = new TimeSpan(23, 59, 59);

        //                //if ((currentTimeSpan >= st) && (currentTimeSpan <= twelveOclock) && (flagWorkTime == false) && (currentShift != "2"))
        //                //{
        //                //    currentTime = currentTime.AddDays(1);
        //                //    currentShift = GetFinalCurrentShiftCode(currentTime, ShopCd, SeriesCd);

        //                //}

        //            }
        //        }
        //    }

        //    return currentShift;
        //}

        //public static ArrayList GetCurrentShiftDate(DateTime currentTime, String ShopCd, String SeriesCd)
        //{
        //    IList<WorkTimeScheduleDTO> workTimeScheduleList = new List<WorkTimeScheduleDTO>();
        //    IList<PatternDTO> patternList = new List<PatternDTO>();
        //    ArrayList currentShiftDate = new ArrayList();
        //    String currentShift = null;
        //    bool flagWorkTime = false;
        //    var param = new WorkTimeScheduleParamDTO();

        //    //get date based on IAMI Calendar
        //    String startNightShift = String.Format("{0}:00", CSTDSystemMasterHelper.GetValue("MASTER", "START_TIME", "1"));
        //    String currShift = GetCurrentProdShift();
        //    TimeSpan currTime = currentTime.TimeOfDay;
        //    if (currTime < System.TimeSpan.Parse("24:00:00") && currTime >= System.TimeSpan.Parse(startNightShift) && currShift.Equals("1"))
        //    {
        //        currentTime = currentTime.AddDays(1);
        //    }

        //    param.ProdDate = currentTime.ToString();
        //    param.ShopCd = ShopCd;
        //    param.SeriesCd = SeriesCd;

        //    workTimeScheduleList = GetWorkTimeScheduleList(param);
        //    if (workTimeScheduleList.Count == 0)
        //    {
        //        currentTime = currentTime.AddDays(1);
        //        workTimeScheduleList = null;
        //        workTimeScheduleList = new List<WorkTimeScheduleDTO>();
        //        workTimeScheduleList = GetWorkTimeScheduleList(param);
        //        flagWorkTime = true;
        //    }

        //    if (workTimeScheduleList.Count > 0)
        //    {
        //        foreach (var item in workTimeScheduleList)
        //        {
        //            PatternDTO p = new PatternDTO();
        //            IList<PatternDTO> tempPatternList = new List<PatternDTO>();
        //            tempPatternList = GetPatternList(item);
        //            patternList.Add(tempPatternList[0]);
        //        }

        //        foreach (var item in patternList)
        //        {
        //            TimeSpan st = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
        //            TimeSpan st1 = new TimeSpan(item.EndTime.Hour, item.EndTime.Minute, item.EndTime.Second);
        //            bool isBetween = IsTimeOfDayBetween(currentTime, st, st1);
        //            if (isBetween)
        //            {
        //                var p = new FinalWorkTimeScheduleParamDTO();
        //                p.PatternCd = item.PatternCd;
        //                p.ProdDate = currentTime;
        //                p.ShopCd = ShopCd;
        //                p.SeriesCd = SeriesCd;
        //                currentShift = GetShift(p);

        //                /* remark by Fid dio 07/12/204
        //                 */
        //                //TimeSpan currentTimeSpan = new TimeSpan(currentTime.Hour, currentTime.Minute, currentTime.Second);
        //                //TimeSpan twelveOclock = new TimeSpan(23, 59, 59);

        //                //if ((currentTimeSpan >= st) && (currentTimeSpan <= twelveOclock) && (flagWorkTime == false) && (currentShift != "2"))
        //                //{
        //                //    currentTime = currentTime.AddDays(1);
        //                //    currentShift = GetFinalCurrentShiftCode(currentTime, ShopCd, SeriesCd);

        //                //}

        //            }
        //        }
        //    }

        //    currentShiftDate.Add(currentShift);
        //    currentShiftDate.Add(currentTime.ToString());

        //    return currentShiftDate;
        //}

        public static string GetFinalCurrentShiftCode(DateTime currentTime)
        {
            IList<WorkTimeScheduleDTO> workTimeScheduleList = new List<WorkTimeScheduleDTO>();
            IList<PatternDTO> patternList = new List<PatternDTO>();
            String currentShift = null;


            workTimeScheduleList = GetWorkTimeScheduleList(currentTime);
            if (workTimeScheduleList.Count > 0)
            {
                foreach (var item in workTimeScheduleList)
                {
                    PatternDTO p = new PatternDTO();
                    IList<PatternDTO> tempPatternList = new List<PatternDTO>();
                    tempPatternList = GetPatternList(item);
                    patternList.Add(tempPatternList[0]);
                }

                foreach (var item in patternList)
                {
                    TimeSpan st = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
                    TimeSpan st1 = new TimeSpan(item.EndTime.Hour, item.EndTime.Minute, item.EndTime.Second);
                    bool isBetween = IsTimeOfDayBetween(currentTime, st, st1);
                    if (isBetween)
                    {
                        var p = new FinalWorkTimeScheduleParamDTO();
                        p.PatternCd = item.PatternCd;
                        p.ProdDate = currentTime;
                        currentShift = GetShift(p);
                    }
                }
            }

            return currentShift;
        }

        

        public static string GetFinalCurrentShiftCode(DateTime currentTime, String ShopCd, String SeriesCd)
        {
            IList<WorkTimeScheduleDTO> workTimeScheduleList = new List<WorkTimeScheduleDTO>();
            IList<PatternDTO> patternList = new List<PatternDTO>();
            String currentShift = null;
            var param = new WorkTimeScheduleParamDTO();

            param.ProdDate = currentTime.ToString();
            param.ShopCd = ShopCd;
            param.SeriesCd = SeriesCd;

            workTimeScheduleList = GetWorkTimeScheduleList(param);
            if (workTimeScheduleList.Count > 0)
            {
                foreach (var item in workTimeScheduleList)
                {
                    PatternDTO p = new PatternDTO();
                    IList<PatternDTO> tempPatternList = new List<PatternDTO>();
                    tempPatternList = GetPatternList(item);
                    patternList.Add(tempPatternList[0]);
                }

                foreach (var item in patternList)
                {
                    TimeSpan st = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
                    TimeSpan st1 = new TimeSpan(item.EndTime.Hour, item.EndTime.Minute, item.EndTime.Second);
                    bool isBetween = IsTimeOfDayBetween(currentTime, st, st1);
                    if (isBetween)
                    {
                        var p = new FinalWorkTimeScheduleParamDTO();
                        p.PatternCd = item.PatternCd;
                        p.ProdDate = currentTime;
                        p.ShopCd = ShopCd;
                        p.SeriesCd = SeriesCd;
                        currentShift = GetShift(p);
                    }
                }
            }

            return currentShift;
        }

        static public bool IsTimeOfDayBetween(DateTime time, TimeSpan startTime, TimeSpan endTime)
        {
            if (endTime == startTime)
            {
                return true;
            }
            else if (endTime < startTime)
            {
                return time.TimeOfDay <= endTime ||
                    time.TimeOfDay >= startTime;
            }
            else
            {
                return time.TimeOfDay >= startTime &&
                    time.TimeOfDay <= endTime;
            }

        }

        /* method to get next shift and date */
        public static ArrayList GetNextShiftDate(DateTime currentTime)
        {
            ArrayList nextShiftDate = new ArrayList();
            String nextShift = "";
            DateTime nextDate;
            TimeSpan StartTimeNight = GetStartTime(currentTime, "1");
            TimeSpan StartTimeDay = GetStartTime(currentTime, "2");
            TimeSpan twelveOclock = new TimeSpan(23, 59, 59);

            TimeSpan currentTimeSpan = new TimeSpan(currentTime.Hour, currentTime.Minute, currentTime.Second);

            if (currentTimeSpan <= StartTimeNight && currentTimeSpan <= StartTimeDay)
            {
                nextShift = "2";
            }
            else
            {
                nextShift = "1";
            }


            /*  Logic for handling when time is between start night shift time and 23:59
	            If the time is between these, next shift should be Day
            */
            if (currentTimeSpan >= StartTimeNight && currentTimeSpan <= twelveOclock)
                nextShift = "2";

            /*
             * logic for checking no data for shift night or day
             */
            if (StartTimeNight == StartTimeDay)
                nextShift = "";

            if ("1".Equals(nextShift))
            {
                nextDate = currentTime.AddDays(1);
            }
            else
            {
                nextDate = currentTime;
            }

            nextShiftDate.Add(nextShift);
            nextShiftDate.Add(nextDate);

            return nextShiftDate;
        }

        /* Method to get start time based on Shift and Prod Date*/
        public static TimeSpan GetStartTime(DateTime currentTime, String ProdShift)
        {
            IList<WorkTimeScheduleDTO> patternShiftList = new List<WorkTimeScheduleDTO>();
            IList<PatternDTO> patternList = new List<PatternDTO>();
            ArrayList nextShiftDate = new ArrayList();
            TimeSpan startTime = new TimeSpan();
            TimeSpan firstStartTime = default(TimeSpan);

            patternShiftList = GetPatternShiftList(currentTime, ProdShift);

            if (patternShiftList.Count == 0)
            {
                currentTime = currentTime.AddDays(1);
                patternShiftList = null;
                patternShiftList = new List<WorkTimeScheduleDTO>();
                patternShiftList = GetPatternShiftList(currentTime, ProdShift);
            }

            if (patternShiftList.Count > 0)
            {
                foreach (var item in patternShiftList)
                {
                    PatternDTO p = new PatternDTO();
                    IList<PatternDTO> tempPatternList = new List<PatternDTO>();
                    tempPatternList = GetPatternList(item);
                    patternList.Add(tempPatternList[0]);
                }

                
                foreach (var item in patternList)
                {
                   startTime = new TimeSpan(item.StartTime.Hour, item.StartTime.Minute, item.StartTime.Second);
                   if (firstStartTime == default(TimeSpan))
                   {
                        firstStartTime = startTime;
                   }
                   else
                   {
                       if (startTime <= firstStartTime)
                       {
                           firstStartTime = startTime;
                       }
                   }
                }
            }

            return firstStartTime;
        }

        public static IList<WorkTimeScheduleDTO> GetWorkTimeScheduleList(DateTime currentTime)
        {
            SqlConnection con = null;

            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = "SELECT DISTINCT [PATTERN_CD] FROM [WASSQL].[ANDON].[dbo].[TB_R_WORK_TIME_SCHEDULE] where PROD_DT= CONVERT(DATE, '" + 
                    currentTime.ToString("dd-MM-yyyy") + "', 105)";

                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);

                List<WorkTimeScheduleDTO> resultList = new List<WorkTimeScheduleDTO>();
                WorkTimeScheduleDTO workTime;

                foreach (DataRow row in dt.Rows)
                {
                    workTime = new WorkTimeScheduleDTO();
                    workTime.PatternCd = row["PATTERN_CD"].ToString();

                    resultList.Add(workTime);
                }

                //resultList = CSTDMapper.Instance().QueryForList<WorkTimeScheduleDTO>("STDNFS.DoGetWorkTimeSchedule", currentTime);

                return resultList;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally 
            {
                if (con.State == ConnectionState.Open && con != null) {
                    con.Close();
                }
            }
        }

        public static IList<WorkTimeScheduleDTO> GetWorkTimeScheduleList(WorkTimeScheduleParamDTO param)
        {
            SqlConnection con = null;

            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = "SELECT DISTINCT [PATTERN_CD] FROM [WASSQL].[ANDON].[dbo].[TB_R_WORK_TIME_SCHEDULE] where PROD_DT= CONVERT(DATE, '" + param.ProdDate + "', 101)" +
                    (String.IsNullOrEmpty(param.ShopCd) ? "" : " AND SHOP_CD = '" + param.ShopCd + "'") +
                    (String.IsNullOrEmpty(param.SeriesCd) ? "" : " AND SERIES_CD = '" + param.SeriesCd + "'");

                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);

                List<WorkTimeScheduleDTO> resultList = new List<WorkTimeScheduleDTO>();
                WorkTimeScheduleDTO workTime;

                foreach (DataRow row in dt.Rows)
                {
                    workTime = new WorkTimeScheduleDTO();
                    workTime.PatternCd = row["PATTERN_CD"].ToString();

                    resultList.Add(workTime);
                }
                return resultList;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con.State == ConnectionState.Open && con != null)
                {
                    con.Close();
                }
            }

            //IList<WorkTimeScheduleDTO> resultList = CSTDMapper.Instance().QueryForList<WorkTimeScheduleDTO>("STDNFS.DoGetWorkTimeByShopSeries", param);
            //return resultList;
        }

        /* Get Pattern List based on Shift */
        public static IList<WorkTimeScheduleDTO> GetPatternShiftList(DateTime currentTime, String prodShift)
        {
            SqlConnection con = null;

            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = "SELECT DISTINCT [PATTERN_CD] FROM [WASSQL].[ANDON].[dbo].[TB_R_WORK_TIME_SCHEDULE]where PROD_DT= CONVERT(DATE, '" +
                    currentTime.ToString("dd-MM-yyyy") + "', 105) AND PROD_SHIFT='" + prodShift + "'";

                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);

                List<WorkTimeScheduleDTO> resultList = new List<WorkTimeScheduleDTO>();
                WorkTimeScheduleDTO workTime;

                foreach (DataRow row in dt.Rows)
                {
                    workTime = new WorkTimeScheduleDTO();
                    workTime.PatternCd = row["PATTERN_CD"].ToString();

                    resultList.Add(workTime);
                }

                return resultList;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con.State == ConnectionState.Open && con != null)
                {
                    con.Close();
                }
            }
        }

        public static IList<PatternDTO> GetPatternList(WorkTimeScheduleDTO item)
        {
            SqlConnection con = null;
            
            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = @"declare @patternCode varchar(5),
                              @MinutesToAdd int

                              set @patternCode = '" + item.PatternCd + @"';
                              set @MinutesToAdd = (CONVERT(int, (SELECT SYS_VAL
                                                                  FROM TB_M_SYSTEM
                                                                  WHERE 1 = 1
                                                                  AND SYS_CAT = 'MASTER'
                                                                  AND SYS_SUB_CAT = 'ADDITIONAL_TIME'
                                                                  AND SYS_CD = 2)))
                              SELECT distinct
                              p1.PATTERN_CD,
                              cast(startTime.START_TIME as datetime) as START_TIME,
                              cast(stopTime.STOP_TIME as datetime) as END_TIME
                              FROM [WASSQL].[ANDON].[dbo].[TB_M_PATTERN_D] p1
                              inner join (SELECT TOP 1 p.[START_TIME] FROM [WASSQL].[ANDON].[dbo].[TB_M_PATTERN_D] p  where p.PATTERN_CD=@patternCode order by p.SEQUENCE asc) startTime
                              on p1.PATTERN_CD = @patternCode
                              inner join (SELECT TOP 1 DATEADD(SECOND, @MinutesToAdd, p2.[STOP_TIME]) AS STOP_TIME FROM [WASSQL].[ANDON].[dbo].[TB_M_PATTERN_D] p2 where p2.PATTERN_CD=@patternCode order by p2.SEQUENCE desc) stopTime
                              on p1.PATTERN_CD = @patternCode";

                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);

                List<PatternDTO> patternList = new List<PatternDTO>();
                PatternDTO pattern;

                foreach (DataRow row in dt.Rows)
                {
                    pattern = new PatternDTO();

                    pattern.PatternCd = row["PATTERN_CD"].ToString();
                    pattern.StartTime = Convert.ToDateTime(row["START_TIME"].ToString());
                    pattern.EndTime = Convert.ToDateTime(row["END_TIME"].ToString());

                    patternList.Add(pattern);
                }
                return patternList;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con.State == ConnectionState.Open && con != null)
                {
                    con.Close();
                }
            }

            //IList<PatternDTO> resultList = CSTDMapper.Instance().QueryForList<PatternDTO>("STDNFS.DoGetPattern", item);
            //return resultList;
        }

        public static string GetShift(FinalWorkTimeScheduleParamDTO p)
        {
            SqlConnection con = null;

            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = "SELECT TOP 1 [PROD_SHIFT] FROM [WASSQL].[ANDON].[dbo].[TB_R_WORK_TIME_SCHEDULE] where PATTERN_CD='" + p.PatternCd +
                    "' and PROD_DT=CONVERT(DATE, '" + p.ProdDate.ToString("dd-MM-yyyy") +"', 105)" +
                    (String.IsNullOrEmpty(p.ShopCd) ? "" : " AND SHOP_CD = '" + p.ShopCd + "'") +
                    (String.IsNullOrEmpty(p.SeriesCd) ? "" : " AND SERIES_CD = '" + p.SeriesCd + "'");

                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);

                List<FinalWorkTimeScheduleDTO> listFinalWts = new List<FinalWorkTimeScheduleDTO>();
                FinalWorkTimeScheduleDTO finalWts;

                foreach (DataRow row in dt.Rows)
                {
                    finalWts = new FinalWorkTimeScheduleDTO();
                    finalWts.ProdShift = row["PROD_SHIFT"].ToString();

                    listFinalWts.Add(finalWts);
                }
                if (dt.Rows.Count > 0)
                {
                    return listFinalWts[0].ProdShift;
                }
                return null;
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con.State == ConnectionState.Open && con != null)
                {
                    con.Close();
                }
            }

            //IList<FinalWorkTimeScheduleDTO> listFinalWts = new List<FinalWorkTimeScheduleDTO>();
            //listFinalWts = CSTDMapper.Instance().QueryForList<FinalWorkTimeScheduleDTO>("STDNFS.DoGetShift", p);
            //return listFinalWts[0].ProdShift;
        }

        //public static String GetNextShift()
        //{
        //    //get next shift
        //    String endNightShift = String.Format("{0}:00", CSTDSystemMasterHelper.GetValue("MASTER", "END_TIME", "1"));
        //    int currShift = Convert.ToInt32(GetCurrentProdShift());
        //    DateTime currDate = CSTDDateUtil.GetCurrentDBDate();
        //    TimeSpan currTime = currDate.TimeOfDay;
        //    if (currTime >= System.TimeSpan.Parse("00:00:00") && currTime < System.TimeSpan.Parse(endNightShift) && currShift == 1)
        //    {
        //        currDate = currDate.AddDays(-1);
        //    }

        //    int nextShift = (currShift % 2) + 1;
        //    DateTime nextDate = currDate.AddDays(1);
        //    String output = String.Format("{0}|{1}", nextDate.ToString("yyyy-MM-dd"), nextShift);
        //    return output;

        //    //end get next shift
        //}

        /* method to get shift and date */
        //public static ArrayList GetCurrentShift()
        //{
        //    ArrayList ShiftDate = new ArrayList();
           

        //    //get shift
        //    String startNightShift = String.Format("{0}:00", CSTDSystemMasterHelper.GetValue("MASTER", "START_TIME", "1"));
        //    String currShift = GetCurrentProdShift();
        //    DateTime currDate = CSTDDateUtil.GetCurrentDBDate();
        //    TimeSpan currTime = currDate.TimeOfDay;
        //    if (currTime < System.TimeSpan.Parse("24:00:00") && currTime >= System.TimeSpan.Parse(startNightShift) && currShift.Equals("1"))
        //    {
        //        currDate = currDate.AddDays(1);
        //    }

        //    ShiftDate.Add(currShift);
        //    ShiftDate.Add(currDate);

        //    return ShiftDate;
        //}

        public static String GetCurrentProdShift()
        {
            DataTable dt = CSTDDBUtil.ExecuteQuery("SELECT CASE WHEN ((select CONVERT(TIME,GETDATE())) BETWEEN (SELECT CONVERT(TIME,A.SYS_VAL) FROM TB_M_SYSTEM A WHERE A.SYS_CAT = 'MASTER' AND A.SYS_SUB_CAT = 'START_TIME' AND SYS_CD = '2') AND (SELECT CONVERT(TIME,A.SYS_VAL) FROM TB_M_SYSTEM A WHERE A.SYS_CAT = 'MASTER' AND A.SYS_SUB_CAT = 'END_TIME' AND SYS_CD = '2')) THEN (select SYS_CD from TB_M_SYSTEM where SYS_CAT = 'MASTER' and SYS_SUB_CAT = 'PRODUCTION_SHIFT' and SYS_VAL = 'Day') ELSE (select SYS_CD from TB_M_SYSTEM where SYS_CAT = 'MASTER' and SYS_SUB_CAT = 'PRODUCTION_SHIFT' and SYS_VAL = 'Night') END AS PROD_SHIFT");
            return dt.Rows[0][0].ToString();
        }

        public static String GetShift(DateTime date)
        {
            SqlConnection con = null;

            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = "SELECT CASE WHEN ((select CONVERT(TIME,'" + date.ToString("MM/dd/yyyy HH:mm:ss") + "')) BETWEEN (SELECT CONVERT(TIME,A.SYS_VAL) FROM TB_M_SYSTEM A WHERE A.SYS_CAT = 'MASTER' AND A.SYS_SUB_CAT = 'START_TIME' AND SYS_CD = '2') AND (SELECT CONVERT(TIME,A.SYS_VAL) FROM TB_M_SYSTEM A WHERE A.SYS_CAT = 'MASTER' AND A.SYS_SUB_CAT = 'END_TIME' AND SYS_CD = '2')) THEN (select SYS_CD from TB_M_SYSTEM where SYS_CAT = 'MASTER' and SYS_SUB_CAT = 'PRODUCTION_SHIFT' and SYS_VAL = 'Day') ELSE (select SYS_CD from TB_M_SYSTEM where SYS_CAT = 'MASTER' and SYS_SUB_CAT = 'PRODUCTION_SHIFT' and SYS_VAL = 'Night') END AS PROD_SHIFT";
                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);
                return dt.Rows[0][0].ToString();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con.State == ConnectionState.Open && con != null)
                {
                    con.Close();
                }
            }
        }

        public static DateTime GetStartTime()
        {
            SqlConnection con = null;

            try
            {
                con = CSTDDBUtil.GetNonPooledConnection;
                con.Open();
                String sql = @"SELECT cast([SYS_VAL] as datetime) as START_TIME
                  FROM [TB_M_SYSTEM]
                  where SYS_CAT = 'MASTER' 
                  AND SYS_SUB_CAT= 'START_TIME'
                  AND SYS_CD = '1'";
                DataTable dt = CSTDDBUtil.ExecuteQuery(con, sql);
                return Convert.ToDateTime(dt.Rows[0][0].ToString());
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con.State == ConnectionState.Open && con != null)
                {
                    con.Close();
                }
            }
        }

        public static String GetNextProdShift()
        {
            String NextShift;

            DataTable dt = CSTDDBUtil.ExecuteQuery("SELECT CASE WHEN ((select CONVERT(TIME,GETDATE())) BETWEEN (SELECT CONVERT(TIME,A.SYS_VAL) FROM TB_M_SYSTEM A WHERE A.SYS_CAT = 'MASTER' AND A.SYS_SUB_CAT = 'START_TIME' AND SYS_CD = '2') AND (SELECT CONVERT(TIME,A.SYS_VAL) FROM TB_M_SYSTEM A WHERE A.SYS_CAT = 'MASTER' AND A.SYS_SUB_CAT = 'END_TIME' AND SYS_CD = '2')) THEN (select SYS_CD from TB_M_SYSTEM where SYS_CAT = 'MASTER' and SYS_SUB_CAT = 'PRODUCTION_SHIFT' and SYS_VAL = 'Day') ELSE (select SYS_CD from TB_M_SYSTEM where SYS_CAT = 'MASTER' and SYS_SUB_CAT = 'PRODUCTION_SHIFT' and SYS_VAL = 'Night') END AS PROD_SHIFT");
            if ("1".Equals(dt.Rows[0][0].ToString())) {
                NextShift = "2";
            }else{
                NextShift = "1";
            }

            return NextShift;
        }

    }

    public class WorkTimeScheduleParamDTO
    {
        public String ProdDate { get; set; }
        public String ShopCd { get; set; }
        public String SeriesCd { get; set; }
    }

    public class WorkTimeScheduleDTO
    {
        public String PatternCd { get; set; }
    }

    public class PatternDTO
    {
        public String PatternCd { get; set; }
        public DateTime StartTime { get; set; }
        public DateTime EndTime { get; set; }
    }

    public class FinalWorkTimeScheduleParamDTO
    {
        public DateTime ProdDate { get; set; }
        public String PatternCd { get; set; }
        public String ShopCd { get; set; }
        public String SeriesCd { get; set; }
    }

    public class FinalWorkTimeScheduleDTO
    {
        public String ProdShift { get; set; }
    }
}