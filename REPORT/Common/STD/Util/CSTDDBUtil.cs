﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Data.SqlTypes;
using Common.STD.DTO;
using Common.STD.Base;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using IBatisNet.DataMapper.Configuration.Statements;
using System.Text.RegularExpressions;
using System.ComponentModel;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDDBUtil
    {
        public static SqlConnection GetPooledConnection
        {
            get
            {
                return new SqlConnection(CSTDMapper.Instance().DataSource.ConnectionString);
            }
        }

        public static SqlConnection GetNonPooledConnection
        {
            get
            {
                return new SqlConnection(CSTDMapper.Instance().DataSource.ConnectionString);
            }
        }

        public static SqlConnection GetBatchConnection
        {
            get
            {
                return new SqlConnection(CSTDMapper.BatchInstance().DataSource.ConnectionString);
            }
        }

        public static DataTable ExecuteQuery(String query)
        {
            SqlConnection con = null;
            try
            {
                con = GetPooledConnection;
                con.Open();
                return ExecuteQuery(con, query);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }
        public static DataTable ExecuteQuery(SqlConnection con, String query)
        {
            DataTable dataTable = new DataTable();
            dataTable.Locale = System.Globalization.CultureInfo.CurrentCulture;
            using (dataTable)
            {

                using (SqlCommand command = con.CreateCommand())
                {
                    command.CommandText = query;
                    //IAsyncResult result = command.BeginExecuteReader();
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        dataTable.Load(dr);
                    }
                    //command.EndExecuteReader(result);
                }
                return dataTable;
            }
        }

        public static DataTable ExecuteQuery(String query, Hashtable queryParameters)
        {
            return ExecuteQuery( query,  queryParameters, -1);
        }
        public static DataTable ExecuteQuery(String query, Hashtable queryParameters, int timeout)
        {
            SqlConnection con = null;
            try
            {
                con = GetPooledConnection;
                con.Open();
                return ExecuteQuery(con, query, queryParameters, timeout);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }


        public static DataTable ExecuteQuery(SqlConnection con, String query, Hashtable queryParameters)
        {
            return ExecuteQuery(con, query, queryParameters, -1);
        }

        public static DataTable ExecuteQuery(SqlConnection con, String query, Hashtable queryParameters,int timeout)
        {
           return ExecuteQuery(con, null, query, queryParameters, timeout);
        }
        public static DataTable ExecuteQuery(SqlConnection con, SqlTransaction sqlTrans, String query, Hashtable queryParameters, int timeout)
        {
            DataTable dataTable = new DataTable();
            dataTable.Locale = System.Globalization.CultureInfo.CurrentCulture;
            using (dataTable)
            {
                using (SqlCommand command = con.CreateCommand())
                {
                    if (sqlTrans != null)
                    {
                        command.Transaction = sqlTrans;
                    }
                    if (timeout != -1)
                    {
                        command.CommandTimeout = timeout;
                    }
                    command.CommandText = query;
                    foreach (String key in queryParameters.Keys)
                    {
                        command.Parameters.AddWithValue(key, queryParameters[key]);
                    }
                    //IAsyncResult result = command.BeginExecuteReader();
                    using (SqlDataReader dr = command.ExecuteReader())
                    {
                        dataTable.Load(dr);
                    }
                    //command.EndExecuteReader(result);
                }
                return dataTable;
            }
        }

        /*
        public static int ExecuteUpdate(String query, Hashtable queryParameters)
        {
            SqlConnection con = null;
            try
            {
                con.Open();
                return ExecuteUpdate(con, query, queryParameters);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }
         * */

        public static int ExecuteUpdate(SqlConnection con, SqlTransaction sqlTrans, String query, Hashtable queryParameters)
        {
            bool NeedClose = false;
            if (con.State == ConnectionState.Closed)
            {
                con.Open();
                NeedClose = true;
            }
            using (SqlCommand command = new SqlCommand(query, con))
            {
                if (sqlTrans != null)
                {
                    command.Transaction = sqlTrans;
                }
                return ExecuteUpdate(command, queryParameters);
            }
            if (NeedClose)
            {
                if (con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public static int ExecuteUpdate(SqlCommand command, Hashtable queryParameters)
        {
            if (queryParameters != null)
            {
                Object val;
                foreach (String key in queryParameters.Keys)
                {
                    SqlParameter param = new SqlParameter();
                    param.ParameterName = key;
                    val = queryParameters[key];
                    param.Value = val;
                    command.Parameters.Add(param);
                }

            }
            return command.ExecuteNonQuery();
        }

        public static String DoGetQuery(String StatementName, object dto)
        {
            return CSTDMapper.GetSQLStatement(StatementName, dto).PreparedStatement.PreparedSql;
        }

        public static String DoGetQueryReplaceParam(String StatementName, object dto)
        {
            PreparedStatement pStt = CSTDMapper.GetSQLStatement(StatementName, dto).PreparedStatement;
            String stSql = pStt.PreparedSql;
            int idxParam = 0;
            foreach (String paramName in pStt.DbParametersName)
            {
                stSql = stSql.Replace("@param" + idxParam, String.Format("'{0}'", dto.GetType().GetProperty(paramName).GetValue(dto, null).ToString()));
                idxParam++;
            }
            return stSql;
        }

        public static IList<Hashtable> ConvertDataTableToListHashTable(DataTable dtTable)
        {
            IList<Hashtable> lsTable = new List<Hashtable>();
            for (int i = 0; i < dtTable.Rows.Count;i++ )
            {

                Hashtable hshRow = new Hashtable();
                foreach (DataColumn column in dtTable.Columns)
                {
                    hshRow.Add(column.ColumnName, dtTable.Rows[i][column.ColumnName].ToString());
                }
                lsTable.Add( hshRow);
            }

            return lsTable;
        }

        public static void ExecuteBulkInsert(DataTable dataTable, SqlConnection connection, String tableName)
        {

            SqlBulkCopy bulkCopy =
                new SqlBulkCopy
                (
                connection,
                SqlBulkCopyOptions.TableLock |
                SqlBulkCopyOptions.FireTriggers |
                SqlBulkCopyOptions.UseInternalTransaction |
                SqlBulkCopyOptions.KeepIdentity,
                null
                );

            foreach (DataColumn col in dataTable.Columns)
            {
                bulkCopy.ColumnMappings.Add(new SqlBulkCopyColumnMapping(col.ColumnName, col.ColumnName));
            }

            // set the destination table name
            bulkCopy.DestinationTableName = tableName;

            // write the data in the "dataTable"
            try
            {
                bulkCopy.WriteToServer(dataTable);
                //sqlTran.Commit();
            }
            catch (SqlException ex)
            {
                if (ex.Message.Contains("Received an invalid column length from the bcp client for colid"))
                {
                    string pattern = @"\d+";
                    Match match = Regex.Match(ex.Message.ToString(), pattern);
                    var index = Convert.ToInt32(match.Value) - 1;

                    throw new Exception(ex.Message);
                }
                else
                {
                    throw ex;
                }
            }
        }

        public static string ToDatabase<T>(List<T> data, string conString, string tableName)
        {
            string result = "";
            using (var connS = new SqlConnection(conString))
            {
                connS.Open();
                var tranS = connS.BeginTransaction();

                try
                {
                    using (var bulk = new SqlBulkCopy(connS, SqlBulkCopyOptions.Default, tranS))
                    {
                        bulk.BulkCopyTimeout = 0;
                        bulk.BatchSize = 10000;
                        bulk.DestinationTableName = tableName;

                        DataTable table = new DataTable();
                        table = ToDataTable<T>(data);

                        PropertyDescriptorCollection properties =
                        TypeDescriptor.GetProperties(typeof(T));
                        foreach (PropertyDescriptor prop in properties)
                            bulk.ColumnMappings.Add(prop.Name, prop.Name);

                        bulk.WriteToServer(table);

                        tranS.Commit();
                    }

                    result = "SUC";
                }
                catch (Exception ex)
                {
                    Console.WriteLine(ex.Message);
                    tranS.Rollback();
                }
                finally
                {
                    tranS.Dispose();
                    connS.Close();

                }
            }

            return result;
        }

        public static DataTable ToDataTable<T>(List<T> data)
        {
            PropertyDescriptorCollection properties =
                TypeDescriptor.GetProperties(typeof(T));
            DataTable table = new DataTable();
            foreach (PropertyDescriptor prop in properties)
                table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            foreach (T item in data)
            {
                DataRow row = table.NewRow();
                foreach (PropertyDescriptor prop in properties)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
                table.Rows.Add(row);
            }
            return table;
        }

    }

}
