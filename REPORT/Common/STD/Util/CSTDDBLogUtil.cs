﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;
using Common.STD.Base;

namespace Common.STD.Util
{
    public static class CSTDDBLogUtil
    {

        public static readonly String FinishNoData = "3";
        public static readonly String FinishSuccessfully = "0";
        public static readonly String FinishBusinessError = "1";
        public static readonly String FinishSystemError = "2";
        public static readonly String InProgress = "4";
        public static readonly String Timeout = "5";


        public static String CreateProcessId()
        {
            /* Oracle */
            //DataTable dt = CSTDDBUtil.ExecuteQuery(con, "SELECT to_char(SYSDATE, 'YYMMDD') || lpad(seq_process_id.nextval, 14, '0') FROM dual");

            /* MS SQL Server */
            DataTable dt = CSTDDBUtil.ExecuteQuery("SELECT /*dbo.fn_CONCAT( FORMAT(CURRENT_TIMESTAMP,'yyyyMMdd') ,dbo.fn_LPAD('0',( NEXT VALUE FOR SEQ_PROCESS ),12) )*/  NEXT VALUE FOR SEQ_PROCESS ");
            return dt.Rows[0][0].ToString();
        }

        public static string CreateLogHeader(SqlConnection con, SqlTransaction sqlTrans, String processId, String functionId, String userId)
        {
            //processId = GetProcessId(con);
            /** Added Process ID which retrieve from NEXT VALUE of SEQUENCE dbo.SEQ_PROCESS **/
            //String insertQuery = "INSERT INTO TB_R_LOG_H(PROCESS_ID, START_DT,MODULE_ID, FUNCTION_ID, PROCESS_STATUS, USER_ID, READ_FLAG, REMARK)" +
            //    " VALUES(" + CSTDBaseDAO.LeadingSQLParameter + "processId, " +
            //    "CURRENT_TIMESTAMP, (SELECT module_id FROM TB_M_FUNCTION a WHERE a.FUNCTION_ID = '" + functionId + "'), " +                            
            //                CSTDBaseDAO.LeadingSQLParameter + "functionId, " +
            //                CSTDBaseDAO.LeadingSQLParameter + "processSts, " +
            //                CSTDBaseDAO.LeadingSQLParameter + "userId," +
            //                CSTDBaseDAO.LeadingSQLParameter + "readFlag," +
            //                CSTDBaseDAO.LeadingSQLParameter + "remark); ";
            String insertQuery = "INSERT INTO TB_R_LOG_H(PROCESS_ID, START_DT,MODULE_ID, FUNCTION_ID, PROCESS_STS, USER_ID, READ_FLAG, REMARK)" +
                " VALUES(" + CSTDBaseDAO.LeadingSQLParameter + "processId, " +
                "CURRENT_TIMESTAMP, (SELECT module_id FROM TB_M_FUNCTION a WHERE a.FUNCTION_ID = '" + functionId + "'), " +
                            CSTDBaseDAO.LeadingSQLParameter + "functionId, " +
                            CSTDBaseDAO.LeadingSQLParameter + "processSts, " +
                            CSTDBaseDAO.LeadingSQLParameter + "userId," +
                            CSTDBaseDAO.LeadingSQLParameter + "readFlag," +
                            CSTDBaseDAO.LeadingSQLParameter + "remark); ";

            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processId"] = processId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "functionId"] = functionId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processSts"] = InProgress;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "userId"] = userId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "readFlag"] = "N";
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "remark"] = "";

            if (sqlTrans == null)
            {
                sqlTrans = con.BeginTransaction();
            }
            
            CSTDDBUtil.ExecuteUpdate(con, sqlTrans, insertQuery, queryParams);            

            sqlTrans.Commit();

            return processId;
        }

        public static string CreateLogHeader(SqlConnection con, SqlTransaction sqlTrans, String functionId, String userId)
        {
            String processId = GetProcessId(con);
            /** Added Process ID which retrieve from NEXT VALUE of SEQUENCE dbo.SEQ_PROCESS **/
            String insertQuery = "INSERT INTO TB_R_LOG_H(PROCESS_ID, START_DT,MODULE_ID, FUNCTION_ID, PROCESS_STATUS, USER_ID, READ_FLAG, REMARK)" +
                " VALUES(" + CSTDBaseDAO.LeadingSQLParameter + "processId, " +
                "CURRENT_TIMESTAMP, (SELECT module_id FROM TB_M_FUNCTION a WHERE a.FUNCTION_ID = '" + functionId + "'), " +                            
                            CSTDBaseDAO.LeadingSQLParameter + "functionId, " +
                            CSTDBaseDAO.LeadingSQLParameter + "processSts, " +
                            CSTDBaseDAO.LeadingSQLParameter + "userId," +
                            CSTDBaseDAO.LeadingSQLParameter + "readFlag," +
                            CSTDBaseDAO.LeadingSQLParameter + "remark); ";

            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processId"] = processId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "functionId"] = functionId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processSts"] = InProgress;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "userId"] = userId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "readFlag"] = "N";
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "remark"] = "";

            if (sqlTrans == null)
            {
                sqlTrans = con.BeginTransaction();
            }
            
            CSTDDBUtil.ExecuteUpdate(con, sqlTrans, insertQuery, queryParams);            

            sqlTrans.Commit();

            return processId;
        }
        
        public static void CreateLogHeaders(SqlConnection con, SqlTransaction sqlTrans, String processId, String functionId1, String functionId2, String userId)
        {
            String insertQuery = "INSERT INTO TB_R_LOG_H(START_DT,MODULE_ID, FUNCTION_ID, PROCESS_STATUS, USER_ID, READ_FLAG, REMARK)" +
                " VALUES(CURRENT_TIMESTAMP, (SELECT module_id FROM TB_M_FUNCTION a WHERE a.FUNCTION_ID = '" + functionId1 + "'), " +
                            CSTDBaseDAO.LeadingSQLParameter + "functionId1, " +
                            CSTDBaseDAO.LeadingSQLParameter + "processSts, " +
                            CSTDBaseDAO.LeadingSQLParameter + "userId," +
                            CSTDBaseDAO.LeadingSQLParameter + "readFlag," +
                            CSTDBaseDAO.LeadingSQLParameter + "remark);" +
                                 " INSERT INTO TB_R_LOG_H(START_DT,MODULE_ID, FUNCTION_ID, PROCESS_STATUS, USER_ID, READ_FLAG, REMARK)" +
                " VALUES(CURRENT_TIMESTAMP, (SELECT module_id FROM TB_M_FUNCTION a WHERE a.FUNCTION_ID = '" + functionId2 + "'), " +
                            CSTDBaseDAO.LeadingSQLParameter + "functionId2, " +
                            CSTDBaseDAO.LeadingSQLParameter + "processSts, " +
                            CSTDBaseDAO.LeadingSQLParameter + "userId," +
                            CSTDBaseDAO.LeadingSQLParameter + "readFlag," +
                            CSTDBaseDAO.LeadingSQLParameter + "remark)";

            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "functionId1"] = functionId1;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "functionId2"] = functionId2;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processSts"] = InProgress;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "userId"] = userId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "readFlag"] = "N";
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "remark"] = "";

            if (sqlTrans == null)
            {
                sqlTrans = con.BeginTransaction();
            }
            CSTDDBUtil.ExecuteUpdate(con, sqlTrans, insertQuery, queryParams);
            sqlTrans.Commit();

        }

        public static void UpdateLogHeader(SqlConnection con, SqlTransaction sqlTrans, String processId, String processSts, String remarks)
        {
            String updateQuery = "UPDATE TB_R_LOG_H SET PROCESS_STS=" +
                CSTDBaseDAO.LeadingSQLParameter + "processSts, REMARK = " +
                CSTDBaseDAO.LeadingSQLParameter + "remarks, END_DT = CURRENT_TIMESTAMP WHERE PROCESS_ID = " +
                CSTDBaseDAO.LeadingSQLParameter + "processId";
            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processId"] = processId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processSts"] = processSts;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "remarks"] = remarks;

            if (sqlTrans == null)
            {
                sqlTrans = con.BeginTransaction();
            }
            CSTDDBUtil.ExecuteUpdate(con, sqlTrans, updateQuery, queryParams);
            sqlTrans.Commit();
        }

        public static void CreateLogDetail(SqlConnection con, String processId, String msgId, String[] messageParameters, String logLocation)
        {
            Boolean conStateOpen = false;

            if (con != null)
            {
                try
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                        conStateOpen = true;
                    }
                    CreateLogDetail(con, null, processId, msgId, messageParameters, logLocation);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (conStateOpen)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static void CreateLogDetail(SqlConnection con, String processId, String message, String logLocation)
        {
            if (con != null)
            {
                try
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                    }
                    CreateLogDetail(con, null, processId, message, logLocation);
                    con.Close();
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static void CreateLogDetail(SqlConnection con, SqlTransaction sqlTrans, String processId, String message, String logLocation)
        {
            CreateLogDetail(con, sqlTrans, processId, message.Split(':')[0].Trim(), message, logLocation);
        }
        public static void CreateLogDetail(SqlConnection con, SqlTransaction sqlTrans, String processId, String msgId, String[] messageParameters, String logLocation)
        {
            String message = CSTDMessageUtil.GetMessage(con, msgId, messageParameters);
            CreateLogDetail(con, sqlTrans, processId, msgId, message, logLocation);
        }


        public static void CreateLogDetail(SqlConnection con, SqlTransaction sqlTrans, String processId, String msgId, String message, String logLocation)
        {
            String msgType = msgId.Substring(msgId.Length - 1);

            //String insertQuery = "insert into tb_r_log_d(process_id, seq_no, MESSAGE_ID, MESSAGE_TYPE, ERROR_LOC, ERROR_MESSAGE, error_dt)" +
            //    " values(" + CSTDBaseDAO.LeadingSQLParameter + "processId, (select isnull(count(1),0)+1 from tb_r_log_d a where a.process_id = " +
            //    CSTDBaseDAO.LeadingSQLParameter + "processId), " +
            //    CSTDBaseDAO.LeadingSQLParameter + "msgId, " +
            //    CSTDBaseDAO.LeadingSQLParameter + "msgType, " +
            //    CSTDBaseDAO.LeadingSQLParameter + "logLocation, " +
            //    CSTDBaseDAO.LeadingSQLParameter + "message, CURRENT_TIMESTAMP)";
            String insertQuery = "insert into tb_r_log_d(process_id, seq_no, MSG_ID, MSG_TYPE, LOC, MSG_TEXT, err_dt)" +
                " values(" + CSTDBaseDAO.LeadingSQLParameter + "processId, (select isnull(count(1),0)+1 from tb_r_log_d a where a.process_id = " +
                CSTDBaseDAO.LeadingSQLParameter + "processId), " +
                CSTDBaseDAO.LeadingSQLParameter + "msgId, " +
                CSTDBaseDAO.LeadingSQLParameter + "msgType, " +
                CSTDBaseDAO.LeadingSQLParameter + "logLocation, " +
                CSTDBaseDAO.LeadingSQLParameter + "message, CURRENT_TIMESTAMP)";

            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "processId"] = processId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "msgId"] = msgId;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "msgType"] = msgType;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "logLocation"] = logLocation;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "message"] = (message == null) ? "" : message;
            if (sqlTrans == null)
            {
                sqlTrans = con.BeginTransaction();
            }
            CSTDDBUtil.ExecuteUpdate(con, sqlTrans, insertQuery, queryParams);
            sqlTrans.Commit();
        }

        public static String CheckLogHeaderStatus(String processId)
        {
            DataTable dt = CSTDDBUtil.ExecuteQuery("select process_status from tb_r_log_h a where a.process_id = " + CSTDBaseDAO.LeadingSQLParameter + "processId",
                new Hashtable() { { CSTDBaseDAO.LeadingSQLParameter + "processId", processId } });
            return dt.Rows[0][0].ToString();
        }


        public static String[] GetLogDetailLastLog(String processId)
        {
            DataTable dt = CSTDDBUtil.ExecuteQuery("select message from tb_r_log_d a where a.process_id = " + CSTDBaseDAO.LeadingSQLParameter + "processId order by seq_no ",
                new Hashtable() { { CSTDBaseDAO.LeadingSQLParameter + "processId", processId } });
            if (dt.Rows.Count > 0)
            {
                String[] errorMessage = new String[dt.Rows.Count];
                for (int i = 0; i < dt.Rows.Count; i++)
                {
                    errorMessage[i] = dt.Rows[i][0].ToString();
                }
                return errorMessage;
            }
            else
            {
                return null;
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We just log the exception and return an HTTP code")]
        public static String GetProcessStatus(String processId)
        {
            SqlConnection con = null;
            String processStatus = null;
            int i = 0;
            try
            {
                con = CSTDDBUtil.GetPooledConnection;
                con.Open();
                int intervalChecking = Int32.Parse(CSTDSystemMasterHelper.GetValue("COMMON", "BATCH_PROCESS", "INTERVAL_CHECKING", CSTDSystemMasterHelper.Mandatory).Trim(), CultureInfo.CurrentCulture);
                int timeOut = Int32.Parse(CSTDSystemMasterHelper.GetValue("COMMON", "BATCH_PROCESS", "TIMEOUT_CHECKING", CSTDSystemMasterHelper.Mandatory).Trim(), CultureInfo.CurrentCulture);
                do
                {
                    System.Threading.Thread.Sleep(intervalChecking * 1000);
                    processStatus = CSTDDBLogUtil.CheckLogHeaderStatus(processId);
                    i++;
                } while (InProgress.Equals(processStatus, StringComparison.OrdinalIgnoreCase) && i < timeOut);
                if (i < timeOut)
                {
                    return processStatus;
                }
                else
                {
                    return Timeout;
                }
            }
            catch (Exception e)
            {
                if (e.Message.IndexOf("MNFS00009E", StringComparison.OrdinalIgnoreCase) > -1)
                {
                    return e.Message;
                }
                else
                {
                    return CSTDMessageUtil.GetMessage("MNFS00009E", new String[] { e.Message });
                }
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        /**
         * 15-04-2014
         * Added By: FID) Praditha
         * Create Start Log, Get Process ID, Create End Log
         * **/
        public static String CreateStartLog(SqlConnection con, String batchName, String functionId, String userId, String logLocation)
        {
            String processId = "";
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }

                processId = CreateLogHeader(con, null, null, functionId, userId);                
                CreateLogDetail(con, processId, "MNFS00035I", new String[] { batchName }, logLocation);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }

            return processId;
        }

        public static void CreateStartLog(SqlConnection con, String processId, String batchName, String functionId, String userId, String logLocation)
        {
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }
                CreateLogHeader(con, null, processId, functionId, userId);
                //CreateLogDetail(con, processId, "MVPRSTD001I", new String[] { batchName }, logLocation);
            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }
        }

        public static void CreateStartLog(SqlConnection con, String processId, String batchName, String functionId, String userId, String logLocation, string messageId)
        {
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }
                CreateLogHeader(con, null, processId, functionId, userId);
                //CreateLogDetail(con, processId, "MVPRSTD001I", new String[] { batchName }, logLocation);

            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }
        }

        public static String CreateStartLogs(SqlConnection con, String batchName, String functionId1, String functionId2, String userId, String logLocation)
        {
            String processId = "";
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }

                CreateLogHeaders(con, null, null, functionId1, functionId2, userId);
                processId = GetProcessId(con);
                CreateLogDetail(con, processId, "MNFS00035I", new String[] { batchName }, logLocation);

            }
            catch (Exception e)
            {
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }

            return processId;
        }

        public static void CreateEndLog(SqlConnection con, String batchName, String processId, String processStatus, String remarks)
        {
            String msgId = "";
            Boolean conStateOpen = false;

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }

                switch (processStatus)
                {
                    case "0": msgId = "MCEDSTD008I";
                        break;
                    case "1": msgId = "MCEDSTD009I";
                        break;
                    case "2": msgId = "MCEDSTD009I";
                        break;
                    case "3": msgId = "MCEDSTD009I";
                        break;
                }

                if (processStatus != "0")
                {
                    CreateLogDetail(con, processId, msgId, new String[] { batchName }, "End Log");
                }
                
                UpdateLogHeader(con, null, processId, processStatus, remarks);
            }
            catch (Exception e)
            { }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }
        }

        public static void CreateEndLog(SqlConnection con, String batchName, String processId, String processStatus, String remarks, string messageId)
        {
            String msgId = "";
            Boolean conStateOpen = false;

            try
            {
                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }

                switch (processStatus)
                {
                    case "0":
                        msgId = "MCEDSTD008I";
                        break;
                    case "1":
                        msgId = "MCEDSTD009I";
                        break;
                    case "2":
                        msgId = "MCEDSTD009I";
                        break;
                        //case "3": msgId = "MNFS00038I";
                        //    break;
                }

                CreateLogDetail(con, processId, msgId, new String[] { batchName }, "End Process");
                UpdateLogHeader(con, null, processId, processStatus, remarks);
            }
            catch (Exception e)
            { }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }
        }

        private static string GetProcessId(SqlConnection con)
        {
            //Get Latest Sequence of Process ID on TB_R_LOG_H
            //DataTable dt = CSTDDBUtil.ExecuteQuery("select IDENT_CURRENT('TB_R_LOG_H')");

            /** Get Next Value of Sequence dbo.SEQ_PROCESS **/
            DataTable dt = CSTDDBUtil.ExecuteQuery("SELECT NEXT VALUE FOR dbo.SEQ_PROCESS");

            return dt.Rows[0][0].ToString();
        }

        public static void TraceLog(String traceLogValue)
        {
            SqlConnection con = CSTDDBUtil.GetNonPooledConnection;
            try
            {
                con.Open();
                SqlTransaction trc = con.BeginTransaction();

                String query = "INSERT INTO TB_T_TRACE_LOG VALUES('" + traceLogValue + "')";

                CSTDDBUtil.ExecuteUpdate(con, trc, query, null);

                trc.Commit();
            }
            catch (SqlException e)
            {
                throw e;
            }
            finally
            {
                if (con != null && con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }

        public static void CreateStartLog(out String processId, SqlConnection con, String messageId, String functionId, String userId, String logLocation)
        {
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }
                processId = CreateLogHeader(con, null, null, functionId, userId);
                CreateLogDetail(con, processId, messageId, new String[] { }, logLocation);

            }
            catch (Exception e)
            {
                processId = "";
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }
        }

        public static void CreateStartLog(out String processId, SqlConnection con, String messageId, String[] messageParam, String functionId, String userId, String logLocation)
        {
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }
                processId = CreateLogHeader(con, null, functionId, userId);
                CreateLogDetail(con, processId, messageId, messageParam, logLocation);

            }
            catch (Exception e)
            {
                processId = "";
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }
        }
        
        public static void CreateLogDetailCustom(SqlConnection con, String processId, String msgId, String[] messageParameters, String logLocation)
        {
            Boolean conStateOpen = false;

            if (con != null)
            {
                try
                {
                    if (con.State != ConnectionState.Open)
                    {
                        con.Open();
                        conStateOpen = true;
                    }
                    CreateLogDetailCustomMessage(con, null, processId, msgId, messageParameters, logLocation);
                }
                catch (Exception e)
                {
                }
                finally
                {
                    if (conStateOpen)
                    {
                        con.Close();
                    }
                }
            }
        }

        public static void CreateLogDetailCustomMessage(SqlConnection con, SqlTransaction sqlTrans, String processId, String msg, String[] messageParameters, String logLocation)
        {
            String message = CSTDMessageUtil.GetMessageCustom(msg, messageParameters);
            CreateLogDetail(con, sqlTrans, processId, "", message, logLocation);
        }

        public static String CreateStartLogCustomMessage(SqlConnection con, String batchName, String functionId, String userId, String logLocation, string customMessage)
        {
            String processId = "";
            Boolean conStateOpen = false;

            try
            {
                if (con == null)
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                }

                if (con.State != ConnectionState.Open)
                {
                    con.Open();
                    conStateOpen = true;
                }

                processId = CreateLogHeader(con, null, null, functionId, userId);
                CreateLogDetailCustom(con, processId, customMessage, new String[] { batchName }, logLocation);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
            finally
            {
                if (conStateOpen)
                {
                    con.Close();
                }
            }

            return processId;
        }        
    }
}
