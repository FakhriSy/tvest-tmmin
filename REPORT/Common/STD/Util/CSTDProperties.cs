﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Web;
namespace Common.STD.Util
{
    public class CSTDProperties
    {
        private Dictionary<String, String> _list;
        private String _filename;
        //private static bool _decrypt = false;
        public static string PropertiesPath { get; set; }
        public CSTDProperties(String file)
        {
            Reload(file);
        }

        public String Get(String field, String defValue)
        {
            return (Get(field) == null) ? (defValue) : (Get(field));
        }
        public String Get(String field)
        {
            return (_list.ContainsKey(field)) ? (_list[field]) : (null);
        }

        public void Set(String field, Object value)
        {
            if (!_list.ContainsKey(field))
                _list.Add(field, value.ToString());
            else
                _list[field] = value.ToString();
        }

        public void Reload()
        {
            Reload(this._filename);
        }

        public void Reload(String fileName)
        {
            this._filename = fileName;
            _list = LoadFromFile(fileName);
        }

        public static Dictionary<String, String> LoadFromFile(String file)
        {
            using (TextReader inputStream = new StreamReader(PropertiesPath + file))
            {
                return LoadProperties(inputStream);
            }
        }

        public static Dictionary<String, String> LoadProperties(TextReader inputStream)
        {
            Dictionary<String, String> props = new Dictionary<String, String>();
            String line;
            while ((line = inputStream.ReadLine()) != null)
            {
                if ((!String.IsNullOrEmpty(line)) &&
                    (!line.StartsWith(";", StringComparison.OrdinalIgnoreCase)) &&
                    (!line.StartsWith("#", StringComparison.OrdinalIgnoreCase)) &&
                    (!line.StartsWith("'", StringComparison.OrdinalIgnoreCase)) &&
                    (line.Contains('=')))
                {
                    int index = line.IndexOf('=');
                    String key = line.Substring(0, index).Trim();
                    String value = line.Substring(index + 1).Trim();

                    if ((value.StartsWith("\"", StringComparison.OrdinalIgnoreCase) && value.EndsWith("\"", StringComparison.OrdinalIgnoreCase)) ||
                        (value.StartsWith("'", StringComparison.OrdinalIgnoreCase) && value.EndsWith("'", StringComparison.OrdinalIgnoreCase)))
                    {
                        value = value.Substring(1, value.Length - 2);
                    }

                    try
                    {
                        //ignore duplicate
                        props.Add(key, value);

                    }
                    catch (ArgumentException)
                    {
                    }
                }
            }
            return props;
        }
    }
}
