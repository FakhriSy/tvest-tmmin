﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Common.STD.DAO;
using Common.STD.DTO;
using Common.STD.Base;

namespace Common.STD.Util
{
    public static class CSTDReportUtil
    {

        public static void RegisterToDownloadTable(SqlConnection con, SqlTransaction sqlTrans, String reportId, String processId, String requestBy, String fileName)
        {
            //String office_id = GetOfficeId(requestBy);
            String insertQuery = "INSERT INTO TB_R_REPORT(PROCESS_ID, REPORT_SEQ, FILE_NAME, CREATED_BY, CREATED_DT, FUNCTION_ID) " +
                " VALUES(" +
                            CSTDBaseDAO.LeadingSQLParameter + "processId, " +
                            CSTDBaseDAO.LeadingSQLParameter + "reportSeq, " +
                            CSTDBaseDAO.LeadingSQLParameter + "fileName, " +
                            CSTDBaseDAO.LeadingSQLParameter + "createdBy, CURRENT_TIMESTAMP, " +
                            CSTDBaseDAO.LeadingSQLParameter + "funcId) ";
            Hashtable insertParam = new Hashtable();
            insertParam[CSTDBaseDAO.LeadingSQLParameter + "processId"] = processId;
            insertParam[CSTDBaseDAO.LeadingSQLParameter + "reportSeq"] = 1;
            insertParam[CSTDBaseDAO.LeadingSQLParameter + "fileName"] = fileName;
            insertParam[CSTDBaseDAO.LeadingSQLParameter + "createdBy"] = requestBy;
            insertParam[CSTDBaseDAO.LeadingSQLParameter + "funcId"] = reportId;

            CSTDDBUtil.ExecuteUpdate(con, sqlTrans, insertQuery, insertParam);

            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD024I", new String[] { "<a href='Report/Download/" + fileName + "'><font color='blue'>" + fileName + "</font></a>" }, "File Results");
        }

        public static String GetFileFromDownloadTable(String processId)
        {
            String selectQuery = "SELECT A.FILE_NAME FROM TB_R_REPORT A WHERE A.PROCESS_ID = " +
                CSTDBaseDAO.LeadingSQLParameter + "processId ";
            DataTable dt = CSTDDBUtil.ExecuteQuery(selectQuery,
                new Hashtable() { { CSTDBaseDAO.LeadingSQLParameter + "processId", processId } });
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

        public static String GetOfficeId(String userId)
        {
            String selectQuery = "select Office_id from tb_m_user where user_id= " + CSTDBaseDAO.LeadingSQLParameter + "userId ";
            DataTable dt = CSTDDBUtil.ExecuteQuery(selectQuery, new Hashtable() { { CSTDBaseDAO.LeadingSQLParameter + "userId", userId } });
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            return null;
        }

    }

}
