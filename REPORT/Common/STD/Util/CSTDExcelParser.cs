﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;
using NPOI.XSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.HSSF.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.Util;


namespace Common.STD.Util
{

    /// <summary>
    /// Loads and reads a file with comma-separated values into a tabular format.
    /// </summary>
    /// <remarks>
    /// Parsing assumes that the first line will always contain headers and that values will be double-quoted to escape double quotes and commas.
    /// </remarks>
    public class CSTDExcelParser
    {
        private const char SEGMENT_DELIMITER = ',';
        private const char DOUBLE_QUOTE = '"';
        private const char CARRIAGE_RETURN = '\r';
        private const char NEW_LINE = '\n';

        private List<String[]> _table = new List<String[]>();
        String SheetName = "";
        String BreakRow = "";
        Nullable<int> SheetIdx = null;
        Nullable<int> StartRow = null;
        Nullable<int> EndRow = null;
        Nullable<int> StartColumn = null;
        Nullable<int> EndColumn = null;
        public String ErrorMessage = "";

        /// <summary>
        /// Gets the data contained by the instance in a tabular format.
        /// </summary>
        public List<String[]> Table
        {
            get
            {
                // validation logic could be added here to ensure that the object isn't in an invalid state

                return _table;
            }
        }

        public CSTDExcelParser(byte[] bytes,Boolean isXLSX)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException("bytes");
            }

            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            ms.Position = 0;
            if (isXLSX)
            {
                ReadX(ms);
            }
            else
            {
                Read(ms);
            }

        }
        public CSTDExcelParser(byte[] bytes, Boolean isXLSX,
            int SheetIdx,
            Nullable<int> StartRow,
            Nullable<int> EndRow,
            Nullable<int> StartColumn,
            Nullable<int> EndColumn)
        {

            //Console.WriteLine(" Parce Excel ");
            this.SheetIdx = SheetIdx;
            this.StartRow = StartRow;
            this.EndRow = EndRow;
            this.StartColumn = StartColumn;
            this.EndColumn = EndColumn;
            if (bytes == null)
            {
                throw new ArgumentNullException("bytes");
            }

            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            ms.Position = 0;
            if (isXLSX)
            {
                ReadX(ms);
            }
            else
            {
                Read(ms);
            }

        }

        public CSTDExcelParser(string Path, Boolean isXLSX,
            string SheetName,
            Nullable<int> StartRow,
            Nullable<int> EndRow,
            Nullable<int> StartColumn,
            Nullable<int> EndColumn)
        {
            if (Path == null)
            {
                throw new ArgumentNullException("path");
            }
            this.SheetName = SheetName;
            this.StartRow = StartRow;
            this.EndRow = EndRow;
            this.StartColumn = StartColumn;
            this.EndColumn = EndColumn;

            FileStream fs = new FileStream(Path, FileMode.Open);
            if (isXLSX)
            {
                ReadX(fs);
            }
            else
            {
                Read(fs);
            }
        }

        public CSTDExcelParser(Stream stream, Boolean isXLSX,
            int SheetIdx,
            Nullable<int> StartRow,
            String BreakRow,
            Nullable<int> StartColumn,
            Nullable<int> EndColumn)
        {

            //Console.WriteLine(" Parce Excel ");
            this.SheetIdx = SheetIdx;
            this.StartRow = StartRow;
            this.BreakRow = BreakRow;
            this.StartColumn = StartColumn;
            this.EndColumn = EndColumn;

            if (isXLSX)
            {
                ReadX(stream);
            }
            else
            {
                Read(stream);
            }
        }

        public CSTDExcelParser(Stream stream, Boolean isXLSX,
            int SheetIdx,
            Nullable<int> StartRow,
            Nullable<int> EndRow,
            Nullable<int> StartColumn,
            Nullable<int> EndColumn)
        {

            //Console.WriteLine(" Parce Excel ");
            this.SheetIdx = SheetIdx;
            this.StartRow = StartRow;
            this.EndRow = EndRow;
            this.StartColumn = StartColumn;
            this.EndColumn = EndColumn;

            if (isXLSX)
            {
                ReadX(stream);
            }
            else
            {
                Read(stream);
            }
        }

        public CSTDExcelParser(Stream stream, Boolean isXLSX,
            string SheetName,
            Nullable<int> StartRow,
            Nullable<int> EndRow,
            Nullable<int> StartColumn,
            Nullable<int> EndColumn)
        {

            //Console.WriteLine(" Parce Excel ");
            this.SheetName = SheetName;
            this.StartRow = StartRow;
            this.EndRow = EndRow;
            this.StartColumn = StartColumn;
            this.EndColumn = EndColumn;

            if (isXLSX)
            {
                ReadX(stream);
            }
            else
            {
                Read(stream);
            }
        }
        public CSTDExcelParser(string path, Boolean isXLSX)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            FileStream fs = new FileStream(path, FileMode.Open);
            if (isXLSX)
            {
                ReadX(fs);
            }
            else
            {
                Read(fs);
            }
        }

        public CSTDExcelParser(Stream stream, Boolean isXLSX)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            if (isXLSX)
            {
                ReadX(stream);
            }
            else
            {
                Read(stream);
            }
        }


        private void ReadX(Stream s)
        {
            XSSFWorkbook hssfworkbook;
            try
            {
                hssfworkbook = new XSSFWorkbook(s);
                ISheet sheet = SheetIdx == null ? hssfworkbook.GetSheet(SheetName) : hssfworkbook.GetSheetAt((int)SheetIdx);
                Read(sheet);
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                Console.WriteLine(
                    e.Message);
            }
        }

        private void Read(Stream s)
        {
            HSSFWorkbook hssfworkbook;
            try
            {
                hssfworkbook = new HSSFWorkbook(s);
                ISheet sheet = SheetIdx == null ? hssfworkbook.GetSheet(SheetName) : hssfworkbook.GetSheetAt((int)SheetIdx);
                if (sheet == null)
                {
                    ErrorMessage = " sheet : [" + (SheetIdx == null ? SheetName : ""+ SheetIdx) + "] not exists in excel file.";
                }
                else
                {
                    Read(sheet);
                }
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                Console.WriteLine(
                    e.Message);
            }
        }

        private void Read(ISheet sheet)
        {
            //HSSFWorkbook hssfworkbook;
            int i = 0;
            int j = 0;
            try
            {
                //Console.WriteLine(" Read Stream ");
                //hssfworkbook = new XSSFWorkbook(s);
                //hssfworkbook = new HSSFWorkbook(s);
                //Console.WriteLine(" Read sheet " + SheetName);

                // sheet = SheetIdx == null ? hssfworkbook.GetSheet(SheetName) : hssfworkbook.GetSheetAt((int)SheetIdx);
                int IndexReadRow = (int)(StartRow == null ? 0 : StartRow);
                Boolean ReadNext = true;
                while (ReadNext)
                //for (i = (int)(StartRow == null ? 0 : StartRow); i < (int)(EndRow == null ? sheet.LastRowNum : EndRow); i++)
                {
                    //Console.WriteLine(" Read Row : " + i );
                    //IRow row = sheet.GetRow(i);
                    if (IndexReadRow > sheet.LastRowNum)
                    {
                        break;
                    }
                    IRow row = sheet.GetRow(IndexReadRow);
                    if (!String.IsNullOrEmpty(BreakRow))
                    {
                        if (BreakRow.Equals(getCellValue(row.GetCell(0))))
                        {
                            ReadNext = false;
                        }
                    }
                    else
                    {
                        if (IndexReadRow == (int)(EndRow == null ? sheet.LastRowNum + 1 : EndRow))
                        {
                            ReadNext = false;
                        }
                    }
                    if (ReadNext)
                    {
                        List<String> parsed = new List<String>();
                        string Data = string.Empty;
                        for (j = (int)(StartColumn == null ? 0 : StartColumn); j < (int)(EndColumn == null ? row.LastCellNum : EndColumn); j++)
                        {
                            //Console.WriteLine(" Read cel : " + j);
                            if (row.GetCell(j) != null)
                            {
                                Data = getCellValue(row.GetCell(j));
                            }
                            else {
                                Data = String.Empty;
                            }
                            
                            parsed.Add(Data);
                        }
                        _table.Add(parsed.ToArray());
                    }
                    IndexReadRow++;
                    i = IndexReadRow;
                }
            }
            catch (Exception e)
            {
                ErrorMessage = e.Message;
                Console.WriteLine(
                    e.Message + "\n" +
                    "Row : " + i + "\n" +
                    "Col : " + j);
            }
        }

        private String getCellValue(ICell cell)
        {
            if (cell.CellType.Equals(CellType.BOOLEAN))
            {
                return cell.BooleanCellValue ? "true" : "false";
            }
            if (cell.CellType.Equals(CellType.STRING))
            {
                return cell.StringCellValue;
            }
            if (cell.CellType.Equals(CellType.NUMERIC))
            {
                if (HSSFDateUtil.IsCellDateFormatted(cell))
                {
                    return cell.DateCellValue.ToShortDateString();
                }
                else
                {
                    return cell.NumericCellValue.ToString();
                }
            }
            if (cell.CellType.Equals(CellType.FORMULA))
            {
                if (cell.CachedFormulaResultType.Equals(CellType.BOOLEAN))
                {
                    return cell.BooleanCellValue ? "true" : "false";
                }
                if (cell.CachedFormulaResultType.Equals(CellType.STRING))
                {
                    return cell.StringCellValue;
                }
                if (cell.CachedFormulaResultType.Equals(CellType.NUMERIC))
                {
                    return cell.NumericCellValue.ToString();
                }
                if (cell.CachedFormulaResultType.Equals(CellType.FORMULA))
                {
                    return cell.CachedFormulaResultType.ToString();
                }
                if (cell.CachedFormulaResultType.Equals(CellType.BLANK))
                {
                    return "";
                }
                if (cell.CachedFormulaResultType.Equals(CellType.ERROR))
                {
                    return "#ERROR#";
                }
                if (cell.CachedFormulaResultType.Equals(CellType.Unknown))
                {
                    return "#Unknown#";
                }
            }
            //    }
            if (cell.CellType.Equals(CellType.BLANK))
            {
                return "";
            }
            if (cell.CellType.Equals(CellType.ERROR))
            {
                return "#ERROR#";
            }
            if (cell.CellType.Equals(CellType.Unknown))
            {
                return "#Unknown#";
            }
            return "";
        }
    
    }
}
