﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Xml;
using System.IO;

namespace Common.STD.Util
{
    public class CSTDXMLConfig
    {
        private Dictionary<String, String> _list;
        private String _filename;
        //private static bool _decrypt = false;
        public CSTDXMLConfig(String file)
        {
            Reload(file);
        }

        public String Get(String field, String defValue)
        {
            return (Get(field) == null) ? (defValue) : (Get(field));
        }
        public String Get(String field)
        {
            return (_list.ContainsKey(field)) ? (_list[field]) : (null);
        }

        public void Set(String field, Object value)
        {
            if (!_list.ContainsKey(field))
                _list.Add(field, value.ToString());
            else
                _list[field] = value.ToString();
        }

        public void Reload()
        {
            Reload(this._filename);
        }

        public void Reload(String fileName)
        {
            this._filename = fileName;
            _list = LoadFromFile(fileName);
        }

        private static Dictionary<String, String> LoadFromFile(String file)
        {
            XmlTextReader reader = new XmlTextReader(CSTDProperties.PropertiesPath + file);
            return LoadXmlConfig(reader);
        }

        public static Dictionary<String, String> LoadXmlConfig(XmlReader reader)
        {
            Dictionary<String, String> props = new Dictionary<String, String>();
            while (reader.Read())
            {
                if (reader.NodeType == XmlNodeType.Element && reader.GetAttribute("key") != null)
                {
                    props.Add(reader.GetAttribute("key"), reader.GetAttribute("value"));
                }
            }
            reader.Close();
            return props;
        }
    }
}
