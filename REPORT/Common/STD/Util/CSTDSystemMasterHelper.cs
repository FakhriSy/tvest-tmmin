﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Collections;
using System.Data;
using System.Data.Sql;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;
using Common.STD.Base;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDSystemMasterHelper
    {
        public static String Mandatory = "1";
        public static String GetValue(string ID, string Type)
        {
            SqlConnection con = null;
            try
            {
                con = CSTDDBUtil.GetPooledConnection;
                con.Open();
                return GetValue(con, ID, Type);
            }
            catch (SqlException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (con != null && con.State == ConnectionState.Open)

                {
                    con.Close();
                }
            }
        }

        public static String GetValue(string cat, String subCat, String cd, String mandatory)
        {
            SqlConnection con = null;
            try
            {
                con = CSTDDBUtil.GetPooledConnection;
                con.Open();
                return GetValue(con, cat, subCat, cd);
            }
            catch (SqlException e)
            {
                throw e;
            }
            catch (Exception e)
            {
                throw e;
            }
            finally
            {
                if (con != null && con.State == ConnectionState.Open)
                {
                    con.Close();
                }
            }
        }
        public static String GetValue(SqlConnection con, string cat, String subCat, String cd)
        {
            String query = "SELECT SYS_VAL FROM TB_M_SYSTEM WHERE SYS_CAT=" +
                                CSTDBaseDAO.LeadingSQLParameter + "Cat AND SYS_SUB_CAT=" +
                                CSTDBaseDAO.LeadingSQLParameter + "SubCat AND SYS_CD=" +
                                CSTDBaseDAO.LeadingSQLParameter + "Cd";
            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "Cat"] = cat;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "SubCat"] = subCat;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "Cd"] = cd;
            DataTable dt = CSTDDBUtil.ExecuteQuery(con, query, queryParams);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0]["SYS_VAL"].ToString();
            }
            else
            {
                return "";
            }
        }

        public static String GetValue(SqlConnection con, string ID, String Type)
        {
            String query = "SELECT SYS_VAL FROM TB_M_SYSTEM WHERE SYS_CAT =" +
                                CSTDBaseDAO.LeadingSQLParameter + "ID AND SYS_CD =" +
                                CSTDBaseDAO.LeadingSQLParameter + "Type";
            Hashtable queryParams = new Hashtable();
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "ID"] = ID;
            queryParams[CSTDBaseDAO.LeadingSQLParameter + "Type"] = Type;
            DataTable dt = CSTDDBUtil.ExecuteQuery(con, query, queryParams);
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0]["SYS_VAL"].ToString();
            }
            else
            {
                return "";
            }
        }



        //public static String GetCode(string cat, String subCat, String value)
        //{
        //    String query = "SELECT SYS_CD FROM TB_M_SYSTEM WHERE STATUS='1' AND SYS_CAT=" + 
        //                        CSTDBaseDAO.LeadingSQLParameter + "Cat AND SYS_SUB_CAT=" + 
        //                        CSTDBaseDAO.LeadingSQLParameter + "SubCat AND SYS_VAL=" + 
        //                        CSTDBaseDAO.LeadingSQLParameter + "Val";
        //    Hashtable queryParams = new Hashtable();
        //    queryParams[CSTDBaseDAO.LeadingSQLParameter + "Cat"] = cat;
        //    queryParams[CSTDBaseDAO.LeadingSQLParameter + "SubCat"] = subCat;
        //    queryParams[CSTDBaseDAO.LeadingSQLParameter + "Val"] = value;
        //    try
        //    {
        //        DataTable dt = CSTDDBUtil.ExecuteQuery(query, queryParams);
        //        if (dt != null && dt.Rows.Count > 0)
        //        {
        //            return dt.Rows[0]["SYS_CD"].ToString();
        //        }
        //        else
        //        {
        //            return "";
        //        }

        //    }
        //    catch (SqlException)
        //    {
        //        return "";
        //    }
        //}
    }
}
