﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Data;
using System.Data.SqlClient;
using log4net;
using System.Diagnostics.CodeAnalysis;
namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDSourcePopulator
    {
        static ILog logger { get { return LogManager.GetLogger(typeof(Common.STD.Util.CSTDErrorUtil)); } }
        //private static CSTDProperties _properties = new CSTDProperties("Populator.properties");
        public static DataTable GetDataSource(String dataSourceName, String dataSourceParameter)
        {
            DataTable _dt = null;
            try
            {
                _dt = CSTDDBUtil.ExecuteQuery(" SELECT QUERY,FIXED FROM TB_M_OPTION WHERE COMBO_NAME='" + dataSourceName + "' ");
                if (_dt.Rows.Count > 0)
                {
                    DataRow dr = _dt.Rows[0];
                    String queryOption = dr["QUERY"].ToString();
                    String fixedOption = dr["FIXED"].ToString();
                    if (!String.IsNullOrEmpty(queryOption))
                    {
                        if (!String.IsNullOrEmpty(dataSourceParameter))
                        {
                            String[] queryParam = dataSourceParameter.Split(';');

                            for (int i = 0; i < queryParam.Length; i++)
                            {
                                queryOption = queryOption.Replace("{" + i.ToString() + "}", queryParam[i].Trim());
                            }
                        }

                        return CSTDDBUtil.ExecuteQuery(queryOption);
                    }
                    else
                        if (!String.IsNullOrEmpty(fixedOption))
                        {
                            _dt = new DataTable();
                            _dt.Locale = System.Globalization.CultureInfo.CurrentCulture;
                            
                            DataColumn d = new DataColumn("OPTION_VALUE", typeof(string));
                            d.MaxLength = 100;
                            _dt.Columns.Add(d);

                            d = new DataColumn("OPTION_LABEL", typeof(string));
                            d.MaxLength = 100;
                            _dt.Columns.Add(d);

                            String[] options = fixedOption.Split(';');
                            foreach (String option in options)
                            {
                                String[] _options = option.Split('=');
                                _dt.Rows.Add();
                                _dt.Rows[_dt.Rows.Count - 1]["OPTION_VALUE"] = _options[0];
                                if (_options.Length > 1)
                                {
                                    _dt.Rows[_dt.Rows.Count - 1]["OPTION_LABEL"] = _options[1];
                                }
                            }
                            return _dt;
                        }
                }
            }
            catch (SqlException e)
            {
                logger.Error(e.Message);
                throw;
            }
            return _dt;
        }
    }
}
