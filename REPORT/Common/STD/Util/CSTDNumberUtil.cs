﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDNumberUtil
    {
        public static int GetNumber(String value)
        {
            return Convert.ToInt32(value, System.Globalization.CultureInfo.CurrentCulture);
        }
    }
}
