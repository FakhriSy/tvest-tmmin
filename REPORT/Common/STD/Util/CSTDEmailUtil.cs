﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Net.Mail;
using Common.STD.DTO;
using System.Diagnostics.CodeAnalysis;
using System.Net.Mime;
using System.Net;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDEmailUtil
    {
        //private static CSTDSystemConfig _systemConfig = new CSTDSystemConfig();

        public static void SendEmail(CSTDEmailDTO emailDTO)
        {
            try
            {
                String[] emailTo = emailDTO.To.Split(';');

                /** Added by FID)Praditha, 21-05-2014
                 *  Purpose: Remove any empty values
                 * **/
                emailTo = emailTo.Where(val => !String.IsNullOrEmpty(val)).ToArray();

                String[] emailCc = null;
                if (!String.IsNullOrEmpty(emailDTO.CC))
                {
                    emailCc = emailDTO.CC.Split(';');
                }

                var xmlConfig = new CSTDXMLConfig(@"SystemConfig.xml");

                using (SmtpClient client = new SmtpClient(xmlConfig.Get("smtp_server"))) //smtp_server ("smtp.gmail.com", 587)
                {
                    client.Port = CSTDNumberUtil.GetNumber(xmlConfig.Get("smtp_port"));
                    ServicePointManager.ServerCertificateValidationCallback += (o, c, ch, er) => true;
                    client.EnableSsl = false;

                    MailMessage message = new MailMessage();
                    message.IsBodyHtml = true;

                    if (string.IsNullOrEmpty(emailDTO.From))
                        message.From = new MailAddress(CSTDSystemMasterHelper.GetValue("EMAIL_SETTING", "SMTP_MAIL_FROM"));
                    else
                        message.From = new MailAddress(emailDTO.From);

                    foreach (string to in emailTo)
                    {
                        message.To.Add(new MailAddress(to));
                    }

                    if (emailCc != null)
                    {
                        foreach (string cc in emailCc)
                        {
                            message.CC.Add(new MailAddress(cc));
                        }
                    }
                    message.Subject = emailDTO.Subject;
                    message.Body = emailDTO.Content;
                    if (emailDTO.AttachmentFile != null)
                    {
                        foreach (string filePath in emailDTO.AttachmentFile)
                        {
                            Attachment data = new Attachment(filePath);
                            message.Attachments.Add(data);
                        }
                    }

                    if (xmlConfig.Get("smtp_mail_external") == "1")
                    {
                        client.EnableSsl = true;
                        client.UseDefaultCredentials = false;
                        NetworkCredential NetworkCred = new NetworkCredential(xmlConfig.Get("smtp_mail_from"), xmlConfig.Get("smtp_pass"));
                        client.DeliveryMethod = SmtpDeliveryMethod.Network;
                        client.Credentials = NetworkCred;
                    }

                    client.Send(message);
                    message.Dispose();//need dispose,if not, will be error in used when moved
                }
            }
            catch (Exception ex)
            {
                Console.WriteLine(ex.Message);
            }

        }
    }
}
