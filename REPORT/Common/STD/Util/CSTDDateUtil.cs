﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Data.SqlClient;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDDateUtil
    {
        public CSTDDateUtil()
        {
        }
        public static String DefaultDateFormat { get { return "{0:yyyy/MM/dd}"; } }
        public static String GetCurrentDate(String format)
        {
            return String.Format(CultureInfo.CurrentCulture,format, DateTime.Now);
        }

        public static DateTime GetCurrentDBDate()
        {
            return CSTDMapper.Instance().QueryForObject<DateTime>("GetCurrentDateTime", null);
        }

        public static String GetCurrentDBDate(String format)
        {
            String stQuery = (" SELECT TO_CHAR(SYSDATE,'" + format + "') FROM DUAL ");

            Hashtable queryParams = new Hashtable();
            DataTable dt = null;
            try
            {
                dt = CSTDDBUtil.ExecuteQuery(stQuery.ToString(), queryParams);
            }
            catch (SqlException)
            {
            } 
            if (dt != null && dt.Rows.Count > 0)
            {
                return dt.Rows[0][0].ToString();
            }
            else
            {
                return null;
            }
        }
    }
}
