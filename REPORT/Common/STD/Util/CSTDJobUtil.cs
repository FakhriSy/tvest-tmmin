﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.STD.DTO;
using Common.STD.DAO;
using System.IO;
using System.Diagnostics;
using log4net;

namespace Common.STD.Util
{
    public class CSTDJobUtil
    {
        static ILog logger = LogManager.GetLogger(typeof(CSTDJobUtil));
        public static String CreateJobParam(String tempFolder, CSTDBatchDTO batchInfo)
        {
            String fileName = batchInfo.RequestBy + "_" + CSTDDateUtil.GetCurrentDBDate().ToString("ddMMyyyyHHmmss") + ".def";
            using (TextWriter tw = new StreamWriter(tempFolder + fileName, false))
            {
                tw.WriteLine("$parm");
                tw.WriteLine("odb_qid \"" + batchInfo.QueueNo + "\" ");
                tw.WriteLine("odb_rid \"" + batchInfo.RequestId + "\" ");
                tw.WriteLine("odb_bid \"" + batchInfo.BatchId + "\" ");
                tw.WriteLine("odb_rby \"" + batchInfo.RequestBy + "\" ");
                tw.WriteLine("odb_rbd \"" + batchInfo.RequestDate + "\" ");
                tw.WriteLine("odb_shl \"" + batchInfo.Shell + "\"");
                tw.WriteLine("odb_pcd \"" + batchInfo.ProjectCode + "\"");

                tw.WriteLine("odb_pr \"" + batchInfo.Parameter + "\" ");
                tw.WriteLine("odb_run \"" + batchInfo.RunAs + "\" ");
                tw.WriteLine("odb_nam \"" + batchInfo.BatchName + "\" ");
            }
            return fileName;
        }

        public static String RunProcess(String shell, String parameter)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = shell;
            start.Arguments = parameter;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.RedirectStandardError = true;
            start.CreateNoWindow = true;
            start.WindowStyle = ProcessWindowStyle.Hidden;

            //
            // Start the process.
            //
            String line, retValue = null;
            try
            {
                using (Process process = Process.Start(start))
                {
                    //
                    // Read in all the text from the process with the StreamReader.
                    //
                    using (StreamReader reader = process.StandardOutput)
                    {
                        while ((line = reader.ReadLine()) != null)
                        {
                            retValue = line;
                            logger.Info(line);

                        }
                    }
                }
            }
            catch (IOException e)
            {
                logger.Error(e.Message + " [ " + shell + " ]");
            }
            catch (Exception e)
            {
                logger.Error(e.Message + " [ "+shell+" ]");
            }
            return retValue;
        }

        public static String RunProcessWithReturn(String shell, String parameter)
        {
            ProcessStartInfo start = new ProcessStartInfo();
            start.FileName = shell;
            start.Arguments = parameter;
            start.UseShellExecute = false;
            start.RedirectStandardOutput = true;
            start.CreateNoWindow = true;
            start.WindowStyle = ProcessWindowStyle.Hidden;
            String retValue = null;
            using (Process process = Process.Start(start))
            {
                using (StreamReader reader = process.StandardOutput)
                {
                    String line;
                    while ((line = reader.ReadLine()) != null)
                    {
                        retValue = line;
                    }
                }
            }
            return retValue;
        }
    }
}
