﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Component Export Digitalization and Automation System
 * Client Name      : TMMIN
 * Program Id       : COMMON
 * Program Name     : Validation common
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID) Yusuf
 * Version          : 01.00.00
 * Creation Date    : 01/03/2018 16:00:00
 * 
 * Update history     Re-fix date       Person in charge      Description 
 *
 * Copyright(C) 2013 - . All Rights Reserved                                                                                              
 *************************************************************************************************/
using Common.STD.Base;
using Common.STD.Validators;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.STD.Util
{
    public class CSTDValidatorUtil
    {
        public void Validate(CSTDBaseDTO inputParam)
        {
            string messageResult = "";
            var context = new ValidationContext(inputParam, null, null);
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(inputParam, context, result, true))
            {
                messageResult = result[0].ErrorMessage;
                messageResult = messageResult.Replace("The field ", "");
                throw new Exception(messageResult);
            }
        }

        public static string[] ValidateMandatory(CSTDBaseDTO inputParam)
        {
            IList<string> messageResult = new List<string>();
            var context = new ValidationContext(inputParam, null, null);
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(inputParam, context, result, true))
            {
                foreach (var item in result)
                {
                    string mandatoryError = item.ErrorMessage;
                    if (mandatoryError.Contains(CSTDMandatory.MADATORY))
                    {
                        messageResult.Add(CSTDMandatory.GetMandatoryField(mandatoryError));
                    }
                }
            }
            return messageResult.ToArray();
        }

        public static string[] ValidateFormat<T>(T inputParam)
        {
            IList<string> messageResult = new List<string>();
            var context = new ValidationContext(inputParam, null, null);
            var result = new List<ValidationResult>();
            if (!Validator.TryValidateObject(inputParam, context, result, true))
            {
                foreach (var item in result)
                {
                    string mandatoryError = item.ErrorMessage;
                    if (mandatoryError.Contains(CSTDFormat.FORMAT))
                    {
                        messageResult.Add(mandatoryError);
                    }
                }
            }
            return messageResult.ToArray();
        }
    }
}
