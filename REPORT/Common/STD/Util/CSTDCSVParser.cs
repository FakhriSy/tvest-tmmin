﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Text;


namespace Common.STD.Util
{
        
    /// <summary>
    /// Loads and reads a file with comma-separated values into a tabular format.
    /// </summary>
    /// <remarks>
    /// Parsing assumes that the first line will always contain headers and that values will be double-quoted to escape double quotes and commas.
    /// </remarks>
    public class CSTDCSVParser
    {
        private const char SEGMENT_DELIMITER = ',';
        private const char DOUBLE_QUOTE = '"';
        private const char CARRIAGE_RETURN = '\r';
        private const char NEW_LINE = '\n';

        private List<String[]> _table = new List<String[]>();

        /// <summary>
        /// Gets the data contained by the instance in a tabular format.
        /// </summary>
        public List<String[]> Table
        {
            get
            {
                // validation logic could be added here to ensure that the object isn't in an invalid state

                return _table;
            }
        }

        public CSTDCSVParser(string path)
        {
            if (path == null)
            {
                throw new ArgumentNullException("path");
            }

            FileStream fs = new FileStream(path, FileMode.Open);
            Read(fs);
        }

        public CSTDCSVParser(Stream stream)
        {
            if (stream == null)
            {
                throw new ArgumentNullException("stream");
            }

            Read(stream);
        }

        public CSTDCSVParser(byte[] bytes)
        {
            if (bytes == null)
            {
                throw new ArgumentNullException("bytes");
            }

            MemoryStream ms = new MemoryStream();
            ms.Write(bytes, 0, bytes.Length);
            ms.Position = 0;

            Read(ms);
        }

        private void Read(Stream s)
        {
            string lines;
            int entriesFound = 0;
            using (var textReader = new StreamReader(s))
            {
                lines = textReader.ReadLine();
                while (lines != null)
                {
                    String[] columns = CsvParser(lines, SEGMENT_DELIMITER);
                    _table.Add(columns);
                    entriesFound++;
                    lines = textReader.ReadLine();
                }
            }
        }

        private String[] CsvParser(String csv, Char separator)
        {
            List<String> parsed = new List<String>();
            string[] temp = csv.Split(separator);
            int counter = 0;
            string Data = string.Empty;
            while (counter < temp.Length)
            {
                Data = temp[counter].Trim();
                if (Data.Trim().StartsWith("\""))
                {
                    bool isLast = false;
                    while (!isLast && counter < temp.Length)
                    {
                        Data += separator.ToString() + temp[counter + 1];
                        counter++;
                        isLast = (temp[counter].Trim().EndsWith("\""));
                    }
                }
                parsed.Add(Data);
                counter++;
            }
            return parsed.ToArray();
        }


    }
}
