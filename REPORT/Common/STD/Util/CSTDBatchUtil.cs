﻿using System;
using System.Collections.Generic;
using System.Collections;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using Common.STD.DAO;
using Common.STD.DTO;
using Common.STD.Base;

namespace Common.STD.Util
{
    public static class CSTDBatchUtil
    {
        public static void PutIntoBatchQueue(String processId, String batchId, String batchName, String requestId, String requestBy, String[] businessParameters)
        {
            String projectCode = CSTDSystemConfig.GetValue("project_cd");
            SqlConnection con = null, batchCon = null;
            SqlTransaction trans = null, batchTrans = null;
            try
            {
                con = CSTDDBUtil.GetPooledConnection;

                String insertParamQuery = "INSERT INTO TB_T_PARAMETER(PROCESS_ID, PARAM_SEQ, VALUE_PARAM) VALUES(" +
                                            CSTDBaseDAO.LeadingSQLParameter + "processId, " +
                                            CSTDBaseDAO.LeadingSQLParameter + "paramSeq, " +
                                            CSTDBaseDAO.LeadingSQLParameter + "paramValue)";
                int paramSeq = 1;
                //while (buffer.Length > 0)
                foreach (String param in businessParameters)
                {
                    Hashtable insertParams = new Hashtable();
                    insertParams[CSTDBaseDAO.LeadingSQLParameter + "processId"] = processId;
                    insertParams[CSTDBaseDAO.LeadingSQLParameter + "paramSeq"] = "" + paramSeq;
                    paramSeq++;

                    insertParams[CSTDBaseDAO.LeadingSQLParameter + "paramValue"] = param;
                    con.Open();
                    using (SqlCommand command = new SqlCommand(insertParamQuery, con))
                    {

                        trans = con.BeginTransaction();
                        command.Transaction = trans;
                        CSTDDBUtil.ExecuteUpdate(command, insertParams);
                        trans.Commit();
                    }
                    con.Close();

                }
                try
                {
                    //insert into tb_r_batch_queue

                    batchCon = CSTDDBUtil.GetBatchConnection;
                    batchCon.Open();
                    batchTrans = batchCon.BeginTransaction();

                    String insertQueueQuery = "INSERT INTO TB_R_BATCH_QUEUE(QUEUE_NO, PROJECT_CODE, BATCH_ID, REQUEST_ID, REQUEST_DATE, REQUEST_BY, PARAMETER) " +
                        "VALUES (NEXT VALUE FOR SEQ_QUEUE, " + CSTDBaseDAO.LeadingSQLParameter + "projectCode, " + CSTDBaseDAO.LeadingSQLParameter + "batchId, " + CSTDBaseDAO.LeadingSQLParameter + "requestId, CURRENT_TIMESTAMP, " + CSTDBaseDAO.LeadingSQLParameter + "requestBy, " + CSTDBaseDAO.LeadingSQLParameter + "parameter) ";
                    Hashtable insertQueueParams = new Hashtable();
                    insertQueueParams[CSTDBaseDAO.LeadingSQLParameter + "projectCode"] = projectCode;
                    insertQueueParams[CSTDBaseDAO.LeadingSQLParameter + "batchId"] = batchId;
                    insertQueueParams[CSTDBaseDAO.LeadingSQLParameter + "requestId"] = requestId;
                    insertQueueParams[CSTDBaseDAO.LeadingSQLParameter + "requestBy"] = requestBy;
                    insertQueueParams[CSTDBaseDAO.LeadingSQLParameter + "parameter"] = batchId + "#:#" + requestBy + "#:#" + processId;

                    using (SqlCommand batchCommand = new SqlCommand(insertQueueQuery, batchCon))
                    {
                        batchCommand.Transaction = batchTrans;
                        CSTDDBUtil.ExecuteUpdate(batchCommand, insertQueueParams);
                        batchTrans.Commit();
                    }
                }
                catch (Exception e)
                {
                    if (batchTrans != null)
                    {
                        batchTrans.Rollback();
                    }
                    if (processId != null)
                    {
                        CSTDDBLogUtil.UpdateLogHeader(con, trans, processId, CSTDDBLogUtil.FinishSystemError, "Error : " + e.Message);
                    }
                }
                finally
                {
                    if (batchCon != null ) batchCon.Close();
                }

                //trans.Commit();
            }
            catch (Exception e)
            {
                if (trans != null)
                {
                    trans.Rollback();
                }
                if (processId != null)
                {
                    CSTDDBLogUtil.UpdateLogHeader(con, trans, processId, CSTDDBLogUtil.FinishSystemError, "Error : " + e.Message);
                }
                throw;
            }
            finally
            {
                if (con != null) con.Close();
            }
        }
    }

}
