﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using IBatisNet.Common.Utilities;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.Configuration;
using System.Reflection;
using System.Diagnostics.CodeAnalysis;
using IBatisNet.Common;
using System.Data;
using IBatisNet.Common.Exceptions;
using System.Xml.Serialization;
using IBatisNet.DataMapper;
using IBatisNet.DataMapper.MappedStatements;
using IBatisNet.DataMapper.Scope;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDMapper
    {
        public static String SqlMapConfigLocation { get; set; }
        public static String SqlBatchMapConfigLocation { get; set; }
        private static volatile ISqlMapper _mapper = null;
        private static volatile ISqlMapper _BatchMapper = null;
        protected static void Configure(object value)
        {
            _mapper = null;
        }
        protected static void InitMapper()
        {
            ConfigureHandler handler = new ConfigureHandler(Configure);
            DomSqlMapBuilder builder = new DomSqlMapBuilder();
            _mapper = builder.ConfigureAndWatch(SqlMapConfigLocation, handler);

            LongQueries lq = new LongQueries();
            lq.setDbProvider(_mapper.DataSource.DbProvider);
            _mapper.DataSource.DbProvider = lq;

        }

        protected static void InitBatchMapper()
        {
            ConfigureHandler handler = new ConfigureHandler(Configure);
            DomSqlMapBuilder builder = new DomSqlMapBuilder();
            _BatchMapper = builder.ConfigureAndWatch(SqlBatchMapConfigLocation, handler);
        }

        public static ISqlMapper Instance()
        {
            if (_mapper == null)
            {
                lock (typeof(SqlMapper))
                {
                    if (_mapper == null) // double-check
                    {
                        InitMapper();
                    }
                }
            }
            return _mapper;
        }

        public static ISqlMapper BatchInstance()
        {
            if (_BatchMapper == null)
            {
                lock (typeof(SqlMapper))
                {
                    if (_BatchMapper == null) // double-check
                    {
                        InitBatchMapper();
                    }
                }
            }
            return _BatchMapper;
        }

        
        public static ISqlMapper Get()
        {
            return Instance();
        }

        public static RequestScope GetSQLStatement(String MappId, Object param)
        {
            Boolean IsNotOpen = false;
            if (!_mapper.IsSessionStarted)
            {
                IsNotOpen = true;
                _mapper.OpenConnection();
            }
            IMappedStatement statement = CSTDMapper.Instance().GetMappedStatement(MappId);
            RequestScope scope = statement.Statement.Sql.GetRequestScope(statement, param, _mapper.LocalSession);
            if (IsNotOpen)
            {
                _mapper.CloseConnection();
            }
            return scope;
        }

    }

    


    public class LongQueries : LongQueriesDecorator
    {

        public LongQueries()
        {
        }
        public override void setCommandTimeout(IDbCommand cmd)
        {
            //cmd.CommandTimeout = 1000;  // here you can configure a value in the App.config
            cmd.CommandTimeout = 0; // unlimited
            cmd.Prepare();
            
        }
    }

    public abstract class LongQueriesDecorator : C_DbProvider
    {
        

        public abstract void setCommandTimeout(IDbCommand cmd);

        // implement all IDbProvider methods calling _iDbProvider.METHOD
        // ...
        // except for

        public override IDbCommand CreateCommand()
        {
            IDbCommand cmd = GetCreateCommand();
            // here you can call the delegate
            setCommandTimeout(cmd);
            return cmd;
        }
        // ...
    }
}
