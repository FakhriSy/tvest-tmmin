﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Collections;
using System.Data.SqlClient;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDMessageUtil
    {

        [SuppressMessage("Microsoft.Usage", "CA2211:NonConstantFieldsShouldNotBeVisible")]
        public static Object CommonServerMessage;

        public static String SuccessRetrieve { get { return "MNFS00021I"; } }
        public static String NoDataFound { get { return "MNFS00014E"; } }
        public static String RecordDeleted { get { return "MNFS00070E"; } }
        public static String RecordUpdated { get { return "MNFS00019E"; } }
        public static String ProcessSuccess { get { return "MNFS00036I"; } }
        public static String ProcessError { get { return "MNFS00037I"; } }
        public static String SaveSuccess { get { return "MNFS00016I"; } }
        public static String DeleteSuccess { get { return "MNFS00020I"; } }
        public static String ConfirmProcessSuccess { get { return "MNFS00047I"; } }
        public static String ApprovedProcessSuccess { get { return "MNFS00047I"; } }


        private static DataTable MessagesDataTable ;

        public static void Init()
        {
            CommonServerMessage = new Dictionary<String, String>();
            #region Load Common Client Message
            //load common client message
            String[] _commonClientMsg = new String[]{
                "MNFS00003E",   //[0] should not be empty
                "MNFS00023C",   //Are you sure want to confirm the record?
                "MNFS00029C",   //Are you sure want to print the record?
                "MNFS00017C",   //Are you sure you want to abort the operation?
                "MNFS00015C",   //Are you sure you want to {0} the records?
                "MNFS00042E",   //Invalid data type [0]. The value must be [1].
                "MNFS00004E",   //Invalid date format for [0]. The date format must be [1].
                "MNFS00045E",   //{0} must be greater than {1}
                "MNFS00022E",  //A single record must be selected to view its details.
                "MNFS00018E",  //A single record must be selected to execute {0} operation.
                "MNFS00079E",   //Data should be confirmed before
                "MNFS00080C",   //Are you sure you want to {0} ?
                "MNFS00047I"    //Data has been confirmed successfully
            };

            StringBuilder query = new StringBuilder();
            query.Append("SELECT MSG_ID, MSG_TEXT FROM TB_M_MESSAGE WHERE MSG_ID IN (")
                .Append("'" + _commonClientMsg[0] + "'");
            for (int i = 1; i < _commonClientMsg.Length; i++)
            {
                query.Append(",'")
                    .Append(_commonClientMsg[i])
                    .Append("'");
            }
            query.Append(")");
            try
            {
                MessagesDataTable = CSTDDBUtil.ExecuteQuery(query.ToString());
            }
            catch (SqlException)
            {
                MessagesDataTable = new DataTable();
                MessagesDataTable.Locale = System.Globalization.CultureInfo.CurrentCulture;
                MessagesDataTable.Columns.Add("MSG_ID");
                MessagesDataTable.Columns.Add("MSG_TEXT");
                foreach (String msg in _commonClientMsg)
                {
                    MessagesDataTable.Rows.Add(msg, "");
                }
            }

            #endregion

            #region Load Common Server Message
            //load common server message
            String[] _commonServerMsg = new String[] {
                "MNFS00021I",  //Data successfully retrieved
                "MNFS00014E",  //No data found
                "MNFS00070E",  //[0] has been deleted by another user
                "MNFS00019E",  //[0] has been updated by another user
                "MNFS00036I",  //[0] ends with success [1]
                "MNFS00037I",  //[0] ends with error
                "MNFS00016I",  //Saving data is completed successfully.
                "MNFS00020I",  //Data has been deleted successfully.
                "MNFS00047I",  //Data has been successfully confirmed
                "MNFS00048I"  //Data has been successfully approved
            };

            query.Clear();
            query.Append("SELECT MSG_ID, MSG_ID + ': ' + MSG_TEXT AS MSG_TEXT FROM TB_M_MESSAGE WHERE MSG_ID IN (")
                .Append("'" + _commonServerMsg[0] + "'");
            for (int i = 1; i < _commonServerMsg.Length; i++)
            {
                query.Append(",'")
                    .Append(_commonServerMsg[i])
                    .Append("'");
            }
            query.Append(")");
            try
            {
                DataTable tempDt = CSTDDBUtil.ExecuteQuery(query.ToString());
                if (tempDt.Rows.Count > 0)
                {
                    for (int i = 0; i < tempDt.Rows.Count; i++)
                    {
                        DataRow dr = tempDt.Rows[i];
                        ((Dictionary<string, string>)CommonServerMessage)[dr["MSG_ID"].ToString()] = dr["MSG_TEXT"].ToString();

                    }
                }
            }
            catch (SqlException)
            {

                MessagesDataTable = new DataTable();
                MessagesDataTable.Locale = System.Globalization.CultureInfo.CurrentCulture;
                MessagesDataTable.Columns.Add("MSG_ID");
                MessagesDataTable.Columns.Add("MSG_TEXT");
                foreach (String msg in _commonClientMsg)
                {
                    MessagesDataTable.Rows.Add(msg, "");
                }
            }
            #endregion
        }

        public static DataTable GetScreenCommonMessages
        {
            get
            {
                return MessagesDataTable;
            }
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We just log the exception and return an HTTP code")]
        public static String GetMessage(String msgId, String[] msgParameters)
        {
            SqlConnection con = null;
            try
            {
                con = CSTDDBUtil.GetPooledConnection;
                con.Open();
                String message = GetMessage(con, msgId, msgParameters);
                if (String.IsNullOrEmpty(message))
                {
                    throw new Exception("MNFS00009E: Undefine error : Message not found in message master with key : MSG_ID=" + msgId);
                }
                else
                {
                    return message;
                }
            }
            catch (Exception e)
            {
                //can't get message from database
                return "MNFS00009E: Undefine error : " + e.Message;
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static String GetMessage(SqlConnection con, String msgId, String[] msgParameters)
        {
            bool IsConnOpen = true;
            try
            {
                if (con.State != ConnectionState.Open)
                {
                    IsConnOpen = false;
                    con.Open();
                }
                //DataTable dt = CSTDDBUtil.ExecuteQuery(con, "SELECT A.MESSAGE_TEXT FROM TB_M_MESSAGE A WHERE A.MESSAGE_ID = @msgId", new Hashtable() { { "@msgId", msgId } });
                DataTable dt = CSTDDBUtil.ExecuteQuery(con, "SELECT A.MSG_TEXT MESSAGE_TEXT FROM TB_M_MESSAGE A WHERE A.MSG_ID = @msgId", new Hashtable() { { "@msgId", msgId } });
                if (dt != null && dt.Rows.Count > 0)
                {
                    String message = msgId + " : " + dt.Rows[0][0].ToString();
                    return ReplaceMessageParameters(message, msgParameters);
                }
            }
            finally
            {
                if (!IsConnOpen)
                {
                    if (con.State == ConnectionState.Open)
                    {
                        con.Close();
                    }
                }
            }
            return null;
        }

        public static String ReplaceMessageParameters(String message, String[] messageParameters)
        {
            if (messageParameters != null && messageParameters.Length > 0)
            {
                for (int i = 0; i < messageParameters.Length; i++)
                {
                    message = message.Replace("{" + i + "}", messageParameters[i]);
                }
            }
            return message;
        }

        public static String[] GetUnhandledMessage(String error)
        {
            return new String[] { GetUnhandledMessageString(error) };
        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We just log the exception and return an HTTP code")]
        public static String GetUnhandledMessageString(String error)
        {
            String messageId = "MNFS00009E";
            
            SqlConnection con = null;
            try
            {
                con = CSTDDBUtil.GetPooledConnection;
                con.Open();
                return GetMessage(con, messageId, new String[] { error });
            }
            catch (Exception e)
            {
                //can't get message from database
                return messageId + " : " + e.Message;
            }
            finally
            {
                if (con != null) con.Close();
            }
        }

        public static String DeletedConcurrentMsg()
        {
            return ((Dictionary<string, string>)CommonServerMessage)[RecordDeleted];
        }

        public static String UpdatedConcurrentMsg()
        {
            return ((Dictionary<string, string>)CommonServerMessage)[RecordUpdated];
        }

        public static String ProcessSuccessMsg(String messageParameter)
        {
            return ReplaceMessageParameters(((Dictionary<string, string>)CommonServerMessage)[ProcessSuccess], new String[] { messageParameter, "" });
        }

        public static String SaveSuccessMsg()
        {
            return ((Dictionary<string, string>)CommonServerMessage)[SaveSuccess];
        }

        public static String DeleteSuccessMsg()
        {
            return ((Dictionary<string, string>)CommonServerMessage)[DeleteSuccess];
        }

        public static String ConfirmSuccessMsg()
        {
            return ((Dictionary<string, string>)CommonServerMessage)[ConfirmProcessSuccess];
        }

        public static String ApproveSuccessMsg()
        {
            return ((Dictionary<string, string>)CommonServerMessage)[ApprovedProcessSuccess];
        }

        public static String GetMessageCustom(String msg, String[] msgParameters)
        {
            string result = msg;
            foreach (var param in msgParameters)
            {
                string idx = "{" + msgParameters.ToList().IndexOf(param).ToString() + "{";
                result = result.Replace(idx, param);
            }
            return result;
        }
    }
}