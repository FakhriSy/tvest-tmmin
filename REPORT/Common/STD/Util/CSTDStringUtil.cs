﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Reflection;
using System.IO;
using System.Collections;
using System.Globalization;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDStringUtil
    {
        public static Object GetValue(Object value, String defaultValue)
        {
            return value != null ? value : defaultValue == null ? "" : defaultValue;
        }

        public static String LinkProcessId(String message)
        {
            if (message == null) return message;
            int indexProcessId = message.ToLower(CultureInfo.CurrentCulture).IndexOf("process id",StringComparison.OrdinalIgnoreCase);
            if (indexProcessId > -1)
            {
                StringBuilder newMessage = new StringBuilder();
                newMessage.Append(message.Substring(0, indexProcessId + 10));
                String postMessage = message.Substring(indexProcessId + 11);
                //find processId no.
                String[] arrPostMessage = postMessage.Split(' ');
                String processId = null;
                if (arrPostMessage != null)
                {
                    for (int i = 0; i < arrPostMessage.Length; i++)
                    {
                        if (processId != null || arrPostMessage[i].Length != 16)
                        {
                            newMessage.Append(" ").Append(arrPostMessage[i]);
                        }
                        else if (arrPostMessage[i].Length == 16 && processId == null)
                        {
                            processId = arrPostMessage[i];
                            newMessage.Append(" ").Append("<span style=\"color:blue;text-decoration: underline; cursor: hand \" onclick=javascript:c_openNewWindow('ACA090102W','/ACA090102W?FormAction=SEARCH&Search.ProcessId=" + processId + "')>" + processId + "</span>");
                        }
                    }
                }
                return newMessage.ToString();
            }
            else
            {
                return message;
            }
        }

        public static String GetCurrentDirectory
        {
            get
            {
                return Path.GetDirectoryName(Assembly.GetEntryAssembly().Location);
            }
        }

        public static String GetConfigFile(String fileName)
        {
            String curDirectory = GetCurrentDirectory;
            return curDirectory.Substring(0, curDirectory.LastIndexOf("\\",StringComparison.OrdinalIgnoreCase)) + @"\config\" + fileName;
        }

        public static String GetApplicationResourcesPath
        {
            get
            {
                return GetCurrentDirectory + @"\Resources\";
            }
        }

        public static String ParsePLSQLReturnValue(Hashtable result)
        {
            int retValue = CSTDNumberUtil.GetNumber(result["ReturnValue"].ToString());
            if (retValue != 0)
            {
                //error
                return CSTDErrorUtil.ParseErrorToXml((String[])result["Ro_Rec_Out_Messages"]);
            }
            else
            {
                //success
                return CSTDErrorUtil.ParseMessagesToXML((String[])result["Ro_Rec_Out_Messages"]);
            }

        }

        public static String ParsePLSQLReturnValue(Hashtable result,String[] message)
        {
            int retValue = CSTDNumberUtil.GetNumber(result["ReturnValue"].ToString());
            String[] stMessage = (String[])result["Ro_Rec_Out_Messages"];
            String[] stMergerMessage = new String[stMessage.Length + message.Length];
            int j = 0;
            for (int i = 0; i < stMessage.Length; i++)
            {
                stMergerMessage[j++] = stMessage[i];
            }
            for (int i = 0; i < message.Length; i++)
            {
                stMergerMessage[j++] = message[i];
            }
            if (retValue != 0)
            {
                //error
                return CSTDErrorUtil.ParseErrorToXml(stMergerMessage);
            }
            else
            {
                //success
                return CSTDErrorUtil.ParseMessagesToXML(stMergerMessage);
            }

        }

        public static String GetRegExpression(String fileFormat)
        {
            String regexp, tempRegexp;
            int firstFormatPos = fileFormat.IndexOf("{");
            int formatLength = (fileFormat.IndexOf("}") - firstFormatPos) + 1;

            tempRegexp = regexp = fileFormat.Substring(firstFormatPos, formatLength);
            regexp = regexp.Replace("{", "").Replace("}", "");
            regexp = regexp.Replace("dd", "\\d{2}").Replace("MM", "\\d{2}").Replace("yyyy", "\\d{4}");

            /** Modified by FID)Praditha, 15/08/2014 - User Request to change Format File Name until Mili Second (fff) **/
            regexp = regexp.Replace("HH", "\\d{2}").Replace("mm", "\\d{2}").Replace("ss", "\\d{2}").Replace("fff", "\\d{3}");

            /** Create UpperCase regex **/
            fileFormat = fileFormat.ToUpper().Replace(tempRegexp.ToUpper(), regexp) + "$";
            fileFormat += "|" + fileFormat.ToLower().Replace(tempRegexp.ToLower(), regexp + "$");

            return fileFormat;
        }

        /**
         * Added by Praditha, 16/07/2014
         * Purpose: Generate Html String for Report Download on Log Monitoring
         * **/
        public static String BuildDownloadFileLink(String processId, String fileName) {
            return "<a href=\"#\" style=\"text-decoration:none\" eView=\"1\" > " +
                    "<span onclick=\"NFS080401WJs.doNFS080401WDownload('" + processId + "')\"> " +
                    "<font color=\"blue\"><b>" + fileName + "</b></font></span></a>.";
        }

        /**
         * Added by FID)Yusuf, 05/03/2018
         * Purpose: Generate Default value while string substring
         * **/
        public static String GetDefaultSubstring(string value, int index, int length, string defaultVal)
        {
            string result = null;
            try
            {
                if (index <= value.Length - 1 && length <= value.Substring(index).Length)
                {
                    result = value.Substring(index, length);
                }
            }
            catch
            {

            }
            return result ?? defaultVal;
        }
    }
}
