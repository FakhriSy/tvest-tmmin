﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Web;
using Newtonsoft.Json.Linq;
using Common.STD.DTO;
using log4net;
using log4net.Config;

namespace Common.STD.Util
{
    public class CSTDWebAPIUtil
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(CSTDWebAPIUtil));

        public CSTDWebAPIUtil()
        {
            //Set default protocol to TLS 1.2
            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
        }

        public class TokenData
        {
            public string access_token { get; set; }

            public string token_type { get; set; }

            public string expires_in { get; set; }
        }

        public static T GetToken<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                string json = string.Empty;

                try
                {
                    json_data = w.DownloadString(url);

                    JObject obj = JObject.Parse(json_data);

                    json = JsonConvert.SerializeObject(obj.SelectToken("token"));

                }
                catch (Exception) { }

                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json) : new T();
            }
        }

        public static List<T> Request<T>(Dictionary<string, string> data, string uri, string jsonConvert, string method) where T : new()
        {
            //get token
            log.Info("Get Token");
            var postReq = "userName=" + CSTDSystemConfig.GetValue("ippcs_uname_ws");
            postReq += "&password=" + CSTDSystemConfig.GetValue("ippcs_pwd_ws");
            string getToken = CSTDSystemConfig.GetValue("ippcs_uri_token_ws") + postReq;
            //log.Info("postReq" + postReq);
            //log.Info("getToken" + getToken);
            var TokenResponse = _download_serialized_json_data_token<TokenData>(getToken);
            log.Info("Token " + TokenResponse.access_token);
            var settings = new JsonSerializerSettings
            {
                NullValueHandling = NullValueHandling.Ignore,
                MissingMemberHandling = MissingMemberHandling.Ignore
            };

            string content = string.Empty;
            dynamic ajax_array = new Object();

            //handle error
            string Errresponse = string.Empty;

            ServicePointManager.SecurityProtocol = (SecurityProtocolType)3072;
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(CSTDSystemConfig.GetValue("ippcs_uri_ws") + uri);

            //set headers
            request.ContentType = "application/json";
            request.UserAgent = "Mozilla/4.0 (compatible; MSIE 6.0; Windows NT 5.1; SV1; .NET CLR 1.1.4322; .NET CLR 2.0.50727)";
            request.Method = method;
            //set token
            request.Headers.Add(HttpRequestHeader.Authorization, "Bearer " + TokenResponse.access_token);

            request.Proxy = WebRequest.DefaultWebProxy;
            request.Credentials = System.Net.CredentialCache.DefaultCredentials; ;
            request.Proxy.Credentials = System.Net.CredentialCache.DefaultCredentials;
            log.Info("Start Get Data");
            try
            {
                if (data != null && data.Count > 0)
                {
                    //write data to be sent
                    string dataJson = JsonConvert.SerializeObject(data);
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamWriter.Write(dataJson);
                    }
                }

                if (jsonConvert != null)
                {
                    //write data to be sent
                    using (var streamWriter = new StreamWriter(request.GetRequestStream()))
                    {
                        streamWriter.Write(jsonConvert);
                    }
                }

                //retrieve response
                var response = request.GetResponse() as HttpWebResponse;

                using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                {
                    content = reader.ReadToEnd();
                }
                response.Close();

                ajax_array = JsonConvert.DeserializeObject<List<T>>(content, settings);
                //ajax_array.Message = null;

                return !string.IsNullOrEmpty(content) ? ajax_array : new List<T>();

            }
            catch (WebException ex)
            {
                using (var reader = new System.IO.StreamReader(ex.Response.GetResponseStream()))
                {
                    Errresponse = reader.ReadToEnd();

                    log.Info("Execute api Error " + Errresponse);
                }
                //throw ex;
                ajax_array = new List<T>();
                //ajax_array.Message = JsonConvert.DeserializeObject(Errresponse).ToString();

                return !string.IsNullOrEmpty(Errresponse) ? ajax_array : new List<T>();
            }
            log.Info("Finish Get Data");
        }

        public static T _download_serialized_json_data_token<T>(string url) where T : new()
        {
            using (var w = new WebClient())
            {
                var json_data = string.Empty;
                string json = string.Empty;

                try
                {
                    json_data = w.DownloadString(url);
                    JObject obj = JObject.Parse(json_data);

                    json = JsonConvert.SerializeObject(obj.SelectToken("token"));

                }
                catch (Exception) { }

                return !string.IsNullOrEmpty(json_data) ? JsonConvert.DeserializeObject<T>(json) : new T();
            }
        }

    }
}
