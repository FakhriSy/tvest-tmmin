﻿using System;
using Common.STD.DTO;
using log4net;
using Microsoft.AspNet.SignalR.Client;
using Microsoft.AspNet.SignalR.Client.Hubs;

namespace Common.STD.Util
{
    public class CSTDHubUtil
    {

        //Should install from NuGet Package Manager Console
        // Install-Package Microsoft.AspNet.SignalR
        // Install-Package Microsoft.AspNet.SignalR.Client

        static ILog Logger = LogManager.GetLogger(typeof(CSTDHubUtil));
        public static void SendScript(string userId, string[] functionIdScript)
        {
            // Connect to the service
            var hubConnection = new HubConnection(CSTDSystemConfig.GetValue("iami_app_server"));

            // Create a proxy to the hub service
            var hub = hubConnection.CreateHubProxy("IamiHub");

            // Print the message when it comes in
            Logger.Info("SendScript On, App URL : " + hubConnection);
            hub.On("SendScript", message => Console.WriteLine(message));

            // Start the connection
            hubConnection.Start().Wait();

            // Send a message to the server
            //hub.Invoke("sendScriptToFunction", new String[] { functionId, script }).Wait();
            Logger.Info("SendScript After Start");
            foreach (string functIdScript in functionIdScript)
            {
                Logger.Info("SendScript");
                hub.Invoke("sendScriptToFunction", new string[] { userId, 
                            functIdScript.Split(new string[] { "#:#" }, StringSplitOptions.None)[0], 
                            functIdScript.Split(new string[] { "#:#" }, StringSplitOptions.None)[1] });
            }
            hubConnection.Stop();

        }

        public static void SendScript(string userId, string functionId, string script)
        {
            // Connect to the service
            var hubConnection = new HubConnection(CSTDSystemConfig.GetValue("iami_app_server"));

            // Create a proxy to the hub service
            var hub = hubConnection.CreateHubProxy("IamiHub");

            // Print the message when it comes in
            hub.On("SendScript", message => Console.WriteLine(message));

            // Start the connection
            hubConnection.Start().Wait();

            // Send a message to the server
            //hub.Invoke("sendScriptToFunction", new String[] { functionId, script }).Wait();
            hub.Invoke("sendScriptToFunction", new string[] { userId, functionId, script });
            hubConnection.Stop();

        }

        public static void SendMessage(string userId, string functionId, string stMessage)
        {
            // Connect to the service
            var hubConnection = new HubConnection(CSTDSystemConfig.GetValue("iami_app_server"));

            // Create a proxy to the hub service
            var hub = hubConnection.CreateHubProxy("IamiHub");

            // Print the message when it comes in
            hub.On("SendScript", message => Console.WriteLine(stMessage));

            // Start the connection
            hubConnection.Start().Wait();

            // Send a message to the server
            //hub.Invoke("sendScriptToFunction", new String[] { functionId, script }).Wait();
            hub.Invoke("sendMessageToPage", new string[] { userId, functionId, stMessage });

        }
    }
}
