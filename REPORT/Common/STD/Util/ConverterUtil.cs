﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Reflection;
using System.Collections;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class ConverterUtil
    {
        public ConverterUtil()
        {
        }

        public static DataTable ToDataTable(Object items)
        {
            IList<Hashtable> Items = (IList<Hashtable>)items;
            DataTable dataTable = new DataTable();
            dataTable.Locale = System.Globalization.CultureInfo.CurrentCulture;
            if (Items.Count > 0)
            {
                Hashtable tmp = Items[0];
                String[] keys = new String[tmp.Keys.Count];
                int j = 0;
                foreach (String key in tmp.Keys)
                {
                    dataTable.Columns.Add(key);
                    keys[j++] = key;
                }

                foreach (Hashtable item in Items)
                {
                    var values = new object[dataTable.Columns.Count];
                    for (int i = 0; i < values.Length; i++)
                    {
                        //inserting property values to datatable rows
                        values[i] = item[keys[i]];
                    }
                    dataTable.Rows.Add(values);
                }
            }
            return dataTable;
        }


        public static String ToXml(IList<Hashtable> items)
        {
            StringBuilder result = new StringBuilder();
            result.Append("<root>");
            if (items.Count > 0)
            {
                Hashtable tmp = items[0];
                String[] keys = new String[tmp.Keys.Count];
                int j = 0;
                foreach (String key in tmp.Keys)
                {
                    keys[j++] = key;
                }

                foreach (Hashtable item in items)
                {
                    result.Append("<tr>");
                    for (int i = 0; i < keys.Length; i++)
                    {
                        result.Append("<td>" + item[keys[i]] + "</td>");
                    }
                    result.Append("</tr>");
                }
            }
            result.Append("</root>");
            return result.ToString();
        }

    }
}
