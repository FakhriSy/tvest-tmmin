﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDErrorUtil
    {
        public CSTDErrorUtil()
        {
        }
        public static String ParseMessagesToXML(String[] errors)
        {
            StringBuilder result = new StringBuilder();
            if (errors == null || errors.Length == 0)
            {
                return "";
            }

            result.Append("<root><info>");
            for (int i = 0; i < errors.Length; i++)
            {
                result.Append("<message><![CDATA[").Append(errors[i]).Append("]]></message>");
            }
            result.Append("</info></root>");
            return result.ToString();
        }

        public static String ParseErrorToXml(String[] errors)
        {
            StringBuilder result = new StringBuilder();
            if (errors == null || errors.Length == 0)
            {
                errors = new String[] { "PL/SQL returns non-zero result with no message information. Please contact PLSQL Developer to solve the problem." };
            }

            result.Append("<root><error><![CDATA[");
            result
                    .Append("<TABLE width=\"100%\" border=\"0\" cellpadding=\"1\" cellspacing=\"2\" class=\"Message_Table\" id=\"tblError\" >\n");
            for (int i = 0; i < errors.Length; i++)
            {
                String message = errors[i];
                if (message == null) continue;
                String stMessageIcon;
                String stAlt = "";
                String stMessage = message;
                if (message.Length > 12)
                {
                    stMessage = message.Substring(0, 10);
                }
                        if (stMessage.EndsWith("I", StringComparison.OrdinalIgnoreCase))
                        {
                            stMessageIcon = "/Images/icon_info.gif";
                        }
                        else if (stMessage.EndsWith("C", StringComparison.OrdinalIgnoreCase))
                        {
                            stMessageIcon = "/Images/icon_confirm.gif";
                        }
                        else
                        {
                            stMessageIcon = "/Images/icon_error.gif";
                        }

                result.Append("<TR><TD>\n");
                result.Append("<img src=\"" + stMessageIcon
                        + "\" width=\"15\" height=\"15\" alt=\"");
                result.Append(stAlt);
                result.Append("\">");
                result.Append("\n</TD><TD width=\"100%\" >\n");
                result.Append(CSTDStringUtil.LinkProcessId(message));
                result.Append("\n</TD></TR>\n");
            }
            result.Append("</TABLE>");
            result.Append("]]></error></root>");
            return result.ToString();
        }

    }
}
