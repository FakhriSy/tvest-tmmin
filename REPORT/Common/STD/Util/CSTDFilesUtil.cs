﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : IAMI New Factory System & Operation Shop
 * Client Name      : 
 * Program Id       : CSTDFilesUtil
 * Program Name     : Common Files Utility
 * Description      : 
 * Environment      : .NET 4.5
 * Author           : FID.Praditha
 * Version          : 01.00.00
 * Creation Date    : 14/04/2014 17:11:00
 * 
 * Update history     Re-fix date       Person in charge      Description 
 *
 * Copyright(C) 2013 - . All Rights Reserved                                                                                              
 *************************************************************************************************/
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace Common.STD.Util
{
    public static class CSTDFilesUtil
    {
        public static void MoveFile(String source, String pathTarget)
        {
            try
            {
                File.Move(source, pathTarget + @"\" + CSTDDateUtil.GetCurrentDBDate().ToString("yyyyMMddHHmmss") + "-" + Path.GetFileName(source));
            }
            catch (Exception e)
            {
                throw e;
            }
        }


        public static string[] ListDirectory(String Path,String filePattern)
        {
            var newList = new List<string>();
            foreach (String tmp in Directory.GetFiles(Path))
            {
                if (System.Text.RegularExpressions.Regex.IsMatch(tmp, filePattern))
                {
                    newList.Add(tmp);
                }
            }
            return newList.ToArray();
        }

        public static bool IsDirectoryExist(String Path)
        {
            return Directory.Exists(Path);
        }
    }
}
