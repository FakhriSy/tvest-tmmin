﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Security.Cryptography;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Util
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CryptographerEngine
    {
        public static string Encrypt(string toEncrypt, bool useHashing)
        {
            byte[] keyArray;
            byte[] toEncryptArray = UTF8Encoding.UTF8.GetBytes(String.IsNullOrEmpty(toEncrypt) ? "" : toEncrypt);
            //System.Configuration.AppSettingsReader settingsReader = new     AppSettingsReader();
            string Key = "ACA123!!";
            if (useHashing)
            {
                using (MD5CryptoServiceProvider hashmd5 = new MD5CryptoServiceProvider())
                {
                    keyArray = hashmd5.ComputeHash(UTF8Encoding.UTF8.GetBytes(Key));
                    hashmd5.Clear();
                }
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(Key);
            }
            using (TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider())
            {
                tDes.Key = keyArray;
                tDes.Mode = CipherMode.ECB;
                tDes.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tDes.CreateEncryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toEncryptArray, 0, toEncryptArray.Length);
                tDes.Clear();
                return Convert.ToBase64String(resultArray, 0, resultArray.Length);
            }
        }
        public static string Decrypt(string cypherValue, bool useHashing)
        {
            byte[] keyArray;
            byte[] toDecryptArray = Convert.FromBase64String(cypherValue);
            //byte[] toEncryptArray = Convert.FromBase64String(cypherString);
            //System.Configuration.AppSettingsReader settingReader = new     AppSettingsReader();
            string key = "ACA123!!";
            if (useHashing)
            {
                using (MD5CryptoServiceProvider hashmd = new MD5CryptoServiceProvider())
                {
                    keyArray = hashmd.ComputeHash(UTF8Encoding.UTF8.GetBytes(key));
                    hashmd.Clear();
                }
            }
            else
            {
                keyArray = UTF8Encoding.UTF8.GetBytes(key);
            }
            using (TripleDESCryptoServiceProvider tDes = new TripleDESCryptoServiceProvider())
            {
                tDes.Key = keyArray;
                tDes.Mode = CipherMode.ECB;
                tDes.Padding = PaddingMode.PKCS7;
                ICryptoTransform cTransform = tDes.CreateDecryptor();
                byte[] resultArray = cTransform.TransformFinalBlock(toDecryptArray, 0, toDecryptArray.Length);

                tDes.Clear();
                return UTF8Encoding.UTF8.GetString(resultArray, 0, resultArray.Length);
                
            }
        }

    }
}
