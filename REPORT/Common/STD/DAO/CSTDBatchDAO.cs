﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common.STD.Util;
using System.Data.SqlClient;
using System.Collections;
using log4net;
using log4net.Config;
using System.IO;
using System.Reflection;
using System.Threading;
using System.Globalization;
using Common.STD.Base;

namespace Common.STD.DAO
{
    public class CSTDBatchDAO     {
        protected static ILog Logger { get { return LogManager.GetLogger(typeof(CSTDBatchDAO)); } }
        protected static String GetRunningQuery { get; set; }
        protected static String GetQueueListQuery { get; set; }
        protected static String InsertQuery { get; set; }
        protected static String DeleteQuery { get; set; }
        protected static String UpdateQuery { get; set; }
        protected static String UpdateRunningCountQuery { get; set; }
        private const int MaximumRetry = 20;

        //protected bool loadConfig = true;
        public CSTDBatchDAO()
        {
            //during startup, it's possible Sql services have not started yet, so try to get connection 
            //and repeat until success or max retry exceeded.
            SqlConnection con = null;
            int retry = 0;
            try
            {
                while (con == null && ++retry < MaximumRetry)
                {
                    try
                    {
                        con = CSTDDBUtil.GetBatchConnection;
                    }
                    catch (SqlException e)
                    {
                        //sleep for 5 second
                        Logger.Error(e.Message);
                        Thread.Sleep(5000);
                    }
                }

            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }


        public static DataTable GetParameter(String ProsessId)
        {
            String selectQuery = "SELECT PROCESS_ID, PARAM_SEQ, VALUE_PARAM FROM TB_T_PARAMETER WHERE PROCESS_ID=" + CSTDBaseDAO.LeadingSQLParameter + "ProsessId ORDER BY PARAM_SEQ ";
            DataTable dt = CSTDDBUtil.ExecuteQuery(selectQuery, new Hashtable() { { CSTDBaseDAO.LeadingSQLParameter + "ProsessId", ProsessId } });
            return dt;
        }

        public static DataTable GetQueueList
        {
            get
            {
                DataTable dtResult = null;
                StringBuilder query = new StringBuilder();
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetBatchConnection;
                    con.Open();
                    if (GetQueueListQuery == null)
                    {
                        query
                            
                        .Append("		select 0 queue_no,		\n")
                        .Append("		       'SCHEDULE' request_id,		\n")
                        .Append("		       a.batch_id,		\n")
                        .Append("		       FORMAT(CURRENT_TIMESTAMP,'dd-mm-yyyy') as request_date,		\n")
                        .Append("		       'SYSTEM' as request_by,		\n")
                        .Append("		       a.parameter,		\n")
                        .Append("		       a.project_code,		\n")
                        .Append("		       b.shell, 		\n")
                        .Append("		       b.run_as, 		\n")
                        .Append("		       b.batch_name,	\n")
                        .Append("		       9 priority_level,			\n")
                        .Append("		       b.concurrency_flag			\n")
                        .Append("		  from tb_m_schedule a, tb_m_batch b		\n")
                        .Append("		 where a.batch_id = b.batch_id		\n")
                        .Append("		   and a.project_code = b.project_code	\n")

                        .Append("		 union		\n")

                        .Append("		select a.queue_no,	\n")
                        .Append("		       a.request_id,	\n")
                        .Append("		       a.batch_id,	\n")
                        .Append("		       FORMAT(a.request_date,'dd-mm-yyyy') as request_date,	\n")
                        .Append("		       a.request_by,	\n")
                        .Append("		       a.parameter,	\n")
                        .Append("		       a.project_code,	\n")
                        .Append("		       b.shell, 	\n")
                        .Append("		       b.run_as, 	\n")
                        .Append("		       b.batch_name, 	\n")
                        .Append("		       b.priority_level, 	\n")
                        .Append("		       b.concurrency_flag			\n")
                        .Append("		  from tb_r_batch_queue a, tb_m_batch b	\n")
                        .Append("		 where a.batch_id = b.batch_id	\n")
                        .Append("		   and a.project_code = b.project_code	\n")
                        .Append("		   and (b.running_count = 0 or b.concurrency_flag = 0)	\n")
                        
                        .Append("		 order by priority_level desc	\n");
                        GetQueueListQuery = query.ToString();

                    }
                    dtResult = CSTDDBUtil.ExecuteQuery(con, GetQueueListQuery);
                }
                catch (SqlException e)
                {
                    Logger.Error(e.StackTrace);
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
                return dtResult;
            }
        }

        public static DataTable GetLongQueueList()
        {
            DataTable dtResult = null;
            StringBuilder query = new StringBuilder();
            SqlConnection con = null;
            try
            {
                con = CSTDDBUtil.GetBatchConnection;
                con.Open();
                String _getQueueLongListQuery = @"SELECT COUNT(QUEUE_NO) NUM_OF_LONG_QUEUE FROM TB_R_BATCH_QUEUE 
WHERE DATEDIFF(second,REQUEST_DATE,CURRENT_TIMESTAMP) > 2 /* second */";

                dtResult = CSTDDBUtil.ExecuteQuery(con, _getQueueLongListQuery);
            }
            catch (SqlException e)
            {
                Logger.Error(e.StackTrace);
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
            return dtResult;
        }

        ////Not use again since CSTDBatchDaemon use array of CSTDBatchThread
        //public static int GetRunningProcesses
        //{
        //    get
        //    {
        //        SqlConnection con = null;
        //        try
        //        {
        //            con = CSTDDBUtil.GetBatchConnection;
        //            con.Open();
        //            if (GetRunningQuery == null)
        //            {
        //                StringBuilder query = new StringBuilder();
        //                query
        //                .Append(" select sum(a.running_count) from tb_m_batch a	\n");
        //                GetRunningQuery = query.ToString();
        //            }
        //            DataTable dt = CSTDDBUtil.ExecuteQuery(con, GetRunningQuery);
        //            if (dt != null && dt.Rows.Count == 1)
        //            {
        //                if(String.IsNullOrEmpty(dt.Rows[0][0].ToString())){
        //                    return 0;
        //                }else{
        //                    return Convert.ToInt32(dt.Rows[0][0].ToString(), CultureInfo.CurrentCulture);
        //                }
        //            }
        //        }
        //        catch (SqlException e)
        //        {
        //            Logger.Error(e.StackTrace);
        //        }
        //        finally
        //        {
        //            if (con != null)
        //            {
        //                con.Close();
        //            }
        //        }
        //        return 0;
        //    }
        //}

        public static void MoveQueueToHistory(String queueNo)
        {
            SqlConnection con = null;
            SqlTransaction sqlTrans = null;
            try
            {
                con = CSTDDBUtil.GetBatchConnection;
                con.Open();
                sqlTrans = con.BeginTransaction();


                if (true)
                {
                    StringBuilder query = new StringBuilder();
                    query.Append("	INSERT INTO tb_l_batch_status_log	\n")
                        .Append("	  (queue_no	\n")
                        .Append("	   ,request_id	\n")
                        .Append("	   ,batch_id	\n")
                        .Append("	   ,request_date	\n")
                        .Append("	   ,request_by	\n")
                        .Append("	   ,support_id	\n")
                        .Append("	   ,project_code	\n")
                        .Append("	   ,parameter	\n")
                        .Append("	   ,current_status	\n")
                        .Append("	   ,batch_result	\n")
                        //.Append("	   ,run_date	\n")
                        .Append("	   )	\n")
                        .Append("	  SELECT a.queue_no	\n")
                        .Append("	         ,a.request_id	\n")
                        .Append("	         ,a.batch_id	\n")
                        .Append("	         ,a.request_date	\n")
                        .Append("	         ,a.request_by	\n")
                        .Append("	         ,a.support_id	\n")
                        .Append("	         ,a.project_code	\n")
                        .Append("	         ,a.parameter	\n")
                        .Append("	         ,0	\n")
                        .Append("	         ,null	\n")
                        //.Append("	         ,current_timestamp	\n")
                        .Append("	    FROM TB_R_BATCH_QUEUE A	\n")
                        .Append("	   WHERE A.QUEUE_NO = " + CSTDBaseDAO.LeadingSQLParameter + "queueNo	\n");
                    InsertQuery = query.ToString();
                    DeleteQuery = " delete from tb_r_batch_queue where queue_no = " + CSTDBaseDAO.LeadingSQLParameter + "queueNo";
                }

                Hashtable param = new Hashtable();
                param[CSTDBaseDAO.LeadingSQLParameter + "queueNo"] = queueNo;
                CSTDDBUtil.ExecuteUpdate(con,sqlTrans, InsertQuery, param);

                //delete from queue
                CSTDDBUtil.ExecuteUpdate(con,sqlTrans, DeleteQuery, param);

                sqlTrans.Commit();
            }
            catch (SqlException e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
                sqlTrans.Rollback();
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }

        public void UpdateQueueStatus(String status, String message, String queueNo)
        {
            SqlConnection con = null;

            SqlTransaction sqlTrans = null;
            try
            {
                con = CSTDDBUtil.GetBatchConnection;


                con.Open();
                sqlTrans = con.BeginTransaction();
                if (UpdateQuery == null)
                {
                    StringBuilder query = new StringBuilder();
                    query
                    .Append("	update tb_l_batch_status_log 	\n")
                    .Append("	set current_status = " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "status, batch_result = " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "batchResult "+
                                "where queue_no = " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "queueNo	\n");
                    UpdateQuery = query.ToString();
                }
                Hashtable queryParams = new Hashtable();
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "status"] = status;
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "batchResult"] = message;
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "queueNo"] = queueNo;

                CSTDDBUtil.ExecuteUpdate(con,sqlTrans, UpdateQuery, queryParams);
                sqlTrans.Commit();
            }
            catch (SqlException e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
                if (sqlTrans != null)
                {
                    sqlTrans.Rollback();
                }
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }

        public void UpdateRunningCount(int delta, String batchId, String projectCode)
        {
            SqlConnection con = null;
            SqlTransaction sqlTrans = null;
            try
            {
                con = CSTDDBUtil.GetBatchConnection;
                con.Open();
                sqlTrans = con.BeginTransaction();
                if (UpdateRunningCountQuery == null)
                {
                    StringBuilder query = new StringBuilder();
                    query
                    .Append("	update tb_m_batch \n")
                    .Append("	   set running_count = running_count + " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "delta	\n")
                    .Append("	 where batch_id = " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "batchId and project_code = " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "projectCd and \n")
                    .Append("	       running_count > ( case when " + 
                                        CSTDBaseDAO.LeadingSQLParameter + "delta = 1 then -1 else 0 end ) \n");
                    UpdateRunningCountQuery = query.ToString();
                }
                Hashtable queryParams = new Hashtable();
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "delta"] = Convert.ToString(delta, CultureInfo.CurrentCulture);
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "batchId"] = batchId;
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "projectCd"] = projectCode;
                queryParams[CSTDBaseDAO.LeadingSQLParameter + "delta"] = Convert.ToString(delta);


                CSTDDBUtil.ExecuteUpdate(con,sqlTrans, UpdateRunningCountQuery, queryParams);
                sqlTrans.Commit();
            }
            catch (SqlException e)
            {
                Logger.Error(e.Message);
                Logger.Error(e.StackTrace);
                if (sqlTrans != null)
                {
                    sqlTrans.Rollback();
                }
            }
            finally
            {
                if (con != null)
                {
                    con.Close();
                }
            }
        }

        public static String GetTWSFlag
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "BFW_USE_TWS", "1");
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }

        public static String GetBatchMail
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "BATCH_MAIL", "1");
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static String GetBatchMailFrom
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "BATCH_MAIL_FROM", "1");
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static String GetBatchMailto
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "BATCH_MAIL_TO", "1").Trim() + CSTDSystemMasterHelper.GetValue(con, "COMM", "BATCH_MAIL_TO", "2").Trim();
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }

        public static String GetTWSMonitor
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "BFW_MONITOR_TWS", "1").Trim();
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static String GetMaximumBatchRun
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "MAX_TWS_EXEC_HOURS", "1").Trim();
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }
        public static String GetBatchLogLocation
        {
            get
            {
                SqlConnection con = null;
                try
                {
                    con = CSTDDBUtil.GetNonPooledConnection;
                    con.Open();
                    return CSTDSystemMasterHelper.GetValue(con, "COMM", "BATCH_LOG_PATH", "1").Trim();
                }
                finally
                {
                    if (con != null)
                    {
                        con.Close();
                    }
                }
            }
        }
    }
}
