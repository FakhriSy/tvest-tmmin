﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Component Export Digitalization and Automation System
 * Client Name      : TMMIN
 * Program Id       : COMMON
 * Program Name     : Common Bussiness Object Wrapper
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID) Yusuf
 * Version          : 01.00.00
 * Creation Date    : 01/03/2018 16:00:00
 * 
 * Update history     Re-fix date       Person in charge      Description 
 *
 * Copyright(C) 2013 - . All Rights Reserved                                                                                              
 *************************************************************************************************/
using Common.STD.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.STD.IBase
{
    public interface ICSTDBaseBO
    {
       Dictionary<string, object> ExecuteBatch(string functionId, string userId, string[] reportParameters);
    }
}
