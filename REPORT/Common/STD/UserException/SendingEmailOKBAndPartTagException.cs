﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.STD.UserException
{
    public class SendingEmailOKBAndPartTagException : Exception
    {
        public SendingEmailOKBAndPartTagException()
        {
    
        }

        public SendingEmailOKBAndPartTagException(string message)
            : base(message)
        {
        
        }

        public SendingEmailOKBAndPartTagException(string message, Exception inner)
            : base(message, inner)
        {
    
        }
    }
}
