﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Common.STD.Base 
{
    public class CSTDBaseDTO : CSTDBaseBaseDTO
    {
        public CSTDBaseDTO()
		{
		}

        public int TotalRows { get; set; }
        public int RowNo { get; set; }
        public String CreatedBy { get; set; }
        public DateTime CreatedDt { get; set; }
        public String UpdatedBy { get; set; }
        public DateTime UpdatedDt { get; set; }
        public Object RowAction { get; set; }
        
        #region added by FID)Widodo 15/08/2014
        public Object InSysCat { get; set; }
        public Object InSysSubCat { get; set; }
        public Object InSysCd { get; set; }
        #endregion
        
    }
}
