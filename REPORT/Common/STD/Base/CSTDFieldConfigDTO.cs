﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Common.STD.Base
{
    public class CSTDFieldConfigDTO : CSTDBaseBaseDTO
    {
        public CSTDFieldConfigDTO()
        {
        }

        public String FunctionID { get; set; }
        public String FieldName { get; set; }
        public String InputId { get; set; }
        public String InputName { get; set; }
        public String AccessMode { get; set; }
        public String Label { get; set; }
        public String DataType { get; set; }
        public String DataFormat { get; set; }
        public String PickerFormat { get; set; }
        public String ValidationFormat { get; set; }
        public String DataSource { get; set; }
        public String Length { get; set; }
        public String Presicion { get; set; }
        public String Mandatory { get; set; }
        public String DisplayObject { get; set; }
        public String Width { get; set; }
        public String Height { get; set; }
        public String Attribute { get; set; }
        public String Style { get; set; }
        public String Title { get; set; }

    }
}
