﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.STD.Util;
using System.Data;
using System.Collections;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.IO;
using System.Data.SqlClient;
using System.Text.RegularExpressions;

namespace Common.STD.Base
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDBaseBO
    {
        public static String OperationAddMode { get { return "ADD"; } }
        public static String OperationEditMode { get { return "EDIT"; } }
        public static String OperationDeleteMode { get { return "DELETE"; } }
        public static String OperationFindMode { get { return "FIND"; } }

        protected static void CalculateStartEndRow(CSTDBaseSearchDTO search)
        {
            search.StartRow = (search.Page - 1) * int.Parse(search.RowPerPage) + 1;
            search.EndRow = search.StartRow + int.Parse(search.RowPerPage) - 1;
        }

        public static DateTime ToDateTime(String Date, String Format)
        {
            if (String.IsNullOrEmpty(Date))
            {
                return new DateTime();
            }
            if (String.IsNullOrEmpty(Format))
            {
                Format = "d-M-yyyy";
            }
            return DateTime.ParseExact(Date, Format, CultureInfo.InvariantCulture);
        }

        protected static Boolean CheckDuplication(CSTDBaseDTO dto, Object messages, String queryId, String uniqueIdentifier)
        {
            List<String> Messages = (List<String>)messages;
            //check if the same user id exist
            CSTDBaseDTO dbDto = CSTDMapper.Instance().QueryForObject<CSTDBaseDTO>(queryId, dto);
            if (dbDto != null)
            {
                Messages.Add(CSTDMessageUtil.GetMessage("MNFS00005E", new String[] { uniqueIdentifier }));
                messages = Messages;
                return false;
            }
            return true;
        }

        protected static Boolean CheckConcurrency(CSTDBaseDTO dto, Object messages, String queryId)
        {
            //concurrency control
            List<String> Messages = (List<String>)messages;
            CSTDBaseDTO dbDto = CSTDMapper.Instance().QueryForObject<CSTDBaseDTO>(queryId, dto);
            if (dbDto == null)
            {
                //record has been deleted
                Messages.Add(CSTDMessageUtil.DeletedConcurrentMsg());
                messages = Messages;
                return false;
            }
            else if (!dbDto.UpdatedDt.ToString().Equals(dto.UpdatedDt.ToString(), StringComparison.OrdinalIgnoreCase))
            {
                Messages.Add(CSTDMessageUtil.UpdatedConcurrentMsg());
                messages = Messages;
                return false;
            }
            return true;
        }


        protected static Object GetResultProccess(Exception e)
        {
            Dictionary<String, List<Object>> resultSearch = new Dictionary<String, List<Object>>();
            CSTDResultProcessDTO CSTDResultProcess = new CSTDResultProcessDTO();
            List<Object> resultDTOListMapped = new List<Object>();
            List<Object> resultPageInfoListMapped = new List<Object>();
            List<Object> resultResultProcessListMapped = new List<Object>();

            resultSearch.Add("ResultSearch", resultDTOListMapped);
            resultPageInfoListMapped.Add(new CSTDPageInfoDTO());
            resultSearch.Add("PageInfo", resultPageInfoListMapped);
            CSTDResultProcess.ProcessStatus = 1;
            CSTDResultProcess.StringErrorMessage = CSTDMessageUtil.GetMessage("MNFS00009E", new String[] { GetExceptionMessage(e) });
            resultResultProcessListMapped.Add(CSTDResultProcess);
            resultSearch.Add("ResultProcess", resultResultProcessListMapped);

            return resultSearch;
        }


        protected static String GetExceptionMessage(Exception e)
        {
            String errorMessage = e.Message;
            Exception InnerException = e.InnerException;
            while (InnerException != null)
            {
                errorMessage += ".\n" + InnerException.Message;
                InnerException = e.InnerException;
            }
            return errorMessage;
        }
        protected static bool CheckTemplateFolder(String templateFolder, String template)
        {
            FileInfo info = new FileInfo(@templateFolder + template);
            return info.Exists;
        }
        String RegexEmail = @"^[A-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[A-z0-9](?:[A-z0-9-]*[A-z0-9])?\.)+[A-z0-9](?:[A-z0-9-]*[A-z0-9])$";
        String RegexIp = @"^(\d{1,3}\.){3}\d{1,3}$";
        String RegexDate = @"^\d{2}[- \/.]\d{2}[- \/.]\d{4}$";
        String RegexDatetime = @"^\d{2}[- \/.]\d{2}[- \/.]\d{4}\s*?\d{2}[- :.]\d{2}$";

        String RegexYearMonth = @"^\d{4}[- \/.]?\d{2}$";
        //String RegexDateYYYYMMDD = @"^\d{4}[- \/.]?\d{2}[- \/.]?\d{2}$";
        String RegexDateYYYYMMDD = @"^\d{4}[- \/.]?(0[1-9]|1[0-2])[- \/.]?([0-2]\d|3[0-1])$";
        String RegexDatetimeYYYYMMDD = @"^\d{4}[- \/.]?\d{2}[- \/.]?\d{2}\s*?\d{2}[- :.]?\d{2}[- :.]?\d{2}$";
        String RegexDatetimeDDMMYYYY = @"^[0-3]\d\/[0-1]\d\/[1-3]\d{3} [0-2]\d:[0-5]\d:[0-5]\d$";
        String RegexDatetimeYYYYMM = @"^\d{4}[- \/.]?\d{2}[- \/.]?\s*?\d{2}[- :.]?\d{2}[- :.]?\d{2}$";
        String RegexDateDDMONYYYY = @"^\d{2}[- \/.]Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec[- \/.]\d{4}$";
        String RegexDateMONYYYY = @"^Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec[- \/.]\d{4}$";

        //add by Fid. Dio, regex for date format mm/yyyy
        String RegexDateMMYYYY = @"^\d{2}[- \/.]?\d{4}$";

        //add by FID) Fidyar, regex for checking time with format HH:MM:SS
        String RegexTimeHHMMSS = @"^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$";

        //Added by FID)Praditha, regex for datetime format YYYYMMDDHHMI
        String RegexDatetimeYYYYMMDDHHMI = @"^\d{4}[- \/.]?\d{2}[- \/.]?\d{2}\s*?\d{2}[- :.]?\d{2}$";
        String RegexTimeHHMM = @"^\d{2}[- :.]?\d{2}$";

        String RegexPhone = @"^\+?\(?\+?\d{1,4}?\)?-?\s?\d{1,16}$";
        String RegexZipUS = @"^(\d{5})(-\d{4})?$";
        String RegexZipCanada = @"^[A-z][0-9][A-z]\s*?[0-9][A-z][0-9]$";
        String RegexCreditCard = @"^\d{13,16}$";
        String RegexNumeric = @"^([-]{0,1})\d+$";
        String RegexDecimal = @"^([-]{0,1})([0-9]{1,3})(,[0-9]{3})*([.]{0}|[.]{1}[0-9]+)$";
        String RegexAlphaNumeric = @"^([A-Za-z]|\d|\s|-|\.|_|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+$";
        String RegexAlphaNumericWildcard = @"^(([A-z]|\d|\s|-|\.|_){1})+([*]{0,1})+$";
        String RegexAllCharacter = @"^([A-z~`!@#$%^&*()-=_+\{\}\:\"";'<>?,.\/\[\]\|\\]|\d|\s|-|\.|_|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+$";
        SqlConnection conn = null;
        
        public List<string> Validate(
            SqlConnection con,
            object ObjectData,
            String Label,
            String DataType,
            String Format,
            String RegexFormat,
            String Mandatory,
            String Length,
            String Presicion)
        {
            conn = con;
            return Validate(
             ObjectData,
             Label,
             DataType,
             Format,
             RegexFormat,
             Mandatory,
             Length,
             Presicion);
        }
        public List<string> Validate(
            object ObjectData,
            String Label,
            String DataType,
            String Format,
            String RegexFormat,
            String Mandatory,
            String Length,
            String Presicion)
        {
            String strDateFormat = "";
            List<String> Messages = new List<string>();

            if (conn == null)
            {
                conn = CSTDDBUtil.GetNonPooledConnection;
            }

            // Check Mandatory
            if (Messages == null)
            {
                Messages = new List<string>();
            }
            String Data = ObjectData == null ? "" : ObjectData.ToString().Trim();
            if ("1".Equals(Mandatory))
            {
                if (String.IsNullOrEmpty(Data))
                {
                    Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00003E", new String[] { Label, Format }));
                }
            }

            //Check Format

            if (!String.IsNullOrEmpty(Data))
            {
                switch (DataType)
                {
                    case "email":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexEmail))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "ip":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexIp))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "date":
                        String CurrDateRegex = "";
                        switch (RegexFormat)
                        {
                            case "date":
                                CurrDateRegex = RegexDate;
                                strDateFormat = "ddMMyyyy";
                                break;
                            case "datetime":
                                CurrDateRegex = RegexDatetime;
                                strDateFormat = "ddMMyyyyHH:mm";
                                break;
                            case "yearmonth":
                                CurrDateRegex = RegexYearMonth;
                                strDateFormat = "yyyyMM";
                                break;
                            case "dateYYYYMMDD":
                                CurrDateRegex = RegexDateYYYYMMDD;
                                strDateFormat = "yyyyMMdd";
                                break;
                            case "datetimeYYYYMMDD":
                                CurrDateRegex = RegexDatetimeYYYYMMDD; ;
                                strDateFormat = "yyyyMMddHH:mm:ss";
                                break;
                            case "datetimeYYYYMMDDHHMI":
                                CurrDateRegex = RegexDatetimeYYYYMMDDHHMI;
                                strDateFormat = "yyyyMMddHH:mm";
                                break;
                            case "dateYYYYMM":
                                CurrDateRegex = RegexDatetimeYYYYMM;
                                strDateFormat = "yyyyMMHH:mm:ss";
                                break;
                            case "dateDDMONYYYY":
                                CurrDateRegex = RegexDateDDMONYYYY;
                                strDateFormat = "ddMMMyyyy";
                                break;
                            case "dateMONYYYY":
                                CurrDateRegex = RegexDateMONYYYY;
                                strDateFormat = "MMMyyyy";
                                break;
                            case "DateMMYYYY":
                                CurrDateRegex = RegexDateMMYYYY;
                                strDateFormat = "MMyyyy";
                                break;
                            case "TimeHH:MM:SS":
                                CurrDateRegex = RegexTimeHHMMSS;
                                strDateFormat = "HH:mm:ss";
                                break;
                            case "TimeHH:MM":
                                CurrDateRegex = RegexTimeHHMM;
                                strDateFormat = "HH:mm";
                                break;
                            case "datetimeDD/MM/YYYY":
                                CurrDateRegex = RegexDatetimeDDMMYYYY;
                                strDateFormat = "dd/MM/yyyy HH:mm:ss";
                                break;
                            default:
                                CurrDateRegex = RegexDate;
                                strDateFormat = "ddMMyyyy";
                                break;
                        }
                        if (!Regex.IsMatch(Data, CurrDateRegex))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        else if (!CheckDateFormat(Data, strDateFormat))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { "value for " + Label + ": " + Data }));
                            /** Comment by FID)Praditha - 23/04/2014 
                             * No need to convert to datetime, because the format will be different
                             * **/
                            //return ToDateTime(Data, Format);
                        }
                        break;
                    case "datetime":
                        goto case "date";
                    case "phone":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexPhone))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "zipUS":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexZipUS))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "zipCanada":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexZipCanada))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "creditCard":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexCreditCard))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "numeric":
                        //FID)Widodo 19/06/2014
                        //check first if the Data contain wildcard or not
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAlphaNumeric))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                            break;
                        }
                        //end FID)Widodo 19/06/2014
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexNumeric))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label }));
                        }
                        break;
                    case "decimal":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexDecimal))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label }));
                        }
                        break;
                    case "alphanumeric":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAlphaNumeric))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericwildcard":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAlphaNumericWildcard) || Data.Count(x => x == '*') > 1)
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "allcharacter":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAllCharacter))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    default:
                        break;
                }
            }

            // Check Length
            String tmpDataCheckLength = "";
            if ("numeric".Equals(DataType) || "decimal".Equals(DataType))
            {
                tmpDataCheckLength = Data.Replace(",", "").Replace(".", "");
            }
            else
            {
                tmpDataCheckLength = Data;
            }
            if ("decimal".Equals(DataType))
            {
                Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00001E", new String[] { Label, Length + "" + (String.IsNullOrEmpty(Presicion) ? "" : "," + Presicion) }));
            }
            /* added by fid Dio 20/10/2014 length must be precisely for nik and engine no*/
            else
                if (("alphanumericnikengine".Equals(DataType) || "alphanumericengine".Equals(DataType)) && !String.IsNullOrEmpty(Data))
            {
                if (tmpDataCheckLength.Length != Int32.Parse(Length) + (String.IsNullOrEmpty(Presicion) ? 0 : Int32.Parse(Presicion)))
                {
                    //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00001E", new String[] { Label, Length + "" }));
                    Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label, Length + " digits" }));
                }
            }
            /* end */
            else
            if (!"date".Equals(DataType) && !"datetime".Equals(DataType))
            {
                if (tmpDataCheckLength.Length > Int32.Parse(Length) + (String.IsNullOrEmpty(Presicion) ? 0 : Int32.Parse(Presicion)))
                {
                    Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00001E", new String[] { Label, Length + "" }));
                }
            }
            return Messages;
        }
        private bool CheckDateFormat(string data, string expectedFormat)
        {
            DateTime dateValue;

            data = data.Replace("-", "").Replace(" ", "").Replace("/", "").Replace(".", "").Replace(":", "");
            expectedFormat = expectedFormat.Replace("-", "").Replace(" ", "").Replace("/", "").Replace(".", "").Replace(":", "");

            if (DateTime.TryParseExact(data, expectedFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

    }
}
