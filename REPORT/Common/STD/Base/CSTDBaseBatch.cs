﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Model;
using System.IO;
using Common.STD.Util;
using System.Diagnostics.CodeAnalysis;
using log4net;
namespace Common.STD.Base
{
    public abstract class CSTDBaseBatch
    {
        public ILog log = LogManager.GetLogger(typeof(CSTDBaseBatch));

        public Object Columns { get; set; }

        public abstract Dictionary<String, Object> ExecuteBatch(String functionId, String userId, String[] reportParameters);
        
        protected static HSSFWorkbook InitializeWorkbook(String template)
        {
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            String fullPathTemplate = @CSTDProperties.PropertiesPath + @"\Template\" + template;
            FileStream file = new FileStream(fullPathTemplate, FileMode.Open, FileAccess.Read);

            HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);

            //create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "Isuzu Astra Motor Indonesia";
            hssfworkbook.DocumentSummaryInformation = dsi;

            //create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "New Factory System Generated Report";
            hssfworkbook.SummaryInformation = si;
            return hssfworkbook;
        }

        protected static bool CheckFile(String template)
        {
            FileInfo info = new FileInfo(@CSTDProperties.PropertiesPath + @"\Template\" + template);
            return info.Exists;
        }
    }

}
