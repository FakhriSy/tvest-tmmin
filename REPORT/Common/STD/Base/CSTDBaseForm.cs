﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using Common.STD.Util;
using System.Diagnostics.CodeAnalysis;

namespace Common.STD.Base
{
    public class CSTDBaseForm 
    {

        public string FormAction { get; set; }
        public string EverSearch { get; set; }
        public int Page { get; set; }
        public int MaxPage { get; set; }
        public int TotalRows { get; set; }

        public Object GridData { get; set; }
        public Object PageInfoDTO { get; set; }
        public Object ResultProcessDTO { get; set; }

        public Object Messages { get; set; }
        public String ObjectNameCaller { get; set; }
        public String RunJavaScript { get; set; }

        public String StartTIme { get; set; }
        public DateTime StartDateTime { get; set; }
        public String FinistTime { get; set; }
        public String stClientFormName { get; set; }
        public String stClientTimeStart { get; set; }

        public String TargetWidth { get; set; }
        public String TargetHeight { get; set; }
        public bool IsError { get; set; }
        public bool NotReplaceQuote { get; set; }
        public String ContainerWidth { get; set; }
        public String ContainerHeight { get; set; }
        

        public Object ResourceProperties { get; set; }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We just log the exception and return an HTTP code")]
        public CSTDBaseForm()
        {
            try
            {

                StartDateTime = CSTDDateUtil.GetCurrentDBDate();
                StartTIme = StartDateTime.ToShortDateString() + " " + StartDateTime.ToLongTimeString() + " " + StartDateTime.Millisecond;

                ResourceProperties = CSTDProperties.LoadFromFile("ResourceEng.properties");

                String[] stNamespace = this.GetType().Namespace.Split(new char[] { '.' });

                Dictionary<String, String> functiomProperties = CSTDProperties.LoadFromFile("Properties/" + stNamespace[stNamespace.Length - 1] + "Eng.properties");
                ResourceProperties = ((Dictionary<String, String>)ResourceProperties).Union(functiomProperties).ToDictionary(k => k.Key, v => v.Value);
                MaxPage = 1;

            }
            catch (Exception e)
            {
                this.Messages = new String[] { e.Message };
            }

        }

        [SuppressMessage("Microsoft.Design", "CA1031:DoNotCatchGeneralExceptionTypes", Justification = "We just log the exception and return an HTTP code")]
        public String GetResource(String resourceName)
        {
            try
            {
                return ((Dictionary<String, String>)ResourceProperties)[resourceName];
            }
            catch (Exception e)
            {
                return "Resource " + resourceName +" not found." + e.Message;
            }


        }

        public void PutSearchResult()
        {
            if (GridData == null || ((IList)GridData).Count == 0)
            {
                Messages = new String[]{ ((Dictionary<string,string>)CSTDMessageUtil.CommonServerMessage)[CSTDMessageUtil.NoDataFound]};
                TotalRows = 0;
                MaxPage = 0;
            }
            else
            {
                Messages = new String[]{((Dictionary<string,string>)CSTDMessageUtil.CommonServerMessage)[CSTDMessageUtil.SuccessRetrieve]};
                CSTDBaseDTO dto = (CSTDBaseDTO) ((IList)GridData)[0];
                TotalRows = dto.TotalRows;
                MaxPage = TotalRows / int.Parse((new CSTDBaseSearchDTO()).RowPerPage);
                if (TotalRows % int.Parse((new CSTDBaseSearchDTO()).RowPerPage) > 0) MaxPage++;
            }
            EverSearch = "true";
        }

        public void GetPreviousMessage(String[] previousMessages)
        {
            if (previousMessages != null && previousMessages.Length > 0)
            {
                String[] newMessages = null;
                if (Messages == null)
                {
                    newMessages = new String[previousMessages.Length];
                }
                else
                {
                    newMessages = new String[((String[])Messages).Length + previousMessages.Length];
                }
                int indexNewMessage = 0;
                for (int i = 0; i < previousMessages.Length; i++)
                {
                    newMessages[indexNewMessage++] = previousMessages[i];
                }

                if (Messages != null)
                {
                    for (int i = 0; i < ((String[])Messages).Length; i++)
                    {
                        newMessages[indexNewMessage++] = ((String[])Messages)[i];
                    }

                } Messages = newMessages;
            }
        }

        public void CheckDataFound(Object checkedValue)
        {
            if (checkedValue == null || String.IsNullOrEmpty(checkedValue.ToString()))
            {
                if (((String[])Messages).Length == 0)
                    Messages = new String[] { CSTDMessageUtil.GetMessage("MNFS00014E", null) };
            }
            else
            {
                if (((String[])Messages).Length == 0)
                    Messages = new String[] { CSTDMessageUtil.GetMessage("MNFS00021I", null) };
            }
        }
    }
}
