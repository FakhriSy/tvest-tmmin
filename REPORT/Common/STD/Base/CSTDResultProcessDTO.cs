﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Common.STD.Base 
{
    public class CSTDResultProcessDTO : CSTDBaseBaseDTO
    {
        public CSTDResultProcessDTO()
		{
            this.AffectedRows = 0;
            this.ProcessStatus = 0;
            this.TotalRows = 0;
		}

        public int AffectedRows { get; set; } 
        public int ProcessStatus { get; set; }
        public int TotalRows { get; set; } 
        public String StringSuccessMessage { get; set; }
        public String StringErrorMessage { get; set; } 
    }
}
