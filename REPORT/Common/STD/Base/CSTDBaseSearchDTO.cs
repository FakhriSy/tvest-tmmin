﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Common.STD.Util;

namespace Common.STD.Base
{
    public class CSTDBaseSearchDTO : CSTDBaseBaseDTO
    {
        public int Page { get; set; }
        public String RowPerPage { get; set; }
        public int StartRow { get; set; }
        public int EndRow { get; set; }

        public CSTDBaseSearchDTO()
        {
            Page = 1;
            //String defRecordPerPage = CSTDSystemMasterHelper.GetValue("COMMON", "REC_PER_PAGE", "01");
            String defRecordPerPage = CSTDSystemMasterHelper.GetValue( "REC_PER_PAGE", "01");
            RowPerPage = String.IsNullOrEmpty(defRecordPerPage) ? "10" : defRecordPerPage;
        }
    }
}
