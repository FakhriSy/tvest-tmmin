﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;

namespace Common.STD.Base 
{
    public class CSTDPageInfoDTO : CSTDBaseBaseDTO
    {
        public CSTDPageInfoDTO()
		{
            this.CurrentPage = 0;
            this.RowPerPage = 0;
            this.PageCount = 0;
            this.RowCount = 0;
        }

        public int CurrentPage { get; set; }
        public int RowPerPage { get; set; }
        public int PageCount { get; set; }
        public int RowCount { get; set; }
    }
}
