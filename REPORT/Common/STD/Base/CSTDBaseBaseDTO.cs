﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
using System.Text.RegularExpressions;
using Common.STD.Util;
using System.Globalization;

namespace Common.STD.Base
{
    public class CSTDBaseBaseDTO
    {
        #region Added by FID)Praditha 21/08/2014, Set Stage Code, Series Code & Shift Code as Common Value
        public String CommStageOnCode { get { return "1"; } }
        //public String CommStageOffCode { get { return "2"; } }
        //public String CommStageOn { get { return CSTDSystemMasterHelper.GetValue("MASTER", "STAGE", CommStageOnCode); } }
        //public String CommStageOff { get { return CSTDSystemMasterHelper.GetValue("MASTER", "STAGE", CommStageOffCode); } }
        public String CommNSeriesCode { get { return "1"; } }
        public String CommFSeriesCode { get { return "2"; } }
        //public String CommNSeries { get { return CSTDSystemMasterHelper.GetValue("MASTER", "LINE_SERIES", CommNSeriesCode); } }
        //public String CommFSeries { get { return CSTDSystemMasterHelper.GetValue("MASTER", "LINE_SERIES", CommFSeriesCode); } }
        public String CommDayShiftCode { get { return "2"; } }
        public String CommNightShiftCode { get { return "1"; } }
        //public String CommDayShift { get { return CSTDSystemMasterHelper.GetValue("MASTER", "PRODUCTION_SHIFT", CommDayShiftCode); } }
        //public String CommNightShift { get { return CSTDSystemMasterHelper.GetValue("MASTER", "PRODUCTION_SHIFT", CommNightShiftCode); } }
        #endregion

        public Object Columns { get; set; }

        public virtual void SetAll(SqlDataReader reader)
        {

        }
        public void SetColumn(SqlDataReader reader)
        {
            Columns = new HashSet<string>();
            int fieldCount = reader.FieldCount;
            for (int i = 0; i < fieldCount; i++)
            {
                ((HashSet<String>)Columns).Add(reader.GetName(i));
            }
        }
        public String GetValue(SqlDataReader reader, String fieldName)
        {
            if (((HashSet<String>)Columns).Contains(fieldName))
            {
                return reader[fieldName].ToString();
            }
            else
            {
                return "";
            }
        }


        public List<String> Messages { get; set; }

        String RegexEmail = @"^[A-z0-9!#$%&\'*+/=?^_`{|}~-]+(?:\.[A-z0-9!#$%&\'*+/=?^_`{|}~-]+)*@(?:[A-z0-9](?:[A-z0-9-]*[A-z0-9])?\.)+[A-z0-9](?:[A-z0-9-]*[A-z0-9])$";
        String RegexIp = @"^(\d{1,3}\.){3}\d{1,3}$";
        String RegexDate = @"^\d{2}[- \/.]\d{2}[- \/.]\d{4}$";
        String RegexDatetime = @"^\d{2}[- \/.]\d{2}[- \/.]\d{4}\s*?\d{2}[- :.]\d{2}$";

        String RegexYearMonth = @"^\d{4}[- \/.]?\d{2}$";
        //String RegexDateYYYYMMDD = @"^\d{4}[- \/.]?\d{2}[- \/.]?\d{2}$";
        String RegexDateYYYYMMDD = @"^\d{4}[- \/.]?(0[1-9]|1[0-2])[- \/.]?([0-2]\d|3[0-1])$";
        String RegexDatetimeYYYYMMDD = @"^\d{4}[- \/.]?\d{2}[- \/.]?\d{2}\s*?\d{2}[- :.]?\d{2}[- :.]?\d{2}$";
        String RegexDatetimeDDMMYYYY = @"^[0-3]\d\/[0-1]\d\/[1-3]\d{3} [0-2]\d:[0-5]\d:[0-5]\d$";
        String RegexDatetimeYYYYMM = @"^\d{4}[- \/.]?\d{2}[- \/.]?\s*?\d{2}[- :.]?\d{2}[- :.]?\d{2}$";
        String RegexDateDDMONYYYY = @"^\d{2}[- \/.]Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec[- \/.]\d{4}$";
        String RegexDateMONYYYY = @"^Jan|Feb|Mar|Apr|May|Jun|Jul|Aug|Sep|Oct|Nov|Dec[- \/.]\d{4}$";
        
        //add by Fid. Dio, regex for date format mm/yyyy
        String RegexDateMMYYYY = @"^\d{2}[- \/.]?\d{4}$";

        //add by FID) Fidyar, regex for checking time with format HH:MM:SS
        String RegexTimeHHMMSS = @"^(?:(?:([01]?\d|2[0-3]):)?([0-5]?\d):)?([0-5]?\d)$";

        //Added by FID)Praditha, regex for datetime format YYYYMMDDHHMI
        String RegexDatetimeYYYYMMDDHHMI = @"^\d{4}[- \/.]?\d{2}[- \/.]?\d{2}\s*?\d{2}[- :.]?\d{2}$";
        String RegexTimeHHMM = @"^\d{2}[- :.]?\d{2}$";

        String RegexPhone = @"^\+?\(?\+?\d{1,4}?\)?-?\s?\d{1,16}$";
        String RegexZipUS = @"^(\d{5})(-\d{4})?$";
        String RegexZipCanada = @"^[A-z][0-9][A-z]\s*?[0-9][A-z][0-9]$";
        String RegexCreditCard = @"^\d{13,16}$";
        String RegexNumeric = @"^([-]{0,1})\d+$";
        String RegexDecimal = @"^([-]{0,1})([0-9]{1,3})(,[0-9]{3})*([.]{0}|[.]{1}[0-9]+)$";
        String RegexAlphaNumeric = @"^([A-Za-z]|\d|\s|-|\.|_|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+$";        
        String RegexAlphaNumericWildcard = @"^(([A-z]|\d|\s|-|\.|_){1})+([*]{0,1})+$";
        String RegexAllCharacter = @"^([A-z~`!@#$%^&*()-=_+\{\}\:\"";'<>?,.\/\[\]\|\\]|\d|\s|-|\.|_|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+$";
        
        //added by FID)Dio, regex for Variant
        //String RegexVariant = @"^([A-z]|\d|\s|-|\.|_|\(|\)|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+$";
        String RegexVariant = @"^([A-Za-z]|\d|\s|-|\.|\(|\))+$";
        //added by FID)Teguh, regex for Variant
        String RegexVariantWildCard = @"^([A-Za-z]|\d|\s|-|\.|\(|\))+([*]{0,1})+$";
        //added by FID)Dio, regex for NIK & Engine N Series
        String RegexNIKEngine = @"^([A-Za-z]|\d)+$";
        //added by FID)Dio, regex for Engine F Series
        String RegexEngine = @"^([A-Za-z]|\d|-)+$";
        //added by FID)Rizal, regex for SupplierName
        String RegexSupplierName = @"^([A-Za-z]|\d|\s|-|\.|_|\(|\)|[\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])+$";
        //added by FID)Rizal, regex for Yes No
        String RegexYesNo = @"^(Y|y|N|n)+$";     

        public void CleanMessage()
        {
            if (Messages != null)
            {
                int IndexMessage = 0;
                int MessageCount = Messages.Count;
                for (int i = 0; i < MessageCount; i++)
                {
                    if (String.IsNullOrEmpty(Messages[IndexMessage]))
                    {
                        Messages.RemoveAt(IndexMessage);
                    }
                    else
                    {
                        IndexMessage++;
                    }
                }
            }
        }

        public static DateTime ToDateTime(String Date, String Format)
        {
            if (String.IsNullOrEmpty(Date))
            {
                return new DateTime();
            }
            if (String.IsNullOrEmpty(Format))
            {
                Format = "d-M-yyyy";
            }
            return DateTime.ParseExact(Date, Format, CultureInfo.InvariantCulture);
        }
        SqlConnection conn = null;
        /// <summary>
        /// 
        /// </summary>
        /// <param name="con"></param>
        /// <param name="ObjectData"></param>
        /// <param name="Label"></param>
        /// <param name="DataType"></param>
        /// <param name="Format"></param>
        /// <param name="RegexFormat"></param>
        /// <param name="Mandatory"></param>
        /// <param name="Length"></param>
        /// <param name="Presicion"></param>
        /// <returns></returns>
        public Object Validate(
            SqlConnection con,
            object ObjectData,
            String Label,
            String DataType,
            String Format,
            String RegexFormat,
            String Mandatory,
            String Length,
            String Presicion)
        {
            conn = con;
            return Validate(
             ObjectData,
             Label,
             DataType,
             Format,
             RegexFormat,
             Mandatory,
             Length,
             Presicion);
        }

        public Object Validate(
            object ObjectData,
            String Label,
            String DataType,
            String Format,
            String RegexFormat,
            String Mandatory,
            String Length,
            String Presicion)
        {
            String strDateFormat = "";

            if (conn == null) {
                conn = CSTDDBUtil.GetNonPooledConnection;
            }

            // Check Mandatory
            if (Messages == null)
            {
                Messages = new List<string>();
            }
            String Data = ObjectData == null ? "" : ObjectData.ToString().Trim();
            if ("1".Equals(Mandatory))
            {
                if (String.IsNullOrEmpty(Data))
                {
                    Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00003E", new String[] { Label, Format }));
                }
            }

            //Check Format

            if (!String.IsNullOrEmpty(Data))
            {
                switch (DataType)
                {
                    case "email":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexEmail))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data , Format }));
                        }
                        break;
                    case "ip":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexIp))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "date":
                        String CurrDateRegex = "";
                        switch (RegexFormat)
                        {
                            case "date":
                                CurrDateRegex = RegexDate;
                                strDateFormat = "ddMMyyyy";
                                break;
                            case "datetime":
                                CurrDateRegex = RegexDatetime;
                                strDateFormat = "ddMMyyyyHH:mm";
                                break;
                            case "yearmonth":
                                CurrDateRegex = RegexYearMonth;
                                strDateFormat = "yyyyMM";
                                break;
                            case "dateYYYYMMDD":
                                CurrDateRegex = RegexDateYYYYMMDD;
                                strDateFormat = "yyyyMMdd";
                                break;
                            case "datetimeYYYYMMDD":
                                CurrDateRegex = RegexDatetimeYYYYMMDD;;
                                strDateFormat = "yyyyMMddHH:mm:ss";
                                break;
                            case "datetimeYYYYMMDDHHMI":
                                CurrDateRegex = RegexDatetimeYYYYMMDDHHMI;
                                strDateFormat = "yyyyMMddHH:mm";
                                break;
                            case "dateYYYYMM":
                                CurrDateRegex = RegexDatetimeYYYYMM;
                                strDateFormat = "yyyyMMHH:mm:ss";
                                break;
                            case "dateDDMONYYYY":
                                CurrDateRegex = RegexDateDDMONYYYY;
                                strDateFormat = "ddMMMyyyy";
                                break;
                            case "dateMONYYYY":
                                CurrDateRegex = RegexDateMONYYYY;
                                strDateFormat = "MMMyyyy";
                                break;
                            case "DateMMYYYY":
                                CurrDateRegex = RegexDateMMYYYY;
                                strDateFormat = "MMyyyy";
                                break;
                            case "TimeHH:MM:SS":
                                CurrDateRegex = RegexTimeHHMMSS;
                                strDateFormat = "HH:mm:ss";
                                break;
                            case "TimeHH:MM":
                                CurrDateRegex = RegexTimeHHMM;
                                strDateFormat = "HH:mm";
                                break;
                            case "datetimeDD/MM/YYYY":
                                CurrDateRegex = RegexDatetimeDDMMYYYY;
                                strDateFormat = "dd/MM/yyyy HH:mm:ss";
                                break;
                            default:
                                CurrDateRegex = RegexDate;
                                strDateFormat = "ddMMyyyy";
                                break;
                        }
                        if (!Regex.IsMatch(Data, CurrDateRegex))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        else if (!CheckDateFormat(Data, strDateFormat))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { "value for " + Label + ": " + Data }));
                            /** Comment by FID)Praditha - 23/04/2014 
                             * No need to convert to datetime, because the format will be different
                             * **/
                            //return ToDateTime(Data, Format);
                        }
                        break;
                    case "datetime":
                        goto case "date";
                    case "phone":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexPhone))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "zipUS":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexZipUS))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "zipCanada":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexZipCanada))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "creditCard":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexCreditCard))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                        }
                        break;
                    case "numeric":
                        //FID)Widodo 19/06/2014
                        //check first if the Data contain wildcard or not
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAlphaNumeric)) 
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                            break;
                        }
                        //end FID)Widodo 19/06/2014
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexNumeric))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label }));
                        }
                        break;
                    case "decimal":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexDecimal))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label }));
                        }
                        break;
                    case "alphanumeric":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAlphaNumeric))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericwildcard":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAlphaNumericWildcard) || Data.Count(x => x == '*') > 1)
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericvariantwildcard":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexVariantWildCard) || Data.Count(x => x == '*') > 1)
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "allcharacter":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexAllCharacter))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericvariant":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexVariant))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericnikengine":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexNIKEngine))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericengine":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexEngine))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "alphanumericsuppliername":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexSupplierName))
                        {
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    case "regexyesno":
                        if (!Regex.IsMatch(Data, !String.IsNullOrEmpty(RegexFormat) ? RegexFormat : RegexYesNo))
                        {
                            Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label + ": " + Data, Format }));
                            //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00002E", new String[] { Label + ": " + Data }));
                        }
                        break;
                    default:
                        break;
                }
            }

            // Check Length
            String tmpDataCheckLength = "";
            if ("numeric".Equals(DataType) || "decimal".Equals(DataType))
            {
                tmpDataCheckLength = Data.Replace(",", "").Replace(".", "");
            }
            else
            {
                tmpDataCheckLength = Data;
            }
            if ("decimal".Equals(DataType))
            {
                Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00001E", new String[] { Label, Length + "" + (String.IsNullOrEmpty(Presicion) ? "" : "," + Presicion) }));
            }
            /* added by fid Dio 20/10/2014 length must be precisely for nik and engine no*/
            else
                if (("alphanumericnikengine".Equals(DataType) || "alphanumericengine".Equals(DataType)) && !String.IsNullOrEmpty(Data))
            {
                if (tmpDataCheckLength.Length != Int32.Parse(Length) + (String.IsNullOrEmpty(Presicion) ? 0 : Int32.Parse(Presicion)))
                {
                    //Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00001E", new String[] { Label, Length + "" }));
                    Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00004E", new String[] { Label, Length + " digits" }));
                }
            }
            /* end */
            else
            if (!"date".Equals(DataType) && !"datetime".Equals(DataType))
            {
                if (tmpDataCheckLength.Length > Int32.Parse(Length) + (String.IsNullOrEmpty(Presicion) ? 0 : Int32.Parse(Presicion)))
                {
                    Messages.Add(CSTDMessageUtil.GetMessage(conn, "MNFS00001E", new String[] { Label, Length + ""  }));
                }
            }
            return Data;
        }

        private bool CheckDateFormat(string data, string expectedFormat)
        {
            DateTime dateValue;

            data = data.Replace("-", "").Replace(" ", "").Replace("/", "").Replace(".", "").Replace(":", "");
            expectedFormat = expectedFormat.Replace("-", "").Replace(" ", "").Replace("/", "").Replace(".", "").Replace(":", "");

            if (DateTime.TryParseExact(data, expectedFormat, CultureInfo.InvariantCulture, DateTimeStyles.None, out dateValue))
            {
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}
