﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.HPSF;
using NPOI.POIFS.FileSystem;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.HSSF.Model;
using System.IO;
using Common.STD.Util;
using System.Diagnostics.CodeAnalysis;
using System.Collections;
using NPOI.XSSF.UserModel;
using NPOI.DDF;
namespace Common.STD.Base
{
    public abstract class CSTDBaseReport
    {
        public Object Columns { get; set; }

        //public abstract Dictionary<String, Object> ExecuteReport(String functionId, String userId, String[] reportParameters);
        public abstract Dictionary<String, Object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation);


        public String GetValue(SqlDataReader reader, String fieldName)
        {
            if (((HashSet<String>)Columns).Contains(fieldName))
            {
                return reader[fieldName].ToString();
            }
            else
            {
                return "";
            }
        }

        protected static HSSFWorkbook InitializeWorkbook(String template)
        {
            //String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("TEMPLATE_REPORT", "URL_TEMPLATE_FOLDER");
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            String fullPathTemplate = @_excelTemplateFolder+ "\\" + template;
            FileStream file = new FileStream(fullPathTemplate, FileMode.Open, FileAccess.Read);

            HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);

            //create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA (TMMIN)";
            hssfworkbook.DocumentSummaryInformation = dsi;

            //create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "CED Report";
            hssfworkbook.SummaryInformation = si;
            return hssfworkbook;
        }
        protected static HSSFWorkbook InitializeWorkbookExcel(String template)
        {
            //String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("TEMPLATE_REPORT", "URL_TEMPLATE_FOLDER");
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FDR", CSTDSystemMasterHelper.Mandatory);
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            String fullPathTemplate = @_excelTemplateFolder + "\\" + template;
            FileStream file = new FileStream(fullPathTemplate, FileMode.Open, FileAccess.Read);

            HSSFWorkbook hssfworkbook = new HSSFWorkbook(file);

            //create a entry of DocumentSummaryInformation
            DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            dsi.Company = "PT. TOYOTA MOTOR MANUFACTURING INDONESIA (TMMIN)";
            hssfworkbook.DocumentSummaryInformation = dsi;

            //create a entry of SummaryInformation
            SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            si.Subject = "CED Report";
            hssfworkbook.SummaryInformation = si;
            return hssfworkbook;
        }
        protected static XSSFWorkbook InitializeXWorkbook(String template)
        {
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("TEMPLATE_REPORT", "URL_TEMPLATE_FOLDER");
            //read the template via FileStream, it is suggested to use FileAccess.Read to prevent file lock.
            //book1.xls is an Excel-2007-generated file, so some new unknown BIFF records are added.
            String fullPathTemplate = @_excelTemplateFolder + "\\" + template;
            FileStream file = new FileStream(fullPathTemplate, FileMode.Open, FileAccess.Read);

            XSSFWorkbook hssfworkbook = new XSSFWorkbook(file);

            //create a entry of DocumentSummaryInformation
            //DocumentSummaryInformation dsi = PropertySetFactory.CreateDocumentSummaryInformation();
            //dsi.Company = "PT. ISUZU ASTRA MOTOR INDONESIA (IAMI)";
            //hssfworkbook.DocumentSummaryInformation = dsi;

            //////create a entry of SummaryInformation
            //SummaryInformation si = PropertySetFactory.CreateSummaryInformation();
            //si.Subject = "NFS Generated Report";
            //hssfworkbook.SummaryInformation = si;
            return hssfworkbook;
        }

        [SuppressMessage("Microsoft.Design", "CA1011:ConsiderPassingBaseTypesAsParameters")]
        protected static void WriteToFile(HSSFWorkbook workbook, String fileName)
        {
            //Write the stream data of workbook to the root directory
            FileStream file = new FileStream(fileName, FileMode.Create);
            workbook.Write(file);
            file.Close();
        }


        /// <summary>
        /// HSSFRow Copy Command
        /// 
        /// Description:  Inserts a existing row into a new row, will automatically push down
        ///               any existing rows.  Copy is done cell by cell and supports, and the
        ///               command tries to copy all properties available (style, merged cells, values, etc...)
        /// </summary>
        /// <param name="workbook">Workbook containing the worksheet that will be changed</param>
        /// <param name="worksheet">WorkSheet containing rows to be copied</param>
        /// <param name="sourceRowNumber">Source Row Number</param>
        /// <param name="destinationRowNumber">Destination Row Number</param>
        protected static void CopyRow(HSSFSheet worksheet, int sourceRowNumber, int destinationRowNumber)
        {
            // Get the source / new row
            HSSFRow newRow = (HSSFRow)worksheet.GetRow(destinationRowNumber);
            HSSFRow sourceRow = (HSSFRow)worksheet.GetRow(sourceRowNumber);

            // If the row exist in destination, push down all rows by 1 else create a new row
            if (newRow != null)
            {
                worksheet.ShiftRows(destinationRowNumber, worksheet.LastRowNum, 1);
            }
            else
            {
                newRow = (HSSFRow)worksheet.CreateRow(destinationRowNumber);
            }

            // Loop through source columns to add to new row
            for (int i = 0; i < sourceRow.LastCellNum; i++)
            {
                // Grab a copy of the old/new cell
                HSSFCell oldCell = (HSSFCell)sourceRow.GetCell(i);
                HSSFCell newCell = (HSSFCell)newRow.CreateCell(i);

                // If the old cell is null jump to next cell
                if (oldCell == null)
                {
                    newCell = null;
                    continue;
                }

                // Copy style from old cell and apply to new cell
                //HSSFCellStyle newCellStyle = (HSSFCellStyle)workbook.CreateCellStyle();
                //newCellStyle.CloneStyleFrom(oldCell.CellStyle); ;
                newCell.CellStyle = oldCell.CellStyle;

                // If there is a cell comment, copy
                if (newCell.CellComment != null) newCell.CellComment = oldCell.CellComment;

                // If there is a cell hyperlink, copy
                if (oldCell.Hyperlink != null) newCell.Hyperlink = oldCell.Hyperlink;

                // Set the cell data type
                newCell.SetCellType(oldCell.CellType);
                //Set the cell data value
                switch (oldCell.CellType)
                {
                    case CellType.BLANK:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                    case CellType.BOOLEAN:
                        newCell.SetCellValue(oldCell.BooleanCellValue);
                        break;
                    case CellType.ERROR:
                        newCell.SetCellErrorValue(oldCell.ErrorCellValue);
                        break;
                    case CellType.FORMULA:
                        newCell.SetCellFormula(oldCell.CellFormula);
                        break;
                    case CellType.NUMERIC:
                        newCell.SetCellValue(oldCell.NumericCellValue);
                        break;
                    case CellType.STRING:
                        newCell.SetCellValue(oldCell.RichStringCellValue);
                        break;
                    case CellType.Unknown:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                }
            }

            // If there are are any merged regions in the source row, copy to new row
            for (int i = 0; i < worksheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = worksheet.GetMergedRegion(i);
                if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.RowNum,
                                                                                (newRow.RowNum +
                                                                                 (cellRangeAddress.FirstRow -
                                                                                  cellRangeAddress.LastRow) /*+ 1*/),
                                                                                cellRangeAddress.FirstColumn,
                                                                                cellRangeAddress.LastColumn);
                    worksheet.AddMergedRegion(newCellRangeAddress);
                }
            }

        }

        protected static void CopyRowWithHeight(HSSFSheet worksheet, int sourceRowNumber, int destinationRowNumber, double rowHeight)
        {
            // Get the source / new row
            HSSFRow newRow = (HSSFRow)worksheet.GetRow(destinationRowNumber);
            HSSFRow sourceRow = (HSSFRow)worksheet.GetRow(sourceRowNumber);

            // If the row exist in destination, push down all rows by 1 else create a new row
            if (newRow != null)
            {
                newRow.HeightInPoints = (float)rowHeight;
                worksheet.ShiftRows(destinationRowNumber, worksheet.LastRowNum, 1);
            }
            else
            {
                newRow = (HSSFRow)worksheet.CreateRow(destinationRowNumber);
                newRow.HeightInPoints = (float)rowHeight;
            }

            // Loop through source columns to add to new row
            for (int i = 0; i < sourceRow.LastCellNum; i++)
            {
                // Grab a copy of the old/new cell
                HSSFCell oldCell = (HSSFCell)sourceRow.GetCell(i);
                HSSFCell newCell = (HSSFCell)newRow.CreateCell(i);

                // If the old cell is null jump to next cell
                if (oldCell == null)
                {
                    newCell = null;
                    continue;
                }

                // Copy style from old cell and apply to new cell
                //HSSFCellStyle newCellStyle = (HSSFCellStyle)workbook.CreateCellStyle();
                //newCellStyle.CloneStyleFrom(oldCell.CellStyle); ;
                newCell.CellStyle = oldCell.CellStyle;

                // If there is a cell comment, copy
                if (newCell.CellComment != null) newCell.CellComment = oldCell.CellComment;

                // If there is a cell hyperlink, copy
                if (oldCell.Hyperlink != null) newCell.Hyperlink = oldCell.Hyperlink;

                // Set the cell data type
                newCell.SetCellType(oldCell.CellType);
                //Set the cell data value
                switch (oldCell.CellType)
                {
                    case CellType.BLANK:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                    case CellType.BOOLEAN:
                        newCell.SetCellValue(oldCell.BooleanCellValue);
                        break;
                    case CellType.ERROR:
                        newCell.SetCellErrorValue(oldCell.ErrorCellValue);
                        break;
                    case CellType.FORMULA:
                        newCell.SetCellFormula(oldCell.CellFormula);
                        break;
                    case CellType.NUMERIC:
                        newCell.SetCellValue(oldCell.NumericCellValue);
                        break;
                    case CellType.STRING:
                        newCell.SetCellValue(oldCell.RichStringCellValue);
                        break;
                    case CellType.Unknown:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                }
            }

            // If there are are any merged regions in the source row, copy to new row
            for (int i = 0; i < worksheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = worksheet.GetMergedRegion(i);
                if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.RowNum,
                                                                                (newRow.RowNum +
                                                                                 (cellRangeAddress.FirstRow -
                                                                                  cellRangeAddress.LastRow) /*+ 1*/),
                                                                                cellRangeAddress.FirstColumn,
                                                                                cellRangeAddress.LastColumn);
                    worksheet.AddMergedRegion(newCellRangeAddress);
                }
            }

        }

        public static void copyCell(HSSFCell oldCell, HSSFCell newCell, bool copyStyle)
        {
            if (copyStyle)
            {
                newCell.CellStyle = oldCell.CellStyle;
            }

            switch (oldCell.CellType)
            {
                case CellType.STRING:
                    // newCell.setCellValue(oldCell.getStringCellValue());
                    newCell.SetCellValue(oldCell.RichStringCellValue);
                    break;

                case CellType.NUMERIC:
                    newCell.SetCellValue(oldCell.NumericCellValue);
                    break;

                case CellType.BLANK:
                    newCell.SetCellValue("");
                    newCell.SetCellType(CellType.BLANK);
                    break;

                case CellType.BOOLEAN:
                    newCell.SetCellValue(oldCell.BooleanCellValue);
                    break;

                case CellType.ERROR:
                    newCell.SetCellValue(oldCell.ErrorCellValue);
                    break;

                case CellType.FORMULA:
                    break;

                default:
                    break;
            }
        }


        protected static bool CheckTemplate(String template)
        {
            FileInfo info = new FileInfo(@CSTDProperties.PropertiesPath + @"\Template\" + template);
            return info.Exists;
        }

        // add by Zein
        protected static bool CheckTemplateFolder(String templateFolder, String template)
        {
            FileInfo info = new FileInfo(@templateFolder+"\\"+template);
            return info.Exists;
        }


        // add by Jumadi
        public static void copyCell(HSSFSheet srcSheet, HSSFSheet destSheet, int startRow, int endRow, short startCol, short endCol, int startRowDest, short startColDest)
        {
            try
            {
                HSSFRow[] srcRows = new HSSFRow[endRow - startRow + 1];
                HSSFRow[] destRows = new HSSFRow[endRow - startRow + 1];
                for (int i = startRow; i <= endRow; i++)
                {
                    HSSFRow srcRow = (HSSFRow)srcSheet.GetRow(i);
                    if (srcRow == null)
                    {
                        //Console.WriteLine(" row " + i + " in source sheet does not exist");
                        //return;
                    }
                    srcRows[i - startRow] = srcRow;

                    HSSFRow destRow = (HSSFRow)destSheet.GetRow(startRowDest + (i - startRow));
                    if (destRow == null)
                    {
                        destRow = (HSSFRow)destSheet.CreateRow(startRowDest + (i - startRow));
                    }

                    destRows[i - startRow] = destRow;
                }

                SortedList mergedRegions = new SortedList();

                for (int i = startRow; i <= endRow; i++)
                {
                    if (destRows[i - startRow] != null && srcRows[i - startRow]!=null)
                    {
                        destRows[i - startRow].Height = srcRows[i - startRow].Height;
                    }
                }
                for (int i = startRow; i <= endRow; i++)
                {
                    int a = 1;

                    if (srcRows[i - startRow] == null)
                    {

                    }else{
                        for (short j = startCol; j <= endCol; j++)
                        {
                            HSSFCell oldCell = (HSSFCell)srcRows[i - startRow].GetCell(j);

                            int idxColDestInt = startColDest + (j - startCol);
                            short idxColDest = (short)idxColDestInt;
                            HSSFCell newCell = (HSSFCell)destRows[i - startRow].GetCell(idxColDest);

                            if (oldCell != null)
                            {
                                if (newCell == null)
                                {
                                    newCell = (HSSFCell)destRows[i - startRow].CreateCell(idxColDest);
                                }

                                copyCell(oldCell, newCell, true);
                                CellRangeAddress mergedRegion = getMergedRegion(srcSheet, srcRows[i - startRow].RowNum, (short)oldCell.ColumnIndex);
                                if (mergedRegion != null)
                                {
                                    int rowFrom = destRows[i - startRow].RowNum;

                                    short columnFrom = (short)(startColDest + (mergedRegion.FirstColumn - startCol));

                                    int rowTo = destRows[i - startRow].RowNum + /*(mergedRegion.FirstRow-destRows[i - startRow].RowNum) +*/ (mergedRegion.LastRow - mergedRegion.FirstRow);

                                    short columnTo = (short)(startColDest + (mergedRegion.FirstColumn - startCol) + (mergedRegion.LastColumn - mergedRegion.FirstColumn));

                                    CellRangeAddress newMergedRegion = new CellRangeAddress(rowFrom, rowTo, columnFrom, columnTo);

                                    if (isNewMergedRegion(newMergedRegion, mergedRegions))
                                    {
                                        mergedRegions.Add(mergedRegions.Count, newMergedRegion);
                                        destSheet.AddMergedRegion(newMergedRegion);
                                    }
                                }
                            }
                        }
                    }
                }
                for (short j = startCol; j <= endCol; j++)
                {
                    int idxColDestInt = startColDest + (j - startCol);
                    short idxColDest = (short)idxColDestInt;
                    destSheet.SetColumnWidth(idxColDest, srcSheet.GetColumnWidth(j));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.Write(e.StackTrace);

            }

        }

        public static void emptyCell(HSSFSheet srcSheet, HSSFSheet destSheet, int startRow, int endRow, short startCol, short endCol, int startRowDest, short startColDest)
        {
            try
            {
                HSSFRow[] srcRows = new HSSFRow[endRow - startRow + 1];
                HSSFRow[] destRows = new HSSFRow[endRow - startRow + 1];
                
                for (int i = startRow; i <= endRow; i++)
                {
                    int a = 1;

                    if (srcRows[i - startRow] == null)
                    {

                    }
                    else
                    {
                        for (short j = startCol; j <= endCol; j++)
                        {
                            HSSFCell oldCell = (HSSFCell)srcRows[i - startRow].GetCell(j);

                            int idxColDestInt = startColDest + (j - startCol);
                            short idxColDest = (short)idxColDestInt;
                            HSSFCell newCell = (HSSFCell)destRows[i - startRow].GetCell(idxColDest);

                            if (oldCell != null)
                            {
                                if (newCell == null)
                                {

                                }
                                else
                                {
                                    newCell.SetCellValue("");
                                }
                            }
                        }
                    }
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.Write(e.StackTrace);

            }

        }

        public static bool isNewMergedRegion(CellRangeAddress region, SortedList mergedRegions)
        {
            for (int i = 0; i < mergedRegions.Count; i++)
            {
                if (region.FirstRow >= ((CellRangeAddress)mergedRegions[i]).FirstRow &&
                    region.FirstColumn >= ((CellRangeAddress)mergedRegions[i]).FirstColumn &&
                    region.FirstRow <= ((CellRangeAddress)mergedRegions[i]).LastRow &&
                    region.FirstColumn <= ((CellRangeAddress)mergedRegions[i]).LastColumn)
                {
                    return false;
                }
            }
            return true;

            //return !mergedRegions.Contains(region);
        }

        public static CellRangeAddress getMergedRegion(HSSFSheet sheet, int rowNum, short cellNum)
        {
            for (int i = 0; i < sheet.NumMergedRegions; i++)
            {
                CellRangeAddress merged = sheet.GetMergedRegion(i);
                if (merged.FirstRow <= rowNum && rowNum <= merged.LastRow && merged.FirstColumn <= cellNum && cellNum <= merged.LastColumn)
                {
                    return merged;
                }
            }

            return null;
        }


        // For XLSX, Add by Zein

        public static void copyCellX(XSSFCell oldCell, XSSFCell newCell, bool copyStyle)
        {
            if (copyStyle)
            {
                newCell.CellStyle = oldCell.CellStyle;
            }

            switch (oldCell.CellType)
            {
                case CellType.STRING:
                    // newCell.setCellValue(oldCell.getStringCellValue());
                    newCell.SetCellValue(oldCell.RichStringCellValue);
                    break;

                case CellType.NUMERIC:
                    newCell.SetCellValue(oldCell.NumericCellValue);
                    break;

                case CellType.BLANK:
                    newCell.SetCellType(CellType.BLANK);
                    break;

                case CellType.BOOLEAN:
                    newCell.SetCellValue(oldCell.BooleanCellValue);
                    break;

                case CellType.ERROR:
                    newCell.SetCellValue(oldCell.ErrorCellValue);
                    break;

                case CellType.FORMULA:
                    break;

                default:
                    break;
            }
        }

        public static void copyCellX(XSSFSheet srcSheet, XSSFSheet destSheet, int startRow, int endRow, short startCol, short endCol, int startRowDest, short startColDest)
        {
            try
            {
                XSSFRow[] srcRows = new XSSFRow[endRow - startRow + 1];
                XSSFRow[] destRows = new XSSFRow[endRow - startRow + 1];
                for (int i = startRow; i <= endRow; i++)
                {
                    XSSFRow srcRow = (XSSFRow)srcSheet.GetRow(i);
                    if (srcRow == null)
                    {
                        Console.WriteLine(" row " + i + " in source sheet does not exist");
                        return;
                    }
                    srcRows[i - startRow] = srcRow;

                    XSSFRow destRow = (XSSFRow)destSheet.GetRow(startRowDest + (i - startRow));
                    if (destRow == null)
                    {
                        destRow = (XSSFRow)destSheet.CreateRow(startRowDest + (i - startRow));
                    }

                    destRows[i - startRow] = destRow;
                }

                SortedList mergedRegions = new SortedList();

                for (int i = startRow; i <= endRow; i++)
                {
                    destRows[i - startRow].Height = srcRows[i - startRow].Height;
                }
                for (int i = startRow; i <= endRow; i++)
                {
                    int a = 1;
                    for (short j = startCol; j <= endCol; j++)
                    {
                        XSSFCell oldCell = (XSSFCell)srcRows[i - startRow].GetCell(j);

                        int idxColDestInt = startColDest + (j - startCol);
                        short idxColDest = (short)idxColDestInt;
                        XSSFCell newCell = (XSSFCell)destRows[i - startRow].GetCell(idxColDest);

                        if (oldCell != null)
                        {
                            if (newCell == null)
                            {
                                newCell = (XSSFCell)destRows[i - startRow].CreateCell(idxColDest);
                            }

                            copyCellX(oldCell, newCell, true);
                            CellRangeAddress mergedRegion = getMergedRegionX(srcSheet, srcRows[i - startRow].RowNum, (short)oldCell.ColumnIndex);
                            if (mergedRegion != null)
                            {
                                int rowFrom = destRows[i - startRow].RowNum;

                                short columnFrom = (short)(startColDest + (mergedRegion.FirstColumn - startCol));

                                int rowTo = destRows[i - startRow].RowNum + /*(mergedRegion.FirstRow-destRows[i - startRow].RowNum) +*/ (mergedRegion.LastRow - mergedRegion.FirstRow);

                                short columnTo = (short)(startColDest + (mergedRegion.FirstColumn - startColDest) + (mergedRegion.LastColumn - mergedRegion.FirstColumn));

                                CellRangeAddress newMergedRegion = new CellRangeAddress(rowFrom, rowTo, columnFrom, columnTo);

                                if (isNewMergedRegion(newMergedRegion, mergedRegions))
                                {
                                    mergedRegions.Add(mergedRegions.Count, newMergedRegion);
                                    destSheet.AddMergedRegion(newMergedRegion);
                                }
                            }
                        }
                    }
                }
                for (short j = startCol; j <= endCol; j++)
                {
                    int idxColDestInt = startColDest + (j - startCol);
                    short idxColDest = (short)idxColDestInt;
                    destSheet.SetColumnWidth(idxColDest, srcSheet.GetColumnWidth(j));
                }

            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Console.Write(e.StackTrace);

            }

        }


        public static CellRangeAddress getMergedRegionX(XSSFSheet sheet, int rowNum, short cellNum)
        {
            for (int i = 0; i < sheet.NumMergedRegions; i++)
            {
                CellRangeAddress merged = sheet.GetMergedRegion(i);
                if (merged.FirstRow <= rowNum && rowNum <= merged.LastRow && merged.FirstColumn <= cellNum && cellNum <= merged.LastColumn)
                {
                    return merged;
                }
            }

            return null;
        }

        //for XSSF , xlsx format, add by Rizal
        protected static void CopyRowX(XSSFSheet worksheet, int sourceRowNumber, int destinationRowNumber)
        {
            // Get the source / new row
            XSSFRow newRow = (XSSFRow)worksheet.GetRow(destinationRowNumber);
            XSSFRow sourceRow = (XSSFRow)worksheet.GetRow(sourceRowNumber);

            // If the row exist in destination, push down all rows by 1 else create a new row
            if (newRow != null)
            {
                worksheet.ShiftRows(destinationRowNumber, worksheet.LastRowNum, 1);
            }
            else
            {
                newRow = (XSSFRow)worksheet.CreateRow(destinationRowNumber);
            }

            // Loop through source columns to add to new row
            for (int i = 0; i < sourceRow.LastCellNum; i++)
            {
                // Grab a copy of the old/new cell
                XSSFCell oldCell = (XSSFCell)sourceRow.GetCell(i);
                XSSFCell newCell = (XSSFCell)newRow.CreateCell(i);

                // If the old cell is null jump to next cell
                if (oldCell == null)
                {
                    newCell = null;
                    continue;
                }

                // Copy style from old cell and apply to new cell
                //HSSFCellStyle newCellStyle = (HSSFCellStyle)workbook.CreateCellStyle();
                //newCellStyle.CloneStyleFrom(oldCell.CellStyle); ;
                newCell.CellStyle = oldCell.CellStyle;

                // If there is a cell comment, copy
                if (newCell.CellComment != null) newCell.CellComment = oldCell.CellComment;

                // If there is a cell hyperlink, copy
                if (oldCell.Hyperlink != null) newCell.Hyperlink = oldCell.Hyperlink;

                // Set the cell data type
                newCell.SetCellType(oldCell.CellType);
                //Set the cell data value
                switch (oldCell.CellType)
                {
                    case CellType.BLANK:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                    case CellType.BOOLEAN:
                        newCell.SetCellValue(oldCell.BooleanCellValue);
                        break;
                    case CellType.ERROR:
                        newCell.SetCellErrorValue(oldCell.ErrorCellValue);
                        break;
                    case CellType.FORMULA:
                        newCell.SetCellFormula(oldCell.CellFormula);
                        break;
                    case CellType.NUMERIC:
                        newCell.SetCellValue(oldCell.NumericCellValue);
                        break;
                    case CellType.STRING:
                        newCell.SetCellValue(oldCell.RichStringCellValue);
                        break;
                    case CellType.Unknown:
                        newCell.SetCellValue(oldCell.StringCellValue);
                        break;
                }
            }

            // If there are are any merged regions in the source row, copy to new row
            for (int i = 0; i < worksheet.NumMergedRegions; i++)
            {
                CellRangeAddress cellRangeAddress = worksheet.GetMergedRegion(i);
                if (cellRangeAddress.FirstRow == sourceRow.RowNum)
                {
                    CellRangeAddress newCellRangeAddress = new CellRangeAddress(newRow.RowNum,
                                                                                (newRow.RowNum +
                                                                                 (cellRangeAddress.FirstRow -
                                                                                  cellRangeAddress.LastRow) /*+ 1*/),
                                                                                cellRangeAddress.FirstColumn,
                                                                                cellRangeAddress.LastColumn);
                    worksheet.AddMergedRegion(newCellRangeAddress);
                }
            }

        }
        //End Added By Rizal

        //Start FID.Ridwan 2019-05-27
        protected static bool CheckSheetName(String templateFolder, String template, String sheetname, String fileExt)
        {
            bool result = true;
            var files = new FileStream(@templateFolder + "\\" + template, FileMode.Open, FileAccess.Read);

            if (fileExt == ".xls")
            {
                HSSFWorkbook workbook = new HSSFWorkbook(files);
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet(sheetname);
                if (sheet == null)
                    result = false;
            }
            else
            {
                XSSFWorkbook workbook = new XSSFWorkbook(files);
                XSSFSheet sheet = (XSSFSheet)workbook.GetSheet(sheetname);
                if (sheet == null)
                    result = false;
            }

            return result;
        }

    }

}
