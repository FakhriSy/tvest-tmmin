﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.IO;

namespace Common.STD.Base 
{
    public class CSTDFileDTO : CSTDBaseBaseDTO
    {
        public CSTDFileDTO()
		{
        }

        public String FileId { get; set; }
        public Int16 IntFileId { get; set; }
        public String FileName { get; set; }
        public byte[] FileStream { get; set; }
        public String FilePath { get; set; }
        public String Owner { get; set; }
        public DateTime CreatedDt { get; set; }
    }
}
