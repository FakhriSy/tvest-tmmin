﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Component Export Digitalization and Automation System
 * Client Name      : TMMIN
 * Program Id       : COMMON
 * Program Name     : Mandatory checking common
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID) Yusuf
 * Version          : 01.00.00
 * Creation Date    : 01/03/2018 16:00:00
 * 
 * Update history     Re-fix date       Person in charge      Description 
 *
 * Copyright(C) 2013 - . All Rights Reserved                                                                                              
 *************************************************************************************************/
using System;
using System.ComponentModel.DataAnnotations;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.STD.Validators
{
    public class CSTDMandatory : ValidationAttribute
    {
        public static string MADATORY = "MADATORY";

        public string aliasName { get; set; }

        public static string GetMandatoryField(string message)
        {
            return message.Split(':')[1];
        }

        public override string FormatErrorMessage(string name)
        {
            return MADATORY + ":" + name;
        }

        public override bool IsValid(object value)
        {
            return value != null && value.ToString() != "";
        }
    }
}
