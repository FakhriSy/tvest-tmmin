﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Component Export Digitalization and Automation System
 * Client Name      : TMMIN
 * Program Id       : COMMON
 * Program Name     : Mandatory Format common
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : FID) Yusuf
 * Version          : 01.00.00
 * Creation Date    : 01/03/2018 16:00:00
 * 
 * Update history     Re-fix date       Person in charge      Description 
 *
 * Copyright(C) 2013 - . All Rights Reserved                                                                                              
 *************************************************************************************************/
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Common.STD.Validators
{
    public class CSTDFormat : ValidationAttribute
    {
        public const int NUMERIC = 0;
        public const int DATE = 1;
        public const int BOOLEAN = 2;
        public const int STRING = 3;

        public static string FORMAT = "FORMAT";

        public string AliasName { get; set; }

        public int Type { get; set; }

        public string[] RequiredFormats { get; set; }

        public static string GetFormatField(string message)
        {
            return message.Split(':')[1];
        }

        public static string GetFormatValue(string message)
        {
            return message.Split(':')[2];
        }

        public override string FormatErrorMessage(string name)
        {
            return FORMAT + ":" + AliasName;
        }

        public override bool IsValid(object value)
        {
            bool result = false;
            string val = value.ToString();
            if (Type == NUMERIC)
            {
                long temp = 0;
                result = Int64.TryParse(val, out temp);
            }
            else if (Type == DATE)
            {
                try
                {
                    foreach (var RequiredFormat in RequiredFormats)
                    {
                        DateTime temp = DateTime.ParseExact(value.ToString(), RequiredFormat, CultureInfo.InvariantCulture);
                        result = temp != null;
                        if (result)
                        {
                            break;
                        }
                    }
                }
                catch
                {

                }
            }
            else if (Type == STRING)
            {
                try
                {
                    foreach (var RequiredFormat in RequiredFormats)
                    {
                        result = value.ToString().Trim() == RequiredFormat;
                        if (result)
                        {
                            break;
                        }
                    }
                }
                catch
                {

                }
            }

            if (result == false)
            {
                AliasName += ":" + value.ToString();
            }
            return result;
        }
    }
}