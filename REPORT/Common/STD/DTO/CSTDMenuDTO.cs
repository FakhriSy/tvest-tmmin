﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Common.STD.DTO
{
    public class CSTDMenuDTO
    {
        public string Name { get; set; }
        public string Title { get; set; }
        public string Location { get; set; }
        public Object Child { get; set; }
    }
}