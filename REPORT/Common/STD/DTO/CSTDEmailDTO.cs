﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Common.STD.DTO
{
    public class CSTDEmailDTO
    {
        public String Subject { get; set; }
        public String To { get; set; }
        public String CC { get; set; }
        public String Content { get; set; }
        public IList<String> AttachmentFile;

        /** Added by FID)Praditha, 19-05-2014
         *  Purpose: Additional attribute for insert into TB_R_SEND_EMAIL
         * **/
        public String ProcessId { get; set; }
        public String UserId { get; set; }
        public String SupplierCode { get; set; }
        public String From { get; set; }
        public String SMTP_Server { get; set; }
        public String SMTP_Port { get; set; }
    }
}
