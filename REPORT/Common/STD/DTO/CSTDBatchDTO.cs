﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;

namespace Common.STD.DTO
{
    public class CSTDBatchDTO
    {
        public String QueueNo { get; set; }
        public String RequestId { get; set; }
        public String BatchId { get; set; }
        public String RequestDate { get; set; }
        public String RequestBy { get; set; }
        public String Shell { get; set; }
        public String ProjectCode { get; set; }
        public String Parameter { get; set; }
        public String RunAs { get; set; }
        public String BatchName { get; set; }
        public String ConCurrency { get; set; }

        public CSTDBatchDTO()
        {

        }

        public CSTDBatchDTO(DataRow dr)
        {
            QueueNo = dr["QUEUE_NO"].ToString();
            RequestId = dr["REQUEST_ID"].ToString();
            BatchId = dr["BATCH_ID"].ToString();
            RequestDate = dr["REQUEST_DATE"].ToString();
            RequestBy = dr["REQUEST_BY"].ToString();
            Shell = dr["SHELL"].ToString();
            ProjectCode = dr["PROJECT_CODE"].ToString();
            Parameter = dr["PARAMETER"].ToString(); ;
            RunAs = dr["RUN_AS"].ToString();
            BatchName = dr["BATCH_NAME"].ToString();
            ConCurrency = dr["CONCURRENCY_FLAG"].ToString();
        }

    }
}
