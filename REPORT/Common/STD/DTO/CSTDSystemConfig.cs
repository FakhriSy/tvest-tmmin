﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using System.Text;
using Common.STD.DTO;
using Common.STD.Util;
namespace Common.STD.DTO
{
    [SuppressMessage("Microsoft.Design", "CA1053:StaticHolderTypesShouldNotHaveConstructors")]
    public class CSTDSystemConfig
    {
        private static CSTDXMLConfig _systemConfig = null;
        public static String GetValue(String key)
        {
            if (_systemConfig == null)
            {
                try
                {
                    _systemConfig = new CSTDXMLConfig("SystemConfig.xml");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    throw;
                }
            }
            return _systemConfig.Get(key);
        }

        public CSTDSystemConfig()
        {
            if (_systemConfig == null)
            {
                try
                {
                    _systemConfig = new CSTDXMLConfig("SystemConfig.xml");
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.StackTrace);
                    throw;
                }
            }
        }
    }
}
