﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using System.Data;
namespace Common.STD.DTO
{
    public class CSTDPLSQLDTO
    {
        public String Name { get; set; }
        public Object Data { get; set; }
        public SqlDbType SqlType { get; set; }
        public String UdtTypeName { get; set; }
        public ParameterDirection Direction { get; set; }
        public Type DtoType { get; set; }

        public int Size { get; set; }

        public void SetSize(int parameterSize)
        {
            this.Size = parameterSize;
        }

        public CSTDPLSQLDTO(String name, SqlDbType sqlType, Object data, ParameterDirection direction)
        {
            this.Name = name;
            this.SqlType = sqlType;
            this.Data = data;
            this.Direction = direction;
        }

        public CSTDPLSQLDTO(String name, SqlDbType sqlType, Object data, ParameterDirection direction, Type dtoType)
        {
            this.Name = name;
            this.SqlType = sqlType;
            this.Data = data;
            this.Direction = direction;
            this.DtoType = dtoType;
        }

        public CSTDPLSQLDTO(String name, SqlDbType sqlType, Object data, ParameterDirection direction, String udtTypeName)
        {
            this.Name = name;
            this.SqlType = sqlType;
            this.Data = data;
            this.Direction = direction;
            this.UdtTypeName = udtTypeName;
        }
    }
}
