﻿using System;
using System.Collections.Generic;
using Common.STD.Base;
using System.Reflection;
using Common.STD.Util;
using log4net;
using log4net.Config;

namespace Report
{
    public class Program
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public static void Main(string[] args)
        {
            try
            {
                //CED020300W#:#tvest.admin#:#TMMIN#:#3374#:#P;06-Mar-2024
                if (args.Length == 0) args = new String[] { "CED021600B", "tvest.admin", "TMMIN", "3418", "04-Oct-19;POS Inhouse;" };
                //if (args.Length == 0) args = new String[] { "CED021200B", "Admin", "TMMIN", "2703", ";;" };
                //if (args.Length == 0) args = new String[] { "CED030100B", "Admin", "TMMIN", "1855", "1" };
                //if (args.Length == 0) args = new String[] { "CED030200B", "System", "TMMIN", "1839", ";2" };
                //if (args.Length == 0) args = new String[] { "CED010411R", "Admin", "TAM", "0", ";;;;;;;;;" };
                //if (args.Length == 0) args = new String[] { "CED041000B", "System", "TMMIN", "15", "CED010400W_Passthru.xls" };
                //if (args.Length == 0) args = new String[] { "CED031100B", "Admin", "TAM", "0", "CED030700W (1).xls" };
                //if (args.Length == 0) args = new String[] { "CED031400R", "Admin", "TAM", "156", "16-Apr-2019;16-Apr-2019;;;;;;;;1191103087;;;;;;;;;Detail" };
                //if (args.Length == 0) args = new String[] { "CED031400R", "Admin", "TAM", "158", "01-Apr-2019;01-Apr-2019;DAY;1J;route;cycle;1H;1;0001;manifest;order;0;;;dest;lot;cas;;Summary" };
                //if (args.Length == 0) args = new String[] { "CED031100B", "System", "TMMIN", "7", "CED030700W (1) - Copy.xls" };
                //if (args.Length == 0) args = new String[] { "CED060100B", "System", "TMMIN", "190", "201809030002" };
                //if (args.Length == 0) args = new String[] { "CED050800R", "Admin", "TMMIN", "213", "213;201907001340A;;NPI215199_(HP_LaserJet_Pro_M706n);System;2" }; //NPI5CDBCB|(HP|LaserJet|Pro|M706n)
                //if (args.Length == 0) args = new String[] { "CED050800R", "Admin", "TMMIN", "696", "696;201905013780A;201905013780000005#201905013780000006#;NPI215199_(HP_LaserJet_Pro_M706n);tvest.admin;3;" };
                //if (args.Length == 0) args = new String[] { "CED031400R", "Admin", "TAM", "727", "01-04-2019;30-04-2019;;;;;;;;;;;;;;;;;Detail" };
                //if (args.Length == 0) args = new String[] { "CED030400L", "System", "TMMIN", "0", "19-03-2019;;;;;;;" };
                //if (args.Length == 0) args = new String[] { "CED040100B", "System", "TMMIN", "98", "98;1;System" };
                //if (args.Length == 0) args = new String[] { "CED040400B", "System", "TMMIN", "219", "CED010400W_CycleTime (9).xls" };
                //if (args.Length == 0) args = new String[] { "CED010404B", "System", "TMMIN", "40", "CED010400W_PartType (8).xls" };
                //if (args.Length == 0) args = new String[] { "CED040200B", "System", "TMMIN", "10", "" };
                //if (args.Length == 0) args = new String[] { "CED020100B", "System", "TMMIN", "25", "CED020100B.xls" };
                //if (args.Length == 0) args = new String[] { "CED020900B", "System", "TMMIN", "173", "POS_Prepare_20190702 (Ready) tes.xls" };
                //if (args.Length == 0) args = new String[] { "CED021100B", "System", "TMMIN", "25", "CED021100B.xls" };
                //if (args.Length == 0) args = new String[] { "CED021900L", "Admin4", "TMMIN", "705", "201909004050;S1-TVEST-LASER02" };
                //if (args.Length == 0) args = new String[] { "CED031400R", "Admin4", "TMMIN", "8383", "03-11-2023;03-11-2023;;;;;;;;;;;;;;;;;Detail" };
                //if (args.Length == 0) args = new String[] { "CED010602B", "System", "TMMIN", "8403", "CED010602W_CycleTime.xls;1" };
                //if (args.Length == 0) args = new String[] { "CED010618B", "System", "TMMIN", "60", "CED010618W_PartJundate_1.xls;PartJundate" };
                //if (args.Length == 0) args = new String[] { "CED010604B", "System", "TMMIN", "9034", "test_error.xls" };
                //if (args.Length == 0) args = new String[] { "CED010611B", "System", "TMMIN", "9984", "V;202311" };
                //if (args.Length == 0) args = new String[] { "CED010620R", "System", "TMMIN", "10012", "" };
                //if (args.Length == 0) args = new String[] { "CED021800L", "Admin4", "TAM", "10020", "202311006000;S1-TVEST-LASER01" };
                //if (args.Length == 0) args = new String[] { "CED050800R", "Admin", "TMMIN", "10019", "10019;201910000766B;202311006955000076;NPI215199_(HP_LaserJet_Pro_M706n);tvest.admin;3;" };
                //if (args.Length == 0) args = new String[] { "CED010203R", "System", "TMMIN", "1", "1;11/24/2023;A1;2" };
                //if (args.Length == 0) args = new String[] { "CED010203R", "System", "TMMIN", "1", "2;08/31/2023;;1" };
                //if (args.Length == 0) args = new String[] { "CED010203R", "System", "TMMIN", "1", "3;11/24/2023;;1" };




                //CED030400B#:#System#:#TMMIN#:#7#:#28-09-2018;ALS ;R201809283067;4;DN07;09;NOT-RECEIVE;NIGHT
                //CED021800L#:#Admin4#:#TAM#:#2275#:#201909004073;S1-TVEST-LASER01

                string functionId = args[0] ?? "";
                string userId = args[1] ?? "";
                string userLocation = args[2] ?? "";
                string processId = args[3] ?? "";

                if (!string.IsNullOrEmpty(functionId) && !string.IsNullOrEmpty(userId) && !string.IsNullOrEmpty(processId))
                {
                    Assembly assembly = typeof(Program).Assembly; // in the same assembly!
                    CSTDProperties.PropertiesPath = CSTDStringUtil.GetApplicationResourcesPath;
                    CSTDMapper.SqlMapConfigLocation = "Resources/IBatisNetConfig/SqlMapReport.config";

                    //Change File Log Location
                    XmlConfigurator.Configure();
                    log4net.Repository.Hierarchy.Hierarchy h = (log4net.Repository.Hierarchy.Hierarchy)LogManager.GetRepository();

                    CSTDDBLogUtil.CreateStartLog(CSTDDBUtil.GetNonPooledConnection, processId, null, functionId, userId, "Start Batch Process");
                    //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, args[3], "MTLS00035I", new String[] { "Execution Report" }, " Start Execution Report ");

                    log.Info("======================================================================================");
                    log.Info("Start Executing Report: " + functionId + ", Process ID: " + processId + ", Requested By: " + userId);

                    string reportType = "Report." + "IPOMS" + "." + functionId + "." + functionId + "Main";// full name - i.e. with namespace (perhaps concatenate)
                    log.Info(reportType);
                    Type type = assembly.GetType(reportType);

                    CSTDBaseReport report = (CSTDBaseReport)Activator.CreateInstance(type);

                    Dictionary<string, object> result = report.ExecuteReport(processId, functionId, userId, args, userLocation);

                    log.Info("Finished Execute Report " + functionId);
                }
                else
                {
                    log.Info("ERROR --> Execute Report " + (functionId ?? "") + " FAILED due to report argument is not setup properly!");
                    log.Info("Function ID: " + functionId + ", Process ID: " + processId + ", Requested By: " + userId);
                }
            }
            catch (Exception e1)
            {
                log.Info("Execute Report Error " + e1.Message + "\n" + e1.StackTrace + "\n" + e1.InnerException);
            }
        }
    }
}
