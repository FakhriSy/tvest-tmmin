﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.Util
{
    public sealed class CSTDPrinterUtil
    {
        #region Structures

        [StructLayout(LayoutKind.Sequential)]
        public class DOCINFOA
        {

            [MarshalAs(UnmanagedType.LPStr)]
            public string pDocName;

            [MarshalAs(UnmanagedType.LPStr)]
            public string pOutputFile;

            [MarshalAs(UnmanagedType.LPStr)]
            public string pDataType;

        }

        #endregion

        #region Methods

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall,
        SetLastError = true, ExactSpelling = true)]
        public static extern bool ClosePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall,
        SetLastError = true, ExactSpelling = true)]
        public static extern bool EndDocPrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall,
        SetLastError = true, ExactSpelling = true)]
        public static extern bool EndPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", EntryPoint = "OpenPrinterA", CallingConvention = CallingConvention.StdCall,
        CharSet = CharSet.Ansi, SetLastError = true, ExactSpelling = true)]
        public static extern bool OpenPrinter([MarshalAs(UnmanagedType.LPStr)] string szPrinter,
        out IntPtr hPrinter, IntPtr pd);

        [DllImport("winspool.Drv", EntryPoint = "StartDocPrinterA",
        CallingConvention = CallingConvention.StdCall, CharSet = CharSet.Ansi,
        SetLastError = true, ExactSpelling = true)]
        public static extern bool StartDocPrinter(IntPtr hPrinter, int level,
        [In, MarshalAs(UnmanagedType.LPStruct)] DOCINFOA di);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall,
        SetLastError = true, ExactSpelling = true)]
        public static extern bool StartPagePrinter(IntPtr hPrinter);

        [DllImport("winspool.Drv", CallingConvention = CallingConvention.StdCall,
        SetLastError = true, ExactSpelling = true)]
        public static extern bool WritePrinter(IntPtr hPrinter, IntPtr pBytes, int dwCount,
        out int dwWritten);

        public static bool SendBytesToPrinter(string szPrinterName, IntPtr pBytes, int dwCount)
        {
            int errNum;
            int dwWritten;
            IntPtr hPrinter;
            DOCINFOA di;
            bool flag;

            errNum = 0;
            dwWritten = 0;
            hPrinter = new IntPtr(0);
            di = new DOCINFOA();
            flag = false;

            di.pDocName = "RawDataPrinter";
            di.pDataType = "RAW";
            if (OpenPrinter(szPrinterName,
                out hPrinter,
                IntPtr.Zero))
            {
                if (StartDocPrinter(hPrinter, 1, di))
                {
                    if (StartPagePrinter(hPrinter))
                    {
                        flag = WritePrinter(hPrinter,
                            pBytes,
                            dwCount,
                            out dwWritten);
                        EndPagePrinter(hPrinter);
                    }

                    EndDocPrinter(hPrinter);
                }

                ClosePrinter(hPrinter);
            }

            if (!flag)
                errNum = Marshal.GetLastWin32Error();

            return flag;
        }

        public static bool SendFileToPrinter(string szPrinterName, string szFileName)
        {
            FileStream input;
            byte[] source;
            IntPtr destination;
            input = new FileStream(szFileName, FileMode.Open);
            source = new byte[input.Length];
            destination = new IntPtr(0);

            source = new BinaryReader(input).ReadBytes(source.Length);
            destination = Marshal.AllocCoTaskMem(source.Length);
            Marshal.Copy(source,
                0,
                destination,
                source.Length);

            bool flag;

            flag = SendBytesToPrinter(szPrinterName,
                destination,
                source.Length);
            Marshal.FreeCoTaskMem(destination);

            input.Close();
            input.Dispose();

            return flag;
        }

        public static bool SendStringToPrinter(string szPrinterName, string szString)
        {
            IntPtr pBytes;

            pBytes = Marshal.StringToCoTaskMemAnsi(szString);
            SendBytesToPrinter(szPrinterName,
                Marshal.StringToCoTaskMemAnsi(szString),
                szString.Length);
            Marshal.FreeCoTaskMem(pBytes);

            return true;
        }

        /** Added by FID)Praditha 03-09-2014, Set Windows Default Printer **/
        [DllImport("Winspool.drv")]
        private static extern bool SetDefaultPrinter(string printerName);

        //public static bool SetPdfPrinterAsDefault()
        //{
        //    //Get PDF Printer Name from System Master
        //    String pdfPrinterName = String.Empty;
        //    pdfPrinterName = CSTDSystemMasterHelper.GetValue("MASTER", "DEFAULT_PRINTER", "PDF");
        //    //pdfPrinterName = CSTDSystemMasterHelper.GetValue( "DEFAULT_PRINTER", "PDF");

        //    return SetDefaultPrinter(pdfPrinterName);
        //}

        #endregion
    }
}
