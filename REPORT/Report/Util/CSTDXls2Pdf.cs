﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Office.Interop.Excel;
using System.IO;
using System.Threading;
using Common.STD.Util;
using Common.STD.DTO;
using Common.STD.DAO;

namespace Report.Util
{
    public class CSTDXls2Pdf
    {
        const int _max_loop = 20;

        public static bool Convert(String source, string destination)
        {
            Application excelApplication = new Application();
            Workbook excelWorkBook = null;
            object paramMissing = Type.Missing;
            XlFixedFormatType paramExportFormat = XlFixedFormatType.xlTypePDF;
            XlFixedFormatQuality paramExportQuality = XlFixedFormatQuality.xlQualityMinimum;
            bool paramOpenAfterPublish = false;
            bool paramIncludeDocProps = true;
            bool paramIgnorePrintAreas = false;
            object paramFromPage = Type.Missing;
            object paramToPage = Type.Missing;
            try
            {
                // Open the source workbook.
                excelWorkBook = excelApplication.Workbooks.Open(source,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing, paramMissing, paramMissing,
                    paramMissing, paramMissing);

                // Save it in the target format.
                if (excelWorkBook != null)
                    excelWorkBook.ExportAsFixedFormat(paramExportFormat,
                        destination, paramExportQuality,
                        paramIncludeDocProps, paramIgnorePrintAreas, paramFromPage,
                        paramToPage, paramOpenAfterPublish,
                        paramMissing);
            }
            //catch (Exception ex)
            //{
            //    throw ex;
            //}
            finally
            {
                // Close the workbook object.
                if (excelWorkBook != null)
                {
                    excelWorkBook.Close(false, paramMissing, paramMissing);
                    excelWorkBook = null;
                }

                // Quit Excel and release the ApplicationClass object.
                if (excelApplication != null)
                {
                    excelApplication.Quit();
                    excelApplication = null;
                }

                GC.Collect();
                GC.WaitForPendingFinalizers();
                GC.Collect();
                GC.WaitForPendingFinalizers();
            }
            if (System.IO.File.Exists(destination))
            {
                return true;
            }
            else
            {
                return false;
            }
        }

        public static void isCoverted(String resultFolder, String destination, String pdfName)
        {
            String tempFileName = destination + "aca";
            File.Move(destination, tempFileName);
            String[] files = null;

            int i = 0;
            do
            {
                Thread.Sleep(1000);
                files = Directory.GetFiles(@resultFolder, pdfName);
                if (i == _max_loop)
                {
                    //throw exception
                    throw new Exception("Xls to pdf converter is not running. Please contact administrator.");
                }
            } while (files.Length == 0);
        }
    }
}
