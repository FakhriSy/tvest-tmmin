﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040100B
{
    public class CED040100BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED040100BBO ReportBO = new CED040100BBO();
            String reportBatchName = "Prepare Data for Autozoner Batch";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "Result");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "E"))
                    {
                        processSts = "1";
                        //CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD009I", new String[] { reportBatchName }, "End log with error");
                    }
                    else
                    {
                        processSts = "0";
                    }
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD008I", new String[] { reportBatchName }, "End Log Success");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD009I", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
