﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;

namespace Report.IPOMS.CED040100B
{
    public class CED040100BBO : CSTDBaseReport
    {
        CED040100BDTO ErrorDTO = new CED040100BDTO();
        String BatchName = "Prepare Data for Autozoner Batch";
        String ProcessBatchName = "Prepare Data for Autozoner Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get parameter from Batch Queue" }, "Get parameter from Batch Queue");
            //string _filePathFolder = CSTDSystemMasterHelper.GetValue("UPLOAD", "FOLDER", "EXCEL_UPLOAD_FOLDER", CSTDSystemMasterHelper.Mandatory);

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Prepare Data for Autozoner Batch";

            string ProcessId_ = reportParams[4].Split(';')[0];
            string SEQ = reportParams[4].Split(';')[1];
            string UserId_ = reportParams[4].Split(';')[2];
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get configuration settings" }, "Get configuration settings");

            //SEQ_EQ
            var msgSeq = "True";
            if (SEQ == "0")
            {
                msgSeq = "False";
            }
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022I", new String[] { "Check EO flag", msgSeq }, "Check EO flag");

            CED040100BDTO param = new CED040100BDTO();
            param.FunctionId = functionId;
            param.ProcessId = ProcessId_;
            param.UserId = UserId_;
            param.Seq = SEQ;
            IList<CED040100BDTO> insertResult = CED040100BDAO.InsertAutozoner(param);
            if (insertResult[0].IsResult.Split('|')[0] == "E")
            {
                messages.Add("Result");
                result["Result"] = "E";
                return result;
            }
            //IList<CED040100BDTO> SummarizeResult = CED040100BDAO.GetSummarize(param);
            //if (SummarizeResult[0].IsResult.Split('|')[0] == "E")
            //{
            //    messages.Add("Result");
            //    result["Result"] = "E";
            //    return result;
            //}

            return result;
        }


    }
}
