﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040100B
{
    public class CED040100BDAO
    {
        public static void DeleteStaging()
        {
            CSTDMapper.Instance().Delete("CED040100B.DeleteStaging", null);
        }

        public static IList<CED040100BDTO> InsertAutozoner(CED040100BDTO param)
        {
            return CSTDMapper.Instance().QueryForList<CED040100BDTO>("CED040100B.InsertAutozoner", param);
        }

        public static IList<CED040100BDTO> GetSummarize(CED040100BDTO param)
        {
            return CSTDMapper.Instance().QueryForList<CED040100BDTO>("CED040100B.GetSummarize", param);
        }

    }
}
