﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040100B
{
    public class CED040100BDTO
    {
        public string UserId { get; set; }
        public string ProcessId { get; set; }
        public string FunctionId { get; set; }
        public string Seq { get; set; }

        public string IsResult { get; set; }
    }
}
