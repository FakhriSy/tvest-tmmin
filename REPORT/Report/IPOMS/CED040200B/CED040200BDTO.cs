﻿using Common.STD.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040200B
{
    public class CED040200BDTO : CSTDBaseDTO
    {
        public string isResult { get; set; }
    }
}
