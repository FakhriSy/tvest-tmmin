﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040200B
{
    public class CED040200BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }
    }
}
