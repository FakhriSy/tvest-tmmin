﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040200B
{
    public class CED040200BBO : CSTDBaseReport
    {
        CED040200BDTO ErrorDTO = new CED040200BDTO();
        String BatchName = "Autozoner Result Generation Batch";
        String ProcessBatchName = "Autozoner Result Generation Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get parameter from Batch Queue" }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get configuration settings" }, "Get configuration settings");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Result of Autozoner 1" }, "Get Result of Autozoner 1");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Prepare Data for Autozoner";

            CED040200BSearchDTO searchDTO = new CED040200BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;

            IList<CED040200BDTO> rowsResult = CED040200BDAO.GetResultProcessData(searchDTO);
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("failValidation");
                result["failValidation"] = "failValidation";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Execution Add Prepare Data Autozoner Batch to Queue" }, "Add Prepare Data Autozoner Batch to Queue");
            CED040200BDAO.InsertBatch(searchDTO);

            return result;
        }
    }
}
