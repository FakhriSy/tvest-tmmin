﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040200B
{
    public class CED040200BDAO
    {
        public static IList<CED040200BDTO> GetResultProcessData(CED040200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED040200BDTO>("CED040200B.DoProcessData", searchDTO);
        }

        public static IList<CED040200BDTO> InsertBatch(CED040200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED040200BDTO>("CED040200B.InsertBatchQueue", searchDTO);
        }
    }
}
