﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020100B
{
    public class CED020100BDAO
    {
        public static void DeleteStaging()
        {
            CSTDMapper.Instance().Delete("CED020100B.DeleteStaging", null);
        }

        public static void InsertStaging(List<CED020100BDTO> dataForm)
        {
            foreach (CED020100BDTO item in dataForm)
            {
                CED020100BDTO formData = new CED020100BDTO();
                formData.SeqNo = item.SeqNo;
                formData.Line = item.Line;
                formData.Dest = item.Dest;
                formData.ModuleNo = item.ModuleNo;
                formData.CaseNo = item.CaseNo;
                formData.Shift = item.Shift;
                formData.RenbanNo = item.RenbanNo;
                formData.ModType = item.ModType;
                formData.Time = item.Time;
                formData.PackTime = item.PackTime;
                formData.Cumm = item.Cumm;
                formData.PosDate = item.PosDate;
                formData.Usage = item.Usage;
                formData.Select = item.Select;
                formData.DateFinish = item.DateFinish;
                formData.PlanFinish = item.PlanFinish;
                formData.RenbanCode = item.RenbanCode;
                formData.ShutterNo = item.ShutterNo;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                formData.IsResult = item.IsResult;
                formData.Seq = item.Seq;
                CSTDMapper.Instance().Insert("CED020100B.InsertStaging", formData);
            }
        }

        public static IList<CED020100BDTO> GetProcessData(CED020100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED020100BDTO>("CED020100B.DoProcessData", searchDTO);
        }
    }
}
