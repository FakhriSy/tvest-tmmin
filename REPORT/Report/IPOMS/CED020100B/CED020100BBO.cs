﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020100B
{
    public class CED020100BBO : CSTDBaseReport
    {
        CED020100BDTO ErrorDTO = new CED020100BDTO();
        String BatchName = "Uplaod POS Final Batch";
        String ProcessBatchName = "Uplaod POS Final Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get Parameter from Batch");
            //string _filePathFolder = CSTDSystemMasterHelper.GetValue("UPLOAD", "FOLDER", "EXCEL_UPLOAD_FOLDER", CSTDSystemMasterHelper.Mandatory);
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "FULLPATH_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            //string _filePathFolder = "C:\\Fujitsu\\tvest\\IPOS\\Report\\Upload\\";
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get Configuration Setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Uplaod POS Final Batch";

            string pathFile = reportParams[4].Split(';')[0];

            string fullpathfile = _filePathFolder + pathFile;
            if (!CheckTemplateFolder(_filePathFolder, pathFile))
            {
                logLocation = "Uploaded file not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MVPRSTD055I", new String[] { null }, "Read file, delete and insert into staging table");

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(pathFile);

            if (!CheckSheetName(_filePathFolder, pathFile, "POSFinal", fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            int rowno = 0;
            List<CED020100BDTO> listData = new List<CED020100BDTO>();
            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    CED020100BDTO dataFrom = new CED020100BDTO();
                    dataFrom.SeqNo = sheet.GetRow(row).GetCell(0).ToString();
                    dataFrom.Line = sheet.GetRow(row).GetCell(1).ToString();
                    dataFrom.Dest = sheet.GetRow(row).GetCell(2).ToString();
                    dataFrom.ModuleNo = sheet.GetRow(row).GetCell(3).ToString();
                    dataFrom.CaseNo = sheet.GetRow(row).GetCell(4).ToString();
                    dataFrom.Shift = sheet.GetRow(row).GetCell(5).ToString();
                    dataFrom.RenbanNo = sheet.GetRow(row).GetCell(6).ToString();
                    dataFrom.ModType = sheet.GetRow(row).GetCell(7).ToString();
                    dataFrom.Time = sheet.GetRow(row).GetCell(8).ToString();
                    dataFrom.PackTime = sheet.GetRow(row).GetCell(9).ToString();
                    dataFrom.Cumm = sheet.GetRow(row).GetCell(10).ToString();
                    dataFrom.PosDate = sheet.GetRow(row).GetCell(11).ToString();
                    dataFrom.Usage = sheet.GetRow(row).GetCell(12).ToString();
                    dataFrom.Select = sheet.GetRow(row).GetCell(13).ToString();
                    dataFrom.DateFinish = sheet.GetRow(row).GetCell(14).ToString();
                    dataFrom.PlanFinish = sheet.GetRow(row).GetCell(15).ToString();
                    dataFrom.RenbanCode = sheet.GetRow(row).GetCell(16).ToString();
                    dataFrom.ShutterNo = sheet.GetRow(row).GetCell(17).ToString();
                    dataFrom.IsResult = "N";
                    dataFrom.UserName = userId;
                    dataFrom.ProcessId = processId;
                    dataFrom.Seq = rowno.ToString();

                    listData.Add(dataFrom);
                }
            }

            CED020100BDAO.InsertStaging(listData);

            CED020100BSearchDTO searchDTO = new CED020100BSearchDTO();
            searchDTO.ProcessId = processId;
            searchDTO.Username = userId;

            IList<CED020100BDTO> rowsProcess = CED020100BDAO.GetProcessData(searchDTO);
            if (rowsProcess[0].IsResult == "Y")
            {
                messages.Add("failValidation");
                result["failValidation"] = "failValidation";
                return result;
            }

            return result;
        }
    }
}
