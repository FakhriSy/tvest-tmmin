﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020100B
{
    public class CED020100BSearchDTO
    {
        public string ProcessId { get; set; }
        public string Username { get; set; }
    }
}
