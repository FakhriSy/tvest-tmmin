﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED010404B
{
    public class CED010404BDTO : CSTDBaseDTO
    {
        public string Proccess { get; set; }
        public string PartNo { get; set; }
        public string DockCd { get; set; }
        public string PackingLineCd { get; set; }
        public string BoxingFlag { get; set; }
        public string PickingFlag { get; set; }
        public string StackingFlag { get; set; }
        public string FlowRackFlag { get; set; }
        public string ValidFrom { get; set; }
        public string LineCd { get; set; }
        public string PatternCd { get; set; }

        public string ProcessId { get; set; }
        public string RowNo { get; set; }
        public string UserName { get; set; }
        public string isResult { get; set; }
    }
}
