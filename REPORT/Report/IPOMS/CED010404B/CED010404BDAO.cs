﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010404B
{
    public class CED010404BDAO
    {
        public static void DeleteStaging(CED010404BDTO dataForm)
        {
            CSTDMapper.Instance().Delete("CED010404B.DeleteStaging", dataForm);
        }

        public static void InsertDataUpload(List<CED010404BDTO> dataForm)
        {
            foreach (CED010404BDTO item in dataForm)
            {
                CED010404BDTO formData = new CED010404BDTO();
                formData.Proccess = item.Proccess;
                formData.PartNo = item.PartNo;
                formData.DockCd = item.DockCd;
                formData.PackingLineCd = item.PackingLineCd;
                formData.BoxingFlag = item.BoxingFlag;
                formData.PickingFlag = item.PickingFlag;
                formData.StackingFlag = item.StackingFlag;
                formData.FlowRackFlag = item.FlowRackFlag;
                formData.ValidFrom = item.ValidFrom;
                formData.LineCd = item.LineCd;
                formData.PatternCd = item.PatternCd;
                formData.RowNo = item.RowNo;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                CSTDMapper.Instance().Insert("CED010404B.InsertDataUpload", formData);
            }
        }

        public static IList<CED010404BDTO> GetResultData(CED010404BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010404BDTO>("CED010404B.DoResultData", searchDTO);
        }
    }
}
