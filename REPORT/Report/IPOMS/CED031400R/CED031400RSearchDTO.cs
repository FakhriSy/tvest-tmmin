﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED031400R
{
    public class CED031400RSearchDTO
    {
        public string DeliveryDateFrom { get; set; }
        public string DeliveryDateTo { get; set; }
        public string ActualReceive { get; set; }
        public string Shift { get; set; }
        public string PhysicalDock { get; set; }
        public string RouteName { get; set; }
        public string RouteCycle { get; set; }
        public string LogicalDock { get; set; }
        public string TS { get; set; }
        public string Supplier { get; set; }
        public string ManifestNo { get; set; }
        public string OrderNo { get; set; }
        public string Status { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string Dock { get; set; }
        public string PartNo { get; set; }
        public string Summary { get; set; }
        public string ProcessID { get; set; }
        public string FunctionId { get; set; }
        public string UserName { get; set; }

        public string KanbanId { get; set; }
        public string Type { get; set; }
    }
}
