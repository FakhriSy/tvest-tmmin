﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED031400R
{
    public class CED031400RDTO
    {
        public string DeliveryDate { get; set; }
        public string Shift { get; set; }
        public string PhysicalDock { get; set; }
        public string LogicalDock { get; set; }
        public string TS { get; set; }
        public string Route { get; set; }
        public string Cycle { get; set; }
        public string LP { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string OrderNo { get; set; }
        public string ManifestNo { get; set; }
        public string KanbanId { get; set; }
        public string PartNo { get; set; }
        public string PlanReceiveOriginal { get; set; }
        public string PlanReceiveUpload { get; set; }
        public string ActualReceive { get; set; }
        public string ModType { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string KanbanPlan { get; set; }
        public string KanbanAct { get; set; }
        public string Status { get; set; }
        public string PlanCatchUp { get; set; }
        public string Problem { get; set; }
        public string PLaneNo { get; set; }
    }
}
