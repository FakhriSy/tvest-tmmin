﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;

namespace Report.IPOMS.CED031400R
{
    public class CED031400RBO : CSTDBaseReport
    {
        CED031400RDTO ErrorDTO = new CED031400RDTO();
        String BatchName = "Okamochi Detail Generation Report";
        String ProcessBatchName = "Okamochi Detail Generation Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        String templateName = "CED031400R.xls";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { "Get Parameters" }, "Get parameter");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Okamochi Detail Generation Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            Console.WriteLine(reportParams.Length);

            CED031400RSearchDTO searchDTO = new CED031400RSearchDTO();
            searchDTO.DeliveryDateFrom = reportParams[4].Split(';')[0];
            searchDTO.DeliveryDateTo = reportParams[4].Split(';')[1];
            searchDTO.Shift = reportParams[4].Split(';')[2];
            searchDTO.PhysicalDock = reportParams[4].Split(';')[3];
            searchDTO.RouteName = reportParams[4].Split(';')[4];
            searchDTO.RouteCycle = reportParams[4].Split(';')[5];
            searchDTO.LogicalDock = reportParams[4].Split(';')[6];
            searchDTO.TS = reportParams[4].Split(';')[7];
            searchDTO.Supplier = reportParams[4].Split(';')[8];
            searchDTO.ManifestNo = reportParams[4].Split(';')[9];
            searchDTO.OrderNo = reportParams[4].Split(';')[10];
            searchDTO.Status = reportParams[4].Split(';')[11];
            searchDTO.KanbanId = reportParams[4].Split(';')[12];
            searchDTO.PartNo = reportParams[4].Split(';')[13];
            //searchDTO.Summary = reportParams[4].Split(';')[14];
            //searchDTO.ProcessID = reportParams[4].Split(';')[15];
            //searchDTO.UserName = reportParams[4].Split(';')[16];
            searchDTO.FunctionId = functionId;

            searchDTO.Dest = reportParams[4].Split(';')[14];
            searchDTO.Lot = reportParams[4].Split(';')[15];
            searchDTO.Case = reportParams[4].Split(';')[16];
            searchDTO.Dock = reportParams[4].Split(';')[17];
            searchDTO.Type = reportParams[4].Split(';')[18];

            searchDTO.ActualReceive = reportParams[4].Split(';')[19];

            if (searchDTO.Summary == "Summary")
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD021I", new String[] { }), "Not Function");
                messages.Add("Not Function");
                result["Result"] = "I";
                return result;
            }
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { "Get Excel Template" }, "Get configuration setting");

            #region Check report template

            //logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder + templateName }), "Template not found");
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder + templateName }));
                result["Result"] = "E";
                return result;
            }
            #endregion
            //try
            //{
            IList<CED031400RDTO> rows = CED031400RDAO.GetResultData(searchDTO);
            if (rows.Count < 1)
            {
                //ini
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD052E", new String[] { "Retrieve Data", "Table" }, "Okamochi Detail data not found");
                result["Result"] = "E";
                messages.Add("fail");
                return result;
            }
            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");
            //style
            HSSFCellStyle iStyleLeft = (HSSFCellStyle)workbook.CreateCellStyle();
            iStyleLeft.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleLeft.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleLeft.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleLeft.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleLeft.Alignment = NPOI.SS.UserModel.HorizontalAlignment.LEFT;
            HSSFCellStyle iStyleRight = (HSSFCellStyle)workbook.CreateCellStyle();
            iStyleRight.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleRight.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleRight.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleRight.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
            iStyleRight.Alignment = NPOI.SS.UserModel.HorizontalAlignment.RIGHT;

            //start looping
            int startRow = 9, rowIndex = startRow;
            long numberOfRow = rows.Count;
            long modOfRow = rows.Count % numberOfRow;
            long rowToRemove = numberOfRow - modOfRow;
            long startIndexRowToRemove = startRow + modOfRow;
            double rowHeight = 11.25;
            int sheetCount = 0;
            int i_seq = 0;

            CSTDNPOIUtil.SetCellValue(sheet, 3, 3, ": " + userId);
            CSTDNPOIUtil.SetCellValue(sheet, 4, 3, ": " + DateTime.Now.ToString("dd-MMM-yyyy HH:mm"));
            CSTDNPOIUtil.SetCellValue(sheet, 5, 3, ": " + rows.Count().ToString());

            foreach (CED031400RDTO row in rows)
            {
                i_seq++;
                if (rowIndex == 65535 || sheetCount == 0)
                {
                    sheet = (HSSFSheet)workbook.CloneSheet(0);
                    sheetCount++;

                    startRow = 9;
                    rowIndex = startRow;
                    numberOfRow = rows.Count;
                    modOfRow = rows.Count % numberOfRow;
                    rowToRemove = numberOfRow - modOfRow;
                    startIndexRowToRemove = startRow + modOfRow;
                    rowHeight = 11.25;
                    workbook.SetSheetName(sheetCount, sheetCount.ToString());
                }
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }


                //fill up column
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 0, i_seq, iStyleRight);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 1, row.DeliveryDate, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 2, row.Shift, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 3, row.PhysicalDock, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 4, row.LogicalDock, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 5, row.TS, iStyleRight);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 6, row.Route, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 7, row.Cycle, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 8, row.LP, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 9, row.SuppCode, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 10, row.SuppName, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 11, row.OrderNo, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 12, row.ManifestNo, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 13, row.PLaneNo, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 14, row.KanbanId, iStyleRight);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 15, row.PartNo, iStyleRight);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 16, row.PlanReceiveOriginal, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 17, row.PlanReceiveUpload, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 18, row.ActualReceive, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 19, row.ModType, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 20, row.Dest, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 21, row.Lot, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 22, row.Case, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 23, row.KanbanPlan, iStyleRight);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 24, row.KanbanAct, iStyleRight);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 25, row.Status, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 26, row.PlanCatchUp, iStyleLeft);
                CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 27, row.Problem, iStyleLeft);
                ++rowIndex;
            }

            workbook.SetSheetHidden(0, true);
            workbook.SetActiveSheet(1);

            result["workbook"] = workbook;
            //}
            //catch (Exception e)
            //{
            //    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD052E", new String[] { e.Message }, "Okamochi Detail data not found");
            //    messages.Add("Result");
            //    result["Result"] = "E";
            //    return result;
            //}
            return result;
        }


    }
}
