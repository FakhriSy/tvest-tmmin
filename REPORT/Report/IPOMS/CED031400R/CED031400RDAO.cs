﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED031400R
{
    public class CED031400RDAO
    {
        public static IList<CED031400RDTO> GetResultData(CED031400RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED031400RDTO>("CED031400R.DoResultData", searchDTO);
        }
    }
}
