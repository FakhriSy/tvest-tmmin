﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.IO;

namespace Report.IPOMS.CED031400R
{
    public class CED031400RMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED031400RBO ReportBO = new CED031400RBO();
            String reportBatchName = "Okamochi Detail Generation Report";
            String ExcelName = "OkamochiDetailReport_";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD032I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "Result");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "E"))
                    {
                        processSts = "1";
                        CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD009I", new String[] { reportBatchName }, "End log with error");
                    }
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "I"))
                    {
                        processSts = "0";
                        CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD008I", new String[] { reportBatchName }), "End Log");
                    }
                }
                else
                {
                    Console.WriteLine(reportParameters.Length);
                    String targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
                    //String reportName = reportBatchName;
                    NPOI.HSSF.UserModel.HSSFWorkbook workbook = (NPOI.HSSF.UserModel.HSSFWorkbook)result["workbook"];
                    String excelFileName = ExcelName + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
                    //String excelFileName = ExcelName + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                    String excelFullPathFileName = targetDir + excelFileName;
                    FileStream file = new FileStream(excelFullPathFileName, FileMode.Create);
                    workbook.Write(file);
                    file.Close();
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD024I", new String[] { reportBatchName }, "Generated excel successfully");

                    CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, excelFileName);
                    processSts = "0";
                    Console.WriteLine("Report successfully saved into file: " + targetDir + excelFileName);
                    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD008I", new String[] { reportBatchName }), "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
