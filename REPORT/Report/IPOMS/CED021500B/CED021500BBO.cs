﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Report.IPOMS.CED021500B
{
    public class CED021500BBO : CSTDBaseReport
    {
        CED021500BDTO ErrorDTO = new CED021500BDTO();
        String BatchName = "POS Jundate Report";
        String ProcessBatchName = "POS Jundate Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "POS Jundate Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            Console.WriteLine(reportParams.Length);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            var xmlConfig = new CSTDXMLConfig(@"SystemConfig.xml");

            string[] commandParams = reportParams[4].Split(';');

            var settings = CED021500BDAO.GetReportPOSJundateSettings();
            var wipCount = settings.FirstOrDefault(x => x.SYS_CD == "WIP_COUNT")?.SYS_VAL;

            CED021500BSearchDTO searchDTO = new CED021500BSearchDTO()
            {
                ProdDate = commandParams[1],
                WIP_COUNT = wipCount ?? "0"
            };

            //searchDTO.ProdLine = reportParams[4].Split(';')[2];

            string prodLine = string.Empty;

            if (commandParams[4] == "1" || commandParams[4] == "")
            {
                prodLine = "ASSY";
            }
            
            if (commandParams[4] == "2")
            {
                prodLine = "WELDING";
            }

            string sheetTitle = $"JUNDATE SUPPLY {prodLine}";

            string shiftOption = commandParams[3];

            #region Fill up report
            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet templateSheet = (HSSFSheet)workbook.GetSheet("Report");

            int sheetCount = 0;

            HSSFSheet sheet;
            string shiftName;

            List<CED021500BDTO> dayRows = new List<CED021500BDTO>();
            List<CED021500BDTO> dayWipRows = new List<CED021500BDTO>();

            if (string.IsNullOrEmpty(shiftOption) || shiftOption == "1")
            {
                searchDTO.shift = "1";
                searchDTO.WIP_FLAG = "";

                dayRows.AddRange(CED021500BDAO.GetResultData(searchDTO));

                searchDTO.WIP_FLAG = "1";
                dayWipRows.AddRange(CED021500BDAO.GetResultData(searchDTO));

                sheet = (HSSFSheet)workbook.CloneSheet(0);
                sheetCount++;

                shiftName = "Day";
                workbook.SetSheetName(sheetCount, shiftName);

                GenerateExcel(sheet, dayRows, dayWipRows, searchDTO.ProdDate, sheetTitle, shiftName);
            }

            List<CED021500BDTO> nightRows = new List<CED021500BDTO>();
            List<CED021500BDTO> nightWipRows = new List<CED021500BDTO>();

            if (string.IsNullOrEmpty(shiftOption) || shiftOption == "2")
            {
                searchDTO.shift = "2";
                searchDTO.WIP_FLAG = "";

                nightRows.AddRange(CED021500BDAO.GetResultData(searchDTO));

                searchDTO.WIP_FLAG = "1";
                nightWipRows.AddRange(CED021500BDAO.GetResultData(searchDTO));

                sheet = (HSSFSheet)workbook.CloneSheet(0);
                sheetCount++;

                shiftName = "Night";
                workbook.SetSheetName(sheetCount, shiftName);

                GenerateExcel(sheet, nightRows, nightWipRows, searchDTO.ProdDate, sheetTitle, shiftName);
            }

            workbook.SetSheetHidden(0, true);
            workbook.SetActiveSheet(1);
            #endregion

            result["workbook"] = workbook;

            return result;
        }

        private void GenerateExcel(
            HSSFSheet sheet, 
            List<CED021500BDTO> data, 
            List<CED021500BDTO> wipData,
            string prodDate,
            string sheetTitle,
            string shiftName)
        {
            int startRow = 11;
            int rowIndex = startRow;
            int numberOfRow = data.Count;
            int modOfRow = numberOfRow > 0 ? data.Count % numberOfRow : 0;
            int rowHeight = 24;
            int seqNo = 0;

            CSTDNPOIUtil.SetCellValue(sheet, 0, 10, sheetTitle);
            CSTDNPOIUtil.SetCellValue(sheet, 7, 2, prodDate);
            CSTDNPOIUtil.SetCellValue(sheet, 7, 4, DateTime.Parse(prodDate).ToString("dddd", new System.Globalization.CultureInfo("id-ID")));
            CSTDNPOIUtil.SetCellValue(sheet, 6, 23, shiftName);

            foreach (CED021500BDTO row in data)
            {
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }

                seqNo++;
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, seqNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.shutterNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.dest);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.lotModuleNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.caseNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.shift);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.renban?.TrimEnd());
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.modType);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.moduleCode);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.dolly1);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.dolly2);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.dolly3);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.dolly4);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.dolly5);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.dolly6);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.dolly7);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.dolly8);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.dolly9);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.line);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.planStart);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 20, row.cycleTime);
                ++rowIndex;
            }

            startRow = rowIndex + 2;
            rowIndex = startRow;

            foreach (CED021500BDTO row in wipData)
            {
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }

                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.shutterNoWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.destWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.lotModuleNoWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.caseNoWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.shiftWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.renbanWip?.TrimEnd());
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.modTypeWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.moduleCodeWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.dolly1Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.dolly2Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.dolly3Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.dolly4Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.dolly5Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.dolly6Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.dolly7Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.dolly8Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.dolly9Wip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.lineWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.planStartWip);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 20, row.cycleTimeWip);
                ++rowIndex;
            }
        }
    }
}
