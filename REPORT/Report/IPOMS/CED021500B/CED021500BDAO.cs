﻿using System.Collections.Generic;
using Common.STD.Util;
using Report.IPOMS.Common;

namespace Report.IPOMS.CED021500B
{
    public class CED021500BDAO
    {
        public static IList<CED021500BDTO> GetResultData(CED021500BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021500BDTO>("CED021500B.DoResultData", searchDTO);
        }

        public static IList<MasterSystemDTO> GetReportPOSJundateSettings()
        {
            return CSTDMapper.Instance().QueryForList<MasterSystemDTO>("CED021500B.GetReportPOSJundateSettings", null);
        }
    }
}
