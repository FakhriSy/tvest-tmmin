﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021500B
{
    public class CED021500BSearchDTO
    {
        public string WIP_COUNT { get; set; }
        public string WIP_FLAG { get; set; }
        public string ProdDate { get; set; }
        public string ProdDayName { get; set; }
        //public string ProdLine { get; set; }
        public string no { get; set; }
        public string shutterNo { get; set; }
        public string dest { get; set; }
        public string lotModuleNo { get; set; }
        public string caseNo { get; set; }
        public string shift { get; set; }
        public string renban { get; set; }
        public string modType { get; set; }
        public string moduleCode { get; set; }

        public string dolly1 { get; set; }
        public string dolly2 { get; set; }
        public string dolly3 { get; set; }
        public string dolly4 { get; set; }
        public string dolly5 { get; set; }
        public string dolly6 { get; set; }
        public string dolly7 { get; set; }
        public string dolly8 { get; set; }
        public string dolly9 { get; set; }
        public string line { get; set; }
        public string planStart { get; set; }
        public string cycleTime { get; set; }
        public string shutterNoWip { get; set; }
        public string destWip { get; set; }
        public string lotModuleNoWip { get; set; }
        public string caseNoWip { get; set; }
        public string shiftWip { get; set; }
        public string renbanWip { get; set; }
        public string modTypeWip { get; set; }
        public string moduleCodeWip { get; set; }
        public string dolly1Wip { get; set; }
        public string dolly2Wip { get; set; }
        public string dolly3Wip { get; set; }
        public string dolly4Wip { get; set; }
        public string dolly5Wip { get; set; }
        public string dolly6Wip { get; set; }
        public string dolly7Wip { get; set; }
        public string dolly8Wip { get; set; }
        public string dolly9Wip { get; set; }
        public string lineWip { get; set; }
        public string planStartWip { get; set; }
        public string cycleTimeWip { get; set; }
    }
}
