﻿using System.Collections.Generic;
using Common.STD.Util;

namespace Report.IPOMS.CED010611B
{
    public class CED010611BDAO
    {
        public static IList<CED010611BDTO> GetResultProcessData(CED010611BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010611BDTO>("CED010611B.DoProcessData", sDTO);
        }
    }
}
