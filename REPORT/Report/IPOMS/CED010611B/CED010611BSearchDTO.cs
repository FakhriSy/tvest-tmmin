﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010611B
{
    public class CED010611BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserId {  get; set; }
        public string VanningPlantCd {  get; set; }
        public string VanningMonth {  get; set; }
    }
}
