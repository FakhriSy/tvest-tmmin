﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using Common.STD.Base;
using Common.STD.Util;

namespace Report.IPOMS.CED010611B
{
    public class CED010611BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<String, Object>();
            CED010611BBO ReportBO = new CED010611BBO();
            string reportBatchName = "Release DVP Batch";
            string processSts = "";

            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start Process");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Calculate(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                        Console.WriteLine(message);

                    var resultDataChecking = result.SingleOrDefault(d => d.Key == "EndWithError");
                    processSts = (resultDataChecking.Value != null && resultDataChecking.Value.ToString() == "EndWithError") ? "1" : "2";
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD007I", new String[] { reportBatchName }, "End Log");
                }

            }
            catch (Exception e)
            {
                processSts = "2";
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }

            return result;
        }
    }
}
