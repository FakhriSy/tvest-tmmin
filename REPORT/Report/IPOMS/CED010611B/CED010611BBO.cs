﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using IPOS.Models.Common;

namespace Report.IPOMS.CED010611B
{
    public class CED010611BBO : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<string, object> Calculate(SqlConnection con, string processId, string functionId, string userId, string[] reportParams, string userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            CED010611BSearchDTO sDTO = new CED010611BSearchDTO
            {
                ProcessId = int.Parse(processId),
                UserId = userId,
                VanningPlantCd = reportParams[4].Split(';')[0],
                VanningMonth = reportParams[4].Split(';')[1]
            };

            IList<CED010611BDTO> rowsResult = CED010611BDAO.GetResultProcessData(sDTO);
            if (rowsResult[0].isResult == "ERROR")
            {
                messages.Add("EndWithError");
                result["EndWithError"] = "EndWithError";
                return result;
            }

            return result;
        }
    }
}
