﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;

namespace Report.IPOMS.CED021100B
{
    public class CED021100BBO : CSTDBaseReport
    {
        CED021100BDTO ErrorDTO = new CED021100BDTO();
        String BatchName = "Upload POS Jundate Batch";
        String ProcessBatchName = "Upload POS Jundate Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { "Get Parameters" }, "Get Parameter from Batch");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Uplaod POS Jundate Batch";

            //string ProsesID = reportParams[4].Split(';')[0];
            //string User = reportParams[4].Split(';')[1];
            //string filename = reportParams[4].Split(';')[2];

            //from screen
            string filename = reportParams[4].Split(';')[0];
            string ProsesID = processId;
            string User = userId;

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get Configuration Setting");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "FULLPATH_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            //string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "TEMP_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { fullpathfile }, "file Not Found");
                messages.Add("Result");
                result["Result"] = "E";
                return result;
            }

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MVPRSTD055I", new String[] { null }, "Read file, delete and insert into staging table");
            CED021100BDAO.DeleteStaging();
            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, "POSJundate", fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }

            var iMandatory = "";
            var iFormat = "";
            int rowno = 0;
            List<CED021100BDTO> listData = new List<CED021100BDTO>();
            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    CED021100BDTO dataFrom = new CED021100BDTO();

                    dataFrom.ProcessId = ProsesID;
                    dataFrom.UserName = User;
                    
                    if (sheet.GetRow(row).GetCell(0) != null)
                    {
                        dataFrom.Line = sheet.GetRow(row).GetCell(0).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(1) != null)
                    {
                        dataFrom.ProdDate = sheet.GetRow(row).GetCell(1).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(2) != null)
                    {
                        dataFrom.ShuterNo = sheet.GetRow(row).GetCell(2).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(3) != null)
                    {
                        dataFrom.Dest = sheet.GetRow(row).GetCell(3).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(4) != null)
                    {
                        dataFrom.ModuleNo = sheet.GetRow(row).GetCell(4).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(5) != null)
                    {
                        dataFrom.CaseNo = sheet.GetRow(row).GetCell(5).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(6) != null)
                    {
                        dataFrom.Shift = sheet.GetRow(row).GetCell(6).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(7) != null)
                    {
                        dataFrom.RenbanNo = sheet.GetRow(row).GetCell(7).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(8) != null)
                    {
                        dataFrom.ModType = sheet.GetRow(row).GetCell(8).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(9) != null)
                    {
                        dataFrom.Dolly1 = sheet.GetRow(row).GetCell(9).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(10) != null)
                    {
                        dataFrom.Dolly2 = sheet.GetRow(row).GetCell(10).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(11) != null)
                    {
                        dataFrom.Dolly3 = sheet.GetRow(row).GetCell(11).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(12) != null)
                    {
                        dataFrom.Dolly4 = sheet.GetRow(row).GetCell(12).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(13) != null)
                    {
                        dataFrom.Dolly5 = sheet.GetRow(row).GetCell(13).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(14) != null)
                    {
                        dataFrom.Dolly6 = sheet.GetRow(row).GetCell(14).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(15) != null)
                    {
                        dataFrom.Dolly7 = sheet.GetRow(row).GetCell(15).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(16) != null)
                    {
                        dataFrom.Dolly8 = sheet.GetRow(row).GetCell(16).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(17) != null)
                    {
                        dataFrom.Dolly9 = sheet.GetRow(row).GetCell(17).ToString();
                    }
                    
                    if (sheet.GetRow(row).GetCell(18) != null)
                    {
                        dataFrom.PlanStart = sheet.GetRow(row).GetCell(18).ToString();
                    }

                    if (sheet.GetRow(row).GetCell(19) != null)
                    {
                        dataFrom.CT = sheet.GetRow(row).GetCell(19).ToString();
                    }

                    //if (sheet.GetRow(row).GetCell(0) != null)
                    //{
                    //    dataFrom.Line = sheet.GetRow(row).GetCell(0).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(1) != null)
                    //{
                    //    dataFrom.Dest = sheet.GetRow(row).GetCell(1).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(2) != null)
                    //{
                    //    dataFrom.ModuleNo = sheet.GetRow(row).GetCell(2).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(3) != null)
                    //{
                    //    dataFrom.CaseNo = sheet.GetRow(row).GetCell(3).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(4) != null)
                    //{
                    //    dataFrom.Shift = sheet.GetRow(row).GetCell(4).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(5) != null)
                    //{
                    //    dataFrom.RenbanNo = sheet.GetRow(row).GetCell(5).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(6) != null)
                    //{
                    //    dataFrom.ModType = sheet.GetRow(row).GetCell(6).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(7) != null)
                    //{
                    //    dataFrom.Time = sheet.GetRow(row).GetCell(7).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(8) != null)
                    //{
                    //    dataFrom.PackTime = sheet.GetRow(row).GetCell(8).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(9) != null)
                    //{
                    //    dataFrom.Cumm = sheet.GetRow(row).GetCell(9).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(10) != null)
                    //{
                    //    dataFrom.PackDate = sheet.GetRow(row).GetCell(10).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(11) != null)
                    //{
                    //    dataFrom.Usage = sheet.GetRow(row).GetCell(11).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(12) != null)
                    //{
                    //    dataFrom.Select = sheet.GetRow(row).GetCell(12).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(13) != null)
                    //{
                    //    dataFrom.DateFinish = sheet.GetRow(row).GetCell(13).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(14) != null)
                    //{
                    //    dataFrom.TimeFinish = sheet.GetRow(row).GetCell(14).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(15) != null)
                    //{
                    //    dataFrom.RenbanCode = sheet.GetRow(row).GetCell(15).ToString();
                    //}
                    //if (sheet.GetRow(row).GetCell(16) != null)
                    //{
                    //    dataFrom.ShutterNo = sheet.GetRow(row).GetCell(16).ToString();
                    //}
                    //dataFrom.ControlModuleNo = "";

                    dataFrom.IsError = "N";
                    //string params_0 = "";
                    //string params_1 = "";
                    //string params_2 = "Line:" + dataFrom.Line + ", Dest:" + dataFrom.Dest + ", Module No:" + dataFrom.ModuleNo + ", Case No:" + dataFrom.CaseNo + ", Shift:" + dataFrom.Shift + ", Renban No:" + dataFrom.RenbanNo + ", Mod Type:" + dataFrom.ModType + ", Pack Date:" + dataFrom.PackDate;
                    //#region Mandatory Checking
                    //if (iMandatory =="")
                    //{
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD018I", new String[] { "Mandatory Checking" }, "Exec Mandatory validation");
                    //}
                    //iMandatory = "1";
                    //if (dataFrom.Line == null || dataFrom.Line == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "Line";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Dest == null || dataFrom.Dest == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "Dest";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.ModuleNo == null || dataFrom.ModuleNo == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "ModuleNo";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.CaseNo == null || dataFrom.CaseNo == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "CaseNo";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Shift == null || dataFrom.Shift == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "Shift";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.RenbanNo == null || dataFrom.RenbanNo == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "RenbanNo";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.ModType == null || dataFrom.ModType == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "ModType";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Time == null || dataFrom.Time == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "Time";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.PackTime == null || dataFrom.PackTime == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "PackTime";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.PackDate == null || dataFrom.PackDate == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "PackDate";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Usage == null || dataFrom.Usage == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "Usage";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Select == null || dataFrom.Select == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "Select";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.DateFinish == null || dataFrom.DateFinish == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "DateFinish";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.TimeFinish == null || dataFrom.TimeFinish == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "TimeFinish";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.RenbanCode == null || dataFrom.RenbanCode == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "RenbanCode";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.ControlModuleNo == null || dataFrom.ControlModuleNo == "")
                    //{
                    //    dataFrom.IsError = "1";
                    //    params_0 = "ControlModuleNo";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD047E", new String[] { params_0, params_2 }, "Mandatory Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //#endregion

                    //#region Format checking
                    //if (iFormat =="")
                    //{
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD020I", new String[] { "Format checking" }, "Exec Format Checking validation");
                    //}
                    //iFormat = "1";
                    //if (dataFrom.Line.Length > 2)
                    //{
                    //    params_0 = "Line";
                    //    params_1 = dataFrom.Line;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Dest.Length > 4)
                    //{
                    //    params_0 = "Dest";
                    //    params_1 = dataFrom.Dest;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.ModuleNo.Length > 6)
                    //{
                    //    params_0 = "ModuleNo";
                    //    params_1 = dataFrom.ModuleNo;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.CaseNo.Length > 3)
                    //{
                    //    params_0 = "CaseNo";
                    //    params_1 = dataFrom.CaseNo;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Shift.Length > 1 || !CheckFormatNumber(dataFrom.Shift))
                    //{
                    //    params_0 = "Shift";
                    //    params_1 = dataFrom.Shift;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.RenbanNo.Length > 9)
                    //{
                    //    params_0 = "RenbanNo";
                    //    params_1 = dataFrom.RenbanNo;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.ModType.Length > 2)
                    //{
                    //    params_0 = "ModType";
                    //    params_1 = dataFrom.ModType;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Time.Length > 5 || !CheckFormatTime(dataFrom.Time))
                    //{
                    //    params_0 = "Time";
                    //    params_1 = dataFrom.Time;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.PackTime.Length > 2 || !CheckFormatNumber(dataFrom.PackTime))
                    //{
                    //    params_0 = "PackTime";
                    //    params_1 = dataFrom.PackTime;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.PackDate.Length > 11 || !CheckFormatDate(dataFrom.PackDate))
                    //{
                    //    params_0 = "PackDate";
                    //    params_1 = dataFrom.PackDate;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Usage.Length > 4)
                    //{
                    //    params_0 = "Usage";
                    //    params_1 = dataFrom.Usage;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Select.Length > 1)
                    //{
                    //    params_0 = "Select";
                    //    params_1 = dataFrom.Select;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.Select != "0")
                    //{
                    //    if (dataFrom.Select != "1")
                    //    {
                    //        params_0 = "Select";
                    //        params_1 = dataFrom.Select;
                    //        dataFrom.IsError = "1";
                    //        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //        messages.Add("Result");
                    //        result["Result"] = "E";
                    //        return result;
                    //    }
                    //}
                    //if (dataFrom.DateFinish.Length > 11 || !CheckFormatDate(dataFrom.DateFinish))
                    //{
                    //    params_0 = "DateFinish";
                    //    params_1 = dataFrom.DateFinish;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.TimeFinish.Length > 5)
                    //{
                    //    params_0 = "TimeFinish";
                    //    params_1 = dataFrom.TimeFinish;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.RenbanCode.Length > 1)
                    //{
                    //    params_0 = "RenbanCode";
                    //    params_1 = dataFrom.RenbanCode;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //if (dataFrom.ControlModuleNo.Length > 12)
                    //{
                    //    params_0 = "ControlModuleNo";
                    //    params_1 = dataFrom.ControlModuleNo;
                    //    dataFrom.IsError = "1";
                    //    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD049E", new String[] { params_0, params_1, params_2 }, "Format Checking");
                    //    messages.Add("Result");
                    //    result["Result"] = "E";
                    //    return result;
                    //}
                    //#endregion

                    listData.Add(dataFrom);
                }
            }


            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD027I", new String[] { null }, "Executing insert staging data into target table process");
            CED021100BDAO.InsertStaging(listData);

            CED021100BSearchDTO searchDTO = new CED021100BSearchDTO();
            searchDTO.FunctionId = functionId;
            searchDTO.ProcessId = ProsesID;
            searchDTO.UserName = User;
            IList<CED021100BDTO> ProsesResult = CED021100BDAO.GetProcessData(searchDTO);
            //if (ProsesResult[0].IsResult.Split('|')[0] == "E")
            //{
            //    messages.Add("Result");
            //    result["Result"] = "E";
            //    return result;
            //}
            //IList<CED021100BDTO> SummarizeResult = CED021100BDAO.Summarize(searchDTO);
            //if (ProsesResult[0].IsResult.Split('|')[0] == "E")
            //{
            //    messages.Add("Result");
            //    result["Result"] = "E";
            //    return result;
            //}

            if (ProsesResult[0].IsResult == "Y")
            {
                messages.Add("failValidation");
                result["failValidation"] = "failValidation";
                return result;
            }

            //CED021100BSearchDTO searchDTO = new CED021100BSearchDTO();
            //searchDTO.ProcessId = int.Parse(processId);
            //searchDTO.Type = typeUpload;
            //searchDTO.UserName = userId;

            //IList<CED021100BDTO> rowsResult = CED021100BDAO.GetResultData(searchDTO);
            //if (rowsResult[0].IS_ERROR == "Y")
            //{
            //    messages.Add("failCheckingBasic");
            //    result["failCheckingBasic"] = "failCheckingBasic";
            //    return result;
            //}

            return result;
        }

        public bool CheckFormatNumber(string someString)
        {
            float myInt;
            bool isNumerical = float.TryParse(someString, out myInt);

            return isNumerical;
        }
        public bool CheckFormatDate(string someString)
        {
            if (CheckFormatNumber(someString) == true)
            {
                someString = DateTime.FromOADate(Convert.ToDouble(someString)).ToString("yyyy-MM-dd");
            }
            DateTime myDate;
            bool isDate = DateTime.TryParse(someString, out myDate);

            return isDate;
        }
        public bool CheckFormatTime(string someString)
        {
            TimeSpan myTime;
            bool isTime = TimeSpan.TryParse(someString, out myTime);

            return isTime;
        }
    }
}
