﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021100B
{
    public class CED021100BDTO
    {
        public string ROW_NUM { get; set; }


        public string Line { get; set; }
        public string ProdDate { get; set; }
        public string ShuterNo { get; set; }
        public string Dest { get; set; }
        public string ModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string Shift { get; set; }
        public string RenbanNo { get; set; }
        public string ModType { get; set; }
        public string Dolly1 { get; set; }
        public string Dolly2 { get; set; }
        public string Dolly3 { get; set; }
        public string Dolly4 { get; set; }
        public string Dolly5 { get; set; }
        public string Dolly6 { get; set; }
        public string Dolly7 { get; set; }
        public string Dolly8 { get; set; }
        public string Dolly9 { get; set; }
        public string PlanStart { get; set; }
        public string CT { get; set; }

        //public string Line { get; set; }
        //public string Dest { get; set; }
        //public string ModuleNo { get; set; }
        //public string CaseNo { get; set; }
        //public string Shift { get; set; }
        //public string RenbanNo { get; set; }
        //public string ModType { get; set; }
        //public string Time { get; set; }
        //public string PackTime { get; set; }
        //public string Cumm { get; set; }
        //public string PackDate { get; set; }
        //public string Usage { get; set; }
        //public string Select { get; set; }
        //public string DateFinish { get; set; }
        //public string TimeFinish { get; set; }
        //public string RenbanCode { get; set; }
        //public string ShutterNo { get; set; }
        //public string ControlModuleNo { get; set; }

        public string IsError { get; set; }
        public string UserName { get; set; }
        public string ProcessId { get; set; }
        public string IsResult { get; set; }
    }
}
