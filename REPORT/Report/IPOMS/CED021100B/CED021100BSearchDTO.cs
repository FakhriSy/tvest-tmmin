﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021100B
{
    public class CED021100BSearchDTO
    {
        public string FunctionId { get; set; }
        public string ProcessId { get; set; }
        public string UserName { get; set; }
        
    }
}
