﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021100B
{
    public class CED021100BDAO
    {
        public static void DeleteStaging()
        {
            CSTDMapper.Instance().Delete("CED021100B.DeleteStaging", null);
        }

        public static void InsertStaging(List<CED021100BDTO> dataForm)
        {
            var no = 0;
            foreach (CED021100BDTO item in dataForm)
            {
                no = no + 1;
                CED021100BDTO formData = new CED021100BDTO();
                formData.ROW_NUM = no.ToString();


                formData.Line = item.Line;
                formData.ProdDate = item.ProdDate;
                formData.ShuterNo = item.ShuterNo;
                formData.Dest = item.Dest;
                formData.ModuleNo = item.ModuleNo;
                formData.CaseNo = item.CaseNo;
                formData.Shift = item.Shift;
                formData.RenbanNo = item.RenbanNo;
                formData.ModType = item.ModType;
                formData.Dolly1 = item.Dolly1;
                formData.Dolly2 = item.Dolly2;
                formData.Dolly3 = item.Dolly3;
                formData.Dolly4 = item.Dolly4;
                formData.Dolly5 = item.Dolly5;
                formData.Dolly6 = item.Dolly6;
                formData.Dolly7 = item.Dolly7;
                formData.Dolly8 = item.Dolly8;
                formData.Dolly9 = item.Dolly9;
                formData.PlanStart = item.PlanStart;
                formData.CT = item.CT;

                formData.IsError = item.IsError;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                //formData.IsResult = item.IsResult;
                CSTDMapper.Instance().Insert("CED021100B.InsertStaging", formData);
            }
        }

        public static IList<CED021100BDTO> GetProcessData(CED021100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021100BDTO>("CED021100B.DoProcessData", searchDTO);
        }
        public static IList<CED021100BDTO> Summarize(CED021100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021100BDTO>("CED021100B.Summarize", searchDTO);
        }
    }
}
