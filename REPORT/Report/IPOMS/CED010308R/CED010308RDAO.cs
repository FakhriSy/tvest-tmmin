﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010308R
{
    public class CED010308RDAO
    {
        public static IList<CED010308RDTO> GetResultData(CED010308RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010308RDTO>("CED010308R.DoResultData", searchDTO);
        }
    }
}
