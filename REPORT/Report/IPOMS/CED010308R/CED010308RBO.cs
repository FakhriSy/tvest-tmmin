﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;

namespace Report.IPOMS.CED010308R
{
    public class CED010308RBO : CSTDBaseReport
    {
        CED010308RDTO ErrorDTO = new CED010308RDTO();
        String BatchName = "Okamochi Summary Generation Report";
        String ProcessBatchName = "Okamochi Summary Generation Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Okamochi Summary Generation Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            Console.WriteLine(reportParams.Length);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            CED010308RSearchDTO searchDTO = new CED010308RSearchDTO();
            searchDTO.DeliveryFrom = reportParams[4].Split(';')[0];
            searchDTO.DeliveryTo = reportParams[4].Split(';')[1];
            searchDTO.Shift = reportParams[4].Split(';')[2];
            searchDTO.PhysicalDock = reportParams[4].Split(';')[3];
            searchDTO.RouteName = reportParams[4].Split(';')[4];
            searchDTO.RouteCycle = reportParams[4].Split(';')[5];
            searchDTO.LogicalDock = reportParams[4].Split(';')[6];
            searchDTO.TS = reportParams[4].Split(';')[7];
            searchDTO.Supplier = reportParams[4].Split(';')[8];
            searchDTO.ManifestNo = reportParams[4].Split(';')[9];
            searchDTO.OrderNo = reportParams[4].Split(';')[10];
            searchDTO.Status = reportParams[4].Split(';')[11];
            searchDTO.Dest = reportParams[4].Split(';')[12];
            searchDTO.Lot = reportParams[4].Split(';')[13];
            searchDTO.Case = reportParams[4].Split(';')[14];
            searchDTO.Summary = reportParams[4].Split(';')[15];
            searchDTO.Detail = reportParams[4].Split(';')[16];
            searchDTO.KanbanId = reportParams[4].Split(';')[17];
            searchDTO.PartNo = reportParams[4].Split(';')[18];

            IList<CED010308RDTO> rows = CED010308RDAO.GetResultData(searchDTO);
            #region Fill up report
            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");

            //start looping
            int startRow = 9, rowIndex = startRow;
            long numberOfRow = rows.Count;
            long modOfRow = rows.Count % numberOfRow;
            long rowToRemove = numberOfRow - modOfRow;
            long startIndexRowToRemove = startRow + modOfRow;
            double rowHeight = 11.25;
            int sheetCount = 0;
            int i_seq = 0;
            foreach (CED010308RDTO row in rows)
            {
                i_seq++;
                if (rowIndex == 65535 || sheetCount == 0)
                {
                    sheet = (HSSFSheet)workbook.CloneSheet(0);
                    sheetCount++;

                    startRow = 9;
                    rowIndex = startRow;
                    numberOfRow = rows.Count;
                    modOfRow = rows.Count % numberOfRow;
                    rowToRemove = numberOfRow - modOfRow;
                    startIndexRowToRemove = startRow + modOfRow;
                    rowHeight = 11.25;
                    workbook.SetSheetName(sheetCount, sheetCount.ToString());
                }
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }

                //fill up column
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, i_seq);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.DeliveryDate);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.Shift);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.PhysicalDock);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.LogicalDock);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.TS);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.Route);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.Cycle);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.LP);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.SuppCode);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.SuppName);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.OrderNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.ManifestNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.PlanReceiveOri);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.PlanReceiveUpload);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.ActReceive);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.ModType);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.Dest);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.Lot);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 20, row.Case);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 21, row.KanbanPlan);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 22, row.KanbanAct);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 23, row.Status);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 24, row.PlanCatchUp);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 25, row.Problem);
                ++rowIndex;
            }

            workbook.SetSheetHidden(0, true);
            workbook.SetActiveSheet(1);
            #endregion

            result["workbook"] = workbook;

            return result;
        }
    }
}
