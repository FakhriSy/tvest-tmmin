﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040400B
{
    public class CED040400BDTO
    {

        public string ROW_NUM { get; set; }

        public string Process { get; set; }
        public string Part_No { get; set; }
        public string Dock_Code { get; set; }
        public string Packing_Line_Cd { get; set; }
        public string Line_Code { get; set; }
        public string Pattern_Code { get; set; }
        public string Cycle_Time { get; set; }
        public string Valid_From { get; set; }
        public string Valid_To { get; set; }

        public string UserID { get; set; }
        public string ProcessId { get; set; }
        public string IsResult { get; set; }
    }
}
