﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040400B
{
    public class CED040400BSearchDTO
    {
        public string ProcessId { get; set; }
        //public string FunctionId { get; set; }
        public string UserID { get; set; }
    }
}
