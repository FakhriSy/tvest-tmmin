﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.SS.UserModel;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.IO;
using System.Globalization;

namespace Report.IPOMS.CED040400B
{
    public class CED040400BBO : CSTDBaseReport
    {
        CED040400BDTO ErrorDTO = new CED040400BDTO();
        String BatchName = "Upload Line and Cycle Time Data Batch";
        String ProcessBatchName = "Upload Line and Cycle Time Data Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("UPLOAD", "FOLDER", "EXCEL_UPLOAD_FOLDER", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Upload Line and Cycle Time Data Batch";

            string filename = reportParams[4].Split(';')[0];
            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { null }, "Import file into staging table");

            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                logLocation = "Uploaded data not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { "Read file, delete and insert into staging table" }, "Read file");
            //20190702 FID.Ridwan - Move to finish process
            //CED040400BDAO.DeleteStaging();
            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, "CycleTime", fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }

            int rowno = 0;
            List<CED040400BDTO> listData = new List<CED040400BDTO>();
            for (int row = 6; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    CED040400BDTO dataFrom = new CED040400BDTO();

                    #region get list from file
                    dataFrom.Process = sheet.GetRow(row).GetCell(0).ToString();
                    dataFrom.Part_No = sheet.GetRow(row).GetCell(1).ToString();
                    dataFrom.Dock_Code = sheet.GetRow(row).GetCell(2).ToString();
                    dataFrom.Packing_Line_Cd = sheet.GetRow(row).GetCell(3).ToString();
                    dataFrom.Line_Code = sheet.GetRow(row).GetCell(4).ToString();
                    dataFrom.Pattern_Code = sheet.GetRow(row).GetCell(5).ToString();
                    dataFrom.Cycle_Time = sheet.GetRow(row).GetCell(6).ToString();
                    dataFrom.Valid_From = sheet.GetRow(row).GetCell(7).ToString();
                    #endregion

                    #region check length
                    if (dataFrom.Process != null)
                    {
                        dataFrom.Process = (dataFrom.Process.Length > 1) ? "Truncate" : dataFrom.Process.Trim();
                    }
                    if (dataFrom.Part_No != null)
                    {
                        dataFrom.Part_No = (dataFrom.Part_No.Length > 12) ? "Truncate" : dataFrom.Part_No.Trim();
                    }
                    if (dataFrom.Dock_Code != null)
                    {
                        dataFrom.Dock_Code = (dataFrom.Dock_Code.Length > 3) ? "Truncate" : dataFrom.Dock_Code.Trim();
                    }
                    if (dataFrom.Packing_Line_Cd != null)
                    {
                        dataFrom.Packing_Line_Cd = (dataFrom.Packing_Line_Cd.Length > 3) ? "Truncate" : dataFrom.Packing_Line_Cd.Trim();
                    }
                    if (dataFrom.Line_Code != null)
                    {
                        dataFrom.Line_Code = (dataFrom.Line_Code.Length > 10) ? "Truncate" : dataFrom.Line_Code.Trim();
                    }
                    if (dataFrom.Pattern_Code != null)
                    {
                        dataFrom.Pattern_Code = (dataFrom.Pattern_Code.Length > 10) ? "Truncate" : dataFrom.Pattern_Code.Trim();
                    }
                    //if (dataFrom.Cycle_Time != null)
                    //{
                    //    dataFrom.Cycle_Time = (dataFrom.Cycle_Time.Length > 4) ? "Truncate" : dataFrom.Cycle_Time.Trim();
                    //}
                    #endregion

                    #region validate format
                    if (dataFrom.Process != null && dataFrom.Process != "")
                    {
                        dataFrom.Process = (CheckFormatProcess(dataFrom.Process.Trim().ToUpper()) == false) ? "E" : dataFrom.Process.Trim();
                    }
                    if (dataFrom.Valid_From != null && dataFrom.Valid_From != "")
                    {
                        dataFrom.Valid_From = (CheckFormatDate(dataFrom.Valid_From.Trim()) == false) ? "01-01-1990" : dataFrom.Valid_From.Trim();
                    }
                    #endregion

                    dataFrom.IsResult = "N";
                    dataFrom.UserID = userId;
                    dataFrom.ProcessId = processId;

                    listData.Add(dataFrom);
                }
            }

            CED040400BDAO.InsertStaging(listData);

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { null }, "Mandatory checking");
            CED040400BSearchDTO searchDTO = new CED040400BSearchDTO();
            searchDTO.ProcessId = processId;
            //searchDTO.FunctionId = functionId;
            searchDTO.UserID = userId;

            IList<CED040400BDTO> rowsResult = CED040400BDAO.GetResultData(searchDTO);
            if (rowsResult[0].IsResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            //IList<CED040400BDTO> ValResult = CED040400BDAO.GetValidasiStg(searchDTO);
            //if (ValResult[0].IsResult.Split('|')[0] == "0")
            //{
            //    messages.Add("failCheckingBasic");
            //    result["failCheckingBasic"] = ValResult[0].IsResult.Split('|')[1];
            //    return result;
            //}

            //IList<CED040400BDTO> InsertUpdateResult = CED040400BDAO.InsertUpdate(searchDTO);
            //if (InsertUpdateResult[0].IsResult.Split('|')[0] == "0")
            //{
            //    messages.Add("failCheckingBasic");
            //    result["failCheckingBasic"] = InsertUpdateResult[0].IsResult.Split('|')[1];
            //    return result;
            //}

            //IList<CED040400BDTO> SummarizeResult = CED040400BDAO.Summarize(searchDTO);
            //var Summarize = SummarizeResult[0].IsResult.Split('|');
            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD025I", new String[] { Summarize[0], Summarize[1] }, "Summarize result");
            
            return result;
        }
        public bool CheckFormatDate(string someString)
        {
            DateTime myDate;
            //string format = "dd-MMM-yy";
            string format = "dd/M/yyyy";
            bool isDate = DateTime.TryParseExact(someString, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out myDate);

            return isDate;
        }
        public bool CheckFormatProcess(string someString)
        {
            bool isProcess = false;

            if (someString == "U" || someString == "D")
            {
                isProcess = true;
            }

            return isProcess;
        }
        public bool CheckFormatFlag(string someString)
        {
            bool isFlag = false;

            if (someString == "Y" || someString == "N")
            {
                isFlag = true;
            }

            return isFlag;
        }
    }
}
