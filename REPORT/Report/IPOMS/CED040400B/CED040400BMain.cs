﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040400B
{
    public class CED040400BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED040400BBO ReportBO = new CED040400BBO();
            String reportBatchName = "Upload Line and Cycle Time Data Batch";
            String processSts = "";
            CED040400BDTO dataFrom = new CED040400BDTO();
            dataFrom.ProcessId = processId;
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "fileNotFound");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "fileNotFound"))
                    {
                        processSts = "1";
                    }
                    else
                    {
                        var resultDataChecking = result.SingleOrDefault(d => d.Key == "failCheckingBasic");
                        if (resultDataChecking.Value != null && resultDataChecking.Value.ToString() == "failCheckingBasic")
                        {
                            processSts = "1";
                        }
                        else
                        {
                            processSts = "2";
                        }
                    }
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD008I", new String[] { reportBatchName }, "End Log Success");
                }

                //Delete move here
                CED040400BDAO.DeleteStaging(dataFrom);
            }
            catch (Exception e)
            {
                processSts = "2";

                //Delete move here
                CED040400BDAO.DeleteStaging(dataFrom);

                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD009I", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
