﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED040400B
{
    public class CED040400BDAO
    {
        public static void DeleteStaging(CED040400BDTO dataForm)
        {
            CSTDMapper.Instance().Delete("CED040400B.DeleteStaging", dataForm);
        }

        public static void InsertStaging(List<CED040400BDTO> dataForm) //,string userId
        {
            var no = 0;
            foreach (CED040400BDTO item in dataForm)
            {
                no = no + 1;
                CED040400BDTO formData = new CED040400BDTO();
                formData.ROW_NUM = no.ToString();
                formData.ProcessId = item.ProcessId;

                formData.Process = item.Process;
                formData.Part_No = item.Part_No;
                formData.Dock_Code = item.Dock_Code;
                formData.Packing_Line_Cd = item.Packing_Line_Cd;
                formData.Line_Code = item.Line_Code;
                formData.Pattern_Code = item.Pattern_Code;
                formData.Cycle_Time = item.Cycle_Time;
                formData.Valid_From = item.Valid_From;
                formData.UserID = item.UserID;

                CSTDMapper.Instance().Insert("CED040400B.InsertStaging", formData);
            }
        }

        public static IList<CED040400BDTO> GetValidasiStg(CED040400BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED040400BDTO>("CED040400B.GetValidasiStg", searchDTO);
        }

        public static IList<CED040400BDTO> InsertUpdate(CED040400BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED040400BDTO>("CED040400B.InsertUpdate", searchDTO);
        }

        public static IList<CED040400BDTO> Summarize(CED040400BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED040400BDTO>("CED040400B.Summarize", searchDTO);
        }

        public static IList<CED040400BDTO> GetResultData(CED040400BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED040400BDTO>("CED040400B.DoResultData", searchDTO);
        }
    }
}
