﻿using Common.STD.Base;
using Report.IPOMS.CED010608B;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010608B
{
    public class CED010608BBO : CSTDBaseReport
    {
        CED010608BDTO ErrorDTO = new CED010608BDTO();
        String BatchName = "Create Monthly Packing Sequence List Batch";
        String ProcessBatchName = "Create Monthly Packing Sequence List Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Calculate(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Create Monthly Packing Sequence List Batch";

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Parameter" }, "Get Default Data Reference");

            CED010608BSearchDTO sDTO = new CED010608BSearchDTO();
            sDTO.ProcessId = int.Parse(processId);
            sDTO.PKG_MTH = reportParams[4].Split(';')[0];
            sDTO.PLANT_CD = reportParams[4].Split(';')[1];
            sDTO.UserId = userId;

            IList<CED010608BDTO> rowsResult = CED010608BDAO.GetResultProcessData(sDTO);
            if (rowsResult[0].isResult == "ERROR")
            {
                messages.Add("EndWithError");
                result["EndWithError"] = "EndWithError";
                return result;
            }

            return result;
        }
    }
}
