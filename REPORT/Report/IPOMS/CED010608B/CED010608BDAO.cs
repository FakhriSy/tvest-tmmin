﻿using Common.STD.Util;
using Report.IPOMS.CED010608B;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010608B
{
    public class CED010608BDAO
    {
        public static IList<CED010608BDTO> GetResultProcessData(CED010608BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010608BDTO>("CED010608B.DoProcessData", sDTO);
        }
    }
}
