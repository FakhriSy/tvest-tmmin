﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010206B
{
    public class CED010206BDAO
    {
        public static IList<CED010206BDTO> GetResultProcessData(CED010206BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010206BDTO>("CED010206B.DoProcessData", searchDTO);
        }
    }
}
