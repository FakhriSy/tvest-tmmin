﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;

namespace Report.IPOMS.CED010206B
{
    public class CED010206BBO : CSTDBaseReport
    {
        CED010206BDTO ErrorDTO = new CED010206BDTO();
        String BatchName = "Sending Case Mark & Content List to IPPCS";
        String ProcessBatchName = "Sending Case Mark & Content List to IPPCS";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Sending Case Mark & Content List to IPPCS";

            CED010206BSearchDTO searchDTO = new CED010206BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;

            IList<CED010206BDTO> rowsResult = CED010206BDAO.GetResultProcessData(searchDTO);
            //if (rowsResult[0].isResult == "Y")
            //{

            //}

            return result;
        }
    }
}
