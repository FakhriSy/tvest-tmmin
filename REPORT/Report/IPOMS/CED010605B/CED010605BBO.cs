﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using Report.IPOMS.CED010605B;

namespace Report.IPOMS.CED010605B
{
    public class CED010605BBO : CSTDBaseReport
    {
        CED010605BDTO ErrorDTO = new CED010605BDTO();
        String BatchName = "Create Capacity Forecasting Batch";
        String ProcessBatchName = "Create Capacity Forecasting Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Calculate(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Create Capacity Forecasting Batch";

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Parameter" }, "Get Default Data Reference");

            CED010605BSearchDTO sDTO = new CED010605BSearchDTO();
            sDTO.ProcessId = int.Parse(processId);
            sDTO.PKG_MTH = reportParams[4].Split(';')[0];
            sDTO.PACKING_PLANT_CD = reportParams[4].Split(';')[1];
            sDTO.GROUP_PKG_LINE = reportParams[4].Split(';')[2];
            sDTO.MP = reportParams[4].Split(';')[3] ;
            sDTO.UserId = userId;

            IList<CED010605BDTO> rowsResult = CED010605BDAO.GetResultProcessData(sDTO);
            if (rowsResult[0].isResult == "ERROR")
            {
                messages.Add("EndWithError");
                result["EndWithError"] = "EndWithError";
                return result;
            }

            return result;
        }
    }
}
