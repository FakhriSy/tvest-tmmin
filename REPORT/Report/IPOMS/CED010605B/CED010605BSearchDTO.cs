﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010605B
{
    public class CED010605BSearchDTO
    {
        public int ProcessId { get; set; }
        public string PKG_MTH { get; set; }
        public string PACKING_PLANT_CD { get; set; }
        public string GROUP_PKG_LINE { get; set; }
        public string MP { get; set; }
        public string UserId { get; set; }
    }
}
