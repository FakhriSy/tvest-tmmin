﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;
using Report.IPOMS.CED010605B;

namespace Report.IPOMS.CED010605B
{
    public class CED010605BDAO
    {
        public static IList<CED010605BDTO> GetResultProcessData(CED010605BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010605BDTO>("CED010605B.DoProcessData", sDTO);
        }
    }
}
