﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030200B
{
    public class CED030200BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }
        public string ManifestNo { get; set; }
        public int Type { get; set; }

        //LOCKING
        public string FUNCTION_ID { get; set; }
        public string LOCK_REF { get; set; }
        public string REMARKS { get; set; }
        public string COMMAND { get; set; }
        public int TYPE { get; set; }
    }
}
