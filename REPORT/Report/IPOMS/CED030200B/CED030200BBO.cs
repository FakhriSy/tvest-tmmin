﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.Collections;

namespace Report.IPOMS.CED030200B
{
    public class CED030200BBO : CSTDBaseReport
    {
        CED030200BDTO ErrorDTO = new CED030200BDTO();
        String BatchName = "Send Data Receiving to IPPCS";
        String ProcessBatchName = "Send Data Receiving to IPPCS";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            String tableName = "";
            Hashtable param = new Hashtable();
            Dictionary<string, string> data = new Dictionary<string, string>();

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Send Data Receiving to IPPCS";

            string ManifestNo = reportParams[4].Split(';')[0];
            int Type = int.Parse(reportParams[4].Split(';')[1]);

            ManifestNo = ManifestNo == "" ? null : ManifestNo;
            Type = Type == null ? 0 : Type;

            IList<CED030200BDTO> rowsResult = null;

            CED030200BSearchDTO searchDTO = new CED030200BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;
            searchDTO.ManifestNo = ManifestNo;
            searchDTO.Type = Type;

            int flag = Convert.ToInt32(CSTDSystemMasterHelper.GetValue("CED030100B", "WEB_SERVICE", "FLAG", CSTDSystemMasterHelper.Mandatory));

            CED030200BSearchDTO LockDTO = new CED030200BSearchDTO();
            LockDTO.ProcessId = int.Parse(processId);
            LockDTO.FUNCTION_ID = "CED030200B";
            LockDTO.LOCK_REF = DateTime.Now.ToString("yyyyMMdd");
            LockDTO.REMARKS = "";
            LockDTO.UserName = userId;
            LockDTO.COMMAND = "I";
            LockDTO.TYPE = 1;

            //check locking
            IList<CED030200BDTO> Locking = CED030200BDAO.CheckLocking(LockDTO);

            if (Locking[0].FUNCTION_LOCKED > 0)
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD100E", new String[] { Locking[0].RUN_PROCESS_ID.ToString() }, "Locking Process");
                messages.Add("fail");
                result["failgetdata"] = "fail";
                return result;
            }
            else
            {
                if (flag == 1)
                {
                    //Web Service  
                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Using method Web Service" }, "Get Method");

                    rowsResult = CED030200BDAO.GetResultProcessDataWS(searchDTO);

                    if (rowsResult[0].isResult == "N")
                    {
                        #region Logic post data to WS IPPCS
                        con.Open();
                        var ManifestData = CSTDDBUtil.ExecuteQuery(con, " select ITEM from TB_T_JSON_PARAM WHERE TYPE = 'M' ", param);
                        var DeliveryData = CSTDDBUtil.ExecuteQuery(con, " select ITEM from TB_T_JSON_PARAM WHERE TYPE = 'D' ", param);
                        con.Close();
                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Send data to IPPCS" }, "Send data to IPPCS");
                        var ManifestResponse = CSTDWebAPIUtil.Request<CED030200BManifestResponse>(data, "PostReceivingManifest", ManifestData.Select().FirstOrDefault().ItemArray[0].ToString(), "POST");
                        if (ManifestResponse[0].Result == "Failed")
                        {
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000E", new String[] { ManifestResponse[0].Message }, "Send data to IPPCS");
                            messages.Add("fail");
                            result["fail"] = "fail";
                            return result;
                        }
                        var DeliveryResponse = CSTDWebAPIUtil.Request<CED030200BManifestResponse>(data, "PostReceivingDeliveryCtl", DeliveryData.Select().FirstOrDefault().ItemArray[0].ToString(), "POST");
                        #endregion
                    }
                }
                else
                {
                    //DB Link
                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Using method DB Link" }, "Get Method");

                    rowsResult = CED030200BDAO.GetResultProcessData(searchDTO);
                }

                if (rowsResult[0].isResult == "Y")
                {
                    messages.Add("fail");
                    result["fail"] = "fail";
                    return result;
                }

                if (rowsResult[0].isResult == "C")
                {
                    messages.Add("CatchError");
                    result["CatchError"] = "CatchError";
                    return result;
                }
            }

            return result;
        }

        public Dictionary<String, Object> RollbackData(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            string ManifestNo = reportParams[4].Split(';')[0];
            int Type = int.Parse(reportParams[4].Split(';')[1]);

            ManifestNo = ManifestNo == "" ? null : ManifestNo;
            Type = Type == null ? 0 : Type;

            CED030200BSearchDTO searchDTO = new CED030200BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;
            searchDTO.ManifestNo = ManifestNo;
            searchDTO.Type = Type;

            IList<CED030200BDTO> rowsResult = CED030200BDAO.GetResultRollbackData(searchDTO);

            return result;
        }
    }
}
