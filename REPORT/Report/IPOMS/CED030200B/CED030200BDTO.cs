﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED030200B
{
    public class CED030200BDTO : CSTDBaseDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }
        public string ManifestNo { get; set; }
        public int Type { get; set; }

        public string isResult { get; set; }

        public int FUNCTION_LOCKED { get; set; } //1
        public int RUN_PROCESS_ID { get; set; } //1
        public int NR_ERR { get; set; } //2
    }

    public class CED030200BManifestResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

}
