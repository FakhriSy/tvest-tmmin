﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED030200B
{
    public class CED030200BDAO
    {
        public static IList<CED030200BDTO> GetResultProcessData(CED030200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030200BDTO>("CED030200B.DoProcessData", searchDTO);
        }

        public static IList<CED030200BDTO> GetResultRollbackData(CED030200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030200BDTO>("CED030200B.DoRollbackData", searchDTO);
        }

        public static IList<CED030200BDTO> GetResultProcessDataWS(CED030200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030200BDTO>("CED030200B.DoProcessDataWS", searchDTO);
        }

        public static IList<CED030200BDTO> CheckLocking(CED030200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030200BDTO>("CED030200B.CheckLocking", searchDTO);
        }

    }
}
