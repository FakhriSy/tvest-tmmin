﻿using Common.STD.Base;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010618B
{
    public class CED010618BDTO : CSTDBaseDTO
    {
        public string UploadId { get; set; }

        public string ModId { get; set; }
        public string ModTypeConv { get; set; }

        public string PartNo { get; set; }
        public string UniqueNo { get; set; }
        public string ZoneCd { get; set; }

        public string ProcessId { get; set; }
        public string RowNo { get; set; }
        public string UserName { get; set; }
        public string isResult { get; set; }
    }
}
