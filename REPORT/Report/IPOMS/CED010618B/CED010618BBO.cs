﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010618B
{
    public class CED010618BBO : CSTDBaseReport
    {
        CED010618BDTO ErrorDTO = new CED010618BDTO();
        String BatchName = "Upload Module Type/ Part Jundate Data Batch";
        String ProcessBatchName = "Upload ModuleType/ Part Jundate Data Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation, String dataName)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "FULLPATH_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            //string _filePathFolder = "C:\\Fujitsu\\tvest\\IPOS\\Report\\Upload\\";
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            string filename = reportParams[4].Split(';')[0];

            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                logLocation = "Uploaded file not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { "Read file, delete and insert into staging table" }, "Read file");

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, dataName, fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }

            int rowno = 0;
            List<CED010618BDTO> listData = new List<CED010618BDTO>();
            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;

                    CED010618BDTO dataForm = new CED010618BDTO();

                    if (dataName == "ModuleType")
                    {
                        dataForm.ModId = sheet.GetRow(row).GetCell(0)?.ToString();
                        dataForm.ModTypeConv = sheet.GetRow(row).GetCell(1)?.ToString();
                    }
                    else if (dataName == "PartJundate")
                    {
                        dataForm.PartNo = sheet.GetRow(row).GetCell(0)?.ToString();
                        dataForm.UniqueNo = sheet.GetRow(row).GetCell(1)?.ToString();
                        dataForm.ZoneCd = sheet.GetRow(row).GetCell(2)?.ToString();
                    }

                    dataForm.ProcessId = processId;
                    dataForm.RowNo = rowno.ToString();
                    dataForm.UserName = userId;
                    dataForm.isResult = "N";

                    listData.Add(dataForm);
                }
            }

            CED010618BDAO.InsertStaging(listData, dataName);

            CED010618BSearchDTO searchDTO = new CED010618BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;

            IList<CED010618BDTO> rowsResult = CED010618BDAO.GetResultData(searchDTO, dataName);
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            return result;
        }
    }
}
