﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010618B
{
    public class CED010618BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }
    }
}
