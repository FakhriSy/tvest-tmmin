﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010618B
{
    public class CED010618BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED010618BBO ReportBO = new CED010618BBO();
            String reportBatchName = "Upload Module Type/ Part Jundate Data Batch";
            String processSts = "";
            CED010618BDTO dataForm = new CED010618BDTO();
            dataForm.ProcessId = processId;
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                string dataName = reportParameters[4].Split(';')[1];
                
                CED010618BDAO.DeleteStaging(dataForm, dataName);
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation, dataName);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "fileNotFound");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "fileNotFound"))
                    {
                        processSts = "1";
                    }
                    else
                    {
                        var resultDataChecking = result.SingleOrDefault(d => d.Key == "failCheckingBasic");
                        if (resultDataChecking.Value != null && resultDataChecking.Value.ToString() == "failCheckingBasic")
                        {
                            processSts = "1";
                        }
                        else
                        {
                            processSts = "2";
                        }
                    }
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD008I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";

                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
