﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010618B
{
    public class CED010618BDAO
    {
        public static void DeleteStaging(CED010618BDTO dataForm, string dataName)
        {
            if (dataName == "ModuleType")
                CSTDMapper.Instance().Delete("CED010618B.DeleteStagingModuleType", dataForm);
            else
                CSTDMapper.Instance().Delete("CED010618B.DeleteStagingPartJundate", dataForm);
        }

        public static void InsertStaging(List<CED010618BDTO> dataForm, string dataName)
        {
            foreach (CED010618BDTO item in dataForm)
                if (dataName == "ModuleType")
                    CSTDMapper.Instance().Insert("CED010618B.InsertStagingModuleType", item);
                else
                    CSTDMapper.Instance().Insert("CED010618B.InsertStagingPartJundate", item);
        }

        public static IList<CED010618BDTO> GetResultData(CED010618BSearchDTO searchDTO, string dataName)
        {
            if (dataName == "ModuleType")
                return CSTDMapper.Instance().QueryForList<CED010618BDTO>("CED010618B.DoResultModuleType", searchDTO);
            else
                return CSTDMapper.Instance().QueryForList<CED010618BDTO>("CED010618B.DoResultPartJundate", searchDTO);

        }
    }
}
