﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010401B
{
    public class CED010401BDAO
    {
        public static IList<CED010401BDTO> GetResultProcessData(CED010401BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010401BDTO>("CED010401B.DoProcessData", searchDTO);
        }
    }
}
