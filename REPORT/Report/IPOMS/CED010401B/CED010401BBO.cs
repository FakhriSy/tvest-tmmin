﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;

namespace Report.IPOMS.CED010401B
{
    public class CED010401BBO : CSTDBaseReport
    {
        CED010401BDTO ErrorDTO = new CED010401BDTO();
        String BatchName = "Prepare Data for Autozoner";
        String ProcessBatchName = "Prepare Data for Autozoner";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Prepare Data for Autozoner";

            CED010401BSearchDTO searchDTO = new CED010401BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.EOFlag = int.Parse(reportParams[4].Split(';')[0]);
            searchDTO.UserName = userId;

            IList<CED010401BDTO> rowsResult = CED010401BDAO.GetResultProcessData(searchDTO);
            //if (rowsResult[0].isResult == "Y")
            //{

            //}

            return result;
        }
    }
}
