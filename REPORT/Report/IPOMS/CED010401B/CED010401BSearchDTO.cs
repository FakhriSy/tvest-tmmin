﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010401B
{
    public class CED010401BSearchDTO
    {
        public int ProcessId { get; set; }
        public int EOFlag { get; set; }
        public string UserName { get; set; }
    }
}
