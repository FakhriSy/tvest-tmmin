﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED060100B
{
    public class CED060100BSearchDTO
    {
        public int ProcessId { get; set; }
        public string CtrlModNo { get; set; }
        public string UserName { get; set; }
    }
}
