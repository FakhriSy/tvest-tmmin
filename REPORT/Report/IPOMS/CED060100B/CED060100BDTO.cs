﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED060100B
{
    public class CED060100BDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }

        public string isResult { get; set; }
    }
}
