﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED060100B
{
    public class CED060100BDAO
    {
        public static IList<CED060100BDTO> GetResultProcessData(CED060100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED060100BDTO>("CED060100B.DoProcessData", searchDTO);
        }
    }
}
