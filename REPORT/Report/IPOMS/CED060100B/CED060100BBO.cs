﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;

namespace Report.IPOMS.CED060100B
{
    public class CED060100BBO : CSTDBaseReport
    {
        CED060100BDTO ErrorDTO = new CED060100BDTO();
        String BatchName = "Send Stacking Data to ROEM";
        String ProcessBatchName = "Send Stacking Data to ROEM";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Send Stacking Data to ROEM";

            string CtrlModNo = reportParams[4].Split(';')[0];

            CtrlModNo = CtrlModNo == "" ? null : CtrlModNo;

            CED060100BSearchDTO searchDTO = new CED060100BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.CtrlModNo = CtrlModNo;
            searchDTO.UserName = userId;

            IList<CED060100BDTO> rowsResult = CED060100BDAO.GetResultProcessData(searchDTO);
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("fail");
                result["fail"] = "fail";
                return result;
            }

            return result;
        }
    }
}
