﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED031100B
{
    public class CED031100BDAO
    {
        public static void DeleteStaging()
        {
            CSTDMapper.Instance().Delete("CED031100B.DeleteStaging", null);
        }

        public static void InsertStaging(List<CED031100BDTO> dataForm)
        {
            foreach (CED031100BDTO item in dataForm)
            {
                CED031100BDTO formData = new CED031100BDTO();
                formData.Route = item.Route;
                formData.Cycle = item.Cycle;
                formData.SupplierCd = item.SupplierCd;
                formData.SupplierPlant = item.SupplierPlant;
                formData.ArrivalPlanDate = item.ArrivalPlanDate;
                formData.ArrivalPlanTime = item.ArrivalPlanTime;
                formData.Dest = item.Dest;
                formData.Lot = item.Lot;
                formData.Case = item.Case;
                formData.Shift = item.Shift;
                formData.Renban = item.Renban;
                formData.ModuleType = item.ModuleType;
                formData.VanningPlanDate = item.VanningPlanDate;
                formData.VanningPlanTime = item.VanningPlanTime;
                formData.TagQuality = item.TagQuality;
                formData.Remark = item.Remark;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                formData.IsResult = item.IsResult;
                formData.RowNo = item.RowNo;
                CSTDMapper.Instance().Insert("CED031100B.InsertStaging", formData);
            }
        }

        public static IList<CED031100BDTO> GetProcessData(CED031100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED031100BDTO>("CED031100B.DoProcessData", searchDTO);
        }
    }
}
