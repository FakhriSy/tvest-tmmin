﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED031100B
{
    public class CED031100BBO : CSTDBaseReport
    {
        CED031100BDTO ErrorDTO = new CED031100BDTO();
        String BatchName = "Upload Okamochi by Module Process";
        String ProcessBatchName = "Upload Okamochi by Module Process";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("UPLOAD", "FOLDER", "EXCEL_UPLOAD_FOLDER", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Upload Okamochi by Module Process";

            string filename = reportParams[4].Split(';')[0];

            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                logLocation = "Uploaded file not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { "Read file, delete and insert into staging table" }, "Read file");
            CED031100BDAO.DeleteStaging();

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, "Okamochi", fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            int rowno = 0;
            List<CED031100BDTO> listData = new List<CED031100BDTO>();
            for (int row = 9; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    CED031100BDTO dataFrom = new CED031100BDTO();

                    #region get list from file
                    dataFrom.Route = sheet.GetRow(row).GetCell(1).ToString();
                    dataFrom.Cycle = sheet.GetRow(row).GetCell(2).ToString();
                    dataFrom.SupplierCd = sheet.GetRow(row).GetCell(3).ToString();
                    dataFrom.SupplierPlant = sheet.GetRow(row).GetCell(4).ToString();
                    dataFrom.ArrivalPlanDate = sheet.GetRow(row).GetCell(5).ToString();
                    dataFrom.ArrivalPlanTime = sheet.GetRow(row).GetCell(6).ToString();
                    dataFrom.Dest = sheet.GetRow(row).GetCell(7).ToString();
                    dataFrom.Lot = sheet.GetRow(row).GetCell(8).ToString();
                    dataFrom.Case = sheet.GetRow(row).GetCell(9).ToString();
                    dataFrom.Shift = sheet.GetRow(row).GetCell(10).ToString();
                    dataFrom.Renban = sheet.GetRow(row).GetCell(11).ToString();
                    dataFrom.ModuleType = sheet.GetRow(row).GetCell(12).ToString();
                    dataFrom.VanningPlanDate = sheet.GetRow(row).GetCell(13).ToString();
                    dataFrom.VanningPlanTime = sheet.GetRow(row).GetCell(14).ToString();
                    dataFrom.TagQuality = sheet.GetRow(row).GetCell(15).ToString();
                    dataFrom.Remark = sheet.GetRow(row).GetCell(16).ToString();
                    #endregion

                    #region validate format
                    if (dataFrom.ArrivalPlanDate != null && dataFrom.ArrivalPlanDate != "")
                    {
                        dataFrom.ArrivalPlanDate = (CheckFormatDate(dataFrom.ArrivalPlanDate.Trim()) == false) ? "01-01-1990" : dataFrom.ArrivalPlanDate.Trim();
                    }
                    else if(dataFrom.ArrivalPlanDate == "")
                    {
                        dataFrom.ArrivalPlanDate = null;
                    }
                    if (dataFrom.ArrivalPlanTime != null && dataFrom.ArrivalPlanTime != "")
                    {
                        dataFrom.ArrivalPlanTime = (CheckFormatTime(dataFrom.ArrivalPlanTime.Trim()) == false) ? "00:00" : dataFrom.ArrivalPlanTime.Trim();
                    }
                    else if (dataFrom.ArrivalPlanTime == "")
                    {
                        dataFrom.ArrivalPlanTime = null;
                    }
                    if (dataFrom.VanningPlanDate != null && dataFrom.VanningPlanDate != "")
                    {
                        dataFrom.VanningPlanDate = (CheckFormatDate(dataFrom.VanningPlanDate.Trim()) == false) ? "01-01-1990" : dataFrom.VanningPlanDate.Trim();
                    }
                    else if (dataFrom.VanningPlanDate == "")
                    {
                        dataFrom.VanningPlanDate = null;
                    }
                    if (dataFrom.VanningPlanTime != null && dataFrom.VanningPlanTime != "")
                    {
                        dataFrom.VanningPlanTime = (CheckFormatTime(dataFrom.VanningPlanTime.Trim()) == false) ? "00:00" : dataFrom.VanningPlanTime.Trim();
                    }
                    else if (dataFrom.VanningPlanTime == "")
                    {
                        dataFrom.VanningPlanTime = null;
                    }
                    if (dataFrom.Case == "")
                    {
                        dataFrom.Case = null;
                    }
                    #endregion

                    #region check length
                    if (dataFrom.SupplierCd != null)
                    {
                        dataFrom.SupplierCd = (dataFrom.SupplierCd.Length > 10) ? "Truncate" : dataFrom.SupplierCd.Trim();
                    }
                    if (dataFrom.SupplierPlant != null)
                    {
                        dataFrom.SupplierPlant = (dataFrom.SupplierPlant.Length > 1) ? "Truncate" : dataFrom.SupplierPlant.Trim();
                    }
                    if (dataFrom.Dest != null)
                    {
                        dataFrom.Dest = (dataFrom.Dest.Length > 10) ? "Truncate" : dataFrom.Dest.Trim();
                    }
                    if (dataFrom.Lot != null)
                    {
                        dataFrom.Lot = (dataFrom.Lot.Length > 6) ? "Truncate" : dataFrom.Lot.Trim();
                    }
                    if (dataFrom.Case != null)
                    {
                        dataFrom.Case = (dataFrom.Case.Length > 3) ? "Truncate" : dataFrom.Case.Trim();
                    }
                    if (dataFrom.Renban != null)
                    {
                        dataFrom.Renban = (dataFrom.Renban.Length > 9) ? "Truncate" : dataFrom.Renban.Trim();
                    }
                    if (dataFrom.ModuleType != null)
                    {
                        dataFrom.ModuleType = (dataFrom.ModuleType.Length > 3) ? "Truncate" : dataFrom.ModuleType.Trim();
                    }
                    if (dataFrom.Shift != null)
                    {
                        dataFrom.Shift = (dataFrom.Shift.Length > 1) ? "Truncate" : dataFrom.Shift.Trim();
                    }
                    if (dataFrom.Route != null)
                    {
                        dataFrom.Route = (dataFrom.Route.Length > 4) ? "Truncate" : dataFrom.Route.Trim();
                    }
                    if (dataFrom.Cycle != null)
                    {
                        dataFrom.Cycle = (dataFrom.Cycle.Length > 2) ? "Truncate" : dataFrom.Cycle.Trim();
                    }
                    #endregion

                    dataFrom.IsResult = "N";
                    dataFrom.UserName = userId;
                    dataFrom.ProcessId = processId;
                    dataFrom.RowNo = rowno.ToString();

                    listData.Add(dataFrom);
                }
            }

            CED031100BDAO.InsertStaging(listData);

            CED031100BSearchDTO searchDTO = new CED031100BSearchDTO();
            searchDTO.ProcessId = processId;

            IList<CED031100BDTO> rowsProcess = CED031100BDAO.GetProcessData(searchDTO);
            if (rowsProcess[0].IsResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            return result;
        }

        public bool CheckFormatNumber(string someString)
        {
            float myInt;
            bool isNumerical = float.TryParse(someString, out myInt);

            return isNumerical;
        }
        public bool CheckFormatDate(string someString)
        {
            DateTime myDate;
            //string format = "dd-MMM-yy";
            string format = "dd/M/yyyy";
            bool isDate = DateTime.TryParseExact(someString, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out myDate);

            return isDate;
        }
        public bool CheckFormatTime(string someString)
        {
            TimeSpan myTime;
            bool isTime = TimeSpan.TryParse(someString, out myTime);

            return isTime;
        }
    }
}
