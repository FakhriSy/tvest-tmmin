﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED031100B
{
    public class CED031100BDTO
    {
        public string Route { get; set; }
        public string Cycle { get; set; }
        public string SupplierCd { get; set; }
        public string SupplierPlant { get; set; }
        public string ArrivalPlanDate { get; set; }
        public string ArrivalPlanTime { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string Shift { get; set; }
        public string Renban { get; set; }
        public string ModuleType { get; set; }
        public string VanningPlanDate { get; set; }
        public string VanningPlanTime { get; set; }
        public string TagQuality { get; set; }
        public string Remark { get; set; }

        public string UserName { get; set; }
        public string ProcessId { get; set; }
        public string RowNo { get; set; }
        public string IsResult { get; set; }
    }
}
