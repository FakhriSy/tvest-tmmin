﻿using Common.STD.Util;
using Report.IPOMS.CED010607B;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010607B
{
    public class CED010607BDAO
    {
        public static IList<CED010607BDTO> GetResultProcessData(CED010607BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010607BDTO>("CED010607B.DoProcessData", sDTO);
        }
    }
}
