﻿using Common.STD.Base;
using Report.IPOMS.CED010607B;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010607B
{
    public class CED010607BBO : CSTDBaseReport
    {
        CED010607BDTO ErrorDTO = new CED010607BDTO();
        String BatchName = "Create Capacity Firm Batch";
        String ProcessBatchName = "Create Capacity Firm Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Calculate(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Create Capacity Firm Batch";

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Parameter" }, "Get Default Data Reference");

            CED010607BSearchDTO sDTO = new CED010607BSearchDTO();
            sDTO.ProcessId = int.Parse(processId);
            sDTO.PKG_MTH = reportParams[4].Split(';')[0];
            sDTO.PLANT_CD = reportParams[4].Split(';')[1];
            sDTO.GROUP_PACKING = reportParams[4].Split(';')[2];
            sDTO.MP = reportParams[4].Split(';')[3];
            sDTO.UserId = userId;

            IList<CED010607BDTO> rowsResult = CED010607BDAO.GetResultProcessData(sDTO);
            if (rowsResult[0].isResult == "ERROR")
            {
                messages.Add("EndWithError");
                result["EndWithError"] = "EndWithError";
                return result;
            }

            return result;
        }
    }
}
