﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010607B
{
    public class CED010607BSearchDTO
    {
        public int ProcessId { get; set; }
        public string PKG_MTH { get; set; }
        public string PLANT_CD { get; set; }
        public string GROUP_PACKING { get; set; }
        public string MP { get; set; }
        public string UserId { get; set; }
    }
}
