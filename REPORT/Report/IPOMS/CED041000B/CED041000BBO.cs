﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED041000B
{
    public class CED041000BBO : CSTDBaseReport
    {
        CED041000BDTO ErrorDTO = new CED041000BDTO();
        String BatchName = "Upload Part PassThru Data Batch";
        String ProcessBatchName = "Upload Part PassThru Data Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("UPLOAD", "FOLDER", "EXCEL_UPLOAD_FOLDER", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Upload Part PassThru Data Batch";

            string filename = reportParams[4].Split(';')[0];

            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                logLocation = "Uploaded file not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { "Read file, delete and insert into staging table" }, "Read file");
            //20190702 FID.Ridwan - Move to finish process
            //CED041000BDAO.DeleteStaging();

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, "Passthru", fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            int rowno = 0;
            List<CED041000BDTO> listData = new List<CED041000BDTO>();
            for (int row = 6; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    CED041000BDTO dataFrom = new CED041000BDTO();

                    #region get list from file
                    dataFrom.Proccess = sheet.GetRow(row).GetCell(0).ToString();
                    dataFrom.PartNo = sheet.GetRow(row).GetCell(1).ToString();
                    dataFrom.PackingLineCode = sheet.GetRow(row).GetCell(2).ToString();
                    dataFrom.LineCode = sheet.GetRow(row).GetCell(3).ToString();
                    dataFrom.PatternCode = sheet.GetRow(row).GetCell(4).ToString();
                    dataFrom.PassthruFlag = sheet.GetRow(row).GetCell(5).ToString();
                    dataFrom.ValidFrom = sheet.GetRow(row).GetCell(6).ToString();
                    //dataFrom.RackAddres = sheet.GetRow(row).GetCell(6).ToString();
                    //dataFrom.QtyBox = sheet.GetRow(row).GetCell(7).ToString();
                    //dataFrom.ValidFrom = sheet.GetRow(row).GetCell(8).ToString();
                    #endregion

                    #region validate format
                    if (dataFrom.Proccess != null && dataFrom.Proccess != "")
                    {
                        dataFrom.Proccess = (CheckFormatProcess(dataFrom.Proccess.Trim().ToUpper()) == false) ? "E" : dataFrom.Proccess.Trim();
                    }
                    if (dataFrom.PassthruFlag != null && dataFrom.PassthruFlag != "")
                    {
                        dataFrom.PassthruFlag = (CheckFormatFlag(dataFrom.PassthruFlag.Trim().ToUpper()) == false) ? "E" : dataFrom.PassthruFlag.Trim();
                    }
                    if (dataFrom.ValidFrom != null && dataFrom.ValidFrom != "")
                    {
                        dataFrom.ValidFrom = (CheckFormatDate(dataFrom.ValidFrom.Trim()) == false) ? "01-01-1990" : dataFrom.ValidFrom.Trim();
                    }
                    #endregion

                    #region check length
                    if (dataFrom.Proccess != null)
                    {
                        dataFrom.Proccess = (dataFrom.Proccess.Length > 1) ? "Truncate" : dataFrom.Proccess.Trim();
                    }
                    if (dataFrom.PartNo != null)
                    {
                        dataFrom.PartNo = (dataFrom.PartNo.Length > 12) ? "Truncate" : dataFrom.PartNo.Trim();
                    }
                    if (dataFrom.PackingLineCode != null)
                    {
                        dataFrom.PackingLineCode = (dataFrom.PackingLineCode.Length > 3) ? "Truncate" : dataFrom.PackingLineCode.Trim();
                    }
                    if (dataFrom.LineCode != null)
                    {
                        dataFrom.LineCode = (dataFrom.LineCode.Length > 2) ? "Truncate" : dataFrom.LineCode.Trim();
                    }
                    if (dataFrom.PatternCode != null)
                    {
                        dataFrom.PatternCode = (dataFrom.PatternCode.Length > 1) ? "Truncate" : dataFrom.PatternCode.Trim();
                    }
                    if (dataFrom.PassthruFlag != null)
                    {
                        dataFrom.PassthruFlag = (dataFrom.PassthruFlag.Length > 1) ? "Truncate" : dataFrom.PassthruFlag.Trim();
                    }
                    //if (dataFrom.RackAddres != null)
                    //{
                    //    dataFrom.RackAddres = (dataFrom.RackAddres.Length > 10) ? "Truncate" : dataFrom.RackAddres.Trim();
                    //}
                    #endregion

                    dataFrom.isResult = "N";
                    dataFrom.UserName = userId;
                    dataFrom.RowNo = rowno.ToString();
                    dataFrom.ProcessId = processId;

                    listData.Add(dataFrom);
                }
            }

            CED041000BDAO.InsertDataUpload(listData);

            CED041000BSearchDTO searchDTO = new CED041000BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;

            IList<CED041000BDTO> rowsResult = CED041000BDAO.GetResultData(searchDTO);
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            return result;
        }
        public bool CheckFormatDate(string someString)
        {
            DateTime myDate;
            //string format = "dd-MMM-yy";
            string format = "dd/M/yyyy";
            bool isDate = DateTime.TryParseExact(someString, format, CultureInfo.InvariantCulture, DateTimeStyles.None, out myDate);

            return isDate;
        }
        public bool CheckFormatProcess(string someString)
        {
            bool isProcess = false;

            if (someString == "U" || someString == "D")
            {
                isProcess = true;
            }

            return isProcess;
        }
        //public bool CheckFormatFlag(string someString)
        //{
        //    bool isFlag = false;

        //    if (someString == "Y" || someString == "N")
        //    {
        //        isFlag = true;
        //    }

        //    return isFlag;
        //}
        public bool CheckFormatFlag(string someString)
        {
            bool isFlag = false;

            if (someString == "1" || someString == "0")
            {
                isFlag = true;
            }

            return isFlag;
        }
    }
}
