﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED041000B
{
    public class CED041000BDAO
    {
        public static void DeleteStaging(CED041000BDTO dataFrom)
        {
            CSTDMapper.Instance().Delete("CED041000B.DeleteStaging", dataFrom);
        }

        public static void InsertDataUpload(List<CED041000BDTO> dataForm)
        {
            foreach (CED041000BDTO item in dataForm)
            {
                CED041000BDTO formData = new CED041000BDTO();
                formData.Proccess = item.Proccess;
                formData.PartNo = item.PartNo;
                formData.PackingLineCode = item.PackingLineCode;
                formData.LineCode = item.LineCode;
                formData.PatternCode = item.PatternCode;
                formData.PassthruFlag = item.PassthruFlag;
                //formData.RackAddres = item.RackAddres;
                //formData.QtyBox = item.QtyBox;
                formData.ValidFrom = item.ValidFrom;
                formData.RowNo = item.RowNo;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                CSTDMapper.Instance().Insert("CED041000B.InsertDataUpload", formData);
            }
        }

        public static IList<CED041000BDTO> GetResultData(CED041000BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED041000BDTO>("CED041000B.DoResultData", searchDTO);
        }
    }
}
