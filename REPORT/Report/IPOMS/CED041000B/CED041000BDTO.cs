﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED041000B
{
    public class CED041000BDTO : CSTDBaseDTO
    {
        public string Proccess { get; set; }
        public string PartNo { get; set; }
        public string PackingLineCode { get; set; }
        public string LineCode { get; set; }
        public string PatternCode { get; set; }
        public string PassthruFlag { get; set; }
        public string RackAddres { get; set; }
        public string QtyBox { get; set; }
        public string ValidFrom { get; set; }
        public string ProcessId { get; set; }
        public string RowNo { get; set; }

        public string UserName { get; set; }
        public string isResult { get; set; }
    }
}
