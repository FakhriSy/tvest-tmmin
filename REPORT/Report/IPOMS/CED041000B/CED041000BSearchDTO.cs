﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED041000B
{
    public class CED041000BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }
    }
}
