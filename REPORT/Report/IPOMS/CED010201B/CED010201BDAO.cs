﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010201B
{
    public class CED010201BDAO
    {
        public static void DeleteStaging()
        {
            CSTDMapper.Instance().Delete("CED010201B.DeleteStaging", null);
        }

        public static void InsertStaging(List<CED010201BDTO> dataForm)
        {
            foreach (CED010201BDTO item in dataForm)
            {
                CED010201BDTO formData = new CED010201BDTO();
                formData.Line = item.Line;
                formData.Dest = item.Dest;
                formData.ModuleNo = item.ModuleNo;
                formData.CaseNo = item.CaseNo;
                formData.Shift = item.Shift;
                formData.RenbanNo = item.RenbanNo;
                formData.ModType = item.ModType;
                formData.Time = item.Time;
                formData.PackTime = item.PackTime;
                formData.Cumm = item.Cumm;
                formData.PosDate = item.PosDate;
                formData.Usage = item.Usage;
                formData.Select = item.Select;
                formData.DateFinish = item.DateFinish;
                formData.TimeFinish = item.TimeFinish;
                formData.RenbanCode = item.RenbanCode;
                formData.ShutterNo = item.ShutterNo;
                formData.ControlModuleNo = item.ControlModuleNo;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                formData.IsResult = item.IsResult;
                CSTDMapper.Instance().Insert("CED010201B.InsertStaging", formData);
            }
        }

        public static IList<CED010201BDTO> GetProcessData(CED010201BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010201BDTO>("CED010201B.DoProcessData", searchDTO);
        }
    }
}
