﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010201B
{
    public class CED010201BDTO
    {
        public string Line { get; set; }
        public string Dest { get; set; }
        public string ModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string Shift { get; set; }
        public string RenbanNo { get; set; }
        public string ModType { get; set; }
        public string Time { get; set; }
        public string PackTime { get; set; }
        public string Cumm { get; set; }
        public string PosDate { get; set; }
        public string Usage { get; set; }
        public string Select { get; set; }
        public string DateFinish { get; set; }
        public string TimeFinish { get; set; }
        public string RenbanCode { get; set; }
        public string ShutterNo { get; set; }
        public string ControlModuleNo { get; set; }

        public string UserName { get; set; }
        public string ProcessId { get; set; }

        public string IsResult { get; set; }
    }
}
