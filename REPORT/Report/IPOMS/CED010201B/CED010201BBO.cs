﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010201B
{
    public class CED010201BBO : CSTDBaseReport
    {
        CED010201BDTO ErrorDTO = new CED010201BDTO();
        String BatchName = "Uplaod POS Final Batch";
        String ProcessBatchName = "Uplaod POS Final Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("UPLOAD", "FOLDER", "EXCEL_UPLOAD_FOLDER", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Uplaod POS Final Batch";

            string pathFile = reportParams[4].Split(';')[0];

            Microsoft.Office.Interop.Excel.Application xlApp;
            Microsoft.Office.Interop.Excel.Workbook xlWorkBook;
            Microsoft.Office.Interop.Excel.Worksheet xlWorkSheet;
            Microsoft.Office.Interop.Excel.Range range;

            string str;
            int rCnt;
            int cCnt;
            int rw = 0;
            int cl = 0;

            string fullpathfile = _filePathFolder + pathFile;
            if (!CheckTemplateFolder(_filePathFolder, pathFile))
            {
                logLocation = "Uploaded file not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MVPRSTD055I", new String[] { null }, "Read file, delete and insert into staging table");

            xlApp = new Microsoft.Office.Interop.Excel.Application();
            xlWorkBook = xlApp.Workbooks.Open(fullpathfile, 0, true, 5, "", "", true, Microsoft.Office.Interop.Excel.XlPlatform.xlWindows, "\t", false, false, 0, true, 1, 0);
            xlWorkSheet = (Microsoft.Office.Interop.Excel.Worksheet)xlWorkBook.Worksheets.get_Item(1);

            range = xlWorkSheet.UsedRange;
            rw = range.Rows.Count;
            cl = range.Columns.Count;

            List<CED010201BDTO> listData = new List<CED010201BDTO>();
            for (rCnt = 2; rCnt <= rw; rCnt++)
            {
                CED010201BDTO dataFrom = new CED010201BDTO();
                dataFrom.Line = Convert.ToString((range.Cells[rCnt, 1] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.Dest = Convert.ToString((range.Cells[rCnt, 2] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.ModuleNo = Convert.ToString((range.Cells[rCnt, 3] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.CaseNo = Convert.ToString((range.Cells[rCnt, 4] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.Shift = Convert.ToString((range.Cells[rCnt, 5] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.RenbanNo = Convert.ToString((range.Cells[rCnt, 6] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.ModType = Convert.ToString((range.Cells[rCnt, 7] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.Time = Convert.ToString((range.Cells[rCnt, 8] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.PackTime = Convert.ToString((range.Cells[rCnt, 9] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.Cumm = Convert.ToString((range.Cells[rCnt, 10] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.PosDate = Convert.ToString((range.Cells[rCnt, 11] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.Usage = Convert.ToString((range.Cells[rCnt, 12] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.Select = Convert.ToString((range.Cells[rCnt, 13] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.DateFinish = Convert.ToString((range.Cells[rCnt, 14] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.TimeFinish = Convert.ToString((range.Cells[rCnt, 15] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.RenbanCode = Convert.ToString((range.Cells[rCnt, 16] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.ShutterNo = Convert.ToString((range.Cells[rCnt, 17] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.ControlModuleNo = Convert.ToString((range.Cells[rCnt, 18] as Microsoft.Office.Interop.Excel.Range).Value2);
                dataFrom.IsResult = "N";
                dataFrom.UserName = userId;
                dataFrom.ProcessId = processId;

                if (CheckFormatNumber(dataFrom.Shift) == false && dataFrom.Shift != null)
                {
                    dataFrom.Shift = "-999";
                }
                if (CheckFormatNumber(dataFrom.PackTime) == false && dataFrom.PackTime != null)
                {
                    dataFrom.PackTime = "-999";
                }
                if (CheckFormatNumber(dataFrom.Usage) == false && dataFrom.Usage != null)
                {
                    dataFrom.Usage = "-999";
                }
                if (CheckFormatNumber(dataFrom.Select) == false && dataFrom.Select != null)
                {
                    dataFrom.Select = "-999";
                }
                if (CheckFormatNumber(dataFrom.ShutterNo) == false && dataFrom.ShutterNo != null)
                {
                    dataFrom.ShutterNo = "-999";
                }
                if (CheckFormatNumber(dataFrom.ControlModuleNo) == false && dataFrom.ControlModuleNo != null)
                {
                    dataFrom.ControlModuleNo = "-999";
                }

                if (CheckFormatTime(dataFrom.Time) == false && dataFrom.Time != null)
                {
                    dataFrom.Time = "00:00";
                }
                if (CheckFormatTime(dataFrom.TimeFinish) == false && dataFrom.TimeFinish != null)
                {
                    dataFrom.TimeFinish = "00:00";
                }

                if (CheckFormatDate(dataFrom.PosDate) == false && dataFrom.PosDate != null)
                {
                    dataFrom.PosDate = "19000101";
                }
                else
                {
                    dataFrom.PosDate = DateTime.FromOADate(Convert.ToDouble(dataFrom.PosDate)).ToString("yyyy-MM-dd");
                }
                if (CheckFormatDate(dataFrom.DateFinish) == false && dataFrom.DateFinish != null)
                {
                    dataFrom.DateFinish = "19000101";
                }
                else
                {
                    dataFrom.DateFinish = DateTime.FromOADate(Convert.ToDouble(dataFrom.DateFinish)).ToString("yyyy-MM-dd");
                }

                listData.Add(dataFrom);
            }

            CED010201BDAO.InsertStaging(listData);

            xlWorkBook.Close(true, null, null);
            xlApp.Quit();

            Marshal.ReleaseComObject(xlWorkSheet);
            Marshal.ReleaseComObject(xlWorkBook);
            Marshal.ReleaseComObject(xlApp);

            CED010201BSearchDTO searchDTO = new CED010201BSearchDTO();
            searchDTO.ProcessId = processId;

            IList<CED010201BDTO> rowsProcess = CED010201BDAO.GetProcessData(searchDTO);
            if (rowsProcess[0].IsResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            //check result
            //if (rowsStaging[0].IS_ERROR == "Y")
            //{
            //    string _targetDir = CSTDSystemMasterHelper.GetValue("URL_UPLOADED_FIRM_ORDER_FILE_FAIL", "UPLOAD");
            //    if (CheckTemplateFolder(_targetDir, pathFile))
            //    {
            //        System.IO.File.Delete(_targetDir + pathFile);
            //    }
            //    System.IO.File.Copy(fullpathfile, (_targetDir + pathFile));

            //    string _targetDirHistory = CSTDSystemMasterHelper.GetValue("URL_UPLOADED_FIRM_ORDER_FILE_HISTORY", "UPLOAD");
            //    if (CheckTemplateFolder(_targetDirHistory, pathFile))
            //    {
            //        System.IO.File.Delete(_targetDirHistory + pathFile);
            //    }
            //    System.IO.File.Move(fullpathfile, (_targetDirHistory + pathFile));

            //    messages.Add("failCheckingData");
            //    result["failCheckingData"] = "failCheckingData";
            //    return result;
            //}
            //else
            //{
            //    string _targetDir = CSTDSystemMasterHelper.GetValue("URL_UPLOADED_FIRM_ORDER_FILE_SUCCESS", "UPLOAD");
            //    if (CheckTemplateFolder(_targetDir, pathFile))
            //    {
            //        System.IO.File.Delete(_targetDir + pathFile);
            //    }
            //    System.IO.File.Copy(fullpathfile, (_targetDir + pathFile));

            //    string _targetDirHistory = CSTDSystemMasterHelper.GetValue("URL_UPLOADED_FIRM_ORDER_FILE_HISTORY", "UPLOAD");
            //    if (CheckTemplateFolder(_targetDirHistory, pathFile))
            //    {
            //        System.IO.File.Delete(_targetDirHistory + pathFile);
            //    }
            //    System.IO.File.Move(fullpathfile, (_targetDirHistory + pathFile));
            //}

            return result;
        }

        public bool CheckFormatNumber(string someString)
        {
            float myInt;
            bool isNumerical = float.TryParse(someString, out myInt);

            return isNumerical;
        }
        public bool CheckFormatDate(string someString)
        {
            if (CheckFormatNumber(someString) == true)
            {
                someString = DateTime.FromOADate(Convert.ToDouble(someString)).ToString("yyyy-MM-dd");
            }
            DateTime myDate;
            bool isDate = DateTime.TryParse(someString, out myDate);

            return isDate;
        }
        public bool CheckFormatTime(string someString)
        {
            TimeSpan myTime;
            bool isTime = TimeSpan.TryParse(someString, out myTime);

            return isTime;
        }
    }
}
