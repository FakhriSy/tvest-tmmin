﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021200B
{
    public class CED021200BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }
        public string PlantCd { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}
