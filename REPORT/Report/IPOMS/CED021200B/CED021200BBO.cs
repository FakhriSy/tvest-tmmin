﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.Net;
using System.Net.Mail;
using NPOI.HSSF.UserModel;
using System.IO;
using log4net;

namespace Report.IPOMS.CED021200B
{
    public class CED021200BBO : CSTDBaseReport
    {
        CED021200BDTO ErrorDTO = new CED021200BDTO();
        String BatchName = "Get Packing Indication Data From ROEM";
        String ProcessBatchName = "Get Packing Indication Data From ROEM";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";

        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Get Packing Indication Data From ROEM";

            string PlantCd = reportParams[4].Split(';')[0];
            string DateFrom = reportParams[4].Split(';')[1];
            string DateTo = reportParams[4].Split(';')[2];

            PlantCd = PlantCd == "" ? null : PlantCd;
            DateFrom = DateFrom == "" ? null : DateFrom;
            DateTo = DateTo == "" ? null : DateTo;

            CED021200BSearchDTO searchDTO = new CED021200BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.UserName = userId;
            searchDTO.PlantCd = PlantCd;
            searchDTO.DateFrom = DateFrom;
            searchDTO.DateTo = DateTo;

            CED021200BSearchDTO listData = new CED021200BSearchDTO();
            listData.ProcessId = int.Parse(processId);

            IList<CED021200BDTO> ListQtyError;

            Object[] obParams1 = new Object[10];
            obParams1[0] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "RECEIVE", CSTDSystemMasterHelper.Mandatory);
            obParams1[1] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "CC", CSTDSystemMasterHelper.Mandatory);
            obParams1[2] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "SUBJECT", CSTDSystemMasterHelper.Mandatory);
            obParams1[3] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "SMTP", CSTDSystemMasterHelper.Mandatory);
            obParams1[4] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "SENDER", CSTDSystemMasterHelper.Mandatory);
            obParams1[5] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "PORT", CSTDSystemMasterHelper.Mandatory);
            obParams1[6] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "USER", CSTDSystemMasterHelper.Mandatory);
            obParams1[7] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "PASSWORD", CSTDSystemMasterHelper.Mandatory);
            obParams1[8] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "CREDENTIALS_FLAG", CSTDSystemMasterHelper.Mandatory);
            obParams1[9] = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "PORT_FLAG", CSTDSystemMasterHelper.Mandatory);

            IList<CED021200BDTO> rowsResult = CED021200BDAO.GetResultProcessData(searchDTO);
            if (rowsResult[0].isResult == "Y")
            {
                //FID.Ridwan : 2019-010-03 Remark temporary because CR
                ListQtyError = CED021200BDAO.GetResultData(listData);
                if (ListQtyError.Count > 0)
                {
                    this.SendMail(obParams1, processId, ListQtyError);
                }
                messages.Add("fail");
                result["fail"] = "fail";
                return result;
            }

            //FID.Ridwan : 2019-010-03 Remark temporary because CR
            ListQtyError = CED021200BDAO.GetResultData(listData);
            if (ListQtyError.Count > 0)
            {
                this.SendMail(obParams1, processId, ListQtyError);
            }
            
            return result;
        }

        public string SendMail(Object[] d1, string processId, IList<CED021200BDTO> ListQty)
        {
            string result = "SUCCESS";

            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Send Email proccess is started" }, "Send Email");
                string Body = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "BODY", CSTDSystemMasterHelper.Mandatory);
                string messageBody = "<br><font>" + Body + " </font><br><br>";
                messageBody = messageBody.Replace("{0}", ListQty.Count.ToString());
                log.Info(messageBody);
                string V_MAIL_TO = d1[0].ToString();
                string V_MAIL_CC = d1[1].ToString();
                string V_MAIL_SUBJECT = d1[2].ToString();
                string SMTP_SERVER = d1[3].ToString();
                string SENDER = d1[4].ToString();
                int PORT = Convert.ToInt32(d1[5]);
                string USER = d1[6].ToString();
                string PWD = d1[7].ToString();
                string CREDENTIALS_FLAG = d1[8].ToString();
                string PORT_FLAG = d1[9].ToString();
                string Attachment = getExcelReport(ListQty, processId);

                String[] emailTo = V_MAIL_TO.Split(';');

                emailTo = emailTo.Where(val => !String.IsNullOrEmpty(val)).ToArray();

                String[] emailCc = null;
                if (!String.IsNullOrEmpty(V_MAIL_CC))
                {
                    emailCc = V_MAIL_CC.Split(';');
                }

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient(SMTP_SERVER);

                mail.From = new MailAddress(SENDER);
                //mail.To.Add(V_MAIL_TO);

                foreach (string to in emailTo)
                {
                    mail.To.Add(new MailAddress(to));
                }

                //if (V_MAIL_CC != "" && V_MAIL_CC != null)
                //{
                //    mail.CC.Add(V_MAIL_CC);
                //}

                if (emailCc != null)
                {
                    foreach (string cc in emailCc)
                    {
                        mail.CC.Add(new MailAddress(cc));
                    }
                }
                mail.Subject = V_MAIL_SUBJECT;
                mail.Body = messageBody;
                mail.IsBodyHtml = true;

                if (Attachment != "N")
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(Attachment);
                    attachment.Name = Path.GetFileName(Attachment);
                    mail.Attachments.Add(attachment);
                }

                if (PORT_FLAG == "Y")
                {
                    client.Port = PORT;
                }

                if (CREDENTIALS_FLAG == "Y")
                {
                    client.Credentials = new System.Net.NetworkCredential(USER, PWD);
                }
                
                client.EnableSsl = false;
                log.Info("Start send email");
                client.Send(mail);
                log.Info("Finish send email");
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Send Email is finish successfully" }, "Send Email");
            }
            catch (SmtpException e)
            {
                log.Info("SmtpException: " + e.Message.ToString());
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "SMTP Exception");

                result = e.Message.ToString();
            }
            catch (SqlException e)
            {
                log.Info("SqlException: " + e.Message.ToString());
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "SQL Exception");

                result = e.Message.ToString();
            }
            catch (Exception e)
            {
                log.Info("Exception: " + e.Message.ToString());
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "Exception");

                result = e.Message.ToString();
            }

            return result;
        }

        // this function is using when Excel report
        public static string getExcelReport(IList<CED021200BDTO> ListQty, string processId)
        {
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            String targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
            String templateName = "CED021200B.xls";
            String result = "N";
            String AttachFlag = "Y";
            
            //#region Check report template
            ////logLocation = "Check template";
            //if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            //{
            //    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder + templateName }), "Template not found");
                
            //    return result;
            //}
            //#endregion

            if (ListQty.Count > 0)
            {
                HSSFWorkbook workbook = InitializeWorkbook(templateName);
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");
                //style
                HSSFCellStyle iStyleLeft = (HSSFCellStyle)workbook.CreateCellStyle();
                iStyleLeft.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleLeft.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleLeft.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleLeft.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleLeft.Alignment = NPOI.SS.UserModel.HorizontalAlignment.LEFT;
                HSSFCellStyle iStyleRight = (HSSFCellStyle)workbook.CreateCellStyle();
                iStyleRight.BorderLeft = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleRight.BorderTop = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleRight.BorderRight = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleRight.BorderBottom = NPOI.SS.UserModel.BorderStyle.THIN;
                iStyleRight.Alignment = NPOI.SS.UserModel.HorizontalAlignment.RIGHT;

                //start looping
                int startRow = 2, rowIndex = startRow;
                long numberOfRow = ListQty.Count;
                long modOfRow = ListQty.Count % numberOfRow;
                long rowToRemove = numberOfRow - modOfRow;
                long startIndexRowToRemove = startRow + modOfRow;
                double rowHeight = 11.25;
                int sheetCount = 0;
                int i_seq = 0;

                foreach (CED021200BDTO row in ListQty)
                {
                    i_seq++;
                    if (rowIndex == 65535 || sheetCount == 0)
                    {
                        sheet = (HSSFSheet)workbook.CloneSheet(0);
                        sheetCount++;

                        startRow = 2;
                        rowIndex = startRow;
                        numberOfRow = ListQty.Count;
                        modOfRow = ListQty.Count % numberOfRow;
                        rowToRemove = numberOfRow - modOfRow;
                        startIndexRowToRemove = startRow + modOfRow;
                        rowHeight = 11.25;
                        workbook.SetSheetName(sheetCount, sheetCount.ToString());
                    }
                    if (rowIndex != startRow)
                    {
                        //copy row style
                        CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                    }

                    if (modOfRow > 0)
                    {
                        HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                        sheet.RemoveRow(r);
                        modOfRow--;
                    }

                    //fill up column
                    CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 0, i_seq, iStyleRight);
                    CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 1, row.PART_NO, iStyleLeft);
                    CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 2, row.PACKING_LINE_CD, iStyleLeft);
                    CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 3, row.QTY_BOX_ROEM, iStyleLeft);
                    CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 4, row.QTY_BOX_MASTER, iStyleLeft);
                    ++rowIndex;
                }

                workbook.SetSheetHidden(0, true);
                workbook.SetActiveSheet(1);

                String excelFileName = "List_PartNo_" + DateTime.Now.ToString("yyyyMMddHHmmss") + ".xls";
                String excelFullPathFileName = targetDir + excelFileName;
                FileStream file = new FileStream(excelFullPathFileName, FileMode.Create);
                workbook.Write(file);
                file.Close();

                result = excelFullPathFileName;
            }

            return result;
        }

        // this function is using when HTML report
        public static string getHtml(IList<CED021200BDTO> ListQty)
        {
            try
            {
                string Up = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "UP", CSTDSystemMasterHelper.Mandatory);
                string Down = CSTDSystemMasterHelper.GetValue("CED021200B", "EMAIL", "DOWN", CSTDSystemMasterHelper.Mandatory);
                string messageBody = "<br><br><font>" + Up + " </font><br><br>";

                string htmlTableStart = "<table style=\"border-collapse:collapse; text-align:center;\" >";
                string htmlTableEnd = "</table>";
                string htmlHeaderRowStart = "<tr style =\"background-color:#6FA1D2; color:#ffffff;\">";
                string htmlHeaderRowEnd = "</tr>";
                string htmlTrStart = "<tr style =\"color:#555555;\">";
                string htmlTrEnd = "</tr>";
                string htmlTdHeaderStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px; background-color:#373737; \">";
                string htmlTdHeaderEnd = "</td>";
                string htmlTdStart = "<td style=\" border-color:#5c87b2; border-style:solid; border-width:thin; padding: 5px; color:black; \">";
                string htmlTdEnd = "</td>";

                //Header Table
                messageBody += htmlTableStart;
                messageBody += htmlHeaderRowStart;
                messageBody += htmlTdHeaderStart + "No. " + htmlTdHeaderEnd;
                messageBody += htmlTdHeaderStart + "Part No " + htmlTdHeaderEnd;
                messageBody += htmlTdHeaderStart + "Packing Line Code " + htmlTdHeaderEnd;
                messageBody += htmlTdHeaderStart + "Qty Box ROEM " + htmlTdHeaderEnd;
                messageBody += htmlTdHeaderStart + "Qty Box Master " + htmlTdHeaderEnd;
                messageBody += htmlHeaderRowEnd;

                if (ListQty.Count == 0)
                    return messageBody;

                //Body Table
                foreach (CED021200BDTO Row in ListQty)
                {
                    messageBody = messageBody + htmlTrStart;
                    messageBody = messageBody + htmlTdStart + Row.ROW_NUM + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + Row.PART_NO + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + Row.PACKING_LINE_CD + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + Row.QTY_BOX_ROEM + htmlTdEnd;
                    messageBody = messageBody + htmlTdStart + Row.QTY_BOX_MASTER + htmlTdEnd;
                    messageBody = messageBody + htmlTrEnd;
                }

                messageBody = messageBody + htmlTableEnd;
                messageBody += "<br><br><font>" + Down + " </font>";

                return messageBody;
            }
            catch (Exception ex)
            {
                return null;
            }
        }

    }
}
