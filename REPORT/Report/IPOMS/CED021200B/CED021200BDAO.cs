﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED021200B
{
    public class CED021200BDAO
    {
        public static IList<CED021200BDTO> GetResultProcessData(CED021200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021200BDTO>("CED021200B.DoProcessData", searchDTO);
        }

        public static IList<CED021200BDTO> GetResultData(CED021200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021200BDTO>("CED021200B.DoResultData", searchDTO);
        }

        public static void DeleteStaging(CED021200BSearchDTO dataForm)
        {
            CSTDMapper.Instance().Delete("CED021200B.DeleteStaging", dataForm);
        }

    }
}
