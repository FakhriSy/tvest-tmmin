﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED021200B
{
    public class CED021200BDTO : CSTDBaseDTO
    {
        public string ROW_NUM { get; set; }
        public string PROCESS_ID { get; set; }
        public string PART_NO { get; set; }
        public string PACKING_LINE_CD { get; set; }
        public string QTY_BOX_ROEM { get; set; }
        public string QTY_BOX_MASTER { get; set; }

        public string isResult { get; set; }
    }
}
