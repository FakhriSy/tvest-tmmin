﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010506R
{
    public class CED010506RDAO
    {
        public static IList<CED010506RDTO> GetResultData(CED010506RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010506RDTO>("CED010506R.DoResultData", searchDTO);
        }
    }
}
