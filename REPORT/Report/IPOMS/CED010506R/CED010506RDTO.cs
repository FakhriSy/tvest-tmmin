﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED010506R
{
    public class CED010506RDTO : CSTDBaseDTO
    {
        public string PartNo { get; set; }
        public string Unique { get; set; }
        public string BoxNo { get; set; }
        public string ProdType { get; set; }
        public string PLaneAddress { get; set; }
        public string Supplier { get; set; }
        public string PosLine { get; set; }
        public string Shutter { get; set; }
        public string Case { get; set; }
        public string QtyBoxPlan { get; set; }
        public string QtyBoxAct { get; set; }
    }
}
