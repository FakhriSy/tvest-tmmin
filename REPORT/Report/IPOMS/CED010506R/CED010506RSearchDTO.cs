﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010506R
{
    public class CED010506RSearchDTO
    {
        public string ProdDate { get; set; }
        public string ShutterNo { get; set; }
        public string PosLine { get; set; }
        public string PartNo { get; set; }
        public string Unique { get; set; }
        public string BoxNo { get; set; }
        public string ProdType { get; set; }
        public string PLaneAddress { get; set; }
        public string Supplier { get; set; }
        public string Case { get; set; }
    }
}
