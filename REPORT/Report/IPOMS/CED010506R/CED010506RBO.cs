﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;

namespace Report.IPOMS.CED010506R
{
    public class CED010506RBO : CSTDBaseReport
    {
        CED010506RDTO ErrorDTO = new CED010506RDTO();
        String BatchName = "Supply Shutter Report Generation";
        String ProcessBatchName = "Supply Shutter Report Generation";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Supply Shutter Report Generation";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            Console.WriteLine(reportParams.Length);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            CED010506RSearchDTO searchDTO = new CED010506RSearchDTO();
            searchDTO.ProdDate = reportParams[4].Split(';')[0];
            searchDTO.ShutterNo = reportParams[4].Split(';')[1];
            searchDTO.PosLine = reportParams[4].Split(';')[2];
            searchDTO.PartNo = reportParams[4].Split(';')[3];
            searchDTO.Unique = reportParams[4].Split(';')[4];
            searchDTO.BoxNo = reportParams[4].Split(';')[5];
            searchDTO.ProdType = reportParams[4].Split(';')[6];
            searchDTO.PLaneAddress = reportParams[4].Split(';')[7];
            searchDTO.Supplier = reportParams[4].Split(';')[8];
            searchDTO.Case = reportParams[4].Split(';')[9];

            IList<CED010506RDTO> rows = CED010506RDAO.GetResultData(searchDTO);
            #region Fill up report
            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");

            //start looping
            int startRow = 7, rowIndex = startRow;
            long numberOfRow = rows.Count;
            long modOfRow = rows.Count % numberOfRow;
            long rowToRemove = numberOfRow - modOfRow;
            long startIndexRowToRemove = startRow + modOfRow;
            double rowHeight = 11.25;
            int sheetCount = 0;
            int i_seq = 0;
            foreach (CED010506RDTO row in rows)
            {
                i_seq++;
                if (rowIndex == 65535 || sheetCount == 0)
                {
                    sheet = (HSSFSheet)workbook.CloneSheet(0);
                    sheetCount++;

                    startRow = 7;
                    rowIndex = startRow;
                    numberOfRow = rows.Count;
                    modOfRow = rows.Count % numberOfRow;
                    rowToRemove = numberOfRow - modOfRow;
                    startIndexRowToRemove = startRow + modOfRow;
                    rowHeight = 11.25;
                    workbook.SetSheetName(sheetCount, sheetCount.ToString());
                }
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }

                //fill up column
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, i_seq);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.PartNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.Unique);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.BoxNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.ProdType);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.PLaneAddress);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.Supplier);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.PosLine);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.Shutter);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.Case);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.QtyBoxPlan);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.QtyBoxAct);
                ++rowIndex;
            }

            workbook.SetSheetHidden(0, true);
            workbook.SetActiveSheet(1);
            #endregion

            result["workbook"] = workbook;

            return result;
        }
    }
}
