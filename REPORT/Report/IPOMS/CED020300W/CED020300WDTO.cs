﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020300W
{
    public class CED020300WDTO
    {
        public string ROW_NO { get; set; }
        public string VANNING_PLANT { get; set; }
        public string VANNING_DATE { get; set; }
        public string DEST { get; set; }
        public string RENBAN { get; set; }
        public string ORI { get; set; }
        public string CASE_QTY { get; set; }
        public string SIZE { get; set; }
        public string VANNING_PLACE { get; set; }
        public string STATUS { get; set; }
        public string TIME { get; set; }
        public string ACTUAL { get; set; }
        public string START_TIME { get; set; }
        public string TT { get; set; }
        public string CUMM { get; set; }
        public string REMARKS { get; set; }
        public string SHIFT { get; set; }
    }
}
