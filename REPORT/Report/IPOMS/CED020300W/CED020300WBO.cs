﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;

namespace Report.IPOMS.CED020300W
{
    public class CED020300WBO : CSTDBaseReport
    {
        CED020300WDTO ErrorDTO = new CED020300WDTO();
        String BatchName = "DVP Inquiry Report";
        String ProcessBatchName = "DVP Inquiry Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {

            Dictionary<String, Object> result = new Dictionary<string, object>();
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

                List<String> messages = new List<String>();
                result["messages"] = messages;
                String reportName = "DVP Inquiry Report";
                String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
                #region Check report template
                String templateName = functionId + ".xls";

                logLocation = "Check template";
                if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
                {
                    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                    messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                    result["templateNotFound"] = "notfound";
                    return result;
                }
                #endregion

                string[] commandParams = reportParams[4].Split(';');
                DateTime parsedDate = DateTime.ParseExact(commandParams[1], "dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture);

                CED020300WSearchDTO searchDTO = new CED020300WSearchDTO
                {
                    VANNING_PLANT = commandParams[0],
                    VANNING_DATE = parsedDate.ToString("yyyy-MM-dd")
                };
                IList<CED020300WDTO> rows = CED020300WDAO.GetResultData(searchDTO);

                #region Fill up report
                HSSFWorkbook workbook = InitializeWorkbook(templateName);
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Draft");

                //--PUTRA 25 MEI 2024 START
                sheet.PrintSetup.PaperSize = (short)PaperSize.A4 + 1;
                //--PUTRA 25 MEI 2024 END

                //sheet.SetColumnWidth(0, 10 * 150);
                int lastRowIndex = 0;

                // Generate for Day shift
                var data = rows.Where(x => x.SHIFT == "1").OrderBy(x => x.START_TIME).ToList();
                GenerateExcel(sheet, data, 0, searchDTO, "1", out lastRowIndex);

                // Generate for Night shift
                data = rows.Where(x => x.SHIFT == "2").OrderBy(x => x.START_TIME).ToList();
                GenerateExcel(sheet, data, lastRowIndex < 40 ? 40 : lastRowIndex + 1, searchDTO, "2", out lastRowIndex);

                #endregion

                result["workbook"] = workbook;
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }

            return result;
        }

        private void GenerateExcel(
            HSSFSheet sheet, 
            List<CED020300WDTO> data, 
            int baseRowIndex, 
            CED020300WSearchDTO searchParams,
            string shift,
            out int lastRowIndex)
        {
            int startRow = baseRowIndex + 13;
            int rowIndex = startRow;
            int rowHeight = 18;
            int seqNo = 0;

            var parsedDate = DateTime.Parse(searchParams.VANNING_DATE);
            var idCulture = new System.Globalization.CultureInfo("id-ID");
            var idDayName = parsedDate.ToString("dddd", idCulture);

            CSTDNPOIUtil.SetCellValue(sheet, baseRowIndex + 8, 2, parsedDate.ToString("dd-MMM-yyyy"));
            CSTDNPOIUtil.SetCellValue(sheet, baseRowIndex + 8, 8, idDayName);
            CSTDNPOIUtil.SetCellValue(sheet, baseRowIndex + 8, 12, searchParams.VANNING_PLACE);

            foreach (CED020300WDTO row in data)
            {
                if (rowIndex >= (shift == "1" ? 39 : (startRow + 18)))
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow + 1, rowIndex, rowHeight);
                }

                seqNo++;
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, seqNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.DEST);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.RENBAN);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.ORI);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.CASE_QTY);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.SIZE);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.VANNING_PLACE);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.STATUS);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.TIME);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.ACTUAL);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.START_TIME);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.TT);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.CUMM);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.REMARKS);
                ++rowIndex;
            }

            CED020300WDTO lastRow;
            if (data.Any())
            {
                lastRow = data.Last();
                var finishTime = DateTime.Parse(lastRow.START_TIME).AddMinutes(Convert.ToInt32(lastRow.TT));
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, finishTime.ToString("HH:mm"));
                rowIndex++;
            }

            lastRowIndex = rowIndex;
        }
    }
}
