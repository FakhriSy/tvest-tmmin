﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED020300W
{
    public class CED020300WDAO
    {
        public static IList<CED020300WDTO> GetResultData(CED020300WSearchDTO searchDTO)
        {
            IList<CED020300WDTO> result = new List<CED020300WDTO>();
            try
            {
                result = CSTDMapper.Instance().QueryForList<CED020300WDTO>("CED020300W.DoResultData", searchDTO);
            }
            catch(Exception ex)
            {
                Console.WriteLine(ex);
            }
            return result;
        }
    }
}
