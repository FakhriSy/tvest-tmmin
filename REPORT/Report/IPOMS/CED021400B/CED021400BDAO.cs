﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED021400B
{
    public class CED021400BDAO
    {
        public static IList<CED021400BDTO> GetResultData(string ProdDate)
        {
            return CSTDMapper.Instance().QueryForList<CED021400BDTO>("CED021400B.DoResultData", ProdDate);
        }
    }
}
