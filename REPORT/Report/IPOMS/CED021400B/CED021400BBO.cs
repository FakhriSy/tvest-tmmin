﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using Common.STD.DTO;
using NPOI.SS.UserModel;

namespace Report.IPOMS.CED021400B
{
    public class CED021400BBO : CSTDBaseReport
    {
        CED021400BDTO ErrorDTO = new CED021400BDTO();
        String BatchName = "POS Prepare Module Report";
        String ProcessBatchName = "POS Prepare Module Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "POS Prepare Module Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            //Console.WriteLine(reportParams.Length);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            CED021400BSearchDTO searchDTO = new CED021400BSearchDTO();
            //searchDTO.DeliveryFrom = reportParams[4].Split(';')[0];
            try
            {
                IList<CED021400BDTO> rows = CED021400BDAO.GetResultData(reportParams[4].Split(';')[1]);
                #region Fill up report
                HSSFWorkbook workbook = InitializeWorkbook(templateName);

                //--PUTRA 25 MEI 2024 START
                var ActWsIndex = workbook.ActiveSheetIndex;
                //Active Worksheet Name

                int sumsheet = 0;
                int.TryParse(ActWsIndex.ToString(), out sumsheet);

                int sheetCount = 0;
                for (int numsheet = 0; numsheet <= sumsheet; numsheet++) {
                    var ActWsName = workbook.GetSheetName(numsheet);
                    //HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");
                    HSSFSheet sheet = (HSSFSheet)workbook.GetSheet(ActWsName); 

                    sheet.PrintSetup.PaperSize = (short)PaperSize.A3 + 1;
                    //--PUTRA 25 MEI 2024 END
                    //start looping
                    int startRow = 14, rowIndex = startRow;
                    long numberOfRow = rows.Count;
                    long modOfRow = numberOfRow > 0 ? rows.Count % numberOfRow : 0;
                    long rowToRemove = numberOfRow - modOfRow;
                    long startIndexRowToRemove = startRow + modOfRow;
                    double rowHeight = 46;
                    
                    int i_seq = 0;

                    sheet = (HSSFSheet)workbook.CloneSheet(0);
                    sheetCount++;

                    workbook.SetSheetName(sheetCount, sheetCount.ToString());

                    if (rows.Any())
                    {
                        CSTDNPOIUtil.SetCellValue(sheet, 11, 2, rows[0].ProdDate);
                        CSTDNPOIUtil.SetCellValue(sheet, 11, 10, rows[0].ProdDayName);
                        CSTDNPOIUtil.SetCellValue(sheet, 11, 15, rows[0].ProdDate);
                        CSTDNPOIUtil.SetCellValue(sheet, 11, 22, rows[0].ProdDayName);
                    }

                    foreach (CED021400BDTO row in rows)
                    {
                        i_seq++;
                        if (rowIndex == 65535 || sheetCount == 0)
                        {
                            sheet = (HSSFSheet)workbook.CloneSheet(0);
                            sheetCount++;

                            startRow = 14;
                            rowIndex = startRow;
                            numberOfRow = rows.Count;
                            modOfRow = rows.Count % numberOfRow;
                            rowToRemove = numberOfRow - modOfRow;
                            startIndexRowToRemove = startRow + modOfRow;
                            rowHeight = 46;
                            workbook.SetSheetName(sheetCount, sheetCount.ToString());
                        }
                        if (rowIndex != startRow)
                        {
                            //copy row style
                            CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                        }

                        if (modOfRow > 0)
                        {
                            HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                            sheet.RemoveRow(r);
                            modOfRow--;
                        }

                        //fill up column
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, row.No);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.PlanStart);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.Line);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.LotModuleNo);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.ModTypeOri);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.ModTypeKonversi);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.ShutterOri);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.PTime);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.CT);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.Actual);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.Problem);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.NoNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.PlanStartNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.LineNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.LotModuleNoNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.ModTypeOriNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.ModTypeKonversiNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.ShutterOriNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 20, row.PTimeNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 21, row.CTNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 22, row.ActualNight);
                        CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 23, row.ProblemNight);
                        ++rowIndex;
                    }
                //--PUTRA 25 MEI 2024 START
                }
                //--PUTRA 25 MEI 2024 END

                if (sheetCount > 0)
                {
                    workbook.SetSheetHidden(0, true);
                    workbook.SetActiveSheet(1);
                }
                #endregion
                result["workbook"] = workbook;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);

            }

            return result;
        }
    }
}
