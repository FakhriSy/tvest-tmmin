﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021400B
{
    public class CED021400BDTO
    {
        public string ProdDate { get; set; }
        public string ProdDayName { get; set; }
        public string No { get; set; }
        public string PlanStart { get; set; }
        public string Line { get; set; }
        public string LotModuleNo { get; set; }
        public string ModTypeOri { get; set; }
        public string ModTypeKonversi { get; set; }
        public string ShutterOri { get; set; }
        public string PTime { get; set; }
        public string CT { get; set; }
        public string Actual { get; set; }
        public string Problem { get; set; }
        public string NoNight { get; set; }
        public string PlanStartNight { get; set; }
        public string LineNight { get; set; }
        public string LotModuleNoNight { get; set; }
        public string ModTypeOriNight { get; set; }
        public string ModTypeKonversiNight { get; set; }
        public string ShutterOriNight { get; set; }
        public string PTimeNight { get; set; }
        public string CTNight { get; set; }
        public string ActualNight { get; set; }
        public string ProblemNight { get; set; }
    }
}
