﻿using System;
using System.Collections.Generic;
using System.Linq;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.IO;
using Common.STD.DTO;
using System.Data;
using System.Collections;
using log4net;

namespace Report.IPOMS.CED021400B
{
    public class CED021400BMain : CSTDBaseReport
    {
        private readonly ILog log = LogManager.GetLogger(typeof(CED021400BMain));

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED021400BBO ReportBO = new CED021400BBO();
            String reportBatchName = "GeneratePOS Prepare Module Report";
            String processSts = "";
            
            try
            {
                //--PUTRA 25 MEI 2024 START
                string paramrpt = reportParameters[4] ?? "";
                string[] arrstr = paramrpt.Split(';');

                string plant = CSTDSystemMasterHelper.GetValue("APPLICATION", "PLANT", "PLANT", CSTDSystemMasterHelper.Mandatory);
                //--PUTRA 25 MEI 2024 END

                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "fileNotFound");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "fileNotFound"))
                    {
                        processSts = "1";
                    }
                    else
                    {
                        processId = "2";
                    }
                }
                else
                {
                    //Console.WriteLine(reportParameters.Length);
                    String targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
                    String reportName = reportBatchName;
                    NPOI.HSSF.UserModel.HSSFWorkbook workbook = (NPOI.HSSF.UserModel.HSSFWorkbook)result["workbook"];
                    //--PUTRA 25 MEI 2024 START
                    //String excelFileName = reportName.Replace(" ", "_") + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                    string day = arrstr[1].Split('-')[0];
                    string tanggal = DateTime.ParseExact(arrstr[1], "dd-MMM-yyyy", System.Globalization.CultureInfo.InvariantCulture).ToString("dd MMMM yyyy", new System.Globalization.CultureInfo("id-ID"));
                    string excelFileName = day + ". POS Prepare Module " + tanggal + " " + plant + ".xls";
                    //--PUTRA 25 MEI 2024 END
                    String excelFullPathFileName = targetDir + excelFileName;
                    FileStream file = new FileStream(excelFullPathFileName, FileMode.Create);
                    workbook.Write(file);
                    string fileSizeStr = FileSizeFormatter.FormatSize(file.Length);
                    file.Close();
                    CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, excelFileName);
                    processSts = "0";
                    Console.WriteLine("Report successfully saved into file: " + targetDir + excelFileName);

                    #region Send Email
                    CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                    emailDTO.From = CSTDSystemMasterHelper.GetValue("CED021400B", "EMAIL_NOTIFICATION", "EMAIL_FROM", CSTDSystemMasterHelper.Mandatory);
                    emailDTO.To = CSTDSystemMasterHelper.GetValue("CED021400B", "EMAIL_NOTIFICATION", "EMAIL_TO", CSTDSystemMasterHelper.Mandatory);
                    emailDTO.CC = CSTDSystemMasterHelper.GetValue("CED021400B", "EMAIL_NOTIFICATION", "EMAIL_CC", CSTDSystemMasterHelper.Mandatory);
                    //emailDTO.Subject = CSTDSystemMasterHelper.GetValue("CED021400B", "EMAIL_NOTIFICATION", "SUBJECT", CSTDSystemMasterHelper.Mandatory) + "_" + processId;

                    IList<String> listAttachmentFiles = new List<String>();
                    listAttachmentFiles.Add(excelFullPathFileName.Replace("/", "\\"));

                    emailDTO.AttachmentFile = listAttachmentFiles;

                    SqlConnection conn = null;
                    var xmlConfig = new CSTDXMLConfig(@"SystemConfig.xml");

                    string bodyEmail = "";
                    string subjectEmail = "";

                    try
                    {
                        conn = CSTDDBUtil.GetPooledConnection;
                        conn.Open();

                        String query = "SELECT SUBJECT,BODY FROM TB_M_EMAIL WHERE POS_TYPE=" +
                                CSTDBaseDAO.LeadingSQLParameter + "Pos_type AND PLANT_CD=" +
                                CSTDBaseDAO.LeadingSQLParameter + "Plant_cd";
                        Hashtable queryParams = new Hashtable();
                        queryParams[CSTDBaseDAO.LeadingSQLParameter + "Pos_type"] = "4";
                        queryParams[CSTDBaseDAO.LeadingSQLParameter + "Plant_cd"] = xmlConfig.Get("plant_cd");
                        DataTable dt = CSTDDBUtil.ExecuteQuery(conn, query, queryParams);


                        if (dt != null && dt.Rows.Count > 0)
                        {
                            bodyEmail = dt.Rows[0]["BODY"].ToString();
                            //--PUTRA 25 MEI 2024 START
                            //subjectEmail = dt.Rows[0]["SUBJECT"].ToString();
                            subjectEmail = excelFileName.Split('.')[1];
                            //--PUTRA 25 MEI 2024 END
                        }

                    }
                    catch (SqlException e)
                    {
                             throw e;
                    }
                    catch (Exception e)
                    {
                        throw e;
                    }
                    finally
                    {
                        if (con != null && con.State == ConnectionState.Open)
                        {
                            con.Close();
                        }
                    }

                    //var headerContent = CSTDSystemMasterHelper.GetValue("CED021400B", "EMAIL_NOTIFICATION", "BODY_HEADER", CSTDSystemMasterHelper.Mandatory);
                    //var footerContent = CSTDSystemMasterHelper.GetValue("CED021400B", "EMAIL_NOTIFICATION", "BODY_FOOTER", CSTDSystemMasterHelper.Mandatory);
                    bodyEmail = bodyEmail.Replace("@@judul_laporan@@", excelFileName);
                    bodyEmail = bodyEmail.Replace("@@tanggal_unduhan@@", DateTime.Now.ToString("dd-MMM-yyyy"));
                    bodyEmail = bodyEmail.Replace("@@format@@", ".xls");
                    bodyEmail = bodyEmail.Replace("@@ukuran@@", fileSizeStr);
                    emailDTO.Content = bodyEmail;
                    //--PUTRA 25 MEI 2024 START
                    //emailDTO.Subject = subjectEmail + "_" + processId;
                    emailDTO.Subject = subjectEmail;
                    //--PUTRA 25 MEI 2024 END                    CSTDEmailUtil.SendEmail(emailDTO);
                    Console.WriteLine("Report successfully send email !");
                    #endregion Send Email

                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD007I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";

                log.Error($"Execute Report Error {e.Message}\n{e.StackTrace}\n{e.InnerException}");

                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
    public static class FileSizeFormatter
    {
        // Load all suffixes in an array
        static readonly string[] suffixes =
        { "Bytes", "KB", "MB", "GB", "TB", "PB" };
        public static string FormatSize(Int64 bytes)
        {
            int counter = 0;
            decimal number = (decimal)bytes;
            while (Math.Round(number / 1024) >= 1)
            {
                number = number / 1024;
                counter++;
            }
            return string.Format("{0:n1}{1}", number, suffixes[counter]);
        }
    }
}
