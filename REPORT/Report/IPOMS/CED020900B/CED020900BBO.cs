﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020900B
{
    public class CED020900BBO : CSTDBaseReport
    {
        CED020900BDTO ErrorDTO = new CED020900BDTO();
        String BatchName = "Uplaod POS Prepare Module Batch";
        String ProcessBatchName = "Uplaod POS Prepare Module Batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get Parameter from Batch");
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "FULLPATH_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get Configuration Setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Uplaod POS Prepare Module Batch";

            string pathFile = reportParams[4].Split(';')[0];

            string fullpathfile = _filePathFolder + pathFile;
            if (!CheckTemplateFolder(_filePathFolder, pathFile))
            {
                logLocation = "Uploaded file not found";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, logLocation);
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(pathFile);

            if (!CheckSheetName(_filePathFolder, pathFile, "POSPrepare", fileExt))
            {
                logLocation = "Sheet name error";
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, logLocation);
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            int rowno = 0;
            var prodDate = sheet.GetRow(0).GetCell(2).ToString();
            var shift = sheet.GetRow(0).GetCell(5).ToString().ToUpper().Replace(" SHIFT", "");
            if (shift == "DAY")
            {
                shift = "1";
            }
            else if (shift == "NIGHT")
            {
                shift = "2";
            }
            List<CED020900BDTO> listData = new List<CED020900BDTO>();
            for (int row = 2; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    CED020900BDTO dataFrom = new CED020900BDTO();
                    dataFrom.PROD_DT = prodDate;
                    dataFrom.SHIFT = shift;
                    dataFrom.PROD_TIME = sheet.GetRow(row).GetCell(1).ToString();
                    dataFrom.ZONE_CD = sheet.GetRow(row).GetCell(2).ToString();
                    dataFrom.LOT_MODULE_NO = sheet.GetRow(row).GetCell(3).ToString().Split('-')[0];
                    if (sheet.GetRow(row).GetCell(3).ToString().Split('-').Length > 1)
                    {
                        dataFrom.CASE_NO = sheet.GetRow(row).GetCell(3).ToString().Split('-')[1];
                    }
                    dataFrom.MOD_TYPE = sheet.GetRow(row).GetCell(4).ToString();
                    dataFrom.MOD_TYPE_CONV = sheet.GetRow(row).GetCell(5).ToString();
                    dataFrom.PREPARE_AREA = sheet.GetRow(row).GetCell(6).ToString();
                    dataFrom.PLAN_START = sheet.GetRow(row).GetCell(7).ToString();
                    dataFrom.PLAN_CT = sheet.GetRow(row).GetCell(8).ToString();
                    dataFrom.MODULE_DEST_CD = sheet.GetRow(row).GetCell(9).ToString();
                    dataFrom.CONTAINER_SNO = sheet.GetRow(row).GetCell(10).ToString();
                    dataFrom.IsResult = "N";
                    dataFrom.UserName = userId;
                    dataFrom.ProcessId = processId;
                    dataFrom.Seq = rowno.ToString();

                    listData.Add(dataFrom);
                }
            }

            CED020900BDAO.InsertStaging(listData);

            CED020900BSearchDTO searchDTO = new CED020900BSearchDTO();
            searchDTO.ProcessId = processId;
            searchDTO.Username = userId;

            IList<CED020900BDTO> rowsProcess = CED020900BDAO.GetProcessData(searchDTO);
            if (rowsProcess[0].IsResult == "Y")
            {
                messages.Add("failValidation");
                result["failValidation"] = "failValidation";
                return result;
            }

            return result;
        }
    }
}
