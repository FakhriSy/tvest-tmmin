﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020900B
{
    public class CED020900BDTO
    {
        public string PROD_DT { get; set; }
        public string SHIFT { get; set; }
        public string PROD_TIME { get; set; }
        public string ZONE_CD { get; set; }
        public string LOT_MODULE_NO { get; set; }
        public string CASE_NO { get; set; }
        public string MOD_TYPE { get; set; }
        public string MOD_TYPE_CONV { get; set; }
        public string PREPARE_AREA { get; set; }
        public string PLAN_START { get; set; }
        public string PLAN_CT { get; set; }
        public string MODULE_DEST_CD { get; set; }
        public string CONTAINER_SNO { get; set; }

        public string UserName { get; set; }
        public string ProcessId { get; set; }
        public string Seq { get; set; }
        public string IsResult { get; set; }
    }
}
