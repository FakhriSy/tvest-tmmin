﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020900B
{
    public class CED020900BSearchDTO
    {
        public string ProcessId { get; set; }
        public string Username { get; set; }
    }
}
