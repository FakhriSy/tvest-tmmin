﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED020900B
{
    public class CED020900BDAO
    {
        public static void DeleteStaging()
        {
            CSTDMapper.Instance().Delete("CED020900B.DeleteStaging", null);
        }

        public static void InsertStaging(List<CED020900BDTO> dataForm)
        {
            foreach (CED020900BDTO item in dataForm)
            {
                CED020900BDTO formData = new CED020900BDTO();
                formData.PROD_DT = item.PROD_DT;
                formData.SHIFT = item.SHIFT;
                formData.PROD_TIME = item.PROD_TIME;
                formData.ZONE_CD = item.ZONE_CD;
                formData.LOT_MODULE_NO = item.LOT_MODULE_NO;
                formData.CASE_NO = item.CASE_NO;
                formData.MOD_TYPE = item.MOD_TYPE;
                formData.MOD_TYPE_CONV = item.MOD_TYPE_CONV;
                formData.PREPARE_AREA = item.PREPARE_AREA;
                formData.PLAN_START = item.PLAN_START;
                formData.PLAN_CT = item.PLAN_CT;
                formData.MODULE_DEST_CD = item.MODULE_DEST_CD;
                formData.CONTAINER_SNO = item.CONTAINER_SNO;
                formData.UserName = item.UserName;
                formData.ProcessId = item.ProcessId;
                formData.IsResult = item.IsResult;
                formData.Seq = item.Seq;
                CSTDMapper.Instance().Insert("CED020900B.InsertStaging", formData);
            }
        }

        public static IList<CED020900BDTO> GetProcessData(CED020900BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED020900BDTO>("CED020900B.DoProcessData", searchDTO);
        }
    }
}
