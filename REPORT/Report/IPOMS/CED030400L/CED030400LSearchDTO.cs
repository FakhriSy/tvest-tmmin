﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030400L
{
    public class CED030400LSearchDTO
    {
        public string PickUpDate { get; set; }
        public string LP { get; set; }
        public string DeliveryNo { get; set; }
        public string TruckStation { get; set; }
        public string Route { get; set; }
        public string Rate { get; set; }
        public string Status { get; set; }
        public string Shift { get; set; }
    }
}
