﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030400L
{
    public class CED030400LBO : CSTDBaseReport
    {
        CED030400LDTO ErrorDTO = new CED030400LDTO();
        String BatchName = "Arrival Departure Generation Report";
        String ProcessBatchName = "Arrival Departure Generation Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Arrival Departure Generation Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Template not found";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder + templateName }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder + templateName }));
                result["templateNotFound"] = "templateNotFound";
                return result;
            }
            #endregion

            #region Get data from database
            CED030400LSearchDTO searchDTO = new CED030400LSearchDTO();
            string[] items = reportParams[4].Split(';');

            // get report parameter from table queue instead of table parameter
            searchDTO.PickUpDate = items[0].ToString();
            searchDTO.LP = items[1].ToString();
            searchDTO.DeliveryNo = items[2].ToString();
            searchDTO.TruckStation = items[3].ToString();
            searchDTO.Route = items[4].ToString();
            searchDTO.Rate = items[5].ToString();
            searchDTO.Status = items[6].ToString();
            searchDTO.Shift = items[7].ToString();

            IList<CED030400LDTO> rows = CED030400LDAO.GetReportData(searchDTO);

            //check result
            if (rows == null || rows.Count == 0)
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD001E", new String[] { "Arrival Departure" }), "Data not found");
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD001E", null));
                result["dataNotFound"] = "dataNotFound";
                return result;
            }
            #endregion

            #region Fill up report
            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");

            //start looping
            int startRow = 9, rowIndex = startRow;
            long numberOfRow = rows.Count;
            long modOfRow = rows.Count % numberOfRow;
            long rowToRemove = numberOfRow - modOfRow;
            long startIndexRowToRemove = startRow + modOfRow;
            double rowHeight = 11.25;
            int sheetCount = 0;
            int i_seq = 0;

            //Create Content
            foreach (CED030400LDTO row in rows)
            {
                i_seq++;
                if (rowIndex == 65535 || sheetCount == 0)
                {
                    sheet = (HSSFSheet)workbook.CloneSheet(0);
                    sheetCount++;

                    startRow = 9;
                    rowIndex = startRow;
                    numberOfRow = rows.Count;
                    modOfRow = rows.Count % numberOfRow;
                    rowToRemove = numberOfRow - modOfRow;
                    startIndexRowToRemove = startRow + modOfRow;
                    rowHeight = 11.25;
                    workbook.SetSheetName(sheetCount, sheetCount.ToString());
                    string DownloadBy= userId.ToString();
                    string DownloadDate = DateTime.Now.ToString("dd.MM.yyyy");
                    string TotalRecord = numberOfRow.ToString();

                    CSTDNPOIUtil.SetCellValue(sheet, 3, 4, ": " + DownloadBy);
                    CSTDNPOIUtil.SetCellValue(sheet, 4, 4, ": " + DownloadDate);
                    CSTDNPOIUtil.SetCellValue(sheet, 5, 4, ": " + TotalRecord);
                }
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }

                //fill up column
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, i_seq);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.Shift);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.TS);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.Route);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.Cycle);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.LP);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.PlanDate);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.PlanArr);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.PlanDept);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.TimePlan);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.ActualDate);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.ActualYard);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.ActualTSArr);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.ActualTSDept);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.TimeActual);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.Bal);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.SMTotal);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.KanbanTotal);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.SkidTotal);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.Problem);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 20, row.CatchUpPlan);
                ++rowIndex;
            }

            workbook.SetSheetHidden(0, true);
            workbook.SetActiveSheet(1);
            #endregion

            result["workbook"] = workbook;
            return result;
        }
    }
}
