﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030400L
{
    public class CED030400LDAO
    {
        public static IList<CED030400LDTO> GetReportData(CED030400LSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030400LDTO>("CED030400L.DoSearch", searchDTO);
        }
    }
}
