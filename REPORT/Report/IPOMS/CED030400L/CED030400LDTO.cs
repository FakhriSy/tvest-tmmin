﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030400L
{
    public class CED030400LDTO
    {
        public string Shift { get; set; }
        public string TS { get; set; }
        public string Route { get; set; }
        public string Cycle { get; set; }
        public string LP { get; set; }
        public string PlanDate { get; set; }
        public string PlanArr { get; set; }
        public string PlanDept { get; set; }
        public string TimePlan { get; set; }
        public string ActualDate { get; set; }
        public string ActualYard { get; set; }
        public string ActualTSArr { get; set; }
        public string ActualTSDept { get; set; }
        public string TimeActual { get; set; }
        public int Bal { get; set; }
        public int SMTotal { get; set; }
        public int KanbanTotal { get; set; }
        public int SkidTotal { get; set; }
        public string Problem { get; set; }
        public string CatchUpPlan { get; set; }

    }
}
