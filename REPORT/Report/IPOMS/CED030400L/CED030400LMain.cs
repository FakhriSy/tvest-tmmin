﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030400L
{
    public class CED030400LMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED030400LBO ReportBO = new CED030400LBO();
            String reportBatchName = "Arrival Departure Generation Report";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start Log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultData = result.SingleOrDefault(d => d.Key == "templateNotFound");
                    if (resultData.Value != null && resultData.Value.ToString() == "templateNotFound")
                    {
                        processSts = "1";
                    }
                    else
                    {
                        var resultDatafile = result.SingleOrDefault(d => d.Key == "dataNotFound");
                        if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "dataNotFound"))
                        {
                            processSts = "1";
                        }
                        else
                        {
                            processId = "2";
                        }
                    }
                }
                else
                {
                    Console.WriteLine(reportParameters.Length);
                    String targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
                    String reportName = reportBatchName;
                    NPOI.HSSF.UserModel.HSSFWorkbook workbook = (NPOI.HSSF.UserModel.HSSFWorkbook)result["workbook"];
                    String excelFileName = reportName.Replace(" ", "_") + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                    String excelFullPathFileName = targetDir + excelFileName;
                    FileStream file = new FileStream(excelFullPathFileName, FileMode.Create);
                    workbook.Write(file);
                    file.Close();
                    CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, excelFileName);
                    //CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD024I", new String[] { "Menu Report Generation" }, "Generated excel successfully");
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD007I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }

    }
}
