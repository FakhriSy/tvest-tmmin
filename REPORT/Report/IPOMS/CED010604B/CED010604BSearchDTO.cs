﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010604B
{
    public class CED010604BSearchDTO
    {
        public int ProcessId { get; set; }
        public string UserName { get; set; }

        //LOCKING
        public string FUNCTION_ID { get; set; }
        public string LOCK_REF { get; set; }
        public string REMARKS { get; set; }
        public string COMMAND { get; set; }
        public int TYPE { get; set; }
    }
}
