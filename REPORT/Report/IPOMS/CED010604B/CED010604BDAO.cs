﻿using System;
using System.Collections.Generic;
using Common.STD.Util;
using Report.IPOMS.CED010602B;

namespace Report.IPOMS.CED010604B
{
    public class CED010604BDAO
    {
        public static void DeleteStaging(CED010602BDTO dataForm)
        {
            CSTDMapper.Instance().Delete("CED010604B.DeleteStaging", dataForm);
        }

        public static void InsertStaging(List<CED010604BDTO> dataForm)
        {
            foreach (CED010604BDTO item in dataForm)
                CSTDMapper.Instance().Insert("CED010604B.InsertStaging", item);
        }

        public static IList<CED010604BDTO> GetResultData(CED010604BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010604BDTO>("CED010604B.DoResult", searchDTO);
        }

        public static IList<CED010604BDTO> LockingProcess(CED010604BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010604BDTO>("CED010604B.LockingProcess", searchDTO);
        }

        internal static IList<CED010604BDTO> CheckLocking(CED010604BSearchDTO lockDTO)
        {
            throw new NotImplementedException();
        }
    }
}
