﻿using Common.STD.Base;
using Common.STD.Util;
using Report.IPOMS.CED010602B;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Report.IPOMS.CED010604B
{
    public class CED010604BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED010604BBO ReportBO = new CED010604BBO();
            String reportBatchName = "Upload Forecast MVPR";
            String processSts = "";
            
            CED010604BSearchDTO LockDTO = new CED010604BSearchDTO
            {
                ProcessId = int.Parse(processId),
                FUNCTION_ID = "010604",
                LOCK_REF = DateTime.Now.ToString("yyyyMMdd"),
                REMARKS = "",
                UserName = userId,
                COMMAND = "I"
            };
            IList<CED010604BDTO> Locking;


            List<String> messages = new List<String>();

            try
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;

                // check locking
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD006I", new String[] { "Lock checking" }, "Lock checking");
                LockDTO.TYPE = 1;
                Locking = CED010604BDAO.LockingProcess(LockDTO);

                if (Locking[0].FUNCTION_LOCKED > 0)
                {
                    // locked
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD100E", new String[] { Locking[0].RUN_PROCESS_ID.ToString() }, "Locking Process");
                    messages.Add("locked");
                    result["locked"] = "locked";
                }
                else
                {
                    // insert locking
                    LockDTO.TYPE = 2;
                    CED010604BDAO.LockingProcess(LockDTO);

                    // delete staging
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD017I", new String[] { null }, "Delete Data from Staging Table");
                    CED010602BDTO dataForm = new CED010602BDTO();
                    dataForm.ProcessId = processId;
                    CED010604BDAO.DeleteStaging(dataForm);
                
                    // download file upload
                    result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                    messages = (List<String>)result["messages"];
                }
                
                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                        Console.WriteLine(message);

                    var resultLocked = result.SingleOrDefault(d => d.Key == "locked");
                    var resultDatafile = result.SingleOrDefault(d => d.Key == "fileNotFound");
                    var resultDataChecking = result.SingleOrDefault(d => d.Key == "failCheckingBasic");

                    if (resultLocked.Value != null && (resultLocked.Value.ToString() == "locked"))
                        processSts = "1";
                    else
                    {
                        if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "fileNotFound"))
                            processSts = "1";
                        else if (resultDataChecking.Value != null && resultDataChecking.Value.ToString() == "failCheckingBasic")
                            processSts = "1";
                        else
                            processSts = "2";

                        //release locking
                        CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD039I", new String[] { "TB_R_LOCK" }, "Locking Process");
                        LockDTO.TYPE = 3;
                        Locking = CED010604BDAO.LockingProcess(LockDTO);
                    }
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD008I", new String[] { reportBatchName }, "End Log");

                    //release locking
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD039I", new String[] { "TB_R_LOCK" }, "Locking Process");
                    LockDTO.TYPE = 3;
                    Locking = CED010604BDAO.LockingProcess(LockDTO);
                }

            }
            catch (Exception e)
            {
                processSts = "2";

                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "");

                //release locking
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD039I", new String[] { "TB_R_LOCK" }, "Locking Process");
                LockDTO.TYPE = 3;
                Locking = CED010604BDAO.LockingProcess(LockDTO);
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
