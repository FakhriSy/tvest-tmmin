﻿using Common.STD.Base;

namespace Report.IPOMS.CED010604B
{
    public class CED010604BDTO : CSTDBaseDTO
    {
        public string PackingPlantCode { get; set; }
        public string PackingMonth { get; set; }
        public string GroupPackingLine { get; set; }
        public string ImporterCode { get; set; }
        public string LotModuleCode { get; set; }
        public string LotCaseNo { get; set; }
        public string TotalQuantity { get; set; }
        public string OriginalLineCode { get; set; }
        public string ModuleType { get; set; }
        public string AverageBox {  get; set; }
        public string CFC { get; set; }
        public string MP {  get; set; }

        public string ProcessId { get; set; }
        public string RowNo { get; set; }
        public string UserName { get; set; }
        public string isResult { get; set; }

        public int FUNCTION_LOCKED { get; set; }
        public int RUN_PROCESS_ID { get; set; }
        public int NR_ERR { get; set; }
    }
}
