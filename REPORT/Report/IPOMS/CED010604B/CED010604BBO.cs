﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Report.IPOMS.CED010604B
{
    public class CED010604BBO : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");

            string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "FULLPATH_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            string filename = reportParams[4];

            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD046E", new String[] { fullpathfile }, "Uploaded file not found");
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD006I", new String[] { "Read file, delete and insert into staging table" }, "Read file");

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, "Forecast MVPR", fileExt))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD072E", new String[] { fullpathfile }, "Sheet name error");
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }

            int rowno = 0;
            List<CED010604BDTO> listData = new List<CED010604BDTO>();
            for (int row = 6; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;

                    CED010604BDTO dataForm = new CED010604BDTO
                    {
                        PackingPlantCode = sheet.GetRow(row).GetCell(0)?.ToString(),
                        PackingMonth = sheet.GetRow(row).GetCell(1)?.ToString(),
                        GroupPackingLine = sheet.GetRow(row).GetCell(2)?.ToString(),
                        ImporterCode = sheet.GetRow(row).GetCell(3)?.ToString(),
                        LotModuleCode = sheet.GetRow(row).GetCell(4)?.ToString(),
                        LotCaseNo = sheet.GetRow(row).GetCell(5)?.ToString(),
                        TotalQuantity = sheet.GetRow(row).GetCell(6)?.ToString(),
                        OriginalLineCode = sheet.GetRow(row).GetCell(7)?.ToString(),
                        ModuleType = sheet.GetRow(row).GetCell(8)?.ToString(),
                        AverageBox = sheet.GetRow(row).GetCell(9)?.ToString(),
                        CFC = sheet.GetRow(row).GetCell(10)?.ToString(),
                        MP = sheet.GetRow(row).GetCell(11)?.ToString(),
                        ProcessId = processId,
                        RowNo = rowno.ToString(),
                        UserName = userId,
                        isResult = "N"
                    };

                    listData.Add(dataForm);
                }
            }

            CED010604BDAO.InsertStaging(listData);

            CED010604BSearchDTO searchDTO = new CED010604BSearchDTO
            {
                ProcessId = int.Parse(processId),
                UserName = userId
            };

            IList<CED010604BDTO> rowsResult = CED010604BDAO.GetResultData(searchDTO);
            
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            return result;
        }
    }
}
