﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED050800R
{
    public class CED050800RDTO : CSTDBaseDTO
    {
        public string CTRL_MOD_NO { get; set; }
        public string PageNumber { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierPlantCode { get; set; }
        public string ArrivalTime { get; set; }
        public string ArrivalPlanTime { get; set; }
        public string DockCode { get; set; }
        public string P_LaneNo { get; set; }
        public string Conv_No { get; set; }
        public string PickingAddress { get; set; }
        public string Zone { get; set; }
        public string Plant_Name { get; set; }
        public string PartNo { get; set; }
        public string PartName { get; set; }
        public string UniqueNo { get; set; }
        public string BoxNo { get; set; }
        public string Shutter { get; set; }
        public string LotCode { get; set; }
        public string CaseNo { get; set; }
        public string PackingDate { get; set; }
        public string PcsPerKanban { get; set; }
        public string OrderNo { get; set; }
        public string KanbanIDBarcode { get; set; }
        public string KanbanID { get; set; }
        public string ImporterCode { get; set; }
        public string ImporterInfo { get; set; }
        public string RenbanNo { get; set; }
        public string KanbanLabelName { get; set; }
        public string Oya { get; set; }
        public string PrintStatus { get; set; }
        public string PrintedReprintedBy { get; set; }
        public string PrintedReprintedDate { get; set; }
        public string ImgPath { get; set; }
        public string QRCode { get; set; }
    }

    public class CED050800RHeaderDTO : CSTDBaseDTO
    {
        public string LotCode { get; set; }
        public string CaseNo { get; set; }
        public string Oya { get; set; }
        public string KanbanHeader { get; set; }
        public string KanbanHeaderOya { get; set; }
        public string KanbanIDBarcode { get; set; }
        public string PrintStatus { get; set; }
        public string PrintedReprintedBy { get; set; }
        public string PrintedReprintedDate { get; set; }
        public string ImgPath { get; set; }
    }

    public class CED050800RInsertDTO : CSTDBaseDTO
    {
        public string IsResult { get; set; }
    }

}
