﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using System.Drawing.Imaging;
using System.IO;
using System.Drawing.Printing;
using System.Management;
using Zen.Barcode;
using Microsoft.Reporting.WebForms;
using Report.Util;
using log4net;

namespace Report.IPOMS.CED050800R
{
    public class CED050800RBO : CSTDBaseReport
    {
        CED050800RDTO ErrorDTO = new CED050800RDTO();
        String BatchName = "Print Kanban 4 Report";
        String ProcessBatchName = "Print Kanban 4 Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        string Ext = ".png";

        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {

            var processSts = "";
            string NamePrinter = "";
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            string kanbanHeader = reportParams[4].Split(';')[1];

            string[] kanbanArray = kanbanHeader.Split('|');

            string printMode = reportParams[4].Split(';')[5];

            //multiple header only
            if (kanbanArray.Count() > 1 && printMode == "2")
            {
                CED050800RSearchDTO searchDTO = new CED050800RSearchDTO();
                searchDTO.FunctionID = functionId;
                searchDTO.ProsesID = reportParams[4].Split(';')[0];
                searchDTO.KanbanHeaderCd = reportParams[4].Split(';')[1];
                searchDTO.KanbanID = reportParams[4].Split(';')[2];
                searchDTO.Printer = reportParams[4].Split(';')[3].Replace("()", " ");
                searchDTO.PrintMode = reportParams[4].Split(';')[5];
                searchDTO.UserName = reportParams[4].Split(';')[4];
                searchDTO.REPRINT_REASON = reportParams[4].Split(';')[6];

                messages.Add(reportParams[4].Split(';')[3].Replace("()", " "));

                IList<CED050800RSearchDTO> RetrieveResult = CED050800RDAO.RetrieveData(searchDTO);
                if (RetrieveResult[0].IsResult.Split('|')[0] == "E")
                {
                    messages.Add("Result");
                    result["Result"] = RetrieveResult[0].IsResult.Split('|')[0];
                    return result;
                }
                else
                {
                    try
                    {
                        //3.Generate PDF file
                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Generate PDF file" }, "Executing specified process");
                        string filenameH = "Header_Kanban_Picking_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                        
                        LocalReport localReportH = new LocalReport();
                        String pathRdlc = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "TEMPLATE_RDLC", CSTDSystemMasterHelper.Mandatory);
                        String pathPng = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "PATH_BARCODE", CSTDSystemMasterHelper.Mandatory);

                        // Header
                        localReportH.ReportPath = pathRdlc + "CED050800Rpt_H.rdlc";
                        localReportH.EnableExternalImages = true;
                        ////Set Param
                        IList<CED050800RHeaderDTO> listDataH = CED050800RDAO.GetBarcodeHeader(searchDTO);
                        var iheader = "";
                        for (var i = 0; i < listDataH.Count(); i++)
                        {
                            if (listDataH[i].KanbanIDBarcode == "0")
                            {
                                if (iheader == "")
                                {
                                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Generate Barcode Header" }, "Executing specified process");
                                    iheader = "1";
                                }
                                searchDTO.KanbanHeaderCd = listDataH[i].KanbanHeader;
                                string pathBarcode = GenerateBarcode(listDataH[i].KanbanHeader, pathPng, "H", searchDTO);
                                if (pathBarcode == "E")
                                {
                                    messages.Add("Result");
                                    result["Result"] = pathBarcode;
                                    return result;
                                }
                                else
                                {
                                    listDataH[i].ImgPath = pathBarcode;
                                }
                            }
                            else
                            {
                                listDataH[i].ImgPath = pathPng + listDataH[i].KanbanHeader + Ext;
                            }
                        }

                        localReportH.Refresh();
                        ReportDataSource reportDataSourceH = new ReportDataSource("dsKanban", listDataH);

                        localReportH.DataSources.Add(reportDataSourceH);

                        string reportTypeHPDF = "PDF";
                        string mimeTypeHPDF;
                        //Setting Export to Image
                        string reportTypeH = "Image";
                        string mimeTypeH = "image/png";
                        string encodingH;
                        string fileNameExtensionH;

                        string deviceInfoH_PDF =
                      "<DeviceInfo>" +
                      "  <OutputFormat>PDF</OutputFormat>" +
                      //"  <PageWidth>21cm</PageWidth>" +
                      //"  <PageHeight>29.7cm</PageHeight>" +
                      "  <PageWidth>8.27in</PageWidth>" +
                      "  <PageHeight>3.94in</PageHeight>" +
                      "  <MarginTop>0</MarginTop>" +
                      "  <MarginLeft>0</MarginLeft>" +
                      "  <MarginRight>0</MarginRight>" +
                      "  <MarginBottom>0</MarginBottom>" +
                      "</DeviceInfo>";

                        string deviceInfoH =
                        "<DeviceInfo>" +
                        "  <OutputFormat>EMF</OutputFormat>" +
                        //"  <PageWidth>21cm</PageWidth>" +
                        //"  <PageHeight>29.7cm</PageHeight>" +
                        "  <PageWidth>8.27in</PageWidth>" +
                        "  <PageHeight>3.94in</PageHeight>" +
                        "  <MarginTop>0</MarginTop>" +
                        "  <MarginLeft>0</MarginLeft>" +
                        "  <MarginRight>0</MarginRight>" +
                        "  <MarginBottom>0</MarginBottom>" +
                        "</DeviceInfo>";

                        Warning[] warningsH;
                        string[] streamsH;
                        byte[] renderedBytesH;
                        byte[] renderedBytesHPDF;

                        //Render the report
                        renderedBytesH = localReportH.Render(
                            reportTypeH,
                            deviceInfoH,
                            out mimeTypeH,
                            out encodingH,
                            out fileNameExtensionH,
                            out streamsH,
                            out warningsH);

                        renderedBytesHPDF = localReportH.Render(
                            reportTypeHPDF,
                            deviceInfoH_PDF,
                            out mimeTypeHPDF,
                            out encodingH,
                            out fileNameExtensionH,
                            out streamsH,
                            out warningsH);

                        // CREATE generate file image header
                        if (printMode == "2")
                        {
                            System.IO.File.WriteAllBytes(pathPng + filenameH + ".PDF", renderedBytesHPDF);
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { "Generate PDF file header", pathPng + filenameH + ".PDF" }, "File generation process done successfully");
                        }
                        //end Header

                        //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { filename + ".PDF", pathPng }, "File generation process done successfully");
                        // Update PRINT_COUNTER
                        //IList<CED050800RSearchDTO> ResultUpdate = CED050800RDAO.UpdateCounter(searchDTO);
                        //if (ResultUpdate[0].IsResult.Split('|')[0] == "E")
                        //{
                        //    //messages.Add("Result");
                        //    //result["Result"] = RetrieveResult[0].IsResult.Split('|')[0];
                        //    //return result;
                        //}

                        //if (listDataH.Count() > 0)
                        //{
                        //    CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, filenameH + Ext);
                        //}
                        if (printMode == "2")
                        {
                            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD024I", new String[] { "<a href='CED010002W/DownloadFilePrint4?filename=" + filenameH + ".PDF'><font color='blue'>" + filenameH + ".PDF</font></a>" }, "File Results");
                        }
                        
                        //Send generated files into printer queueing
                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Send generated files into printer queueing" }, "Executing specified process");
                        #region SET Printer
                        try
                        {
                            String pathPrinter = CSTDSystemMasterHelper.GetValue("PRINTER", "PRINTER_NAME", "PRINTER_NAME", CSTDSystemMasterHelper.Mandatory);
                            //string NamePrinter = @"\\g07idjktsms02\"+ searchDTO.Printer;
                            //fid.ridwan
                            log.Info(searchDTO.Printer);
                            //if (searchDTO.Printer == "")
                            //{
                            //    NamePrinter = pathPrinter;
                            //}
                            //else if (pathPrinter == "")
                            //{
                            //    NamePrinter = searchDTO.Printer;
                            //}
                            //Set_Printer.SetDefaultPrinter(NamePrinter);
                            NamePrinter = searchDTO.Printer;
                            log.Info(NamePrinter);
                            #region cek printer default PERLU CEK
                            //var iprintr = "";
                            //var iprintrSET = "";
                            //PrinterSettings settings = new PrinterSettings();
                            ////settings.PrinterName = NamePrinter;
                            //foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                            //{
                            //    iprintr = iprintr + printer + ", ";
                            //    settings.PrinterName = printer;
                            //    if (settings.IsDefaultPrinter)
                            //    {
                            //        iprintrSET = printer;
                            //    }
                            //}

                            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Printers: " + iprintr }, "Executing specified process");
                            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Set Printer: " + iprintrSET }, "Executing specified process");
                            #endregion
                        }
                        catch (Exception e)
                        {
                            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD050E", new String[] { e.Message }, "Error during specified process");
                            messages.Add("Result");
                            result["Result"] = 'E';
                            return result;
                        }
                        #endregion SET printer

                        #region exec print
                        //print report header SEMENTARA DIMATIKAN SAMPE PRINTING DI SERVER SIAP
                        if (printMode == "2")
                        {
                            //var resultPrintH = PrintReport(localReportH, deviceInfoH);
                            var resultPrintH = PrintReport(localReportH, deviceInfoH, NamePrinter);
                            if (resultPrintH.Split('|')[0] == "E")
                            {
                                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD050E", new String[] { resultPrintH.Split('|')[1] }, "Error during specified process");
                                messages.Add("Result");
                                result["Result"] = resultPrintH.Split('|')[0];
                                return result;
                            }
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800002I", new String[] { "Send generated files into printer queueing", searchDTO.Printer }, "Generated header file(s) sent to printer queueing successfully");
                        }
                        #endregion

                    }
                    catch (Exception e)
                    {
                        CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD050E", new String[] { e.Message }, "Error during specified process");
                        messages.Add("Result");
                        result["Result"] = 'E';
                        return result;
                    }
                }
            }
            else
            {
                for (int xx = 0; xx < kanbanArray.Count(); xx++)
                {
                    if (kanbanArray[xx] != "")
                    {
                        CED050800RSearchDTO searchDTO = new CED050800RSearchDTO();
                        searchDTO.FunctionID = functionId;
                        searchDTO.ProsesID = reportParams[4].Split(';')[0];
                        searchDTO.KanbanHeaderCd = kanbanArray[xx]; //reportParams[4].Split(';')[1];
                        searchDTO.KanbanID = reportParams[4].Split(';')[2];
                        searchDTO.Printer = reportParams[4].Split(';')[3].Replace("()", " ");
                        searchDTO.PrintMode = reportParams[4].Split(';')[5];
                        searchDTO.UserName = reportParams[4].Split(';')[4];
                        searchDTO.REPRINT_REASON = reportParams[4].Split(';')[6];
                        //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Retrieve Kanban 4 data" }, "Executing specified process");
                        messages.Add(reportParams[4].Split(';')[3].Replace("()", " "));
                        //searchDTO.DATE_FROM = reportParams[4].Split(';')[4];
                        //searchDTO.DATE_TO = reportParams[4].Split(';')[5];

                        IList<CED050800RSearchDTO> RetrieveResult = CED050800RDAO.RetrieveData(searchDTO);
                        if (RetrieveResult[0].IsResult.Split('|')[0] == "E")
                        {
                            messages.Add("Result");
                            result["Result"] = RetrieveResult[0].IsResult.Split('|')[0];
                            return result;
                        }
                        else
                        {
                            try
                            {
                                //3.Generate PDF file
                                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Generate PDF file" }, "Executing specified process");
                                string filenameH = "Header_Kanban_Picking_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");
                                string filename = "Kanban_Picking_" + DateTime.Now.ToString("yyyyMMddHHmmssfff");

                                LocalReport localReportH = new LocalReport();
                                LocalReport localReport = new LocalReport();
                                String pathRdlc = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "TEMPLATE_RDLC", CSTDSystemMasterHelper.Mandatory);
                                String pathPng = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "PATH_BARCODE", CSTDSystemMasterHelper.Mandatory);

                                // Header
                                localReportH.ReportPath = pathRdlc + "CED050800Rpt_H.rdlc";
                                localReportH.EnableExternalImages = true;
                                ////Set Param
                                IList<CED050800RHeaderDTO> listDataH = CED050800RDAO.GetBarcodeHeader(searchDTO);
                                var iheader = "";
                                for (var i = 0; i < listDataH.Count(); i++)
                                {
                                    if (listDataH[i].KanbanIDBarcode == "0")
                                    {
                                        if (iheader == "")
                                        {
                                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Generate Barcode Header" }, "Executing specified process");
                                            iheader = "1";
                                        }
                                        searchDTO.KanbanHeaderCd = listDataH[i].KanbanHeader;
                                        string pathBarcode = GenerateBarcode(listDataH[i].KanbanHeader, pathPng, "H", searchDTO);
                                        if (pathBarcode == "E")
                                        {
                                            messages.Add("Result");
                                            result["Result"] = pathBarcode;
                                            return result;
                                        }
                                        else
                                        {
                                            listDataH[i].ImgPath = pathBarcode;
                                        }
                                    }
                                    else
                                    {
                                        listDataH[i].ImgPath = pathPng + listDataH[i].KanbanHeader + Ext;
                                    }
                                }

                                localReportH.Refresh();
                                ReportDataSource reportDataSourceH = new ReportDataSource("dsKanban", listDataH);

                                localReportH.DataSources.Add(reportDataSourceH);

                                string reportTypeHPDF = "PDF";
                                string mimeTypeHPDF;
                                //Setting Export to Image
                                string reportTypeH = "Image";
                                string mimeTypeH = "image/png";
                                string encodingH;
                                string fileNameExtensionH;

                                string deviceInfoH_PDF =
                              "<DeviceInfo>" +
                              "  <OutputFormat>PDF</OutputFormat>" +
                              //"  <PageWidth>21cm</PageWidth>" +
                              //"  <PageHeight>29.7cm</PageHeight>" +
                              "  <PageWidth>8.27in</PageWidth>" +
                              "  <PageHeight>3.94in</PageHeight>" +
                              "  <MarginTop>0</MarginTop>" +
                              "  <MarginLeft>0</MarginLeft>" +
                              "  <MarginRight>0</MarginRight>" +
                              "  <MarginBottom>0</MarginBottom>" +
                              "</DeviceInfo>";

                                string deviceInfoH =
                                "<DeviceInfo>" +
                                "  <OutputFormat>EMF</OutputFormat>" +
                                //"  <PageWidth>21cm</PageWidth>" +
                                //"  <PageHeight>29.7cm</PageHeight>" +
                                "  <PageWidth>8.27in</PageWidth>" +
                                "  <PageHeight>3.94in</PageHeight>" +
                                "  <MarginTop>0</MarginTop>" +
                                "  <MarginLeft>0</MarginLeft>" +
                                "  <MarginRight>0</MarginRight>" +
                                "  <MarginBottom>0</MarginBottom>" +
                                "</DeviceInfo>";

                                Warning[] warningsH;
                                string[] streamsH;
                                byte[] renderedBytesH;
                                byte[] renderedBytesHPDF;

                                //Render the report
                                renderedBytesH = localReportH.Render(
                                    reportTypeH,
                                    deviceInfoH,
                                    out mimeTypeH,
                                    out encodingH,
                                    out fileNameExtensionH,
                                    out streamsH,
                                    out warningsH);

                                renderedBytesHPDF = localReportH.Render(
                                    reportTypeHPDF,
                                    deviceInfoH_PDF,
                                    out mimeTypeHPDF,
                                    out encodingH,
                                    out fileNameExtensionH,
                                    out streamsH,
                                    out warningsH);

                                // CREATE generate file image header
                                if (printMode == "1" || printMode == "2")
                                {
                                    System.IO.File.WriteAllBytes(pathPng + filenameH + ".PDF", renderedBytesHPDF);
                                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { "Generate PDF file header", pathPng + filenameH + ".PDF" }, "File generation process done successfully");
                                }
                                //end Header

                                // DETAIL
                                localReport.ReportPath = pathRdlc + "CED050800Rpt.rdlc";
                                localReport.EnableExternalImages = true;
                                ////Set Param
                                IList<CED050800RDTO> listData = CED050800RDAO.GetBarcode(searchDTO);
                                var idetail = "";
                                for (var i = 0; i < listData.Count(); i++)
                                {
                                    if (listData[i].KanbanIDBarcode == "0")
                                    {
                                        if (idetail == "")
                                        {
                                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Generate Barcode Detail" }, "Executing specified process");
                                            idetail = "1";
                                        }
                                        searchDTO.KanbanID = listData[i].KanbanID;
                                        string pathBarcode = GenerateBarcode(listData[i].KanbanID, pathPng, "D", searchDTO);
                                        string pathQR = GenerateQRCode(listData[i].QRCode, pathPng);
                                        if (pathBarcode == "E")
                                        {
                                            messages.Add("Result");
                                            result["Result"] = pathBarcode;
                                            return result;
                                        }
                                        else
                                        {
                                            listData[i].ImgPath = pathBarcode + "|" + pathQR;
                                        }
                                    }
                                    else
                                    {
                                        listData[i].ImgPath = pathPng + listData[i].KanbanID + Ext + "|" + pathPng + listData[i].QRCode + Ext;
                                    }
                                }

                                localReport.Refresh();
                                ReportDataSource reportDataSource = new ReportDataSource("dsKanban", listData);

                                localReport.DataSources.Add(reportDataSource);

                                ////Setting Export to PDF
                                string reportTypePDF = "PDF";
                                string mimeTypePDF;
                                //string encoding;
                                //string fileNameExtension;

                                ////The DeviceInfo settings should be changed based on the reportType
                                ////http://msdn2.microsoft.com/en-us/library/ms155397.aspx
                                //string deviceInfo =
                                //"<DeviceInfo>" +
                                //"  <OutputFormat>PDF</OutputFormat>" +
                                //"  <PageWidth>21cm</PageWidth>" +
                                //"  <PageHeight>29.7cm</PageHeight>" +
                                //"  <MarginTop>0cm</MarginTop>" +
                                //"  <MarginLeft>0cm</MarginLeft>" +
                                //"  <MarginRight>0cm</MarginRight>" +
                                //"  <MarginBottom>0cm</MarginBottom>" +
                                //"</DeviceInfo>";

                                //Setting Export to Image
                                string reportType = "Image";
                                string mimeType = "image/png";
                                string encoding;
                                string fileNameExtension;

                                string deviceInfo_PDF =
                                "<DeviceInfo>" +
                                "  <OutputFormat>EMF</OutputFormat>" +
                                //"  <PageWidth>21cm</PageWidth>" +
                                //"  <PageHeight>29.7cm</PageHeight>" +
                                "  <PageWidth>8.27in</PageWidth>" +
                                "  <PageHeight>3.94in</PageHeight>" +
                                "  <MarginTop>0</MarginTop>" +
                                "  <MarginLeft>0</MarginLeft>" +
                                "  <MarginRight>0</MarginRight>" +
                                "  <MarginBottom>0</MarginBottom>" +
                                "</DeviceInfo>";

                                string deviceInfo =
                                "<DeviceInfo>" +
                                "  <OutputFormat>EMF</OutputFormat>" +
                                //"  <PageWidth>21cm</PageWidth>" +
                                //"  <PageHeight>29.7cm</PageHeight>" +
                                "  <PageWidth>8.27in</PageWidth>" +
                                "  <PageHeight>3.94in</PageHeight>" +
                                "  <MarginTop>0</MarginTop>" +
                                "  <MarginLeft>0</MarginLeft>" +
                                "  <MarginRight>0</MarginRight>" +
                                "  <MarginBottom>0</MarginBottom>" +
                                "</DeviceInfo>";

                                Warning[] warnings;
                                string[] streams;
                                byte[] renderedBytes;
                                byte[] renderedBytesPDF;

                                //Render the report
                                renderedBytes = localReport.Render(
                                    reportType,
                                    deviceInfo,
                                    out mimeType,
                                    out encoding,
                                    out fileNameExtension,
                                    out streams,
                                    out warnings);

                                renderedBytesPDF = localReport.Render(
                                    reportTypePDF,
                                    deviceInfo_PDF,
                                    out mimeTypePDF,
                                    out encoding,
                                    out fileNameExtension,
                                    out streams,
                                    out warnings);

                                // CREATE generate file image detail
                                if (printMode == "1" || printMode == "3")
                                {
                                    System.IO.File.WriteAllBytes(pathPng + filename + ".PDF", renderedBytesPDF);
                                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { "Generate PDF file detail", pathPng + filename + ".PDF" }, "File generation process done successfully");
                                }
                                //end detail

                                //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { filename + ".PDF", pathPng }, "File generation process done successfully");
                                // Update PRINT_COUNTER
                                //IList<CED050800RSearchDTO> ResultUpdate = CED050800RDAO.UpdateCounter(searchDTO);
                                //if (ResultUpdate[0].IsResult.Split('|')[0] == "E")
                                //{
                                //    //messages.Add("Result");
                                //    //result["Result"] = RetrieveResult[0].IsResult.Split('|')[0];
                                //    //return result;
                                //}

                                //if (listDataH.Count() > 0)
                                //{
                                //    CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, filenameH + Ext);
                                //}
                                if (printMode == "1" || printMode == "2")
                                {
                                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD024I", new String[] { "<a href='CED010002W/DownloadFilePrint4?filename=" + filenameH + ".PDF'><font color='blue'>" + filenameH + ".PDF</font></a>" }, "File Results");
                                }
                                if (printMode == "1" || printMode == "3")
                                {
                                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD024I", new String[] { "<a href='CED010002W/DownloadFilePrint4?filename=" + filename + ".PDF'><font color='blue'>" + filename + ".PDF</font></a>" }, "File Results");
                                }
                                //CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, filename + ".PDF");

                                //Send generated files into printer queueing
                                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Send generated files into printer queueing" }, "Executing specified process");
                                #region SET Printer
                                try
                                {
                                    String pathPrinter = CSTDSystemMasterHelper.GetValue("PRINTER", "PRINTER_NAME", "PRINTER_NAME", CSTDSystemMasterHelper.Mandatory);
                                    //string NamePrinter = @"\\g07idjktsms02\"+ searchDTO.Printer;
                                    log.Info(searchDTO.Printer);
                                    //fid.ridwan
                                    //if (searchDTO.Printer == "")
                                    //{
                                    //    NamePrinter = pathPrinter;
                                    //}
                                    //else if (pathPrinter == "")
                                    //{
                                    //    NamePrinter = searchDTO.Printer;
                                    //}
                                    NamePrinter = searchDTO.Printer;
                                    log.Info(NamePrinter);
                                    //Set_Printer.SetDefaultPrinter(NamePrinter);

                                    #region cek printer default PERLU CEK
                                    //var iprintr = "";
                                    //var iprintrSET = "";
                                    //PrinterSettings settings = new PrinterSettings();
                                    ////settings.PrinterName = NamePrinter;
                                    //foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                                    //{
                                    //    iprintr = iprintr + printer + ", ";
                                    //    settings.PrinterName = printer;
                                    //    if (settings.IsDefaultPrinter)
                                    //    {
                                    //        iprintrSET = printer;
                                    //    }
                                    //}

                                    //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Printers: " + iprintr }, "Executing specified process");
                                    //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Set Printer: " + iprintrSET }, "Executing specified process");
                                    #endregion
                                }
                                catch (Exception e)
                                {
                                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD050E", new String[] { e.Message }, "Error during specified process");
                                    messages.Add("Result");
                                    result["Result"] = 'E';
                                    return result;
                                }
                                #endregion SET printer

                                #region exec print
                                //print report header SEMENTARA DIMATIKAN SAMPE PRINTING DI SERVER SIAP
                                if (printMode == "1" || printMode == "2")
                                {
                                    //var resultPrintH = PrintReport(localReportH, deviceInfoH);
                                    var resultPrintH = PrintReport(localReportH, deviceInfoH, NamePrinter);
                                    if (resultPrintH.Split('|')[0] == "E")
                                    {
                                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD050E", new String[] { resultPrintH.Split('|')[1] }, "Error during specified process");
                                        messages.Add("Result");
                                        result["Result"] = resultPrintH.Split('|')[0];
                                        return result;
                                    }
                                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800002I", new String[] { "Send generated files into printer queueing", searchDTO.Printer }, "Generated header file(s) sent to printer queueing successfully");
                                }
                                //print report detail
                                if (printMode == "1" || printMode == "3")
                                {
                                    //var resultPrint = PrintReport(localReport, deviceInfo);
                                    var resultPrint = PrintReport(localReport, deviceInfo, NamePrinter);
                                    if (resultPrint.Split('|')[0] == "E")
                                    {
                                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD050E", new String[] { resultPrint.Split('|')[1] }, "Error during specified process");
                                        messages.Add("Result");
                                        result["Result"] = resultPrint.Split('|')[0];
                                        return result;
                                    }
                                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800002I", new String[] { "Send generated files into printer queueing", searchDTO.Printer }, "Generated detail file(s) sent to printer queueing successfully");
                                }
                                #endregion

                            }
                            catch (Exception e)
                            {
                                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD050E", new String[] { e.Message }, "Error during specified process");
                                messages.Add("Result");
                                result["Result"] = 'E';
                                return result;
                            }
                        }
                    }
                }
            }

            
            //update counter
            CED050800RSearchDTO searchCounter = new CED050800RSearchDTO();
            searchCounter.FunctionID = functionId;
            searchCounter.ProsesID = reportParams[4].Split(';')[0];
            searchCounter.KanbanHeaderCd = reportParams[4].Split(';')[1];
            searchCounter.KanbanID = reportParams[4].Split(';')[2];
            searchCounter.Printer = reportParams[4].Split(';')[3].Replace("()", " ");
            searchCounter.PrintMode = reportParams[4].Split(';')[5];
            searchCounter.UserName = reportParams[4].Split(';')[4];
            searchCounter.REPRINT_REASON = reportParams[4].Split(';')[6];

            IList<CED050800RSearchDTO> ResultUpdate = CED050800RDAO.UpdateCounter(searchCounter);

            messages.Add("Result");
            result["Result"] = 'I';
            return result;
        }

        public string GenerateBarcode(string keyid, string pathPng, string code, CED050800RSearchDTO searchDTO)
        {
            var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
            var image = draw.Draw(keyid, 70, 1);
            byte[] arr;
            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                arr = memStream.ToArray();
            }
            var pathFile = pathPng + keyid + Ext;
            System.IO.File.WriteAllBytes(pathFile, arr);
            System.Drawing.Image i = System.Drawing.Image.FromFile(pathFile);
            if (code == "H")
            {
                i.RotateFlip(rotateFlipType: System.Drawing.RotateFlipType.RotateNoneFlipX);
            }
            else
            {
                i.RotateFlip(rotateFlipType: System.Drawing.RotateFlipType.Rotate90FlipXY);
            }
            i.Save(pathFile);
            i.Dispose();

            searchDTO.code = code;
            IList<CED050800RSearchDTO> ResultInsert = CED050800RDAO.insertBarcode(searchDTO);
            if (ResultInsert[0].IsResult == "E")
            {
                //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001E", new String[] { "Send generated files into printer queueing" }, "Generated file not found on specified directory");
                return ResultInsert[0].IsResult;
            }

            return pathFile;
        }

        public string GenerateQRCode(string keyid, string pathPng)
        {
            CodeQrBarcodeDraw draw = BarcodeDrawFactory.CodeQr;
            var image = draw.Draw(keyid, 70, 1);
            byte[] arr;
            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                arr = memStream.ToArray();
            }

            var pathFile = pathPng + keyid + Ext;
            System.IO.File.WriteAllBytes(pathFile, arr);

            return pathFile;
        }

        //public string PrintReport(LocalReport report, string deviceInfo)
        public string PrintReport(LocalReport report, string deviceInfo, string namaPrinter)
        {
            try
            {
                PrintReport PR = new PrintReport();
                PR.ExportWithInfoNonA4(report, deviceInfo);
                PR.Print(namaPrinter);
                log.Info(namaPrinter);
                return "I|Print Success";
            }
            catch (Exception e)
            {
                return "E|" + e.Message;
            }
        }
    }
}

