﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED050800R
{
    public class CED050800RSearchDTO
    {

        public string ProsesID { get; set; }
        public string FunctionID { get; set; }
        public string KanbanHeaderCd { get; set; }
        public string KanbanID { get; set; }
        public string Printer { get; set; }
        public string UserName { get; set; }
        public string PrintMode { get; set; }
        public string REPRINT_REASON { get; set; }

        public string IsResult { get; set; }

        public string code { get; set; }

    }
}
