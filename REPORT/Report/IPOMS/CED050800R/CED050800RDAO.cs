﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED050800R
{
    public class CED050800RDAO
    {
        public static IList<CED050800RSearchDTO> RetrieveData(CED050800RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED050800RSearchDTO>("CED050800R.RetrieveData", searchDTO);
        }

        public static IList<CED050800RHeaderDTO> GetBarcodeHeader(CED050800RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED050800RHeaderDTO>("CED050800R.GetBarcodeHeader", searchDTO);
        }

        public static IList<CED050800RDTO> GetBarcode(CED050800RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED050800RDTO>("CED050800R.GetBarcode", searchDTO);
        }

        public static IList<CED050800RSearchDTO> insertBarcode(CED050800RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED050800RSearchDTO>("CED050800R.insertBarcode", searchDTO);
        }

        public static IList<CED050800RSearchDTO> UpdateCounter(CED050800RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED050800RSearchDTO>("CED050800R.UpdateCounter", searchDTO);
        }

    }
}
