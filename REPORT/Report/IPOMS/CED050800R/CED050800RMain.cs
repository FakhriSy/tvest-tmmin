﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.IO;

namespace Report.IPOMS.CED050800R
{
    public class CED050800RMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED050800RBO ReportBO = new CED050800RBO();
            String reportBatchName = "Print Kanban 4 Report";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "Result");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "E"))
                    {
                        processSts = "1";
                        CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD009I", new String[] { reportBatchName }, "End log with error");
                    }
                    else
                    {
                        processSts = "0";
                        CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD008I", new String[] { reportBatchName }, "End log Success");
                    }
                }
                //else
                //{
                //    processSts = "0";
                //    CSTDDBLogUtil.CreateLogDetail(con, processId, processSts, new String[] { reportBatchName }, "End Log");
                //}
            }
            catch (Exception e)
            {
                processSts = "2";
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MVPRSTD075E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
