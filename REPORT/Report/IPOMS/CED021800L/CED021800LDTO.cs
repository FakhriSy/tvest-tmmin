﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021800L
{
    public class CED021800LDTO
    {
        public string ExporterCode { get; set; }
        public string ExporterName { get; set; }
        public string DestCode { get; set; }
        public string ImporterName { get; set; }
        public string CountryName { get; set; }
        public string ContDest { get; set; }
        public string ContSerial { get; set; }
        public string LotMod { get; set; }
        public string PackDate { get; set; }
        public string VannDate { get; set; }
        public string GroupNo { get; set; }
        public string PathBarcode { get; set; }
    }
}
