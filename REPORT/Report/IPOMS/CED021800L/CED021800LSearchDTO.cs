﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021800L
{
    public class CED021800LSearchDTO
    {
        public string ProcessId { get; set; }
        public string UserName { get; set; }
        public string ControlModuleNo { get; set; }
        public string UserLocation { get; set; }
        public string PrinterName { get; set; }
    }
}
