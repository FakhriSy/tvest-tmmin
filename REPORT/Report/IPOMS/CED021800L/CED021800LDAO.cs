﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021800L
{
    public class CED021800LDAO
    {
        public static IList<CED021800LDTO> GetProcessData(CED021800LSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021800LDTO>("CED021800L.DoProcessData", searchDTO);
        }

        public static void InsertBatch(CED021800LSearchDTO searchDTO)
        {
            CSTDMapper.Instance().Insert("CED021800L.InsertBatchQueue", searchDTO);
        }
    }
}
