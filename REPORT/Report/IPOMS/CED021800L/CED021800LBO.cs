﻿using Common.STD.Base;
using Common.STD.Util;
using Microsoft.Reporting.WebForms;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Management;
using System.Printing;
using System.Text;
using System.Threading.Tasks;
using Zen.Barcode;

namespace Report.IPOMS.CED021800L
{
    public class CED021800LBO : CSTDBaseReport
    {
        CED021800LDTO ErrorDTO = new CED021800LDTO();
        String BatchName = "Printing Case Mark Report";
        String ProcessBatchName = "Printing Case Mark Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Printing Case Mark Report" }, "Executing specified process");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Printing Case Mark Report";

            CED021800LSearchDTO searchDTO = new CED021800LSearchDTO();
            searchDTO.ProcessId = processId;
            searchDTO.UserName = userId;
            searchDTO.UserLocation = userLocation;
            searchDTO.ControlModuleNo = reportParams[4].Split(';')[0];
            searchDTO.PrinterName = reportParams[4].Split(';')[1];

            //Console.WriteLine("Param : " + reportParams[4].ToString());

            //List<string> listPrinter = new List<string>();
            //foreach (string printer in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
            //{
            //    listPrinter.Add(printer.ToString());
            //    Console.WriteLine(printer.ToString());
            //}

            //var server = new PrintServer();
            //var queues = server.GetPrintQueues(new[] { EnumeratedPrintQueueTypes.Local, EnumeratedPrintQueueTypes.Connections });
            //foreach (string printer in queues.Select(q => q.FullName).ToList())
            //{
            //    listPrinter.Add(printer.ToString());
            //    Console.WriteLine(printer.ToString());
            //}

            //System.Management.ManagementScope objMS =
            //    new System.Management.ManagementScope(ManagementPath.DefaultPath);
            //objMS.Connect();

            //SelectQuery objQuery = new SelectQuery("SELECT * FROM Win32_Printer");
            //ManagementObjectSearcher objMOS = new ManagementObjectSearcher(objMS, objQuery);
            //System.Management.ManagementObjectCollection objMOC = objMOS.Get();

            //foreach (ManagementObject Printers in objMOC)
            //{
            //    if (Convert.ToBoolean(Printers["Local"]))       // LOCAL PRINTERS.
            //    {
            //        Console.WriteLine(Printers["Name"]);
            //    }
            //    if (Convert.ToBoolean(Printers["Network"]))     // ALL NETWORK PRINTERS.
            //    {
            //        Console.WriteLine(Printers["Name"]);
            //    }
            //}

            IList<CED021800LDTO> listData = CED021800LDAO.GetProcessData(searchDTO);
            if (listData.Count == 0 || listData == null)
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD051E", new String[] { null }, "Data TB_R_PACKING_H");
                messages.Add("failNotFound");
                result["failNotFound"] = "failNotFound";
                return result;
            }

            String _TemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            LocalReport report = new LocalReport();
            report.ReportPath = _TemplateFolder + "CED010218Rpt.rdlc";
            report.EnableExternalImages = true;
            for (var i = 0; i < listData.Count(); i++)
            {
                string pathBarcode = "file:" + GenerateBarcode(listData[i].GroupNo.Split('|')[0]);
                string pathQRCode = "file:" + GenerateQRCode(listData[i].GroupNo.Split('|')[1]);
                listData[i].PathBarcode = pathBarcode + "|" + pathQRCode;
            }
            report.Refresh();
            report.DataSources.Add(new ReportDataSource("dsData", listData));

            string typePrint = CSTDSystemMasterHelper.GetValue("PRINT", "TYPE", "TYPE", CSTDSystemMasterHelper.Mandatory);


            if (typePrint == "0")
            {
                string printerName = reportParams[4].Split(';')[1];
                for (int i = 0; i < 2; i++)
                {
                    PrintReport PR = new PrintReport();
                    PR.Export(report);
                    PR.Print(printerName);
                }
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800002I", new String[] { listData.Count.ToString(), printerName }, "Generated file(s) sent to printer queueing successfully");
            }
            else if (typePrint == "1")
            {
                string reportType = "PDF";
                string mimeType;
                string encoding;
                string fileNameExtension;

                string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>8.27in</PageWidth>" +
                "  <PageHeight>11.69in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>0.8in</MarginLeft>" +
                "  <MarginRight>0.2in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = report.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                string targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
                string pdfFileName = "";
                string pdfFullPath = "";
                for (int i = 0; i < 2; i++)
                {
                    pdfFileName = searchDTO.ControlModuleNo + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_CaseMark.pdf";
                    pdfFullPath = targetDir + pdfFileName;
                    using (FileStream fs = new FileStream(pdfFullPath, FileMode.Create))
                    {
                        fs.Write(renderedBytes, 0, renderedBytes.Length);
                    }

                    if (i == 1)
                    {
                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { listData.Count.ToString(), "Temp Directory" }, "File generation process done successfully");
                        CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, pdfFileName);
                        Console.WriteLine("Report successfully saved into file: " + targetDir + pdfFileName);
                    }
                }
            }
            //sementara
            CED021800LDAO.InsertBatch(searchDTO);

            return result;
        }

        public string GenerateBarcode(string keyid)
        {
            String _TempFolder = CSTDSystemMasterHelper.GetValue("IMAGE", "FOLDER", "BARCODE", CSTDSystemMasterHelper.Mandatory);
            var pathFile = _TempFolder + "barcode_" + keyid + ".png";
            if (!System.IO.File.Exists(pathFile))
            {
                var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
                var image = draw.Draw(keyid, 70, 1);
                byte[] arr;
                using (var memStream = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(pathFile, FileMode.Create, FileAccess.ReadWrite))
                    {
                        image.Save(memStream, ImageFormat.Png);
                        arr = memStream.ToArray();
                        fs.Write(arr, 0, arr.Length);
                    }
                }
            }

            return pathFile;
        }

        public string GenerateQRCode(string keyid)
        {
            String _TempFolder = CSTDSystemMasterHelper.GetValue("IMAGE", "FOLDER", "BARCODE", CSTDSystemMasterHelper.Mandatory);
            var pathFile = _TempFolder + "qrcode_" + keyid + ".png";
            if (!System.IO.File.Exists(pathFile))
            {
                CodeQrBarcodeDraw draw = BarcodeDrawFactory.CodeQr;
                var image = draw.Draw(keyid, 70, 1);
                byte[] arr;
                using (var memStream = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(pathFile, FileMode.Create, FileAccess.ReadWrite))
                    {
                        image.Save(memStream, ImageFormat.Png);
                        arr = memStream.ToArray();
                        fs.Write(arr, 0, arr.Length);
                    }
                }
            }

            return pathFile;
        }
    }
}
