﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021800L
{
    public class CED021800LMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED021800LBO ReportBO = new CED021800LBO();
            String reportBatchName = "Printing Case Mark Report";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start Log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDataChecking = result.SingleOrDefault(d => d.Key == "failNotFound");
                    if (resultDataChecking.Value != null && resultDataChecking.Value.ToString() == "failNotFound")
                    {
                        processSts = "1";
                    }
                    else
                    {
                        processSts = "2";
                    }
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD007I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
