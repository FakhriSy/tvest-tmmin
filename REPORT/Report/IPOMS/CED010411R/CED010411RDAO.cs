﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010411R
{
    public class CED010411RDAO
    {
        public static IList<CED010411RDTO> GetResultData(CED010411RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010411RDTO>("CED010411R.DoResultData", searchDTO);
        }
    }
}
