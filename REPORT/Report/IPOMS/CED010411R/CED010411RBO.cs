﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;

namespace Report.IPOMS.CED010411R
{
    public class CED010411RBO : CSTDBaseReport
    {
        CED010411RDTO ErrorDTO = new CED010411RDTO();
        String BatchName = "Part Passthru Data Download Report";
        String ProcessBatchName = "Part Passthru Data Download Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Part Passthru Data Download Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            Console.WriteLine(reportParams.Length);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            CED010411RSearchDTO searchDTO = new CED010411RSearchDTO();
            searchDTO.PartNo = reportParams[4].Split(';')[0];
            searchDTO.LineId = reportParams[4].Split(';')[1];
            searchDTO.PatternId = reportParams[4].Split(';')[2];
            searchDTO.DockCode = reportParams[4].Split(';')[3];
            searchDTO.PackingLine = reportParams[4].Split(';')[4];
            searchDTO.PassthruFlag = reportParams[4].Split(';')[5];
            searchDTO.PickingFlag = reportParams[4].Split(';')[6];
            searchDTO.BoxingFlag = reportParams[4].Split(';')[7];
            searchDTO.StackingFlag = reportParams[4].Split(';')[8];
            searchDTO.FloorRackFlag = reportParams[4].Split(';')[9];

            IList<CED010411RDTO> rows = CED010411RDAO.GetResultData(searchDTO);
            #region Fill up report
            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");

            //start looping
            int startRow = 7, rowIndex = startRow;
            long numberOfRow = rows.Count;
            long modOfRow = rows.Count % numberOfRow;
            long rowToRemove = numberOfRow - modOfRow;
            long startIndexRowToRemove = startRow + modOfRow;
            double rowHeight = 11.25;
            int sheetCount = 0;
            int i_seq = 0;
            foreach (CED010411RDTO row in rows)
            {
                i_seq++;
                if (rowIndex == 65535 || sheetCount == 0)
                {
                    sheet = (HSSFSheet)workbook.CloneSheet(0);
                    sheetCount++;

                    startRow = 7;
                    rowIndex = startRow;
                    numberOfRow = rows.Count;
                    modOfRow = rows.Count % numberOfRow;
                    rowToRemove = numberOfRow - modOfRow;
                    startIndexRowToRemove = startRow + modOfRow;
                    rowHeight = 11.25;
                    workbook.SetSheetName(sheetCount, sheetCount.ToString());
                }
                if (rowIndex != startRow)
                {
                    //copy row style
                    CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                }

                if (modOfRow > 0)
                {
                    HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                    sheet.RemoveRow(r);
                    modOfRow--;
                }

                //fill up column
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, i_seq);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.Part_No);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.Dock_Code);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.Line_Id);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.Pattern_Id);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.Packing_Line);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.Boxing_Flag);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.Picking_Flag);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.FloorRack_Flag);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.Stacking_Flag);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.PartType_ValidFrom);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.PartType_ValidTo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.Cycle_Time);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.Cycle_ValidFrom);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 20, row.Cycle_ValidTo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 21, row.Passthru_Flag);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 22, row.Passthru_ValidFrom);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 23, row.Passthru_ValidTo);
                ++rowIndex;
            }

            workbook.SetSheetHidden(0, true);
            workbook.SetActiveSheet(1);
            #endregion

            result["workbook"] = workbook;           

            return result;
        }
    }
}
