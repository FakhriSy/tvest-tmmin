﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED010411R
{
    public class CED010411RDTO : CSTDBaseDTO
    {
        public string Part_No { get; set; }
        public string Dock_Code { get; set; }
        public string Line_Id { get; set; }
        public string Pattern_Id { get; set; }
        public string Packing_Line { get; set; }
        public string Boxing_Flag { get; set; }
        public string Picking_Flag { get; set; }
        public string FloorRack_Flag { get; set; }
        public string Stacking_Flag { get; set; }
        public string PartType_ValidFrom { get; set; }
        public string PartType_ValidTo { get; set; }
        public decimal Cycle_Time { get; set; }
        public string Cycle_ValidFrom { get; set; }
        public string Cycle_ValidTo { get; set; }
        public string Passthru_Flag { get; set; }
        public string Passthru_ValidFrom { get; set; }
        public string Passthru_ValidTo { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Changed_By { get; set; }
        public DateTime Changed_Date { get; set; }
    }
}
