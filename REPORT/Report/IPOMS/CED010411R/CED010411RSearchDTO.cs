﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010411R
{
    public class CED010411RSearchDTO
    {
        public string PartNo { get; set; }
        public string LineId { get; set; }
        public string PatternId { get; set; }
        public string DockCode { get; set; }
        public string PackingLine { get; set; }
        public string PassthruFlag { get; set; }
        public string PickingFlag { get; set; }
        public string BoxingFlag { get; set; }
        public string StackingFlag { get; set; }
        public string FloorRackFlag { get; set; }
    }
}
