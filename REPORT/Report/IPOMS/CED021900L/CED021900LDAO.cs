﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021900L
{
    public class CED021900LDAO
    {
        public static IList<CED021900LDTO> GetProcessDataHeader(CED021900LSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021900LDTO>("CED021900L.DoProcessDataHeader", searchDTO);
        }

        public static IList<CED021900LDTODetail> GetProcessDataDetail(CED021900LSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021900LDTODetail>("CED021900L.DoProcessDataDetail", searchDTO);
        }
    }
}
