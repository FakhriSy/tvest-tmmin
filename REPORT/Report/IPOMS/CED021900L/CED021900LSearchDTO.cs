﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021900L
{
    public class CED021900LSearchDTO
    {
        public string ProcessId { get; set; }
        public string Username { get; set; }
        public string ControlModuleNo { get; set; }
    }
}
