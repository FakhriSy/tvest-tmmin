﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021900L
{
    public class CED021900LDTO
    {
        public string PackingPlant { get; set; }
        public string PackingLine { get; set; }
        public string PlantPackDate { get; set; }
        public string SeqNo { get; set; }
        public string DestCode { get; set; }
        public string CountSerialNo { get; set; }
        public string ModDest { get; set; }
        public string LotMod { get; set; }
        public string ControlModuleNo { get; set; }
        public string GrossWt { get; set; }
        public string BundleNo { get; set; }
        public string BundleWt { get; set; }
        public string GroupNo { get; set; }
        public string PathBarcode { get; set; }
    }

    public class CED021900LDTODetail
    {
        public Int64 SrNo { get; set; }
        public string BoxNo { get; set; }
        public string PartNo { get; set; }
        public int Qty { get; set; }
        public string PenBox { get; set; }
        public string PartName { get; set; }
        public string OrderNo { get; set; }
    }
}
