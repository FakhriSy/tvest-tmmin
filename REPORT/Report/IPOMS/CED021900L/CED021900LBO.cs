﻿using Common.STD.Base;
using Common.STD.Util;
using Microsoft.Reporting.WebForms;
using Report.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Zen.Barcode;

namespace Report.IPOMS.CED021900L
{
    public class CED021900LBO : CSTDBaseReport
    {
        CED021900LDTO ErrorDTO = new CED021900LDTO();
        String BatchName = "Printing Content List Report";
        String ProcessBatchName = "Printing Content List Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Printing Case Mark Report" }, "Executing specified process");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Printing Content List Report";

            CED021900LSearchDTO searchDTO = new CED021900LSearchDTO();
            searchDTO.ProcessId = processId;
            searchDTO.Username = userId;
            searchDTO.ControlModuleNo = reportParams[4].Split(';')[0];

            IList<CED021900LDTO> listData = CED021900LDAO.GetProcessDataHeader(searchDTO);
            if (listData.Count == 0 || listData == null)
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD051E", new String[] { null }, "Data TB_R_PACKING_H");
                messages.Add("failNotFound");
                result["failNotFound"] = "failNotFound";
                return result;
            }

            String _TemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            LocalReport report = new LocalReport();
            report.ReportPath = _TemplateFolder + "CED010219Rpt.rdlc";
            report.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSourceContentList);
            report.EnableExternalImages = true;
            for (var i = 0; i < listData.Count(); i++)
            {
                string pathBarcode = "file:" + GenerateBarcode(listData[i].GroupNo);
                listData[i].PathBarcode = pathBarcode;
            }
            report.Refresh();
            report.DataSources.Add(new ReportDataSource("dsHeader", listData));

            string typePrint = CSTDSystemMasterHelper.GetValue("PRINT", "TYPE", "TYPE", CSTDSystemMasterHelper.Mandatory);
            if (typePrint == "0")
            {
                string printerName = reportParams[4].Split(';')[1];
                PrintReport PR = new PrintReport();
                PR.Export(report);
                PR.Print(printerName);
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800002I", new String[] { listData.Count.ToString(), printerName }, "Generated file(s) sent to printer queueing successfully");
            }
            else if (typePrint == "1")
            {
                string reportType = "PDF";
                string mimeType;
                string encoding;
                string fileNameExtension;

                string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>PDF</OutputFormat>" +
                "  <PageWidth>8.27in</PageWidth>" +
                "  <PageHeight>11.69in</PageHeight>" +
                "  <MarginTop>0.5in</MarginTop>" +
                "  <MarginLeft>0.8in</MarginLeft>" +
                "  <MarginRight>0.2in</MarginRight>" +
                "  <MarginBottom>0.5in</MarginBottom>" +
                "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                renderedBytes = report.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                string targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
                string pdfFileName = searchDTO.ControlModuleNo + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_ContentList.pdf";
                string pdfFullPath = targetDir + pdfFileName;
                using (FileStream fs = new FileStream(pdfFullPath, FileMode.Create))
                {
                    fs.Write(renderedBytes, 0, renderedBytes.Length);
                }
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001I", new String[] { listData.Count.ToString(), "Temp Directory" }, "File generation process done successfully");
                CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, pdfFileName);
                Console.WriteLine("Report successfully saved into file: " + targetDir + pdfFileName);
            }

            return result;
        }

        public void SetSubDataSourceContentList(object sender, SubreportProcessingEventArgs e)
        {
            var param = e.Parameters["Param"].Values.First().ToString();
            CED021900LSearchDTO searchDTO = new CED021900LSearchDTO();
            searchDTO.ControlModuleNo = param;
            IList<CED021900LDTODetail> listDataDetail = CED021900LDAO.GetProcessDataDetail(searchDTO);
            e.DataSources.Add(new ReportDataSource("dsDetail", listDataDetail));
        }

        public string GenerateBarcode(string keyid)
        {
            String _TempFolder = CSTDSystemMasterHelper.GetValue("IMAGE", "FOLDER", "BARCODE", CSTDSystemMasterHelper.Mandatory);
            var pathFile = _TempFolder + "barcode_" + keyid + ".png";
            if (!System.IO.File.Exists(pathFile))
            {
                var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
                var image = draw.Draw(keyid, 70, 1);
                byte[] arr;
                using (var memStream = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(pathFile, FileMode.Create, FileAccess.ReadWrite))
                    {
                        image.Save(memStream, ImageFormat.Png);
                        arr = memStream.ToArray();
                        fs.Write(arr, 0, arr.Length);
                    }
                }
            }

            return pathFile;
        }
    }
}
