﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010203R
{
    public class CED010203RDTO
    {
        public string QtyPcs { get; set; }
        public string QtyBox { get; set; }
        public string Dest { get; set; }
        public string LotMod { get; set; }
        public string Case { get; set; }
        public string Shift { get; set; }
        public string Renban { get; set; }
        public string ModType { get; set; }
        public string M3 { get; set; }
        public string Ratio { get; set; }
        public string PlanTime { get; set; }
        public string PlanCT { get; set; }
        public string PackProgressProc { get; set; }
        public string PackProgressAct { get; set; }
        public string PackProgressDelay { get; set; }
        public string No { get; set; }
        public string PackResultStart { get; set; }
        public string PackResultFinish { get; set; }
        public string PackResultAct { get; set; }
        public string PackResultBlc { get; set; }
        public string PackResultCumm { get; set; }
        public string PPShop { get; set; }
        public string PPStacking { get; set; }
        public string PPVanning { get; set; }
        public string PPTool { get; set; }
        public string MRModul { get; set; }
        public string MRPart { get; set; }
        public string MRKanban { get; set; }
        public string MQKarat { get; set; }
        public string MQDefect { get; set; }
        public string MQMiss { get; set; }
        public string MQShortage { get; set; }
        public string Remark { get; set; }
    }

    public class CED010203R_POSFinalDTO
    {
        public string LINE { get; set; }
        public string DEST { get; set; }
        public string MODULE_NO { get; set; }
        public string CASE_NO { get; set; }
        public int? SHIFT { get; set; }
        public string RENBAN_NO { get; set; }
        public string MOD_TYPE { get; set; }
        public string TIME { get; set; }
        public int? PACK_TIME { get; set; }
        public string CUMM { get; set; }
        public string POS_DATE { get; set; }
        public string USAGE { get; set; }
        public string SELECT { get; set; }
        public string DATE_FINISH { get; set; }
        public string TIME_FINISH { get; set; }
        public string RENBAN_CODE { get; set; }
        public string SHOOTER_NO { get; set; }
    }

    public class CED010203R_POSPrepareDTO
    {
        public decimal? NO { get; set; }
        public string PROD_TIME { get; set; }
        public string LINE { get; set; }
        public string ZONE_CD { get; set; }
        public string MODULE_NO { get; set; }
        public string CASE_NO { get; set; }
        public int? SHIFT { get; set; }
        public string MOD_TYPE { get; set; }
        public string MOD_TYPE_CONV { get; set; }
        public string SHOOTER_ORI { get; set; }
        public string TIME { get; set; }
        public int? CT { get; set; }
        public string DEST_CD { get; set; }
        public string RENBAN_NO { get; set; }
    }

    public class CED010203R_POSJundateDTO
    {
        public string LINE { get; set; }
        public string POS_DATE { get; set; }
        public string SHOOTER_NO { get; set; }
        public string DEST { get; set; }
        public string MODULE_NO { get; set; }
        public string CASE_NO { get; set; }
        public int? SHIFT { get; set; }
        public string RENBAN_NO { get; set; }
        public string MOD_TYPE { get; set; }
        public string DOLLY_1 { get; set; }
        public string DOLLY_2 { get; set; }
        public string DOLLY_3 { get; set; }
        public string DOLLY_4 { get; set; }
        public string DOLLY_5 { get; set; }
        public string DOLLY_6 { get; set; }
        public string DOLLY_7 { get; set; }
        public string DOLLY_8 { get; set; }
        public string DOLLY_9 { get; set; }
        public string PLAN_START { get; set; }
        public int? CT { get; set; }
    }
}
