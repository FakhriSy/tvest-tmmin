﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010203R
{
    public class CED010203RDAO
    {
        public static IList<CED010203RDTO> GetResultData(CED010203RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010203RDTO>("CED010203R.DoResultData", searchDTO);
        }

        public static IList<CED010203R_POSFinalDTO> GetResultDataFinal(CED010203RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010203R_POSFinalDTO>("CED010203R.DoResultDataFinal", searchDTO);
        }

        public static IList<CED010203R_POSPrepareDTO> GetResultDataPrepare(CED010203RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010203R_POSPrepareDTO>("CED010203R.DoResultDataPrepare", searchDTO);
        }

        public static IList<CED010203R_POSJundateDTO> GetResultDataJundate(CED010203RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010203R_POSJundateDTO>("CED010203R.DoResultDataJundate", searchDTO);
        }
    }
}
