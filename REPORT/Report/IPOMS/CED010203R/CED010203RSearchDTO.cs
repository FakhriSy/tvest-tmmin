﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010203R
{
    public class CED010203RSearchDTO
    {
        public string ProdDate { get; set; }
        public string ProdLine { get; set; }
        public string Shift { get; set; }
    }
}
