﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;

namespace Report.IPOMS.CED010203R
{
    public class CED010203RBO : CSTDBaseReport
    {
        CED010203RDTO ErrorDTO = new CED010203RDTO();
        String BatchName = "Generate POS Daily Report";
        String ProcessBatchName = "Generate POS Daily Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Generate POS Daily Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            Console.WriteLine(reportParams.Length);
            #region Check report template
            var posType = reportParams[4].Split(';')[0];
            String templateName = "";
            if (posType == "1")
            {
                templateName = "CED020100R.xls";
            }
            else if (posType == "2")
            {
                templateName = "CED021100R.xls";
            }
            else if (posType == "3")
            {
                templateName = "CED020900R.xls";
            }

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            CED010203RSearchDTO searchDTO = new CED010203RSearchDTO();
            searchDTO.ProdDate = reportParams[4].Split(';')[1];
            searchDTO.ProdLine = reportParams[4].Split(';')[2];
            searchDTO.Shift = reportParams[4].Split(';')[3];

            HSSFWorkbook workbook = InitializeWorkbook(templateName);

            IList<CED010203R_POSFinalDTO> rowsFinal = null;
            IList<CED010203R_POSPrepareDTO> rowsPrepare = null;
            IList<CED010203R_POSJundateDTO> rowsJundate = null;
            if (posType == "1")
            {
                rowsFinal = CED010203RDAO.GetResultDataFinal(searchDTO);
                if (rowsFinal.Count == 0)
                {
                    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }), "Get Data");
                    messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }));
                    result["dataNotFound"] = "dataNotFound";
                    return result;
                }

                #region Fill up report POS Final
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("POSFinal");

                //start looping
                int startRow = 1, rowIndex = startRow;
                long numberOfRow = rowsFinal.Count;
                long modOfRow = rowsFinal.Count % numberOfRow;
                long rowToRemove = numberOfRow - modOfRow;
                long startIndexRowToRemove = startRow + modOfRow;
                double rowHeight = 11.25;
                int sheetCount = 0;
                int i_seq = 0;
                foreach (CED010203R_POSFinalDTO row in rowsFinal)
                {
                    i_seq++;
                    if (rowIndex == 65535 || sheetCount == 0)
                    {
                        sheet = (HSSFSheet)workbook.CloneSheet(0);
                        sheetCount++;

                        startRow = 1;
                        rowIndex = startRow;
                        numberOfRow = rowsFinal.Count;
                        modOfRow = rowsFinal.Count % numberOfRow;
                        rowToRemove = numberOfRow - modOfRow;
                        startIndexRowToRemove = startRow + modOfRow;
                        rowHeight = 11.25;
                        //workbook.SetSheetName(sheetCount, sheetCount.ToString());
                        workbook.SetSheetName(sheetCount, "POSFinal");
                    }
                    if (rowIndex != startRow)
                    {
                        //copy row style
                        CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                    }

                    if (modOfRow > 0)
                    {
                        HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                        sheet.RemoveRow(r);
                        modOfRow--;
                    }

                    //fill up column
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, i_seq);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.LINE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.DEST);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.MODULE_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.CASE_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.SHIFT);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.RENBAN_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.MOD_TYPE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.TIME);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.PACK_TIME);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.CUMM);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.POS_DATE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.USAGE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.SELECT);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.DATE_FINISH);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.TIME_FINISH);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.RENBAN_CODE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.SHOOTER_NO);
                    ++rowIndex;
                }

                workbook.RemoveSheetAt(workbook.GetSheetIndex("Report"));
                #endregion
            }
            else if (posType == "2")
            {
                rowsJundate = CED010203RDAO.GetResultDataJundate(searchDTO);
                if (rowsJundate.Count == 0)
                {
                    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }), "Get Data");
                    messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }));
                    result["dataNotFound"] = "dataNotFound";
                    return result;
                }

                #region Fill up report POS Jundate
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("POSJundate");

                //start looping
                int startRow = 1, rowIndex = startRow;
                long numberOfRow = rowsJundate.Count;
                long modOfRow = rowsJundate.Count % numberOfRow;
                long rowToRemove = numberOfRow - modOfRow;
                long startIndexRowToRemove = startRow + modOfRow;
                double rowHeight = 11.25;
                int sheetCount = 0;
                int i_seq = 0;
                foreach (CED010203R_POSJundateDTO row in rowsJundate)
                {
                    i_seq++;
                    if (rowIndex == 65535 || sheetCount == 0)
                    {
                        sheet = (HSSFSheet)workbook.CloneSheet(0);
                        sheetCount++;

                        startRow = 1;
                        rowIndex = startRow;
                        numberOfRow = rowsJundate.Count;
                        modOfRow = rowsJundate.Count % numberOfRow;
                        rowToRemove = numberOfRow - modOfRow;
                        startIndexRowToRemove = startRow + modOfRow;
                        rowHeight = 11.25;
                        //workbook.SetSheetName(sheetCount, sheetCount.ToString());
                        workbook.SetSheetName(sheetCount, "POSJundate");
                    }
                    if (rowIndex != startRow)
                    {
                        //copy row style
                        CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                    }

                    if (modOfRow > 0)
                    {
                        HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                        sheet.RemoveRow(r);
                        modOfRow--;
                    }

                    //fill up column
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, row.LINE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.POS_DATE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.SHOOTER_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.DEST);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.MODULE_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.CASE_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.SHIFT);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.RENBAN_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.MOD_TYPE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.DOLLY_1);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.DOLLY_2);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.DOLLY_3);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.DOLLY_4);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.DOLLY_5);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.DOLLY_6);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 15, row.DOLLY_7);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 16, row.DOLLY_8);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 17, row.DOLLY_9);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 18, row.PLAN_START);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 19, row.CT);
                    ++rowIndex;
                }

                workbook.RemoveSheetAt(workbook.GetSheetIndex("Report"));
                #endregion
            }
            else if (posType == "3")
            {
                rowsPrepare = CED010203RDAO.GetResultDataPrepare(searchDTO);
                if (rowsPrepare.Count == 0)
                {
                    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }), "Get Data");
                    messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }));
                    result["dataNotFound"] = "dataNotFound";
                    return result;
                }

                #region Fill up report POS Prepare
                HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("POSPrepare");

                //start looping
                int startRow = 2, rowIndex = startRow;
                long numberOfRow = rowsPrepare.Count;
                long modOfRow = rowsPrepare.Count % numberOfRow;
                long rowToRemove = numberOfRow - modOfRow;
                long startIndexRowToRemove = startRow + modOfRow;
                double rowHeight = 11.25;
                int sheetCount = 0;
                int i_seq = 0;
                foreach (CED010203R_POSPrepareDTO row in rowsPrepare)
                {
                    i_seq++;
                    if (rowIndex == 65535 || sheetCount == 0)
                    {
                        sheet = (HSSFSheet)workbook.CloneSheet(0);
                        sheetCount++;

                        startRow = 2;
                        rowIndex = startRow;
                        numberOfRow = rowsPrepare.Count;
                        modOfRow = rowsPrepare.Count % numberOfRow;
                        rowToRemove = numberOfRow - modOfRow;
                        startIndexRowToRemove = startRow + modOfRow;
                        rowHeight = 11.25;
                        //workbook.SetSheetName(sheetCount, sheetCount.ToString());
                        workbook.SetSheetName(sheetCount, "POSPrepare");
                    }
                    if (rowIndex != startRow)
                    {
                        //copy row style
                        CopyRowWithHeight(sheet, startRow, rowIndex, rowHeight);
                    }

                    if (modOfRow > 0)
                    {
                        HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                        sheet.RemoveRow(r);
                        modOfRow--;
                    }

                    //fill up column
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, row.NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, row.PROD_TIME);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, row.LINE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.MODULE_NO);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.MOD_TYPE);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.MOD_TYPE_CONV);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.SHOOTER_ORI);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.TIME);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.CT);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.DEST_CD);
                    CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.RENBAN_NO);
                    ++rowIndex;
                }

                workbook.RemoveSheetAt(workbook.GetSheetIndex("Report"));
                #endregion
            }

            result["workbook"] = workbook;

            return result;
        }
    }
}
