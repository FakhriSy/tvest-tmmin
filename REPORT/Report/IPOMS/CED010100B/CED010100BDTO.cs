﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;

namespace Report.IPOMS.CED010100B
{
    public class CED010100BDTO : CSTDBaseDTO
    {
        public string isResult { get; set; }
    }
}
