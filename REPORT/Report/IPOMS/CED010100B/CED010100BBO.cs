﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;

namespace Report.IPOMS.CED010100B
{
    public class CED010100BBO : CSTDBaseReport
    {
        CED010100BDTO ErrorDTO = new CED010100BDTO();
        String BatchName = "Cycle Time Calculation D-Line";
        String ProcessBatchName = "Cycle Time Calculation D-Line";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Cycle Time Calculation D-Line";

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Parameter" }, "Get Default Data Reference");

            CED010100BSearchDTO sDTO = new CED010100BSearchDTO();
            sDTO.ProcessId = int.Parse(processId);

            IList<CED010100BDTO> rowsResult = CED010100BDAO.GetResultProcessData(sDTO);
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("failValidation");
                result["failValidation"] = "failValidation";
                return result;
            }

            return result;
        }
    }
}
