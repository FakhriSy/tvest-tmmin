﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010100B
{
    public class CED010100BDAO
    {
        public static IList<CED010100BDTO> GetResultProcessData(CED010100BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010100BDTO>("CED010100B.DoProcessData", sDTO);
        }
    }
}
