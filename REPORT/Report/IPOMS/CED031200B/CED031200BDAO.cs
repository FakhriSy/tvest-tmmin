﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED031200B
{
    public class CED031200BDAO
    {
        public static IList<CED031200BDTO> GetResultProcessData(CED031200BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED031200BDTO>("CED031200B.DoProcessData", searchDTO);
        }
    }
}
