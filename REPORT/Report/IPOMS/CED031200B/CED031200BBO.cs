﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;

namespace Report.IPOMS.CED031200B
{
    public class CED031200BBO : CSTDBaseReport
    {
        CED031200BDTO ErrorDTO = new CED031200BDTO();
        String BatchName = "Create Kanban 2";
        String ProcessBatchName = "Create Kanban 2";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Create Kanban 2";

            CED031200BSearchDTO searchDTO = new CED031200BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);

            IList<CED031200BDTO> rowsResult = CED031200BDAO.GetResultProcessData(searchDTO);
            //if (rowsResult[0].isResult == "Y")
            //{

            //}

            return result;
        }
    }
}
