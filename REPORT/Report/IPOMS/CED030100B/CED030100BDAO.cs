﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED030100B
{
    public class CED030100BDAO
    {
        public static IList<CED030100BDTO> GetResultProcessData(CED030100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030100BDTO>("CED030100B.DoProcessData", searchDTO);
        }

        public static IList<CED030100BDTO> GetResultCheckManifestDouble(CED030100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030100BDTO>("CED030100B.CheckManifestDouble", searchDTO);
        }

        public static IList<CED030100BDTO> GetResultProcessDataWS(CED030100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030100BDTO>("CED030100B.DoProcessDataWS", searchDTO);
        }

        public static IList<CED030100BDTO> CheckLocking(CED030100BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED030100BDTO>("CED030100B.CheckLocking", searchDTO);
        }

    }
}
