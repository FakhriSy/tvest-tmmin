﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.Data;
using System.Reflection;
using System.ComponentModel;

namespace Report.IPOMS.CED030100B
{
    public class CED030100BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED030100BBO ReportBO = new CED030100BBO();
            String reportBatchName = "Get Data from IPPCS (Daily)";
            String processSts = "";

            CED030100BSearchDTO LockDTO = new CED030100BSearchDTO();
            LockDTO.ProcessId = int.Parse(processId);
            LockDTO.FUNCTION_ID = "CED030100B";
            LockDTO.LOCK_REF = DateTime.Now.ToString("yyyyMMdd");
            LockDTO.REMARKS = "";
            LockDTO.UserName = userId;
            LockDTO.COMMAND = "I";
            LockDTO.TYPE = 3;
            IList<CED030100BDTO> Locking = null;

            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;

                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "failgetdata");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "failgetdata"))
                    {
                        processSts = "1";
                        //release locking
                        Locking = CED030100BDAO.CheckLocking(LockDTO);
                    }
                    else
                    {
                        processSts = "2";
                        //release locking
                        Locking = CED030100BDAO.CheckLocking(LockDTO);
                    }
                }
                else
                {
                    processSts = "0";
                    //release locking
                    Locking = CED030100BDAO.CheckLocking(LockDTO);
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD007I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                //release locking
                Locking = CED030100BDAO.CheckLocking(LockDTO);
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }

    }
}
