﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED030100B
{
    public class CED030100BSearchDTO
    {
        public int ProcessId { get; set; }
        public int EOFlag { get; set; }
        public string UserName { get; set; }

        //LOCKING
        public string FUNCTION_ID { get; set; }
        public string LOCK_REF { get; set; }
        public string REMARKS { get; set; }
        public string COMMAND { get; set; }
        public int TYPE { get; set; }
    }
}
