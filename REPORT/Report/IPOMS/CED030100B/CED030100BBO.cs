﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using System.Collections;
using Common.STD.DTO;
using log4net;

namespace Report.IPOMS.CED030100B
{
    public class CED030100BBO : CSTDBaseReport
    {
        CED030100BDTO ErrorDTO = new CED030100BDTO();
        String BatchName = "Get Data From IPPCS (Daily)";
        String ProcessBatchName = "Get Data From IPPCS (Daily)";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";

        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get parameter from Batch Queue" }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022I", new String[] { "Check EO flag from parameter", (reportParams[4].Split(';')[0] == "1") ? "TRUE" : "FALSE" }, "Check EO flag from parameter");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get configuration setting" }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Get Data From IPPCS (Daily)";
            int EOFlag = int.Parse(reportParams[4].Split(';')[0]);

            CED030100BSearchDTO searchDTO = new CED030100BSearchDTO();
            searchDTO.ProcessId = int.Parse(processId);
            searchDTO.EOFlag = EOFlag;
            searchDTO.UserName = userId;

            IList<CED030100BDTO> rowsResult = null;

            int flag = Convert.ToInt32(CSTDSystemMasterHelper.GetValue("CED030100B", "WEB_SERVICE", "FLAG", CSTDSystemMasterHelper.Mandatory));

            CED030100BSearchDTO LockDTO = new CED030100BSearchDTO();
            LockDTO.ProcessId = int.Parse(processId);
            LockDTO.FUNCTION_ID = "CED030100B";
            LockDTO.LOCK_REF = DateTime.Now.ToString("yyyyMMdd");
            LockDTO.REMARKS = "";
            LockDTO.UserName = userId;
            LockDTO.COMMAND = "I";
            LockDTO.TYPE = 1;

            //check locking
            IList<CED030100BDTO> Locking = CED030100BDAO.CheckLocking(LockDTO);

            if (Locking[0].FUNCTION_LOCKED > 0)
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD100E", new String[] { Locking[0].RUN_PROCESS_ID.ToString() }, "Locking Process");
                messages.Add("failgetdata");
                result["failgetdata"] = "failgetdata";
                return result;
            }
            else
            {
                //insert locking
                LockDTO.TYPE = 2;
                Locking = CED030100BDAO.CheckLocking(LockDTO);
                if (flag == 1)
                {
                    //Web Service  
                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Using method Web Service" }, "Get Method");

                    #region Logic get data from WS IPPCS
                    String tableName = "";
                    Hashtable param = new Hashtable();
                    Dictionary<string, string> data = new Dictionary<string, string>();

                    try
                    {
                        //PostEOData
                        CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Post Data" }, "Post IPPCS Data");
                        data["EOFlag"] = EOFlag.ToString();
                        log.Info("EO Flag" + EOFlag.ToString());
                        var isResult = CSTDWebAPIUtil.Request<CED030100GetEOData>(data, "PostEOData", null, "POST");
                        log.Info(isResult);
                        if (isResult.Count > 0 && isResult[0].isResult.ToString() != "N" && isResult[0].isResult.ToString() != "Y")
                        {
                            var paramResult = isResult[0].isResult.ToString().Split('|');
                            if (paramResult[0] == "NE" ){
                                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD110E", new String[] { paramResult[1].ToString().Split(';')[0], paramResult[1].ToString().Split(';')[1] }, "Post IPPCS");
                                messages.Add("failgetdata");
                                result["failgetdata"] = "failgetdata";
                                return result;
                            }

                            if (paramResult[0] == "BE")
                            {
                                CSTDDBLogUtil.CreateLogDetail(con, processId, paramResult[1].ToString().Split(';')[0], new String[] { paramResult[1].ToString().Split(';')[1], paramResult[1].ToString().Split(';')[2] }, "Post IPPCS");
                                messages.Add("failgetdata");
                                result["failgetdata"] = "failgetdata";
                                return result;
                            }
                        }

                        if (isResult.Count > 0 && isResult[0].isResult.ToString() == "N")
                        {
                            data = null;
                            //GetSupplierMaster
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Supplier Master" }, "Get Supplier Master");
                            tableName = "TB_T_SUPPLIER";
                            var SupplierMaster = CSTDWebAPIUtil.Request<CED030100SupplierMaster>(data, "GetSupplierMaster", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100SupplierMaster>(SupplierMaster, con.ConnectionString, tableName);

                            //GetDockMaster
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Dock Master" }, "Get Dock Master");
                            tableName = "TB_T_DOCK";
                            var DockMaster = CSTDWebAPIUtil.Request<CED030100GetDockMaster>(data, "GetDockMaster", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDockMaster>(DockMaster, con.ConnectionString, tableName);

                            //GetPartInfoMaster
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Part Info Master" }, "Get Part Info Master");
                            tableName = "TB_T_PART_INFO";
                            var PartInfoMaster = CSTDWebAPIUtil.Request<CED030100GetPartInfoMaster>(data, "GetPartInfoMaster", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetPartInfoMaster>(PartInfoMaster, con.ConnectionString, tableName);

                            //GetPart
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Part" }, "Get Part");
                            tableName = "TB_T_PART";
                            var Part = CSTDWebAPIUtil.Request<CED030100GetPart>(data, "GetPart", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetPart>(Part, con.ConnectionString, tableName);

                            //GetPartUnique
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Part Unique" }, "Get Part Unique");
                            tableName = "TB_T_PART_UNIQUE";
                            var PartUnique = CSTDWebAPIUtil.Request<CED030100GetPartUnique>(data, "GetPartUnique", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetPartUnique>(PartUnique, con.ConnectionString, tableName);

                            //GetPartType
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Part Type" }, "Get Part Type");
                            tableName = "TB_T_PART_TYPE";
                            var PartType = CSTDWebAPIUtil.Request<CED030100GetPartType>(data, "GetPartType", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetPartType>(PartType, con.ConnectionString, tableName);

                            //GetLogisticPartner
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Logistic Partner" }, "Get Logistic Partner");
                            tableName = "TB_T_LOGISTIC_PARTNER";
                            var LogisticPartner = CSTDWebAPIUtil.Request<CED030100GetLogisticPartner>(data, "GetLogisticPartner", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetLogisticPartner>(LogisticPartner, con.ConnectionString, tableName);

                            //GetDeliveryStationMapping
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Delivery Station Mapping" }, "Get Delivery Station Mapping");
                            tableName = "TB_T_DELIVERY_STATION_MAPPING";
                            var DeliveryStationMapping = CSTDWebAPIUtil.Request<CED030100GetDeliveryStationMapping>(data, "GetDeliveryStationMapping", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDeliveryStationMapping>(DeliveryStationMapping, con.ConnectionString, tableName);

                            //GetPhysicalDockMapping
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Physical Dock Mapping" }, "Get Physical Dock Mapping");
                            tableName = "TB_T_PHYSICAL_DOCK_MAPPING";
                            var PhysicalDockMapping = CSTDWebAPIUtil.Request<CED030100GetPhysicalDockMapping>(data, "GetPhysicalDockMapping", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetPhysicalDockMapping>(PhysicalDockMapping, con.ConnectionString, tableName);

                            //GetDailyOrderManifest
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Daily Order Manifest" }, "Get Daily Order Manifest");
                            tableName = "TB_T_DAILY_ORDER_MANIFEST";
                            var DailyOrderManifest = CSTDWebAPIUtil.Request<CED030100GetDailyOrderManifest>(data, "GetDailyOrderManifest", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDailyOrderManifest>(DailyOrderManifest, con.ConnectionString, tableName);

                            //GetDailyOrderPart
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Daily Order Part" }, "Get Daily Order Part");
                            tableName = "TB_T_DAILY_ORDER_PART";
                            var DailyOrderPart = CSTDWebAPIUtil.Request<CED030100GetDailyOrderPart>(data, "GetDailyOrderPart", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDailyOrderPart>(DailyOrderPart, con.ConnectionString, tableName);

                            //GetDeliveryCtlManifest
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Delivery CTL Manifest" }, "Get Delivery CTL Manifest");
                            tableName = "TB_T_DELIVERY_CTL_MANIFEST";
                            var DeliveryCtlManifest = CSTDWebAPIUtil.Request<CED030100GetDeliveryCtlManifest>(data, "GetDeliveryCtlManifest", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDeliveryCtlManifest>(DeliveryCtlManifest, con.ConnectionString, tableName);

                            //GetDclTlmsOrderAssignment
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get DCL TLMS Order Assignment" }, "Get DCL TLMS Order Assignment");
                            tableName = "TB_T_DCL_TLMS_ORDER_ASSIGNMENT";
                            var DclTlmsOrderAssignment = CSTDWebAPIUtil.Request<CED030100GetDclTlmsOrderAssignment>(data, "GetDclTlmsOrderAssignment", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDclTlmsOrderAssignment>(DclTlmsOrderAssignment, con.ConnectionString, tableName);

                            //GetDclTlmsDriverData
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get DCL TLMS Driver Data" }, "Get DCL TLMS Driver Data");
                            tableName = "TB_T_DCL_TLMS_DRIVER_DATA";
                            var DclTlmsDriverData = CSTDWebAPIUtil.Request<CED030100GetDclTlmsDriverData>(data, "GetDclTlmsDriverData", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDclTlmsDriverData>(DclTlmsDriverData, con.ConnectionString, tableName);

                            //GetDeliveryCtlH
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Delivery CTL Header" }, "Get Delivery CTL Header");
                            tableName = "TB_T_DELIVERY_CTL_H";
                            var DeliveryCtlH = CSTDWebAPIUtil.Request<CED030100GetDeliveryCtlH>(data, "GetDeliveryCtlH", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDeliveryCtlH>(DeliveryCtlH, con.ConnectionString, tableName);

                            //GetDeliveryCtlD
                            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Delivery CTL Detail" }, "Get Delivery CTL Detail");
                            tableName = "TB_T_DELIVERY_CTL_D";
                            var DeliveryCtlD = CSTDWebAPIUtil.Request<CED030100GetDeliveryCtlD>(data, "GetDeliveryCtlD", null, "GET");
                            CSTDDBUtil.ExecuteUpdate(con, null, " DELETE FROM " + tableName, param);
                            CSTDDBUtil.ToDatabase<CED030100GetDeliveryCtlD>(DeliveryCtlD, con.ConnectionString, tableName);

                            rowsResult = CED030100BDAO.GetResultProcessDataWS(searchDTO);
                        }
                        else
                        {
                            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD050E", new String[] { "Error during specified process" }, "Error during specified process");
                            messages.Add("failgetdata");
                            result["failgetdata"] = "failgetdata";
                            return result;
                        }
                    }
                    catch (Exception e)
                    {
                        CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD050E", new String[] { e.Message.ToString() }, "Error during specified process");
                        messages.Add("syserror");
                        result["syserror"] = "syserror";
                        log.Info(e.InnerException.ToString());
                        return result;
                    }
                    #endregion
                }
                else
                {
                    //DB Link
                    CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Using method DB Link" }, "Get Method");

                    rowsResult = CED030100BDAO.GetResultProcessData(searchDTO);
                }

                IList<CED030100BDTO> rowsResultCheckManifest = CED030100BDAO.GetResultCheckManifestDouble(searchDTO);
                string contentEmail = "<table border='1' style='border-collapse: collapse'><thead><tr><td>&nbsp;&nbsp;Manifest No&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Order Release Date&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Order No&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Supplier Code&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Supplier Plant Code&nbsp;&nbsp;</td><td>&nbsp;&nbsp;Dock Code&nbsp;&nbsp;</td></tr></thead><tbody>";
                if (rowsResultCheckManifest.Count > 0)
                {
                    foreach (var item in rowsResultCheckManifest)
                    {
                        contentEmail += "<tr><td>" + item.isResult.Split('-')[0].Trim() + "</td><td>" + item.isResult.Split('-')[1].Trim() + "</td><td>" + item.isResult.Split('-')[2].Trim() + "</td><td>" + item.isResult.Split('-')[3].Trim() + "</td><td>" + item.isResult.Split('-')[4].Trim() + "</td><td>" + item.isResult.Split('-')[5].Trim() + "</td></tr>";
                    }
                    contentEmail += "</tbody></table>";
                    CSTDEmailDTO emailDTO = new CSTDEmailDTO();
                    emailDTO.From = CSTDSystemMasterHelper.GetValue("CED030100B", "EMAIL_NOTIFICATION", "EMAIL_FROM", CSTDSystemMasterHelper.Mandatory);
                    emailDTO.To = CSTDSystemMasterHelper.GetValue("CED030100B", "EMAIL_NOTIFICATION", "EMAIL_TO", CSTDSystemMasterHelper.Mandatory);
                    emailDTO.CC = CSTDSystemMasterHelper.GetValue("CED030100B", "EMAIL_NOTIFICATION", "EMAIL_CC", CSTDSystemMasterHelper.Mandatory);
                    emailDTO.Subject = CSTDSystemMasterHelper.GetValue("CED030100B", "EMAIL_NOTIFICATION", "SUBJECT", CSTDSystemMasterHelper.Mandatory);
                    var headerContent = CSTDSystemMasterHelper.GetValue("CED030100B", "EMAIL_NOTIFICATION", "BODY_HEADER", CSTDSystemMasterHelper.Mandatory);
                    var footerContent = CSTDSystemMasterHelper.GetValue("CED030100B", "EMAIL_NOTIFICATION", "BODY_FOOTER", CSTDSystemMasterHelper.Mandatory);
                    emailDTO.Content = headerContent + "<br><br>" + contentEmail + "<br><br>" + footerContent;
                    CSTDEmailUtil.SendEmail(emailDTO);
                }
            }
            

            return result;
        }
    }
}
