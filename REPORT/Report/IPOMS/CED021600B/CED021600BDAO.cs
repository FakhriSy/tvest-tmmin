﻿using System.Collections.Generic;
using Common.STD.Util;

namespace Report.IPOMS.CED021600B
{
    public class CED021600BDAO
    {
        public static IList<CED021600BDTO> GetResultData(CED021600BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021600BDTO>("CED021600B.DoResultData", searchDTO);
        }

        public static IList<CED021600BSUMPOSDTO> GetSumPOSData(CED021600BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED021600BSUMPOSDTO>("CED021600B.GetSumPOSData", searchDTO);
        }
    }
}
