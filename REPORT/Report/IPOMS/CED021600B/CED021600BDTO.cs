﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED021600B
{
    public class CED021600BDTO
    {
        public string SubPackingLine { get; set; }
        public string PackingLineCode { get; set; }
        public string REV { get; set; }
        public string MP { get; set; }
        public string EFF { get; set; }
        public string ProdDate { get; set; }
        public string ProdDayName { get; set; }
        public string NO { get; set; }
        public string QTY { get; set; }
        public string QtyBox { get; set; }
        public string Dest { get; set; }
        public string LotModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string SHIFT { get; set; }
        public string RENBAN { get; set; }
        public string ModType { get; set; }
        public string M3 { get; set; }
        public string RATIO { get; set; }
        public string PlanTime { get; set; }
        public string CT { get; set; }
    }
}
