﻿using System;

namespace Report.IPOMS.CED021600B
{
    public class CED021600BSUMPOSDTO
    {
        public string PackingLineCode { get; set; }
        public string Dest { get; set; }
        public string LotModuleNo { get; set; }
        public string ShooterNo { get; set; }
        public string CaseNo { get; set; }
        public string Shift { get; set; }
        public string Renban { get; set; }
        public string RenbanCode { get; set; }
        public string ModType { get; set; }
        public DateTime? PlanStartTime { get; set; }
        public DateTime? PlanFinishTime { get; set; }
        public int CT { get; set; }
        public string Cumm { get; set; }
        public DateTime? ProdDate { get; set; }
    }
}
