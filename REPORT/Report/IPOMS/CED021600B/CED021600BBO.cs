﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using System.Data.SqlClient;
using NPOI.HSSF.UserModel;
using Common.STD.DTO;
using NPOI.SS.UserModel;

namespace Report.IPOMS.CED021600B
{
    public class CED021600BBO : CSTDBaseReport
    {
        CED021600BDTO ErrorDTO = new CED021600BDTO();
        String BatchName = "POS Inhouse Report";
        String ProcessBatchName = "POS Inhouse Report";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "POS Inhouse Report";
            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            //Console.WriteLine(reportParams.Length);
            #region Check report template
            String templateName = functionId + ".xls";

            logLocation = "Check template";
            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD045E", new String[] { _excelTemplateFolder }));
                result["templateNotFound"] = "notfound";
                return result;
            }
            #endregion

            string[] commandParams = reportParams[4].Split(';');

            DateTime prodTime = DateTime.Parse(commandParams[0]);

            CED021600BSearchDTO searchDTO = new CED021600BSearchDTO()
            {
                ProdDate = prodTime.ToString("yyyy-MM-dd")
            };

            string shiftOption = commandParams[2];
            string packingLineOption = commandParams.Length > 3 ? commandParams[3] : "";
            string[] packingLineFilter = packingLineOption == "" ? (new string[0]) : packingLineOption.Split(',');

            try
            {
                List<CED021600BDTO> dayRows = new List<CED021600BDTO>();
                if (string.IsNullOrEmpty(shiftOption) || shiftOption == "1")
                {
                    searchDTO.SHIFT = "1";
                    dayRows.AddRange(CED021600BDAO.GetResultData(searchDTO));
                    
                    if (packingLineFilter.Any())
                    {
                        dayRows = dayRows.Where(x => packingLineFilter.Contains(x.PackingLineCode)).ToList();
                    }
                }

                List<CED021600BDTO> nightRows = new List<CED021600BDTO>();
                if (string.IsNullOrEmpty(shiftOption) || shiftOption == "2")
                {
                    searchDTO.SHIFT = "2";
                    nightRows.AddRange(CED021600BDAO.GetResultData(searchDTO));

                    if (packingLineFilter.Any())
                    {
                        nightRows = nightRows.Where(x => packingLineFilter.Contains(x.PackingLineCode)).ToList();
                    }
                }

                #region Fill up report
                HSSFWorkbook workbook = InitializeWorkbook(templateName);
                //--PUTRA 25 MEI 2024 START
                var ActWsIndex = workbook.ActiveSheetIndex;
                
                int sumsheet = 0;
                int.TryParse(ActWsIndex.ToString(), out sumsheet);

                HSSFSheet sheet;

                for (int numsheet = 0; numsheet <= sumsheet; numsheet++)
                {
                    //--PUTRA 25 MEI 2024 END
                    
                    double rowHeight = 18.5;
                    int sheetCount = 0;

                    int baseRowIndex = 0;
                    int startRow = 0;
                    int rowIndex = 0;
                    int numberOfRow = 0;
                    int modOfRow = 0;
                    int rowToRemove = 0;
                    int startIndexRowToRemove = 0;

                    IList<CED021600BDTO> data;

                    foreach (string packingLine in packingLineFilter)
                    {
                        // Generate for Day shift
                        data = dayRows.Where(x => x.PackingLineCode == packingLine).ToList();

                        sheet = (HSSFSheet)workbook.CloneSheet(0);
                        sheetCount++;

                        workbook.SetSheetName(sheetCount, packingLine);

                        startRow = 14;
                        rowIndex = startRow;
                        numberOfRow = data.Count;
                        modOfRow = numberOfRow > 0 ? data.Count % numberOfRow : 0;
                        rowToRemove = numberOfRow - modOfRow;
                        startIndexRowToRemove = startRow + modOfRow;

                        if (data.Any())
                        {
                            //CSTDNPOIUtil.SetCellValue(sheet, 10, 2, data[0].REV);
                            CSTDNPOIUtil.SetCellValue(sheet, 10, 5, data[0].ProdDate);
                            CSTDNPOIUtil.SetCellValue(sheet, 10, 7, data[0].ProdDayName);
                            CSTDNPOIUtil.SetCellValue(sheet, 9, 11, data[0].MP);
                            CSTDNPOIUtil.SetCellValue(sheet, 10, 11, data[0].EFF + " %");
                            CSTDNPOIUtil.SetCellValue(sheet, 9, 18, packingLine);
                        }

                        int seqNo = 0;
                        foreach (CED021600BDTO row in data)
                        {
                            if (rowIndex == 65535 || sheetCount == 0)
                            {
                                sheet = (HSSFSheet)workbook.CloneSheet(0);
                                sheetCount++;

                                workbook.SetSheetName(sheetCount, packingLine);

                                startRow = 14;
                                rowIndex = startRow;
                                numberOfRow = data.Count;
                                modOfRow = numberOfRow > 0 ? data.Count % numberOfRow : 0;
                                rowToRemove = numberOfRow - modOfRow;
                                startIndexRowToRemove = startRow + modOfRow;
                                rowHeight = 18.5;
                            }

                            if (rowIndex > (startRow + 35))
                            {
                                //copy row style
                                CopyRowWithHeight(sheet, startRow + 1, rowIndex, rowHeight);
                            }

                            if (modOfRow > 0)
                            {
                                HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                                sheet.RemoveRow(r);
                                modOfRow--;
                            }

                            //fill up column
                            seqNo++;
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, seqNo);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.QTY);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.QtyBox);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.Dest);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.LotModuleNo);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.CaseNo);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.SHIFT);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.RENBAN);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.ModType);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.M3);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.RATIO);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.PlanTime);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.CT);
                            ++rowIndex;
                        }

                        // Add finish time
                        CED021600BDTO lastRow;
                        if (data.Any())
                        {
                            lastRow = data.Last();
                            var finishTime = DateTime.Parse(lastRow.PlanTime).AddMinutes(Convert.ToInt32(lastRow.CT));
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, finishTime.ToString("HH:mm"));
                            rowIndex++;
                        }

                        // Generate for Night shift
                        data = nightRows.Where(x => x.PackingLineCode == packingLine).ToList();

                        baseRowIndex = shiftOption == "2" || rowIndex <= 50 ? 70 : rowIndex + 20;
                        startRow = baseRowIndex + 11;
                        rowIndex = startRow;
                        numberOfRow = data.Count;
                        modOfRow = numberOfRow > 0 ? data.Count % numberOfRow : 0;
                        rowToRemove = numberOfRow - modOfRow;
                        startIndexRowToRemove = startRow + modOfRow;

                        if (data.Any())
                        {
                            //CSTDNPOIUtil.SetCellValue(sheet, (baseRowIndex + 7), 2, data[0].REV);
                            CSTDNPOIUtil.SetCellValue(sheet, (baseRowIndex + 7), 5, data[0].ProdDate);
                            CSTDNPOIUtil.SetCellValue(sheet, (baseRowIndex + 7), 7, data[0].ProdDayName);
                            CSTDNPOIUtil.SetCellValue(sheet, (baseRowIndex + 6), 11, data[0].MP);
                            CSTDNPOIUtil.SetCellValue(sheet, (baseRowIndex + 7), 11, data[0].EFF + " %");
                            CSTDNPOIUtil.SetCellValue(sheet, (baseRowIndex + 6), 18, packingLine);
                        }

                        seqNo = 0;
                        foreach (CED021600BDTO row in data)
                        {
                            if (rowIndex == 65535 || sheetCount == 0)
                            {
                                sheet = (HSSFSheet)workbook.CloneSheet(0);
                                sheetCount++;

                                workbook.SetSheetName(sheetCount, packingLine);

                                startRow = baseRowIndex + 11;
                                rowIndex = startRow;
                                numberOfRow = data.Count;
                                modOfRow = numberOfRow > 0 ? data.Count % numberOfRow : 0;
                                rowToRemove = numberOfRow - modOfRow;
                                startIndexRowToRemove = startRow + modOfRow;
                                rowHeight = 18.5;
                            }

                            if (rowIndex > (startRow + 30))
                            {
                                //copy row style
                                CopyRowWithHeight(sheet, startRow + 1, rowIndex, rowHeight);
                            }

                            if (modOfRow > 0)
                            {
                                HSSFRow r = (HSSFRow)sheet.GetRow(rowIndex + 1);
                                sheet.RemoveRow(r);
                                modOfRow--;
                            }

                            //fill up column
                            seqNo++;
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, seqNo);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, row.QTY);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, row.QtyBox);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, row.Dest);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, row.LotModuleNo);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, row.CaseNo);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, row.SHIFT);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, row.RENBAN);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, row.ModType);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, row.M3);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 12, row.RATIO);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, row.PlanTime);
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 14, row.CT);
                            ++rowIndex;
                        }

                        // Add finish time
                        if (data.Any())
                        {
                            lastRow = data.Last();
                            var finishTime = DateTime.Parse(lastRow.PlanTime).AddMinutes(Convert.ToInt32(lastRow.CT));
                            CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 13, finishTime.ToString("HH:mm"));
                            rowIndex++;
                        }
                    }

                    // Generate SUM_POS sheet
                    if (packingLineFilter.Any())
                    {
                        searchDTO.SHIFT = shiftOption;
                        IList<CED021600BSUMPOSDTO> sumPosData = CED021600BDAO.GetSumPOSData(searchDTO);
                        sumPosData = sumPosData.Where(x => packingLineFilter.Contains(x.PackingLineCode)).ToList();

                        sheet = (HSSFSheet)workbook.CreateSheet("SUM_POS");
                        sheetCount++;

                        var headerFontStyle = workbook.CreateFont();
                        headerFontStyle.FontName = "Times New Roman";
                        headerFontStyle.FontHeightInPoints = 12;
                        headerFontStyle.Boldweight = (short)FontBoldWeight.BOLD;

                        var headerCellStyle = workbook.CreateCellStyle();
                        headerCellStyle.BorderTop = BorderStyle.THIN;
                        headerCellStyle.BorderRight = BorderStyle.THIN;
                        headerCellStyle.BorderBottom = BorderStyle.THIN;
                        headerCellStyle.BorderLeft = BorderStyle.THIN;
                        headerCellStyle.Alignment = HorizontalAlignment.CENTER;
                        headerCellStyle.SetFont(headerFontStyle);

                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 0, "Line", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 1, "Dest", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 2, "Module No", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 3, "Case No", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 4, "Shift", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 5, "Renban No", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 6, "Mod Type", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 7, "Time", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 8, "Pack Time", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 9, "Cumm", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 10, "POS Date", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 11, "Usage", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 12, "Select", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 13, "Date Finish", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 14, "Time Finish", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 15, "Renban Code", headerCellStyle);
                        CSTDNPOIUtil.SetCellValueWithStyle(sheet, 0, 16, "Shutter No", headerCellStyle);

                        var dataFontStyle = workbook.CreateFont();
                        dataFontStyle.FontName = "Times New Roman";
                        dataFontStyle.FontHeightInPoints = 12;
                        dataFontStyle.Boldweight = (short)FontBoldWeight.NORMAL;

                        var dataCellStyle = workbook.CreateCellStyle();
                        dataCellStyle.BorderTop = BorderStyle.THIN;
                        dataCellStyle.BorderRight = BorderStyle.THIN;
                        dataCellStyle.BorderBottom = BorderStyle.THIN;
                        dataCellStyle.BorderLeft = BorderStyle.THIN;
                        dataCellStyle.Alignment = HorizontalAlignment.CENTER;
                        dataCellStyle.SetFont(dataFontStyle);

                        rowIndex = 1;

                        foreach (CED021600BSUMPOSDTO row in sumPosData)
                        {
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 0, row.PackingLineCode, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 1, row.Dest, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 2, row.LotModuleNo, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 3, row.CaseNo ?? "", dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 4, row.Shift, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 5, row.Renban, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 6, row.ModType, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 7, row.PlanStartTime?.ToString("HH:mm"), dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 8, row.CT, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 9, "", dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 10, searchDTO.ProdDate, dataCellStyle);

                            var usage = row.PlanStartTime.HasValue ? (row.PlanStartTime.Value - DateTime.Parse(row.PlanStartTime.Value.ToString("yyyy-MM-dd 00:00"))).TotalMinutes + row.CT : 0;
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 11, usage, dataCellStyle);
                            
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 12, "1", dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 13, row.PlanFinishTime?.ToString("dd-MMM-yyyy"), dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 14, row.PlanFinishTime?.ToString("HH:mm"), dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 15, row.RenbanCode, dataCellStyle);
                            CSTDNPOIUtil.SetCellValueWithStyle(sheet, rowIndex, 16, row.ShooterNo, dataCellStyle);
                            ++rowIndex;
                        }

                        for (int i = 0; i < 17; i++)
                        {
                            sheet.AutoSizeColumn(i);
                        }
                    }

                    if (sheetCount > 0)
                    {
                        workbook.SetSheetHidden(0, true);
                        workbook.SetActiveSheet(1);

                        ISheet currentSheet;

                        for (int i = 0; i < sheetCount; i++)
                        {
                            if (workbook.GetPrintArea(i) != null)
                            {
                                workbook.RemovePrintArea(i);
                            }

                            currentSheet = workbook.GetSheetAt(i);
                            currentSheet.PrintSetup.PaperSize = (short)PaperSize.A4 + 1;
                            currentSheet.PrintSetup.Landscape = true;
                            currentSheet.PrintSetup.Scale = 44;

                            currentSheet.SetMargin(MarginType.LeftMargin, 0.1);
                            currentSheet.SetMargin(MarginType.RightMargin, 0.1);
                            currentSheet.SetMargin(MarginType.TopMargin, 0.9);
                            currentSheet.SetMargin(MarginType.BottomMargin, 0.9);
                            currentSheet.HorizontallyCenter = true;
                        }
                    }

                    result["workbook"] = workbook;
                }
                #endregion
                //--PUTRA 25 MEI 2024 START
            }
            //--PUTRA 25 MEI 2024 END
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            return result;
        }
    }
}
