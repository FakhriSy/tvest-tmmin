﻿using Common.STD.Base;
using Report.IPOMS.CED010609B;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010609B
{
    public class CED010609BBO : CSTDBaseReport
    {
        CED010609BDTO ErrorDTO = new CED010609BDTO();
        String BatchName = "Release POS Daily batch";
        String ProcessBatchName = "Release POS Daily batch";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Calculate(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;
            String reportName = "Release POS Daily batch";

            //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD021I", new String[] { "Get Parameter" }, "Get Default Data Reference");

            var par = reportParams[4].Split(';');
            CED010609BSearchDTO sDTO = new CED010609BSearchDTO();
            sDTO.ProcessId = int.Parse(processId);
            sDTO.PKG_MTH = par[1];
            sDTO.PLANT_CD = par[2];
            sDTO.POS_DT = par[3];
            sDTO.UserId = userId;

            IList<CED010609BDTO> rowsResult = null;
            if (par[0] == "Inhouse")
            {
                rowsResult = CED010609BDAO.GetResultProcessDataInhouse(sDTO);
            }
            else if (par[0] == "Outhouse")
            {
                rowsResult = CED010609BDAO.GetResultProcessDataOuthouse(sDTO);
            }
            
            if (rowsResult[0].isResult == "ERROR")
            {
                messages.Add("EndWithError");
                result["EndWithError"] = "EndWithError";
                return result;
            }

            return result;
        }
    }
}
