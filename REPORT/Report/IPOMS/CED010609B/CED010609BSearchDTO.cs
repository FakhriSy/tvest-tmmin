﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010609B
{
    public class CED010609BSearchDTO
    {
        public int ProcessId { get; set; }
        public string PKG_MTH { get; set; }
        public string PLANT_CD { get; set; }
        public string POS_DT { get; set; }
        public string UserId { get; set; }
    }
}
