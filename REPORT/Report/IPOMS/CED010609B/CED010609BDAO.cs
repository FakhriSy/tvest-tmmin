﻿using Common.STD.Util;
using Report.IPOMS.CED010609B;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010609B
{
    public class CED010609BDAO
    {
        public static IList<CED010609BDTO> GetResultProcessDataInhouse(CED010609BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010609BDTO>("CED010609B.DoProcessDataInhouse", sDTO);
        }

        public static IList<CED010609BDTO> GetResultProcessDataOuthouse(CED010609BSearchDTO sDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010609BDTO>("CED010609B.DoProcessDataOuthouse", sDTO);
        }
    }
}
