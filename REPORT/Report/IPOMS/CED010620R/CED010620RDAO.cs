﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Util;

namespace Report.IPOMS.CED010620R
{
    public class CED010620RDAO
    {
        public static IList<CED010620RDTO> GetResultData(CED010620RSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED010620RDTO>("CED010620R.DoResultData", searchDTO);
        }
    }
}
