﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010620R
{
    public class CED010620RDTO
    {
        public string SupplierName {get; set;}
        public string Rev {get; set;}
        public string ProdDate {get; set;}
        public string ProdDay {get; set;}
        public string EGCode {get; set;}
        public string Quantity { get; set;}
        public string ContainerDestCode {get; set;}
        public string LotModuleNo {get; set;}
        public string CaseNo {get; set;}
        public string Shift {get; set;}
        public string Renban {get; set;}
        public string ModType {get; set;}
        public string DeliveryTimePlan {get; set;}
        public string DeliveryTimeActual {get; set;}
        public string Remark {get; set;}
        public string Email { get; set; }
    }
}
