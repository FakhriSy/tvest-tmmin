﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.Formula.Functions;
using log4net;

namespace Report.IPOMS.CED010620R
{
    public class CED010620RBO : CSTDBaseReport
    {
        private static readonly ILog log = LogManager.GetLogger(typeof(Program));

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, string functionId, string userId, string[] reportParams, string userLocation)
        {
            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            #region Check report template

            String _excelTemplateFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_TEMPLATE_FOLDER", CSTDSystemMasterHelper.Mandatory);
            String templateName = functionId + ".xls";

            if (!CheckTemplateFolder(_excelTemplateFolder, templateName))
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { _excelTemplateFolder + templateName }), "Check template");
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { _excelTemplateFolder + templateName }));
                result["templateNotFound"] = "templateNotFound";
                return result;
            }

            #endregion

            #region Get Data

            CED010620RSearchDTO searchDTO = new CED010620RSearchDTO();
            searchDTO.ProdDate = reportParams[4].Split(';')[0];
            searchDTO.Shift = reportParams[4].Split(';')[1];
            IList<CED010620RDTO> data = CED010620RDAO.GetResultData(searchDTO);
            string listEmail = "";
            foreach(var r in data.Select(x => x.Email).Distinct().ToList())
            {
                listEmail += r + ";";
            }
            
            if (data.Count == 0)
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }), "Get Data");
                messages.Add(CSTDMessageUtil.GetMessage("MCEDSTD053E", new String[] { "Data" }));
                result["dataNotFound"] = "dataNotFound";
                return result;
            }

            #endregion

            #region Fill up report

            HSSFWorkbook workbook = InitializeWorkbook(templateName);
            HSSFSheet sheet_template = (HSSFSheet)workbook.GetSheet("Report");
            HSSFSheet sheet = (HSSFSheet)workbook.GetSheet("Report");

            int rowIndex = 9;
            string prev_supp_name = "";

            foreach (CED010620RDTO d in data)
            {
                if (d.SupplierName != prev_supp_name)
                {
                    rowIndex = 9;
                    sheet = sheet_template.CopySheet(d.SupplierName, true);
                    CSTDNPOIUtil.SetCellValue(sheet, 2, 0, "Supplier : " + d.SupplierName);
                    CSTDNPOIUtil.SetCellValue(sheet, 6, 0, d.Rev);
                    CSTDNPOIUtil.SetCellValue(sheet, 6, 3, d.ProdDate);
                    CSTDNPOIUtil.SetCellValue(sheet, 6, 5, d.ProdDay);
                    prev_supp_name = d.SupplierName;
                }

                if (rowIndex > 9)
                    CopyRowWithHeight(sheet, 9, rowIndex, 11.25);

                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 0, rowIndex-8);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 1, d.EGCode);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 2, d.Quantity);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 3, d.ContainerDestCode);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 4, d.LotModuleNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 5, d.CaseNo);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 6, d.Shift);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 7, d.Renban);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 8, d.ModType);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 9, d.DeliveryTimePlan);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 10, d.DeliveryTimeActual);
                CSTDNPOIUtil.SetCellValue(sheet, rowIndex, 11, d.Remark);

                rowIndex++;
            }

            workbook.RemoveSheetAt(workbook.GetSheetIndex("Report"));

            #endregion

            result["workbook"] = workbook;

            String reportBatchName = "POS Outhouse Report";
            String targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
            String reportName = reportBatchName;
            String filename = reportName.Replace(" ", "_") + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
            String fullPathFilename = targetDir + filename;
            FileStream file = new FileStream(fullPathFilename, FileMode.Create);
            workbook.Write(file);
            file.Close();
            CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, filename);
            Console.WriteLine("Report successfully saved into file: " + fullPathFilename);

            Object[] obParams1 = new Object[10];
            obParams1[0] = listEmail;// CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "RECEIVE", CSTDSystemMasterHelper.Mandatory);
            obParams1[1] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "CC", CSTDSystemMasterHelper.Mandatory);
            obParams1[2] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "SUBJECT", CSTDSystemMasterHelper.Mandatory);
            obParams1[3] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "SMTP", CSTDSystemMasterHelper.Mandatory);
            obParams1[4] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "SENDER", CSTDSystemMasterHelper.Mandatory);
            obParams1[5] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "PORT", CSTDSystemMasterHelper.Mandatory);
            obParams1[6] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "USER", CSTDSystemMasterHelper.Mandatory);
            obParams1[7] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "PASSWORD", CSTDSystemMasterHelper.Mandatory);
            obParams1[8] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "CREDENTIALS_FLAG", CSTDSystemMasterHelper.Mandatory);
            obParams1[9] = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "PORT_FLAG", CSTDSystemMasterHelper.Mandatory);
            this.SendMail(obParams1, processId, fullPathFilename);

            return result;
        }

        public string SendMail(Object[] d1, string processId, string Attachment)
        {
            string result = "SUCCESS";

            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Send Email proccess is started" }, "Send Email");
                string Body = CSTDSystemMasterHelper.GetValue("CED010620R", "EMAIL", "BODY", CSTDSystemMasterHelper.Mandatory);
                string messageBody = "<br><font>" + Body + " </font><br><br>";
                //messageBody = messageBody.Replace("{0}", ListQty.Count.ToString());
                log.Info(messageBody);
                string V_MAIL_TO = d1[0].ToString();
                string V_MAIL_CC = d1[1].ToString();
                string V_MAIL_SUBJECT = d1[2].ToString();
                string SMTP_SERVER = d1[3].ToString();
                string SENDER = d1[4].ToString();
                int PORT = Convert.ToInt32(d1[5]);
                string USER = d1[6].ToString();
                string PWD = d1[7].ToString();
                string CREDENTIALS_FLAG = d1[8].ToString();
                string PORT_FLAG = d1[9].ToString();

                String[] emailTo = V_MAIL_TO.Split(';');

                emailTo = emailTo.Where(val => !String.IsNullOrEmpty(val)).ToArray();

                String[] emailCc = null;
                if (!String.IsNullOrEmpty(V_MAIL_CC))
                {
                    emailCc = V_MAIL_CC.Split(';');
                }

                MailMessage mail = new MailMessage();
                SmtpClient client = new SmtpClient(SMTP_SERVER);

                mail.From = new MailAddress(SENDER);
                //mail.To.Add(V_MAIL_TO);

                foreach (string to in emailTo)
                {
                    mail.To.Add(new MailAddress(to));
                }

                //if (V_MAIL_CC != "" && V_MAIL_CC != null)
                //{
                //    mail.CC.Add(V_MAIL_CC);
                //}

                if (emailCc != null)
                {
                    foreach (string cc in emailCc)
                    {
                        mail.CC.Add(new MailAddress(cc));
                    }
                }
                mail.Subject = V_MAIL_SUBJECT;
                mail.Body = messageBody;
                mail.IsBodyHtml = true;

                if (Attachment != "N")
                {
                    System.Net.Mail.Attachment attachment;
                    attachment = new System.Net.Mail.Attachment(Attachment);
                    attachment.Name = Path.GetFileName(Attachment);
                    mail.Attachments.Add(attachment);
                }

                if (PORT_FLAG == "Y")
                {
                    client.Port = PORT;
                }

                if (CREDENTIALS_FLAG == "Y")
                {
                    client.Credentials = new System.Net.NetworkCredential(USER, PWD);
                }

                client.EnableSsl = false;
                log.Info("Start send email");
                client.Send(mail);
                log.Info("Finish send email");
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD000I", new String[] { "Send Email is finish successfully" }, "Send Email");
            }
            catch (SmtpException e)
            {
                log.Info("SmtpException: " + e.Message.ToString());
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "SMTP Exception");

                result = e.Message.ToString();
            }
            catch (SqlException e)
            {
                log.Info("SqlException: " + e.Message.ToString());
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "SQL Exception");

                result = e.Message.ToString();
            }
            catch (Exception e)
            {
                log.Info("Exception: " + e.Message.ToString());
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "Exception");

                result = e.Message.ToString();
            }

            return result;
        }
    }
}
