﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED010620R
{
    public class CED010620RSearchDTO
    {
        public string ProdDate { get; set; }
        public string Shift { get; set; }
    }
}
