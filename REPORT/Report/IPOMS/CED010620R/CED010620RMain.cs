﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Common.STD.Base;
using Common.STD.Util;

namespace Report.IPOMS.CED010620R
{
    public class CED010620RMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;
            
            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED010620RBO ReportBO = new CED010620RBO();
            String reportBatchName = "POS Outhouse Report";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                        Console.WriteLine(message);

                    var resultDataTemplate = result.SingleOrDefault(d => d.Key == "templateNotFound");
                    var resultData = result.SingleOrDefault(d => d.Key == "dataNotFound");

                    if (resultDataTemplate.Value != null && resultDataTemplate.Value.ToString() == "templateNotFound")
                    {
                        processSts = "1";
                    }
                    else if (resultData.Value != null && resultData.Value.ToString() == "dataNotFound")
                    {
                        processSts = "1";
                    }
                    else
                    {
                        processId = "2";
                    }
                }
                else
                {
                    //String targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER", CSTDSystemMasterHelper.Mandatory);
                    //NPOI.HSSF.UserModel.HSSFWorkbook workbook = (NPOI.HSSF.UserModel.HSSFWorkbook)result["workbook"];
                    //String reportName = reportBatchName;
                    //String filename = reportName.Replace(" ", "_") + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                    //String fullPathFilename = targetDir + filename;
                    //FileStream file = new FileStream(fullPathFilename, FileMode.Create);
                    //workbook.Write(file);
                    //file.Close();
                    //CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, filename);
                    processSts = "0";
                    //Console.WriteLine("Report successfully saved into file: " + fullPathFilename);
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD007I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }

            return result;
        }
    }
}
