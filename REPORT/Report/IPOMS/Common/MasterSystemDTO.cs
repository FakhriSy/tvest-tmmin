﻿using System;

namespace Report.IPOMS.Common
{
    public class MasterSystemDTO
    {
        public string SYS_CAT { get; set; }
        public string SYS_SUB_CAT { get; set; }
        public string SYS_CD { get; set; }
        public string SYS_VAL { get; set; }
        public string REMARK { get; set; }
        public DateTime CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public DateTime UPDATED_DT { get; set; }
        public string UPDATED_BY { get; set; }
    }
}
