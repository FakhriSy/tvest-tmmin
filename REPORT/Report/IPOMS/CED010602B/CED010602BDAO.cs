﻿using System.Collections.Generic;
using Common.STD.Util;

namespace Report.IPOMS.CED010602B
{
    public class CED010602BDAO
    {
        public static void DeleteStaging(CED010602BDTO dataForm, string table)
        {
            if (table == "CycleTime")
                CSTDMapper.Instance().Delete("CED010602B.DeleteStagingCycleTime", dataForm);
            else
                CSTDMapper.Instance().Delete("CED010602B.DeleteStagingPreparationTime", dataForm);
        }

        public static void InsertStaging(List<CED010602BDTO> dataForm, string table)
        {
            foreach (CED010602BDTO item in dataForm)
                if (table == "CycleTime")
                    CSTDMapper.Instance().Insert("CED010602B.InsertStagingCycleTime", item);
                else
                    CSTDMapper.Instance().Insert("CED010602B.InsertStagingPreparationTime", item);
        }

        public static IList<CED010602BDTO> GetResultData(CED010602BSearchDTO searchDTO, string table)
        {
            if (table == "CycleTime")
                return CSTDMapper.Instance().QueryForList<CED010602BDTO>("CED010602B.DoResultCycleTimeData", searchDTO);
            else
                return CSTDMapper.Instance().QueryForList<CED010602BDTO>("CED010602B.DoResultPreparationTimeData", searchDTO);

        }

    }
}
