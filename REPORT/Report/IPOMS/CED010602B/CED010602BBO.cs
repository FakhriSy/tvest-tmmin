﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using NPOI.SS.UserModel;
using NPOI.XSSF.UserModel;

namespace Report.IPOMS.CED010602B
{
    public class CED010602BBO : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> Download(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation, String table)
        {
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD015I", new String[] { null }, "Get parameter from Batch Queue");
            
            string _filePathFolder = CSTDSystemMasterHelper.GetValue("REPORT", "FOLDER", "FULLPATH_UPLOAD", CSTDSystemMasterHelper.Mandatory);
            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD016I", new String[] { null }, "Get configuration setting");

            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            string filename = reportParams[4].Split(';')[0];

            string fullpathfile = _filePathFolder + filename;
            if (!CheckTemplateFolder(_filePathFolder, filename))
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD046E", new String[] { fullpathfile }, "Uploaded file not found");
                messages.Add("fileNotFound");
                result["fileNotFound"] = "fileNotFound";
                return result;
            }

            CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { "Read file, delete and insert into staging table" }, "Read file");

            ISheet sheet;
            var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
            var fileExt = Path.GetExtension(filename);

            if (!CheckSheetName(_filePathFolder, filename, table, fileExt))
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD072E", new String[] { fullpathfile }, "Sheet name error");
                messages.Add("Errorsheetname");
                result["Errorsheetname"] = "Errorsheetname";
                return result;
            }

            if (fileExt == ".xls")
            {
                HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }
            else
            {
                XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                sheet = hssfwb.GetSheetAt(0);
            }

            int rowno = 0;
            List<CED010602BDTO> listData = new List<CED010602BDTO>();
            for (int row = 6; row <= sheet.LastRowNum; row++)
            {
                if (sheet.GetRow(row) != null)
                {
                    rowno++;
                    
                    CED010602BDTO dataForm = new CED010602BDTO();

                    dataForm.UploadId = sheet.GetRow(row).GetCell(0).ToString();
                    
                    if (table == "CycleTime")
                    {
                        dataForm.ModId = sheet.GetRow(row).GetCell(1).ToString();
                        dataForm.MP = sheet.GetRow(row).GetCell(2).ToString();
                        dataForm.CycleTime = sheet.GetRow(row).GetCell(3).ToString();
                        dataForm.KitFlag = sheet.GetRow(row).GetCell(4).ToString();
                        dataForm.DedicatedLine = sheet.GetRow(row).GetCell(5).ToString();
                        dataForm.SemifinishFlag = sheet.GetRow(row).GetCell(6).ToString();
                        dataForm.PckLineFrom = sheet.GetRow(row).GetCell(7).ToString();
                        dataForm.PckLineTo = sheet.GetRow(row).GetCell(8).ToString();
                        dataForm.ModType = sheet.GetRow(row).GetCell(9).ToString();
                    }
                    else if (table == "PreparationTime")
                    {
                        dataForm.ModType = sheet.GetRow(row).GetCell(1).ToString();
                        dataForm.PreparationTime = sheet.GetRow(row).GetCell(2).ToString();
                        dataForm.Measurement = sheet.GetRow(row).GetCell(3).ToString();
                        dataForm.Ratio = sheet.GetRow(row).GetCell(4).ToString();
                    }

                    dataForm.ProcessId = processId;
                    dataForm.RowNo = rowno.ToString();
                    dataForm.UserName = userId;
                    dataForm.isResult = "N";

                    listData.Add(dataForm);
                }
            }

            CED010602BDAO.InsertStaging(listData, table);

            CED010602BSearchDTO searchDTO = new CED010602BSearchDTO
            {
                ProcessId = int.Parse(processId),
                UserName = userId
            };

            IList<CED010602BDTO> rowsResult = CED010602BDAO.GetResultData(searchDTO, table);
            if (rowsResult[0].isResult == "Y")
            {
                messages.Add("failCheckingBasic");
                result["failCheckingBasic"] = "failCheckingBasic";
                return result;
            }

            return result;
        }
    }
}
