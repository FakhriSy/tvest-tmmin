﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;

namespace Report.IPOMS.CED010602B
{
    public class CED010602BMain : CSTDBaseReport
    {
        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED010602BBO ReportBO = new CED010602BBO();
            String reportBatchName = "Upload Cycle & Preparation Time Data Batch";
            String processSts = "";
            CED010602BDTO dataForm = new CED010602BDTO();
            dataForm.ProcessId = processId;
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCEDSTD006I", new String[] { reportBatchName }, "Start log");
                string table = reportParameters[4].Split(';')[1];
                table = (table == "1") ? "CycleTime" : "PreparationTime";
                CED010602BDAO.DeleteStaging(dataForm, table);
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.Download(con, processId, functionId, userId, reportParameters, userLocation, table);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultDatafile = result.SingleOrDefault(d => d.Key == "fileNotFound");
                    if (resultDatafile.Value != null && (resultDatafile.Value.ToString() == "fileNotFound"))
                    {
                        processSts = "1";
                    }
                    else
                    {
                        var resultDataChecking = result.SingleOrDefault(d => d.Key == "failCheckingBasic");
                        if (resultDataChecking.Value != null && resultDataChecking.Value.ToString() == "failCheckingBasic")
                        {
                            processSts = "1";
                        }
                        else
                        {
                            processSts = "2";
                        }
                    }
                }
                else
                {
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD008I", new String[] { reportBatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";

                Console.WriteLine(e.Message);
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MCEDSTD022E", new String[] { e.InnerException.ToString() }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, reportBatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }
    }
}
