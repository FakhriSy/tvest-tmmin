﻿using Common.STD.Base;

namespace Report.IPOMS.CED010602B
{
    public class CED010602BDTO : CSTDBaseDTO
    {
        public string UploadId { get; set; }

        public string ModId { get; set; }
        public string MP { get; set; }
        public string CycleTime { get; set; }
        public string KitFlag { get; set; }
        public string DedicatedLine { get; set; }
        public string SemifinishFlag { get; set; }
        public string PrevPckLine { get; set; }
        public string PckLineFrom { get; set; }
        public string PckLineTo { get; set; }

        public string ModType { get; set; }
        public string PreparationTime {  get; set; }
        public string Measurement { get; set; }
        public string Ratio {  get; set; }

        public string ProcessId { get; set; }
        public string RowNo { get; set; }
        public string UserName { get; set; }
        public string isResult { get; set; }
    }
}
