﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED022700B
{
    public class CED022700BDTO
    {
        public string RESULT_MESSAGE { get; set; }

    }

    public class CED022700BReport
    {
        public String ProcessId { get; set; }
        public String FileName { get; set; }

        public String InProcessId { get; set; }
        public String InFileName { get; set; }
    }
}
