﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED022700B
{
    public class CED022700BSearchDTO
    {
        public string PROCESS_ID { get; set; }
        public string URL_UPLOADED_FILE { get; set; }
        public string USER_LOGIN { get; set; }
    }
}
