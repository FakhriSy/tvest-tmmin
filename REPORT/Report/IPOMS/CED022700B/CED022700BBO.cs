﻿using Common.STD.Base;
using Common.STD.Util;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Web;
//using System.Web.UI.WebControls;

namespace Report.IPOMS.CED022700B
{
    public class CED022700BBO
    {
        CED022700BDTO ErrorDTO = new CED022700BDTO();
        String BatchName = "Upload Image Production Volume & Working Calendar";
        String ProcessBatchName = "Upload Image Production Volume & Working Calendar";
        String ProcessId = "";
        String logLocation = "";
        String FunctionId = "";
        String UserId = "";

        private static readonly String _extFile = CSTDSystemMasterHelper.GetValue("PROD_VOL_IMAGE", "EXT_FILE");
        private static readonly String _destDirUpload = CSTDSystemMasterHelper.GetValue("REPORT", "FULLPATH_UPLOAD");

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {
            throw new NotImplementedException();
        }

        public Dictionary<String, Object> MoveImage(SqlConnection con, string processId, String functionId, String userId, String[] reportParams, String userLocation)
        {
            Dictionary<String, Object> result = new Dictionary<string, object>();
            List<String> messages = new List<String>();
            result["messages"] = messages;

            #region Check report Drektory Upload
            String _extFile = CSTDSystemMasterHelper.GetValue("PROD_VOL_IMAGE", "EXT_FILE");
            String _destDirUpload = CSTDSystemMasterHelper.GetValue("PROD_VOL_IMAGE", "DEST_DIR");
            String _historyDir = CSTDSystemMasterHelper.GetValue("PROD_VOL_IMAGE", "HISTORY_DIR");
            logLocation = "Check Direktori Upload";

            //if (!Directory.Exists(HttpContext.Current.Server.MapPath(templateName)))
            //{
            //    //CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MVPRSTD098E", new String[] { _tempDirUpload }), logLocation);
            //    //messages.Add(CSTDMessageUtil.GetMessage("MVPRSTD098E", new String[] { _tempDirUpload }));
            //    //result["folderdir"] = "notfound";
            //    //return result;
            //    Directory.CreateDirectory(HttpContext.Current.Server.MapPath(templateName));
            //}
            #endregion

            #region Get data from database
            CED022700BSearchDTO searchDTO = new CED022700BSearchDTO();
            string[] items = reportParams[4].Split(';');

            //get report parameter from table queue instead of table parameter
            searchDTO.PROCESS_ID = (items[0].ToString());
            searchDTO.URL_UPLOADED_FILE = (items[1].ToString());
            searchDTO.USER_LOGIN = (items[2].ToString());

            //string[] fileArray = Directory.GetFiles(HttpContext.Current.Server.MapPath(templateName));
            string[] fileArray = System.IO.Directory.GetFiles(sourcePath);
            //List<string> filesList = fileArray.ToList();
            //check result
            if (fileArray == null || fileArray.Length == 0)
            {
                CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MVPRSTD098E", new String[] { _tempDirUpload }), logLocation);
                messages.Add(CSTDMessageUtil.GetMessage("MVPRSTD098E", null));
                result["filesNotFound"] = "notfound";
                return result;
            }
            else
            {
                foreach (string f in fileArray)
                {
                    //cek file in destination folder
                    ////string filename = System.IO.Path.Combine(HttpContext.Current.Server.MapPath(_destDirUpload), f);
                    ////if (File.Exists(filename))
                    ////{
                    ////    CSTDDBLogUtil.CreateLogDetail(con, processId, CSTDMessageUtil.GetMessage("MVPRSTD098E", new String[] { _tempDirUpload }), logLocation);
                    ////    messages.Add(CSTDMessageUtil.GetMessage("MVPRSTD098E", null));
                    ////    result["fileNotFound"] = "imagenotfound";
                    ////    return result;
                    ////}
                    ////else
                    ////{
                    ////    // Use static Path methods to extract only the file name from the path.
                    ////    string fileName = System.IO.Path.GetFileName(f);
                    ////    string destFile = System.IO.Path.Combine(targetPath, fileName);
                    ////    System.IO.File.Copy(f, destFile, true);
                    ////}

                    // Use static Path methods to extract only the file name from the path.
                    string fileName = System.IO.Path.GetFileName(f);
                    string destFile = System.IO.Path.Combine(targetPath, fileName);
                    System.IO.File.Copy(f, destFile, true);
                }
            }
            #endregion

            // Batch tidak mengahilkan file excell

            return result;
        }

        internal void InsertReport(string excelFileName, string processId)
        {
            CED022700BReport dto = new CED022700BReport();
            dto.InProcessId = processId;
            dto.InFileName = excelFileName;
            CED022700BDAO.InsertReport(dto);
        }
    }
}
