﻿using Common.STD.Base;
using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED022700B
{
    public class CED022700BMain : CSTDBaseReport
    {
        private static readonly String _tempDirUpload = CSTDSystemMasterHelper.GetValue("REPORT", "TEMP_UPLOAD");

        public override Dictionary<string, object> ExecuteReport(string processId, string functionId, string userId, string[] reportParameters, string userLocation)
        {

            SqlConnection con = null;

            Dictionary<String, Object> result = new Dictionary<string, object>();
            CED022700BBO ReportBO = new CED022700BBO();
            String BatchName = "Upload Image Production Volume";
            String processSts = "";
            try
            {
                CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MVPRSTD053I", new String[] { BatchName }, "Start log");
                con = CSTDDBUtil.GetNonPooledConnection;
                result = ReportBO.MoveImage(con, processId, functionId, userId, reportParameters, userLocation);
                List<String> messages = (List<String>)result["messages"];

                if (messages.Count > 0)
                {
                    foreach (String message in messages)
                    {
                        Console.WriteLine(message);
                    }

                    var resultData = result.SingleOrDefault(d => d.Key == "filesNotFound");
                    if (resultData.Value != null && resultData.Value.ToString() == "notfound")
                    {
                        processSts = "1";
                    }
                    else
                    {
                        processSts = "2";
                    }
                }
                else
                {
                    //String _targetDir = CSTDSystemMasterHelper.GetValue("REPORT", "EXCEL_REPORT_FOLDER");
                    //Console.WriteLine(reportParameters.Length);
                    //String targetDir = _targetDir;
                    //String reportName = reportBatchName;
                    //NPOI.HSSF.UserModel.HSSFWorkbook workbook = (NPOI.HSSF.UserModel.HSSFWorkbook)result["workbook"];
                    //String excelFileName = reportName + "_" + processId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".xls";
                    //String excelFullPathFileName = targetDir + excelFileName;
                    //FileStream file = new FileStream(excelFullPathFileName, FileMode.Create);
                    //workbook.Write(file);
                    //file.Close();
                    //CSTDReportUtil.RegisterToDownloadTable(con, null, functionId, processId, userId, excelFileName);
                    processSts = "0";
                    CSTDDBLogUtil.CreateLogDetail(con, processId, "MVPRSTD050I", new String[] { BatchName }, "End Log");
                }
            }
            catch (Exception e)
            {
                processSts = "2";
                CSTDDBLogUtil.CreateLogDetail(con, processId, "MVPRSTD075E", new String[] { e.Message }, "");
            }
            finally
            {
                if (con != null)
                {
                    CSTDDBLogUtil.CreateEndLog(con, BatchName, processId, processSts, "");
                    con.Close();
                }
            }
            return result;
        }



    }
}
