﻿using Common.STD.Util;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Report.IPOMS.CED022700B
{
    public class CED022700BDAO
    {
        public static void InsertReport(CED022700BReport dto)
        {
            CSTDMapper.Instance().Insert("CED022700B.InsertReport", dto);
        }

        public static IList<CED022700BDTO> GetReportData(CED022700BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED022700BDTO>("CED022700B.DoSearch", searchDTO);
        }

        //public static void UpdateDataIPPCS(CED030200BSearchDTO searchDTO)
        //{
        //    CSTDMapper.Instance().Update("CED030200B.DoUpdate", null);
        //}

        public static IList<CED022700BDTO> UpdateDataIPPCS(CED022700BSearchDTO searchDTO)
        {
            return CSTDMapper.Instance().QueryForList<CED022700BDTO>("CED022700B.DoUpdate", searchDTO);
        }
    }
}
