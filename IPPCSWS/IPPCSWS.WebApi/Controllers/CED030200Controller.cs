﻿using IPPCSWS.WebApi.Data.SqlServer.Context;
using IPPCSWS.WebApi.Models.CED030200;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPPCSWS.WebApi.Controllers
{
    public class CED030200Controller : ApiController //CorsController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPPCSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(CED030200DailyOrderManifestResponse))]
        [HttpPost, Route("PostReceivingManifest")]
        public async Task<IHttpActionResult> PostReceivingManifest(CED030200DailyOrderManifestRequest parameter)
        {
            int paramCounter = 0;
            string MANIFEST_NO = string.Empty;
            string MANIFEST_RECEIVING_DT = string.Empty;
            string CREATED_BY = string.Empty;
            string PROCESS_ID = string.Empty;

            foreach (var item in parameter.listManifest)
            {
                paramCounter++;
                MANIFEST_NO += item.MANIFEST_NO + (paramCounter < parameter.listManifest.Count() ? "|" : "");
                MANIFEST_RECEIVING_DT += item.MANIFEST_RECEIVING_DT + (paramCounter < parameter.listManifest.Count() ? "|" : "");
                CREATED_BY += item.CREATED_BY + (paramCounter < parameter.listManifest.Count() ? "|" : "");
                PROCESS_ID += item.PROCESS_ID + (paramCounter < parameter.listManifest.Count() ? "|" : "");
            }

            try
            {
                return await Task.Run(() =>
                {
                    var ManifestNo = new SqlParameter
                    {
                        ParameterName = "MANIFEST_NO",
                        Value = MANIFEST_NO
                    };

                    var ManifestReceiving = new SqlParameter
                    {
                        ParameterName = "MANIFEST_RECEIVING_DT",
                        Value = MANIFEST_RECEIVING_DT
                    };

                    var CreatedBy = new SqlParameter
                    {
                        ParameterName = "CREATED_BY",
                        Value = CREATED_BY
                    };

                    var ProcessId = new SqlParameter
                    {
                        ParameterName = "PROCESS_ID",
                        Value = PROCESS_ID
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = ManifestNo;
                    p[1] = ManifestReceiving;
                    p[2] = CreatedBy;
                    p[3] = ProcessId;

                    var o = _context.Database.SqlQuery<CED030200DailyOrderManifestResponse>("exec [SP_CED030200B_PostReceivingManifest] @MANIFEST_NO,@MANIFEST_RECEIVING_DT,@CREATED_BY,@PROCESS_ID", p).ToList<CED030200DailyOrderManifestResponse>();
                    
                    return Ok<List<CED030200DailyOrderManifestResponse>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030200DeliveryResponse))]
        [HttpPost, Route("PostReceivingDeliveryCtl")]
        public async Task<IHttpActionResult> PostReceivingDeliveryCtl(CED030200DeliveryRequest parameter)
        {
            int paramCounter = 0;
            string DELIVERY_NO = string.Empty;
            string DOCK_CD = string.Empty;
            string DELIVERY_STS = string.Empty;
            string ARRIVAL_GATE_DT = string.Empty;
            string ARRIVAL_ACTUAL_DT = string.Empty;
            string ARRIVAL_STATUS = string.Empty;
            string ARRIVAL_GAP = string.Empty;
            string DEPARTURE_ACTUAL_DT = string.Empty;
            string DEPARTURE_GAP = string.Empty;
            string DEPARTURE_STATUS = string.Empty;
            string REMAINING = string.Empty;
            string CREATED_BY = string.Empty;
            string PROCESS_ID = string.Empty;

            foreach (var item in parameter.listDelivery)
            {
                paramCounter++;
                DELIVERY_NO += item.DELIVERY_NO + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                DOCK_CD += item.DOCK_CD + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                DELIVERY_STS += item.DELIVERY_STS + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                ARRIVAL_GATE_DT += item.ARRIVAL_GATE_DT + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                ARRIVAL_ACTUAL_DT += item.ARRIVAL_ACTUAL_DT + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                ARRIVAL_STATUS += item.ARRIVAL_STATUS + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                ARRIVAL_GAP += item.ARRIVAL_GAP + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                DEPARTURE_ACTUAL_DT += item.DEPARTURE_ACTUAL_DT + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                DEPARTURE_GAP += item.DEPARTURE_GAP + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                DEPARTURE_STATUS += item.DEPARTURE_STATUS + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                REMAINING += item.REMAINING + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                CREATED_BY += item.CREATED_BY + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
                PROCESS_ID += item.PROCESS_ID + (paramCounter < parameter.listDelivery.Count() ? "|" : "");
            }

            try
            {
                return await Task.Run(() =>
                {
                    var DeliveryNo = new SqlParameter
                    {
                        ParameterName = "DELIVERY_NO",
                        Value = DELIVERY_NO
                    };

                    var DockCd = new SqlParameter
                    {
                        ParameterName = "DOCK_CD",
                        Value = DOCK_CD
                    };

                    var DeliverySts = new SqlParameter
                    {
                        ParameterName = "DELIVERY_STS",
                        Value = DELIVERY_STS
                    };

                    var ArrivalGateDt = new SqlParameter
                    {
                        ParameterName = "ARRIVAL_GATE_DT",
                        Value = ARRIVAL_GATE_DT
                    };

                    var ArrivalActualDt = new SqlParameter
                    {
                        ParameterName = "ARRIVAL_ACTUAL_DT",
                        Value = ARRIVAL_ACTUAL_DT
                    };

                    var ArrivalSts = new SqlParameter
                    {
                        ParameterName = "ARRIVAL_STATUS",
                        Value = ARRIVAL_STATUS
                    };

                    var ArrivalGap = new SqlParameter
                    {
                        ParameterName = "ARRIVAL_GAP",
                        Value = ARRIVAL_GAP
                    };

                    var DepartureActualDt = new SqlParameter
                    {
                        ParameterName = "DEPARTURE_ACTUAL_DT",
                        Value = DEPARTURE_ACTUAL_DT
                    };

                    var DepartureGap = new SqlParameter
                    {
                        ParameterName = "DEPARTURE_GAP",
                        Value = DEPARTURE_GAP
                    };

                    var DepartureSts = new SqlParameter
                    {
                        ParameterName = "DEPARTURE_STATUS",
                        Value = DEPARTURE_STATUS
                    };

                    var Remaining = new SqlParameter
                    {
                        ParameterName = "REMAINING",
                        Value = REMAINING
                    };

                    var CreatedBy = new SqlParameter
                    {
                        ParameterName = "CREATED_BY",
                        Value = CREATED_BY
                    };

                    var ProcessId = new SqlParameter
                    {
                        ParameterName = "PROCESS_ID",
                        Value = PROCESS_ID
                    };

                    SqlParameter[] p = new SqlParameter[13];
                    p[0] = DeliveryNo;
                    p[1] = DockCd;
                    p[2] = DeliverySts;
                    p[3] = ArrivalGateDt;
                    p[4] = ArrivalActualDt;
                    p[5] = ArrivalSts;
                    p[6] = ArrivalGap;
                    p[7] = DepartureActualDt;
                    p[8] = DepartureGap;
                    p[9] = DepartureSts;
                    p[10] = Remaining;
                    p[11] = CreatedBy;
                    p[12] = ProcessId;

                    var o = _context.Database.SqlQuery<CED030200DeliveryResponse>("exec [SP_CED030200B_PostReceivingDeliveryCtl] @DELIVERY_NO,@DOCK_CD,@DELIVERY_STS,@ARRIVAL_GATE_DT,@ARRIVAL_ACTUAL_DT,@ARRIVAL_STATUS,@ARRIVAL_GAP,@DEPARTURE_ACTUAL_DT,@DEPARTURE_GAP,@DEPARTURE_STATUS,@REMAINING,@CREATED_BY,@PROCESS_ID", p).ToList<CED030200DeliveryResponse>();

                    return Ok<List<CED030200DeliveryResponse>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

    }
}
