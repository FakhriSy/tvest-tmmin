﻿using IPPCSWS.WebApi.Data.SqlServer.Context;
using IPPCSWS.WebApi.Models.CED030100;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPPCSWS.WebApi.Controllers
{
    public class CED030100Controller : ApiController //CorsController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPPCSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(CED030100SupplierMaster))]
        [HttpGet, Route("GetSupplierMaster")]
        public async Task<IHttpActionResult> GetSupplierMaster()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100SupplierMaster>("exec [SP_CED030100B_GetSupplierMaster]").ToList<CED030100SupplierMaster>();
                    return Ok<List<CED030100SupplierMaster>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDockMaster))]
        [HttpGet, Route("GetDockMaster")]
        public async Task<IHttpActionResult> GetDockMaster()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDockMaster>("exec [SP_CED030100B_GetDockMaster]").ToList<CED030100GetDockMaster>();
                    return Ok<List<CED030100GetDockMaster>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetPartInfoMaster))]
        [HttpGet, Route("GetPartInfoMaster")]
        public async Task<IHttpActionResult> GetPartInfoMaster()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetPartInfoMaster>("exec [SP_CED030100B_GetPartInfoMaster]").ToList<CED030100GetPartInfoMaster>();
                    return Ok<List<CED030100GetPartInfoMaster>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetPart))]
        [HttpGet, Route("GetPart")]
        public async Task<IHttpActionResult> GetPart()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetPart>("exec [SP_CED030100B_GetPart]").ToList<CED030100GetPart>();
                    return Ok<List<CED030100GetPart>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetPartUnique))]
        [HttpGet, Route("GetPartUnique")]
        public async Task<IHttpActionResult> GetPartUnique()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetPartUnique>("exec [SP_CED030100B_GetPartUnique]").ToList<CED030100GetPartUnique>();
                    return Ok<List<CED030100GetPartUnique>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetPartType))]
        [HttpGet, Route("GetPartType")]
        public async Task<IHttpActionResult> GetPartType()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetPartType>("exec [SP_CED030100B_GetPartType]").ToList<CED030100GetPartType>();
                    return Ok<List<CED030100GetPartType>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetLogisticPartner))]
        [HttpGet, Route("GetLogisticPartner")]
        public async Task<IHttpActionResult> GetLogisticPartner()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetLogisticPartner>("exec [SP_CED030100B_GetLogisticPartner]").ToList<CED030100GetLogisticPartner>();
                    return Ok<List<CED030100GetLogisticPartner>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDeliveryStationMapping))]
        [HttpGet, Route("GetDeliveryStationMapping")]
        public async Task<IHttpActionResult> GetDeliveryStationMapping()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDeliveryStationMapping>("exec [SP_CED030100B_GetDeliveryStationMapping]").ToList<CED030100GetDeliveryStationMapping>();
                    return Ok<List<CED030100GetDeliveryStationMapping>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetPhysicalDockMapping))]
        [HttpGet, Route("GetPhysicalDockMapping")]
        public async Task<IHttpActionResult> GetPhysicalDockMapping()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetPhysicalDockMapping>("exec [SP_CED030100B_GetPhysicalDockMapping]").ToList<CED030100GetPhysicalDockMapping>();
                    return Ok<List<CED030100GetPhysicalDockMapping>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDailyOrderManifest))]
        [HttpGet, Route("GetDailyOrderManifest")]
        public async Task<IHttpActionResult> GetDailyOrderManifest()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDailyOrderManifest>("exec [SP_CED030100B_GetDailyOrderManifest]").ToList<CED030100GetDailyOrderManifest>();
                    return Ok<List<CED030100GetDailyOrderManifest>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDailyOrderPart))]
        [HttpGet, Route("GetDailyOrderPart")]
        public async Task<IHttpActionResult> GetDailyOrderPart()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDailyOrderPart>("exec [SP_CED030100B_GetDailyOrderPart]").ToList<CED030100GetDailyOrderPart>();
                    return Ok<List<CED030100GetDailyOrderPart>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDeliveryCtlManifest))]
        [HttpGet, Route("GetDeliveryCtlManifest")]
        public async Task<IHttpActionResult> GetDeliveryCtlManifest()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDeliveryCtlManifest>("exec [SP_CED030100B_GetDeliveryCtlManifest]").ToList<CED030100GetDeliveryCtlManifest>();
                    return Ok<List<CED030100GetDeliveryCtlManifest>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDclTlmsOrderAssignment))]
        [HttpGet, Route("GetDclTlmsOrderAssignment")]
        public async Task<IHttpActionResult> GetDclTlmsOrderAssignment()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDclTlmsOrderAssignment>("exec [SP_CED030100B_GetDclTlmsOrderAssignment]").ToList<CED030100GetDclTlmsOrderAssignment>();
                    return Ok<List<CED030100GetDclTlmsOrderAssignment>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDclTlmsDriverData))]
        [HttpGet, Route("GetDclTlmsDriverData")]
        public async Task<IHttpActionResult> GetDclTlmsDriverData()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDclTlmsDriverData>("exec [SP_CED030100B_GetDclTlmsDriverData]").ToList<CED030100GetDclTlmsDriverData>();
                    return Ok<List<CED030100GetDclTlmsDriverData>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDeliveryCtlH))]
        [HttpGet, Route("GetDeliveryCtlH")]
        public async Task<IHttpActionResult> GetDeliveryCtlH()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDeliveryCtlH>("exec [SP_CED030100B_GetDeliveryCtlH]").ToList<CED030100GetDeliveryCtlH>();
                    return Ok<List<CED030100GetDeliveryCtlH>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetDeliveryCtlD))]
        [HttpGet, Route("GetDeliveryCtlD")]
        public async Task<IHttpActionResult> GetDeliveryCtlD()
        {
            try
            {
                return await Task.Run(() =>
                {
                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetDeliveryCtlD>("exec [SP_CED030100B_GetDeliveryCtlD]").ToList<CED030100GetDeliveryCtlD>();
                    return Ok<List<CED030100GetDeliveryCtlD>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(CED030100GetEOData))]
        [HttpPost, Route("PostEOData")]
        public async Task<IHttpActionResult> PostEOData(CED030100GetEODataParam parameter)
        {
            try
            {
                return await Task.Run(() =>
                {
                    var EO_Flag = new SqlParameter
                    {
                        ParameterName = "EO_Flag",
                        Value = parameter.EOFlag
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = EO_Flag;

                    _context.Database.CommandTimeout = 1800;
                    var o = _context.Database.SqlQuery<CED030100GetEOData>("exec [SP_CED030100B_InsertTemp] @EO_Flag", p).ToList<CED030100GetEOData>();
                    return Ok<List<CED030100GetEOData>>(o);
                });
            }
            catch (Exception ae)
            {
                return BadRequest(ae.Message);
            }
        }

    }
}
