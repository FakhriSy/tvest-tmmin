﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Web;
using IPPCSWS.WebApi.Data.Interfaces;
using IPPCSWS.WebApi.Data.SqlServer.Context;

namespace IPPCSWS.WebApi.Data.SqlServer.Unity
{
    /// <summary>
    /// EF unity extension
    /// </summary>
    public class EFUnityExtension : UnityContainerExtension
    {
        /// <summary>
        /// Initialize
        /// </summary>
        protected override void Initialize()
        {
            var dbConnectionString = ConfigurationManager.ConnectionStrings["IPPCSWSConnection"].ConnectionString;

            Container.RegisterType<DbContext, ApplicationDbContext>(new PerResolveLifetimeManager(), new InjectionConstructor(dbConnectionString));

            Container.RegisterType<ITransactionManager, TransactionManager>(new PerResolveLifetimeManager());
        }
    }
}