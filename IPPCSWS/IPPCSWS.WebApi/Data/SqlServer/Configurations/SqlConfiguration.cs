﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Data.Entity.SqlServer;
using System.Linq;
using System.Runtime.Remoting.Messaging;
using System.Web;

namespace IPPCSWS.WebApi.Data.SqlServer.Configurations
{
    /// <summary>
    /// Sql configuration
    /// </summary>
    public class SqlConfiguration : DbConfiguration
    {
        /// <summary>
        /// Constructor
        /// </summary>
        public SqlConfiguration()
        {
            SetExecutionStrategy("System.Data.SqlClient", () => SuspendExecutionStrategy ? (IDbExecutionStrategy)new DefaultExecutionStrategy() : new SqlAzureExecutionStrategy());
        }

        /// <summary>
        /// Suspended execution strategy
        /// </summary>
        public static bool SuspendExecutionStrategy
        {
            get
            {
                return (bool?)CallContext.LogicalGetData("SuspendExecutionStrategy") ?? false;
            }
            set
            {
                CallContext.LogicalSetData("SuspendExecutionStrategy", value);
            }
        }
    }
}