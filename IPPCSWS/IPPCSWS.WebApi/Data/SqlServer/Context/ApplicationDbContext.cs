﻿using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.SqlClient;
using System.Linq;
using System.Web;
using IPPCSWS.WebApi.Data.SqlServer.Configurations;
using IPPCSWS.WebApi.Models;
using System.Data.Entity.Infrastructure;

namespace IPPCSWS.WebApi.Data.SqlServer.Context
{
    public class ApplicationDbContextSystem : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContextSystem()
            : base("IPPCSWSConnection", throwIfV1Schema: false)
        {
            Configuration.ProxyCreationEnabled = false;
            Configuration.LazyLoadingEnabled = false;
        }

        public static ApplicationDbContextSystem Create()
        {
            return new ApplicationDbContextSystem();
        }

    }

    /// <summary>
    /// Application db context
    /// </summary>
    public class ApplicationDbContext : DbContext
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="plainConnection">Plain connection</param>
        public ApplicationDbContext(string plainConnection)
            : base(new SqlConnection(plainConnection), false)
        {
            this.Database.Log = s => System.Diagnostics.Debug.WriteLine(s);
            Database.SetInitializer<ApplicationDbContext>(null);

            // Get the ObjectContext related to this DbContext
            var objectContext = (this as IObjectContextAdapter).ObjectContext;

            // Sets the command timeout for all the commands
            objectContext.CommandTimeout = 1800;
        }

        /// <summary>
        /// On model creating
        /// </summary>
        /// <param name="modelBuilder">Model builder</param>
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.HasDefaultSchema(Schema.DefaultSchema);
        }
    }
}