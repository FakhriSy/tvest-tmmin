﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPPCSWS.WebApi.Models.CED030100
{
    public class CED030100
    {
    }

    public class CED030100SupplierMaster
    {
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_ABBREVIATION { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDockMaster
    {
        public string DOCK_CD { get; set; }
        public string PLANT_CD { get; set; }
        public string DOCK_NM { get; set; }
        public string PRODUCTION_DT { get; set; }
        public string COMPANY_CD { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetPartInfoMaster
    {
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string DOCK_CD { get; set; }
        public string KANBAN_NO { get; set; }
        public string PART_NAME { get; set; }
        public string PCK_TYPE { get; set; }
        public string KANBAN_QTY { get; set; }
        public string SAFETY_PCS { get; set; }
        public string LINE_LEAD_TIME { get; set; }
        public string START_DT { get; set; }
        public string END_DT { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetPart
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetPartUnique
    {
        public string PART_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string UNIQUE_NO { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetPartType
    {
        public string PART_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string QTY_KANBAN { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetLogisticPartner
    {
        public string LOG_PARTNER_CD { get; set; }
        public string TCYMDFROM { get; set; }
        public string LOG_PARTNER_NAME { get; set; }
        public string ACTIVE_FLAG { get; set; }
        public string LOG_PARTNER_ADDRESS { get; set; }
        public string POSTAL_CD { get; set; }
        public string CITY { get; set; }
        public string ATTENTION { get; set; }
        public string PHONE_NUMBER { get; set; }
        public string FAX_NUMBER { get; set; }
        public string NPWP { get; set; }
        public string DELETION_FLAG { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDeliveryStationMapping
    {
        public string DOOR_CD_TLMS { get; set; }
        public string DOCK_CD { get; set; }
        public string PHYSICAL_DOCK { get; set; }
        public string PHYSICAL_STATION { get; set; }
        public string USED_FLAG { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetPhysicalDockMapping
    {
        public string PHYSICAL_LOGPTCD { get; set; }
        public string PHYSICAL_PLANTCD { get; set; }
        public string PHYSICAL_DOCKCD { get; set; }
        public string LOGICAL_LOGPTCD { get; set; }
        public string LOGICAL_PLANTCD { get; set; }
        public string LOGICAL_DOCKCD { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDailyOrderManifest
    {
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string ORDER_TYPE { get; set; }
        public string ORDER_RELEASE_DT { get; set; }
        public string COMPANY_CD { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string TOTAL_ITEM { get; set; }
        public string TOTAL_QTY { get; set; }
        public string ERROR_QTY { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string DELAY_FLAG { get; set; }
        public string MANIFEST_RECEIVING_FLAG { get; set; }
        public string P_LANE_CD { get; set; }
        public string P_LANE_SEQ { get; set; }
        public string P_LANE_DT { get; set; }
        public string SYNC_FLAG { get; set; }
        public string DEPARTURE_PLAN_DT { get; set; }
    }

    public class CED030100GetDailyOrderPart
    {
        public string MANIFEST_NO { get; set; }
        public string ITEM_NO { get; set; }
        public string SUPPLIER_PLANT { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string PART_BARCODE { get; set; }
        public string PART_NO { get; set; }
        public string KANBAN_PRINT_ADDRESS { get; set; }
        public string KANBAN_NO { get; set; }
        public string QTY_PER_CONTAINER { get; set; }
        public string ORDER_QTY { get; set; }
        public string ORDER_PLAN_QTY_LOT { get; set; }
        public string RECEIVED_STATUS { get; set; }
        public string PROCESS_TYPE { get; set; }
        public string CASE_NO { get; set; }
        public string MOD_DST_CD { get; set; }
        public string BOX_NO { get; set; }
        public string PLN_PCK_DT { get; set; }
        public string CONT_SNO { get; set; }
        public string LOT_MOD_NO { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDeliveryCtlManifest
    {
        public string DELIVERY_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string STATION { get; set; }
        public string MANIFEST_NO { get; set; }
        public string MANIFEST_TYPE { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDclTlmsOrderAssignment
    {
        public string CONCTOCRTE { get; set; }
        public string CONCTOCRTETRP { get; set; }
        public string CONCTODRTEDATE { get; set; }
        public string RTEGRPCD { get; set; }
        public string RUNSEQ { get; set; }
        public string RTEDATE { get; set; }
        public string SUPPCD { get; set; }
        public string SUPPPLANTCD { get; set; }
        public string SUPPDOCKCD { get; set; }
        public string NUMPALLETE { get; set; }
        public string ORDDATE { get; set; }
        public string ORDSEQ { get; set; }
        public string RCVCOMPDOCKCD { get; set; }
        public string DHNDSTRTIME2 { get; set; }
        public string DHNDENDTIME2 { get; set; }
        public string SUPPNAME { get; set; }
        public string PARTEMPKBTYPECD { get; set; }
        public string UNLOADSTRDATE { get; set; }
        public string UNLOADSTRTIME { get; set; }
        public string UNLOADENDDATE { get; set; }
        public string UNLOADENDTIME { get; set; }
        public string PLANE { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDclTlmsDriverData
    {
        public string APPLTERMFROM { get; set; }
        public string APPLTERMTO { get; set; }
        public string RTEGRPCD { get; set; }
        public string RTEDATE { get; set; }
        public string RUNSEQ { get; set; }
        public string LOGPTCD { get; set; }
        public string DOCKCD { get; set; }
        public string PLANTCD { get; set; }
        public string INBOUTBFLAG { get; set; }
        public string DRIVERACCSEQ { get; set; }
        public string LOGPARTNERCD { get; set; }
        public string ARRVDATETIME { get; set; }
        public string DPTDATETIME { get; set; }
        public string TRAVELDISTKM { get; set; }
        public string DOORCD { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetDeliveryCtlH
    {
        public string DELIVERY_NO { get; set; }
        public string DELIVERY_TYPE { get; set; }
        public string REVISE_NO { get; set; }
        public string PICKUP_DT { get; set; }
        public string ROUTE { get; set; }
        public string RATE { get; set; }
        public string LP_CD { get; set; }
        public string CONFIRM_STS { get; set; }
        public string DELIVERY_STS { get; set; }
        public string CANCEL_FLAG { get; set; }
        public string APPROVE_BY { get; set; }
        public string APPROVE_DT { get; set; }
        public string REJECT_BY { get; set; }
        public string REJECT_DT { get; set; }
        public string NOTIFY_STS { get; set; }
        public string SYNC_FLAG { get; set; }
        public string PUSH_TO_WEB_FLAG { get; set; }
    }

    public class CED030100GetDeliveryCtlD
    {
        public string DELIVERY_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string STATION { get; set; }
        public string ARRIVAL_GATE_DT { get; set; }
        public string ARRIVAL_PLAN_DT { get; set; }
        public string DEPARTURE_PLAN_DT { get; set; }
        public string ARRIVAL_STATUS { get; set; }
        public string ARRIVAL_GAP { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string DEPARTURE_ACTUAL_DT { get; set; }
        public string DEPARTURE_STATUS { get; set; }
        public string DEPARTURE_GAP { get; set; }
        public string REMAINING { get; set; }
        public string DOORCD { get; set; }
        public string ABANDON_FLAG { get; set; }
        public string CONFIRM_FLAG { get; set; }
        public string SYNC_FLAG { get; set; }
    }

    public class CED030100GetEODataParam
    {
        public int EOFlag { get; set; }
    }

    public class CED030100GetEOData
    {
        public string isResult { get; set; }
    }

}