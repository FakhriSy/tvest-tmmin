﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using IPPCSWS.WebApi.Data.SqlServer.Context;
using IPPCSWS.WebApi.Models;

namespace IPPCSWS.WebApi.Models
{
    public class CommonRepository
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["CEDWSConnection"].ConnectionString);
            }
        }
        private const String QUICK_REPLY_SYNTAX = "{replies:title=";
        private const String QUICK_REPLY_SYNTAX_SUFFIX = "}";
        private const String BUTTON_SYNTAX = "{button:";
        private const String BUTTON_SYNTAX_SUFFIX = "}";
        private const String IMAGE_SYNTAX = "{image:";
        private const String IMAGE_SYNTAX_SUFFIX = "}";
        private const String FORM_SYNTAX = "{form:";
        private const String FORM_SYNTAX_SUFFIX = "}";




        private const String BUBLE_SEPARATOR = "&split&";
        private const String QUICK_REPLY_SEPARATOR = "@===@";
        private const String COMMA = ",";
        private const String EQUAL = "=";

        private const String IMAGE_STICKER_SYSTEM_ID = "IMAGE_STICKER";

        private const String HALLO = "HALLO";
        private const String GESTURING_OK = "GESTURING_OK";
        private const String IDEA_ENTHUSIAS_STAND = "IDEA_ENTHUSIAS_STAND";
        private const String IDEA_ENTHUSIAS = "IDEA_ENTHUSIAS";
        private const String TIPPING_HAND = "TIPPING_HAND";
        private const String CONFUSED_STAND = "CONFUSED_STAND";
        private const String CONFUSED = "CONFUSED";
        private const String CONFUSED_OPEN_HAND = "CONFUSED_OPEN_HAND";
        private const String THANK_YOU_STAND = "THANK_YOU_STAND";
        private const String THANK_YOU = "THANK_YOU";
        private const String SAD_SORRY = "SAD_SORRY";
        private const String HAIR_REVISED = "HAIR_REVISED";
        private const String SPORT_OUTFIT = "SPORT_OUTFIT";

        public String Hallo { get { return HALLO; } }
        public String GesturingOK { get { return GESTURING_OK; } }
        public String IdeaEnthusiasStand { get { return IDEA_ENTHUSIAS_STAND; } }
        public String IdeaEnthusias { get { return IDEA_ENTHUSIAS; } }
        public String TippingHand { get { return TIPPING_HAND; } }
        public String Confused { get { return CONFUSED; } }
        public String ConfusedStand { get { return CONFUSED_STAND; } }
        public String ConfusedOpenHand { get { return CONFUSED_OPEN_HAND; } }
        public String ThankYouHand { get { return THANK_YOU_STAND; } }
        public String ThankYou { get { return THANK_YOU; } }
        public String SadSorry { get { return SAD_SORRY; } }
        public String HairRevised { get { return HAIR_REVISED; } }
        public String SportOutfit { get { return SPORT_OUTFIT; } }
        public String BubleSeparator { get { return BUBLE_SEPARATOR; } }

        StringBuilder quickReply;
        StringBuilder button;

        public OutputResponse OutputResponse(string output, bool next, bool success, bool repeat, bool agent)
        {
            OutputResponse outResp = new OutputResponse();
            OutputModel outModel = new OutputModel();
            outModel.output = output;
            outResp.value = outModel;
            outResp.next = next;
            outResp.success = success;
            outResp.repeat = repeat;
            outResp.agent = agent;
            return outResp;
        }
        public OutputResponse OutputResponseOther(string output, string model, bool next, bool success, bool repeat, bool agent, Dictionary<string, string> entities)
        {
            OutputResponse outResp = new OutputResponse();
            OutputModel outModel = new OutputModel();
            outModel.output = output;
            outResp.value = outModel;
            outResp.next = next;
            outResp.success = success;
            outResp.repeat = repeat;
            outResp.agent = agent;
            outResp.entities = entities;
            return outResp;
        }

        public string QuickReplyResponse(string title, List<string> value)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                foreach (string item in value)
                {
                    quickReply.Append(item).Append(QUICK_REPLY_SEPARATOR).Append(item).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        public string QuickReplyResponse(string title, List<Tuple<string, string>> tuples)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                foreach (Tuple<string, string> item in tuples)
                {
                    quickReply.Append(item.Item1).Append(QUICK_REPLY_SEPARATOR).Append(item.Item2).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        /*untuk pengkondisian dengan parameter lain. penambahan parameter bawaan untuk kondisi2 berikutnya.*/
        public string QuickReplayResponseOther(string title, List<string> value, string other)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                foreach (string item in value)
                {
                    quickReply.Append(item).Append(QUICK_REPLY_SEPARATOR).Append(item).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        public string FailedResponse()
        {
            string outputTemp = "";
            outputTemp = "Maaf saya tidak mengerti.�";
            return outputTemp;
        }
        public string QuickReplayResponseTest(string title, List<string> value, List<string> valueid)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                string[] values = value.ToArray();
                string[] valuesid = valueid.ToArray();
                for (int i = 0; i < values.Length; i++)
                {
                    quickReply.Append(values[i]).Append(QUICK_REPLY_SEPARATOR).Append(valuesid[i]).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        public string GenerateForm(string formId, List<Tuple<string, string>> listSetValue)
        {
            StringBuilder temp = new StringBuilder();
            temp.Append(FORM_SYNTAX);
            temp.Append(formId);
            string outputTemp = "";
            if (listSetValue.Count > 0)
            {
                temp.Append(COMMA);
                foreach (Tuple<string, string> item in listSetValue)
                {
                    temp.Append(item.Item1);
                    temp.Append(EQUAL);
                    temp.Append(item.Item2);
                    temp.Append(COMMA);
                }
                temp.Append(FORM_SYNTAX_SUFFIX);
                outputTemp = temp.ToString();
                int index = outputTemp.LastIndexOf(',');
                outputTemp = outputTemp.Remove(index, 1);
            }
            return outputTemp;
        }
        public string GetValueTextByIDType(string systemId, string systemType)
        {
            string outputTemp = "";
            string sql = @" SELECT SYSTEM_VALUE_TEXT as SystemValueText" +
                          " FROM TB_M_SYSTEM " +
                          " WHERE SYSTEM_ID = '" + systemId + "'" +
                          " AND SYSTEM_TYPE = '" + systemType + "'"
                          ;
            var o = _context.Database.SqlQuery<SystemMaster>(sql).ToList<SystemMaster>();
            return o.FirstOrDefault().SystemValueText.ToString();
        }
        public string ToRupiah(int angka)
        {
            return String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
        }
        public string Shorten(string longUrl, string login, string apikey, out string message)
        {
            StringBuilder msg = new StringBuilder();
            var url = string.Format("http://api.bit.ly/v3/shorten?login={1}&apiKey={2}&longUrl={0}", HttpUtility.UrlEncode(longUrl), login, apikey);
            msg.Append("poin = 0 " + url);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            msg.Append("poin = 1 ");
            try
            {
                WebResponse response = request.GetResponse();
                msg.Append("poin = 2 ");
                using (Stream responseStream = response.GetResponseStream())
                {
                    msg.Append("poin = 3 ");
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    msg.Append("poin = 4 ");
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    msg.Append("poin = 5 ");
                    dynamic jsonResponse = js.Deserialize<dynamic>(reader.ReadToEnd());
                    msg.Append("poin = 6 ");
                    string s = jsonResponse["data"]["url"];
                    msg.Append("success =  " + s);
                    message = msg.ToString();
                    s = s.Replace("http", "https");
                    return s;
                }
            }
            catch (WebException ex)
            {
                msg.Append("ex =  " + ex.Message);
                message = msg.ToString();
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

    }    
}