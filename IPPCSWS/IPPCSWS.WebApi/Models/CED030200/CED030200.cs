﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPPCSWS.WebApi.Models.CED030200
{
    public class CED030200DailyOrderManifestRequest
    {
        public List<listManifestData> listManifest { get; set; }
    }

    public class listManifestData
    {
        public string MANIFEST_NO { get; set; }
        public string MANIFEST_RECEIVING_DT { get; set; }
        public string CREATED_BY { get; set; }
        public string PROCESS_ID { get; set; }
    }

    public class CED030200DailyOrderManifestResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED030200DeliveryRequest
    {
        public List<listDeliveryData> listDelivery { get; set; }
    }

    public class listDeliveryData
    {
        public string DELIVERY_NO { get; set; }
        public string DOCK_CD { get; set; }
        public string DELIVERY_STS { get; set; }
        public string ARRIVAL_GATE_DT { get; set; }
        public string ARRIVAL_ACTUAL_DT { get; set; }
        public string ARRIVAL_STATUS { get; set; }
        public string ARRIVAL_GAP { get; set; }
        public string DEPARTURE_ACTUAL_DT { get; set; }
        public string DEPARTURE_GAP { get; set; }
        public string DEPARTURE_STATUS { get; set; }
        public string REMAINING { get; set; }
        public string CREATED_BY { get; set; }
        public string PROCESS_ID { get; set; }
    }

    public class CED030200DeliveryResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }
}