﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPPCSWS.WebApi.Models
{
    public class OutputResponse
    {
        public OutputModel value { get; set; }
        public bool next { get; set; }
        public bool success { get; set; }
        public bool repeat { get; set; }
        public bool agent { get; set; }
        public Dictionary<string, string> entities { get; set; }
    }
}