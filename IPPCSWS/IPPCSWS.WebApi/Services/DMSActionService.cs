﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IPPCSWS.WebApi.Models;
using IPPCSWS.WebApi.Services.Interfaces;

namespace IPPCSWS.WebApi.Services
{
    /// <summary>
    /// DMS action service
    /// </summary>
    public class DMSActionService : IDMSActionService
    {
        /// <summary>
        /// Data action service
        /// </summary>
        [Dependency]
        public IDataActionService DataActionService { get; set; }

        /// <summary>
        /// DMS service
        /// </summary>
        [Dependency]
        public IDMSService DMSService { get; set; }
    }
}