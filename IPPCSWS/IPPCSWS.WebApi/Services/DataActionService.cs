﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IPPCSWS.WebApi.Models;
using IPPCSWS.WebApi.Services.Interfaces;

namespace IPPCSWS.WebApi.Services
{
    /// <summary>
    /// Data action service
    /// </summary>
    public class DataActionService : IDataActionService
    {
        /// <summary>
        /// Data service
        /// </summary>
        [Dependency]
        public IDataService DataService { get; set; }
    }
}