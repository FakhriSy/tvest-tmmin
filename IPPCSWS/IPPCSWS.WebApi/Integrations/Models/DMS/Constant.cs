﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPPCSWS.WebApi.Integrations.Models.DMS
{
    /// <summary>
    /// Constant
    /// </summary>
    public class Constant
    {
        /// <summary>
        /// Create part url
        /// </summary>
        public const string CreatePartUrl = "/api/as/tpos/parts/add";

        /// <summary>
        /// Create claim url
        /// </summary>
        public const string CreateClaimUrl = "/api/as/tpos/claim";
    }
}