﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;

namespace IPPCSWS.WebApi.Integrations.Utilities
{
    /// <summary>
    /// Integration result
    /// </summary>
    /// <typeparam name="T_in"></typeparam>
    /// <typeparam name="T_out"></typeparam>
    public class IntegrationResult<T_in, T_out>
    {
        #region property

        /// <summary>
        /// Request url
        /// </summary>
        public string RequestUrl { get; set; }

        /// <summary>
        /// Request payload
        /// </summary>
        public string RequestPayload { get; set; }

        /// <summary>
        /// Request method
        /// </summary>
        public string RequestMethod { get; set; }

        /// <summary>
        /// Disable auto conversion
        /// </summary>
        public bool DisableAutoConversion { get; set; }

        #endregion

        #region variables

        /// <summary>
        /// Message
        /// </summary>
        private Task<HttpResponseMessage> message;

        /// <summary>
        /// Action
        /// </summary>
        private Func<T_in, T_out> action;

        #endregion

        #region construction

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Message</param>
        public IntegrationResult(Task<HttpResponseMessage> message)
        {
            this.message = message;
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="message">Message</param>
        /// <param name="action">Action</param>
        public IntegrationResult(Task<HttpResponseMessage> message, Func<T_in, T_out> action)
        {
            this.message = message;
            this.action = action;
        }

        #endregion

        #region public method

        /// <summary>
        /// Build result
        /// </summary>
        /// <returns></returns>
        public async Task<T_out> BuildResult()
        {
            string errorContent = string.Empty;

            try
            {
                var serviceMessage = await message.ConfigureAwait(false);

                if (serviceMessage.StatusCode == System.Net.HttpStatusCode.NotFound)
                    return default(T_out);

                var content = serviceMessage.Content;

                if (!serviceMessage.IsSuccessStatusCode)
                    errorContent = await content.ReadAsStringAsync();

                serviceMessage.EnsureSuccessStatusCode();

                if (action != null)
                {
                    var actionResult = await content.ReadAsAsync<T_in>();
                    return action.Invoke(actionResult);
                }
                else
                {
                    if (DisableAutoConversion) return default(T_out);

                    var actionResult = await content.ReadAsAsync<T_in>();
                    return AutoMapper.Mapper.Map<T_out>(actionResult);
                }
            }
            catch (Exception ex)
            {
                var innerMessage = errorContent;
                innerMessage += ex.ToString();

                var url = RequestUrl == null ? "external service" : RequestUrl;
                var method = RequestMethod == null ? "" : string.Format(" (HTTP {0})", RequestMethod);
                var payload = RequestPayload == null ? "" : string.Format(" Json payload : {0}", RequestPayload);
                var message = string.Format("Problem occured in {0}{1}.{2}", url, method, payload);

                var exception = new Exception(message, new Exception(innerMessage));
                throw exception;
            }
        }

        #endregion
    }
}