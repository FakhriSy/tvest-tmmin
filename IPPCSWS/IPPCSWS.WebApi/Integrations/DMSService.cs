﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IPPCSWS.WebApi.Integrations.Utilities;
using IPPCSWS.WebApi.Models;
using IPPCSWS.WebApi.Services.Interfaces;

namespace IPPCSWS.WebApi.Integrations
{
    /// <summary>
    /// DMS service
    /// </summary>
    public class DMSService : BaseService, IDMSService
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="client">Client</param>
        public DMSService(IntegrationClient client) : base(client)
        {

        }
    }
}