IF NOT EXISTS(SELECT 1 FROM AG100_DB.dbo.TB_M_SYSTEM WHERE SYS_CAT = 'POS' AND SYS_SUB_CAT = 'REPORT_JUNDATE' AND SYS_CD = 'WIP_COUNT')
BEGIN
	INSERT INTO AG100_DB.dbo.TB_M_SYSTEM (SYS_CAT, SYS_SUB_CAT, SYS_CD, SYS_VAL, REMARK, CREATED_DT, CREATED_BY, UPDATED_DT, UPDATED_BY)
	VALUES ('POS', 'REPORT_JUNDATE', 'WIP_COUNT', '2', '', GETDATE(), 'SYSTEM', GETDATE(), 'SYSTEM');
END;
