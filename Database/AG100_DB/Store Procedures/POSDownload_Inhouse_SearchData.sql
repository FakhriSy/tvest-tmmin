ALTER PROCEDURE [dbo].[POSDownload_Inhouse_SearchData] 
    -- Add the parameters for the stored procedure here
@PROD_DATE DATE,					-- date with yyyy-MM-dd
@SHIFT AS varchar(6) = ''						-- int with number between 1 and 2
AS

BEGIN
    -- Procedure body
    SET NOCOUNT ON; -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.

    BEGIN TRY
        DECLARE 
            @@MSG_TXT VARCHAR(MAX),
            @@MSG_TYPE CHAR,

			@POS_TYPE INT = 1 -- Check at TB_M_SYSTEM on SYS_SUB_CAT column > POS_TYPE

			-- Check if @PROD_DATE is not provided
			IF @PROD_DATE IS NULL OR TRY_CONVERT(DATE, @PROD_DATE) IS NULL OR ISDATE(CONVERT(VARCHAR(10), @PROD_DATE, 126)) = 0 OR @PROD_DATE = ''
			BEGIN
				RAISERROR('PROD_DATE must be provided in correct date format.', 16, 1)
				RETURN
			END

			SELECT
				'Sub_Packing_Line' as SubPackingLine,
				--Change start by Hendra Agung - 20240603 - Add ZONE_CD
				ZONE_CD as PackingLineCode,
				--Change end by Hendra Agung - 20240603 - Add ZONE_CD
				'Rev' as REV,
                (SELECT CAST(SUM(mp) AS VARCHAR)
                    FROM [AG100_DB].[dbo].[TB_R_MVPR_FORECAST]
                    where PKG_MTH= LEFT(CONVERT(varchar, @PROD_DATE,112),6)) as MP,
				'98' as EFF,
				REPLACE(CONVERT(NVARCHAR, PROD_DT, 106), ' ', '-') AS ProdDate,
				CASE 
					WHEN DATENAME(dw, PROD_DT) = 'Monday' THEN 'Senin' 
					WHEN DATENAME(dw, PROD_DT) = 'Tuesday' THEN 'Selasa' 
					WHEN DATENAME(dw, PROD_DT) = 'Wednesday' THEN 'Rabu' 
					WHEN DATENAME(dw, PROD_DT) = 'Thursday' THEN 'Kamis' 
					WHEN DATENAME(dw, PROD_DT) = 'Friday' THEN 'Jumat' 
					WHEN DATENAME(dw, PROD_DT) = 'Saturday' THEN 'Sabtu' 
					WHEN DATENAME(dw, PROD_DT) = 'Sunday' THEN 'Minggu' 
					ELSE DATENAME(dw, PROD_DT) -- For any unexpected values, return the original English name
				END AS ProdDayName,
				CAST(ROW_NUMBER() OVER (ORDER BY PLAN_START ASC) AS varchar)  as NO,
				'1' as QTY,
				CAST(Box_qty as Varchar) as QtyBox,
				CAST(MODULE_DEST_CD as Varchar) as Dest,
				CAST(LOT_MODULE_NO as varchar) as LotModuleNo,
				CASE WHEN CASE_NO = '***' THEN NULL ELSE CAST(CASE_NO as varchar) END AS CaseNo,
				CAST(SHIFT AS varchar) as SHIFT,
				CAST(CONTAINER_SNO as varchar)as RENBAN,
				CAST(MOD_TYPE as varchar)as ModType,
				CAST(M3 as varchar)as M3,  --update mp, eff, m3, ratio, plan time 13 mei 2024 irsyad
				case when RATIO is not null then CAST(RATIO as varchar)
					else null 
					end  RATIO,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) PlanTime, --update convert datetime ke hh:mm 15 mei 2024 irsyad
				CAST(PLAN_CT AS varchar) as CT
			FROM 
				[AG100_DB].[dbo].[TB_R_POS]

			WHERE 
				POS_TYPE = @POS_TYPE
				AND ((@SHIFT IS NULL OR @SHIFT = '') OR SHIFT = @SHIFT)
				AND CAST(PROD_DT AS DATE) = CAST(@PROD_DATE AS DATE)
				AND ISNULL(ZONE_CD, '') <> ''
			ORDER BY 
			--Change start by Hendra Agung - 20240603 - Add ZONE_CD
				ZONE_CD ASC, PLAN_START ASC, PROD_DT DESC;
			--Change end by Hendra Agung - 20240603 - Add ZONE_CD


    END TRY
    BEGIN CATCH
        SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
        SET @@MSG_TYPE = 'E'

        SELECT @@MSG_TYPE + '|' + @@MSG_TXT
    END CATCH
END
;

