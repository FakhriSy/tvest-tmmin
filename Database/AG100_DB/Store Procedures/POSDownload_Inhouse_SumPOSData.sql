IF EXISTS (SELECT * FROM sys.objects 
	WHERE object_id = OBJECT_ID(N'[dbo].[POSDownload_Inhouse_SumPOSData]') AND type in (N'P'))
BEGIN
    DROP PROCEDURE [dbo].[POSDownload_Inhouse_SumPOSData];
END;
GO

CREATE PROCEDURE [dbo].[POSDownload_Inhouse_SumPOSData] 
    -- Add the parameters for the stored procedure here
@PROD_DATE DATE,					-- date with yyyy-MM-dd
@SHIFT AS varchar(6) = ''						-- int with number between 1 and 2
AS

BEGIN
    -- Procedure body
    SET NOCOUNT ON; -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.

    BEGIN TRY
        DECLARE 
            @@MSG_TXT VARCHAR(MAX),
            @@MSG_TYPE CHAR,

			@POS_TYPE INT = 1 -- Check at TB_M_SYSTEM on SYS_SUB_CAT column > POS_TYPE

			-- Check if @PROD_DATE is not provided
			IF @PROD_DATE IS NULL OR TRY_CONVERT(DATE, @PROD_DATE) IS NULL 
				OR ISDATE(CONVERT(VARCHAR(10), @PROD_DATE, 126)) = 0 OR @PROD_DATE = ''
			BEGIN
				RAISERROR('PROD_DATE must be provided in correct date format.', 16, 1)
				RETURN
			END

			SELECT
				ZONE_CD as PackingLineCode,
				MODULE_DEST_CD as Dest,
				LOT_MODULE_NO as LotModuleNo,
				SHOOTER_NO as ShooterNo,
				CASE WHEN CASE_NO = '***' THEN NULL ELSE CASE_NO END as CaseNo,
				CAST(SHIFT AS varchar) as Shift,
				CONTAINER_SNO as Renban,
				LEFT(CONTAINER_SNO, 1) as RenbanCode,
				MOD_TYPE as ModType,
				PLAN_START as PlanStartTime, 
				PLAN_FINISH as PlanFinishTime,
				PLAN_CT as CT,
				'' as Cumm,
				PROD_DT as ProdDate
			FROM 
				[AG100_DB].[dbo].[TB_R_POS]

			WHERE 
				POS_TYPE = @POS_TYPE
				AND ((@SHIFT IS NULL OR @SHIFT = '') OR SHIFT = @SHIFT)
				AND CAST(PROD_DT AS DATE) = CAST(@PROD_DATE AS DATE)
				AND ISNULL(ZONE_CD, '') <> '' 
			ORDER BY 
				ZONE_CD ASC, PLAN_START ASC, PROD_DT DESC;


    END TRY
    BEGIN CATCH
        SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
        SET @@MSG_TYPE = 'E'

        SELECT @@MSG_TYPE + '|' + @@MSG_TXT
    END CATCH
END
;
