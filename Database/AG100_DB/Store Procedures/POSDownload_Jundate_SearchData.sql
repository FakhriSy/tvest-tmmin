ALTER PROCEDURE [dbo].[POSDownload_Jundate_SearchData] 
    -- Add the parameters for the stored procedure here
@PROD_DATE DATE,					-- date with yyyy-MM-dd
--@PROD_LINE INT,						-- the line represents Plant Production line 
@SHIFT INT,							-- int with number between 1 and 2
@WIP_COUNT INT,
@WIP_FLAG INT
AS

BEGIN
    -- Procedure body
    SET NOCOUNT ON; -- SET NOCOUNT ON added to prevent extra result sets from interfering with SELECT statements.

    BEGIN TRY
        DECLARE 
            @@MSG_TXT VARCHAR(MAX),
            @@MSG_TYPE CHAR,
			

			@POS_TYPE INT = 2 -- Check at TB_M_SYSTEM on SYS_SUB_CAT column > POS_TYPE

			-- Check if @PROD_DATE is not provided
			IF @PROD_DATE IS NULL OR TRY_CONVERT(DATE, @PROD_DATE) IS NULL OR ISDATE(CONVERT(VARCHAR(10), @PROD_DATE, 126)) = 0 OR @PROD_DATE = ''
			BEGIN
				RAISERROR('PROD_DATE must be provided in correct date format.', 16, 1)
				RETURN
			END


			-- Add your SQL logic here 
			DECLARE @NextDay DATE;
			SET @NextDay = DATEADD(DAY, 1, @PROD_DATE);

			IF @WIP_FLAG = '' OR @WIP_FLAG = 0
			BEGIN
			SELECT
				REPLACE(CONVERT(NVARCHAR, PROD_DT, 106), ' ', '-') AS ProdDate,
				CASE 
					WHEN DATENAME(dw, PROD_DT) = 'Monday' THEN 'Senin' 
					WHEN DATENAME(dw, PROD_DT) = 'Tuesday' THEN 'Selasa' 
					WHEN DATENAME(dw, PROD_DT) = 'Wednesday' THEN 'Rabu' 
					WHEN DATENAME(dw, PROD_DT) = 'Thursday' THEN 'Kamis' 
					WHEN DATENAME(dw, PROD_DT) = 'Friday' THEN 'Jumat' 
					WHEN DATENAME(dw, PROD_DT) = 'Saturday' THEN 'Sabtu' 
					WHEN DATENAME(dw, PROD_DT) = 'Sunday' THEN 'Minggu' 
					ELSE DATENAME(dw, PROD_DT) -- For any unexpected values, return the original English name
				END AS ProdDayName,
				CAST(ROW_NUMBER() OVER (ORDER BY PLAN_START ASC) AS varchar)  as no,
				SHOOTER_NO as shutterNo,
				MODULE_DEST_CD as dest,
				LOT_MODULE_NO as lotModuleNo,
				CASE WHEN CASE_NO = '***' THEN NULL ELSE CASE_NO END AS caseNo,
				CAST(SHIFT AS varchar) as shift,
				CONTAINER_SNO as renban,
				MOD_TYPE as modType,
				CONCAT(MODULE_DEST_CD,LEFT(LOT_MODULE_NO,2),CASE_NO)AS moduleCode,
				DOLLY_1 as dolly1,
				DOLLY_2 as dolly2,
				DOLLY_3 as dolly3,
				DOLLY_4 as dolly4,
				DOLLY_5 as dolly5,
				DOLLY_6 as dolly6,
				DOLLY_7 as dolly7,
				DOLLY_8 as dolly8,
				DOLLY_9 as dolly9,
				ZONE_CD as line,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) AS planStart,
				CAST(PLAN_CT AS varchar) as cycleTime,
				'' as shutterNoWip,
				'' as destWip,
				'' as lotModuleNoWip,
				'' AS caseNoWip,
				'' as shiftWip,
				'' as renbanWip,
				'' as modTypeWip,
				'' AS moduleCodeWip,
				'' as dolly1Wip,
				'' as dolly2Wip,
				'' as dolly3Wip,
				'' as dolly4Wip,
				'' as dolly5Wip,
				'' as dolly6Wip,
				'' as dolly7Wip,
				'' as dolly8Wip,
				'' as dolly9Wip,
				'' as lineWip,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) AS planStartWip,
				CAST(PLAN_CT AS varchar) as cycleTimeWip,
				'' as WIP_COUNT,
				'' as WIP_FLAG
			FROM 
				[TB_R_POS]

			WHERE 
				POS_TYPE = @POS_TYPE
				AND ((@SHIFT IS NULL OR @SHIFT = '') OR SHIFT = @SHIFT)
				AND CAST(PROD_DT AS DATE) = @PROD_DATE

			ORDER BY 
				PLAN_START ASC, PROD_DT DESC;
			END
			

			

			--WIP
			ELSE IF @SHIFT = '1' AND  @WIP_FLAG = 1
			BEGIN
				SET @SHIFT = '2'
				SELECT TOP (@WIP_COUNT)
				REPLACE(CONVERT(NVARCHAR, PROD_DT, 106), ' ', '-') AS ProdDate,
				CASE 
					WHEN DATENAME(dw, PROD_DT) = 'Monday' THEN 'Senin' 
					WHEN DATENAME(dw, PROD_DT) = 'Tuesday' THEN 'Selasa' 
					WHEN DATENAME(dw, PROD_DT) = 'Wednesday' THEN 'Rabu' 
					WHEN DATENAME(dw, PROD_DT) = 'Thursday' THEN 'Kamis' 
					WHEN DATENAME(dw, PROD_DT) = 'Friday' THEN 'Jumat' 
					WHEN DATENAME(dw, PROD_DT) = 'Saturday' THEN 'Sabtu' 
					WHEN DATENAME(dw, PROD_DT) = 'Sunday' THEN 'Minggu' 
					ELSE DATENAME(dw, PROD_DT) -- For any unexpected values, return the original English name
				END AS ProdDayName,
				''  as no,
				'' as shutterNo,
				'' as dest,
				'' as lotModuleNo,
				'' AS caseNo,
				'' as shift,
				'' as renban,
				'' as modType,
				'' AS moduleCode,
				'' as dolly1,
				'' as dolly2,
				'' as dolly3,
				'' as dolly4,
				'' as dolly5,
				'' as dolly6,
				'' as dolly7,
				'' as dolly8,
				'' as dolly9,
				'' as line,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) AS planStart,
				CAST(PLAN_CT AS varchar) as cycleTime,
				SHOOTER_NO as shutterNoWip,
				MODULE_DEST_CD as destWip,
				LOT_MODULE_NO as lotModuleNoWip,
				CASE WHEN CASE_NO = '***' THEN NULL ELSE CASE_NO END AS caseNoWip,
				CAST(SHIFT AS varchar) as shiftWip,
				CONTAINER_SNO as renbanWip,
				MOD_TYPE as modTypeWip,
				CONCAT(MODULE_DEST_CD,LEFT(LOT_MODULE_NO,2),CASE_NO)AS moduleCodeWip,
				DOLLY_1 as dolly1Wip,
				DOLLY_2 as dolly2Wip,
				DOLLY_3 as dolly3Wip,
				DOLLY_4 as dolly4Wip,
				DOLLY_5 as dolly5Wip,
				DOLLY_6 as dolly6Wip,
				DOLLY_7 as dolly7Wip,
				DOLLY_8 as dolly8Wip,
				DOLLY_9 as dolly9Wip,
				ZONE_CD as lineWip,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) AS planStartWip,
				CAST(PLAN_CT AS varchar) as cycleTimeWip
			FROM 
				[TB_R_POS]

			WHERE 
				POS_TYPE = @POS_TYPE
				AND ((@SHIFT IS NULL OR @SHIFT = '') OR SHIFT = @SHIFT)
				AND CAST(PROD_DT AS DATE) = @PROD_DATE

			ORDER BY 
				PLAN_START ASC;
			END

		ELSE IF @SHIFT = '2' AND  @WIP_FLAG = 1
			BEGIN
				SET @SHIFT = '1'
				SELECT TOP (@WIP_COUNT)
				REPLACE(CONVERT(NVARCHAR, PROD_DT, 106), ' ', '-') AS ProdDate,
				CASE 
					WHEN DATENAME(dw, PROD_DT) = 'Monday' THEN 'Senin' 
					WHEN DATENAME(dw, PROD_DT) = 'Tuesday' THEN 'Selasa' 
					WHEN DATENAME(dw, PROD_DT) = 'Wednesday' THEN 'Rabu' 
					WHEN DATENAME(dw, PROD_DT) = 'Thursday' THEN 'Kamis' 
					WHEN DATENAME(dw, PROD_DT) = 'Friday' THEN 'Jumat' 
					WHEN DATENAME(dw, PROD_DT) = 'Saturday' THEN 'Sabtu' 
					WHEN DATENAME(dw, PROD_DT) = 'Sunday' THEN 'Minggu' 
					ELSE DATENAME(dw, PROD_DT) -- For any unexpected values, return the original English name
				END AS ProdDayName,
				''  as no,
				'' as shutterNo,
				'' as dest,
				'' as lotModuleNo,
				'' AS caseNo,
				'' as shift,
				'' as renban,
				'' as modType,
				'' AS moduleCode,
				'' as dolly1,
				'' as dolly2,
				'' as dolly3,
				'' as dolly4,
				'' as dolly5,
				'' as dolly6,
				'' as dolly7,
				'' as dolly8,
				'' as dolly9,
				'' as line,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) AS planStart,
				CAST(PLAN_CT AS varchar) as cycleTime,
				SHOOTER_NO as shutterNoWip,
				MODULE_DEST_CD as destWip,
				LOT_MODULE_NO as lotModuleNoWip,
				CASE WHEN CASE_NO = '***' THEN NULL ELSE CASE_NO END AS caseNoWip,
				CAST(SHIFT AS varchar) as shiftWip,
				CONTAINER_SNO as renbanWip,
				MOD_TYPE as modTypeWip,
				CONCAT(MODULE_DEST_CD,LEFT(LOT_MODULE_NO,2),CASE_NO)AS moduleCodeWip,
				DOLLY_1 as dolly1Wip,
				DOLLY_2 as dolly2Wip,
				DOLLY_3 as dolly3Wip,
				DOLLY_4 as dolly4Wip,
				DOLLY_5 as dolly5Wip,
				DOLLY_6 as dolly6Wip,
				DOLLY_7 as dolly7Wip,
				DOLLY_8 as dolly8Wip,
				DOLLY_9 as dolly9Wip,
				ZONE_CD as lineWip,
				CAST(FORMAT(PLAN_START, 'HH:mm') AS varchar) AS planStartWip,
				CAST(PLAN_CT AS varchar) as cycleTimeWip
			FROM 
				[TB_R_POS]

			WHERE 
				POS_TYPE = @POS_TYPE
				AND ((@SHIFT IS NULL OR @SHIFT = '') OR SHIFT = @SHIFT)
				AND CAST(PROD_DT AS DATE) = @NextDay

			ORDER BY 
				PLAN_START ASC;
			END
    END TRY
    BEGIN CATCH
        SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
        SET @@MSG_TYPE = 'E'

        SELECT @@MSG_TYPE + '|' + @@MSG_TXT
    END CATCH
END
;
