﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
//using System.Data;
//using IPOS.Controllers;
//using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
//using Toyota.Common.Web.Platform;
//using Toyota.Common.Database;
using IPOS.Models.CED050700W;
using Microsoft.Reporting.WebForms;
using System.Drawing.Imaging;
using Zen.Barcode;

namespace IPOS.Controllers
{
    public class CED050700WController : CommonController
    {
        string modulID = "05";
        string functionID = "CED050700W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED050700W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.DateFrom = getDateFrom();
            viewModel.GridPaging.FunctionID = "CED050700W";

            Model = viewModel;
        }

        public string getDateFrom()
        {
            string result = "";
            IList<CED050700WDateFrom> getData = CED050700WRepository.Instance.getDateFrom();
            foreach(var item in getData)
            {
                result = item.From.ToString();
            }
            return result;
        }

        [HttpPost]
        public ActionResult CED050700WSearchNoData(CED050700WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED050700W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = "CED050700W";

                return PartialView("CED050700WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public ActionResult CED050700WSearchData(CED050700WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED050700W>();
                viewModel.DataList = CED050700WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED050700WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = "CED050700W";

                return PartialView("CED050700WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED050700WSearchDataDetail(CED050700WSearchModelDtl iParam)
        {
            try
            {
                var viewModel = new GenericModel<CED050700WDetail>();
                viewModel.DataList = CED050700WRepository.Instance.SearchDataDetail(iParam);
                viewModel.GridPaging = new Paging(CED050700WRepository.Instance.SearchCountDataDetail(iParam), iParam.CurrentPage, iParam.DataPerPage);
                viewModel.GridPaging.FunctionID = "DetailCED050700W";
                ViewBag.CountData = viewModel.DataList.Count().ToString();

                //return PartialView("CED050700WGrid", viewModel);
                return PartialView("CED050700WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public string GenerateBarcode(string keyid)
        {
            var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
            var image = draw.Draw(keyid, 70, 1);
            byte[] arr;
            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                arr = memStream.ToArray();
            }
            var pathFile = Server.MapPath("~/Report/Download/barcode_" + keyid + ".png");
            System.IO.File.WriteAllBytes(pathFile, arr);
            System.Drawing.Image i = System.Drawing.Image.FromFile(pathFile);
            i.RotateFlip(rotateFlipType: System.Drawing.RotateFlipType.Rotate90FlipXY);
            i.Save(pathFile);
            i.Dispose();

            return pathFile;
        }

        [HttpPost]
        public string GenerateModul(CED050700WSearchModelDtl SearchModel)
        {
            List<CED050700WPrint> listData = CED050700WRepository.Instance.SearchDataPrint(SearchModel).ToList();
            LocalReport localReport = new LocalReport();
            //localReport.ReportPath = Server.MapPath("~/Report/Report1.rdlc");
            localReport.ReportPath = Server.MapPath("~/Report/CED050800Rpt.rdlc");
            localReport.EnableExternalImages = true;
            for(var i = 0; i < listData.Count(); i++)
            {
                string pathBarcode = GenerateBarcode(listData[i].KanbanID);
                listData[i].ImgPath = pathBarcode;
            }
            //var inumber = 0;
            //foreach (var item in listData)
            //{
            //    string pathBarcode = GenerateBarcode(item.KanbanID);
            //    listData[inumber].ImgPath = pathBarcode;
            //    inumber = inumber + 1;
            //}
            //string filename = "CED010507Rpt" + listData[0].KanbanID + ".png";
            //if (!System.IO.File.Exists(Server.MapPath("~/Report/Download/" + filename)))
            //{
            //    try
            //    {

            ////Set Param

            ////ReportParameter[] rptParam = new ReportParameter[]
            ////{
            ////    new ReportParameter("ImgPath", "123")
            ////};
            ////localReport.SetParameters(rptParam);
            ////localReport.SetParameters(new ReportParameter("ImgBarcode", pathBarcode));
            localReport.Refresh();
                    ReportDataSource reportDataSource = new ReportDataSource("dsKanban", listData);

                    localReport.DataSources.Add(reportDataSource);

                    //Setting Export to PDF
                    string reportType = "PDF";
                    string mimeType;
                    string encoding;
                    string fileNameExtension;

                    //The DeviceInfo settings should be changed based on the reportType
                    //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
                    string deviceInfo =
                    "<DeviceInfo>" +
                    "  <OutputFormat>PDF</OutputFormat>" +
                    "  <PageWidth>8.66in</PageWidth>" +
                    "  <PageHeight>4.33in</PageHeight>" +
                    "  <MarginTop>0in</MarginTop>" +
                    "  <MarginLeft>0in</MarginLeft>" +
                    "  <MarginRight>0in</MarginRight>" +
                    "  <MarginBottom>0in</MarginBottom>" +
                    "</DeviceInfo>";

                    ////Setting Export to Image
                    //string reportType = "Image";
                    //string mimeType = "image/png";
                    //string encoding;
                    //string fileNameExtension;

                    //string deviceInfo =
                    //"<DeviceInfo>" +
                    //"  <OutputFormat>PNG</OutputFormat>" +
                    //"  <PageWidth>11.69in</PageWidth>" +
                    //"  <PageHeight>8.27in</PageHeight>" +
                    //"  <MarginTop>0.2in</MarginTop>" +
                    //"  <MarginLeft>0.2in</MarginLeft>" +
                    //"  <MarginRight>0.2in</MarginRight>" +
                    //"  <MarginBottom>0.2in</MarginBottom>" +
                    //"</DeviceInfo>";

                    Warning[] warnings;
                    string[] streams;
                    byte[] renderedBytes;

                    //Render the report
                    renderedBytes = localReport.Render(
                        reportType,
                        deviceInfo,
                        out mimeType,
                        out encoding,
                        out fileNameExtension,
                        out streams,
                        out warnings);

                    System.IO.File.WriteAllBytes(Server.MapPath("~/Report/Download/CED050700Rpt.pdf"), renderedBytes);
                    //System.IO.File.WriteAllBytes(Server.MapPath("~/Report/Download/" + filename), renderedBytes);

                    //Show Direct PDF / Image
                    //return File(renderedBytes, mimeType);
                //}
                //catch (Exception e)
                //{
                //    return e.Message;
                //}
            //}

            return "";
        }

        [HttpPost]
        public JsonResult SendParamBatchDownload(CED050700WSearchModelDtl searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED050800R";
                string functscreen = "CED050700W";
                string userlocation = "TMMIN";
                string parameter =
                                processid + ";" +
                                (searchModel.KANBAN_HEADER_CD == null ? "" : searchModel.KANBAN_HEADER_CD) + ";" +
                                //(searchModel.LotModNoDtl == null ? "" : searchModel.LotModNoDtl) + ";" +
                                //(searchModel.CaseNoDtl == null ? "" : searchModel.CaseNoDtl) + ";" +
                                (searchModel.KanbanIDDtl == null ? "" : searchModel.KanbanIDDtl) + ";" +
                                (searchModel.Printer == null ? "" : searchModel.Printer) + ";" +
                                //(searchModel.DATE_FROM == null ? "" : searchModel.DATE_FROM) + ";" +
                                //(searchModel.DATE_TO == null ? "" : searchModel.DATE_TO) + ";" +
                                //(searchModel.GenerateMode == null ? "" : searchModel.GenerateMode) + ";" +
                                username + ";" +
                                searchModel.PrintMode + ";" +
                                searchModel.REPRINT_REASON;
                //2019-07-15 : Remark by FID.Ridwan as Request FID.Dina  
                //string update = CED050700WRepository.Instance.UpdatePackingDetail(searchModel.REPRINT_REASON, searchModel.KanbanIDDtl, username);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

    }
}