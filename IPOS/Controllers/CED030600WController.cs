﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED030600W;

namespace IPOS.Controllers
{
    public class CED030600WController : CommonController
    {
        string modulID = "03";
        string functionID = "CED030600W";

        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {

            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED030600W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED030600WSearchNoData(CED030600WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED030600W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED030600WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public ActionResult CED030600WSearchData(CED030600WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED030600W>();
                viewModel.DataList = CED030600WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED030600WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED030600WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED030600WEDSearch(CED030600WSearchModel SearchModel)
        {
            try
            {
                functionID = "CED030600WED";
                var viewModel = new GenericModel<CED030600W>();
                viewModel.DataList = CED030600WRepository.Instance.SearchEDData(SearchModel);
                viewModel.GridPaging = new Paging(CED030600WRepository.Instance.SearchCountEDData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED030600WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED030600WSearchDataDetail(CED030600WSearchModel SearchModel)
        {
            try
            {
                functionID = "DetailCED030600W";
                if (SearchModel.tablename == "0")
                    functionID = "DetailCED030600W";
                var viewModel = new GenericModel<CED030600WDetail>();
                viewModel.DataList = CED030600WRepository.Instance.SearchDataDetail(SearchModel);
                //viewModel.GridPaging = new Paging(CED030600WRepository.Instance.SearchCountDataDetail(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging = new Paging(15, SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;
                
                return PartialView("CED030600WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED030600WSaveRowEditData(string tablename, string deliveryno, string dockcd, string totalskid, string problem, string catchupplan, string changed_date)
        {
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            totalskid = totalskid == "" ? null : totalskid;
            problem = problem == "" ? null : problem;
            catchupplan = catchupplan == "" ? null : catchupplan;

            string result = CED030600WRepository.Instance.SaveData(tablename, deliveryno, dockcd, totalskid, problem, catchupplan, currentuser, changed_date);
            string[] devaultval =  "Error|No data Available".Split('|');
            if (!result.Contains('E') && (!result.Contains('|')))
            {
                var e = (result != null) ? result.Split('|') : devaultval;
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = result });
            }
            else
            {
                var e = (result != null) ? result.Split('|') : devaultval;
                return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
            }

        }

        [HttpPost]
        public JsonResult SendParamBatch()
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string EOflag = "1";
                string functbatch = "CED030100B";
                string functscreen = "CED030600W";
                string userlocation = "TMMIN";
                string parameter = EOflag;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED030600WDownload(CED030600WSearchModel searchfilter)
        {
            //User user = Lookup.Get<User>();
            //string username = user.Username;
            //string processid = CommonRepository.Instance.GetProcessId();
            //string functbatch = "CED030400L";
            //string functscreen = "CED030600W";
            //string userlocation = "TMMIN";
            //string parameter = searchfilter.PickUpDate + ";" + searchfilter.L_P + ";" + searchfilter.Delivery_No + ";" + searchfilter.TruckStation + ";" + searchfilter.Route + ";" + searchfilter.Rate + ";" + searchfilter.Status + ";" + searchfilter.Shift;
            //string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

            //return Json(new { result = returnVal });

            User user = Lookup.Get<User>();
            string processid = CommonRepository.Instance.GetProcessId();
            string currentuser = (user != null) ? user.Username : "System";
            string result = CED030600WRepository.Instance.DownloadData(searchfilter, currentuser, processid); //SearchModel
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }


    }
}