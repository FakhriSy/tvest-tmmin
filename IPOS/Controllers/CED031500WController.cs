﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED031500W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED031500WController : PageController
    {
        string modulID = "03";
        string functionID = "CED031500W";
        public User user { get { return Lookup.Get<User>(); } }
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED031500W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED031500WSearchNoData(CED031500WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031500W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031500WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }


        [HttpPost]
        public JsonResult CED031500WReceiveData(string param)
        {
            try
            {
                IList<CED031500WReceive> receivedata = new List<CED031500WReceive>();
                string username = user.Username;
                receivedata = CED031500WRepository.Instance.SearchDataRecive(param, username);
                if (receivedata.Count == 0){
                    return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "NOT FOUND" });
                }else if (receivedata[0].CHECK_VALIDATE != "ERROR") {
                    if (receivedata[0].CHECK_VALIDATE != "NOT FOUND")
                    {
                        if (receivedata[0].CHECK_VALIDATE != "ALREADY SCAN")
                        {
                            return Json(new { success = true, data = receivedata }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "ALREADY SCAN" });
                        }
                    }
                    else
                    {
                        return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "NOT FOUND" });
                    }
                }
                else
                {
                    return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "ERROR" });
                }
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
        [HttpPost]
        public ActionResult CED031500WSearchDataInitial(CED031500WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031500W>();
                viewModel.DataList = CED031500WRepository.Instance.SearchDataInitial(SearchModel);
                viewModel.GridPaging = new Paging(CED031500WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031500WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031500WSearchData(CED031500WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031500W>();
                viewModel.DataList = CED031500WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED031500WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031500WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031500WSearchDataDetail(CED031500WSearchModel SearchModel)
        {
            try
            {
                functionID = "DetailCED031500W";
                var viewModel = new GenericModel<CED031500WDetail>();
                viewModel.DataList = CED031500WRepository.Instance.SearchDataDetail(SearchModel);
                //viewModel.GridPaging = new Paging(CED030600WRepository.Instance.SearchCountDataDetail(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging = new Paging(15, SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;
                
                return PartialView("CED031500WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult CED031500WSaveData(List<CED031500WSearchModel> list)
        {
            string result = "";
            var e = result.Split('|');
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            if (list != null)
            {
                foreach (var dataForm in list)
                {
                    CED031500WSearchModel data = new CED031500WSearchModel();
                    data.MANIFEST_NO = dataForm.MANIFEST_NO;
                    data.PART_BARCODE = dataForm.PART_BARCODE;
                    data.FULL_STATUS = dataForm.FULL_STATUS;
                    data.PCS_STATUS = dataForm.PCS_STATUS;
                    result = CED031500WRepository.Instance.SaveData(data, currentuser);
                    e = result.Split('|');
                }
            }
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult CED031500WSaveDataFlag(string ManifestNo, string PartNo, string CheckType)
        {
            string result = CED031500WRepository.Instance.SaveDataFlag(ManifestNo, PartNo, CheckType);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult CED031500WSaveDataTempHeader(List<CED031500WSearchModel> list)
        {
            string result = "";
            var e = result.Split('|');
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            if (list != null)
            {
                foreach (var dataForm in list)
                {
                    CED031500WSearchModel data = new CED031500WSearchModel();
                    data.MANIFEST_NO = dataForm.MANIFEST_NO;
                    data.PART_NO = dataForm.PART_NO;
                    data.REC_STATUS = dataForm.REC_STATUS;
                    result = CED031500WRepository.Instance.SaveDataTempHeader(data, currentuser);
                    e = result.Split('|');
                }
            }
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult CED031500WSaveDataReceive(string ManifestNo)
        {
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            string result = CED031500WRepository.Instance.SaveDataReceive(ManifestNo, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }
    }
}