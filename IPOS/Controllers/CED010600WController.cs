﻿using System.Web.Mvc;
using System;
using IPOS.Models.CED010600W;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using NPOI.SS.Formula.Functions;
using System.IO;
using System.Web;

namespace IPOS.Controllers
{
    public class CED010600WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010600W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010600W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult CED010600WSearchNoData()
        {
            try
            {
                var viewModel = new GenericModel<CED010600W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010600WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010600WSearchData(CED010600WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010600W>();
                viewModel.DataList = CED010600WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging = new Paging(CED010600WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010600WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010600WSaveData(CED010600W m, int mode, int table)
        {
            string result = CED010600WRepository.Instance.SaveData(m, mode, table, user.Username);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010600WDeleteData(CED010600W m, int table)
        {
            string result = CED010600WRepository.Instance.DeleteData(m, table);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public JsonResult SendParamBatchUpload(string filename, string table)
        {
            try
            {
                string functbatch = "CED010602B";
                string functscreen = "CED010600W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                string parameter = filename + ";" + table;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}