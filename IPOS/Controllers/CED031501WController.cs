﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Data;
using System.Data.OleDb;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED031501W;
using IPOS.Models.Common;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;

namespace IPOS.Controllers
{
    public class CED031501WController : CommonController
    {
        string modulID = "03";
        string functionID = "CED031501W";
        public User user { get { return Lookup.Get<User>(); } }
        public static string currentUser = "";

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED031501W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;

            SetCurrentUser(user.Username);
        }

        public static void SetCurrentUser(string username)
        {
            currentUser = username;
        }

        public static SelectList UserDockMapping(String typeComboBox)
        {
            NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

            IEnumerable<comboBox> dataList = CED031501WRepository.Instance.UserDockMapping(currentUser);
            IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

            return new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty);
        }

        public ActionResult CED031501WSearchNoData(CED031501WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031501W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031501WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED031501WSearchData(CED031501WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031501W>();
                viewModel.DataList = CED031501WRepository.Instance.SearchData(searchModel, user.Username);
                viewModel.GridPaging = new Paging(CED031501WRepository.Instance.SearchCountData(searchModel, user.Username), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031501WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED031501WSaveData(CED031501W m, int mode)
        {
            string result = CED031501WRepository.Instance.SaveData(m, mode, user.Username);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED031501WReceive(CED031501W m)
        {
            var MSG_TYPE = "";
            var MSG_TXT = "";
            m.Changed_By = user.Username;

            string result = CED031501WRepository.Instance.ReceiveDataValidate(m); // insert ke TB_S_DAILY_ORDER_MANIFEST_IPPCS
            var e = result.Split('|');
            MSG_TYPE = e[0];
            MSG_TXT = e[1];

            if (MSG_TYPE == "I")
            {
                // prepare parameter
                m.PROCESS_ID = e[2];
                m.ArrivalTime = e[3];

                //var syncReceivingResult = hitAPI(m); // hit api
                CED031501WAPI syncReceivingResult = new CED031501WAPI(); //bypass
                syncReceivingResult.statusCode = 200; //bypass

                // the result in syncReceivingResult.statusCode
                // apply or clear only
                string result2 = CED031501WRepository.Instance.ReceiveDataApply(m.PROCESS_ID, syncReceivingResult.statusCode.ToString());

                if (syncReceivingResult.statusCode == 200)
                {
                    var e2 = result2.Split('|');
                    MSG_TYPE = e2[0];
                    MSG_TXT = e2[1];

                    if (e[1].ToLower().Contains("not processed"))   
                    {
                        MSG_TXT = e[1];
                    }

                    //Call Exe Interface
                    if (MSG_TXT == "Manifest Successfully Received - Finished Case")
                    {
                        string pathFolder = CommonController.GetSystemMasterValue("FOLDER", "PATH", "INTERFACE");
                        System.Diagnostics.Process process1 = new System.Diagnostics.Process();
                        process1.StartInfo.FileName = pathFolder + "Interface.exe";
                        process1.StartInfo.Arguments = "010208 CREATE";
                        process1.Start();
                        process1.Close();
                    }
                }
                else
                {
                    MSG_TYPE = "E";
                    MSG_TXT = syncReceivingResult.message;
                }
            }
            return Json(new resultMessage { MSG_TYPE = MSG_TYPE, MSG_TXT = MSG_TXT, PROCESS_ID = m.PROCESS_ID });
        }

        private static CED031501WAPI hitAPI(CED031501W _params)
        {
            var result = new CED031501WAPI();

            //var apitoken = hitAPIAuth();
            //if (apitoken.result == false)
            //{
            //    result.statusCode = 401;
            //    result.result = apitoken.result;
            //    result.message = "[Sync] " + apitoken.message;
            //    return result;
            //}

            string baseUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_BASE_URL").Trim();
            string endPointUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_RECEIVING_SYNC").Trim();
            JObject paramsBody = new JObject();
            // prepare params
            paramsBody["MANIFEST_NO"] = _params.ManifestNo;
            paramsBody["DOCK_CD"] = _params.DockCode;
            paramsBody["ARRIVAL_ACTUAL_DT"] = _params.ArrivalTime;
            paramsBody["CHANGED_BY"] = _params.Changed_By;
            paramsBody["PROBLEM_FLAG"] = "0";

            try
            {
                //string jsonBody = JsonSerializer.SerializeToString(data);
                string jsonBody = paramsBody.ToString(Formatting.None);
                string url = baseUrl + endPointUrl;
                // Create the request body.
                HttpContent content = new StringContent(jsonBody, System.Text.Encoding.UTF8, "application/json");
                // Send request.
                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromSeconds(30);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apitoken.message);
                var response = client.PostAsync(url, content);
                var responseMessage = response.Result.Content.ReadAsStringAsync().Result;
                try
                {
                    var responseMessageArray = JArray.Parse(responseMessage);
                    result = JObject.Parse(responseMessageArray[0].ToString()).ToObject<CED031501WAPI>();
                    result.statusCode = (int)response.Result.StatusCode;
                }
                catch (Exception ex2)
                {
                    try
                    {
                        result = JObject.Parse(responseMessage.ToString()).ToObject<CED031501WAPI>();
                        result.statusCode = (int)response.Result.StatusCode;
                    }
                    catch (Exception ex3)
                    {
                        var responseMessageArray = JArray.Parse(responseMessage);
                        result.message = ex3.Message.ToString();
                        result.statusCode = 400;

                    }

                }
                return result;
            }
            catch (Exception ex)
            {
                result.statusCode = 400;
                result.result = false;
                result.message = ex.InnerException is null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                result.message = (result.message == "A task was canceled." ? "Request time out" : result.message);
                result.message = "[Sync] Error : " + result.message;
            }

            return result;
        }

        private static CED031501WAPI hitAPIAuth()
        {
            string baseUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_BASE_URL").Trim();
            //string baseUrl = "http://localhost:59999";
            string endPointUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_AUTH").Trim();
            string userName = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_USER").Trim();
            string password = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_PASS").Trim();

            var result = new CED031501WAPI();
            result.result = true;
            try
            {
                string url = baseUrl + endPointUrl + "?userName=" + userName + "&password=" + password;
                // Send request.
                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromSeconds(30);
                var response = client.GetAsync(url);
                var responseMessage = response.Result.Content.ReadAsStringAsync().Result;
                var checkResult = JObject.Parse(responseMessage);
                result.statusCode = (int)response.Result.StatusCode;

                try
                {
                    var token = (string)checkResult["token"]["access_token"];
                    var token_type = (string)checkResult["token"]["token_type"];
                    var expires_in = (int)checkResult["token"]["expires_in"];

                    result.message = token;

                    if (result.statusCode != (int)HttpStatusCode.OK || string.IsNullOrEmpty(result.message))
                    {
                        result.result = false;
                    }
                }
                catch (Exception ex2)
                {
                    result.result = false;
                    result.message = "error when getting token";
                }
                return result;

            }
            catch (Exception ex)
            {
                result.statusCode = 400;
                result.result = false;
                result.message = ex.InnerException.Message.ToString();
                result.message = (result.message == "A task was canceled." ? "Request time out" : result.message);
            }

            return result;
        }

        [HttpPost]
        public JsonResult SendParamBatchDownload(CED031501WSearchModel searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED031400R";
                string functscreen = "CED031501W";
                string userlocation = "TAM";
                string parameter = (searchModel.DeliveryDateFrom == null ? "" : searchModel.DeliveryDateFrom) + ";" +
                                    (searchModel.DeliveryDateTo == null ? "" : searchModel.DeliveryDateTo) + ";" +
                                    (searchModel.Shift == null ? "" : searchModel.Shift) + ";" +
                                    (searchModel.PhysicalDock == null ? "" : searchModel.PhysicalDock) + ";" +
                                    (searchModel.RouteName == null ? "" : searchModel.RouteName) + ";" +
                                    (searchModel.RouteCycle == null ? "" : searchModel.RouteCycle) + ";" +
                                    (searchModel.LogicalDock == null ? "" : searchModel.LogicalDock) + ";" +
                                    (searchModel.TS == null ? "" : searchModel.TS) + ";" +
                                    (searchModel.Supplier == null ? "" : searchModel.Supplier) + ";" +
                                    (searchModel.ManifestNo == null ? "" : searchModel.ManifestNo) + ";" +
                                    (searchModel.OrderNo == null ? "" : searchModel.OrderNo) + ";" +
                                    (searchModel.Status == null ? "" : searchModel.Status) + ";" +
                                    (searchModel.KanbanId == null ? "" : searchModel.KanbanId) + ";" +
                                    (searchModel.PartNo == null ? "" : searchModel.PartNo) + ";" +
                                    (searchModel.Dest == null ? "" : searchModel.Dest) + ";" +
                                    (searchModel.Lot == null ? "" : searchModel.Lot) + ";" +
                                    (searchModel.Case == null ? "" : searchModel.Case) + ";" +
                                    (searchModel.Dock == null ? "" : searchModel.Dock) + ";" +
                                    (searchModel.Type == null ? "" : searchModel.Type);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchUpload(string filename)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED031100B";
                string functscreen = "CED031501W";
                string userlocation = "TMMIN";
                string parameter = (filename == null ? "" : filename);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}