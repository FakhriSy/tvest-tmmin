﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using Microsoft.Reporting.WebForms;
using IPOS.Models.Common;
using Zen.Barcode;
using System.Drawing.Imaging;
using System.Net;
using System.Text;
using System.Threading.Tasks;
//using System.Net.Http;
//using System.Net.Http.Headers;
using System.Web.Script.Serialization;

namespace IPOS.Controllers
{
    public class TestReportController : CommonController
    {
        SQLConnect dbConn = new SQLConnect();
        protected override void Startup()
        {
            
        }

        public ActionResult PrintReport(string printerName)
        {
            try
            {
                DataSet ds = dbConn.GetDataSet("CED021800B_GetDataCaseMark '20190319001'");
                LocalReport report = new LocalReport();
                report.ReportPath = Server.MapPath("~/Report/CED010218Rpt.rdlc");
                report.Refresh();
                report.DataSources.Add(
                   new ReportDataSource("dsData", ds.Tables[0]));
                PrintReport PR = new Models.Common.PrintReport();
                PR.Export(report, "");
                PR.Print("");

                return Json("Print Success");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }

        public ActionResult ShowRDLCKanban4() {
            DataSet ds = dbConn.GetDataSet("SP_CED050800R_GetBarcodeHeader '1', '1', '201907001340A', '1', '1', '1'");
            string filename = "barcode_" + ds.Tables[0].Rows[0]["KanbanHeader"].ToString() + ".png";
            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath("~/Report/CED050800Rpt_H.rdlc");
            localReport.EnableExternalImages = true;
            string pathBarcode = GenerateBarcode4(ds.Tables[0].Rows[0]["KanbanHeader"].ToString(), "H"); //@"D:/MyData/kerjaan/IPOMS-Data/SOURCE_PROD/IPOMS/IPOS/Report/Download/" + 
            //localReport.SetParameters(new ReportParameter("ImgPath", pathBarcode));
            localReport.Refresh();

            ds.Tables[0].Rows[0][9] = pathBarcode;
            ds.Tables[0].AcceptChanges();

            ReportDataSource reportDataSource = new ReportDataSource("dsKanban", ds.Tables[0]);

            localReport.DataSources.Add(reportDataSource);

            //Setting Export to PDF
            string reportTypeHPDF = "PDF";
            string mimeTypeHPDF;
            //Setting Export to Image
            string reportTypeH = "Image";
            string mimeTypeH = "image/png";
            string encodingH;
            string fileNameExtensionH;

            string deviceInfoH_PDF =
          "<DeviceInfo>" +
          "  <OutputFormat>PDF</OutputFormat>" +
          "  <PageWidth>21cm</PageWidth>" +
          "  <PageHeight>29.7cm</PageHeight>" +
          "  <MarginTop>0</MarginTop>" +
          "  <MarginLeft>0</MarginLeft>" +
          "  <MarginRight>0</MarginRight>" +
          "  <MarginBottom>0</MarginBottom>" +
          "</DeviceInfo>";

            string deviceInfoH =
            "<DeviceInfo>" +
            "  <OutputFormat>EMF</OutputFormat>" +
            "  <PageWidth>21cm</PageWidth>" +
            "  <PageHeight>29.7cm</PageHeight>" +
            "  <MarginTop>0</MarginTop>" +
            "  <MarginLeft>0</MarginLeft>" +
            "  <MarginRight>0</MarginRight>" +
            "  <MarginBottom>0</MarginBottom>" +
            "</DeviceInfo>";

            Warning[] warningsH;
            string[] streamsH;
            byte[] renderedBytesH;
            byte[] renderedBytesHPDF;

            //Render the report
            renderedBytesH = localReport.Render(
                reportTypeH,
                deviceInfoH,
                out mimeTypeH,
                out encodingH,
                out fileNameExtensionH,
                out streamsH,
                out warningsH);

            renderedBytesHPDF = localReport.Render(
                reportTypeHPDF,
                deviceInfoH_PDF,
                out mimeTypeHPDF,
                out encodingH,
                out fileNameExtensionH,
                out streamsH,
                out warningsH);

            //Show Direct PDF / Image
            return File(renderedBytesHPDF, mimeTypeHPDF);
        }

        public string GenerateBarcode4(string keyid, string code)
        {
            var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
            var image = draw.Draw(keyid, 70, 1);
            byte[] arr;
            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                arr = memStream.ToArray();
            }
            var pathFile = Server.MapPath("~/Report/Download/barcode_" + keyid + ".png");
            System.IO.File.WriteAllBytes(pathFile, arr);
            System.Drawing.Image i = System.Drawing.Image.FromFile(pathFile);
            if (code == "H")
            {
                i.RotateFlip(rotateFlipType: System.Drawing.RotateFlipType.RotateNoneFlipX);
            }
            else
            {
                i.RotateFlip(rotateFlipType: System.Drawing.RotateFlipType.Rotate90FlipXY);
            }
            i.Save(pathFile);
            i.Dispose();

            //searchDTO.code = code;
            //IList<CED050800RSearchDTO> ResultInsert = CED050800RDAO.insertBarcode(searchDTO);
            //if (ResultInsert[0].IsResult == "E")
            //{
            //    //CSTDDBLogUtil.CreateLogDetail(CSTDDBUtil.GetNonPooledConnection, processId, "MCED050800001E", new String[] { "Send generated files into printer queueing" }, "Generated file not found on specified directory");
            //    return ResultInsert[0].IsResult;
            //}

            return pathFile;
        }

        public ActionResult ShowRDLCContentList(){
            DataSet ds = dbConn.GetDataSet("sp_GetDataContentListHeader");
            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath("~/Report/CED010219Rpt.rdlc");
            localReport.SubreportProcessing += new SubreportProcessingEventHandler(SetSubDataSourceContentList);
            localReport.Refresh();
            ReportDataSource reportDataSource = new ReportDataSource("dsHeader", ds.Tables[0]);

            localReport.DataSources.Add(reportDataSource);

            //Setting Export to PDF
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>8.27in</PageWidth>" +
            "  <PageHeight>11.69in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>0.5in</MarginLeft>" +
            "  <MarginRight>0.5in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            ////Setting Export to Image
            //string reportType = "Image";
            //string mimeType = "image/png";
            //string encoding;
            //string fileNameExtension;

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PNG</OutputFormat>" +
            //"  <PageWidth>7.5</PageWidth>" +
            //"  <PageHeight>6.3</PageHeight>" +
            //"  <MarginTop>0in</MarginTop>" +
            //"  <MarginLeft>0in</MarginLeft>" +
            //"  <MarginRight>0in</MarginRight>" +
            //"  <MarginBottom>0in</MarginBottom>" +
            //"</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            //Show Direct PDF / Image
            return File(renderedBytes, mimeType);
        }

        public void SetSubDataSourceContentList(object sender, SubreportProcessingEventArgs e)
        {
            var param = e.Parameters["Param"].Values.First().ToString();
            DataSet dsDet = dbConn.GetDataSet("sp_GetDataContentListDetail '" + param + "'");
            e.DataSources.Add(new ReportDataSource("dsDetail", dsDet.Tables[0]));
        }

        public ActionResult ShowRDLCCaseMark()
        {
            DataSet ds = dbConn.GetDataSet("sp_GetDataCaseMark ''");
            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath("~/Report/CED010218Rpt.rdlc");
            localReport.Refresh();
            ReportDataSource reportDataSource = new ReportDataSource("dsData", ds.Tables[0]);

            localReport.DataSources.Add(reportDataSource);

            //Setting Export to PDF
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>8.27in</PageWidth>" +
            "  <PageHeight>11.69in</PageHeight>" +
            "  <MarginTop>0.5in</MarginTop>" +
            "  <MarginLeft>0.5in</MarginLeft>" +
            "  <MarginRight>0.5in</MarginRight>" +
            "  <MarginBottom>0.5in</MarginBottom>" +
            "</DeviceInfo>";

            ////Setting Export to Image
            //string reportType = "Image";
            //string mimeType = "image/png";
            //string encoding;
            //string fileNameExtension;

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PNG</OutputFormat>" +
            //"  <PageWidth>7.5</PageWidth>" +
            //"  <PageHeight>6.3</PageHeight>" +
            //"  <MarginTop>0in</MarginTop>" +
            //"  <MarginLeft>0in</MarginLeft>" +
            //"  <MarginRight>0in</MarginRight>" +
            //"  <MarginBottom>0in</MarginBottom>" +
            //"</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            //Show Direct PDF / Image
            return File(renderedBytes, mimeType);
        }

        public string GenerateBarcode(string keyid)
        {
            var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
            var image = draw.Draw(keyid, 70, 1);
            byte[] arr;
            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                arr = memStream.ToArray();
            }
            var pathFile = Server.MapPath("~/Report/Download/barcode_" + keyid + ".png");
            System.IO.File.WriteAllBytes(pathFile, arr);
            System.Drawing.Image i = System.Drawing.Image.FromFile(pathFile);
            i.RotateFlip(rotateFlipType: System.Drawing.RotateFlipType.Rotate90FlipXY);
            i.Save(pathFile);
            i.Dispose();

            return pathFile;
        }
        public ActionResult ShowRDLCPrintKanban()
        {
            DataSet ds = dbConn.GetDataSet("sp_PrintKanban");
            string filename = "CED010507Rpt" + ds.Tables[0].Rows[0]["Barcode"].ToString() + ".png";
            LocalReport localReport = new LocalReport();
            localReport.ReportPath = Server.MapPath("~/Report/CED010507Rpt.rdlc");
            localReport.EnableExternalImages = true;
            string pathBarcode = GenerateBarcode(ds.Tables[0].Rows[0]["Barcode"].ToString());
            localReport.SetParameters(new ReportParameter("ImgBarcode", pathBarcode));
            localReport.Refresh();
            ReportDataSource reportDataSource = new ReportDataSource("dsKanban", ds.Tables[0]);

            localReport.DataSources.Add(reportDataSource);

            //Setting Export to PDF
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>11.69in</PageWidth>" +
            "  <PageHeight>8.27in</PageHeight>" +
            "  <MarginTop>0.2in</MarginTop>" +
            "  <MarginLeft>0.2in</MarginLeft>" +
            "  <MarginRight>0.2in</MarginRight>" +
            "  <MarginBottom>0.2in</MarginBottom>" +
            "</DeviceInfo>";

            ////Setting Export to Image
            //string reportType = "Image";
            //string mimeType = "image/png";
            //string encoding;
            //string fileNameExtension;

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PNG</OutputFormat>" +
            //"  <PageWidth>7.5</PageWidth>" +
            //"  <PageHeight>6.3</PageHeight>" +
            //"  <MarginTop>0in</MarginTop>" +
            //"  <MarginLeft>0in</MarginLeft>" +
            //"  <MarginRight>0in</MarginRight>" +
            //"  <MarginBottom>0in</MarginBottom>" +
            //"</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            //Render the report
            renderedBytes = localReport.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            //Show Direct PDF / Image
            return File(renderedBytes, mimeType);
        }

        public string uploadImagetoAPI()
        {
            string filename = Server.MapPath("~/Content/Images/Login/etios.jpg");
            String strFile = System.IO.Path.GetFileName(filename);
            byte[] data = System.IO.File.ReadAllBytes(filename);
            string imageBase64Data = Convert.ToBase64String(data);
            
            var msgResult = "";
            //using (var client = new HttpClient())
            //{
            //    ImageModel p = new ImageModel { fileName = strFile, imgFile = imageBase64Data };
            //    client.BaseAddress = new Uri("http://192.168.43.247:8012");
            //    var response = client.PostAsJsonAsync("uploadImageAPI", p).Result;
            //    if (response.IsSuccessStatusCode)
            //    {
            //        msgResult = response.ReasonPhrase;
            //    }
            //    else
            //        msgResult = response.ReasonPhrase;
            //}  

            return msgResult;
        }
    }

    public class ImageModel
    {
        public string fileName { get; set; }
        public string imgFile { get; set; }
    }
}