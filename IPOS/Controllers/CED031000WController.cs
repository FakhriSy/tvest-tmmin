﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED031000W;

namespace IPOS.Controllers
{
    public class CED031000WController : Controller //CommonController
    {
        string modulID = "03";
        string functionID = "CED031000W";
        string SysSubCat = "REFRESH_INTERVAL";
        //public User user { get { return Lookup.Get<User>(); } }

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            ViewData["RetriveData1"] = CED031000WRepository.Instance.RetrieveData(1);
            ViewData["RetriveData2"] = CED031000WRepository.Instance.RetrieveData(2);
            ViewData["RetriveData3"] = CED031000WRepository.Instance.RetrieveData(3);
            ViewData["RetriveData4"] = CED031000WRepository.Instance.RetrieveData(4);

            ViewData["AllData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "ALL_DATA"));
            ViewData["DelayData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "DELAY_INFORMATION"));
            ViewData["PlanData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "PLAN_ARRIVAL"));

            return View("CED031000W");
        }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED031000W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED031000W";
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    ViewData["RetriveData1"] = CED031000WRepository.Instance.RetrieveData(1);
        //    ViewData["RetriveData2"] = CED031000WRepository.Instance.RetrieveData(2);
        //    ViewData["RetriveData3"] = CED031000WRepository.Instance.RetrieveData(3);
        //    ViewData["RetriveData4"] = CED031000WRepository.Instance.RetrieveData(4);

        //    ViewData["AllData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "ALL_DATA"));
        //    ViewData["DelayData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "DELAY_INFORMATION"));
        //    ViewData["PlanData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "PLAN_ARRIVAL"));

        //    Model = viewModel;
        //}

        [HttpPost]
        public ActionResult CED031000WSearchData()
        {
            try
            {
                ViewData["RetriveData1"] = CED031000WRepository.Instance.RetrieveData(1);
                ViewData["RetriveData2"] = CED031000WRepository.Instance.RetrieveData(2);
                ViewData["RetriveData3"] = CED031000WRepository.Instance.RetrieveData(3);
                ViewData["RetriveData4"] = CED031000WRepository.Instance.RetrieveData(4);

                return PartialView("CED031000WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031000WSearchDataDelay()
        {
            try
            {
                ViewData["RetriveData2"] = CED031000WRepository.Instance.RetrieveData(2);

                return PartialView("CED031000WGridDelay", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031000WSearchDataPlan()
        {
            try
            {
                ViewData["RetriveData3"] = CED031000WRepository.Instance.RetrieveData(3);

                return PartialView("CED031000WGridPlanArrival", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

    }
}