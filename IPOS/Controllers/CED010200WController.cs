﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED010200W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED010200WController : PageController
    {
        string modulID = "01";
        string functionID = "CED010200W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010200W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CED010200WSearchNoData(CED010200WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010200W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010200WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }


        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CED010200WSearchData(CED010200WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010200W>();
                viewModel.DataList = CED010200WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010200WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010200WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010200WSaveData(CED010200W m, int mode)
        {
            //User user = Lookup.Get<User>();
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            string result = CED010200WRepository.Instance.SaveData(m, mode, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010200WDeleteData(CED010200W m)
        {
            //User user = Lookup.Get<User>();
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            string result = CED010200WRepository.Instance.DeleteData(m, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public ActionResult CED010200WSearchDataZone(CED010200WSearchModel SearchModel)
        {
            try
            {
                functionID = "ZoneCED010200W";
                var viewModel = new GenericModel<CED010200WZone>();
                viewModel.DataList = CED010200WRepository.Instance.SearchDataZone(SearchModel);
                viewModel.GridPaging = new Paging(CED010200WRepository.Instance.SearchCountDataZone(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;
                IList<CED010200WZone> getData = CED010200WRepository.Instance.SearchDataZone(SearchModel);
                ViewBag.CountZone = getData.Count.ToString();

                return PartialView("CED010200WZone", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010200WSaveDataZone(CED010200WSearchModel m, int mode)
        {
            //User user = Lookup.Get<User>();
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            string result = CED010200WRepository.Instance.SaveDataZone(m, mode, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010200WDeleteDataZone(CED010200WSearchModel m)
        {
            //User user = Lookup.Get<User>();
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            string result = "";
            result = CED010200WRepository.Instance.DeleteDataZone(m, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }
    }
}