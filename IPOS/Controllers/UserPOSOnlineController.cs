﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.UserPOSOnline;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class UserPOSOnlineController : CommonController
    {
        string modulID = "01";
        string functionID = "UserPOSOnline";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<UserPOSOnline>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult UserPOSOnlineSearchNoData(UserPOSOnlineSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<UserPOSOnline>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("UserPOSOnlineGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult UserPOSOnlineSearchData(UserPOSOnlineSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<UserPOSOnline>();
                viewModel.GridPaging = new Paging(UserPOSOnlineRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.DataList = UserPOSOnlineRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("UserPOSOnlineGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult DeleteData(string UserId, string POS)
        {
            string msg = UserPOSOnlineRepository.Instance.DeleteData(UserId, POS);
            return Json(msg, JsonRequestBehavior.AllowGet);
        }
    }
}