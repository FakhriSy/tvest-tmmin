﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED071300W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED071300WController : Controller //CommonController
    {
        string modulID = "07";
        string functionID = "CED071300W";

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            //FID.Ridwan : 2020-01-07
            var userName = "";
            HttpCookie aCookie = Request.Cookies["nameClosing"];
            if (aCookie != null)
            {
                userName = Server.UrlDecode(aCookie.Value);
            }
            else
            {
                userName = "TVEST";
            }
            ViewData["UserName"] = userName;
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            ViewBag.PathIMG = CommonRepository.Instance.getSystemMasterValue(functionID, "IMAGE", "img");

            return View("CED071300W");
        }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED071300W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = functionID;
        //    ViewBag.PathIMG = GetSystemMasterValue(functionID, "IMAGE", "img");
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    Model = viewModel;
        //}

        [HttpPost]
        public ActionResult CED071300WSearchData()
        {
            try
            {
                return PartialView("CED071300WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult getKanbanID(string KanbanID)
        {
            IList<CED071300W> result = CED071300WRepository.Instance.SearchData(KanbanID);
            return Json(result);
        }

        public JsonResult getDataGrid(string PartBarcode, string ProblemID)
        {
            IList<CED071300WGrid> result = CED071300WRepository.Instance.SearchDataGrid(PartBarcode, ProblemID);
            return Json(result);
        }

        public ActionResult CED071300WSaveData(CED071300W m)
        {
            //User user = Lookup.Get<User>();
            string currentuser = "SYSTEM"; //(user != null) ? user.Username : "System";
            string result = CED071300WRepository.Instance.SaveData(m, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult FinishProcess()
        {
            HttpCookie aCookie = Request.Cookies["noregClosing"];
            var userName = aCookie.Value;
            HttpCookie aCookiePos = Request.Cookies["posClosing"];
            var POS = aCookiePos.Value;
            string result = CED071300WRepository.Instance.FinishProcess(userName, POS);
            return Json(result);
        }

        public int CheckExist(params object[] param)
        {
            var result = 0;
            string query = "SELECT COUNT(*) FROM {0} WHERE {1}";
            query = String.Format(query, param);
            result = CommonRepository.Instance.checkExistExec(query);
            return result;
        }
    }
}