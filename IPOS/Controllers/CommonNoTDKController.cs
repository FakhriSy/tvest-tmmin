﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPOS.Controllers
{
    public class CommonNoTDKController : Controller
    {
        public ActionResult GetMessageText(string msgID, string param)
        {
            try
            {
                string result = CommonRepository.Instance.getMessageText(msgID, param);
                var e = result.Split('|');

                return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        #region getData
        public string getData(params object[] param)
        {
            string result = "";
            string query = "SELECT {0} AS T FROM {1} WHERE {2}";
            query = String.Format(query, param);
            result = CommonRepository.Instance.getData(query);
            return result;
        }
        #endregion

    }
}