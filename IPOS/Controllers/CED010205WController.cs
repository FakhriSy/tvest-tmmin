﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED010205W;

namespace IPOS.Controllers
{
    public class CED010205WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010205W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName("01", "CED010205W");
            var viewModel = new GenericModel<CED010205W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = "CED010205W";

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010205WSearchData()
        {
            try
            {
                //var viewModel = new GenericModel<CED010205W>();
                //viewModel.DataList = CED010205WRepository.Instance.SearchData(SearchModel);
                //viewModel.GridPaging = new Paging(CED010205WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                //viewModel.GridPaging.FunctionID = "CED010205W";

                //return PartialView("CED010205WGrid", viewModel);
                return PartialView("CED010205WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010205WSearchDataDetail()
        {
            try
            {
                var viewModel = new GenericModel<CED010205WDetail>();
                viewModel.DataList = CED010205WRepository.Instance.SearchDataDetail();
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = "CED010205W";

                return PartialView("CED010205WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}