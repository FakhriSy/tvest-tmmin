﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED010102W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED010102WController : PageController
    {
        string modulID = "00";
        string functionID = "CED010102W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010102W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010102WSearchNoData(CED010102WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010102W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010102WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED010102WSearchData(CED010102WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010102W>();
                viewModel.DataList = CED010102WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010102WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010102WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010102WSaveData(CED010102W m, int mode)
        {
            User user = Lookup.Get<User>();
            string result = CED010102WRepository.Instance.SaveData(m, mode, user.Username);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010102WDeleteData(CED010102W m)
        {
            string result = CED010102WRepository.Instance.DeleteData(m);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }
    }
}