﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED010305W;

namespace IPOS.Controllers
{
    public class CED010305WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010305W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName("01", "CED010305W");
            var viewModel = new GenericModel<CED010305W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = "CED010305W";

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010305WSearchData(int TS)
        {
            try
            {
                ViewData["RetriveData1"] = CED010305WRepository.Instance.RetrieveData(1, TS);
                ViewData["RetriveData2"] = CED010305WRepository.Instance.RetrieveData(2, 0);

                return PartialView("CED010305WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult getDataStation(int ts)
        {
            IList<CED010305W> result = CED010305WRepository.Instance.RetrieveStation(ts);
            return Json(result);
        }
    }
}