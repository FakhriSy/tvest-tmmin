﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED020200W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED020200WController : CommonController
    {
        string modulID = "02";
        string functionID = "CED020200W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED020200W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult CED020200WSearchNoData(CED020200WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED020200W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED020200WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED020200WSearchData(CED020200WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED020200W>();
                viewModel.DataList = CED020200WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging = new Paging(CED020200WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED020200WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED020200WSearchDataDetail(CED020200WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED020200WDetail>();
                viewModel.DataList = CED020200WRepository.Instance.SearchDataDetail(SearchModel);
                //viewModel.GridPaging = new Paging(CED020200WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                //viewModel.GridPaging.FunctionID = "CED020200W";
                IList<CED020200WDetail> getData = CED020200WRepository.Instance.SearchDataDetail(SearchModel);
                ViewBag.LineCode = getData[0].LineCode;
                ViewBag.LineName = getData[0].LineName;

                return PartialView("CED020200WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult SendParamBatchDownload(CED020200WSearchModel searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED010203R";
                //Ganti function id dan register ke SC
                string functscreen = "CED010203R";
                string userlocation = "TMMIN";
                string parameter = (searchModel.PosType == null ? "" : searchModel.PosType) + ";" +
                                (searchModel.ProdDate == null ? "" : searchModel.ProdDate) + ";" +
                                (searchModel.ProdLine == null ? "" : searchModel.ProdLine) + ";" +
                                (searchModel.SHIFT == null ? "" : searchModel.SHIFT);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult SendParamBatchUpload(string filename, string type)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "";
                if (type == "POS Final")
                {
                    functbatch = "CED020100B";
                }
                else if (type == "POS Prepare")
                {
                    functbatch = "CED020900B";
                }
                else if (type == "POS Jundate")
                {
                    functbatch = "CED021100B";
                }
                string functscreen = "CED020200W";
                string userlocation = "TAM";
                string parameter = (filename == null ? "" : filename) + ";" +
                            (type == null ? "" : type);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED020200WDeleteDataDetail(CED020200WSearchModel data)
        {
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            string result = "";
            result = CED020200WRepository.Instance.DeleteDataDetail(data, currentuser);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public JsonResult SendParamBatchDownloadROEM(CED020200WSearchModelBatch searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                //string functbatch = "CED021200B";
                string functbatch = "CED010609B";
                string functscreen = "CED020200W";
                string userlocation = "TMMIN";
                //string parameter =
                //                (searchModel.PlantCd == null ? "" : searchModel.PlantCd) + ";" +
                //                (searchModel.DateFrom == null ? "" : searchModel.DateFrom) + ";" +
                //                (searchModel.DateTo == null ? "" : searchModel.DateTo);

                string returnVal = "";
                List<dynamic> resultCheck = CED020200WRepository.Instance.CheckPOSType(searchModel);
                if (resultCheck.Count > 0)
                {
                    if (resultCheck[0].MSG.ToString().Contains("Inhouse"))
                    {
                        string parameter = "Inhouse;" +
                                (searchModel.DateFrom == null ? "" : Convert.ToDateTime(searchModel.DateFrom).ToString("yyyyMM")) + ";" +
                                (searchModel.PlantCd == null ? "" : searchModel.PlantCd) + ";" +
                                (searchModel.DateFrom == null ? "" : Convert.ToDateTime(searchModel.DateFrom).ToString("yyyyMMdd"));
                        returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);
                    }

                    if (resultCheck[0].MSG.ToString().Contains("Outhouse"))
                    {
                        string parameter = "Outhouse;" +
                                (searchModel.DateFrom == null ? "" : Convert.ToDateTime(searchModel.DateFrom).ToString("yyyyMM")) + ";" +
                                (searchModel.PlantCd == null ? "" : searchModel.PlantCd) + ";" +
                                (searchModel.DateFrom == null ? "" : Convert.ToDateTime(searchModel.DateFrom).ToString("yyyyMMdd"));
                        returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);
                    }

                    if (resultCheck[0].MSG.ToString() == "")
                    {
                        return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "MCEDSTD001E : No Data Found" });
                    }
                }


                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        //[HttpPost]
        //public JsonResult SendParamBatchOuthouseReport(string ProdDate)
        //{
        //    try
        //    {
        //        string functbatch = "CED010620R";
        //        string functscreen = "CED020200W";
        //        string username = user.Username;
        //        string userlocation = "TMMIN";
        //        string processid = CommonRepository.Instance.GetProcessId();
        //        string parameter = ProdDate;
        //        string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

        //        return Json(new { result = returnVal });
        //    }
        //    catch (Exception e)
        //    {
        //        return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
        //    }
        //}


        [HttpPost]
        public JsonResult SendParamBatchAllReport(CED020200WSearchModel searchModel)
        {
            try
            {
                string functbatch = "";
                string parameter = "";
                if (searchModel.PosType == "POS InHouse")
                {
                    if (searchModel.ZoneCode.Contains("All"))
                    {
                        var zoneCodes = CommonRepository.Instance.getCombobox("POS_PACKING_LINE", null);
                        searchModel.ZoneCode = string.Join(",", zoneCodes.Select(x => x.VALUE).ToArray());
                    }

                    functbatch = "CED021600B";
                    parameter = (searchModel.ProdDate == null ? "" : searchModel.ProdDate) + ";" +
                                    (searchModel.PosType == null ? "" : searchModel.PosType) + ";" +
                                    (searchModel.SHIFT == null ? "" : searchModel.SHIFT) + ";" +
                                    (searchModel.ZoneCode == null ? "" : searchModel.ZoneCode);
                }
                else if (searchModel.PosType == "POS Prepare Module")
                {
                    functbatch = "CED021400B";
                    parameter = "Prepare;" + (searchModel.ProdDate == null ? "" : searchModel.ProdDate);
                }
                else if (searchModel.PosType == "POS Jundate")
                {
                    functbatch = "CED021500B";
                    parameter = "Jundate;" + (searchModel.ProdDate == null ? "" : searchModel.ProdDate) + ";" +
                                    (searchModel.PosType == null ? "" : searchModel.PosType) + ";" +
                                    (searchModel.SHIFT == null ? "" : searchModel.SHIFT) + ";" +
                                    (searchModel.ProdLine == null ? "" : searchModel.ProdLine);
                }
                else if (searchModel.PosType == "POS OutHouse")
                {
                    functbatch = "CED010620R";
                    parameter = (searchModel.ProdDate == null ? "" : searchModel.ProdDate) + ";" +
                                (searchModel.SHIFT == null ? "" : searchModel.SHIFT);
                }

                string functscreen = "CED020200W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);
                parameter = "";
                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }



    }
}