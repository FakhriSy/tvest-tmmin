﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED030900W;

namespace IPOS.Controllers
{
    public class CED030900WController : Controller //CommonController
    {
        string modulID = "03";
        string functionID = "CED030900W";
        //public User user { get { return Lookup.Get<User>(); } }

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            //ViewData["RetriveData1"] = CED030900WRepository.Instance.RetrieveData(1);
            //ViewData["RetriveData2"] = CED030900WRepository.Instance.RetrieveData(2);
            //ViewData["RetriveData3"] = CED030900WRepository.Instance.RetrieveData(3);
            //ViewData["RetriveData4"] = CED030900WRepository.Instance.RetrieveData(4);

            ViewBag.Refresh = CommonRepository.Instance.getSystemMasterValue(functionID, "REFRESH_INTERVAL", "REFRESH");

            //FID.Ridwan : 2019-09-11 
            ViewData["BindTS"] = CED030900WRepository.Instance.BindTS();

            return View("CED030900W");
        }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED030900W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED030900W";
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    ViewData["RetriveData1"] = CED030900WRepository.Instance.RetrieveData(1);
        //    ViewData["RetriveData2"] = CED030900WRepository.Instance.RetrieveData(2);
        //    ViewData["RetriveData3"] = CED030900WRepository.Instance.RetrieveData(3);
        //    ViewData["RetriveData4"] = CED030900WRepository.Instance.RetrieveData(4);

        //    ViewBag.Refresh = GetSystemMasterValue("CED030900W", "REFRESH_INTERVAL", "REFRESH");

        //    Model = viewModel;
        //}

        [HttpPost]
        public ActionResult CED030900WSearchData(string TS)
        {
            try
            {
                ViewData["RetriveData1"] = CED030900WRepository.Instance.RetrieveData(1, TS);
                ViewData["RetriveData2"] = CED030900WRepository.Instance.RetrieveData(2, TS);
                ViewData["RetriveData3"] = CED030900WRepository.Instance.RetrieveData(3, TS);
                ViewData["RetriveData4"] = CED030900WRepository.Instance.RetrieveData(4, TS);

                return PartialView("CED030900WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}