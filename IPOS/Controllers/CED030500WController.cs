﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED030500W;

namespace IPOS.Controllers
{
    public class CED030500WController : Controller //CommonController
    {
        string modulID = "03";
        string functionID = "CED030500W";
        //public User user { get { return Lookup.Get<User>(); } }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED030500W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED030500W";
        //    ViewData["BindTS"] = CED030500WRepository.Instance.BindTS();

        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));
        //    ViewBag.TopRows = GetSystemMasterValue("CED030500W", "INFORMATION", "ROWS");
        //    ViewBag.Refresh = GetSystemMasterValue("CED030500W", "TIME", "REFRESH");

        //    Model = viewModel;
        //}

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";

            ViewData["BindTS"] = CED030500WRepository.Instance.BindTS();

            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));
            ViewBag.TopRows = GetSystemMasterValue("CED030500W", "INFORMATION", "ROWS");
            ViewBag.Refresh = GetSystemMasterValue("CED030500W", "TIME", "REFRESH");

            return View("CED030500W");
        }

        public ActionResult CED030500WSearchData(string TS)
        {
            try
            {
                ViewData["RetriveData1"] = CED030500WRepository.Instance.RetrieveData("1", TS);
                ViewData["RetriveData2"] = CED030500WRepository.Instance.RetrieveData("2", TS);

                return PartialView("CED030500WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult getDataStation(string ts)
        {
            IList<CED030500W> result = CED030500WRepository.Instance.RetrieveStation(ts);
            return Json(result);
        }

        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}