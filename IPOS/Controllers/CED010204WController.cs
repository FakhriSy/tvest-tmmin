﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.Common;
using IPOS.Models.CED010204W;

namespace IPOS.Controllers
{
    public class CED010204WController : Controller
    {
        string modulID = "00";
        string functionID = "CED010204W";
        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    ViewBag.Interval = CommonRepository.Instance.getSystemMasterValue("ENG_PCK", "ENG_PCK", "REFRESH");
        //}

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";
            ViewBag.Interval = CommonRepository.Instance.getSystemMasterValue("ENG_PCK", "ENG_PCK", "REFRESH");
            
            CED010204WRepository repo = new CED010204WRepository();
            var data = repo.GetData();
            ViewBag.Shift = data[0].Shift;

            return View("CED010204W");
        }

        public JsonResult LoadData()
        {
            CED010204WRepository repo = new CED010204WRepository();
            var data = repo.GetData();
            return Json(data);
        }
    }
}