﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED030300W;

namespace IPOS.Controllers
{
    public class CED030300WController : Controller //CommonController
    {
        string modulID = "03";
        string functionID = "CED030300W";
        //public User user { get { return Lookup.Get<User>(); } }
        string userName = "TVEST";

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED030300W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED030300W";
        //    ViewBag.TimeIdle = GetSystemMasterValue("CED030300W", "IDLE_TIME", "300");
        //    ViewBag.ProcessIdle = GetSystemMasterValue("CED030300W", "IDLE_TIME", "10");
        //    ViewBag.SoundError = GetSystemMasterValue("CED030300W", "SOUND_NOTIFICATION", "ERROR");
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    Model = viewModel;
        //}

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";
            ViewBag.TimeIdle = GetSystemMasterValue("CED030300W", "IDLE_TIME", "300");
            ViewBag.ProcessIdle = GetSystemMasterValue("CED030300W", "IDLE_TIME", "10");
            ViewBag.SoundError = GetSystemMasterValue("CED030300W", "SOUND_NOTIFICATION", "ERROR");
            ViewBag.MsgHide = GetSystemMasterValue("CED030300W", "MSG_TIME", "MSG");
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));
            //FID.Ridwan : 2019-10-07
            ViewBag.ClearScreen = GetSystemMasterValue("CED030300W", "BARCODE_CLEAR_SCREEN", "CLEAR");

            return View("CED030300W");
        }

        [HttpPost]
        public ActionResult CED030300WSearchData()
        {
            try
            {
                return PartialView("CED030300WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult CheckDockNo(string dockNo)
        {
            var msg = "";
            IList<CED030300WValidate> result = CED030300WRepository.Instance.CheckDockNo(dockNo);
            if (result != null)
            {
                msg = result[0].MSG;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult CheckDeliveryNo(string dockNo, string deliveryNo)
        {
            //string userName = user.Username;
            var msg = "";
            IList<CED030300WValidate> result = CED030300WRepository.Instance.CheckDeliveryNo(dockNo, deliveryNo, userName);
            if (result != null)
            {
                msg = result[0].MSG;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult ValidateScan(string dockNo, string deliveryNo)
        {
            //string userName = user.Username;
            var msg = "";
            IList<CED030300WValidate> result = CED030300WRepository.Instance.ValidateData(dockNo, deliveryNo, userName);
            if (result != null)
            {
                msg = result[0].MSG;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public JsonResult RetrieveDataDCL(string dockNo, string deliveryNo)
        {
            IList<CED030300W> result = CED030300WRepository.Instance.SearchData(dockNo, deliveryNo);
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public JsonResult SubmitSkid(string dockNo, string deliveryNo, string totalSkid)
        {
            //string userName = user.Username;
            var msg = "";
            IList<CED030300WValidate> result = CED030300WRepository.Instance.SubmitSkid(dockNo, deliveryNo, totalSkid, userName);
            if (result != null)
            {
                msg = result[0].MSG;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}