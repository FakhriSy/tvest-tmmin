﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Data;
using System.Data.OleDb;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED030700W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED030700WController : CommonController
    {
        string modulID = "03";
        string functionID = "CED030700W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED030700W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult CED030700WSearchNoData(CED030700WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED030700W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED030700WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED030700WSearchData(CED030700WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED030700W>();
                viewModel.DataList = CED030700WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging = new Paging(CED030700WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED030700WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED030700WSaveData(CED030700W m, int mode)
        {
            string result = CED030700WRepository.Instance.SaveData(m, mode, user.Username);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public JsonResult SendParamBatchDownload(CED030700WSearchModel searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED031400R";
                string functscreen = "CED030700W";
                string userlocation = "TAM";
                string parameter = (searchModel.DeliveryDateFrom == null ? "" : searchModel.DeliveryDateFrom) + ";" +
                                    (searchModel.DeliveryDateTo == null ? "" : searchModel.DeliveryDateTo) + ";" +
                                    (searchModel.Shift == null ? "" : searchModel.Shift) + ";" +
                                    (searchModel.PhysicalDock == null ? "" : searchModel.PhysicalDock) + ";" +
                                    (searchModel.RouteName == null ? "" : searchModel.RouteName) + ";" +
                                    (searchModel.RouteCycle == null ? "" : searchModel.RouteCycle) + ";" +
                                    (searchModel.LogicalDock == null ? "" : searchModel.LogicalDock) + ";" +
                                    (searchModel.TS == null ? "" : searchModel.TS) + ";" +
                                    (searchModel.Supplier == null ? "" : searchModel.Supplier) + ";" +
                                    (searchModel.ManifestNo == null ? "" : searchModel.ManifestNo) + ";" +
                                    (searchModel.OrderNo == null ? "" : searchModel.OrderNo) + ";" +
                                    (searchModel.Status == null ? "" : searchModel.Status) + ";" +
                                    (searchModel.KanbanId == null ? "" : searchModel.KanbanId) + ";" +
                                    (searchModel.PartNo == null ? "" : searchModel.PartNo) + ";" +
                                    (searchModel.Dest == null ? "" : searchModel.Dest) + ";" +
                                    (searchModel.Lot == null ? "" : searchModel.Lot) + ";" +
                                    (searchModel.Case == null ? "" : searchModel.Case) + ";" +
                                    (searchModel.Dock == null ? "" : searchModel.Dock) + ";" +
                                    (searchModel.Type == null ? "" : searchModel.Type) + ";" +
                                    (searchModel.ActualReceive == null ? "" : searchModel.ActualReceive);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchUpload(string filename)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED031100B";
                string functscreen = "CED030700W";
                string userlocation = "TMMIN";
                string parameter = (filename == null ? "" : filename);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}