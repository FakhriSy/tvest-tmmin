﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED031502W;
using IPOS.Models.Common;
using Newtonsoft.Json.Linq;
using System.Net.Http;
using Newtonsoft.Json;
using System.Net.Http.Headers;
using System.Net;

namespace IPOS.Controllers
{
    public class CED031502WController : PageController
    {
        string modulID = "03";
        string functionID = "CED031502W";
        public User user { get { return Lookup.Get<User>(); } }
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED031502W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public JsonResult CED031502WReceiveData(string param)
        {
            try
            {
                IList<CED031502WReceive> receivedata = new List<CED031502WReceive>();
                string username = user.Username;
                receivedata = CED031502WRepository.Instance.SearchDataRecive(param, username);
                if (receivedata.Count == 0){
                    return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "NOT FOUND" });
                }else if (receivedata[0].CHECK_VALIDATE != "ERROR" /* && receivedata[0].SUP_COMPLETENESS_STATUS != "0" */) {
                    if (receivedata[0].CHECK_VALIDATE != "NOT FOUND")
                    {
                        if (receivedata[0].CHECK_VALIDATE != "ALREADY SCAN")
                        {
                            return Json(new { success = true, data = receivedata }, JsonRequestBehavior.AllowGet);
                        }
                        else
                        {
                            return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "ALREADY SCAN" });
                        }
                    }
                    else
                    {
                        return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "NOT FOUND" });
                    }
                }
                //else if(receivedata[0].SUP_COMPLETENESS_STATUS == "0")
                //{
                //    return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "UNCONFIRMED" });
                //}
                else
                {
                    return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "ERROR" });
                }
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
        [HttpPost]
        public ActionResult CED031502WSearchDataInitial(CED031502WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031502W>();
                viewModel.DataList = CED031502WRepository.Instance.SearchDataInitial(SearchModel);
                viewModel.GridPaging = new Paging(CED031502WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031502WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031502WSearchData(CED031502WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031502W>();
                viewModel.DataList = CED031502WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED031502WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031502WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031502WSearchDataDetail(CED031502WSearchModel SearchModel)
        {
            try
            {
                functionID = "DetailCED031502W";
                var viewModel = new GenericModel<CED031502WDetail>();
                viewModel.DataList = CED031502WRepository.Instance.SearchDataDetail(SearchModel);
                //viewModel.GridPaging = new Paging(CED030600WRepository.Instance.SearchCountDataDetail(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging = new Paging(15, SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;
                
                return PartialView("CED031502WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031502WSearchDataProblemPart(CED031502WSearchModel SearchModel)
        {
            try
            {
                functionID = "DetailCED031502W";
                var viewModel = new GenericModel<CED031502WProblemPart>();
                viewModel.DataList = CED031502WRepository.Instance.SearchDataPproblemPart(SearchModel);
                viewModel.GridPaging = new Paging(15, SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031502WProblemHistory", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult CED031502WSaveData(List<CED031502WSearchModel> list)
        {
            string result = "";
            var e = result.Split('|');
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            if (list != null)
            {
                foreach (var dataForm in list)
                {
                    CED031502WSearchModel data = new CED031502WSearchModel();
                    data.MANIFEST_NO = dataForm.MANIFEST_NO;
                    data.PART_BARCODE = dataForm.PART_BARCODE;
                    data.FULL_STATUS = dataForm.FULL_STATUS;
                    data.PCS_STATUS = dataForm.PCS_STATUS;
                    result = CED031502WRepository.Instance.SaveData(data, currentuser);
                    e = result.Split('|');
                }
            }
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult CED031502WSaveDataFlag(string ManifestNo, string PartNo, string CheckType)
        {
            string result = CED031502WRepository.Instance.SaveDataFlag(ManifestNo, PartNo, CheckType);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult CED031502WSaveDataTempHeader(List<CED031502WSearchModel> list)
        {
            string result = "";
            var e = result.Split('|');
            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            if (list != null)
            {
                foreach (var dataForm in list)
                {
                    CED031502WSearchModel data = new CED031502WSearchModel();
                    data.MANIFEST_NO = dataForm.MANIFEST_NO;
                    data.PART_NO = dataForm.PART_NO;
                    data.REC_STATUS = dataForm.REC_STATUS;
                    result = CED031502WRepository.Instance.SaveDataTempHeader(data, currentuser);
                    e = result.Split('|');
                }
            }
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult CED031502WSaveDataReceive(string ManifestNo)
        {
            var MSG_TYPE = "";
            var MSG_TXT = "";

            User user = Lookup.Get<User>();
            string currentuser = (user != null) ? user.Username : "System";
            // == belokin ke api
            var _header = CED031502WRepository.Instance.prepareHeader(ManifestNo, currentuser);
            // save ke temp dulu sambil prepare var buat syn
            var _problem = CED031502WRepository.Instance.prepareProblem(ManifestNo, currentuser);
            var _problemDetail = CED031502WRepository.Instance.prepareProblemDetail(ManifestNo, currentuser);

            // hit api
            var syncReceivingResult = hitAPI(_header, _problem, _problemDetail);

            //bypass
            //CED031502WApi syncReceivingResult = new CED031502WApi();
            //syncReceivingResult.statusCode = 200;

            if (syncReceivingResult.statusCode == 200)
            {
                string result2 = CED031502WRepository.Instance.SaveDataReceive(ManifestNo, currentuser);
                var e2 = result2.Split('|');
                MSG_TYPE = e2[0];
                MSG_TXT = e2[1];
            }
            else
            {
                MSG_TYPE = "E";
                MSG_TXT = syncReceivingResult.message;

                string FUNCTION_ID = "CED031502W";
                string MODULE_ID = "03";
                string FUNCTION_NM = CED031502WRepository.Instance.GetFunctionName(FUNCTION_ID, MODULE_ID);

                string process_id = CED031502WRepository.Instance.CreateLog(user.Username, FUNCTION_ID, MODULE_ID, "MCED031500005E", FUNCTION_NM, MSG_TXT, "2", null, "start");
                CED031502WRepository.Instance.CreateLog(user.Username, FUNCTION_ID, MODULE_ID, "MCED031500006E", FUNCTION_NM, FUNCTION_NM, "2", process_id, "end");
            }

            //string result = CED031502WRepository.Instance.SaveDataReceive(ManifestNo, currentuser);
            //var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = MSG_TYPE, MSG_TXT = MSG_TXT });
        }

        private static CED031502WApi hitAPI(CED031502WReceive _header, IList<CED031502WProblemPartDetail> _problems, IList<CED031502WProblemPartDetail> _problemDetails)
        {
            var result = new CED031502WApi();

            //var apitoken = hitAPIAuth();
            //if (apitoken.result == false)
            //{
            //    result.statusCode = 401;
            //    result.result = apitoken.result;
            //    result.message = "[Sync] Error : " + apitoken.message;
            //    return result;
            //}

            string baseUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_BASE_URL").Trim();
            string endPointUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_RECEIVING_SYNC").Trim();
            JObject paramsBody = new JObject();
            // prepare params
            paramsBody["MANIFEST_NO"] = _header.MANIFEST_NO;
            paramsBody["DOCK_CD"] = _header.DOCK_CD;
            paramsBody["ARRIVAL_ACTUAL_DT"] = _header.ARRIVAL_TIME;
            paramsBody["CHANGED_BY"] = _header.CHANGED_BY;
            paramsBody["PROBLEM_FLAG"] = _header.PROBLEM_FLAG;

            JArray problem = new JArray();
            foreach (var _problem in _problems)
            {
                var tmpproblem = new JObject();
                //tmpproblem["MANIFEST_NO"] = _problem.MANIFEST_NO;
                tmpproblem["PART_NO"] = _problem.PART_NO;
                tmpproblem["ORDER_QTY"] = _problem.ORDER_QTY;
                tmpproblem["RECEIVED_QTY"] = _problem.RECEIVED_QTY;
                tmpproblem["SHORTAGE_QTY"] = _problem.SHORTAGE_QTY;
                tmpproblem["MISSPART_QTY"] = _problem.MISSPART_QTY;
                tmpproblem["DAMAGE_QTY"] = _problem.DAMAGE_QTY;
                tmpproblem["RECEIVED_STATUS"] = _problem.RECEIVED_STATUS;
                tmpproblem["DELETION_FLAG"] = _problem.DELETION_FLAG;
                tmpproblem["DELETION_DT"] = _problem.DELETION_DT;
                tmpproblem["DELETION_BY"] = _problem.DELETION_BY;
                tmpproblem["AREA_NAME"] = _problem.AREA_NAME;
                tmpproblem["SOLVED_FLAG"] = _problem.SOLVED_FLAG;
                tmpproblem["CREATED_BY"] = _problem.CREATED_BY;
                tmpproblem["CREATED_DT"] = _problem.CREATED_DT;
                tmpproblem["CHANGED_BY"] = _problem.CHANGED_BY;
                tmpproblem["CHANGED_DT"] = _problem.CHANGED_DT;
                tmpproblem["REASON"] = _problem.REASON;

                problem.Add(tmpproblem);
            }

            JArray problemDetail = new JArray();
            foreach (var _problemDetail in _problemDetails)
            {
                var tmpproblem = new JObject();
                //tmpproblem["MANIFEST_NO"] = _problemDetail.MANIFEST_NO;
                tmpproblem["PART_NO"] = _problemDetail.PART_NO;
                tmpproblem["PROBLEM_TYPE"] = _problemDetail.PROBLEM_TYPE;
                tmpproblem["PROBLEM_DT"] = _problemDetail.PROBLEM_DT;
                tmpproblem["QTY"] = _problemDetail.QTY;
                tmpproblem["CREATED_BY"] = _problemDetail.CREATED_BY;
                tmpproblem["CREATED_DT"] = _problemDetail.CREATED_DT;
                tmpproblem["CHANGED_BY"] = _problemDetail.CHANGED_BY;
                tmpproblem["CHANGED_DT"] = _problemDetail.CHANGED_DT;
                tmpproblem["PART_BARCODE"] = _problemDetail.PART_BARCODE;
                tmpproblem["LAST_STATUS"] = _problemDetail.LAST_STATUS;

                problemDetail.Add(tmpproblem);
            }

            paramsBody["PROBLEM"] = problem;
            paramsBody["PROBLEM_DETAIL"] = problemDetail;

            try
            {
                //string jsonBody = JsonSerializer.SerializeToString(data);
                string jsonBody = paramsBody.ToString(Formatting.None);
                string url = baseUrl + endPointUrl;
                // Create the request body.
                HttpContent content = new StringContent(jsonBody, System.Text.Encoding.UTF8, "application/json");
                // Send request.
                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromSeconds(30);
                //client.DefaultRequestHeaders.Authorization = new AuthenticationHeaderValue("Bearer", apitoken.message);
                var response = client.PostAsync(url, content);
                var responseMessage = response.Result.Content.ReadAsStringAsync().Result;
                try
                {
                    var responseMessageArray = JArray.Parse(responseMessage);
                    result = JObject.Parse(responseMessageArray[0].ToString()).ToObject<CED031502WApi>();
                    result.statusCode = (int)response.Result.StatusCode;
                }
                catch (Exception ex2)
                {
                    try
                    {
                        result = JObject.Parse(responseMessage.ToString()).ToObject<CED031502WApi>();
                        result.statusCode = (int)response.Result.StatusCode;
                    }
                    catch (Exception ex3)
                    {
                        var responseMessageArray = JArray.Parse(responseMessage);
                        result.message = ex3.Message.ToString();
                        result.statusCode = 400;

                    }

                }
                return result;

            }
            catch (Exception ex)
            {
                result.statusCode = 400;
                result.result = false;
                result.message = ex.InnerException is null ? ex.Message.ToString() : ex.InnerException.Message.ToString();
                result.message = (result.message == "A task was canceled." ? "Request time out" : result.message);
                result.message = "[Sync] Error : " + result.message;
            }

            return result;
        }

        private static CED031502WApi hitAPIAuth()
        {
            string baseUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_BASE_URL").Trim();
            //string baseUrl = "http://localhost:59999";
            string endPointUrl = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_AUTH").Trim();
            string userName = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_USER").Trim();
            string password = CommonController.GetSystemMasterValue("TVEST_X_IPPCS", "API", "API_PASS").Trim();

            var result = new CED031502WApi();
            result.result = true;
            try
            {
                string url = baseUrl + endPointUrl + "?userName=" + userName + "&password=" + password;
                // Send request.
                HttpClient client = new HttpClient();
                client.Timeout = TimeSpan.FromSeconds(30);
                var response = client.GetAsync(url);
                var responseMessage = response.Result.Content.ReadAsStringAsync().Result;
                var checkResult = JObject.Parse(responseMessage);
                result.statusCode = (int)response.Result.StatusCode;

                try
                {
                    var token = (string)checkResult["token"]["access_token"];
                    var token_type = (string)checkResult["token"]["token_type"];
                    var expires_in = (int)checkResult["token"]["expires_in"];

                    result.message = token;

                    if (result.statusCode != (int)HttpStatusCode.OK || string.IsNullOrEmpty(result.message))
                    {
                        result.result = false;
                    }
                }
                catch (Exception ex2)
                {
                    result.result = false;
                    result.message = "error when getting token";
                }
                return result;

            }
            catch (Exception ex)
            {
                result.statusCode = 400;
                result.result = false;
                result.message = ex.InnerException.Message.ToString();
                result.message = (result.message == "A task was canceled." ? "Request time out" : result.message);
            }

            return result;
        }
    }
}