﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED021500W;
using IPOS.Models.Common;
using System.Reflection;

namespace IPOS.Controllers
{
    public class CED021500WController : PageController
    {
        string modulID = "02";
        string functionID = "CED021500W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED021500W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            ViewBag.Type = 0;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED021500WSearchNoData(CED021500WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED021500W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED021500WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED021500WSearchData(CED021500WSearchModel SearchModel)
        {
            try
            {
                Guid PID = Guid.NewGuid();
                string result;

                result = CED021500WRepository.Instance.SaveData(SearchModel, PID.ToString());

                var viewModel = new GenericModel<CED021500W>();
                viewModel.DataList = CED021500WRepository.Instance.SearchData(SearchModel, PID.ToString());
                viewModel.GridPaging = new Paging(CED021500WRepository.Instance.SearchCountData(SearchModel, PID.ToString()), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                var grid = "";
                if (SearchModel.Process == "PIPELINE")
                    grid = "CED021500WGrid";
                else if (SearchModel.Process == "SORTING")
                    grid = "CED021500WGridSorting";
                else if (SearchModel.Process == "BOXING")
                    grid = "CED021500WGridBoxing";
                else if (SearchModel.Process == "FLOWRACK")
                    grid = "CED021500WGridFlowrack";
                else if (SearchModel.Process == "PICKING")
                    grid = "CED021500WGridPicking";
                else if (SearchModel.Process == "STACKING")
                    grid = "CED021500WGridStacking";
                else if (SearchModel.Process == "PROBLEM")
                    grid = "CED021500WGridProblem";
                else if (SearchModel.Process == "MODULETYPE")
                    grid = "CED021500WGridModuletype";

                if (SearchModel.Type == "Detail")
                    ViewBag.Type = 0;
                else
                    ViewBag.Type = 1;

                result = CED021500WRepository.Instance.DeleteTempData(SearchModel, PID.ToString());

                return PartialView(grid, viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.InnerException.ToString() });
            }
        }

        [HttpPost]
        public ActionResult ComboBoxSelectListChildCode(String typeComboBox, String comboBoxID, String comboBoxParent)
        {
            try
            {
                /*
                 * Type ComboBox :
                 * 0 : BLANK
                 * 1 : -ALL-
                 * 2 : -SELECT-
                 */
                NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

                IEnumerable<comboBox> dataList = CED021500WRepository.Instance.getComboboxCode(comboBoxID, null);
                if (!String.IsNullOrEmpty(comboBoxParent))
                {
                    dataList = dataList.Where(data => data.PARENT == comboBoxParent);
                }
                IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

                return Json(new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

    }
}