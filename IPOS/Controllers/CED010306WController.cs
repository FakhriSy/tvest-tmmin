﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED010306W;

namespace IPOS.Controllers
{
    public class CED010306WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010306W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName("01", "CED010306W");
            var viewModel = new GenericModel<CED010306W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = "CED010306W";

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010306WSearchNoData(CED010306WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010306W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = "CED010306W";

                return PartialView("CED010306WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public ActionResult CED010306WSearchData(CED010306WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010306W>();
                viewModel.DataList = CED010306WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010306WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = "CED010306W";

                return PartialView("CED010306WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED010306WSearchDataDetail(CED010306WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010306WDetail>();
                viewModel.DataList = CED010306WRepository.Instance.SearchDataDetail(SearchModel);
                viewModel.GridPaging = new Paging(CED010306WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = "CED010306W";

                return PartialView("CED010306WDetail", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}