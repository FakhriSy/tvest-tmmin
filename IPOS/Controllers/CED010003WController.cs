﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED010003W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED010003WController : PageController
    {
        string modulID = "00";
        string functionID = "CED010003W";
        public User user { get { return Lookup.Get<User>(); } }
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010003W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;
            ViewBag.Username = user.Username;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010003WSearchNoData()
        {
            try
            {
                var viewModel = new GenericModel<CED010003W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010003WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED010003WSearchData(string username, string StartDT, string EndDT, string ProcessID, string FunctionID, int page, int size)
        {
            try
            {   
                var viewModel = new GenericModel<CED010003W>();
                viewModel.DataList = CED010003WRepository.Instance.SearchData(username, StartDT, EndDT, ProcessID, FunctionID, page, size);
                viewModel.GridPaging = new Paging(CED010003WRepository.Instance.SearchCountData(username, StartDT, EndDT, ProcessID, FunctionID), page, size);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010003WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public FileResult DownloadFile(string filename)
        {
            string fullPath = CommonController.GetSystemMasterValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER") + filename;
            return File(fullPath, "application/vnd.ms-excel", filename);
        }
    }
}