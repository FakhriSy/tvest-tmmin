﻿using System.Web.Mvc;
using System;
using IPOS.Models.CED031600W;
using IPOS.Models.Common;
using Toyota.Common.Web.Platform;
using Toyota.Common.Credential;
using System.IO;
using System.Web;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Data.OleDb;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using System.Web.WebPages;

namespace IPOS.Controllers
{
    public class CED031600WController : CommonController
    {
        string modulID = "03";
        string functionID = "CED031600W";

        public User user { get { return Lookup.Get<User>(); } }
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED031600W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED031600WSearchNoData(CED031600WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031600W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED031600WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED031600WSearchData(CED031600WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED031600W>();
                viewModel.DataList = CED031600WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED031600WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;
                viewModel.type = SearchModel.type;

                return PartialView("CED031600WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchUpload(string filename)
        {
            try
            {
                string functbatch = "CED010604B";
                string functscreen = "CED310600W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                string parameter = filename;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchForeCast(string PKG_MTH, string PACKING_PLANT_CD, string GROUP_PKG_LINE, string MP)
        {
            try
            {
                string functbatch = "CED010605B";
                string functscreen = "CED310600W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                string parameter = PKG_MTH + ";" + PACKING_PLANT_CD + ";" + GROUP_PKG_LINE + ";" + MP;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchCapacityFirm(string PKG_MTH, string PLANT_CD, string GROUP_PACKING, string MP)
        {
            try
            {
                string functbatch = "CED010607B";
                string functscreen = "CED310600W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                string parameter = PKG_MTH + ";" + PLANT_CD + ";" + GROUP_PACKING + ";" + MP;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchMPSL(string PKG_MTH, string PLANT_CD)
        {
            try
            {
                string functbatch = "CED010608B";
                string functscreen = "CED310600W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                string parameter = PKG_MTH + ";" + PLANT_CD;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

    }
}