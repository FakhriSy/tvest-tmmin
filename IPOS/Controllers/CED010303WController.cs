﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED010303W;

namespace IPOS.Controllers
{
    public class CED010303WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010303W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010303W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = "CED010303W";

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010303WSearchData()
        {
            try
            {
                return PartialView("CED010303WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}