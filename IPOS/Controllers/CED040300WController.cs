﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED040300W;
using Microsoft.Reporting.WebForms;
using Zen.Barcode;
using System.Drawing.Imaging;

namespace IPOS.Controllers
{
    public class CED040300WController : Controller //CommonController
    {
        string modulID = "04";
        string functionID = "CED040300W";
        //public User user { get { return Lookup.Get<User>(); } }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED040300W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED040300W";
        //    ViewBag.PathBKP = GetSystemMasterValue("CED040300W", "IMAGE", "BKPImage");
        //    ViewBag.PathPS1 = GetSystemMasterValue("CED040300W", "IMAGE", "PS1Image");
        //    ViewBag.PathPS2 = GetSystemMasterValue("CED040300W", "IMAGE", "PS2Image");
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    Model = viewModel;
        //}

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";

            ViewBag.PathBKP = GetSystemMasterValue("CED040300W", "IMAGE", "BKPImage");
            ViewBag.PathPS1 = GetSystemMasterValue("CED040300W", "IMAGE", "PS1Image");
            ViewBag.PathPS2 = GetSystemMasterValue("CED040300W", "IMAGE", "PS2Image");
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            //FID.Ridwan : 2020-01-14
            ViewBag.Clear = GetSystemMasterValue("CED040300W", "BARCODE_CLEAR", "CLEAR");

            return View("CED040300W");
        }

        [HttpPost]
        public ActionResult CED040300WSearchData()
        {
            try
            {
                return PartialView("CED040300WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED040300WPopup()
        {
            try
            {
                return PartialView("CED040300WPopup", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult getDataKanban(string barcode, string LineCd, string KanbanType)
        {
            IList<CED040300W> result = CED040300WRepository.Instance.SearchData(barcode, LineCd, KanbanType);
            return Json(result);
        }

        public JsonResult getDataGrid1(CED040300W search_data)
        {
            IList<CED040300WGrid1> result = CED040300WRepository.Instance.SearchDataGrid1(search_data);
            return Json(result);
        }

        public ActionResult CED040300WSaveData(CED040300WTrx m)
        {
            string result = CED040300WRepository.Instance.SaveData(m);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult ValidateBoxing(string pos, string part_no, string part_barcode, int qty_kanban, int qty_box, string pattern_cd)
        {
            string result = CED040300WRepository.Instance.ValidateScanBoxing(pos, part_no, part_barcode, qty_kanban, qty_box, pattern_cd);
            return Json(result);
        }

        public string GenerateBarcode(string keyid)
        {
            var pathFile = Server.MapPath("~/Report/Download/barcode_" + keyid + ".png");
            if (!System.IO.File.Exists(pathFile))
            {
                var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
                var image = draw.Draw(keyid, 70, 1);
                byte[] arr;
                using (var memStream = new MemoryStream())
                {
                    using (FileStream fs = new FileStream(pathFile, FileMode.Create, FileAccess.ReadWrite))
                    {
                        image.Save(memStream, ImageFormat.Png);
                        arr = memStream.ToArray();
                        fs.Write(arr, 0, arr.Length);
                    }
                }
            }

            return pathFile;
        }

        public ActionResult PrintReport(string pos, string kanban2)
        {
            try
            {
                HttpCookie aCookie = Request.Cookies["name"];
                var userName = aCookie.Value;
                List<CED040300WPrint> listData = CED040300WRepository.Instance.PrintKanban3(pos, userName, kanban2).ToList();
                LocalReport report = new LocalReport();
                report.ReportPath = Server.MapPath("~/Report/Kanban3.rdlc");
                report.EnableExternalImages = true;
                for (var i = 0; i < listData.Count(); i++)
                {
                    string pathBarcode = "file:" + GenerateBarcode(listData[i].Kanban);
                    listData[i].PathBarcode = pathBarcode;
                }
                report.Refresh();
                report.DataSources.Add(
                   new ReportDataSource("dsKanban", listData));
                PrintReport PR = new Models.Common.PrintReport();
                string deviceInfo =
                  @"<DeviceInfo>
                    <OutputFormat>EMF</OutputFormat>
                    <PageWidth>3in</PageWidth>
                    <PageHeight>2in</PageHeight>
                    <MarginTop>0in</MarginTop>
                    <MarginLeft>0in</MarginLeft>
                    <MarginRight>0in</MarginRight>
                    <MarginBottom>0in</MarginBottom>
                </DeviceInfo>";
                PR.Export(report, deviceInfo);
                string PrinterName = "";
                PR.Print(PrinterName);

                //ShowReportKanban3(pos, kanban2, listData);

                return Json("Print Success");
            }
            catch (Exception ex)
            {
                return Json(ex.ToString());
            }
        }

        public JsonResult ProblemProcess(string KanbanId, string StartTime)
        {
            HttpCookie aCookie = Request.Cookies["noreg"];
            var userName = aCookie.Value;
            HttpCookie aCookiePOS = Request.Cookies["pos"];
            var pos = aCookiePOS.Value;
            string result = CED040300WRepository.Instance.ProblemProcess(KanbanId, userName, pos, StartTime, "", "", "", 0, 0);
            return Json(result);
        }

        public JsonResult ProblemProcessMultiple(string KanbanId, string StartTime, string PartNo, string PatternCd, string kabanType, int QtyKanban, int QtyBox)
        {
            HttpCookie aCookie = Request.Cookies["noreg"];
            var userName = aCookie.Value;
            HttpCookie aCookiePOS = Request.Cookies["pos"];
            var pos = aCookiePOS.Value;
            string result = CED040300WRepository.Instance.ProblemProcess(KanbanId, userName, pos, StartTime, PartNo, PatternCd, kabanType, QtyKanban, QtyBox);
            return Json(result);
        }

        public JsonResult UpdateFlagBoxing(string KanbanId, string StartTime, string kanbanProcess)
        {
            HttpCookie aCookie = Request.Cookies["noreg"];
            var userName = aCookie.Value;
            HttpCookie aCookiePOS = Request.Cookies["pos"];
            var pos = aCookiePOS.Value;
            string result = CED040300WRepository.Instance.UpdateFlagBoxing(KanbanId, StartTime, userName, kanbanProcess, pos);
            return Json(result);
        }

        public JsonResult FinishProcess(string KanbanId, string StartTime, string POS)
        {
            HttpCookie aCookie = Request.Cookies["noreg"];
            var userName = aCookie.Value;
            string result = CED040300WRepository.Instance.FinishProcess(KanbanId, StartTime, userName, POS);
            return Json(result);
        }

        public JsonResult CheckKanban(string barcode)
        {
            string result = CED040300WRepository.Instance.CheckKanban(barcode);
            return Json(result);
        }

        public JsonResult CheckStockKanban3(string barcode)
        {
            string result = CED040300WRepository.Instance.CheckStockKanban3(barcode);
            return Json(result);
        }

        public ActionResult ShowReportKanban3(string pos, string kanban2, List<CED040300WPrint> listData)
        {
            LocalReport report = new LocalReport();
            report.ReportPath = Server.MapPath("~/Report/Kanban3.rdlc");
            report.EnableExternalImages = true;
            for (var i = 0; i < listData.Count(); i++)
            {
                string pathBarcode = "file:" + GenerateBarcode(listData[i].Kanban);
                listData[i].PathBarcode = pathBarcode;
            }
            report.Refresh();
            report.DataSources.Add(
               new ReportDataSource("dsKanban", listData));

            //Setting Export to PDF
            string reportType = "PDF";
            string mimeType;
            string encoding;
            string fileNameExtension;

            //The DeviceInfo settings should be changed based on the reportType
            //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
            string deviceInfo =
            "<DeviceInfo>" +
            "  <OutputFormat>PDF</OutputFormat>" +
            "  <PageWidth>3in</PageWidth>" +
            "  <PageHeight>2in</PageHeight>" +
            "  <MarginTop>0in</MarginTop>" +
            "  <MarginLeft>0in</MarginLeft>" +
            "  <MarginRight>0in</MarginRight>" +
            "  <MarginBottom>0in</MarginBottom>" +
            "</DeviceInfo>";

            ////Setting Export to Image
            //string reportType = "Image";
            //string mimeType = "image/png";
            //string encoding;
            //string fileNameExtension;

            //string deviceInfo =
            //"<DeviceInfo>" +
            //"  <OutputFormat>PNG</OutputFormat>" +
            //"  <PageWidth>7.5</PageWidth>" +
            //"  <PageHeight>6.3</PageHeight>" +
            //"  <MarginTop>0in</MarginTop>" +
            //"  <MarginLeft>0in</MarginLeft>" +
            //"  <MarginRight>0in</MarginRight>" +
            //"  <MarginBottom>0in</MarginBottom>" +
            //"</DeviceInfo>";

            Warning[] warnings;
            string[] streams;
            byte[] renderedBytes;

            //Render the report
            renderedBytes = report.Render(
                reportType,
                deviceInfo,
                out mimeType,
                out encoding,
                out fileNameExtension,
                out streams,
                out warnings);

            //Show Direct PDF / Image
            //return File(renderedBytes, mimeType);

            string targetDir = GetSystemMasterValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER");
            string pdfFileName = pos + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + "_Kanban3.pdf";
            string pdfFullPath = targetDir + pdfFileName;
            using (FileStream fs = new FileStream(pdfFullPath, FileMode.Create))
            {
                fs.Write(renderedBytes, 0, renderedBytes.Length);
            }

            return Json("", JsonRequestBehavior.AllowGet);
        }

        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public String DirectPrint(string parameter)
        {
            string result = "sukses";
            try
            {
                string path = GetSystemMasterValue("CED040300W", "PATH", "DIRECT_PRINTING");
                System.Diagnostics.Process process1 = new System.Diagnostics.Process();
                process1.StartInfo.FileName = @path;
                process1.StartInfo.Arguments = parameter;
                process1.Start();
                process1.WaitForExit();
                process1.Close();
                return result;
            }
            catch (Exception e)
            {
                result = e.Message.ToString();
                return e.Message;
            }
        }
 
        //FID.Ridwan : 2019-12-21
        public JsonResult CheckKanbanCancel(string barcode)
        {
            string result = CED040300WRepository.Instance.CheckKanbanCancel(barcode);
            return Json(result);
        }

        public JsonResult getDataKanbanCancel(string barcode, string POS)
        {
            HttpCookie aCookie = Request.Cookies["noreg"];
            var userName = aCookie.Value;

            IList<CED040300WCancelGrid> result = CED040300WRepository.Instance.SearchDataCancel(barcode, POS, userName);
            return Json(result);
        }

        public JsonResult FinishProcessCancel(string barcode, string POS)
        {
            HttpCookie aCookie = Request.Cookies["noreg"];
            var userName = aCookie.Value;
            string result = CED040300WRepository.Instance.FinishProcessCancel(barcode, userName, POS);
            return Json(result);
        }

    }
}