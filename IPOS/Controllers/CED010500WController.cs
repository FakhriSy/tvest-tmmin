﻿using System.Web.Mvc;
using System;
using IPOS.Models.CED010500W;
using IPOS.Models.Common;
using Toyota.Common.Web.Platform;
using Toyota.Common.Credential;
using NPOI.SS.UserModel;
using System.IO;
using NPOI.HSSF.UserModel;
using NPOI.XSSF.UserModel;
using System.Collections.Generic;
using System.Diagnostics;
using System.Web.UI.WebControls;
using ServiceStack.Text;
using NPOI.SS.Formula.Functions;

namespace IPOS.Controllers
{
    public class CED010500WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010500W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010500W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010500WSearchNoData(CED010500WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010500W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010500WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED010500WSearchData(CED010500WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010500W>();
                viewModel.DataList = CED010500WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010500WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010500WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010500WSaveData(CED010500WSaveModel SaveModel)
        {
            User user = Lookup.Get<User>();
            string result = CED010500WRepository.Instance.SaveData(SaveModel, user.Username);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010500WCreateCalendar()
        {
            User user = Lookup.Get<User>();
            string result = CED010500WRepository.Instance.CreateCalendar();
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public JsonResult CED010500WUpload(string filename)
        {
            try
            {
                string result = "";
                User user = Lookup.Get<User>();
                CommonRepository _comm = new CommonRepository();
                string _filePathFolder = _comm.getSystemMasterValue("REPORT", "FOLDER", "TEMP_UPLOAD");
                string fullpathfile = Server.MapPath(_filePathFolder + filename);

                ISheet sheet;
                var files = new FileStream(fullpathfile, FileMode.Open, FileAccess.Read);
                var fileExt = Path.GetExtension(filename);

                if (fileExt == ".xls")
                {
                    HSSFWorkbook hssfwb = new HSSFWorkbook(files);
                    sheet = hssfwb.GetSheetAt(0);
                }
                else
                {
                    XSSFWorkbook hssfwb = new XSSFWorkbook(files);
                    sheet = hssfwb.GetSheetAt(0);
                }

                for (int row = 2; row <= sheet.LastRowNum; row++)
                {
                    if (sheet.GetRow(row) != null)
                    {
                        CED010500WSaveModel data = new CED010500WSaveModel();
                        var packMonth = sheet.GetRow(row).GetCell(1).ToString();
                        DateTime day = DateTime.Parse(packMonth.Substring(0, 4) + "-" + packMonth.Substring(4, 2) + "-01");
                        var eoMonth = day.EndOfLastMonth().Day;
                        for(var d = 0; d < eoMonth; d++)
                        {
                            data.PACKING_PLANT = sheet.GetRow(row).GetCell(0).ToString();
                            data.DATE = day.AddDays(d).ToString("yyyy-MM-dd");
                            var dayName = day.AddDays(d).ToString("dddd");
                            data.WORKING_DAY = (dayName == "Saturday" || dayName == "Sunday") ? "0" : "1";
                            data.SHIFT = sheet.GetRow(row).GetCell(2 + d).ToString();
                            result = CED010500WRepository.Instance.SaveData(data, user.Username);
                        }
                    }
                }

                return Json(new resultMessage { MSG_TYPE = result.Split('|')[0], MSG_TXT = result.Split('|')[1] });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}