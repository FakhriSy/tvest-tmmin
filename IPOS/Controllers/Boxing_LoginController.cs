﻿using IPOS.Models.Boxing_Login;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPOS.Controllers
{
    public class Boxing_LoginController : Controller //CommonController
    {
        public ActionResult Index()
        {
            return View("Boxing_Login");
        }

        public JsonResult checkLogin(string line, string noreg)
        {
            var ipAdd = Request.ServerVariables["HTTP_X_FORWARDED_FOR"];

            if (string.IsNullOrEmpty(ipAdd))
            {
                ipAdd = Request.ServerVariables["REMOTE_ADDR"];
            }

            //string ipAdd = string.Empty;
            //ipAdd = Request.UserHostAddress;

            string result = Boxing_LoginRepository.Instance.ValidateLogin(line, noreg, ipAdd);
            return Json(result);
        }
    }
}