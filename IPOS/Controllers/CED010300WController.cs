﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED010300W;
using IPOS.Models.Common;
using System.Reflection;

namespace IPOS.Controllers
{
    public class CED010300WController : PageController
    {
        string modulID = "01";
        string functionID = "CED010300W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010300W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CED010300WSearchNoData(CED010300WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010300W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010300WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CED010300WSearchData(CED010300WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010300W>();
                viewModel.DataList = CED010300WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010300WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                ViewBag.QKP_IMAGE_GET = CommonRepository.Instance.getSystemMasterValue("MASTER", "QKP_IMAGE_GET", functionID);

                return PartialView("CED010300WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010300WSearchDataSupplier(CED010300WSearchModelSupplier SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<LockupSupplier>();
                viewModel.DataList = CED010300WRepository.Instance.SearchDataSupplier(SearchModel);
                viewModel.GridPaging = new Paging(CED010300WRepository.Instance.SearchCountDataSupplier(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = "CED010300WlookupSupplier";

                return PartialView("CED010300WLockupSupplier", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
        public ActionResult CED010300WSearchDataPart(CED010300WSearchModelPart SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<LockupPart>();
                viewModel.DataList = CED010300WRepository.Instance.SearchDataPart(SearchModel);
                viewModel.GridPaging = new Paging(CED010300WRepository.Instance.SearchCountDataPart(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = "CED010300WlookupPart";

                return PartialView("CED010300WLockupPart", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public string upload(HttpPostedFileBase file, string sys_sub_cat)
        {
            var fileName = "";
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the filename
                fileName = Guid.NewGuid().ToString().Replace("-", "") + "_" + Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var ipath = CommonRepository.Instance.getSystemMasterValue("MASTER", sys_sub_cat, functionID);
                file.SaveAs(ipath + fileName);
            }
            // redirect back to the index action to show the form once again
            return fileName;
        }

        public ActionResult CED010300WSaveData(CED010300W m, int mode)
        {
            User user = Lookup.Get<User>();
            string result = "";
            var valExt = 0;

            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    var ext = Path.GetExtension(file.FileName).ToLower().Replace(".", "");
                    if (ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "tif" || ext == "gif" || ext == "bmp")
                    {
                        valExt = 1;
                    }
                    else
                    {
                        valExt = 0;
                    }
                }
                if (valExt != 1 && Request.Files.Count > 0)
                {
                    result = "E|Extension file upload.!";
                }
                else
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i];
                        if (m.QkpImage == i.ToString() && valExt == 1)
                        {
                            m.QkpImage = upload(file, "QKP_IMAGE");
                        }
                    }
                    result = CED010300WRepository.Instance.SaveData(m, mode, user.Username);
                }
                var e = result.Split('|');
                return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
            }
            catch (Exception e1)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e1.Message });
            }

            //result = CED010300WRepository.Instance.SaveData(m, mode, user.Username);
            //var e = result.Split('|');
            //return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010300WDeleteData(CED010300W m)
        {
            User user = Lookup.Get<User>();
            string result = CED010300WRepository.Instance.DeleteData(m, user.Username);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public ActionResult ComboBoxSelectListChildCode(String typeComboBox, String comboBoxID, String comboBoxParent)
        {
            try
            {
                /*
                 * Type ComboBox :
                 * 0 : BLANK
                 * 1 : -ALL-
                 * 2 : -SELECT-
                 */
                NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

                IEnumerable<comboBox> dataList = CED010300WRepository.Instance.getComboboxCode(comboBoxID, null);
                if (!String.IsNullOrEmpty(comboBoxParent))
                {
                    dataList = dataList.Where(data => data.PARENT == comboBoxParent);
                }
                IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

                return Json(new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public JsonResult UploadFile()
        {
            string result = "0";
            string SystemPath = CommonController.GetSystemMasterValue("MASTER", "PATH_UPLOAD", "CED010300W"); //"/Report/Images/CED010300W/";
            List<string> ListFileName = new List<string>();
            string TargetPath = String.Empty;
            string Msg = String.Empty;

            for (int i = 0; i < Request.Files.Count; i++)
            {
                var file = Request.Files[i];
                var fileName = Path.GetFileName(file.FileName);
                ListFileName.Add(fileName);
                TargetPath = System.Web.Hosting.HostingEnvironment.MapPath(SystemPath);
                if (!System.IO.Directory.Exists(TargetPath))
                {
                    System.IO.Directory.CreateDirectory(TargetPath);
                }
                var path = Path.Combine(TargetPath, fileName);
                try
                {
                    file.SaveAs(path);
                    result = "true|" + fileName;
                }
                catch (Exception ex)
                {
                    System.IO.File.Delete(path);
                    Msg = ex.Message.ToString();
                    result = "false|" + Msg;
                }
            }
            return Json(result, JsonRequestBehavior.AllowGet);
        }

        public string CED010300WMoveImage(string fileName)
        {
            string result = string.Empty;
            try
            {
                string path = HttpContext.Request.MapPath(CommonController.GetSystemMasterValue("MASTER", "PATH_UPLOAD", "CED010300W") + fileName); //"~/Report/Images/CED010300W/"

                string targetPath = CommonController.GetSystemMasterValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER"); //"D://MyData//kerjaan//IPOMS-Data//SOURCE//IPOMS//IPOS//Report//Download";

                string destFile = System.IO.Path.Combine(targetPath, fileName);

                if (!System.IO.Directory.Exists(targetPath))
                {
                    System.IO.Directory.CreateDirectory(targetPath);
                }

                System.IO.File.Copy(path, destFile, true);

                result = "true|sukses";
            }
            catch (Exception e)
            {
                result = "false|MCEDSTD017E|" + fileName;
                //result = "false|MCEDSTD050E|" + e.Message.ToString();
            }
            

            return result;
        }

        [HttpGet]
        //Action Filter, it will auto delete the file after download
        [DeleteFileAttribute]
        public ActionResult Download(String file)
        {
            //get the temp folder and file path in server
            String folderTempDownload = Server.MapPath(GetDestinationPath());
            String fileTempLocation = Path.Combine(folderTempDownload, file);

            string ext = Path.GetExtension(file);

            return File(fileTempLocation, "image/" + ext, file);
        }

        private static String GetDestinationPath()
        {
            return "~/Report/Download/";
        }

    }
}