﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED010407W;
using Microsoft.Reporting.WebForms;
using Zen.Barcode;
using System.Drawing.Imaging;
using System.Drawing.Printing;
using System.Drawing;

namespace IPOS.Controllers
{
    public class CED010407WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010407W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName("01", "CED010407W");
            var viewModel = new GenericModel<CED010407W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = "CED010407W";

            List<string> listPrinter = new List<string>();
            foreach (string printer in PrinterSettings.InstalledPrinters)
            {
                listPrinter.Add(printer.ToString());
            }
            ViewData["ListPrinter"] = listPrinter;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010407WSearchNoData(CED010407WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010407W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = "CED010407W";

                return PartialView("CED010407WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        [HttpPost]
        public ActionResult CED010407WSearchData(CED010407WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010407W>();
                viewModel.DataList = CED010407WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010407WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = "CED010407W";

                return PartialView("CED010407WGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED010407WSearchDataDetail(CED010407WSearchModel SearchModel)
        {
            try
            {
                ViewData["FileName"] = GenerateModul(SearchModel);

                return PartialView("CED010407WDetail");
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public string GenerateBarcode(string keyid)
        {
            var draw = new Code128BarcodeDraw(Code128Checksum.Instance);
            var image = draw.Draw(keyid, 70, 1);
            byte[] arr;
            using (var memStream = new MemoryStream())
            {
                image.Save(memStream, ImageFormat.Png);
                arr = memStream.ToArray();
            }
            var pathFile = Server.MapPath("~/Report/Download/barcode_" + keyid + ".png");
            System.IO.File.WriteAllBytes(pathFile, arr);

            return pathFile;
        }

        public string GenerateModul(CED010407WSearchModel SearchModel)
        {
            List<CED010407WDetail> listData = CED010407WRepository.Instance.SearchDataDetail(SearchModel).ToList();
            string filename = "CED010409Rpt" + listData[0].PartNo + ".png";
            if (!System.IO.File.Exists(Server.MapPath("~/Report/Download/" + filename)))
            {
                LocalReport localReport = new LocalReport();
                localReport.ReportPath = Server.MapPath("~/Report/CED010409Rpt.rdlc");
                localReport.EnableExternalImages = true;
                //Set Param
                string pathSource = Server.MapPath("").Replace("\\CED010407W", "");
                string pathBarcode = GenerateBarcode(listData[0].Barcode);
                localReport.SetParameters(new ReportParameter("ImgPathSource", pathSource));
                localReport.SetParameters(new ReportParameter("ImgBarcode", pathBarcode));
                localReport.Refresh();
                ReportDataSource reportDataSource = new ReportDataSource("DSCED010409", listData);

                localReport.DataSources.Add(reportDataSource);

                //Setting Export to PDF
                //string reportType = "PDF";
                //string mimeType;
                //string encoding;
                //string fileNameExtension;

                //The DeviceInfo settings should be changed based on the reportType
                //http://msdn2.microsoft.com/en-us/library/ms155397.aspx
                //string deviceInfo =
                //"<DeviceInfo>" +
                //"  <OutputFormat>PDF</OutputFormat>" +
                //"  <PageWidth>8.27in</PageWidth>" +
                //"  <PageHeight>11.69in</PageHeight>" +
                //"  <MarginTop>0.5in</MarginTop>" +
                //"  <MarginLeft>0.5in</MarginLeft>" +
                //"  <MarginRight>0.5in</MarginRight>" +
                //"  <MarginBottom>0.5in</MarginBottom>" +
                //"</DeviceInfo>";

                //Setting Export to Image
                string reportType = "Image";
                string mimeType = "image/png";
                string encoding;
                string fileNameExtension;

                string deviceInfo =
                "<DeviceInfo>" +
                "  <OutputFormat>PNG</OutputFormat>" +
                "  <PageWidth>7.5</PageWidth>" +
                "  <PageHeight>6.3</PageHeight>" +
                "  <MarginTop>0in</MarginTop>" +
                "  <MarginLeft>0in</MarginLeft>" +
                "  <MarginRight>0in</MarginRight>" +
                "  <MarginBottom>0in</MarginBottom>" +
                "</DeviceInfo>";

                Warning[] warnings;
                string[] streams;
                byte[] renderedBytes;

                //Render the report
                renderedBytes = localReport.Render(
                    reportType,
                    deviceInfo,
                    out mimeType,
                    out encoding,
                    out fileNameExtension,
                    out streams,
                    out warnings);

                //System.IO.File.WriteAllBytes(Server.MapPath("~/Report/Download/CED010409Rpt.pdf"), renderedBytes);
                System.IO.File.WriteAllBytes(Server.MapPath("~/Report/Download/" + filename), renderedBytes);

                //Show Direct PDF / Image
                //return File(renderedBytes, mimeType);
            }

            return filename;
        }

        public ActionResult PrintDocModul(string PartNo, string PrinterName)
        {
            string filename = "CED010409Rpt" + PartNo + ".png";
            PrintDocument pd = new PrintDocument();
            pd.DefaultPageSettings.PrinterSettings.PrinterName = PrinterName;
            pd.DefaultPageSettings.Landscape = false;
            pd.PrintPage += (sender, args) => { 
                Image i = Image.FromFile(Server.MapPath("~/Report/Download/" + filename));
                Rectangle m = args.MarginBounds;
                if ((double)i.Width / (double)i.Height > (double)m.Width / (double)m.Height) // image is wider 
                { 
                    m.Height = (int)((double)i.Height / (double)i.Width * (double)m.Width); 
                } else { 
                    m.Width = (int)((double)i.Width / (double)i.Height * (double)m.Height); 
                } 
                args.Graphics.DrawImage(i, m); 
            }; 
            pd.Print();

            return Json(filename, JsonRequestBehavior.AllowGet);
        }
    }
}