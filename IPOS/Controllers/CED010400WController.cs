﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Data;
using System.Data.OleDb;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED010400W;
using IPOS.Models.Common;
using System.Web.WebPages;

namespace IPOS.Controllers
{
    public class CED010400WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010400W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010400W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [ValidateAntiForgeryToken]
        public ActionResult CED010400WSearchNoData(CED010400WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010400W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010400WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult CED010400WSearchData(CED010400WSearchModel searchModel)
        {
            try
            {
                ViewBag.BKP_IMAGE_GET = CommonRepository.Instance.getSystemMasterValue("MASTER", "BKP_IMAGE_GET", functionID);
                ViewBag.DS_IMAGE_GET = CommonRepository.Instance.getSystemMasterValue("MASTER", "DS_IMAGE_GET", functionID);
                ViewBag.PKP_IMAGE_GET = CommonRepository.Instance.getSystemMasterValue("MASTER", "PKP_IMAGE_GET", functionID);
                ViewBag.PS1_IMAGE_GET = CommonRepository.Instance.getSystemMasterValue("MASTER", "PS1_IMAGE_GET", functionID);
                ViewBag.PS2_IMAGE_GET = CommonRepository.Instance.getSystemMasterValue("MASTER", "PS2_IMAGE_GET", functionID);

                var viewModel = new GenericModel<CED010400W>();
                viewModel.DataList = CED010400WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging = new Paging(CED010400WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010400WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public string upload(HttpPostedFileBase file, string sys_sub_cat)
        {
            var fileName = "";
            // Verify that the user selected a file
            if (file != null && file.ContentLength > 0)
            {
                // extract only the filename
                fileName = Guid.NewGuid().ToString().Replace("-", "") + "_" + Path.GetFileName(file.FileName);
                // store the file inside ~/App_Data/uploads folder
                var ipath = CommonRepository.Instance.getSystemMasterValue("MASTER", sys_sub_cat, functionID);
                file.SaveAs(ipath + fileName);
            }
            // redirect back to the index action to show the form once again
            return fileName;
        }

        [HttpPost]
        public ActionResult CED010400WSaveData(CED010400W m, int mode)
        {
            string result = "";
            var valExt = 0;
            try
            {
                for (int i = 0; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase file = Request.Files[i];
                    var ext = Path.GetExtension(file.FileName).ToLower().Replace(".", "");
                    if (ext == "jpg" || ext == "jpeg" || ext == "png" || ext == "tif" || ext == "gif" || ext == "bmp")
                    {
                        valExt = 1;
                    }
                    else
                    {
                        valExt = 0;
                    }
                }

                if (valExt != 1 && m.Table == "3" && Request.Files.Count > 0)
                {
                    result = "E|Extension file upload.!";
                }
                else
                {
                    for (int i = 0; i < Request.Files.Count; i++)
                    {
                        HttpPostedFileBase file = Request.Files[i];
                        if (m.Bkp_Image == i.ToString() && valExt == 1)
                        {
                            m.Bkp_Image = upload(file, "BKP_IMAGE");
                        }
                        if (m.Pkp_Image == i.ToString() && valExt == 1)
                        {
                            m.Pkp_Image = upload(file, "PKP_IMAGE");
                        }
                        if (m.Ps1_Image == i.ToString() && valExt == 1)
                        {
                            m.Ps1_Image = upload(file, "PS1_IMAGE");
                        }
                        if (m.Ps2_Image == i.ToString() && valExt == 1)
                        {
                            m.Ps2_Image = upload(file, "PS2_IMAGE");
                        }
                        if (m.Ds_Image == i.ToString() && valExt == 1)
                        {
                            m.Ds_Image = upload(file, "DS_IMAGE");
                        }
                    }
                    //m.ValidFrom_Minus = m.ValidFrom.AsDateTime().AddDays(-1).ToString();
                    result = CED010400WRepository.Instance.SaveData(m, mode, user.Username);
                }
                var e = result.Split('|');
                return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
            }
            catch (Exception e1)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e1.Message });
            }
        }

        public ActionResult CED010400WDeleteData(List<CED010400W> model)
        {
            string result = CED010400WRepository.Instance.DeleteData(model, user.Username);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        [HttpPost]
        public JsonResult SendParamBatchDownload(CED010400WSearchModel searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED040900R";
                string functscreen = "CED010400W";
                string userlocation = "TMMIN";
                string parameter = processid + ";" +
                                  (searchModel.Part_No == null ? "" : searchModel.Part_No) + ";" +
                                  (searchModel.Line_Id == null ? "" : searchModel.Line_Id) + ";" +
                                  (searchModel.Pattern_Id == null ? "" : searchModel.Pattern_Id) + ";" +
                                  //(searchModel.Dock_Code == null ? "" : searchModel.Dock_Code) + ";" +
                                  (searchModel.Passthru_Flag == null ? "" : searchModel.Passthru_Flag) + ";" +
                                  (searchModel.showLatest == null ? "" : searchModel.showLatest);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchUpload(string filename, string type) //, string validFrom, string validTo)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = type; //"CED010412B";
                string functscreen = "CED010400W";
                string userlocation = "TMMIN";
                string parameter = (filename == null ? "" : filename);
                //+ ";" +
                //        (type == null ? "" : type) + ";" +
                //        (validFrom == null ? "" : validFrom) + ";" +
                //        (validTo == null ? "" : validTo);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}