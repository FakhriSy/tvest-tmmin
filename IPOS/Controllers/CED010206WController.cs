﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.CED010206W;
using IPOS.Models.Common;
using IPOS.Models.CED030700W;

namespace IPOS.Controllers
{
    public class CED010206WController : PageController
    {
        string modulID = "01";
        string functionID = "CED010206W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010206W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010206WSearchNoData(CED010206WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010206W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010206WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }


        [HttpPost]
        public ActionResult CED010206WSearchData(CED010206WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010206W>();
                viewModel.DataList = CED010206WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010206WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010206WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatch(string CONTROL_MODULE_NO)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED021800L";
                string functscreen = "CED010206W";
                string userlocation = "TMMIN";
                string parameter = (CONTROL_MODULE_NO == null ? "" : CONTROL_MODULE_NO) + ";";
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}