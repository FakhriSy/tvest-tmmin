﻿using IPOS.Models.CED010618W;
using IPOS.Models.Common;
using System;
using Toyota.Common.Credential;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;

namespace IPOS.Controllers
{
    public class CED010618WController : PageController
    {
        string modulID = "01";
        string functionID = "CED010618W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010618W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public User user { get { return Lookup.Get<User>(); } }
        public ActionResult CED010618WSearchModuleTypeNoData(CED010618WSearchModuleTypeModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010618W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID + "ModuleType";

                return PartialView("CED010618WModuleTypeGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        public ActionResult CED010618WSearchPartJundateNoData(CED010618WSearchPartJundateModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010618W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID + "PartJundate";

                return PartialView("CED010618WPartJundateGrid", viewModel);
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }


        public ActionResult CED010618WSearchModuleType(CED010618WSearchModuleTypeModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010618W>();
                viewModel.DataList = CED010618WRepository.Instance.SearchModuleType(searchModel);
                viewModel.GridPaging = new Paging(CED010618WRepository.Instance.CountModuleType(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID + "ModuleType";

                return PartialView("CED010618WModuleTypeGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010618WSearchPartJundate(CED010618WSearchPartJundateModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010618W>();
                viewModel.DataList = CED010618WRepository.Instance.SearchPartJundate(searchModel);
                viewModel.GridPaging = new Paging(CED010618WRepository.Instance.CountPartJundate(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID + "PartJundate";

                return PartialView("CED010618WPartJundateGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010618WSaveModuleType(CED010618W m, int mode)
        {
            string result = CED010618WRepository.Instance.SaveModuleType(m, mode,  user.Username);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010618WSavePartJundate(CED010618W m, int mode)
        {
            string result = CED010618WRepository.Instance.SavePartJundate(m, mode, user.Username);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010618WDeleteModuleType(CED010618W m)
        {
            string result = CED010618WRepository.Instance.DeleteModuleType(m);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult CED010618WDeletePartJundate(CED010618W m)
        {
            string result = CED010618WRepository.Instance.DeletePartJundate(m);
            var e = result.Split('|');
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }


        [HttpPost]
        public JsonResult SendParamBatchUpload(string filename, string dataName)
        {
            try
            {
                string functbatch = "CED010618B";
                string functscreen = "CED010618W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();
                string parameter = filename + ";" + dataName;
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}