﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class ReportController : Controller
    {
        public ActionResult Download(string id)
        {
            String _folderDownload = GetSystemMasterValue("REPORT", "FOLDER", "EXCEL_REPORT_FOLDER");
            String fileTempLocation = Path.Combine(_folderDownload, id);

            return File(fileTempLocation, "application/octet-stream", id);
        }

        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}