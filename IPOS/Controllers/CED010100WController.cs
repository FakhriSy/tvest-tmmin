﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED010100W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED010100WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010100W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010100W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult CED010100WSearchNoData(CED010100WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010100W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010100WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010100WSearchData(CED010100WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010100W>();
                viewModel.GridPaging = new Paging(CED010100WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.DataList = CED010100WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010100WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult SendParamBatchDownload(CED010100WSearchModel searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED010100W";
                string functscreen = "CED010100W";
                string userlocation = "TAM";
                string parameter = (searchModel.ProdDate == null ? "" : searchModel.ProdDate);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}