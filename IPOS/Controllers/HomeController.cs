﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using IPOS.Models;

namespace IPOS.Controllers
{
    public class HomeController : PageController
    {
        protected override void Startup()
        {
            Settings.Title = "Dashboard";
            if (!ApplicationSettings.Instance.Security.SimulateAuthenticatedSession)
            {
                ViewData["ListFunction"] = null;//AppRepository.Instance.getApps(AppRepository.Instance.countApps());
            }
            else
            {
                ViewData["ListFunction"] = null;
            }
        }

        public ActionResult WidgetSettings()
        {
            return PartialView("_WidgetSettings");
        }

    }
}
