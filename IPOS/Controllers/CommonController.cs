﻿/************************************************************************************************
 * Program History : 
 * 
 * Project Name     : Gentani Management System
 * Client Name      : PT. TMMIN (Toyota Manufacturing Motor Indonesia)
 * Function Id      : 
 * Function Name    : 
 * Function Group   : Common
 * Program Id       : Common
 * Program Name     : Common
 * Program Type     : Controller
 * Description      : 
 * Environment      : .NET 4.0, ASP MVC 4.0
 * Author           : 
 * Version          : 01.00.00
 * Creation Date    : 
 *
 * Update history		Re-fix date				Person in charge				Description
 * FID. Fitrah          18-04-208                                               Update MSG_TXT to TB_M_MESSAGE
 * Copyright(C) 2018 - . All Rights Reserved
 *************************************************************************************************/
using IPOS.Models.Common;
using NPOI.HSSF.UserModel;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using Toyota.Common.Credential;

namespace IPOS.Controllers
{
    public class CommonController : PageController
    {
        #region Combobox
        [HttpPost]
        public ActionResult ComboBoxList(String typeComboBox, String comboBoxID, String comboBoxParam)
        {
            try
            {
                /*
                 * Type ComboBox :
                 * 0 : BLANK
                 * 1 : -ALL-
                 * 2 : -SELECT-
                 */
                NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

                IEnumerable<comboBox> dataList = CommonRepository.Instance.getCombobox(comboBoxID, comboBoxParam);
                IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

                return Json(new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        public static SelectList ComboBoxSelectList(String typeComboBox, String comboBoxID, String comboBoxParam)
        {
            /*
             * Type ComboBox :
             * 0 : BLANK
             * 1 : -ALL-
             * 2 : -SELECT-
             */
            NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

            IEnumerable<comboBox> dataList = CommonRepository.Instance.getCombobox(comboBoxID, comboBoxParam);
            IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

            return new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty);
        }

        [HttpPost]
        public ActionResult ComboBoxSelectListChild(String typeComboBox, String comboBoxID, String comboBoxParent)
        {
            try
            {
                /*
                 * Type ComboBox :
                 * 0 : BLANK
                 * 1 : -ALL-
                 * 2 : -SELECT-
                 */
                NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

                IEnumerable<comboBox> dataList = CommonRepository.Instance.getCombobox(comboBoxID, null);
                if (!String.IsNullOrEmpty(comboBoxParent))
                {
                    dataList = dataList.Where(data => data.PARENT == comboBoxParent);
                }
                IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

                return Json(new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        public ActionResult TextBoxChild(String typeComboBox, String comboBoxID, String comboBoxParent)
        {
            try
            {
                /*
                 * Type ComboBox :
                 * 0 : BLANK
                 * 1 : -ALL-
                 * 2 : -SELECT-
                 */
                NameValueItem index0 = typeComboBox.Equals("0") ? NameValueItem.Empty : typeComboBox.Equals("1") ? NameValueItem.All : NameValueItem.Mandatory;

                IEnumerable<comboBox> dataList = CommonRepository.Instance.getCombobox(comboBoxID, null);
                if (!String.IsNullOrEmpty(comboBoxParent))
                {
                    dataList = dataList.Where(data => data.PARENT == comboBoxParent);
                }
                else
                {
                    dataList = new List<comboBox>();
                }
                IEnumerable<NameValueItem> nameValueList = dataList.Select(data => new NameValueItem(data.NAME, data.VALUE));

                return Json(new SelectList(Enumerable.Repeat(index0, 1).Concat(nameValueList), NameValueItem.ValueProperty, NameValueItem.NameProperty, NameValueItem.Empty));
            }
            catch (Exception e)
            {
                return Json(e.Message);
            }
        }

        #endregion

        #region Download and Upload
        public static readonly String _folderTemplateDownload = GetSystemMasterValue("REPORT", "FOLDER", "TEMPLATE_DOWNLOAD");
        public static readonly String _folderTempDownload = GetSystemMasterValue("REPORT", "FOLDER", "TEMP_DOWNLOAD");
        public static readonly String _folderTemplateUpload = GetSystemMasterValue("REPORT", "FOLDER", "TEMPLATE_UPLOAD");
        public static readonly String _folderTempUpload = GetSystemMasterValue("REPORT", "FOLDER", "TEMP_UPLOAD");

        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult GetTemplateFile(String fileName)
        {
            string pathUrl = HttpContext.Request.Url.AbsolutePath;
            var splitAct = pathUrl.Split('/');
            if (splitAct[splitAct.Length - 1].ToLower() != "gettemplatefile")
            {
                return HttpNotFound();
            }

            String folderTemplate = Server.MapPath(_folderTemplateUpload);
            String fileTemplateLocation = Path.Combine(folderTemplate, fileName + ".xls");
            try
            {
                FileInfo info = new FileInfo(fileTemplateLocation);
                if (!info.Exists)
                {
                    //Modified By Fitrah
                    var MSG = CommonRepository.Instance.getMessageText("MCEDSTD045E", fileName + ";" + folderTemplate).Split('|');
                    return Json(new resultMessage { MSG_TYPE = MSG[0], MSG_TXT = MSG[1] });
                }

                FileStream stream = new FileStream(fileTemplateLocation, FileMode.Open, FileAccess.Read);
                HSSFWorkbook workbook = new HSSFWorkbook(stream);
                stream.Close();

                String folderTempDownload = Server.MapPath(_folderTempDownload);
                if (!System.IO.Directory.Exists(folderTempDownload))
                {
                    System.IO.Directory.CreateDirectory(folderTempDownload);
                }
                String fileTempLocation = Path.Combine(folderTempDownload, fileName + ".xls");

                using (var exportData = new MemoryStream()) //binding to stream reader
                {
                    workbook.Write(exportData);
                    FileStream file = new FileStream(fileTempLocation, FileMode.Create, FileAccess.Write);
                    exportData.WriteTo(file);
                    file.Close();
                }
                return Json(new { fileName = fileName + ".xls" });
            }
            catch (Exception e)
            {
                var MSG = CommonRepository.Instance.getMessageText("MGMSSTD028E", fileTemplateLocation + ";").Split('|');
                return Json(new resultMessage { MSG_TYPE = MSG[0], MSG_TXT = MSG[1] });
            }
        }

        [HttpPost]
        public ActionResult UploadFile()
        {
            //Get all files from Request object             
            HttpFileCollectionBase files = Request.Files;
            // Validation changes into javascript 20180525
            // if (files.Count > 0)
            // {
            try
            {
                String fname = String.Empty, folderLocation, fileLocation;
                for (int i = 0; i < files.Count; i++)
                {
                    HttpPostedFileBase file = files[i];

                    // String fileExtension = System.IO.Path.GetExtension(file.FileName);
                    // if (fileExtension == ".xls" || fileExtension == ".xlsx")
                    // {
                    // Checking for Internet Explorer  
                    if (Request.Browser.Browser.ToUpper() == "IE" || Request.Browser.Browser.ToUpper() == "INTERNETEXPLORER")
                    {
                        string[] testfiles = file.FileName.Split(new char[] { '\\' });
                        fname = testfiles[testfiles.Length - 1];
                    }
                    else
                    {
                        fname = file.FileName;
                    }

                    // Get the complete folder path and store the file inside it.
                    folderLocation = Server.MapPath(_folderTempUpload);
                    if (!System.IO.Directory.Exists(folderLocation))
                    {
                        System.IO.Directory.CreateDirectory(folderLocation);
                    }

                    fileLocation = Path.Combine(folderLocation, fname);
                    if (System.IO.File.Exists(fileLocation))
                    {
                        System.IO.File.Delete(fileLocation);
                    }

                    file.SaveAs(fileLocation);
                }
                //  else
                //  {
                //      //Modified By Fitrah
                //      var MSG = CommonRepository.Instance.getMessageText("MGMSSTD014E", "Upload;.xls or .xlsx;").Split('|');
                //      return Json(new resultMessage { MSG_TYPE = MSG[0], MSG_TXT =MSG[1] });
                //  }
                // }

                // Returns message that successfully uploaded  
                return Json(new { fileName = fname }, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = ex.Message });
            }
            //}
            //else
            //{
            //    //return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = "Please select a file to upload." });
            //    //Modify By FID.Fitrah
            //    var MSG = CommonRepository.Instance.getMessageText("MGMSSTD010E", "").Split('|');
            //    return Json(new resultMessage { MSG_TYPE = MSG[0], MSG_TXT = MSG[1] });
            //}
        }

        [HttpGet]
        //Action Filter, it will auto delete the file after download
        [DeleteFileAttribute]
        public ActionResult Download(String file)
        {
            string pathUrl = HttpContext.Request.Url.AbsolutePath;
            var splitAct = pathUrl.Split('/');
            if (splitAct[splitAct.Length - 1] != "")
            {
                return HttpNotFound();
            }

            //get the temp folder and file path in server
            String folderTempDownload = Server.MapPath(_folderTempDownload);
            String fileTempLocation = Path.Combine(folderTempDownload, file);

            //return the file for download, this is an Excel 
            //so I set the file content type to "application/vnd.ms-excel"
            return File(fileTempLocation, "application/vnd.ms-excel", file);
        }
        #endregion

        public static void SendMessage(String msg)
        {
            CommonRepository.Instance.SendMessage(msg);
        }

        #region Create Log
        public static String CreateLogStart(String userID, String modulID, String functionID)
        {
            commonLog paramLog = new commonLog();
            var msg = GetMessage("MGMSSTD038I", CommonRepository.Instance.GetFunctionName(modulID, functionID)).Split('|');
            paramLog.msgText = msg[1].Split(':')[1];
            paramLog.userID = userID;
            paramLog.logLocation = functionID.LastIndexOf("B") != -1 ? "Start Batch Process" : "Start Report Process";
            paramLog.msgID = "MGMSSTD038I";
            paramLog.msgType = paramLog.msgID.Substring(paramLog.msgID.Length - 1);
            paramLog.modulID = modulID;
            paramLog.functionID = functionID;

            return CommonRepository.Instance.doInsertLog(paramLog);
        }

        public static String CreateLogDetail(String msgID, String msgText, String userID, String logLocation, String processID, String modulID, String functionID, String processSts)
        {
            commonLog paramLog = new commonLog();
            paramLog.msgText = msgText.Split(':')[1];
            paramLog.userID = userID;
            paramLog.logLocation = logLocation;
            paramLog.processID = processID;
            paramLog.msgID = msgID;
            paramLog.msgType = paramLog.msgID.Substring(paramLog.msgID.Length - 1);
            paramLog.modulID = modulID;
            paramLog.functionID = functionID;
            paramLog.processSts = processSts;

            return CommonRepository.Instance.doInsertLog(paramLog);
        }

        public static String CreateLogFinish(String userID, String processID, String modulID, String functionID, String processSts)
        {
            commonLog paramLog = new commonLog();
            String msgID = processSts.Equals("0") ? "MGMSSTD022I" : "MGMSSTD023I";
            var msg = GetMessage(msgID, CommonRepository.Instance.GetFunctionName(modulID, functionID)).Split('|');
            paramLog.msgText = msg[1].Split(':')[1];
            paramLog.userID = userID;
            paramLog.logLocation = "End Process";
            paramLog.processID = processID;
            paramLog.msgID = msgID;
            paramLog.msgType = paramLog.msgID.Substring(paramLog.msgID.Length - 1);
            paramLog.modulID = modulID;
            paramLog.functionID = functionID;
            paramLog.processSts = processSts;

            return CommonRepository.Instance.doInsertLog(paramLog);
        }
        #endregion

        #region Lock Table
        public static String SetLock(String processID, String functionID, String lockReff, String remark, String userID, String mode)
        {
            return CommonRepository.Instance.doSetLock(processID, functionID, lockReff, remark, userID, mode);
        }
        #endregion

        #region Message
        [ValidateAntiForgeryToken]
        public ActionResult GetMessageText(string msgID, string param)
        {
            try
            {
                string pathUrl = HttpContext.Request.Url.AbsolutePath;
                var splitAct = pathUrl.Split('/');
                if (splitAct[splitAct.Length - 1].ToLower() != "getmessagetext")
                {
                    return HttpNotFound();
                }

                string result = CommonRepository.Instance.getMessageText(msgID, param);
                var e = result.Split('|');

                return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public static String GetMessage(String msgID, String msgParam)
        {
            try
            {
                String result = CommonRepository.Instance.getMessageText(msgID, msgParam);
                return result;
            }
            catch (Exception e)
            {
                return CommonRepository.Instance.getMessageText("MGMSM009E", e.Message);
            }
        }
        #endregion

        #region System Master
        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
        #endregion

        #region CheckExit
        public int CheckExist(params object[] param)
        {
            var result = 0;
            string query = "SELECT COUNT(*) FROM {0} WHERE {1}";
            query = String.Format(query, param);
            result = CommonRepository.Instance.checkExistExec(query);
            return result;
        }
        #endregion

        #region getData
        public string getData(params object[] param)
        {
            string result = "";
            string query = "SELECT {0} AS T FROM {1} WHERE {2}";
            query = String.Format(query, param);
            result = CommonRepository.Instance.getData(query);
            return result;
        }
        #endregion

        public JsonResult getCombobox(string comboBoxID, string comboBoxParent)
        {
            var List = CommonRepository.Instance.getCombobox(comboBoxID, comboBoxParent);
            return Json(List.ToArray(), JsonRequestBehavior.AllowGet);
        }


        protected override void OnException(ExceptionContext filterContext)
        {
            filterContext.ExceptionHandled = true;

            filterContext.Result = new ViewResult
            {
                ViewName = "~/Views/Shared/Error.cshtml"
            };
        }
    }

    //auto delete the file after download
    public class DeleteFileAttribute : ActionFilterAttribute
    {
        public override void OnResultExecuted(ResultExecutedContext filterContext)
        {
            filterContext.HttpContext.Response.Flush();

            if ((filterContext.Result as FilePathResult) != null)
            {
                //convert the current filter context to file and get the file path
                string filePath = (filterContext.Result as FilePathResult).FileName;

                //delete the file after download
                System.IO.File.Delete(filePath);
            }
        }
    }
}