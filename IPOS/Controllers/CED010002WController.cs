﻿using IPOS.Models.CED010002W;
using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Web.Platform;
using System.Web.Mvc;
using Toyota.Common.Credential;
using System.IO;

namespace IPOS.Controllers
{
    public class CED010002WController : PageController
    {
        string modulID = "00";
        string functionID = "CED010002W";
        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010002W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        [HttpPost]
        public ActionResult CED010002WSearchNoData(CED010002WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010002W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010002WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public ActionResult CED010002WSearchData(CED010002WSearchModel SearchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010002W>();
                viewModel.DataList = CED010002WRepository.Instance.SearchData(SearchModel);
                viewModel.GridPaging = new Paging(CED010002WRepository.Instance.SearchCountData(SearchModel), SearchModel.CurrentPage, SearchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010002WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public FileResult DownloadFilePrint4(string filename)
        {
            string fullPath = CommonController.GetSystemMasterValue("REPORT", "FOLDER", "PATH_BARCODE") + filename;
            return File(fullPath, "application/pdf", filename);
        }
    }
}