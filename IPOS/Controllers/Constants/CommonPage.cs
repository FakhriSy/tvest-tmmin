﻿using System;

namespace IPOS.Controllers.Constants
{
    public sealed class CommonPage
    {
        public const String MainLayout = "~/Views/Shared/_Layout.cshtml";
        public const String GridPaging = "~/Views/Shared/_UIPaging.cshtml";
        public const String MainLayoutNoMenu = "~/Views/Shared/_LayoutNoMenu.cshtml";
        public const String MainLayoutNoMenuTheme = "~/Views/Shared/_LayoutNoMenuTheme.cshtml";
        public const String MainLayoutNoMenuTheme1024 = "~/Views/Shared/_LayoutNoMenuTheme1024.cshtml";
    }
}
