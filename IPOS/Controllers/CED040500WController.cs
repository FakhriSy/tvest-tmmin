﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED040500W;

namespace IPOS.Controllers
{
    public class CED040500WController : Controller //CommonController
    {
        string modulID = "04";
        string functionID = "CED040500W";
        string SysSubCat = "REFRESH_INTERVAL";
        //public User user { get { return Lookup.Get<User>(); } }

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            ViewData["UserName"] = "TVEST";
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            this.CED040500WSearchData("1");
            ViewData["TriggerType"] = "1";

            //ViewData["RetriveData1"] = CED040500WRepository.Instance.RetrieveData(1);
            //ViewData["RetriveData2"] = CED040500WRepository.Instance.RetrieveData(2);

            ViewData["AllData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "ALL_DATA"));

            return View("CED040500W");
        }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED040500W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED040500W";
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    ViewData["RetriveData1"] = CED040500WRepository.Instance.RetrieveData(1);
        //    ViewData["RetriveData2"] = CED040500WRepository.Instance.RetrieveData(2);

        //    ViewData["AllData"] = Convert.ToInt32(CommonRepository.Instance.getSystemMasterValue(functionID, SysSubCat, "ALL_DATA"));

        //    Model = viewModel;
        //}

        [HttpPost]
        public ActionResult CED040500WSearchData(string Type)
        {
            try
            {
                ViewData["TriggerType"] = Type;
                ViewData["RetriveData1"] = CED040500WRepository.Instance.RetrieveData(1, Type);
                ViewData["RetriveData2"] = CED040500WRepository.Instance.RetrieveData(2, Type);

                return PartialView("CED040500WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}