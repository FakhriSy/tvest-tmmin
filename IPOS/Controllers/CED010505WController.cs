﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using System.Data;
using System.Data.OleDb;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED010505W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED010505WController : CommonController
    {
        string modulID = "01";
        string functionID = "CED010505W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED010505W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult CED010505WSearchNoData(CED010505WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010505W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010505WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED010505WSearchData(CED010505WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED010505W>();
                viewModel.DataList = CED010505WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging = new Paging(CED010505WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED010505WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult SendParamBatchDownload(CED010505WSearchModel searchModel)
        {
            try
            {
                string username = user.Username;
                string processid = CommonRepository.Instance.GetProcessId();
                string functbatch = "CED010506R";
                string functscreen = "CED010505W";
                string userlocation = "TAM";
                string parameter = (searchModel.ProdDate == null ? "" : searchModel.ProdDate) + ";" +
                                    (searchModel.ShutterNo == null ? "" : searchModel.ShutterNo) + ";" +
                                    (searchModel.PosLine == null ? "" : searchModel.PosLine) + ";" +
                                    (searchModel.PartNo == null ? "" : searchModel.PartNo) + ";" +
                                    (searchModel.Unique == null ? "" : searchModel.Unique) + ";" +
                                    (searchModel.BoxNo == null ? "" : searchModel.BoxNo) + ";" +
                                    (searchModel.ProdType == null ? "" : searchModel.ProdType) + ";" +
                                    (searchModel.PLaneAddress == null ? "" : searchModel.PLaneAddress) + ";" +
                                    (searchModel.Supplier == null ? "" : searchModel.Supplier) + ";" +
                                    (searchModel.Case == null ? "" : searchModel.Case);
                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);

                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}