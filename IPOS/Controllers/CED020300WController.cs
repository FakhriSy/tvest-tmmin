﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.CED020300W;
using IPOS.Models.Common;

namespace IPOS.Controllers
{
    public class CED020300WController : CommonController
    {
        string modulID = "02";
        string functionID = "CED020300W";
        public User user { get { return Lookup.Get<User>(); } }

        protected override void Startup()
        {
            Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
            var viewModel = new GenericModel<CED020300W>();
            viewModel.DataList = null;
            viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
            viewModel.GridPaging.FunctionID = functionID;

            Model = viewModel;
        }

        public ActionResult CED020300WSearchNoData(CED020300WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED020300W>();
                viewModel.DataList = null;
                viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED020300WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public ActionResult CED020300WSearchData(CED020300WSearchModel searchModel)
        {
            try
            {
                var viewModel = new GenericModel<CED020300W>();
                viewModel.DataList = CED020300WRepository.Instance.SearchData(searchModel);
                viewModel.GridPaging = new Paging(CED020300WRepository.Instance.SearchCountData(searchModel), searchModel.CurrentPage, searchModel.DataPerPage);
                viewModel.GridPaging.FunctionID = functionID;

                return PartialView("CED020300WGrid", viewModel);
            }
            catch (Exception e)
            {
                //return Json(e.Message);
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult CED020300WDownloadExcel(CED020300WSearchModel searchModel)
        {
            try
            {
                string functbatch = "CED020300W";
                string parameter = searchModel.VanningPlant+";"+searchModel.VanningDate;

                string functscreen = "CED020300W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();

                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);
                parameter = "";
                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult CED020300WReleaseDVP(CED020300WSearchModel searchModel)
        {
            try
            {
                string functbatch = "CED010611BBO";
                

                string functscreen = "CED020300W";
                string username = user.Username;
                string userlocation = "TMMIN";
                string processid = CommonRepository.Instance.GetProcessId();

                DateTime date;
                string month = "";

                if (DateTime.TryParseExact(searchModel.VanningDate, "dd-MMM-yyyy", null, System.Globalization.DateTimeStyles.None, out date))
                {
                    // Access the month
                    month = date.ToString("yyyyMM");
                }

                string parameter = searchModel.VanningPlant + ";" + month;

                string returnVal = CommonRepository.Instance.SendParamBatch(functbatch, functscreen, username, userlocation, processid, parameter);
                parameter = "";
                return Json(new { result = returnVal });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        [HttpPost]
        public JsonResult CED020300WCreateDVP(CED020300WSearchModel searchModel)
        {
            try
            {
                return Json(new resultMessage {  });
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }
    }
}