﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Web.Platform;
using Toyota.Common;

namespace GEMAS.Controllers
{
    public class ChangePasswordController : PageController
    {
        protected override void Startup()
        {
            Settings.Title = "Change Password";  
        }

        //public ActionResult CheckCommonPassword(string newpassword, string confirmpassword)
        //{
        //    Result msg = TDKUtility.Validate.Instance.IsValidPassword
        //        (
        //            Lookup.Get<Toyota.Common.Credential.User>().Username,
        //            Lookup.Get<Toyota.Common.Credential.User>().Password,
        //            newpassword,
        //            confirmpassword
        //        ); 
        //    return Json(msg, JsonRequestBehavior.AllowGet);
        //}


        public ActionResult CheckCommonEmail(string email)
        {
            bool isValidMail = TDKUtility.Validate.Instance.IsValidEmail(email);
            return Json(isValidMail, JsonRequestBehavior.AllowGet);
        }

    }
}
