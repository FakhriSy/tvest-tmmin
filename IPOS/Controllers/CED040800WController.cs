﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED040800W;
using System.Web.Mvc;
using System.IO;
using System.Drawing;
using System.Drawing.Imaging;

namespace IPOS.Controllers
{
    public class CED040800WController : Controller //CommonController
    {
        string modulID = "04";
        string functionID = "CED040800W";
        //public User user { get { return Lookup.Get<User>(); } }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    string kanbanid = Request.QueryString["kanban"].ToString();
        //    string kanbantype = Request.QueryString["kanbanType"].ToString();
        //    var viewModel = new GenericModel<CED040800W>();
        //    viewModel.DataList = CED040800WRepository.Instance.SearchData(kanbanid, kanbantype);
        //    ViewBag.PathImageServer = GetSystemMasterValue("CED040800W", "IMG_PATH", "VIEW");
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    Model = viewModel;
        //}

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);

            //FID.Ridwan : 2020-01-07
            var userName = "";
            HttpCookie aCookie = Request.Cookies["nameInput"];
            if (aCookie != null)
            {
                userName = Server.UrlDecode(aCookie.Value);
            }
            else
            {
                userName = "TVEST";
            }
            ViewData["UserName"] = userName;

            string kanbanid = Request.QueryString["kanban"].ToString();
            string kanbantype = Request.QueryString["kanbanType"].ToString();
            ViewData["RetriveData"] = CED040800WRepository.Instance.SearchData(kanbanid, kanbantype);
            ViewBag.PathImageServer = GetSystemMasterValue("CED040800W", "IMG_PATH", "VIEW");
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            return View("CED040800W");
        }

        public JsonResult getDataGrid(string kanbanId)
        {
            IList<CED040800WGrid> result = CED040800WRepository.Instance.SearchDataGrid(kanbanId);
            return Json(result);
        }

        public JsonResult getDataDefect()
        {
            IList<Defect> result = CED040800WRepository.Instance.GetDefect();
            return Json(result);
        }

        public JsonResult getDataSubDefect(string defectcd)
        {
            IList<SubDefect> result = CED040800WRepository.Instance.GetSubDefect(defectcd);
            return Json(result);
        }

        public ActionResult ProcessReject(string problemId)
        {
            string UserName = "TvestAdmin";
            string result = CED040800WRepository.Instance.ProcessReject(problemId, UserName);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public ActionResult ProcessIssue(string problemId, string mainDefect, string subDefect, string kanbanId, string problemImg1, string problemImg2, string type, string kanbanType, string lastChangeDate, string pos)
        {
            string pathDestination = GetSystemMasterValue("CED040800W", "IMG_PATH", "FOLDER");
            string folderYear = DateTime.Now.ToString("yyyy");
            string folderMonth = DateTime.Now.ToString("MM");
            pathDestination = pathDestination + folderYear + "/" + folderMonth + "/";
            string pathTemp = Server.MapPath("~/UploadTemp/ImgProblem");
            string savedImgProblem1 = Path.Combine(pathTemp, problemImg1);
            string savedImgProblem2 = Path.Combine(pathTemp, problemImg2);
            //string ftp_server = "ftp://10.165.8.80/IMAGE";
            //string ftp_user = "ipoms";
            //string ftp_pass = "1234";
            //ftp ftpClient = new ftp(ftp_server, ftp_user, ftp_pass);
            string thumbImgProblem1 = "";
            string thumbImgProblem2 = "";
            if (problemImg1 != "")
            {
                if (!Directory.Exists(pathDestination))
                {
                    Directory.CreateDirectory(pathDestination);
                }

                if (System.IO.File.Exists(pathDestination + problemImg1))
                {
                    System.IO.File.Delete(pathDestination + problemImg1);
                }
                System.IO.File.Copy(savedImgProblem1, pathDestination + problemImg1);
                //ftpClient.upload(problemImg1, savedImgProblem1);
                byte[] data = System.IO.File.ReadAllBytes(savedImgProblem1);
                Image img = byteArrayToImage(data);
                byte[] dataThumb = MakeThumbnail(data, img.Width / 2, img.Height / 2);
                MemoryStream msThumb = new MemoryStream(dataThumb);
                FileStream fsThumb = new FileStream(Path.Combine(pathTemp, "thumb_" + problemImg1), FileMode.Create);
                msThumb.WriteTo(fsThumb);
                msThumb.Close();
                fsThumb.Close();
                fsThumb.Dispose();

                byte[] dataThumbString = System.IO.File.ReadAllBytes(Path.Combine(pathTemp, "thumb_" + problemImg1));
                thumbImgProblem1 = Convert.ToBase64String(dataThumbString);
            }
            if (problemImg2 != "")
            {
                if (!Directory.Exists(pathDestination))
                {
                    Directory.CreateDirectory(pathDestination);
                }

                if (System.IO.File.Exists(pathDestination + problemImg2))
                {
                    System.IO.File.Delete(pathDestination + problemImg2);
                }
                System.IO.File.Copy(savedImgProblem2, pathDestination + problemImg2);
                //ftpClient.upload(problemImg2, savedImgProblem2);
                byte[] data = System.IO.File.ReadAllBytes(savedImgProblem2);
                Image img = byteArrayToImage(data);
                byte[] dataThumb = MakeThumbnail(data, img.Width / 2, img.Height / 2);
                MemoryStream msThumb = new MemoryStream(dataThumb);
                FileStream fsThumb = new FileStream(Path.Combine(pathTemp, "thumb_" + problemImg2), FileMode.Create);
                msThumb.WriteTo(fsThumb);
                msThumb.Close();
                fsThumb.Close();
                fsThumb.Dispose();

                byte[] dataThumbString = System.IO.File.ReadAllBytes(Path.Combine(pathTemp, "thumb_" + problemImg2));
                thumbImgProblem2 = Convert.ToBase64String(dataThumbString);
            }

            if (System.IO.File.Exists(Path.Combine(pathTemp, problemImg1)))
            {
                System.IO.File.Delete(Path.Combine(pathTemp, problemImg1));
            }
            if (System.IO.File.Exists(Path.Combine(pathTemp, problemImg2)))
            {
                System.IO.File.Delete(Path.Combine(pathTemp, problemImg2));
            }
            if (System.IO.File.Exists(Path.Combine(pathTemp, "thumb_" + problemImg1)))
            {
                System.IO.File.Delete(Path.Combine(pathTemp, "thumb_" + problemImg1));
            }
            if (System.IO.File.Exists(Path.Combine(pathTemp, "thumb_" + problemImg2)))
            {
                System.IO.File.Delete(Path.Combine(pathTemp, "thumb_" + problemImg2));
            }

            if (problemImg1 != "")
            {
                problemImg1 = folderYear + "/" + folderMonth + "/" + problemImg1;
            }

            if (problemImg2 != "")
            {
                problemImg2 = folderYear + "/" + folderMonth + "/" + problemImg2;
            }
            string UserName = "TvestAdmin";
            string result = CED040800WRepository.Instance.ProcessIssue(problemId, mainDefect, subDefect, kanbanId, problemImg1, problemImg2, thumbImgProblem1, thumbImgProblem2, type, UserName, kanbanType, lastChangeDate, pos);
            var e = result.Split('|');

            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] + "|" + e[2] });
        }

        private static byte[] MakeThumbnail(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            var jpegQuality = 50;
            Image image;
            using (var inputStream = new MemoryStream(myImage))
            {
                image = Image.FromStream(new MemoryStream(myImage)).GetThumbnailImage(thumbWidth, thumbHeight, null, new IntPtr());
                var jpegEncoder = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, jpegQuality);
                Byte[] outputBytes;
                using (var outputStream = new MemoryStream())
                {
                    image.Save(outputStream, jpegEncoder, encoderParameters);
                    return outputBytes = outputStream.ToArray();
                }
            }
        }

        private static Image byteArrayToImage(byte[] bytesArr)
        {
            MemoryStream memstr = new MemoryStream(bytesArr);
            Image img = Image.FromStream(memstr);
            return img;
        }

        public JsonResult ProcessIssueDetail(List<CED040800WGrid> list)
        {
            string result = "I|Not Found|";
            var e = result.Split('|');
            string currentuser = "TvestAdmin";
            if (list != null)
            {
                foreach (var dataForm in list)
                {
                    CED040800WGrid data = new CED040800WGrid();
                    data.ProblemId = dataForm.ProblemId;
                    data.KanbanId = dataForm.KanbanId;
                    data.Qty = dataForm.Qty;
                    data.LastPosition = dataForm.LastPosition;
                    data.Line = dataForm.Line;
                    data.UserName = dataForm.UserName;
                    result = CED040800WRepository.Instance.ProcessIssueDetail(data, currentuser);
                    e = result.Split('|');
                }
            }
            return Json(new resultMessage { MSG_TYPE = e[0], MSG_TXT = e[1] });
        }

        public JsonResult getListKanban(string partNo, string KanbanId)
        {
            IList<CED040800WGrid> result = CED040800WRepository.Instance.GetListKanban(partNo, KanbanId);
            return Json(result);
        }

        public JsonResult getDataAddKanban(string kanbanId)
        {
            IList<CED040800WGrid> result = CED040800WRepository.Instance.SearchDataAddKanban(kanbanId);
            return Json(result);
        }

        public JsonResult UploadImageProblem()
        {
            var msg = "";
            foreach (string file in Request.Files)
            {
                HttpPostedFileBase hpf = Request.Files[file] as HttpPostedFileBase;
                if (hpf.ContentLength == 0)
                    continue;

                string savedFileName = Path.Combine(Server.MapPath("~/UploadTemp/ImgProblem"), Path.GetFileName(hpf.FileName));
                hpf.SaveAs(savedFileName);
                msg = hpf.FileName;
            }
            return Json(msg, JsonRequestBehavior.AllowGet);
        }

        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }
    }
}