﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using IPOS.Controllers;
using IPOS.Controllers.Util;
using IPOS.Models.Common;
using Toyota.Common.Credential;
using Toyota.Common.Web.Platform;
using Toyota.Common.Database;
using IPOS.Models.CED040600W;

namespace IPOS.Controllers
{
    public class CED040600WController : Controller //CommonController
    {
        string modulID = "04";
        string functionID = "CED040600W";
        //public User user { get { return Lookup.Get<User>(); } }

        //protected override void Startup()
        //{
        //    Settings.Title = CommonRepository.Instance.GetFunctionName(modulID, functionID);
        //    var viewModel = new GenericModel<CED040600W>();
        //    viewModel.DataList = null;
        //    viewModel.GridPaging = new Paging(0, 1, Paging.DefaultDataPerPage);
        //    viewModel.GridPaging.FunctionID = "CED040600W";
        //    ViewBag.PathQKP = GetSystemMasterValue("CED040600W", "IMAGE", "QKPImage");
        //    ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

        //    Model = viewModel;
        //}

        public ActionResult Index()
        {
            ViewData["SettingsTitle"] = CommonRepository.Instance.GetFunctionName(modulID, functionID);

            //FID.Ridwan : 2020-01-07
            var userName = "";
            HttpCookie aCookie = Request.Cookies["nameInput"];
            if (aCookie != null)
            {
                userName = Server.UrlDecode(aCookie.Value);
            }
            else
            {
                userName = "TVEST";
            }
            
            ViewData["UserName"] = userName;

            ViewBag.PathQKP = GetSystemMasterValue("CED040600W", "IMAGE", "QKPImage");
            ViewBag.Shift = CommonRepository.Instance.GetShiftName(DateTime.Now.ToString("HH:mm"));

            return View("CED040600W");
        }

        [HttpPost]
        public ActionResult CED040600WSearchData()
        {
            try
            {
                return PartialView("CED040600WGrid", null);
            }
            catch (Exception e)
            {
                return Json(new resultMessage { MSG_TYPE = "E", MSG_TXT = e.Message });
            }
        }

        public JsonResult getDataKanban(string kup)
        {
            IList<CED040600W> result = CED040600WRepository.Instance.SearchData(kup);
            return Json(result);
        }

        public JsonResult getDataGrid1(string DiNo)
        {
            IList<CED040600WGrid1> result = CED040600WRepository.Instance.SearchDataGrid1(DiNo);
            return Json(result);
        }

        public static String GetSystemMasterValue(String sysCat, String sysSubCat, String sysCd)
        {
            try
            {
                String result = CommonRepository.Instance.getSystemMasterValue(sysCat, sysSubCat, sysCd);

                return result;
            }
            catch (Exception e)
            {
                return e.Message;
            }
        }

        public JsonResult FinishProcess(string KanbanId, string StartTime, string POS)
        {
            HttpCookie aCookie = Request.Cookies["noregInput"];
            var userName = aCookie.Value;
            string result = CED040600WRepository.Instance.FinishProcess(KanbanId, StartTime, userName, POS);
            return Json(result);
        }

    }
}