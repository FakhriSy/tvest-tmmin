﻿-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
BEGIN TRY
	DECLARE
		@@QUERY VARCHAR(MAX)
	,@@MANIFEST_NO VARCHAR(25)
	,@@ORDER_NO VARCHAR(25)
	,@@PART_NO VARCHAR(25)
	,@@UNIQUE VARCHAR(25)
	,@@ORDER_QTY decimal(18,0)
	,@@RECEIVE_QTY decimal(18,0)
	,@@ROW_START VARCHAR(25)
	,@@ROW_END VARCHAR(25)
	-- Message
	,@@MSG_TXT VARCHAR(MAX)
	,@@MSG_TYPE CHAR
	,@@MANIFEST_PARAM VARCHAR(25)
		
	SET @@MANIFEST_NO = RTRIM(LTRIM(@MANIFEST_NO));
	SET @@ORDER_NO = RTRIM(LTRIM(@ORDER_NO));
	SET @@PART_NO = RTRIM(LTRIM(@PART_NO));
	SET @@UNIQUE = RTRIM(LTRIM(@UNIQUE));
	SET @@ROW_START = @ROW_START;
	SET @@ROW_END = @ROW_END;
	
	SET @@MANIFEST_PARAM = @@MANIFEST_NO

	IF (LEN(@@MANIFEST_NO) > 10)
	BEGIN
		SELECT TOP 1 @@MANIFEST_PARAM = X.MANIFEST_NO FROM TB_R_DAILY_ORDER_MANIFEST X, TB_R_DAILY_ORDER_PART Y WHERE X.MANIFEST_NO=Y.MANIFEST_NO AND Y.PART_BARCODE = @@MANIFEST_NO ORDER BY X.ORDER_RELEASE_DT DESC
	END

	IF EXISTS (SELECT A.MANIFEST_NO FROM TB_R_DAILY_ORDER_MANIFEST A LEFT JOIN TB_R_DAILY_ORDER_PART B ON A.MANIFEST_NO=B.MANIFEST_NO WHERE (A.MANIFEST_NO = @@MANIFEST_NO OR B.PART_BARCODE = @@MANIFEST_NO) AND A.MANIFEST_NO = @@MANIFEST_PARAM AND A.MANIFEST_STS NOT IN (1, 3))
	BEGIN
		SELECT *
		FROM (SELECT ROW_NUMBER() OVER(ORDER BY A.MANIFEST_NO,A.KANBAN_NO asc) ROW_NO, A.PART_NO, B.PART_NAME, A.KANBAN_NO, SUM(A.ORDER_QTY) ORDER_QTY, SUM(ISNULL(A.RECEIVE_QTY, 0)) RECEIVE_QTY, ISNULL(A.FLAG_STATUS, '99') + '|' + 
		
		CASE WHEN (
					SELECT COUNT(1) FROM 
						TB_P_MANIFEST_KANBAN_RCV_H X, 
						TB_M_SYSTEM Y 
					WHERE	(isnull(X.DOCK_CD,'')=isnull(Y.SYS_CD,''))  
							AND Y.SYS_CAT='CED031500W' 
							AND Y.SYS_SUB_CAT='DOCK_CD_CASE' 
							AND X.MANIFEST_NO=A.MANIFEST_NO) > 0 
							THEN '0' ELSE '1' END CHECK_STATUS, 
		
		
		CASE WHEN ISNULL(C.SUP_COMPLETENESS_STATUS,'0') = '0' THEN 'ORI' ELSE 'PARK' END CHECK_MANIFEST,ISNULL(C.SUP_COMPLETENESS_STATUS,'0') SUP_COMPLETENESS_STATUS,
		SUM(CASE WHEN COALESCE(PROBLEM_TYPE,A.SHORTAGE_FLAG) = '1' THEN ISNULL(A.ORDER_QTY, 0) ELSE 0 END) SHORTAGE_QTY,
		SUM(CASE WHEN COALESCE(PROBLEM_TYPE,A.SHORTAGE_FLAG) = '2' THEN ISNULL(A.ORDER_QTY, 0) ELSE 0 END) MISSPART_QTY,
		SUM(CASE WHEN COALESCE(PROBLEM_TYPE,A.SHORTAGE_FLAG) = '3' THEN ISNULL(A.ORDER_QTY, 0) ELSE 0 END) DAMAGE_QTY
			FROM TB_P_MANIFEST_KANBAN_RCV_D A
				LEFT JOIN TB_M_PART B ON A.PART_NO = B.PART_NO
				LEFT JOIN TB_R_DAILY_ORDER_MANIFEST C ON A.MANIFEST_NO = C.MANIFEST_NO
			WHERE (A.MANIFEST_NO = @@MANIFEST_NO OR A.PART_BARCODE = @@MANIFEST_NO) AND A.MANIFEST_NO = @@MANIFEST_PARAM --AND A.KANBAN_STS IN ('0', '1')
				AND (ISNULL(@@PART_NO,'')='' OR A.PART_NO LIKE '%'+@@PART_NO+'%')
				AND (ISNULL(@@UNIQUE,'')='' OR A.KANBAN_NO LIKE '%'+@@UNIQUE+'%')
			GROUP BY A.MANIFEST_NO, A.PART_NO, B.PART_NAME, A.KANBAN_NO, A.FLAG_STATUS, A.DOCK_CD, ISNULL(C.SUP_COMPLETENESS_STATUS,'0')
		)TB --WHERE TB.ROW_NO BETWEEN @@ROW_START AND @@ROW_END
	END
	ELSE
	BEGIN
		SELECT *
		FROM (SELECT ROW_NUMBER() OVER(ORDER BY MANIFEST_NO,A.KANBAN_NO asc) ROW_NO, A.PART_NO, B.PART_NAME, A.KANBAN_NO, SUM(A.ORDER_QTY) ORDER_QTY, SUM(ISNULL(A.RECEIVE_QTY, 0)) RECEIVE_QTY, '-|' + 
		CASE WHEN (
					SELECT COUNT(1) FROM 
						TB_R_DAILY_ORDER_MANIFEST X, 
						TB_M_SYSTEM Y 
					WHERE	(isnull(X.DOCK_CD,'')=isnull(Y.SYS_CD,''))  
							AND Y.SYS_CAT='CED031500W' 
							AND Y.SYS_SUB_CAT='DOCK_CD_CASE' 
							AND X.MANIFEST_NO=A.MANIFEST_NO) > 0 
							THEN '0' ELSE '1' END CHECK_STATUS, 'ORI' CHECK_MANIFEST,
		SUM(CASE WHEN SHORTAGE_FLAG = '1' THEN ISNULL(A.ORDER_QTY, 0) ELSE 0 END) SHORTAGE_QTY,
		SUM(CASE WHEN SHORTAGE_FLAG = '2' THEN ISNULL(A.ORDER_QTY, 0) ELSE 0 END) MISSPART_QTY,
		SUM(CASE WHEN SHORTAGE_FLAG = '3' THEN ISNULL(A.ORDER_QTY, 0) ELSE 0 END) DAMAGE_QTY
			FROM TB_R_DAILY_ORDER_PART A
				LEFT JOIN TB_M_PART B ON A.PART_NO = B.PART_NO
			WHERE (A.MANIFEST_NO = @@MANIFEST_NO OR A.PART_BARCODE = @@MANIFEST_NO) AND A.MANIFEST_NO = @@MANIFEST_PARAM AND A.PART_BARCODE IS NOT NULL --AND A.KANBAN_STS IN ('0', '1')
				AND (ISNULL(@@PART_NO,'')='' OR A.PART_NO LIKE '%'+@@PART_NO+'%')
				AND (ISNULL(@@UNIQUE,'')='' OR A.KANBAN_NO LIKE '%'+@@UNIQUE+'%')
			GROUP BY A.MANIFEST_NO, A.PART_NO, B.PART_NAME, A.KANBAN_NO, A.DOCK_CD
		)TB --WHERE TB.ROW_NO BETWEEN @@ROW_START AND @@ROW_END
	END
END TRY
	
BEGIN CATCH
	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
	SET @@MSG_TYPE = 'E'

	SELECT @@MSG_TYPE+'|'+@@MSG_TXT
END CATCH