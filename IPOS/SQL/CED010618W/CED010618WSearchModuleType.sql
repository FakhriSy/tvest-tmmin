﻿/*SET NOCOUNT ON;
BEGIN TRY
DECLARE @@MSG_TXT VARCHAR(MAX), @@MSG_TYPE CHAR */

	SELECT *
		FROM (
			SELECT ROW_NUMBER() OVER (
					ORDER BY A.MOD_ID ASC
					) AS No
				,[MOD_ID]
				,[MODUL_TYPE_CONV]
				,[CREATED_BY]
				,[CREATED_DT]
				,[CHANGED_BY]
				,[CHANGED_DT]
			FROM [dbo].[TB_M_MODULE_TYPE] A
			WHERE ISNULL(@MOD_ID,'') = '' OR  A.MOD_ID LIKE '%' +  @MOD_ID + '%'
			) TB
		WHERE TB.No BETWEEN @ROW_START
				AND @ROW_END

/*END TRY
	
BEGIN CATCH
	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
	SET @@MSG_TYPE = 'E'

	SELECT @@MSG_TYPE+'|'+@@MSG_TXT
END CATCH*/