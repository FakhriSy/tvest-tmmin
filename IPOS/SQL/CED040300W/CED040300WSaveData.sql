﻿DECLARE @@trxId bigint
SELECT @@trxId = COUNT(1) + 1 FROM tbl_Trx

INSERT INTO tbl_Trx 
	(lngTrxId, dtmDateTime, lngDIBP, strLine, strPatt, strUnique, strPartNo, lngQtyBox, lngCT, lngActCT)
VALUES 
	(@@trxId, GetDate(), @lngDIBP, @strLine, @strPatt, @strUnique, @strPartNo, @lngQtyBox, @lngCT, @lngActCT)

SELECT 'I|Data is updated successfully.'