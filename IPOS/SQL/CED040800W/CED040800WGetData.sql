﻿/*IF @KanbanType = 'Kanban 1-4'*/
IF @KanbanType = 'Kanban 4'
BEGIN
	SELECT A.PROBLEM_ID ProblemId
		,FORMAT(A.CHANGED_DT, 'yyyy-MM-dd HH:mm:ss') LastChangeDate
		,A.DEFECT_CD MainDefect
		,A.SUB_DEFECT_CD SubDefect
		,Y.UNIQUE_NO UniqueNo
		,X.PART_NO PartNo
		,Z.PART_NAME PartName
		,Y.SUPPLIER_CD + '|' + ISNULL(SS.SUPPLIER_ABBREVIATION,'') + ' - ' + Y.SUPPLIER_PLANT_CD Supplier
		,CONVERT(VARCHAR,COUNT(X.PART_BARCODE)) TotalKanban
		,CONVERT(VARCHAR,SUM(X.BOX_QTY)) TotalPcs
		,A.PROBLEM_IMAGE_1 ImgProblem1
		,A.PROBLEM_IMAGE_2 ImgProblem2
	FROM TB_R_PACKING_D X WITH (NOLOCK)
	LEFT JOIN TB_M_PART_UNIQUE Y WITH (NOLOCK) ON X.PART_NO = Y.PART_NO
		AND X.KANBAN_NO = Y.UNIQUE_NO
		AND X.SUPPLIER_CD = Y.SUPPLIER_CD
		AND X.SUPPLIER_PLANT_CD = Y.SUPPLIER_PLANT_CD
		AND Y.DT_TO = (
			SELECT MAX(DT_TO)
			FROM TB_M_PART_UNIQUE WITH (NOLOCK)
			WHERE PART_NO = Y.PART_NO
				AND UNIQUE_NO = Y.UNIQUE_NO
				AND SUPPLIER_CD = Y.SUPPLIER_CD
				AND SUPPLIER_PLANT_CD = Y.SUPPLIER_PLANT_CD 
			)
	LEFT JOIN TB_M_PART Z WITH (NOLOCK) ON X.PART_NO = Z.PART_NO
	--LEFT JOIN TB_R_PROBLEM_H A ON X.PART_BARCODE = A.INITIAL_PART_BARCODE
	LEFT JOIN TB_R_PROBLEM_D PD WITH (NOLOCK) ON PD.PART_BARCODE = X.PART_BARCODE
	LEFT JOIN TB_R_PROBLEM_H A WITH (NOLOCK) ON A.PROBLEM_ID = PD.PROBLEM_ID
		AND A.PROBLEM_DT = (
			SELECT MAX(PROBLEM_DT)
			FROM TB_R_PROBLEM_H HH WITH (NOLOCK)
			WHERE HH.PROBLEM_ID = A.PROBLEM_ID
				AND PROBLEM_STS NOT IN (
					'4'
					,'5'
					)
			)
	LEFT JOIN TB_M_SUPPLIER SS WITH (NOLOCK) ON Y.SUPPLIER_CD = SS.SUPPLIER_CD
		AND Y.SUPPLIER_PLANT_CD = SS.SUPPLIER_PLANT_CD
	WHERE X.PART_BARCODE = @KanbanId  AND LEFT(X.KANBAN_TYPE, 1) = 4
	GROUP BY A.PROBLEM_ID
		,A.CHANGED_DT
		,A.DEFECT_CD
		,A.SUB_DEFECT_CD
		,Y.UNIQUE_NO
		,X.PART_NO
		,Z.PART_NAME
		,Y.SUPPLIER_CD
		,Y.SUPPLIER_PLANT_CD
		,A.PROBLEM_IMAGE_1
		,A.PROBLEM_IMAGE_2
		,SS.SUPPLIER_ABBREVIATION
END

IF @KanbanType = 'Kanban 1-2'
BEGIN
	SELECT TOP 1 A.PROBLEM_ID ProblemId
		,FORMAT(A.CHANGED_DT, 'yyyy-MM-dd HH:mm:ss') LastChangeDate
		,A.DEFECT_CD MainDefect
		,A.SUB_DEFECT_CD SubDefect
		,Y.UNIQUE_NO UniqueNo
		,X.PART_NO PartNo
		,Z.PART_NAME PartName
		,Y.SUPPLIER_CD + '|' + ISNULL(SS.SUPPLIER_ABBREVIATION,'') + ' - ' + Y.SUPPLIER_PLANT_CD Supplier
		--,CONVERT(VARCHAR,COUNT(PD.PART_BARCODE)) TotalKanban
		,CONVERT(VARCHAR,(SELECT COUNT(1) FROM TB_R_PROBLEM_D DD WHERE A.PROBLEM_ID = DD.PROBLEM_ID)) TotalKanban
		,'0' TotalPcs
		,A.PROBLEM_IMAGE_1 ImgProblem1
		,A.PROBLEM_IMAGE_2 ImgProblem2
	FROM TB_R_DAILY_ORDER_PART X WITH (NOLOCK)
	JOIN TB_R_DAILY_ORDER_MANIFEST CC WITH (NOLOCK) ON X.MANIFEST_NO = CC.MANIFEST_NO
	LEFT JOIN TB_M_PART_UNIQUE Y WITH (NOLOCK) ON X.PART_NO = Y.PART_NO
		AND X.KANBAN_NO = Y.UNIQUE_NO
		AND CC.SUPPLIER_CD = Y.SUPPLIER_CD
		AND CC.SUPPLIER_PLANT_CD = Y.SUPPLIER_PLANT_CD
		AND Y.DT_TO = (
			SELECT MAX(DT_TO)
			FROM TB_M_PART_UNIQUE WITH (NOLOCK)
			WHERE PART_NO = Y.PART_NO
				AND UNIQUE_NO = Y.UNIQUE_NO
				AND SUPPLIER_CD = Y.SUPPLIER_CD
				AND SUPPLIER_PLANT_CD = Y.SUPPLIER_PLANT_CD 
			)
	LEFT JOIN TB_M_PART Z WITH (NOLOCK) ON X.PART_NO = Z.PART_NO
	--LEFT JOIN TB_R_PROBLEM_H A ON X.PART_BARCODE = A.INITIAL_PART_BARCODE
	LEFT JOIN TB_R_PROBLEM_D PD WITH (NOLOCK) ON PD.PART_BARCODE = X.PART_BARCODE
	LEFT JOIN TB_R_PROBLEM_H A WITH (NOLOCK) ON A.PROBLEM_ID = PD.PROBLEM_ID
		AND A.PROBLEM_DT = (
			SELECT MAX(PROBLEM_DT)
			FROM TB_R_PROBLEM_H HH WITH (NOLOCK)
			WHERE HH.PROBLEM_ID = A.PROBLEM_ID
				AND PROBLEM_STS NOT IN (
					'4'
					,'5'
					)
			)
	LEFT JOIN TB_M_SUPPLIER SS WITH (NOLOCK) ON Y.SUPPLIER_CD = SS.SUPPLIER_CD
		AND Y.SUPPLIER_PLANT_CD = SS.SUPPLIER_PLANT_CD
	WHERE X.PART_BARCODE = @KanbanId
		/*AND X.KANBAN_TYPE = '2'*/
	--GROUP BY A.PROBLEM_ID
	--	,A.CHANGED_DT
	--	,A.DEFECT_CD
	--	,A.SUB_DEFECT_CD
	--	,Y.UNIQUE_NO
	--	,X.PART_NO
	--	,Z.PART_NAME
	--	,Y.SUPPLIER_CD
	--	,Y.SUPPLIER_PLANT_CD
	--	,A.PROBLEM_IMAGE_1
	--	,A.PROBLEM_IMAGE_2
	--	,SS.SUPPLIER_ABBREVIATION
	ORDER BY CC.ARRIVAL_PLAN_DT DESC
END

IF @KanbanType = 'Kanban 3'
BEGIN
	SELECT A.PROBLEM_ID ProblemId
		,FORMAT(A.CHANGED_DT, 'yyyy-MM-dd HH:mm:ss') LastChangeDate
		,A.DEFECT_CD MainDefect
		,A.SUB_DEFECT_CD SubDefect
		,Y.UNIQUE_NO UniqueNo
		,X.PART_NO PartNo
		,Z.PART_NAME PartName
		,Y.SUPPLIER_CD + '|' + ISNULL(SS.SUPPLIER_ABBREVIATION,'') + ' - ' + Y.SUPPLIER_PLANT_CD Supplier
		,CONVERT(VARCHAR,COUNT(X.PART_BARCODE)) TotalKanban
		,CONVERT(VARCHAR,SUM(X.BOX_QTY)) TotalPcs
		,A.PROBLEM_IMAGE_1 ImgProblem1
		,A.PROBLEM_IMAGE_2 ImgProblem2
	FROM TB_R_PACKING_PROCESS X WITH (NOLOCK)
	LEFT JOIN TB_M_PART_UNIQUE Y WITH (NOLOCK) ON X.PART_NO = Y.PART_NO
		AND X.KANBAN_NO = Y.UNIQUE_NO
		AND X.SUPPLIER_CD = Y.SUPPLIER_CD
		AND X.SUPPLIER_PLANT_CD = Y.SUPPLIER_PLANT_CD
		AND Y.DT_TO = (
			SELECT MAX(DT_TO)
			FROM TB_M_PART_UNIQUE WITH (NOLOCK)
			WHERE PART_NO = Y.PART_NO
				AND UNIQUE_NO = Y.UNIQUE_NO
				AND SUPPLIER_CD = Y.SUPPLIER_CD
				AND SUPPLIER_PLANT_CD = Y.SUPPLIER_PLANT_CD 
			)
	LEFT JOIN TB_M_PART Z WITH (NOLOCK) ON X.PART_NO = Z.PART_NO
	--LEFT JOIN TB_R_PROBLEM_H A ON X.PART_BARCODE = A.INITIAL_PART_BARCODE
	LEFT JOIN TB_R_PROBLEM_D PD WITH (NOLOCK) ON PD.PART_BARCODE = X.PART_BARCODE
	LEFT JOIN TB_R_PROBLEM_H A WITH (NOLOCK) ON A.PROBLEM_ID = PD.PROBLEM_ID
		AND A.PROBLEM_DT = (
			SELECT MAX(PROBLEM_DT)
			FROM TB_R_PROBLEM_H HH WITH (NOLOCK)
			WHERE HH.PROBLEM_ID = A.PROBLEM_ID
				AND PROBLEM_STS NOT IN (
					'4'
					,'5'
					)
			)
	LEFT JOIN TB_M_SUPPLIER SS WITH (NOLOCK) ON Y.SUPPLIER_CD = SS.SUPPLIER_CD
		AND Y.SUPPLIER_PLANT_CD = SS.SUPPLIER_PLANT_CD
	WHERE X.PART_BARCODE = @KanbanId
	GROUP BY A.PROBLEM_ID
		,A.CHANGED_DT
		,A.DEFECT_CD
		,A.SUB_DEFECT_CD
		,Y.UNIQUE_NO
		,X.PART_NO
		,Z.PART_NAME
		,Y.SUPPLIER_CD
		,Y.SUPPLIER_PLANT_CD
		,A.PROBLEM_IMAGE_1
		,A.PROBLEM_IMAGE_2
		,SS.SUPPLIER_ABBREVIATION
END
