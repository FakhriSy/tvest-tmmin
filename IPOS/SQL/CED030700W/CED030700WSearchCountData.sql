﻿/*SET NOCOUNT ON;
BEGIN TRY
	DECLARE @@MSG_TXT VARCHAR(MAX), @@MSG_TYPE CHAR
	
	IF @Type = 'Summary'
	BEGIN
		SELECT COUNT(1) FROM (
			SELECT ROW_NUMBER() OVER(ORDER BY A.MANIFEST_NO ASC) No,A.ARRIVAL_PLAN_DT DeliveryDate, '' [Shift], F.PHYSICAL_DOCKCD PhysicalDock, 
				F.LOGICAL_DOCKCD LogicalDock, E.STATION TS, D.ROUTE [Route], D.RATE Cycle, D.LP_CD LP, A.SUPPLIER_CD + ' - ' + A.SUPPLIER_PLANT_CD SuppCode,
				G.SUPPLIER_NAME SuppName, A.ORDER_NO OrderNo, A.MANIFEST_NO ManifestNo, A.ARRIVAL_PLAN_DT PlanReceiveOriginal,
				A.ARRIVAL_ACTUAL_DT ActualReceive, '' ModType, B.MOD_DST_CD Dest, B.LOT_MOD_NO Lot, B.CASE_NO [Case],
				COUNT(B.PART_BARCODE) KanbanPlan, SUM(CASE WHEN B.RECEIVED_STATUS = 2 THEN 1 ELSE 0 END) KanbanAct,
				'' [Status], '' PlanCatchUp, '' Problem
			FROM TB_R_DAILY_ORDER_MANIFEST A
				LEFT JOIN TB_R_DAILY_ORDER_PART B ON A.MANIFEST_NO=B.MANIFEST_NO
				LEFT JOIN TB_R_DELIVERY_CTL_MANIFEST C ON A.MANIFEST_NO=B.MANIFEST_NO
				LEFT JOIN TB_R_DELIVERY_CTL_H D ON C.DELIVERY_NO=D.DELIVERY_NO
				LEFT JOIN TB_R_DELIVERY_CTL_D E ON D.DELIVERY_NO=E.DELIVERY_NO
				LEFT JOIN TB_M_PHYSICAL_DOCK_MAPPING F ON A.DOCK_CD=F.DOCK_CD
				LEFT JOIN TB_M_SUPPLIER G ON A.SUPPLIER_CD=G.SUPPLIER_CD AND A.SUPPLIER_PLANT_CD=G.SUPPLIER_PLANT_CD
			WHERE 1 = 1
				AND (ISNULL(@DeliveryFrom,'')='' OR CONVERT(VARCHAR(10), A.ARRIVAL_PLAN_DT, 101) BETWEEN @DeliveryFrom AND @DeliveryTo)
			--	AND (ISNULL(@Shift,'')='' OR PART_NO LIKE '%'+@Shift+'%')
				AND (ISNULL(@PhysicalDock,'')='' OR F.PHYSICAL_DOCKCD LIKE '%'+@PhysicalDock+'%')
				AND (ISNULL(@RouteName,'')='' OR D.ROUTE LIKE '%'+@RouteName+'%')
				AND (ISNULL(@RouteCycle,'')='' OR D.RATE LIKE '%'+@RouteCycle+'%')
				AND (ISNULL(@LogicalDock,'')='' OR F.LOGICAL_DOCKCD LIKE '%'+@LogicalDock+'%')
				AND (ISNULL(@TS,'')='' OR E.STATION LIKE '%'+@TS+'%')
				AND (ISNULL(@Supplier,'')='' OR A.SUPPLIER_CD LIKE '%'+@Supplier+'%')
				AND (ISNULL(@ManifestNo,'')='' OR A.MANIFEST_NO LIKE '%'+@ManifestNo+'%')
				AND (ISNULL(@OrderNo,'')='' OR A.ORDER_NO LIKE '%'+@OrderNo+'%')
			--	AND (ISNULL(@Status,'')='' OR PART_NO LIKE '%'+@Status+'%')
				AND (ISNULL(@KanbanId,'')='' OR B.PART_BARCODE LIKE '%'+@KanbanId+'%')
				AND (ISNULL(@PartNo,'')='' OR B.PART_NO LIKE '%'+@PartNo+'%')
				AND (ISNULL(@Dest,'')='' OR B.MOD_DST_CD LIKE '%'+@Dest+'%')
				AND (ISNULL(@Lot,'')='' OR B.LOT_MOD_NO LIKE '%'+@Lot+'%')
				AND (ISNULL(@Case,'')='' OR B.CASE_NO LIKE '%'+@Case+'%')
			GROUP BY A.ARRIVAL_PLAN_DT, F.PHYSICAL_DOCKCD, F.LOGICAL_DOCKCD, E.STATION, D.ROUTE, D.RATE, D.LP_CD,
				A.SUPPLIER_CD, A.SUPPLIER_PLANT_CD, G.SUPPLIER_NAME, A.ORDER_NO, A.MANIFEST_NO, A.ARRIVAL_ACTUAL_DT, B.MOD_DST_CD,
				B.LOT_MOD_NO, B.CASE_NO
		) TB
	END
	ELSE IF @Type = 'Detail'
	BEGIN
		SELECT COUNT(1) FROM (
			SELECT * FROM (
			SELECT ROW_NUMBER() OVER(ORDER BY A.RTEGRPCD ASC) No,
				convert(varchar,FORMAT(A.RTEDATE,'dd-MMM-yyyy')) AS DeliveryDate
				,dbo.fn_GET_SHIFT(A.RTEDATE) AS [Shift]
				,(select PHYSICAL_DOCKCD from TB_M_PHYSICAL_DOCK_MAPPING where RCVCOMPDOCKCD=B.RCVCOMPDOCKCD) AS PhysicalDock
				,B.RCVCOMPDOCKCD AS LogicalDock
				,(select STATION from TB_M_DOCK_STATION_MAPPING where PHYSICAL_DOCK_CD=A.DOORCD AND TB_M_DOCK_STATION_MAPPING.LOGICAL_DOCK_CD = B.RCVCOMPDOCKCD) AS TS
				,A.RTEGRPCD AS [Route]
				,A.RUNSEQ AS Cycle
				,A.LOGPARTNERCD AS LP
				,B.SUPPCD+'-'+B.SUPPPLANTCD AS SuppCode
				,B.SUPPNAME AS SuppName
				,E.ORDER_NO AS OrderNo
				,E.MANIFEST_NO AS ManifestNo
				,F.PART_BARCODE AS KanbanId
				,F.PART_NO AS PartNo
				,convert(varchar,FORMAT(A.ARRVDATETIME,'dd-MMM-yyyy HH:mm')) AS PlanReceiveOriginal
				,CONVERT(VARCHAR,FORMAT(CAST(CONVERT(VARCHAR,H.UPLOAD_ARRIVAL_PLAN_DT) +' '+ LEFT(CONVERT(VARCHAR,H.UPLOAD_ARRIVAL_PLAN_TIME),8) AS DATETIME),'dd-MMM-yyyy HH:mm')) AS PlanReceiveUpload
				,convert(varchar,FORMAT(D.ARRIVAL_ACTUAL_DT,'dd-MMM-yyyy HH:mm')) AS ActualReceive
				,G.MOD_TYPE AS ModType
				,F.MOD_DST_CD AS Dest
				,F.LOT_MOD_NO AS Lot
				,F.CASE_NO AS [Case]
				,convert(varchar,F.ORDER_QTY) AS KanbanPlan
				,convert(varchar,F.RECEIVE_QTY) AS KanbanAct
				,CASE WHEN F.KANBAN_STS='0' THEN 'Not Receive' WHEN F.KANBAN_STS='1' THEN 'Partial Received' WHEN F.KANBAN_STS='2' THEN 'Full Received' END AS [Status]
				,convert(varchar,FORMAT(E.CATCHUP_PLAN,'dd-MMM-yyyy HH:mm')) AS PlanCatchUp
				,E.RCV_PROBLEM_DESC AS Problem 

			FROM TB_R_DCL_TLMS_DRIVER_DATA A																				
			LEFT JOIN TB_R_DCL_TLMS_ORDER_ASSIGNMENT B																				
				ON A.RTEGRPCD = B.RTEGRPCD																			
					AND A.RUNSEQ = B.RUNSEQ																		
					AND A.RTEDATE = B.RTEDATE																		
			LEFT JOIN TB_R_DELIVERY_CTL_H C																				
				ON A.RTEDATE = C.PICKUP_DT																			
					AND A.RTEGRPCD = C.ROUTE																		
					AND RIGHT('00' + A.RUNSEQ, 2) = C.RATE																		
			LEFT JOIN TB_R_DELIVERY_CTL_D D																				
				ON C.DELIVERY_NO = D.DELIVERY_NO																			
			LEFT JOIN TB_R_DAILY_ORDER_MANIFEST E																				
				ON left(B.ORDDATE,8)+RIGHT('00'+CAST(B.ORDSEQ as varchar(2)),2)																			
						= E.ORDER_NO																	
					AND B.RCVCOMPDOCKCD = E.DOCK_CD																		
					AND B.SUPPCD = E.SUPPLIER_CD																		
					AND B.SUPPPLANTCD = E.SUPPLIER_PLANT_CD																		
			LEFT JOIN TB_R_DAILY_ORDER_PART F																				
				ON E.MANIFEST_NO = F.MANIFEST_NO																			
			LEFT JOIN TB_R_POS G																				
				ON F.CONT_SNO = G.CONTAINER_SNO																			
					AND F.LOT_MOD_NO = G.LOT_MODULE_NO																		
					AND F.CASE_NO = G.CASE_NO																		
					AND F.MOD_DST_CD = G.MODULE_DEST_CD	
			LEFT JOIN TB_R_OKAMOCHI_MODULE AS H
				ON E.SUPPLIER_CD=H.SUPPLIER_CD
					AND E.SUPPLIER_PLANT_CD=H.SUPPLIER_PLANT_CD
					AND F.MOD_DST_CD=H.DEST_CD
					AND F.LOT_MOD_NO=H.LOT_MOD_NO
					AND F.CASE_NO=H.CASE_NO
					AND F.CONT_SNO=H.CONT_SNO
					AND CONVERT(DATE,A.RTEDATE)=H.UPLOAD_ARRIVAL_PLAN_DT
			WHERE 1 = 1
				AND (ISNULL(@Supplier,'')='' OR B.SUPPCD LIKE '%'+@Supplier+'%')
			)AS ZZ
			WHERE (ISNULL(@DeliveryFrom,'')='' OR CONVERT(VARCHAR(10), ZZ.DeliveryDate, 101) BETWEEN @DeliveryFrom AND @DeliveryTo)
				AND (ISNULL(@Shift,'')='' OR ZZ.[Shift] LIKE '%'+@Shift+'%')
				AND (ISNULL(@PhysicalDock,'')='' OR ZZ.PhysicalDock LIKE '%'+@PhysicalDock+'%')
				AND (ISNULL(@RouteName,'')='' OR ZZ.[Route] LIKE '%'+@RouteName+'%')
				AND (ISNULL(@RouteCycle,'')='' OR ZZ.Cycle LIKE '%'+@RouteCycle+'%')
				AND (ISNULL(@LogicalDock,'')='' OR ZZ.LogicalDock LIKE '%'+@LogicalDock+'%')
				AND (ISNULL(@TS,'')='' OR ZZ.TS LIKE '%'+@TS+'%')
				AND (ISNULL(@ManifestNo,'')='' OR ZZ.ManifestNo LIKE '%'+@ManifestNo+'%')
				AND (ISNULL(@OrderNo,'')='' OR ZZ.OrderNo LIKE '%'+@OrderNo+'%')
				AND (ISNULL(@Status,'')='' OR ZZ.[Status] LIKE '%'+@Status+'%')
				AND (ISNULL(@KanbanId,'')='' OR ZZ.KanbanId LIKE '%'+@KanbanId+'%')
				AND (ISNULL(@PartNo,'')='' OR ZZ.PartNo LIKE '%'+@PartNo+'%')
				AND (ISNULL(@Dest,'')='' OR ZZ.Dest LIKE '%'+@Dest+'%')
				AND (ISNULL(@Lot,'')='' OR ZZ.Lot LIKE '%'+@Lot+'%')
				AND (ISNULL(@Case,'')='' OR ZZ.[Case] LIKE '%'+@Case+'%')
		) TB
	END

END TRY
	
BEGIN CATCH
	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
	SET @@MSG_TYPE = 'E'

	SELECT @@MSG_TYPE+'|'+@@MSG_TXT
END CATCH*/

EXEC [dbo].[CED030700W_CountData] @DeliveryFrom
	,@DeliveryTo
	,@ActualReceiveFrom
	,@ActualReceiveTo
	,@Shift
	,@PhysicalDock
	,@RouteName
	,@RouteCycle
	,@LogicalDock
	,@TS
	,@Supplier
	,@ManifestNo
	,@OrderNo
	,@Status
	,@KanbanId
	,@PartNo
	,@Dest
	,@Lot
	,@Case
	,@Type