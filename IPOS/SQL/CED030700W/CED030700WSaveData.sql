﻿/*SET NOCOUNT ON;
BEGIN TRY
	DECLARE @@PARAM VARCHAR(100), @@STS INT, @@MSG_TXT VARCHAR(MAX), @@MSG_TYPE CHAR, @@NR_ERR INT = 0
		
	-- Basic Validation --
	-- 1. Check Mandatory --
	EXEC SP_CheckMandatory 'Manifest No', @ManifestNo, @@STS OUTPUT, @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT
	IF (@@STS = 1)
	BEGIN
		GOTO ERR_FLAG
	END

	-- Insert data
	IF (@MODE = '1')
	BEGIN
		--insert into [table_name]
		EXEC SP_GetMessage 'MCEDSTD001I', @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT, @@NR_ERR OUTPUT
	END
	-- Edit data
	ELSE
	BEGIN
		IF @Type='Summary'
		BEGIN
			-- Business Validation --
			-- 1. Check Existing --      
			IF (EXISTS(SELECT TOP 1 1 FROM TB_R_DAILY_ORDER_MANIFEST WHERE MANIFEST_NO = @ManifestNo) AND @MODE = '1')
			BEGIN
				SET @@PARAM = 'Manifest No = '+@ManifestNo;
				EXEC SP_GetMessage 'MCEDSTD033E', @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT, @@NR_ERR OUTPUT, @@PARAM
				GOTO ERR_FLAG
			END

			-- 2. Check Concurrency --
			IF (NOT EXISTS(SELECT * FROM TB_R_DAILY_ORDER_MANIFEST WHERE MANIFEST_NO = @ManifestNo) AND @MODE = '2')
			BEGIN
				EXEC SP_GetMessage 'MCEDSTD034E', @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT, @@NR_ERR OUTPUT
				GOTO ERR_FLAG
			END
			--Update TB_R_DAILY_ORDER_MANIFEST
			UPDATE TB_R_DAILY_ORDER_MANIFEST SET
				CATCHUP_PLAN = convert(datetime,@PlanCatchUp,103)
				,RCV_PROBLEM_DESC = @Problem
				,CHANGED_BY = @USER_LOGIN
				,CHANGED_DATE = GETDATE()
			WHERE  MANIFEST_NO = @ManifestNo
		END
		ELSE
		BEGIN
		
			-- Business Validation --
			-- 1. Check Existing --      
			IF (EXISTS(SELECT TOP 1 1 FROM TB_R_DAILY_ORDER_PART WHERE MANIFEST_NO = @ManifestNo AND PART_BARCODE=@KanbanId) AND @MODE = '1')
			BEGIN
				SET @@PARAM = 'Manifest No = '+@ManifestNo;
				EXEC SP_GetMessage 'MCEDSTD033E', @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT, @@NR_ERR OUTPUT, @@PARAM
				GOTO ERR_FLAG
			END

			-- 2. Check Concurrency --
			IF (NOT EXISTS(SELECT * FROM TB_R_DAILY_ORDER_PART WHERE MANIFEST_NO = @ManifestNo AND PART_BARCODE=@KanbanId) AND @MODE = '2')
			BEGIN
				EXEC SP_GetMessage 'MCEDSTD034E', @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT, @@NR_ERR OUTPUT
				GOTO ERR_FLAG
			END
			--Update TB_R_DAILY_ORDER_PART
				UPDATE TB_R_DAILY_ORDER_PART SET
					CATCHUP_PLAN = convert(datetime,@PlanCatchUp,103)
					,RCV_PROBLEM_DESC = @Problem
					,CHANGED_BY = @USER_LOGIN
					,CHANGED_DT = GETDATE()
				WHERE  MANIFEST_NO = @ManifestNo
					AND PART_BARCODE=@KanbanId
		END
	
		EXEC SP_GetMessage 'MCEDSTD001I', @@MSG_TXT OUTPUT, @@MSG_TYPE OUTPUT, @@NR_ERR OUTPUT
	END

	GOTO END_FLAG
END TRY
	
BEGIN CATCH
	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
	SET @@MSG_TYPE = 'E'
END CATCH

ERR_FLAG:
END_FLAG:
	SELECT @@MSG_TYPE+'|'+@@MSG_TXT*/

EXEC [dbo].[CED030700W_SaveData] @ManifestNo
	,@PlanCatchUp
	,@Problem
	,@KanbanId
	,@USER_LOGIN
	,@Type
	,@MODE
	,@ItemParam
	,@ChangedDtParam