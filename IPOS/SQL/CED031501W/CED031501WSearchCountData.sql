﻿EXEC [dbo].[CED031501W_CountData]
	@ArrivalDateFrom
	,@ArrivalDateTo
	,@Supplier
	,@PhysicalDock
	,@RouteName
	,@RouteCycle
	,@DeliveryNo
	,@ManifestNo
	,@PartNo
	,@CompletenessStatus
	,@Username