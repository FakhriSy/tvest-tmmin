﻿EXEC [dbo].[CED031501W_SearchData]
	@ArrivalDateFrom
	,@ArrivalDateTo
	,@Supplier
	,@PhysicalDock
	,@RouteName
	,@RouteCycle
	,@DeliveryNo
	,@ManifestNo
	,@PartNo
	,@CompletenessStatus
	,@ROW_START
	,@ROW_END
	,@Username