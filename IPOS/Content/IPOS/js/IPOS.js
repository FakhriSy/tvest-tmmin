﻿/************************************************************************************************************
 * Program History :                                                                                        *
 *                                                                                                          *
 * Project Name     : IPOS Management System                                                              *
 * Client Name      : PT. TMMIN (Toyota Motor Manufacturing Indonesia)                                      *
 * Function Id      :                                                                                       *
 * Function Name    : IPOS main JS                                                                        *
 * Function Group   : IPOS                                                                                *
 * Program Id       : IPOS                                                                                *
 * Program Name     : IPOS main JS                                                                        *
 * Program Type     : JavaScript                                                                            *
 * Description      :                                                                                       *
 * Environment      : .NET 4.0, ASP MVC 4.0                                                                 *
 * Author           :                                                                                       *
 * Version          : 01.00.00                                                                              *
 * Creation Date    :                                                                                       *
 *                                                                                                          *
 * Update history		Re-fix date				Person in charge				Description					*
 *                                                                                                          * 
 * Copyright(C) 2018 - Fujitsu Indonesia. All Rights Reserved                                               *
 ************************************************************************************************************/

var loading;
function showLoading() {
    loading = bootbox.dialog({
        message: '<div class="text-center"><i class="fa fa-spin fa-spinner"></i> Loading...</div>',
        size: 'small',
        closeButton: false
    });
    return true;
}

function hideLoading() {
    loading.modal('hide');
}

function setEnableDisable(obj, mode) {
    // button - hide
    objButton = $(obj).find('button');
    $.each(objButton, function () {
        $(this).hide();
    });
    // button - unhide
    objButton = $(obj).find('button[class~="' + mode + '"]');
    $.each(objButton, function () {
        $(this).show();
    });

    // input for Textbox - disabled
    objInput = $(obj).find('input');
    $.each(objInput, function () {
        $(this).attr("disabled", true);
        $(this).css("cursor", "default");
    });

    // input for Combobox - disabled
    objInput = $(obj).find('select');
    $.each(objInput, function () {
        $(this).attr("disabled", true);
        $(this).css("background-color", "#eeeeee");
        $(this).css("color", "lightgray");
        $(this).css("cursor", "default");
    });

    // input for Textbox - enabled
    objInput = $(obj).find('input[class~="' + mode + '"]');
    $.each(objInput, function () {
        $(this).attr("disabled", false);
        if ($(this).is(":radio") || $(this).is(":checkbox")) {
            $(this).css("cursor", "default");
        } else {
            $(this).css("cursor", "text");
        };
    });

    // input for Combobox - enabled
    objInput = $(obj).find('select[class~="' + mode + '"]');
    $.each(objInput, function () {
        $(this).attr("disabled", false);
        $(this).css("background-color", "white");
        $(this).css("color", "#696969");
    });
}

function Key(e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 ||
        (e.keyCode == 65 && e.ctrlKey === true) ||
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        return;
    }
    if ((e.shiftKey || (e.keyCode < 48 || e.keyCode > 57)) && (e.keyCode < 96 || e.keyCode > 105)) {
        e.preventDefault();
    }
}

function format_number(number, prefix, thousand_separator, decimal_separator) {
    var thousand_separator = thousand_separator || ',',
		decimal_separator = decimal_separator || '.',
		regex = new RegExp('[^' + decimal_separator + '\\d]', 'g'),
		number_string = number.replace(regex, '').toString(),
		split = number_string.split(decimal_separator),
		rest = split[0].length % 3,
		result = split[0].substr(0, rest),
		thousands = split[0].substr(rest).match(/\d{3}/g);

    if (thousands) {
        separator = rest ? thousand_separator : '';
        result += separator + thousands.join(thousand_separator);
    }
    result = split[1] != undefined ? result + decimal_separator + split[1] : result;
    return prefix == undefined ? result : (result ? prefix + result : '');
};

function doRefreshComboBox(id, typeCombo, idCombo, paramCombo) {
    $.ajax({
        type: 'POST',
        url: "./Common/ComboBoxList",
        data: {
            typeComboBox: typeCombo,
            comboBoxID: idCombo,
            comboBoxParam: paramCombo
        },
        success: function (result) {
            $("#" + id).empty();
            $.each(result, function (i, result) {
                $("#" + id).append('<option value="' + result.Value + '">' + result.Text + '</option>');
            });
        },
        error: function (result) {
            alert("data error");
        }
    });
}

function doRefreshChildComboBox(idParent, idChild, typeCombo, idCombo) {
    var valParent = $("#" + idParent).val().split(':')[0];
    $.ajax({
        type: 'POST',
        url: "./Common/ComboBoxSelectListChild",
        data: {
            typeComboBox: typeCombo,
            comboBoxID: idCombo,
            comboBoxParent: valParent
        },
        success: function (result) {
            $("#" + idChild).empty();
            $.each(result, function (i, result) {
                $("#" + idChild).append('<option value="' + result.Value + '">' + result.Text + '</option>');
            });

            if (valParent != "" && result.length > 1) {
                $("#" + idChild).prop("disabled", false);
                $("#" + idChild).css("color", "#858585");
                $("#" + idChild).css("background-color", "#ffffff");
            }
            else {
                $("#" + idChild).prop("disabled", true);
                $("#" + idChild).css("color", "lightgray");
                $("#" + idChild).css("background-color", "#eeeeee");
            }

            $("#" + idChild).change();
        },
        error: function (result) {
            alert("data error");
        }
    });
}

function doRefreshChildTextBox(idParent, idChild) {
    var valParent = $("#" + idParent).val().split(':');
    if (valParent.length == 2) {
        $("#" + idChild).val(valParent[1]);
    }
    else {
        $("#" + idChild).val("");
    }
}

function disabledPaging(functionID) {
    $("[id^=lnk-page-" + functionID + "]").data("disabled", true);
    $("[id^=lnk-page-" + functionID + "]").addClass("_pagination-disabled");
    $("[id^=lnk-page-" + functionID + "]").css("color", "lightgray");
    $("[id^=lnk-page-" + functionID + "]").parent().removeClass("active");
    $("._pagination-label").css("color", "lightgray");
    //$("#input-page-" + functionID + "").prop("disabled", true);
    $("#btn-page-" + functionID + "-go").prop("disabled", true);
    $("#btn-page-" + functionID + "-go").css("color", "lightgray");
    //$("#cmd-page-" + functionID + "-size").prop("disabled", true);
    $("#cmd-page-" + functionID + "-size").css("color", "lightgray");
    $("[id^=lnk-page-" + functionID + "]").css("cursor", "default");
    $("._pagination-label").css("cursor", "default");
}

function removeMandatoryInput() {
    // for Textbox
    objInput = $('#tbTemplateDataRow #templateDataRow').find('input');
    for (i = 0; i < objInput.length; i++) {
        id_nm = objInput[i].getAttribute("id");
        if ($("#" + id_nm).is(":disabled")) {
            $("#" + id_nm).removeClass("mandatory");
        }
    }
    // for Combobox
    objInput = $('#tbTemplateDataRow #templateDataRow').find('select');
    for (i = 0; i < objInput.length; i++) {
        id_nm = objInput[i].getAttribute("id");
        if ($("#" + id_nm).is(":disabled")) {
            $("#" + id_nm).removeClass("mandatory");
        }
    }
}

function initialMode() {
    $('#grid').hide();
    setEnableDisable('.hidden-xs', 'eView');
    //Initial form Upload
    $('.input-ghost').val(null);
    $(".input-file").find('input').val('');
    //Initial datepicker
    $('.input-group.date').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        todayHighlight: true
    });
}

function clearMode() {
    $('#searchForm')[0].reset();
    $.each($('#tblScroll tbody').find('[id="DataRow"]'), function () {
        $(this).remove();
    });
    //$("#btnEdit").hide();
    //$("#btnDelete").hide();
    //$("#btnDownload").hide();
    $('.input-ghost').val(null);
    $(".input-file").find('input').val('');
}

function searchMode() {
    setEnableDisable('.hidden-xs', 'eView');
    //$("#btnEdit").show();
    //$("#btnDelete").show();
    //$("#btnDownload").show();
    $("#formUpload").show();
    $('.input-ghost').val(null);
    $(".input-file").find('input').val('');
}

function addMode(functionID) {
    $('#grid').show();
    var rowTemplate = $('#tbTemplateDataRow').html();
    $(".grid-checkbox-body").removeAttr("checked");
    $('#tblScroll tbody tr#noDataRow').remove();
    if ($('#tblScroll tbody tr:first').length > 0) {
        var newRow = $('#tblScroll tbody tr:first').before(rowTemplate);
    }
    else {
        var newRow = $('#tblScroll tbody').append(rowTemplate);
    }
    $('#mode').val(1);
    setEnableDisable('.hidden-xs', 'eAdd');
    $("#formUpload").hide();
    disabledPaging(functionID);
    //Initial datepicker
    $('.input-group.date').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        todayHighlight: true
    });
}

function editMode(functionID) {
    $('#mode').val(2);
    setEnableDisable('.hidden-xs', 'eEdit');
    $("#formUpload").hide();
    removeMandatoryInput();
    disabledPaging(functionID);

    $.each($('#tScrollBody').find('input:checked'),
        function () {
            var currRow = $(this).closest('tr[class~="tr-grid-detail"]');
            var rowIndex = $('#tScrollBody tbody tr').index(currRow);
            var rowTemplate = $('#tbTemplateDataRow').html();
            $(currRow).hide().after(rowTemplate.replace('templateDataRow', 'DataRow' + rowIndex));
            var newRow = $('#DataRow' + rowIndex);
            itemArray = [];
            objItem = $('#tScrollBody tbody tr:eq(' + rowIndex + ')').find('td');
            lenObj = objItem.length;
            for (i = 1; i < lenObj; i++) {
                itemArray[i] = objItem[i].innerText;
            };
            for (j = 1; j < lenObj; j++) {
                col_row = newRow.find('td')[j].childNodes;
                if (col_row.length == 0) {
                    tag_type = "LABEL";
                    id_nm = newRow.find('td')[j].getAttribute('id')
                } else {
                    tag_type = col_row[1].nodeName;
                    id_nm = col_row[1].getAttribute('id');
                }

                if (tag_type == "DIV" && id_nm == null) {
                    col_row_detail = col_row[1].childNodes;
                    tag_type = col_row_detail[3].nodeName;
                    id_nm = col_row_detail[3].getAttribute('id');
                    if (id_nm == null) {
                        tag_type = col_row_detail[1].nodeName;
                        id_nm = col_row_detail[1].getAttribute('id');
                    }
                }

                if (tag_type == "LABEL") {
                    $('#' + id_nm).text(itemArray[j]);
                }
                else if (tag_type == "SELECT") {
                    $('#' + id_nm).val(itemArray[j]);
                    $('#' + id_nm).find("option:contains('" + itemArray[j] + "')").each(function () {
                        if ($(this).text() == itemArray[j]) {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
                else {
                    $('#' + id_nm).val(itemArray[j]);
                }
            }
            itemArray = null;
            $(newRow).show();
        }
    );
    //Initial datepicker
    $('.input-group.date').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        todayHighlight: true
    });
}

//add if multi checkbox in grid
function editModeMultiCheckbox(functionID) {
    $('#mode').val(2);
    setEnableDisable('.hidden-xs', 'eEdit');
    $("#formUpload").hide();
    removeMandatoryInput();
    disabledPaging(functionID);

    $.each($('#tScrollBody').find('input[name="checkDetail"]:checked'),
        function () {
            var currRow = $(this).closest('tr[class~="tr-grid-detail"]');
            var rowIndex = $('#tScrollBody tbody tr').index(currRow);
            var rowTemplate = $('#tbTemplateDataRow').html();
            $(currRow).hide().after(rowTemplate.replace('templateDataRow', 'DataRow' + rowIndex));
            var newRow = $('#DataRow' + rowIndex);
            itemArray = [];
            objItem = $('#tScrollBody tbody tr:eq(' + rowIndex + ')').find('td');
            lenObj = objItem.length;
            for (i = 1; i < lenObj; i++) {
                itemArray[i] = objItem[i].innerText;
            };
            for (j = 1; j < lenObj; j++) {
                col_row = newRow.find('td')[j].childNodes;
                if (col_row.length == 0) {
                    tag_type = "LABEL";
                    id_nm = newRow.find('td')[j].getAttribute('id')
                } else {
                    tag_type = col_row[1].nodeName;
                    id_nm = col_row[1].getAttribute('id');
                }

                if (tag_type == "DIV" && id_nm == null) {
                    col_row_detail = col_row[1].childNodes;
                    tag_type = col_row_detail[3].nodeName;
                    id_nm = col_row_detail[3].getAttribute('id');
                    if (id_nm == null) {
                        tag_type = col_row_detail[1].nodeName;
                        id_nm = col_row_detail[1].getAttribute('id');
                    }
                }

                if (tag_type == "LABEL") {
                    $('#' + id_nm).text(itemArray[j]);
                }
                else if (tag_type == "SELECT") {
                    $('#' + id_nm).find("option:contains('" + itemArray[j] + "')").each(function () {
                        if ($(this).text() == itemArray[j]) {
                            $(this).attr("selected", "selected");
                        }
                    });
                }
                else {
                    $('#' + id_nm).val(itemArray[j]);
                }
            }
            itemArray = null;
            $(newRow).show();
        }
    );
    //Initial datepicker
    $('.input-group.date').datepicker({
        format: 'dd-M-yyyy',
        autoclose: true,
        todayHighlight: true
    });
}

function afterSave() {
    setEnableDisable('.hidden-xs', 'eView');
    $('#tblScroll tbody tr:first').remove();
    $("#formUpload").show();
    $('.input-ghost').val(null);
    $(".input-file").find('input').val('');
}

function cancelMode() {
    $('#tblScroll tbody tr#templateDataRow').remove();
    $("#formUpload").show();
    $('.input-ghost').val(null);
    $(".input-file").find('input').val('');
}

function checkboxAll(obj, target, objTarget) {
    if ($(obj).is(":checked")) {
        $.each($(target).find('[id = ' + objTarget + ']'), function () {
            $(this).prop('checked', true);
        })
    } else {
        $.each($(target).find('[id = ' + objTarget + ']'), function () {
            $(this).prop('checked', false);
        })
    }
}

function checkboxDetail(obj, objId, objTarget) {
    var toChecked = true;
    if (!($(obj).find('[id=' + objId + ']').length == $(obj).find('input:checked').length)) {
        toChecked = false;
    }
    $(objTarget).prop('checked', toChecked);
}

function getMessage(msgType, msgText, functionCallback) {
    if (msgType == "I") {
        bootbox.dialog({
            title: '<div class="close" style="opacity: 1 !important; margin-top: -7px;"><span aria-hidden="true"><img src="Content/Bootstrap/img/information.png" class="modal-icon" /></span></div><h4 class="modal-title" id="popup-title">Information</h4>',
            message: msgText,
            closeButton: false,
            buttons: {
                main: {
                    label: "OK",
                    className: "btn btn-xs btn-primary btn-size1 btn-Dialog-OK",
                    callback: functionCallback
                }
            }
        });
    } else if (msgType == "C") {
        bootbox.dialog({
            title: '<div class="close" style="opacity: 1 !important; margin-top: -7px;"><span aria-hidden="true"><img src="Content/Bootstrap/img/question.png" class="modal-icon" /></span></div><h4 class="modal-title" id="popup-title">Confirmation</h4>',
            message: msgText,
            closeButton: false,
            buttons: {
                success: {
                    label: "Yes",
                    className: "btn btn-xs btn-success btn-size1",
                    callback: functionCallback
                },
                danger: {
                    label: "No",
                    className: "btn btn-xs btn-danger btn-size1",
                    callback: function () {
                        return
                    }
                }
            }
        });
    } else if (msgType == "E") {
        bootbox.dialog({
            title: '<div class="close" style="opacity: 1 !important; margin-top: -7px;"><span aria-hidden="true"><img src="Content/Bootstrap/img/Error.png" class="modal-icon" /></span></div><h4 class="modal-title" id="popup-title">Error</h4>',
            message: msgText,
            closeButton: false,
            buttons: {
                main: {
                    label: "OK",
                    className: "btn btn-xs btn-primary btn-size1",
                    callback: functionCallback
                }
            }
        });
    }
}

function setMessage(msgID, param, paramCallback) {
    // param demiliter semicolon (;) maximum 7
    if (msgID != '') {
        $.ajax({
            type: 'POST',
            url: 'Common/getMessageText',
            data: {
                __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
                msgID: msgID,
                param: param
            },
            success: function (result) {
                getMessage(result.MSG_TYPE, result.MSG_TXT, paramCallback);
            }
        });
    }
}

function getTemplate(fileName) {
    $.ajax({
        type: 'POST',
        url: 'Common/GetTemplateFile',
        data: {
            __RequestVerificationToken: $('input[name="__RequestVerificationToken"]').val(),
            fileName: fileName
        },
        beforeSubmit: showLoading(),
        success: function (result) {
            //get the file name for download
            if (result.fileName != "" && result.fileName != undefined) {
                //use window.location.href for redirect to download action for download the file
                window.location.href = "Common/Download/?file=" + result.fileName;
            }
            else {
                getMessage(result.MSG_TYPE, result.MSG_TXT, function () { return });
            }
        },
        error: function (result) {
            getMessage(result.MSG_TYPE, result.MSG_TXT, function () { return });
        },
        complete: function (result) {
            hideLoading();
        }
    });
}

function browseFile() {
    $(".input-file").before(
        function () {
            if ($(".input-file").prev().hasClass('input-ghost')) {
                $(".input-file").prev().remove();
            }
            var element = $("<input type='file' accept='.xls,.xlsx' class='input-ghost' style='visibility:hidden; height:0'>");
            element.attr("name", $(this).attr("name"));
            element.change(function () {
                element.next(element).find('input').val((element.val()).split('\\').pop());
            });
            element.click();
            return element;
        }
    );
}

function getLogDetail(processID) {
    $(".btn-Dialog-OK").click();
    $('#detail-log-modal').modal({
        backdrop: 'static',
        keyboard: false
    });
    $("#temp_process_id").val(processID);
    CED010002WdoSearch();
}

function CED010002WdoSearch(page) {
    var funcID = "CED010002W";
    var page = page != undefined ? (page != '' || page != null ? page : 1) : 1;
    var pageSize = typeof $("#cmd-page-" + funcID + "-size").val() !== 'undefined' ? $("#cmd-page-" + funcID + "-size").val() : 10;
    showLoading();
    $.ajax({
        type: 'POST',
        url: 'CED010002W/CED010002WSearchData',
        data: {
            ProcessID: $("#temp_process_id").val(),
            CurrentPage: page,
            DataPerPage: pageSize
        },
        success: function (result) {
            $('#tablegriddet').html(result);
        },
        error: function (result) {
            getMessage(result.MSG_TYPE, result.MSG_TXT, function () { return });
        },
        complete: function (result) {
            hideLoading();
        }
    });
}
function disableDatepicker(id) {
    $(id).removeClass('date-picker');
    $(id).next().css('pointer-events', 'none')
}

function enabledDatepicker(id) {
    $(id).addClass('date-picker');
    $(id).next().css('pointer-events', 'auto')
}

function checkFile() {
    var isValid = 1;
    var allowedExtension = ['xls', 'xlsx'];
    var fileName = $('#txtUploadFile').val();
    var fileExtension = fileName.split('.').pop().toLowerCase();

    if (fileName == '') {
        setMessage('MCEDSTD061E', null, null);
        return isValid;
    }

    for (var index in allowedExtension) {
        if (fileExtension === allowedExtension[index]) {
            isValid = 0;
        }
    }
    if (isValid == 1) {
        setMessage('MCEDSTD062E', 'Upload;.xls or .xlsx', null);
        return isValid;
    }

    return isValid;
}

function NoSpace(e) {
    if ($.inArray(e.keyCode, [46, 8, 9, 27, 13, 110, 190]) !== -1 || 
        (e.keyCode == 65 && e.ctrlKey === true) ||
        (e.keyCode >= 35 && e.keyCode <= 40)) {
        return;
    }

    if (e.shiftKey || (e.keyCode == 32)) {
        e.preventDefault();
    }
}

// add msg no menu
function setMessageNoMenu(msgID, param, paramCallback) {
    // param demiliter semicolon (;) maximum 7
    if (msgID != '') {
        $.ajax({
            type: 'POST',
            url: 'CommonNoTDK/GetMessageText',
            data: {
                msgID: msgID,
                param: param
            },
            success: function (result) {
                getMessageNoMenu(result.MSG_TYPE, result.MSG_TXT, paramCallback);
            }
        });
    }
}
function getMessageNoMenu(msgType, msgText, functionCallback) {
    if (msgType == "I") {
        bootbox.dialog({
            title: '<div class="close" style="opacity: 1 !important; margin-top: -7px;"><span aria-hidden="true"><img src="Content/Bootstrap/img/information.png" class="modal-icon" /></span></div><h4 class="modal-title" id="popup-title">Information</h4>',
            message: msgText,
            closeButton: false,
            buttons: {
                main: {
                    label: "OK",
                    className: "button secondary btnIPopup",
                    callback: functionCallback
                }
            }
        });
    } else if (msgType == "C") {
        bootbox.dialog({
            title: '<div class="close" style="opacity: 1 !important; margin-top: -7px;"><span aria-hidden="true"><img src="Content/Bootstrap/img/question.png" class="modal-icon" /></span></div><h4 class="modal-title" id="popup-title">Confirmation</h4>',
            message: msgText,
            closeButton: false,
            buttons: {
                success: {
                    label: "Yes",
                    className: "button success btnYPopup",
                    callback: functionCallback
                },
                danger: {
                    label: "No",
                    className: "button alert",
                    callback: function () {
                        return
                    }
                }
            }
        });
    } else if (msgType == "E") {
        bootbox.dialog({
            title: '<div class="close" style="opacity: 1 !important; margin-top: -7px;"><span aria-hidden="true"><img src="Content/Bootstrap/img/Error.png" class="modal-icon" /></span></div><h4 class="modal-title" id="popup-title">Error</h4>',
            message: msgText,
            closeButton: false,
            buttons: {
                main: {
                    label: "OK",
                    className: "button secondary btnEPopup",
                    callback: functionCallback
                }
            }
        });
    }
}
function setMessageWithLabel(msgID, param, element) {
    if (msgID != '') {
        $.ajax({
            type: 'POST',
            url: 'CommonNoTDK/GetMessageText',
            data: {
                msgID: msgID,
                param: param
            },
            success: function (result) {
                $("#" + element).text(result.MSG_TYPE + " : " + result.MSG_TXT);
            }
        });
    }
}

function getDataNonTDK(param) {
    var value = "";
    var url = 'CommonNoTDK/getData';
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        url: url,
        async: false,
        data: JSON.stringify({
            param: param
        }),
        success: function (result) {
            if (result > 0) {
                value = result;
            }
        }
    });
    return value;
}

function checkExist(param) {
    var value = false;
    var url = 'Common/CheckExist';
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        url: url,
        async: false,
        data: JSON.stringify({
            param: param
        }),
        success: function (result) {
            if (result > 0) {
                value = true;
            }
        }
    });
    return value;
}

function getData(param) {
    var value = "";
    var url = 'Common/getData';
    $.ajax({
        type: "POST",
        dataType: "json",
        contentType: 'application/json; charset=utf-8',
        traditional: true,
        url: url,
        async: false,
        data: JSON.stringify({
            param: param
        }),
        success: function (result) {
            if (result > 0) {
                value = result;
            }
        }
    });
    return value;
}

function doRefreshChild(idParent, idChild, idCombo) {
    var valParent = $("#" + idParent).val().split(':')[0];
    if (valParent != "") {
        $.ajax({
            type: 'POST',
            url: "./Common/getCombobox",
            data: {
                comboBoxID: idCombo,
                comboBoxParent: valParent
            },
            success: function (result) {
                $("#" + idChild).empty();
                $("#" + idChild).append('<option value="">-ALL-</option>');
                for (var i = 0; i < result.length; i++) {
                    $("#" + idChild).append('<option value="' + result[i].VALUE + '">' + result[i].NAME + '</option>');
                }
                if (valParent != "" && result.length >= 1) {
                    $("#" + idChild).prop("disabled", false);
                    $("#" + idChild).css("color", "#858585");
                    $("#" + idChild).css("background-color", "#ffffff");
                }
                else {
                    $("#" + idChild).prop("disabled", true);
                    $("#" + idChild).css("color", "lightgray");
                    $("#" + idChild).css("background-color", "#eeeeee");
                }

                $("#" + idChild).change();
            },
            error: function (result) {
                alert("data error");
            }
        });
    } else {
        $("#" + idChild).empty();
    }
    
}
