﻿using IPOS.Models.Common;
using NPOI.SS.Formula.Functions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010206W
{
    public class CED010206WSearchModel : SearchModel
    {
        public string PROD_DT { get; set; }
        public string MODULE_DEST_CD { get; set; }
        public string PACKING_LINE_CD { get; set; }
        public string PART_ORD_RENBAN { get; set; }
    }
}