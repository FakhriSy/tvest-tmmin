﻿using IPOS.Models.CED010206W;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010206W
{
    public class CED010206WRepository
    {
        #region Singleton
        private static CED010206WRepository instance = null;
        public static CED010206WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010206WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED010206W> SearchData(CED010206WSearchModel SearchModel)
        {
            List<CED010206W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROD_DT = SearchModel.PROD_DT,
                    MODULE_DEST_CD = SearchModel.MODULE_DEST_CD,
                    PACKING_LINE_CD = SearchModel.PACKING_LINE_CD,
                    PART_ORD_RENBAN = SearchModel.PART_ORD_RENBAN,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010206W>("CED010206W/CED010206WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010206WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.SingleOrDefault<int>("CED010206W/CED010206WSearchCountData", SearchModel);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}