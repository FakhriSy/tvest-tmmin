﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010206W
{
    public class CED010206W
    {
        public int ROW_NO { get; set; }
        public string CONTROL_MODULE_NO { get; set; }
        public string PROD_DT { get; set; }
        public string PACKING_PLAN_DT { get; set; }
        public string PACKING_LINE_CD { get; set; }
        public string MODULE_DEST_CD { get; set; }
        public string LOT_MODULE_NO { get; set; }
        public string CASE_NO { get; set; }
        public string CONTAINER_DEST_CD { get; set; }
        public string CONTAINER_SNO { get; set; }
        public string SEQ_NO { get; set; }
    }
}