﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010002W
{
    public class CED010002W
    {
        public string SEQ_NO { get; set; }
        public string ERR_DT { get; set; }
        public string LOC { get; set; }
        public string MSG_TYPE { get; set; }
        public string MSG_ID { get; set; }
        public string MSG_TEXT { get; set; }
        public string START_DT { get; set; }
        public string END_DT { get; set; }
        public string FUNCTION_NM { get; set; }
        public string PROCESS_ID { get; set; }
        public string STATUS { get; set; }
        public string USERID { get; set; }
    }
}