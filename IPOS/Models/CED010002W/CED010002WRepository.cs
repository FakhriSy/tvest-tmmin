﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010002W
{
    public class CED010002WRepository
    {
        #region Singleton
        private static CED010002WRepository instance = null;
        public static CED010002WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010002WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010002W> SearchData(CED010002WSearchModel SearchModel)
        {
            List<CED010002W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROCESS_ID = SearchModel.ProcessID,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010002W>("CED010002W/CED010002WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010002WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROCESS_ID = SearchModel.ProcessID
                };
                result = db.SingleOrDefault<int>("CED010002W/CED010002WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}