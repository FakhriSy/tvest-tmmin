﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010505W
{
    public class CED010505WRepository
    {
        #region Singleton
        private static CED010505WRepository instance = null;
        public static CED010505WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010505WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010505W> SearchData(CED010505WSearchModel m)
        {
            List<CED010505W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    ProdDate = m.ProdDate,
                    ShutterNo = m.ShutterNo,
                    PosLine = m.PosLine,
                    PartNo = m.PartNo,
                    Unique = m.Unique,
                    BoxNo = m.BoxNo,
                    ProdType = m.ProdType,
                    PLaneAddress = m.PLaneAddress,
                    Supplier = m.Supplier,
                    Case = m.Case,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED010505W>("CED010505W/CED010505WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010505WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    ProdDate = m.ProdDate,
                    ShutterNo = m.ShutterNo,
                    PosLine = m.PosLine,
                    PartNo = m.PartNo,
                    Unique = m.Unique,
                    BoxNo = m.BoxNo,
                    ProdType = m.ProdType,
                    PLaneAddress = m.PLaneAddress,
                    Supplier = m.Supplier,
                    Case = m.Case
                };
                result = db.SingleOrDefault<int>("CED010505W/CED010505WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}