﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010505W
{
    public class CED010505WSearchModel : SearchModel
    {
        public string ProdDate { get; set; }
        public string ShutterNo { get; set; }
        public string PosLine { get; set; }
        public string PartNo { get; set; }
        public string Unique { get; set; }
        public string BoxNo { get; set; }
        public string ProdType { get; set; }
        public string PLaneAddress { get; set; }
        public string Supplier { get; set; }
        public string Case { get; set; }
    }
}