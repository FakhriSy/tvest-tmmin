﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED030700W
{
    public class CED030700WRepository
    {
        #region Singleton
        private static CED030700WRepository instance = null;
        public static CED030700WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED030700WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED030700W> SearchData(CED030700WSearchModel m)
        {
            List<CED030700W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DeliveryFrom = m.DeliveryDateFrom,
                    DeliveryTo = m.DeliveryDateTo,
                    ActualReceiveFrom = m.ActualReceiveFrom,
                    ActualReceiveTo = m.ActualReceiveTo,
                    Shift = m.Shift,
                    PhysicalDock = m.PhysicalDock,
                    RouteName = m.RouteName,
                    RouteCycle = m.RouteCycle,
                    LogicalDock = m.LogicalDock,
                    TS = m.TS,
                    Supplier = m.Supplier,
                    ManifestNo = m.ManifestNo,
                    OrderNo = m.OrderNo,
                    Status = m.Status,
                    Dest = m.Dest,
                    Lot = m.Lot,
                    Case = m.Case,
                    Dock = m.Dock,
                    PartNo = m.PartNo,
                    KanbanId = m.KanbanId,
                    Type = m.Type,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED030700W>("CED030700W/CED030700WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED030700WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DeliveryFrom = m.DeliveryDateFrom,
                    DeliveryTo = m.DeliveryDateTo,
                    ActualReceiveFrom = m.ActualReceiveFrom,
                    ActualReceiveTo = m.ActualReceiveTo,
                    Shift = m.Shift,
                    PhysicalDock = m.PhysicalDock,
                    RouteName = m.RouteName,
                    RouteCycle = m.RouteCycle,
                    LogicalDock = m.LogicalDock,
                    TS = m.TS,
                    Supplier = m.Supplier,
                    ManifestNo = m.ManifestNo,
                    OrderNo = m.OrderNo,
                    Status = m.Status,
                    Dest = m.Dest,
                    Lot = m.Lot,
                    Case = m.Case,
                    Dock = m.Dock,
                    PartNo = m.PartNo,
                    KanbanId = m.KanbanId,
                    Type = m.Type
                };
                result = db.SingleOrDefault<int>("CED030700W/CED030700WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED030700W m, int mode, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    Type=m.Type,
                    ManifestNo = m.ManifestNo,
                    KanbanId = m.KanbanId,
                    PlanCatchUp = m.PlanCatchUp,
                    Problem = m.Problem,
                    Changed_Date = DateTime.Now.ToString("dd-MMM-yyyy"),
                    USER_LOGIN = userLogin,
                    ItemParam = m.ItemParam,
                    ChangedDtParam = m.ChangedDtParam

                };
                result = db.SingleOrDefault<string>("CED030700W/CED030700WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}