﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED020200W
{
    public class CED020200W
    {
        public string ACT_START { get; set; }
        public string PROD_DATE { get; set; }
        public string PROD_DAY { get; set; }
        public string POS_TYPE { get; set; }
        public string PROD_LINE { get; set; }
        public string SEQ_NO_1 { get; set; }
        public string SEQ_NO_2 { get; set; }
        public string SEQ_NO_3 { get; set; }
        public string SEQ_NO_4 { get; set; }
        public string SEQ_NO_5 { get; set; }
        public string SEQ_NO_6 { get; set; }
        public string SEQ_NO_7 { get; set; }
        public string SEQ_NO_8 { get; set; }
        public string SEQ_NO_9 { get; set; }
        public string SEQ_NO_10 { get; set; }
        public string SEQ_NO_11 { get; set; }
        public string SEQ_NO_12 { get; set; }
        public string SEQ_NO_13 { get; set; }
        public string SEQ_NO_14 { get; set; }
        public string SEQ_NO_15 { get; set; }
        public string SEQ_NO_16 { get; set; }
        public string SEQ_NO_17 { get; set; }
        public string SEQ_NO_18 { get; set; }
        public string SEQ_NO_19 { get; set; }
        public string SEQ_NO_20 { get; set; }
        public string SEQ_NO_21 { get; set; }
        public string SEQ_NO_22 { get; set; }
        public string SEQ_NO_23 { get; set; }
        public string SEQ_NO_24 { get; set; }
        public string SEQ_NO_25 { get; set; }

        public string SEQ_NO_1BG { get; set; }
        public string SEQ_NO_2BG { get; set; }
        public string SEQ_NO_3BG { get; set; }
        public string SEQ_NO_4BG { get; set; }
        public string SEQ_NO_5BG { get; set; }
        public string SEQ_NO_6BG { get; set; }
        public string SEQ_NO_7BG { get; set; }
        public string SEQ_NO_8BG { get; set; }
        public string SEQ_NO_9BG { get; set; }
        public string SEQ_NO_10BG { get; set; }
        public string SEQ_NO_11BG { get; set; }
        public string SEQ_NO_12BG { get; set; }
        public string SEQ_NO_13BG { get; set; }
        public string SEQ_NO_14BG { get; set; }
        public string SEQ_NO_15BG { get; set; }
        public string SEQ_NO_16BG { get; set; }
        public string SEQ_NO_17BG { get; set; }
        public string SEQ_NO_18BG { get; set; }
        public string SEQ_NO_19BG { get; set; }
        public string SEQ_NO_20BG { get; set; }
        public string SEQ_NO_21BG { get; set; }
        public string SEQ_NO_22BG { get; set; }
        public string SEQ_NO_23BG { get; set; }
        public string SEQ_NO_24BG { get; set; }
        public string SEQ_NO_25BG { get; set; }
    }

    public class CED020200WDetail
    {
        public string No { get; set; }
        public string ShutterNo { get; set; }
        public string QtyPcs { get; set; }
        public string QtyBox { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string shift { get; set; }
        public string Renban { get; set; }
        public string ModType { get; set; }
        public string M3 { get; set; }
        public string Ratio { get; set; }
        public string PlanTime { get; set; }
        public string PlanCT { get; set; }
        public string PackProcessProc { get; set; }
        public string PackProcessAct { get; set; }
        public string PackProcessDelay { get; set; }
        public string PackResultNo { get; set; }
        public string PackResultStart { get; set; }
        public string PackResultFinish { get; set; }
        public string PackResultActual { get; set; }
        public string PackResultBlc { get; set; }
        public string PackResultCumm { get; set; }
        public string Status { get; set; }
        public string LineCode { get; set; }
        public string LineName { get; set; }
        public string Type { get; set; }
        public string PcsQty { get; set; }
    }
}