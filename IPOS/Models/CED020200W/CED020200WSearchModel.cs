﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED020200W
{
    public class CED020200WSearchModel : SearchModel
    {
        public string ACT_START { get; set; }
        public string ProdDate { get; set; }
        public string ProdLine { get; set; }
        public string PosType { get; set; }
        public string ZoneCode { get; set; }
        public string LOT_MODULE_NO { get; set; }
        public string CASE_NO { get; set; }
        public string CONTAINER_SNO { get; set; }
        public string SHIFT { get; set; }
        public string ZONE_CD { get; set; }
        public string PROD_DATE { get; set; }
        public string POS_TYPE { get; set; }
        public string SHOOTER_NO { get; set; }
        public IList<CED020200WSearchModel> LIST_DELETE { get; set; }
    }

    public class CED020200WSearchModelBatch : SearchModel
    {
        public string PlantCd { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}