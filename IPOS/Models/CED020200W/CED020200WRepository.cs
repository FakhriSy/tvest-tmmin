﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Data.SqlClient;

namespace IPOS.Models.CED020200W
{
    public class CED020200WRepository
    {
        #region Singleton
        private static CED020200WRepository instance = null;
        public static CED020200WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED020200WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED020200W> SearchData(CED020200WSearchModel m)
        {
            List<CED020200W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROD_DATE = m.ProdDate,
                    POS_TYPE = m.PosType,
                    PROD_LINE = m.ProdLine,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED020200W>("CED020200W/CED020200WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED020200WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROD_DATE = m.ProdDate,
                    POS_TYPE = m.PosType,
                    PROD_LINE = m.ProdLine,
                };
                result = db.SingleOrDefault<int>("CED020200W/CED020200WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED020200WDetail> SearchDataDetail(CED020200WSearchModel m)
        {
            List<CED020200WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROD_DATE = m.ProdDate,
                    POS_TYPE = m.PosType,
                    PROD_LINE = m.ProdLine,
                    ZONE_CODE = m.ZoneCode
                };
                result = db.Fetch<CED020200WDetail>("CED020200W/CED020200WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        protected SqlParameter CreateSqlParameterOutputTblOfDeleteDetail(string parameterName, IList<CED020200WSearchModel> listData, string typeName)
        {
            DataTable table = new DataTable();
            table.Columns.Add("ZONE_CODE", type: typeof(string));
            table.Columns.Add("CONTAINER_SNO", type: typeof(string));
            table.Columns.Add("SHOOTER_NO", type: typeof(string));
            table.Columns.Add("LOT_MODULE_NO", type: typeof(string));
            table.Columns.Add("ACT_START", type: typeof(string));
            table.Columns.Add("CASE_NO", type: typeof(string));
            if (listData != null)
            {
                foreach (CED020200WSearchModel data in listData)
                {
                    DataRow row = table.NewRow();
                    row["ZONE_CODE"] = data.ZoneCode;
                    row["CONTAINER_SNO"] = data.CONTAINER_SNO;
                    row["SHOOTER_NO"] = data.SHOOTER_NO;
                    row["LOT_MODULE_NO"] = data.LOT_MODULE_NO;
                    row["ACT_START"] = data.ACT_START;
                    row["CASE_NO"] = data.CASE_NO;
                    table.Rows.Add(row);
                }
            }

            var paramStruct = new System.Data.SqlClient.SqlParameter(parameterName, System.Data.SqlDbType.Structured);
            paramStruct.SqlDbType = SqlDbType.Structured;
            paramStruct.SqlValue = table;
            paramStruct.TypeName = typeName;
            return paramStruct;
        }

        public string DeleteDataDetail(CED020200WSearchModel m, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                SqlParameter outputTblMasterProduct = CreateSqlParameterOutputTblOfDeleteDetail("TableOfDeleteDetail", m.LIST_DELETE, "dbo.TableOfDeleteDetail");
                db.BeginTransaction();
                dynamic args = new
                {
                    T_DeleteDetail = outputTblMasterProduct,
                    POS_TYPE = m.POS_TYPE,
                    PROD_DATE = m.PROD_DATE,
                    USER_LOGIN = username
                };

                result = db.SingleOrDefault<string>("CED020200W/CED020200WDeleteDataDetail", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<dynamic> CheckPOSType(CED020200WSearchModelBatch searchModel)
        {
            List<dynamic> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    POS_DATE = (searchModel.DateFrom == null ? "" : Convert.ToDateTime(searchModel.DateFrom).ToString("yyyyMMdd")),
                    PACKING_PLANT_CD = (searchModel.PlantCd == null ? "" : searchModel.PlantCd),
                    PACKING_MTH = (searchModel.DateFrom == null ? "" : Convert.ToDateTime(searchModel.DateFrom).ToString("yyyyMM")),
                };
                result = db.Fetch<dynamic>("CED020200W/CheckPOSType", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}