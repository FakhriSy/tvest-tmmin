﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010205W
{
    public class CED010205W
    {

    }

    public class CED010205WDetail
    {
        public string No { get; set; }
        public string Module_No { get; set; }
        public string Case_No { get; set; }
        public string Case_Type { get; set; }
        public string Pack_Line { get; set; }
        public string Planning_Date { get; set; }
        public string Planning_Time { get; set; }
        public string Actual_Date { get; set; }
        public string Actual_Time { get; set; }
        public string Fill_In_Status { get; set; }
        public string Stuffing_Status { get; set; }
        public string Remark { get; set; }
    }
}