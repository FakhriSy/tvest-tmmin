﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010205W
{
    public class CED010205WRepository
    {
        #region Singleton
        private static CED010205WRepository instance = null;
        public static CED010205WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010205WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED010205WDetail> SearchDataDetail()
        {
            List<CED010205WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Id = ""
                };
                result = db.Fetch<CED010205WDetail>("CED010205W/CED010205WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}