﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010204W
{
    public class CED010204WRepository
    {
        #region Singleton
        private static CED010204WRepository instance = null;
        public static CED010204WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010204WRepository();
                }
                return instance;
            }
        }
        #endregion


        public IList<dynamic> GetData()
        {
            List<dynamic> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    
                };
                result = db.Fetch<dynamic>("CED010204W/CED010204WGetData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}