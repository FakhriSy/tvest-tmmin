﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED031502W
{
    public class CED031502WReceive
    {
        public string MANIFEST_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PLANT_NM { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER { get; set; }
        public string DOCK { get; set; }
        public string CHECK_VALIDATE { get; set; }
        public string ARRIVAL_TIME { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DATE { get; set; }
        public string PROBLEM_FLAG { get; set; }
        public string SUP_COMPLETENESS_STATUS { get; set; }


    }
    public class CED031502W
    {
        public string ROW_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string UNIQUE { get; set; }
        public string KANBAN_NO { get; set; }
        public string ORDER_QTY { get; set; }
        public string RECEIVE_QTY { get; set; }
        public string CHECK_STATUS { get; set; }
        public string CHECK_MANIFEST { get; set; }
        public string KANBAN_TYPE { get; set; }
        public string SHORTAGE_QTY { get; set; }
        public string MISSPART_QTY { get; set; }
        public string DAMAGE_QTY { get; set; }
        public string SUP_COMPLETENESS_STATUS { get; set; }
    }

    public class CED031502WDetail
    {
        public string No { get; set; }
        public string Part_No { get; set; }
        public string Part_Name { get; set; }
        public string Unique { get; set; }
        public string KanbanID { get; set; }
        public string Kanban_Status { get; set; }
        public string Shortage_Pcs { get; set; }
        public string Shortage_Flag { get; set; }
        public string KanbanType { get; set; }
        public string Order_Qty { get; set; }
    }

    public class CED031502WProblemPart
    {
        public string NO { get; set; }
        public string PART_BARCODE { get; set; }
        public string PROBLEM_TYPE { get; set; }
        public string PROBLEM { get; set; }
        public string QTY { get; set; }
        public string RECEIVED_QTY { get; set; }
        public string PROBLEM_DT { get; set; }
        public string CREATED_BY { get; set; }
    }

    public class CED031502WProblemPartDetail
    {
        public string MANIFEST_NO { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_QTY { get; set; }
        public string RECEIVED_QTY { get; set; }
        public string SHORTAGE_QTY { get; set; }
        public string MISSPART_QTY { get; set; }
        public string DAMAGE_QTY { get; set; }
        public string RECEIVED_STATUS { get; set; }
        public string DELETION_FLAG { get; set; }
        public string DELETION_DT { get; set; }
        public string DELETION_BY { get; set; }
        public string AREA_NAME { get; set; }
        public string SOLVED_FLAG { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
        public string REASON { get; set; }

        //public string MANIFEST_NO { get; set; }
        //public string PART_NO { get; set; }
        public string PROBLEM_TYPE { get; set; }
        public string PROBLEM_DT { get; set; }
        public string QTY { get; set; }
        //public string CREATED_BY { get; set; }
        //public string CREATED_DT { get; set; }
        //public string CHANGED_BY { get; set; }
        //public string CHANGED_DT { get; set; }
        public string PART_BARCODE { get; set; }
        public string LAST_STATUS { get; set; }
    }

    public class CED031502WApi
    {
        public string MSG { get; set; }
        public string PROCESS_ID { get; set; }
        public string PROCESS_CD { get; set; }

        public int statusCode { get; set; }
        public bool result { get; set; }
        public string message { get; set; }

    }
}