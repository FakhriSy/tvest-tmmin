﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED031502W
{
    public class CED031502WRepository
    {
        #region Singleton
        private static CED031502WRepository instance = null;
        public static CED031502WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED031502WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED031502WReceive> SearchDataRecive(string param, string username)
        {
            List<CED031502WReceive> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PARAM = param,
                    USERNAME = username
                };
                result = db.Fetch<CED031502WReceive>("CED031502W/CED031502WReceiveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public IList<CED031502W> SearchDataInitial(CED031502WSearchModel SearchModel)
        {
            List<CED031502W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    ORDER_NO = SearchModel.ORDER_NO,
                    SUPPLIER_PLANTCODE = SearchModel.SUPPLIER_PLANTCODE,
                    RCV_PLANTCODE = SearchModel.RCV_PLANTCODE,
                    DOCK_CODE = SearchModel.DOCK_CODE,
                    PART_NO = SearchModel.PART_NO,
                    UNIQUE = SearchModel.UNIQUE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031502W>("CED031502W/CED031502WSearchDataInitial", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public IList<CED031502W> SearchData(CED031502WSearchModel SearchModel)
        {
            List<CED031502W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    ORDER_NO = SearchModel.ORDER_NO,
                    SUPPLIER_PLANTCODE = SearchModel.SUPPLIER_PLANTCODE,
                    RCV_PLANTCODE = SearchModel.RCV_PLANTCODE,
                    DOCK_CODE = SearchModel.DOCK_CODE,
                    PART_NO = SearchModel.PART_NO,
                    UNIQUE = SearchModel.UNIQUE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031502W>("CED031502W/CED031502WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED031502WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.SingleOrDefault<int>("CED031502W/CED031502WSearchCountData", SearchModel);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031502WDetail> SearchDataDetail(CED031502WSearchModel SearchModel)
        {
            List<CED031502WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    ORDER_NO = SearchModel.ORDER_NO,
                    SUPPLIER_CODE = SearchModel.SUPPLIER_CODE,
                    SUPPLIER_PLANTCODE = SearchModel.SUPPLIER_PLANTCODE,
                    RCV_PLANTCODE = SearchModel.RCV_PLANTCODE,
                    DOCK_CODE = SearchModel.DOCK_CODE,
                    PART_NO = SearchModel.PART_NO,
                    UNIQUE = SearchModel.UNIQUE,
                    PART_BARCODE = SearchModel.PART_BARCODE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031502WDetail>("CED031502W/CED031502WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031502WProblemPart> SearchDataPproblemPart(CED031502WSearchModel SearchModel)
        {
            List<CED031502WProblemPart> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    PART_NO = SearchModel.PART_NO
                };
                result = db.Fetch<CED031502WProblemPart>("CED031502W/CED031502WSearchProblemPart", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED031502WSearchModel m, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = m.MANIFEST_NO,
                    PART_BARCODE = m.PART_BARCODE,
                    FULL_STATUS = m.FULL_STATUS,
                    PCS_STATUS = m.PCS_STATUS,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<string>("CED031502W/CED031502WSavedataDetail", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataFlag(string ManifestNo, string PartNo, string CheckType)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = ManifestNo,
                    PART_NO = PartNo,
                    CHECK_TYPE = CheckType
                };
                result = db.SingleOrDefault<string>("CED031502W/CED031502WSavedataFlag", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataTempHeader(CED031502WSearchModel m, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = m.MANIFEST_NO,
                    PART_NO = m.PART_NO,
                    REC_STATUS = m.REC_STATUS,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<string>("CED031502W/CED031502WSavedataTempHeader", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataReceive(string ManifestNo, string username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = ManifestNo,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<string>("CED031502W/CED031502WSavedataReceive", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public CED031502WReceive prepareHeader(string param, string username)
        {
            CED031502WReceive result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = param,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<CED031502WReceive>("CED031502W/CED031502WPrepareHeader", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031502WProblemPartDetail> prepareProblem(string manifestNo, string username)
        {
            List<CED031502WProblemPartDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = manifestNo,
                    USER_LOGIN = username,
                };
                result = db.Fetch<CED031502WProblemPartDetail>("CED031502W/CED031502WprepareProblem", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031502WProblemPartDetail> prepareProblemDetail(string manifestNo, string username)
        {
            List<CED031502WProblemPartDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = manifestNo,
                    USER_LOGIN = username,
                };
                result = db.Fetch<CED031502WProblemPartDetail>("CED031502W/CED031502WprepareProblemDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string GetFunctionName(string FunctionID, string ModuleID)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            string result = null;
            string query = "SELECT FUNCTION_NM FROM TB_M_FUNCTION WHERE MODULE_ID = '" + ModuleID + "' AND FUNCTION_ID = '" + FunctionID + "'; ";

            result = db.SingleOrDefault<string>(query, new { });

            return result;
        }

        public string CreateLog(string USERNAME, string FUNCTION_ID, string MODULE_ID, string MSG_ID, string FUNCTION_NAME, string MESSAGE, string process_sts = null, string process_id = null, string action = null)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();

            string result = null;

            string pid = null;

            if (process_id != null)
            {
                pid = process_id;
            }

            string query_header = "";
            if (action != null)
            {
                if (action == "start")
                {
                    query_header = "DECLARE @@p_pid as VARCHAR(MAX); SELECT @@p_pid = NEXT VALUE FOR dbo.SEQ_PROCESS; INSERT INTO TB_R_LOG_H (PROCESS_ID, MODULE_ID, FUNCTION_ID, START_DT, PROCESS_STS, USER_ID) VALUES (@@p_pid, '" + MODULE_ID + "', '" + FUNCTION_ID + "', GETDATE(), 3, '" + USERNAME + "'); SELECT @@p_pid; ";
                    pid = db.ExecuteScalar<string>(query_header, new { });
                }
                else
                {
                    query_header = "UPDATE TB_R_LOG_H SET PROCESS_STS = '" + process_sts + "' WHERE PROCESS_ID = '" + pid + "'; select 'test' ";
                    db.Execute(query_header, new { });
                }
            }

            string query = "execute SP_CreateLOGEmanifest '" + FUNCTION_ID + "','" + pid + "','" + USERNAME + "','" + MSG_ID + "','" + FUNCTION_NAME + "','" + MESSAGE + "',';', '" + process_sts + "'; select 'test'; ";

            db.Execute(query, new { });

            return pid;
        }
    }
}