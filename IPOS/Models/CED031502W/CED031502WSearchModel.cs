﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED031502W
{
    public class CED031502WSearchModel : SearchModel
    {
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANTCODE { get; set; }
        public string RCV_PLANTCODE { get; set; }
        public string DOCK_CODE { get; set; }
        public string PART_NO { get; set; }
        public string PART_BARCODE { get; set; }
        public string UNIQUE { get; set; }
        public int RECEIVE_QTY { get; set; }
        public string SHORTAGE_PCS { get; set; }
        public string FULL_STATUS { get; set; }
        public string PCS_STATUS { get; set; }
        public string REC_STATUS { get; set; }
    }
}