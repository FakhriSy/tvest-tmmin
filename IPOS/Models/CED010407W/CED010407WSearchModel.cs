﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010407W
{
    public class CED010407WSearchModel : SearchModel
    {
        public string PartNo { get; set; }
        public string Address { get; set; }
        public string UniqueNo { get; set; }
        public string ProdLine { get; set; }
        public string Pattern { get; set; }
    }
}