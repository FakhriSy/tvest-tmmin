﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010407W
{
    public class CED010407WRepository
    {
        #region Singleton
        private static CED010407WRepository instance = null;
        public static CED010407WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010407WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010407W> SearchData(CED010407WSearchModel SearchModel)
        {
            List<CED010407W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    Address = SearchModel.Address,
                    UniqueNo = SearchModel.UniqueNo,
                    ProdLine = SearchModel.ProdLine,
                    Pattern = SearchModel.Pattern,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010407W>("CED010407W/CED010407WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010407WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    Address = SearchModel.Address,
                    UniqueNo = SearchModel.UniqueNo,
                    ProdLine = SearchModel.ProdLine,
                    Pattern = SearchModel.Pattern
                };
                result = db.SingleOrDefault<int>("CED010407W/CED010407WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED010407WDetail> SearchDataDetail(CED010407WSearchModel SearchModel)
        {
            List<CED010407WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    Address = SearchModel.Address
                };
                result = db.Fetch<CED010407WDetail>("CED010407W/CED010407WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}