﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010407W
{
    public class CED010407W
    {
        public string No { get; set; }
        public string PartNo { get; set; }
        public string UniqueNo { get; set; }
        public string Address { get; set; }
        public string Pattern { get; set; }
        public string ProdLine { get; set; }
        public string QtyBox { get; set; }
        public string ValidFrom { get; set; }
    }

    public class CED010407WDetail
    {
        public string PartNo { get; set; }
        public string Address { get; set; }
        public string QtyBox { get; set; }
        public string Keypoint { get; set; }
        public string PackingSpec { get; set; }
        public string RevNo { get; set; }
        public string RevItem { get; set; }
        public string Admin { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string Barcode { get; set; }
    }
}