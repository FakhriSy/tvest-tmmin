﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED050700W
{
    public class CED050700WSearchModel : SearchModel
    {
        public string DATE_FROM { get; set; }
        public string DATE_TO { get; set; }
        public string LOT_MOD_NO { get; set; }
        public string CASE_NO { get; set; }
        public string OYA { get; set; }
        public string STATUS { get; set; }
        public string LINE_CD { get; set; }
        public string PACKING_LINE_CD { get; set; }
        public string ROW_START { get; set; }
        public string ROW_END { get; set; }
    }

    public class CED050700WSearchModelDtl : SearchModel
    {
        public string DATE_FROM { get; set; }
        public string DATE_TO { get; set; }
        public string KANBAN_HEADER_CD { get; set; }

        public string ProdDateDtl { get; set; }
        public string RanbanNoDtl { get; set; }
        public string LotModNoDtl { get; set; }
        public string DestCodeDtl { get; set; }
        public string CaseNoDtl { get; set; }
        public string KanbanIDDtl { get; set; }
        public string OyaDtl { get; set; }
        public string StatusDtl { get; set; }

        public string ROW_START { get; set; }
        public string ROW_END { get; set; }

        public string GenerateMode { get; set; }
        public string Printer { get; set; }
        public string UserName { get; set; }
        public string REPRINT_REASON { get; set; }
        public string PrintMode { get; set; }
        

    }
}