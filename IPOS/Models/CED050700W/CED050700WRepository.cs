﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED050700W
{
    public class CED050700WRepository
    {
        #region Singleton
        private static CED050700WRepository instance = null;
        public static CED050700WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED050700WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED050700WDateFrom> getDateFrom()
        {
            List<CED050700WDateFrom> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Mode = ""
                };
                result = db.Fetch<CED050700WDateFrom>("CED050700W/CED050700WgetDateFrom", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED050700W> SearchData(CED050700WSearchModel SearchModel)
        {
            List<CED050700W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DATE_FROM = SearchModel.DATE_FROM,
                    DATE_TO = SearchModel.DATE_TO,
                    LOT_MOD_NO = SearchModel.LOT_MOD_NO,
                    CASE_NO = SearchModel.CASE_NO,
                    OYA = SearchModel.OYA,
                    STATUS = SearchModel.STATUS,
                    LINE_CD = SearchModel.LINE_CD,
                    PACKING_LINE_CD = SearchModel.PACKING_LINE_CD,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED050700W>("CED050700W/CED050700WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED050700WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DATE_FROM = SearchModel.DATE_FROM,
                    DATE_TO = SearchModel.DATE_TO,
                    LOT_MOD_NO = SearchModel.LOT_MOD_NO,
                    CASE_NO = SearchModel.CASE_NO,
                    OYA = SearchModel.OYA,
                    STATUS = SearchModel.STATUS,
                    LINE_CD = SearchModel.LINE_CD,
                    PACKING_LINE_CD = SearchModel.PACKING_LINE_CD,
                };
                result = db.SingleOrDefault<int>("CED050700W/CED050700WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED050700WDetail> SearchDataDetail(CED050700WSearchModelDtl SearchModel)
        {
            List<CED050700WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KANBAN_HEADER_CD = SearchModel.KANBAN_HEADER_CD,
                    DATE_FROM = SearchModel.DATE_FROM,
                    DATE_TO = SearchModel.DATE_TO,
                    RanbanNoDtl = SearchModel.RanbanNoDtl,
                    LotModNoDtl = SearchModel.LotModNoDtl,
                    DestCodeDtl = SearchModel.DestCodeDtl,
                    CaseNoDtl = SearchModel.CaseNoDtl,
                    KanbanIDDtl = SearchModel.KanbanIDDtl,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End,
                    ProdDateDtl = SearchModel.ProdDateDtl,
                    OyaDtl = SearchModel.OyaDtl,
                    StatusDtl = SearchModel.StatusDtl
                };
                result = db.Fetch<CED050700WDetail>("CED050700W/CED050700WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public int SearchCountDataDetail(CED050700WSearchModelDtl SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KANBAN_HEADER_CD = SearchModel.KANBAN_HEADER_CD,
                    DATE_FROM = SearchModel.DATE_FROM,
                    DATE_TO = SearchModel.DATE_TO,
                    RanbanNoDtl = SearchModel.RanbanNoDtl,
                    LotModNoDtl = SearchModel.LotModNoDtl,
                    DestCodeDtl = SearchModel.DestCodeDtl,
                    CaseNoDtl = SearchModel.CaseNoDtl,
                    KanbanIDDtl = SearchModel.KanbanIDDtl,
                    ProdDateDtl = SearchModel.ProdDateDtl,
                    OyaDtl = SearchModel.OyaDtl,
                    StatusDtl = SearchModel.StatusDtl
                };
                result = db.SingleOrDefault<int>("CED050700W/CED050700WSearchCountDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED050700WPrint> SearchDataPrint(CED050700WSearchModelDtl SearchModel)
        {
            List<CED050700WPrint> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KANBAN_HEADER_CD = SearchModel.KANBAN_HEADER_CD,
                    //DATE_FROM = SearchModel.DATE_FROM,
                    //DATE_TO = SearchModel.DATE_TO,
                    //RanbanNoDtl = SearchModel.RanbanNoDtl,
                    //LotModNoDtl = SearchModel.LotModNoDtl,
                    //DestCodeDtl = SearchModel.DestCodeDtl,
                    //CaseNoDtl = SearchModel.CaseNoDtl,
                    //KanbanIDDtl = SearchModel.KanbanIDDtl,
                    //ROW_START = SearchModel.CurrentPage,
                    //ROW_END = SearchModel.DataPerPage
                };
                result = db.Fetch<CED050700WPrint>("CED050700W/CED050700WSearchDataPrint", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string UpdatePackingDetail(string REPRINT_REASON, string KanbanIDDtl, string UserName)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    REPRINT_REASON = REPRINT_REASON,
                    KanbanIDDtl = KanbanIDDtl,
                    UserName = UserName

                };
                result = db.SingleOrDefault<string>("CED050700W/CED050700WUpdatePackingDetail", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }


    }
}