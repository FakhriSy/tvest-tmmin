﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED050700W
{
    public class CED050700WDateFrom
    {
        public string From { get; set; }
    }
    public class CED050700W
    {
        public string No { get; set; }
        public string KANBAN_HEADER_CD { get; set; }
        public string ProdDate { get; set; }
        public string LotModNo { get; set; }
        public string CaseNo { get; set; }
        public string RenbanNo { get; set; }
        public string DestCode { get; set; }
        public string ReprintReason { get; set; }
        public string TotalKanbanPicking { get; set; }
        public string Oya { get; set; }
        public string PrintCounter { get; set; }

        public string PackingLineCd { get; set; }
        public string LineCd { get; set; }
        public string Status { get; set; }
        public string PrintCounterDetail { get; set; }
    }

    public class CED050700WDetail
    {
        public string No { get; set; }
        public string ProdDate { get; set; }
        public string KanbanID { get; set; }
        public string Unique { get; set; }
        public string PartNo { get; set; }
        public string PartName { get; set; }
        public string DockCode { get; set; }
        public string Supplier { get; set; }
        public string OrderNo { get; set; }
        public string PcsKbn { get; set; }
        public string PickingAddress { get; set; }
        public string Shooter { get; set; }
        public string Status { get; set; }
        public string Oya { get; set; }
        public string ReprintReason { get; set; }

        public string PackingLineCd { get; set; }
        public string LineCd { get; set; }
    }
    public class CED050700WPrint
    {
        public string PageNumber { get; set; }
        public string SupplierName { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierPlantCode { get; set; }
        public string ArrivalTime { get; set; }
        public string DockCode { get; set; }
        public string P_LaneNo { get; set; }
        public string Conv_No { get; set; }
        public string PickingAddress { get; set; }
        public string Zone { get; set; }
        public string Plant_Name { get; set; }
        public string PartNo { get; set; }
        public string PartName { get; set; }
        public string UniqueNo { get; set; }
        public string BoxNo { get; set; }
        public string Shutter { get; set; }
        public string LotCode { get; set; }
        public string CaseNo { get; set; }
        public string PackingDate { get; set; }
        public string PcsPerKanban { get; set; }
        public string OrderNo { get; set; }
        public string KanbanIDBarcode { get; set; }
        public string KanbanID { get; set; }
        public string ImporterCode { get; set; }
        public string ImporterInfo { get; set; }
        public string RenbanNo { get; set; }
        public string KanbanLabelName { get; set; }
        public string Oya { get; set; }
        public string ImgPath { get; set; }
    }

}