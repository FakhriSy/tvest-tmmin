﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010200W
{
    public class CED010200WSearchModel : SearchModel
    {
        public string GROUPING_CODE { get; set; }
        public string GROUPING_NAME { get; set; }
        public string ZONE_CODE { get; set; }
        public string ZONE_NAME { get; set; }
        public string ZONE_STATUS { get; set; }
        public string ZONE_NAME_LINK { get; set; }
        public string LINE_CODE { get; set; }
        public string CHANGED_DT { get; set; }
        public string GROUP_ZONE { get; set; }
    }
}