﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010200W
{
    public class CED010200W
    {
        public string ROW_NO { get; set; }
        public string GROUPING_CODE { get; set; }
        public string GROUPING_NAME { get; set; }
        public string ZONE_CODE { get; set; }
        public string COUNT { get; set; }
        //add by riani (10 03 2019) --> penambahan created dan change
        public string CREATED_BY { get; set; }
        public string CREATED_ON { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_ON { get; set; }
    }

    public class CED010200WZone
    {
        public string ZONE_CODE { get; set; }
        public string ZONE_GROUPING_CODE { get; set; }
        public string ZONE_NAME { get; set; }
        //add by riani (10 03 2019) --> penambahan created dan change, link name, line code
        public string ZONE_NAME_LINK { get; set; }
        public string LINE_CD { get; set; }
        public string ZONE_STATUS { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_ON { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_ON { get; set; }
        public string GROUP { get; set; }
    }
}