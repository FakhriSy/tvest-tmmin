﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010200W
{
    public class CED010200WRepository
    {
        #region Singleton
        private static CED010200WRepository instance = null;
        public static CED010200WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010200WRepository();
                }
                return instance;
            }
        }
        #endregion


        public IList<CED010200W> SearchData(CED010200WSearchModel SearchModel)
        {
            List<CED010200W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    GROUPING_CODE = SearchModel.GROUPING_CODE,
                    GROUPING_NAME = SearchModel.GROUPING_NAME,
                    ZONE_NAME = SearchModel.ZONE_NAME,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010200W>("CED010200W/CED010200WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010200WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.SingleOrDefault<int>("CED010200W/CED010200WSearchCountData", SearchModel);
            }
            finally
            {
                db.Close();
            }
            return result;
        }


        public string SaveData(CED010200W m, int mode, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    GROUPING_CODE = m.GROUPING_CODE,
                    GROUPING_NAME = m.GROUPING_NAME,
                    USER_LOGIN = username,
                    CHANGED_BY = m.CHANGED_BY,
                    CHANGED_DT = m.CHANGED_ON
                };
                result = db.SingleOrDefault<string>("CED010200W/CED010200WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }


        public string DeleteData(CED010200W m, String username)
        {
            string result = null;
            string errorresult ="";
            string errorgroungcode="";
            string[] listroupcode = m.GROUPING_CODE.Split(';');
            string[] listchangedt = m.CHANGED_ON.Split(';');
            //DELETE TRANSACTION
            for (int i = 0; i < listroupcode.Length; i++)
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                try
                {
                    db.BeginTransaction();
                    dynamic args = new
                    {
                        GROUPING_CODE = listroupcode[i],
                        USER_LOGIN = username,
                        CHANGED_DT = listchangedt[i]
                    };
                    result = db.SingleOrDefault<string>("CED010200W/CED010200WDeleteData", args);
                    db.CommitTransaction();
                }
                catch (Exception e)
                {
                    db.AbortTransaction();
                    result = e.Message.ToString();
                }
                finally
                {
                    db.Close();
                    //penambahan untuk message error
                    if(result.Split('|')[0]=="E")
                    {
                        //errorresult = errorresult +"\n"+ "(" + listroupcode[i] + ")" + result.Split('|')[1];
                        errorresult = result;
                    }
                    else
                    {
                        errorresult = errorresult+"";
                    }
                }
            }
            if (errorresult=="")
            {
                return result;
            }
            else
            {
                //return "E|" + errorresult;
                return errorresult;
            }
        }
        public IList<CED010200WZone> SearchDataZone(CED010200WSearchModel SearchModel)
        {
            List<CED010200WZone> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    GROUPING_CODE = SearchModel.GROUPING_CODE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010200WZone>("CED010200W/CED010200WSearchDataZone", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountDataZone(CED010200WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    GROUPING_CODE = SearchModel.GROUPING_CODE
                };

                result = db.SingleOrDefault<int>("CED010200W/CED010200WSearchCountDataZone", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataZone(CED010200WSearchModel m, int mode, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    ZONE_CODE = m.ZONE_CODE,
                    GROUPING_CODE = m.GROUPING_CODE,
                    ZONE_NAME = m.ZONE_NAME,
                    ZONE_STATUS = m.ZONE_STATUS,
                    ZONE_NAME_LINK = m.ZONE_NAME_LINK,
                    LINE_CODE= m.LINE_CODE,
                    USER_LOGIN = username,
                    CHANGED_DT = m.CHANGED_DT,
                    GROUP_ZONE = m.GROUP_ZONE
                };
                result = db.SingleOrDefault<string>("CED010200W/CED010200WSaveDataZone", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteDataZone(CED010200WSearchModel m, String username)
        {
            string result = null;
            //add by riani ( 10 03 2019) --> looping for multiple delete
            string[] listzonecode = m.ZONE_CODE.Split(';');
            for (int i = 0; i < listzonecode.Length; i++)
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                try
                {
                    db.BeginTransaction();
                    dynamic args = new
                    {
                        GROUPING_CODE = m.GROUPING_CODE,
                        ZONE_CODE = listzonecode[i],
                        USER_LOGIN = username,
                        CHANGED_DT = m.CHANGED_DT
                    };
                    result = db.SingleOrDefault<string>("CED010200W/CED010200WDeleteDataZone", args);
                    db.CommitTransaction();
                }
                catch (Exception e)
                {
                    db.AbortTransaction();
                    result = e.Message.ToString();
                }
                finally
                {
                    db.Close();
                }
            }

            return result;
        }
    }
}