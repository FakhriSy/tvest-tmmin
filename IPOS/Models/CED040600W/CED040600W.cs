﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED040600W
{
    public class CED040600W
    {
        public string lngItmNo { get; set; }
        public string lngDIno { get; set; }
        public string strPartNo { get; set; }
        public string strUnique { get; set; }
        public string strPartNm { get; set; }
        public string strSuppCd { get; set; }
        public string strSuppPlt { get; set; }
        public string strSuppAbr { get; set; }
        public string strQKPImage { get; set; }
        public string lngRevNo { get; set; }
        public string strRevItem { get; set; }
        public string strAdmin { get; set; }
        public string dtmTCFrom { get; set; }
        public string dtmTCTo { get; set; }
        public string ProblemId { get; set; }
        public string KanbanId { get; set; }
        public string KanbanType { get; set; }
    }

    public class CED040600WGrid1
    {
        public string lngDIno { get; set; }
        public string strItemCek { get; set; }
        public string strStdValue { get; set; }
        public string strMethode { get; set; }
    }
}