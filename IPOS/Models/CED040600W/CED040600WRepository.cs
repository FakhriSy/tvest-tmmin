﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED040600W
{
    public class CED040600WRepository
    {
        #region Singleton
        private static CED040600WRepository instance = null;
        public static CED040600WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED040600WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED040600W> SearchData(string kup)
        {
            List<CED040600W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KUP = kup
                };
                result = db.Fetch<CED040600W>("CED040600W/CED040600WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040600WGrid1> SearchDataGrid1(string DiNo)
        {
            List<CED040600WGrid1> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DiNo = DiNo
                };
                result = db.Fetch<CED040600WGrid1>("CED040600W/CED040600WSearchDataGrid1", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string FinishProcess(string KanbanId, string StartTime, string userName, string POS)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    KanbanId = KanbanId,
                    StartTime = StartTime,
                    UserLogin = userName,
                    POS = POS
                };
                result = db.SingleOrDefault<string>("CED040600W/CED040600WFinishProcess", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

    }
}