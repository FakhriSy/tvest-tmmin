﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED030900W
{
    public class CED030900WRepository
    {
        #region Singleton
        private static CED030900WRepository instance = null;
        public static CED030900WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED030900WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED030900W> RetrieveData(int cat, string ts)
        {
            List<CED030900W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    CAT = cat,
                    TS = ts
                };
                result = db.Fetch<CED030900W>("CED030900W/CED030900WRetrieveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030900WBindStation> BindTS()
        {
            List<CED030900WBindStation> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {

                };
                result = db.Fetch<CED030900WBindStation>("CED030900W/CED030900WBindTS", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}