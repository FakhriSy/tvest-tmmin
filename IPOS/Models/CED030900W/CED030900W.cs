﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED030900W
{
    public class CED030900W
    {
        public string ManifestPlan1 { get; set; }
        public string ManifestPlan2 { get; set; }
        public string ManifestAct { get; set; }
        public string ManifestDelay { get; set; }
        public string KanbanPlan1 { get; set; }
        public string KanbanPlan2 { get; set; }
        public string KanbanAct { get; set; }
        public string KanbanDelay { get; set; }

        public string Route { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string Dock { get; set; }
        public string OrderNo { get; set; }
        public string ManifestNo { get; set; }
        public string PartNo { get; set; }
        public string TotKbn { get; set; }
        public string PlanCatchUp { get; set; }
        public string Problem { get; set; }
        public string Station { get; set; }
        public string ArrivalPlan { get; set; }
        public string TotalSupp { get; set; }
        public string QtyKanbanPlan { get; set; }
        public string QtyKanbanAct { get; set; }
        public string Bal { get; set; }
        public string Status { get; set; }
    }

    public class CED030900WBindStation
    {
        public string STATION { get; set; }
    }
}