﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010102W
{
    public class CED010102W
    {
        public string ROW_NO { get; set; }
        public string SYS_CAT { get; set; }
        public string SYS_SUB_CAT { get; set; }
        public string SYS_CD { get; set; }
        public string SYS_VAL { get; set; }
        public string REMARK { get; set; }
        public string CREATED_DT { get; set; }
        public string CREATED_BY { get; set; }
        public string UPDATED_DT { get; set; }
        public string UPDATED_BY { get; set; }
    }
}