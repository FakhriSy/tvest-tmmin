﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010102W
{
    public class CED010102WRepository
    {
        #region Singleton
        private static CED010102WRepository instance = null;
        public static CED010102WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010102WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010102W> SearchData(CED010102WSearchModel SearchModel)
        {
            List<CED010102W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SYS_CAT = SearchModel.SysCat,
                    SYS_SUB_CAT = SearchModel.SysSubCat,
                    SYS_VAL = SearchModel.SysVal,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010102W>("CED010102W/CED010102WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010102WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SYS_CAT = SearchModel.SysCat,
                    SYS_SUB_CAT = SearchModel.SysSubCat,
                    SYS_VAL = SearchModel.SysVal
                };
                result = db.SingleOrDefault<int>("CED010102W/CED010102WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED010102W m, int mode, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    SYS_CAT = m.SYS_CAT,
                    SYS_SUB_CAT = m.SYS_SUB_CAT,
                    SYS_CD = m.SYS_CD,
                    SYS_VAL = m.SYS_VAL,
                    USER_LOGIN = username,
                    REMARK = m.REMARK,
                    UPDATED_DT = m.UPDATED_DT,
                    UPDATED_BY = m.UPDATED_BY,
                };
                result = db.SingleOrDefault<string>("CED010102W/CED010102WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteData(CED010102W m)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            //string[] sysCat = m.SYS_CAT.Split(',');
            //string[] sysSub = m.SYS_SUB_CAT.Split(',');
            //string[] sysCd = m.SYS_CD.Split(',');

            try
            {
                db.BeginTransaction();
                //for (int i = 0; i < sysCat.Length; i++)
                //{
                dynamic args = new
                {
                    SYS_CAT = m.SYS_CAT//sysCat[i],
                    //SYS_SUB_CAT = sysSub[i],
                    //SYS_CD = sysCd[i]
                };
                result = db.SingleOrDefault<string>("CED010102W/CED010102WDeleteData", args);
                //}

                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}