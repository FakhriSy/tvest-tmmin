﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010102W
{
    public class CED010102WSearchModel : SearchModel
    {
        public string SysCat { get; set; }
        public string SysSubCat { get; set; }
        public string SysVal { get; set; }
    }
}