﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED031500W
{
    public class CED031500WReceive
    {
        public string MANIFEST_NO { get; set; }
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_NAME { get; set; }
        public string PLANT_NM { get; set; }
        public string PART_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string RCV_PLANT_CD { get; set; }
        public string DOCK_CD { get; set; }
        public string SUPPLIER { get; set; }
        public string DOCK { get; set; }
        public string CHECK_VALIDATE { get; set; }
    }
    public class CED031500W
    {
        public string ROW_NO { get; set; }
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
        public string UNIQUE { get; set; }
        public string KANBAN_NO { get; set; }
        public string ORDER_QTY { get; set; }
        public string RECEIVE_QTY { get; set; }
        public string CHECK_STATUS { get; set; }
        public string CHECK_MANIFEST { get; set; }
        public string KANBAN_TYPE { get; set; }
    }

    public class CED031500WDetail
    {
        public string No { get; set; }
        public string Part_No { get; set; }
        public string Part_Name { get; set; }
        public string Unique { get; set; }
        public string KanbanID { get; set; }
        public string Kanban_Status { get; set; }
        public string Shortage_Pcs { get; set; }
        public string KanbanType { get; set; }
        public string Order_Qty { get; set; }
    }
}