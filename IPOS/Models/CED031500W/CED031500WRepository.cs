﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED031500W
{
    public class CED031500WRepository
    {
        #region Singleton
        private static CED031500WRepository instance = null;
        public static CED031500WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED031500WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED031500WReceive> SearchDataRecive(string param, string username)
        {
            List<CED031500WReceive> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PARAM = param,
                    USERNAME = username
                };
                result = db.Fetch<CED031500WReceive>("CED031500W/CED031500WReceiveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public IList<CED031500W> SearchDataInitial(CED031500WSearchModel SearchModel)
        {
            List<CED031500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    ORDER_NO = SearchModel.ORDER_NO,
                    SUPPLIER_PLANTCODE = SearchModel.SUPPLIER_PLANTCODE,
                    RCV_PLANTCODE = SearchModel.RCV_PLANTCODE,
                    DOCK_CODE = SearchModel.DOCK_CODE,
                    PART_NO = SearchModel.PART_NO,
                    UNIQUE = SearchModel.UNIQUE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031500W>("CED031500W/CED031500WSearchDataInitial", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public IList<CED031500W> SearchData(CED031500WSearchModel SearchModel)
        {
            List<CED031500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    ORDER_NO = SearchModel.ORDER_NO,
                    SUPPLIER_PLANTCODE = SearchModel.SUPPLIER_PLANTCODE,
                    RCV_PLANTCODE = SearchModel.RCV_PLANTCODE,
                    DOCK_CODE = SearchModel.DOCK_CODE,
                    PART_NO = SearchModel.PART_NO,
                    UNIQUE = SearchModel.UNIQUE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031500W>("CED031500W/CED031500WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED031500WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                result = db.SingleOrDefault<int>("CED031500W/CED031500WSearchCountData", SearchModel);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031500WDetail> SearchDataDetail(CED031500WSearchModel SearchModel)
        {
            List<CED031500WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = SearchModel.MANIFEST_NO,
                    ORDER_NO = SearchModel.ORDER_NO,
                    SUPPLIER_CODE = SearchModel.SUPPLIER_CODE,
                    SUPPLIER_PLANTCODE = SearchModel.SUPPLIER_PLANTCODE,
                    RCV_PLANTCODE = SearchModel.RCV_PLANTCODE,
                    DOCK_CODE = SearchModel.DOCK_CODE,
                    PART_NO = SearchModel.PART_NO,
                    UNIQUE = SearchModel.UNIQUE,
                    PART_BARCODE = SearchModel.PART_BARCODE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031500WDetail>("CED031500W/CED031500WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED031500WSearchModel m, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = m.MANIFEST_NO,
                    PART_BARCODE = m.PART_BARCODE,
                    FULL_STATUS = m.FULL_STATUS,
                    PCS_STATUS = m.PCS_STATUS,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<string>("CED031500W/CED031500WSavedataDetail", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataFlag(string ManifestNo, string PartNo, string CheckType)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = ManifestNo,
                    PART_NO = PartNo,
                    CHECK_TYPE = CheckType
                };
                result = db.SingleOrDefault<string>("CED031500W/CED031500WSavedataFlag", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataTempHeader(CED031500WSearchModel m, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = m.MANIFEST_NO,
                    PART_NO = m.PART_NO,
                    REC_STATUS = m.REC_STATUS,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<string>("CED031500W/CED031500WSavedataTempHeader", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveDataReceive(string ManifestNo, string username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MANIFEST_NO = ManifestNo,
                    USER_LOGIN = username,
                };
                result = db.SingleOrDefault<string>("CED031500W/CED031500WSavedataReceive", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}