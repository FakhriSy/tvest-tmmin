﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010300W
{
    public class CED010300WSearchModel : SearchModel
    {
        public string PartNo { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierPlantCode { get; set; }
        public string LATEST_VALUE { get; set; }
    }
}