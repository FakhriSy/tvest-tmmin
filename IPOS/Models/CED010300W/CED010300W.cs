﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010300W
{
    public class CED010300W
    {
        public string SupplierCode { get; set; }
        public string SupplierPlantCode { get; set; }
        public string PartNo { get; set; }
        public string PartNos { get; set; }
        public string UniqueNo { get; set; }
        public string QkpImage { get; set; }
        public string RevItem { get; set; }
        public string RevNo { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
    }

    public class LockupSupplier
    {
        public string SUPPLIER_CD { get; set; }
        public string SUPPLIER_PLANT_CD { get; set; }
        public string SUPPLIER_ABBREVIATION { get; set; }
        public string SUPPLIER_NAME { get; set; }
    }
    public class LockupPart
    {
        public string PART_NO { get; set; }
        public string PART_NAME { get; set; }
    }
}