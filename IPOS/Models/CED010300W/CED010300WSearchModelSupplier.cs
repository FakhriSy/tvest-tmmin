﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010300W
{
    public class CED010300WSearchModelSupplier : SearchModel
    {
        public string SupplierCode { get; set; }
        public string SupplierName { get; set; }
    }
}