﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010300W
{
    public class CED010300WSearchModelPart : SearchModel
    {
        public string PartNo { get; set; }
        public string PartName { get; set; }
    }
}