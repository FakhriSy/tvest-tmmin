﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010300W
{
    public class CED010300WRepository
    {
        #region Singleton
        private static CED010300WRepository instance = null;
        public static CED010300WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010300WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010300W> SearchData(CED010300WSearchModel SearchModel)
        {
            List<CED010300W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    SupplierCode = SearchModel.SupplierCode,
                    SupplierPlantCode = SearchModel.SupplierPlantCode,
                    LATEST_VALUE = SearchModel.LATEST_VALUE,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010300W>("CED010300W/CED010300WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010300WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    SupplierCode = SearchModel.SupplierCode,
                    SupplierPlantCode = SearchModel.SupplierPlantCode,
                    LATEST_VALUE = SearchModel.LATEST_VALUE
                };
                result = db.SingleOrDefault<int>("CED010300W/CED010300WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED010300W m, int mode, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    SupplierCode = m.SupplierCode,
                    SupplierPlantCode = m.SupplierPlantCode,
                    PartNo = m.PartNo,
                    UniqueNo = m.UniqueNo,
                    QkpImage = m.QkpImage,
                    RevItem = m.RevItem,
                    RevNo = m.RevNo,
                    DateFrom = m.DateFrom,
                    DateTo = m.DateTo,
                    USER_LOGIN = username,
                    CHANGED_DT = Convert.ToDateTime(m.CHANGED_DT).ToString("yyyy-MM-dd HH:mm:ss"),
                    CHANGED_BY = m.CHANGED_BY
                };
                result = db.SingleOrDefault<string>("CED010300W/CED010300WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteData(CED010300W m, string username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    PARAMKEY = m.PartNo,
                    SupplierCode = m.SupplierCode,
                    SupplierPlantCode = m.SupplierPlantCode,
                    PartNos = m.PartNos,
                    DateTo = m.DateTo,
                    CHANGED_BY = username
                };
                result = db.SingleOrDefault<string>("CED010300W/CED010300WDeleteData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<LockupSupplier> SearchDataSupplier(CED010300WSearchModelSupplier SearchModel)
        {
            List<LockupSupplier> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SupplierCode = SearchModel.SupplierCode,
                    SupplierName = SearchModel.SupplierName,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<LockupSupplier>("CED010300W/CED010300WSearchDataSupplier", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountDataSupplier(CED010300WSearchModelSupplier SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SupplierCode = SearchModel.SupplierCode,
                    SupplierName = SearchModel.SupplierName
                };
                result = db.SingleOrDefault<int>("CED010300W/CED010300WSearchCountDataSupplier", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<LockupPart> SearchDataPart(CED010300WSearchModelPart SearchModel)
        {
            List<LockupPart> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    PartName = SearchModel.PartName,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<LockupPart>("CED010300W/CED010300WSearchDataPart", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountDataPart(CED010300WSearchModelPart SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = SearchModel.PartNo,
                    PartName = SearchModel.PartName
                };
                result = db.SingleOrDefault<int>("CED010300W/CED010300WSearchCountDataPart", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IEnumerable<comboBox> getComboboxCode(String comboBoxID, String comboBoxParam)
        {
            IEnumerable<comboBox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    COMBO_ID = comboBoxID,
                    COMBO_PARAM = comboBoxParam
                };
                result = db.Fetch<comboBox>("Common/GetComboBox", args);
            }
            finally
            {
                db.Close();
            }
            var _listresust = result.ToList().Select(x => new comboBox { NAME = x.NAME.Trim().Split(':')[0], PARENT = x.PARENT, VALUE = x.VALUE });
            return _listresust;
        }

    }
}