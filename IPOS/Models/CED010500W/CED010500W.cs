﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010500W
{
    public class CED010500W
    {
        public string ROW_NO { get; set; }
        public string PACKING_PLANT { get; set; }
        public string PCK_MTH { get; set; }
        public string D_1 { get; set; }
        public string D_2 { get; set; }
        public string D_3 { get; set; }
        public string D_4 { get; set; }
        public string D_5 { get; set; }
        public string D_6 { get; set; }
        public string D_7 { get; set; }
        public string D_8 { get; set; }
        public string D_9 { get; set; }
        public string D_10 { get; set; }
        public string D_11 { get; set; }
        public string D_12 { get; set; }
        public string D_13 { get; set; }
        public string D_14 { get; set; }
        public string D_15 { get; set; }
        public string D_16 { get; set; }
        public string D_17 { get; set; }
        public string D_18 { get; set; }
        public string D_19 { get; set; }
        public string D_20 { get; set; }
        public string D_21 { get; set; }
        public string D_22 { get; set; }
        public string D_23 { get; set; }
        public string D_24 { get; set; }
        public string D_25 { get; set; }
        public string D_26 { get; set; }
        public string D_27 { get; set; }
        public string D_28 { get; set; }
        public string D_29 { get; set; }
        public string D_30 { get; set; }
        public string D_31 { get; set; }
        public string WD_1 { get; set; }
        public string WD_2 { get; set; }
        public string WD_3 { get; set; }
        public string WD_4 { get; set; }
        public string WD_5 { get; set; }
        public string WD_6 { get; set; }
        public string WD_7 { get; set; }
        public string WD_8 { get; set; }
        public string WD_9 { get; set; }
        public string WD_10 { get; set; }
        public string WD_11 { get; set; }
        public string WD_12 { get; set; }
        public string WD_13 { get; set; }
        public string WD_14 { get; set; }
        public string WD_15 { get; set; }
        public string WD_16 { get; set; }
        public string WD_17 { get; set; }
        public string WD_18 { get; set; }
        public string WD_19 { get; set; }
        public string WD_20 { get; set; }
        public string WD_21 { get; set; }
        public string WD_22 { get; set; }
        public string WD_23 { get; set; }
        public string WD_24 { get; set; }
        public string WD_25 { get; set; }
        public string WD_26 { get; set; }
        public string WD_27 { get; set; }
        public string WD_28 { get; set; }
        public string WD_29 { get; set; }
        public string WD_30 { get; set; }
        public string WD_31 { get; set; }
        public string TOTAL_WORKING_DAY { get; set; }
    }
}