﻿using System;
using System.Collections.Generic;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010500W
{
    public class CED010500WRepository
    {
        #region Singleton
        private static CED010500WRepository instance = null;
        public static CED010500WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010500WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010500W> SearchData(CED010500WSearchModel SearchModel)
        {
            List<CED010500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    YEAR = SearchModel.Year,
                    MONTH = SearchModel.Month,
                    PACKING_PLANT = SearchModel.PackingPlant,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010500W>("CED010500W/CED010500WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010500WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    YEAR = SearchModel.Year,
                    MONTH = SearchModel.Month,
                    PACKING_PLANT = SearchModel.PackingPlant
                };
                result = db.SingleOrDefault<int>("CED010500W/CED010500WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED010500WSaveModel SaveModel, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    PACKING_PLANT = SaveModel.PACKING_PLANT,
                    DATE = SaveModel.DATE,
                    WORKING_DAY = SaveModel.WORKING_DAY,
                    SHIFT = SaveModel.SHIFT,
                    USER_LOGIN = username
                };
                result = db.SingleOrDefault<string>("CED010500W/CED010500WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string CreateCalendar()
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new { };
                result = db.SingleOrDefault<string>("CED010500W/CED010500WCreateCalendar", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}