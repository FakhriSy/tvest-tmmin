﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010500W
{
    public class CED010500WSaveModel
    {
        public string PACKING_PLANT { get; set; }
        public string DATE { get; set; }
        public string WORKING_DAY {  get; set; }
        public string SHIFT { get; set; }
    }
}