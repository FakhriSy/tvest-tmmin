﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010500W
{
    public class CED010500WSearchModel : SearchModel
    {
        public string Year { get; set; }
        public string Month { get; set; }
        public string PackingPlant { get; set; }
    }
}