﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED021500W
{
    public class CED021500WRepository
    {
        #region Singleton
        private static CED021500WRepository instance = null;
        public static CED021500WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED021500WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED021500W> SearchData(CED021500WSearchModel SearchModel, String pid)
        {
            List<CED021500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.SetCommandTimeout(1800);
            try
            {
                dynamic args = new
                {
                    Process = SearchModel.Process,
                    Param = SearchModel.Param, //[WOT] - Add Param to Search/EndDate
                    Type = SearchModel.Type,
                    pid = pid,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED021500W>("CED021500W/CED021500WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED021500WSearchModel SearchModel, String pid)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.SetCommandTimeout(1800);
            try
            {
                dynamic args = new
                {
                    Process = SearchModel.Process,
                    Type = SearchModel.Type,
                    pid = pid,

                };
                result = db.SingleOrDefault<int>("CED021500W/CED021500WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED021500WSearchModel SearchModel, String pid)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.SetCommandTimeout(1800);
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Process = SearchModel.Process,
                    Param = SearchModel.Param,
                    pid = pid,
                };
                result = db.SingleOrDefault<string>("CED021500W/CED021500WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                result = e.InnerException.ToString();
                result = e.Message.ToString();
                db.AbortTransaction();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteTempData(CED021500WSearchModel SearchModel, String pid)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.SetCommandTimeout(1800);
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Process = SearchModel.Process,
                    pid = pid,
                };
                result = db.SingleOrDefault<string>("CED021500W/CED021500WDeleteTempData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                result = e.InnerException.ToString();
                result = e.Message.ToString();
                db.AbortTransaction();
            }
            return result;
        }

        public IEnumerable<comboBox> getComboboxCode(String comboBoxID, String comboBoxParam)
        {
            IEnumerable<comboBox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    COMBO_ID = comboBoxID,
                    COMBO_PARAM = comboBoxParam
                };
                result = db.Fetch<comboBox>("Common/GetComboBox", args);
            }
            finally
            {
                db.Close();
            }
            var _listresust = result.ToList().Select(x => new comboBox { NAME = x.NAME.Trim().Split(':')[0], PARENT = x.PARENT, VALUE = x.VALUE });
            return _listresust;
        }

    }
}