﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED021500W
{
    public class CED021500WSearchModel : SearchModel
    {
        public string Process { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
        public string Shift { get; set; }
        public string DivLane { get; set; }
        public string Zone { get; set; }
        public string ShutterNo { get; set; }
        public string ProblemID { get; set; }
        public string Supplier { get; set; }
        public string ManifestNo { get; set; }
        public string OrderNo { get; set; }
        public string ShutterSts { get; set; }
        public string PickingSts { get; set; }
        public string PackingLine { get; set; }
        public string ModuleSts { get; set; }
        public string PhysicalDock { get; set; }
        public string PartNo { get; set; }
        public string KanbanID { get; set; }
        public string DescCd { get; set; }
        public string LotNo { get; set; }
        public string CaseNo { get; set; }
        public string ModuleType { get; set; }
        public string LogicalDock { get; set; }
        public string TruckStation { get; set; }
        public string RouteName { get; set; }
        public string Renban { get; set; }
        public string StackingSts { get; set; }
        public string FillInSts { get; set; }
        public string RouteCycle { get; set; }
        public string PassThrough { get; set; }
        public string BoxingSts { get; set; }
        public string VanningSts { get; set; }
        public string ProblemDtFrom { get; set; }
        public string ProblemDtTo { get; set; }
        public string ProblemType { get; set; }

        public string AddressNo { get; set; }
        public string SupplyStatus { get; set; }

        public string Param { get; set; }
        public string Type { get; set; }
    }
}