﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED030300W
{
    public class CED030300W
    {
        public string TS { get; set; }
        public string Dock { get; set; }
        public string Remaining { get; set; }
        public string Route { get; set; }
        public string Cycle { get; set; }
        public string YardArrival { get; set; }
        public string TSArrivalPlan { get; set; }
        public string TSArrivalActual { get; set; }
        public string TSArrivalGAP { get; set; }
        public string TSDeparturePlan { get; set; }
        public string TSDepartureActual { get; set; }
        public string TSDepartureGAP { get; set; }
        public string ArrPlan { get; set; }
    }

    public class CED030300WValidate
    {
        public string MSG { get; set; }
    }
}