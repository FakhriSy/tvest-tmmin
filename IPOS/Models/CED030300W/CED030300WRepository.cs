﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED030300W
{
    public class CED030300WRepository
    {
        #region Singleton
        private static CED030300WRepository instance = null;
        public static CED030300WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED030300WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED030300W> SearchData(string dockNo, string deliveryNo)
        {
            List<CED030300W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DOCK_CD = dockNo,
                    DELIVERY_NO = deliveryNo
                };
                result = db.Fetch<CED030300W>("CED030300W/CED030300WRetrieveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030300WValidate> ValidateData(string dockNo, string deliveryNo, string userName)
        {
            List<CED030300WValidate> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DOCK_CD = dockNo,
                    DELIVERY_NO = deliveryNo,
                    USER_LOGIN = userName
                };
                result = db.Fetch<CED030300WValidate>("CED030300W/CED030300WValidateData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030300WValidate> CheckDockNo(string dockNo)
        {
            List<CED030300WValidate> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DOCK_CD = dockNo
                };
                result = db.Fetch<CED030300WValidate>("CED030300W/CED030300WCheckDockNo", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030300WValidate> CheckDeliveryNo(string dockNo, string deliveryNo, string userName)
        {
            List<CED030300WValidate> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DOCK_CD = dockNo,
                    DELIVERY_NO = deliveryNo,
                    USER_LOGIN = userName
                };
                result = db.Fetch<CED030300WValidate>("CED030300W/CED030300WCheckDeliveryNo", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030300WValidate> SubmitSkid(string dockNo, string deliveryNo, string totalSkid, string userName)
        {
            List<CED030300WValidate> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DOCK_CD = dockNo,
                    DELIVERY_NO = deliveryNo,
                    TOTAL_SKID = totalSkid,
                    USER_LOGIN = userName
                };
                result = db.Fetch<CED030300WValidate>("CED030300W/CED0303600WSubmitTotalSkid", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}