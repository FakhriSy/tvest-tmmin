﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED030300W
{
    public class CED030300WSearchModel : SearchModel
    {
        public string MANIFEST_NO { get; set; }
        public string ORDER_NO { get; set; }
        public string SUPPLIER_CODE { get; set; }
        public string SUPPLIER_PLANTCODE { get; set; }
        public string RCV_PLANTCODE { get; set; }
        public string DOCK_CODE { get; set; }
        public string PART_NO { get; set; }
        public string UNIQUE { get; set; }
    }
}