﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED031600W
{
    public class CED031600W
    {
        public string PACKING_PLANT_CD { get; set; }
        public string PKG_MTH { get; set; }
        public string GROUP_PKG_LINE { get; set; }
        public string TOTAL_POS { get; set; }
        public string MP { get; set; }
        public float DLY_OVERTIME { get; set; }
        public string TOTAL_MODULE { get; set; }
        public string DLY_MODULE { get; set; }
        public string TOTAL_MP { get; set; }
        public string DLY_WORK_HOUR { get; set; }
        public string TOTAL_WORK_HOUR { get; set; }
        public string type { get; set; }
    }
}