﻿using IPOS.Models.CED010500W;
using System;
using System.Collections.Generic;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED031600W
{
    public class CED031600WRepository
    {
        private static CED031600WRepository instance = null;
        public static CED031600WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED031600WRepository();
                }
                return instance;
            }
        }
        public IList<CED031600W> SearchData(CED031600WSearchModel SearchModel)
        {
            List<CED031600W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PACKING_MONTH = SearchModel.PackingMonth,
                    PACKING_PLANT = SearchModel.PackingPlant,
                    type = SearchModel.type,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED031600W>("CED031600W/CED031600WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED031600WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PACKING_MONTH = SearchModel.PackingMonth,
                    PACKING_PLANT = SearchModel.PackingPlant,
                    type = SearchModel.type
                };
                result = db.SingleOrDefault<int>("CED031600W/CED031600WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

    }
}