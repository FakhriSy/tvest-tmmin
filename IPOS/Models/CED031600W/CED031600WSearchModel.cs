﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED031600W
{
    public class CED031600WSearchModel : SearchModel
    {
        public string PackingMonth { get; set; }
        public string PackingPlant { get; set; }
        public string type { get; set; }
    }
}