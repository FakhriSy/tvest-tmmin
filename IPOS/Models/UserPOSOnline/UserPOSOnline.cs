﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.UserPOSOnline
{
    public class UserPOSOnline
    {
        public string USER_ID { get; set; }
        public string POS { get; set; }
        public string IP { get; set; }
        public string FORM { get; set; }
        public string LOGIN_DATE { get; set; }
    }
}