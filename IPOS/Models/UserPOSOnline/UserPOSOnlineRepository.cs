﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.UserPOSOnline
{
    public class UserPOSOnlineRepository
    {
        #region Singleton
        private static UserPOSOnlineRepository instance = null;
        public static UserPOSOnlineRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new UserPOSOnlineRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<UserPOSOnline> SearchData(UserPOSOnlineSearchModel m)
        {
            List<UserPOSOnline> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    USER_ID = m.UserId,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<UserPOSOnline>("UserPOSOnline/UserPOSOnlineSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(UserPOSOnlineSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    USER_ID = m.UserId
                };
                result = db.SingleOrDefault<int>("UserPOSOnline/UserPOSOnlineSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteData(string UserId, string POS)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    USER_ID = UserId,
                    POS = POS
                };
                result = db.SingleOrDefault<string>("UserPOSOnline/DeleteData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}