﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.UserPOSOnline
{
    public class UserPOSOnlineSearchModel : SearchModel
    {
        public string UserId { get; set; }
    }
}