﻿using IPOS.Models.CED020200W;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Utilities;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010618W
{
    public class CED010618WRepository
    {
            #region Singleton
            private static CED010618WRepository instance = null;
            public static CED010618WRepository Instance
            {
                get
                {
                    if (instance == null)
                    {
                        instance = new CED010618WRepository();
                    }
                    return instance;
                }
            }
            #endregion
            public IList<CED010618W> SearchModuleType(CED010618WSearchModuleTypeModel m)
            {
                List<CED010618W> result = null;
                IDBContext db = DatabaseManager.Instance.GetContext();
                try
                {
                    dynamic args = new
                    {
                        MOD_ID = m.ModuleType,
                        ROW_START = m.Start,
                        ROW_END = m.End
                    };
                    result = db.Fetch<CED010618W>("CED010618W/CED010618WSearchModuleType", args);
                }
                finally
                {
                    db.Close();
                }
                return result;
            }

            public int CountModuleType(CED010618WSearchModuleTypeModel m)
            {
                int result = 0;
                IDBContext db = DatabaseManager.Instance.GetContext();
                try
                {
                    dynamic args = new
                    {
                        MOD_ID = m.ModuleType
                    };
                    result = db.SingleOrDefault<int>("CED010618W/CED010618WCountModuleType", args);
                }
                finally
                {
                    db.Close();
                }
                return result;
            }

        public IList<CED010618W> SearchPartJundate(CED010618WSearchPartJundateModel m)
        {
            List<CED010618W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PART_NO = m.PartNo,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED010618W>("CED010618W/CED010618WSearchPartJundate", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int CountPartJundate(CED010618WSearchPartJundateModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PART_NO = m.PartNo
                };
                result = db.SingleOrDefault<int>("CED010618W/CED010618WCountPartJundate", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveModuleType(CED010618W m, int mode, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    MOD_ID = m.MOD_ID,
                    MODUL_TYPE_CONV = m.MODUL_TYPE_CONV,
                    CHANGED_DT = m.CHANGED_DT,
                    USER_LOGIN = userLogin
                };
                result = db.SingleOrDefault<string>("CED010618W/CED010618WSaveModuleType", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SavePartJundate(CED010618W m, int mode, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    PART_NO = m.PART_NO,
                    UNIQUE_NO = m.UNIQUE_NO,
                    ZONE_CD = m.ZONE_CD,
                    CHANGED_DT = m.CHANGED_DT,
                    USER_LOGIN = userLogin
                };
                result = db.SingleOrDefault<string>("CED010618W/CED010618WSavePartJundate", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteModuleType(CED010618W m)
        {
            string result = null;
            
            string[] listModId = m.MOD_ID.Split(';');
            string[] listChangedDt = m.CHANGED_DT.Split(';');
            bool isError = false;
            //DELETE TRANSACTION
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.BeginTransaction();
            try
            {
                for (int i = 0; i < listChangedDt.Length - 1; i++)
                {
                    dynamic args = new
                    {
                        MOD_ID = listModId[i],
                        CHANGED_DT = listChangedDt[i]
                    };
                    result = db.SingleOrDefault<string>("CED010618W/CED010618WDeleteModuleType", args);
                    if (result.Split('|')[0] == "E")
                    {
                        isError = true;
                        break;
                    }
                }
                if (isError)
                {
                    db.AbortTransaction();
                }
                else { 
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();  
            }
            return result;
        }

        public string DeletePartJundate(CED010618W m)
        {
            string result = null;
            string[] listPartNo = m.PART_NO.Split(';');
            string[] listChangedDt = m.CHANGED_DT.Split(';');
            bool isError = false;
            //DELETE TRANSACTION
            IDBContext db = DatabaseManager.Instance.GetContext();
            db.BeginTransaction();
            try
            {
                for (int i = 0; i < listChangedDt.Length - 1; i++)
                {
                    dynamic args = new
                    {
                        PART_NO = listPartNo[i],
                        CHANGED_DT = listChangedDt[i]
                    };
                    result = db.SingleOrDefault<string>("CED010618W/CED010618WDeletePartJundate", args);
                    if (result.Split('|')[0] == "E")
                    {
                        isError = true;
                        break;
                    }
                }
                if (isError)
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }

    

}