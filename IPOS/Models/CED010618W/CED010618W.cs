﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010618W
{
    public class CED010618W
    {
        public string NO { get; set; }
        public string MOD_ID { get; set; }
        public string MODUL_TYPE_CONV { get; set; }

        public string PART_NO { get; set; }
        public string UNIQUE_NO { get; set; }
        public string ZONE_CD { get; set; }

        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
    }
    /*public class CED010618WModuleType
    {
        public string NO { get; set; }
        public string MOD_ID { get; set; }
        public string MODUL_TYPE_CONV { get; set;}
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get;set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get;set; }


    }
    public class CED010618WPartJundate
    {
        public string NO { get; set; }
        public string PART_NO { get; set; }
        public string UNIQUE_NO { get; set; }
        public string ZONE_CD { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get; set; }
    }*/
}