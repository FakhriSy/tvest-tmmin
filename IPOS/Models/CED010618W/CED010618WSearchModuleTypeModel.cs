﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010618W
{
    public class CED010618WSearchModuleTypeModel : SearchModel
    {
        public string ModuleType { get; set; }
    }

    public class CED010618WSearchPartJundateModel : SearchModel
    {
        public string PartNo { get; set; }
    }
}