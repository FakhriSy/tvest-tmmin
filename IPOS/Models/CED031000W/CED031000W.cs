﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED031000W
{
    public class CED031000W
    {
        public string PlanQtyCurrent { get; set; }
        public string PlanQty { get; set; }
        public string ActQty { get; set; }
        public string DelayQty { get; set; }
        public string SuppCode { get; set; }
        public string ManifestNo { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string Renban { get; set; }
        public string ArrivalPlan { get; set; }
        public string Status { get; set; }
        public string PlanCatchUp { get; set; }
        public string Problem { get; set; }
        public string CountUp { get; set; }
        public string SuppName { get; set; }
        public string QtyCasePlan { get; set; }
        public string QtyCaseAct { get; set; }
        public string Progress { get; set; }
        public string Dock { get; set; }
        public string OrderNo { get; set; }
    }
}