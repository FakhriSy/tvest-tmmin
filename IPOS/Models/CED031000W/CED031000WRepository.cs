﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED031000W
{
    public class CED031000WRepository
    {
        #region Singleton
        private static CED031000WRepository instance = null;
        public static CED031000WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED031000WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED031000W> RetrieveData(int cat)
        {
            List<CED031000W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    CAT = cat
                };
                result = db.Fetch<CED031000W>("CED031000W/CED031000WRetrieveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}