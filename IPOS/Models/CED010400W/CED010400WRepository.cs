﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010400W
{
    public class CED010400WRepository
    {
        #region Singleton
        private static CED010400WRepository instance = null;
        public static CED010400WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010400WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010400W> SearchData(CED010400WSearchModel m)
        {
            List<CED010400W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Table = m.Table,
                    PartNo = m.Part_No,
                    LineId = m.Line_Id,
                    PatternId = m.Pattern_Id,
                    DockCode = m.Dock_Code,
                    PackingLine = m.Packing_Line,
                    Passthru = m.Passthru_Flag,
                    Picking = m.Picking_Flag,
                    Boxing = m.Boxing_Flag,
                    Stacking = m.Stacking_Flag,
                    FloorRack = m.FloorRack_Flag,
                    showLatest = m.showLatest,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED010400W>("CED010400W/CED010400WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010400WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Table = m.Table,
                    PartNo = m.Part_No,
                    LineId = m.Line_Id,
                    PatternId = m.Pattern_Id,
                    DockCode = m.Dock_Code,
                    PackingLine = m.Packing_Line,
                    Passthru = m.Passthru_Flag,
                    Picking = m.Picking_Flag,
                    Boxing = m.Boxing_Flag,
                    Stacking = m.Stacking_Flag,
                    FloorRack = m.FloorRack_Flag,
                    showLatest = m.showLatest
                };
                result = db.SingleOrDefault<int>("CED010400W/CED010400WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED010400W m, int mode, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    Table = m.Table,
                    Part_No = m.Part_No,
                    Dock_Code = m.Dock_Code,
                    Line_Id = m.Line_Id,
                    Pattern_Id = m.Pattern_Id,
                    Packing_Line=m.Packing_Line,
                    Boxing_Flag = m.Boxing_Flag,
                    Picking_Flag = m.Picking_Flag,
                    FloorRack_Flag = m.FloorRack_Flag,
                    Stacking_Flag = m.Stacking_Flag,
                    ValidFrom = m.ValidFrom,
                    ValidTo = m.ValidTo,
                    Cycle_Time = m.Cycle_Time,
                    Passthru_Flag = m.Passthru_Flag,
                    Rack_Address = m.Rack_Address,
                    Max_Stock_Rack = m.Max_Stock_Rack,
                    Qty_Box = m.Qty_Box,
                    Qty_Kanban = m.Qty_Kanban,
                    Bkp_Image = m.Bkp_Image,
                    Pkp_Image = m.Pkp_Image,
                    Ps1_Image = m.Ps1_Image,
                    Ps2_Image = m.Ps2_Image,
                    Ds_Image = m.Ds_Image,

                    USER_LOGIN = userLogin,
                    ValidFrom_Hide = m.ValidFrom_Hide,
                    ValidFrom_Minus=m.ValidFrom_Minus,
                    Changed_Date=m.Changed_Date

                };
                result = db.SingleOrDefault<string>("CED010400W/CED010400WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteData(List<CED010400W> model,string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                foreach (CED010400W row in model)
                {
                    dynamic args = new
                    {
                        Table = row.Table,
                        Part_No = row.Part_No,
                        Dock_Code = row.Dock_Code,
                        Packing_Line = row.Packing_Line,
                        Line_Id = row.Line_Id,
                        Pattern_Id = row.Pattern_Id,
                        ValidFrom = row.ValidFrom,
                        USER_LOGIN = userLogin
                    };
                    result = db.SingleOrDefault<string>("CED010400W/CED010400WDeleteData", args);
                    if (result.Split('|')[0] == "E")
                    {
                        break;
                    }
                }
                if (result.Split('|')[0] == "E")
                {
                    db.AbortTransaction();
                }
                else
                {
                    db.CommitTransaction();
                }
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message;
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}