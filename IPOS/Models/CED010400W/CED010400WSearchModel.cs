﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010400W
{
    public class CED010400WSearchModel : SearchModel
    {
        public string Table { get; set; }
        public string Part_No { get; set; }
        public string Line_Id { get; set; }
        public string Pattern_Id { get; set; }
        public string Dock_Code { get; set; }
        public string Packing_Line { get; set; }
        public string Passthru_Flag { get; set; }
        public string Picking_Flag { get; set; }
        public string Boxing_Flag { get; set; }
        public string Stacking_Flag { get; set; }
        public string FloorRack_Flag { get; set; }
        public string showLatest { get; set; }
    }
}