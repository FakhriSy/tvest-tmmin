﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010400W
{
    public class CED010400W
    {
        public string Table { get; set; }
        public string No { get; set; }
        public string Part_No { get; set; }
        public string Dock_Code { get; set; }
        public string Line_Id { get; set; }
        public string Pattern_Id { get; set; }
        public string Packing_Line { get; set; }
        public string Boxing_Flag { get; set; }
        public string Picking_Flag { get; set; }
        public string FloorRack_Flag { get; set; }
        public string Stacking_Flag { get; set; }
        public string ValidFrom { get; set; }
        public string ValidTo { get; set; }
        public string Cycle_Time { get; set; }
        //public string Cycle_ValidFrom { get; set; }
        //public string Cycle_ValidTo { get; set; }
        public string Passthru_Flag { get; set; }
        //public string Passthru_ValidFrom { get; set; }
        //public string Passthru_ValidTo { get; set; }
        public string Created_By { get; set; }
        public string Created_Date { get; set; }
        public string Changed_By { get; set; }
        public string Changed_Date { get; set; }
        public string ValidFrom_Hide { get; set; }
        public string ValidFrom_Minus { get; set; }

        public string Rack_Address { get; set; }
        public string Max_Stock_Rack { get; set; }
        public string Qty_Box { get; set; }
        public string Qty_Kanban { get; set; }
        public string Bkp_Image { get; set; }
        public string Pkp_Image { get; set; }
        public string Ps1_Image { get; set; }
        public string Ps2_Image { get; set; }
        public string Ds_Image { get; set; }

        public string Bkp_Path { get; set; }
        public string Pkp_Path { get; set; }
        public string Ps1_Path { get; set; }
        public string Ps2_Path { get; set; }
        public string Ds_Path { get; set; }


    }
}