﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.Common
{
    public class Common
    {
    }

    public class comboBox
    {
        public String NAME { get; set; }
        public String VALUE { get; set; }
        public String PARENT { get; set; }
    }

    public class resultMessage
    {
        public const string Info = "I";
        public const string Success = "S";
        public const string Error = "E";

        public string MSG_TXT { get; set; }
        public string MSG_TYPE { get; set; }
        public string PROCESS_ID { get; set; }
    }

    public class commonLog
    {
        public const string Success = "0";
        public const string BusinessError = "1";
        public const string SystemError = "2";
        public const string Warning = "3";

        public String msgText { get; set; }
        public String userID { get; set; }
        public String logLocation { get; set; }
        public String processID { get; set; }
        public String msgID { get; set; }
        public String msgType { get; set; }
        public String modulID { get; set; }
        public String functionID { get; set; }
        public String processSts { get; set; }
    }

    public class listLine
    {
        public String LINE_CD { get; set; }
        public String LINE_DESC { get; set; }
    }

    public class listCat
    {
        public String MAT_CAT_CD { get; set; }
        public String MAT_CAT_DESC { get; set; }
    }

    public class listPlant
    {
        public String PLANT_ID { get; set; }
        public String PLANT_DESC { get; set; }
    }

    public class listUOM
    {
        public String UOM_ID { get; set; }
        public String UOM_NM { get; set; }
    }

    public class listMaster
    {
        public String SYS_CD { get; set; }
        public String SYS_VAL { get; set; }
    }
}