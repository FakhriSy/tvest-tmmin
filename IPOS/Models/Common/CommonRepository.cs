﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.Common
{
    public class CommonRepository
    {
        #region Singleton
        private static CommonRepository instance = null;
        public static CommonRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CommonRepository();
                }
                return instance;
            }
        }
        #endregion

        public string GetFunctionName(string ModuleID, string FunctionID)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MODULE_ID = ModuleID,
                    FUNCTION_ID = FunctionID
                };
                result = db.SingleOrDefault<string>("Common/GetFunctionName", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public IEnumerable<comboBox> getCombobox(String comboBoxID, String comboBoxParam)
        {
            IEnumerable<comboBox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    COMBO_ID = comboBoxID,
                    COMBO_PARAM = comboBoxParam
                };
                result = db.Fetch<comboBox>("Common/GetComboBox", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public List<Int32> getRecordPage()
        {
            List<Int32> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                };
                result = db.Fetch<Int32>("Common/GetRecordPage", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string getMessageText(string MsgID, string param)
        {
            var result = "";
            IDBContext db = DatabaseManager.Instance.GetContext();
            String[] parameter = new String[7];
            parameter = ";;;;;;".Split(';');

            if (!String.IsNullOrEmpty(param))
            {
                for (int i = 0; i < param.Split(';').Length; i++)
                {
                    parameter[i] = param.Split(';')[i];
                }
            }

            try
            {
                dynamic args = new
                {
                    MSG_ID = MsgID,
                    PARAM_1 = parameter[0],
                    PARAM_2 = parameter[1],
                    PARAM_3 = parameter[2],
                    PARAM_4 = parameter[3],
                    PARAM_5 = parameter[4],
                    PARAM_6 = parameter[5],
                    PARAM_7 = parameter[6]
                };
                result = db.SingleOrDefault<string>("Common/GetMessageText", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public String getSystemMasterValue(string sysCat, string sysSubCat, string sysCd)
        {
            string result = "";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SYS_CAT = sysCat,
                    SYS_SUB_CAT = sysSubCat,
                    SYS_CD = sysCd
                };
                result = db.SingleOrDefault<String>("Common/GetSystemMaster", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<listMaster> getDataMaster(string sysCat, string sysSubCat)
        {
            IList<listMaster> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    SYS_CAT = sysCat,
                    SYS_SUB_CAT = sysSubCat
                };
                result = db.Fetch<listMaster>("Common/GetMaster", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public String doInsertLog(commonLog paramLog)
        {
            String result = String.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    p_messages = paramLog.msgText,
                    p_user = paramLog.userID,
                    p_location = paramLog.logLocation,
                    p_pid = paramLog.processID,
                    p_msg_id = paramLog.msgID,
                    p_msg_type = paramLog.msgType,
                    p_module_id = paramLog.modulID,
                    p_function_id = paramLog.functionID,
                    p_process_status = paramLog.processSts
                };
                result = db.SingleOrDefault<String>("Common/InsertLog", args);
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public String doSetLock(String processID, String functionID, String lockReff, String remark, String userID, String mode)
        {
            String result = String.Empty;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    p_processID = processID,
                    p_functionID = functionID,
                    p_lockReff = lockReff,
                    p_remark = remark,
                    p_userID = userID,
                    p_command = mode
                };
                result = db.SingleOrDefault<String>("Common/SetLock", args);
            }
            catch (Exception e)
            {

            }
            finally
            {
                db.Close();
            }

            return result;
        }


        public void SendMessage(string Msg)
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.Fetch<string>("Common/SendMessage", new { Message = Msg });
            }
            catch (Exception)
            {
            }
            db.Close();
        }
        public IList<listLine> getListLine()
        {
            IList<listLine> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                };
                result = db.Fetch<listLine>("Common/GetLine", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }
        
        public static string ExcelConnect(string param1,string fileExtension)
        {
            var _result = "";
            if (fileExtension == ".xls")
                _result = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + param1 +
                          ";Extended Properties=\"Excel 8.0;HDR=Yes;IMEX=2\"";
            else if (fileExtension == ".xlsx")
                _result = "Provider=Microsoft.ACE.OLEDB.12.0;Data Source=" + param1 +
                          ";Extended Properties=\"Excel 12.0;HDR=Yes;IMEX=2\"";
            return _result;
        }	
	
		public IList<listCat> getListCat()
        {
            IList<listCat> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                };
                result = db.Fetch<listCat>("Common/GetCategory", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public IList<listPlant> getListPlant()
        {
            IList<listPlant> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                };
                result = db.Fetch<listPlant>("Common/GetPlant", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        public String getLineDesc(string lineCd)
        {
            string result = "";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    LINE_CD = lineCd
                };
                result = db.SingleOrDefault<String>("Common/GetLineDesc", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public String getCategoryDesc(string catId)
        {
            string result = "";
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MAT_CAT_CD = catId
                };
                result = db.SingleOrDefault<String>("Common/GetCategoryDesc", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<listUOM> getUOM()
        {
            IList<listUOM> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                };
                result = db.Fetch<listUOM>("Common/GetUOM", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }

        #region Send_Param_Batch
        public string SendParamBatch(string functbatch, string functscreen, string username, string userlocation, string processid, string parameter)
        {
            string msg = "";
            IDBContext db = DatabaseManager.Instance.GetContext("ODB");
            var d = db.Execute("Common/QueueDownload", new
            {
                FunctBatch = functbatch,
                FunctScreen = functscreen,
                Username = username,
                UserLocation = userlocation,
                ProcessId = processid,
                Parameter = parameter
            });
            db.Close();
            msg = "DONE-" + processid;
            return msg;
        }
        #endregion

        public string GetProcessId()
        {
            IDBContext db = DatabaseManager.Instance.GetContext();
            int result = db.SingleOrDefault<int>("Common/GetProcessId");

            return result.ToString();
        }

        public int checkExistExec(string query)
        {
            var result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            dynamic args = new
            {
                query = query
            };
            result = db.SingleOrDefault<int>("Common/CheckExist", args);
            db.Close();
            return result;
        }

        public string getData(string query)
        {
            var result = "";
            try
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                dynamic args = new
                {
                    query = query
                };
                result = db.SingleOrDefault<string>("Common/CheckExist", args);
                db.Close();
            }
            catch (Exception e)
            {
                string msg = e.InnerException.ToString();
            }

            return result;
        }

        public string GetShiftName(string timeToday)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    timeToday = timeToday
                };
                result = db.SingleOrDefault<string>("Common/GetShift", args);
            }
            finally
            {
                db.Close();
            }

            return result;
        }
    }
}