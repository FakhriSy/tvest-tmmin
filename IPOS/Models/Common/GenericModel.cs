﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Credential;

namespace IPOS.Models.Common
{
    public class GenericModel<T> where T : class
    {
        public User CurrentUser { get; set; }
        public IList<T> DataList { get; set; }
        public Paging GridPaging { get; set; }
        public string DateFrom { get; set; }
        public String MsgNotFound
        {
            get
            {
                return CommonRepository.Instance.getMessageText("MCEDSTD030I", null).Split('|')[1];
            }
        }
        public string type { get; set; } //Select Option
    }
}