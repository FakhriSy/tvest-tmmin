﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.Common
{
    public class SearchModel
    {
        public Int32 CurrentPage { get; set; }
        public Int32 DataPerPage { get; set; }
        public Int32 Start
        {
            get
            {
                return ((CurrentPage - 1) * DataPerPage) + 1;
            }
        }
        public Int32 End
        {
            get
            {
                return CurrentPage * DataPerPage;
            }
        }
    }
}