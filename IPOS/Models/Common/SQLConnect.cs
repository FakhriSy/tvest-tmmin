﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using System.Data.SqlClient;

namespace IPOS.Models.Common
{
    public class SQLConnect
    {
        string connectionSring = "";
        public SQLConnect()
        {
            connectionSring = System.Configuration.ConfigurationManager.ConnectionStrings["CED_DBConnectionString"].ConnectionString;
        }

        public void initCommand(SqlConnection conn, SqlCommand cmd, string query)
        {
            if (conn.State != ConnectionState.Open) conn.Open();
            cmd.Connection = conn;
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = query;
        }

        public string ExeQuery(string query)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionSring))
            {
                initCommand(conn, cmd, query);
                var val = cmd.ExecuteNonQuery();
                cmd.Parameters.Clear();
                cmd.Dispose();
                conn.Close();
                if (val >= 0)
                {
                    return "Success";
                }
                else
                {
                    return "Error";
                }
            }
        }

        public DataTable GetDataTable(string query)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionSring))
            {
                initCommand(conn, cmd, query);
                DataTable dt = new DataTable();
                SqlDataAdapter data = new SqlDataAdapter(cmd);
                data.Fill(dt);
                cmd.Parameters.Clear();
                cmd.Dispose();
                conn.Close();
                return dt;
            }
        }

        public DataSet GetDataSet(string query)
        {
            SqlCommand cmd = new SqlCommand();
            using (SqlConnection conn = new SqlConnection(connectionSring))
            {
                initCommand(conn, cmd, query);
                DataSet ds = new DataSet();
                SqlDataAdapter data = new SqlDataAdapter(cmd);
                data.Fill(ds);
                cmd.Parameters.Clear();
                cmd.Dispose();
                conn.Close();
                return ds;
            }
        }
    }
}