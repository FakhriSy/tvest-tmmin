﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED030600W
{
    public class CED030600WRepository
    {
        #region Singleton
        private static CED030600WRepository instance = null;
        public static CED030600WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED030600WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED030600W> SearchData(CED030600WSearchModel SearchModel)
        {
            List<CED030600W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PickUpDate = SearchModel.PickUpDate,
                    L_P = SearchModel.L_P,
                    Delivery_No = SearchModel.Delivery_No,
                    TruckStation = SearchModel.TruckStation,
                    Route = SearchModel.Route,
                    Rate = SearchModel.Rate,
                    Status = SearchModel.Status,
                    Shift = SearchModel.Shift,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED030600W>("CED030600W/CED030600WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030600W> SearchEDData(CED030600WSearchModel SearchModel)
        {
            List<CED030600W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED030600W>("CED030600W/CED030600WSearchEDData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
        public int SearchCountData(CED030600WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PickUpDate = SearchModel.PickUpDate,
                    L_P = SearchModel.L_P,
                    Delivery_No = SearchModel.Delivery_No,
                    TruckStation = SearchModel.TruckStation,
                    Route = SearchModel.Route,
                    Rate = SearchModel.Rate,
                    Status = SearchModel.Status,
                    Shift = SearchModel.Shift,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.SingleOrDefault<int>("CED030600W/CED030600WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountEDData(CED030600WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.SingleOrDefault<int>("CED030600W/CED030600WSearchCountEDData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030600WDetail> SearchDataDetail(CED030600WSearchModel SearchModel)
        {
            List<CED030600WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Delivery_No = SearchModel.Delivery_No,
                    tablename = SearchModel.tablename,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED030600WDetail>("CED030600W/CED030600WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountDataDetail(CED030600WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Delivery_No = SearchModel.Delivery_No,
                    tablename = SearchModel.tablename,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.SingleOrDefault<int>("CED030600W/CED030600WSearchCountDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(string tablename, string deliveryno, string dockcd, string totalskid, string problem, string catchupplan, string username, string changed_date)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    tablename = tablename,
                    Delivery_No = deliveryno,
                    Dock_cd = dockcd,
                    TOTAL_SKID = totalskid,
                    PROBLEM = problem,
                    CATCHUPPLAN = catchupplan,
                    USER_LOGIN = username,
                    CHANGED_DATE = changed_date
                };
                result = db.SingleOrDefault<string>("CED030600W/CED030600WSaveRowUpdate", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DownloadData(CED030600WSearchModel searchfilter, String username, String processid)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();

            string parameters = searchfilter.PickUpDate + ";" + searchfilter.L_P + ";" + searchfilter.Delivery_No + ";" + searchfilter.TruckStation + ";" + searchfilter.Route + ";" + searchfilter.Rate + ";" + searchfilter.Status + ";" + searchfilter.Shift; //searchfilter.tablename + ";" + 
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    PROJECT_CODE = "IPOS",
                    BATCH_ID = "CED030400L",
                    REQUEST_ID = "CED030400L",
                    REQUEST_BY = username,
                    PROCESS_ID = processid,
                    PARAM = "CED030400L#:#" + username + "#:#TMMIN#:#" + processid + "#:#" + parameters //NVS0513#:#Admin#:#TAM#:#999#:#1;28-09-2018;ALS ;asf;4;adf;ad;ON-TIME;DAY
                };
                result = db.SingleOrDefault<string>("CED030600W/CED030600WDownload", args);

                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IEnumerable<Common.comboBox> listLP(string comboBoxID, string comboBoxParam)
        {

            try
            {
                return Common.CommonRepository.Instance.getCombobox(comboBoxID, comboBoxParam);
            }
            catch (Exception)
            {
                throw;
            }
        }

    }
}