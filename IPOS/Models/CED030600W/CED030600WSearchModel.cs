﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED030600W
{
    public class CED030600WSearchModel : SearchModel
    {
        public string PickUpDate { get; set; }
        public string L_P { get; set; }
        public string Delivery_No { get; set; }
        public string TruckStation { get; set; }
        public string Route { get; set; }
        public string Rate { get; set; }
        public string Status { get; set; }
        public string Shift { get; set; }
        public string tablename { get; set; }

    }
}