﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace IPOS.Models.CED030600W
{
    public class CED030600W
    {
        public string No { get; set; }
        public string Delivery_No { get; set; }
        public string Pickup_Date { get; set; }
        public string Route { get; set; }
        public string Rate { get; set; }
        public string Route_Rate { get; set; }
        public string L_P { get; set; }
        public string Dock { get; set; }
        public string Truck_Station { get; set; }
        public string YardIn_Date { get; set; }
        public string YardIn_Time { get; set; }
        public string PlanArrival_Date { get; set; }
        public string PlanArrival_Time { get; set; }
        public string ActualArrival_Date { get; set; }
        public string ActualArrival_Time { get; set; }
        public string ActualArrival_Status { get; set; }
        public string PlanDeparture_Date { get; set; }
        public string PlanDeparture_Time { get; set; }
        public string ActualDeparture_Date { get; set; }
        public string ActualDeparture_Time { get; set; }
        public string ActualDeparture_Status { get; set; }
        public string Total_Kanban { get; set; }
        public string Total_Manifest     { get; set; }
        public string Total_Skid { get; set; }
        public string Problem { get; set; }
        public string CatchUpPlan { get; set; }
        public string ARRIVAL_GAP { get; set; }
        public string DEPARTURE_GAP { get; set; }
        public string CHANGED_DATE { get; set; }
    }

    public class CED030600WDetail
    {
        public string No { get; set; }
        public string Manifest_No { get; set; }
        public string Order_No { get; set; }
        public string Supplier { get; set; }
        public string Supplier_Plant { get; set; }
        public string Order_Release_Date { get; set; }
        public string Total_Kanban { get; set; }
    }
}