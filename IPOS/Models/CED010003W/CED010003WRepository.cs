﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010003W
{
    public class CED010003WRepository
    {
        #region Singleton
        private static CED010003WRepository instance = null;
        public static CED010003WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010003WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010003W> SearchData(string UserName, string ProcessDtFrom, string ProcessDtTo, string ProcessID, string FunctionID, int page, int size)
        {
            List<CED010003W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    UserName = UserName,
                    ProcessDtFrom = (ProcessDtFrom == "") ? null : ProcessDtFrom,
                    ProcessDtTo = (ProcessDtTo == "") ? null : ProcessDtTo,
                    ProcessID = ProcessID,
                    FunctionID = FunctionID,
                    ROW_START = ((page - 1) * size) + 1,
                    ROW_END = page * size
                };
                result = db.Fetch<CED010003W>("CED010003W/CED010003WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(string UserName, string ProcessDtFrom, string ProcessDtTo, string ProcessID, string FunctionID)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    UserName = UserName,
                    ProcessDtFrom = (ProcessDtFrom == "") ? null : ProcessDtFrom,
                    ProcessDtTo = (ProcessDtTo == "") ? null : ProcessDtTo,
                    ProcessID = ProcessID,
                    FunctionID = FunctionID
                };
                result = db.SingleOrDefault<int>("CED010003W/CED010003WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}