﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010003W
{
    public class CED010003W
    {
        public string NUMBER { get; set; }
        public string ProcessId { get; set; }
        public decimal? ReportSequence { get; set; }
        public string Filename { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDate { get; set; }
        public string FunctionId { get; set; }
    }
}