﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010100W
{
    public class CED010100WRepository
    {
        #region Singleton
        private static CED010100WRepository instance = null;
        public static CED010100WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010100WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED010100W> SearchData(CED010100WSearchModel m)
        {
            List<CED010100W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROD_DATE = m.ProdDate,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED010100W>("CED010100W/CED010100WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010100WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROD_DATE = m.ProdDate
                };
                result = db.SingleOrDefault<int>("CED010100W/CED010100WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}