﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010100W
{
    public class CED010100W
    {
        public string ProdDate { get; set; }
        public string PLane { get; set; }
        public string StartTime { get; set; }
        public string CycleTime { get; set; }
    }
}