﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010100W
{
    public class CED010100WSearchModel : SearchModel
    {
        public string ProdDate { get; set; }
    }
}