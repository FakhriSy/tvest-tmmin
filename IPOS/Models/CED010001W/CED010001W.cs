﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010001W
{
    public class CED010001W
    {
        public string ROW_NO { get; set; }
        public string PROCESS_ID { get; set; }
        public string MODULE_ID { get; set; }
        public string FUNCTION_NAME { get; set; }
        public string START_DT { get; set; }
        public string END_DT { get; set; }
        public string PROCESS_STS { get; set; }
        public string USER_ID { get; set; }
    }
}