﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010001W
{
    public class CED010001WRepository
    {
        #region Singleton
        private static CED010001WRepository instance = null;
        public static CED010001WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010001WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010001W> SearchData(CED010001WSearchModel SearchModel)
        {
            List<CED010001W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROCESS_ID = SearchModel.ProcessID,
                    START_DT = SearchModel.StartDT,
                    END_DT = SearchModel.EndDT,
                    MODULE_ID = SearchModel.ModuleID,
                    PROCESS_STS = SearchModel.ProcessSTS,
                    USER_ID = SearchModel.UserID,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010001W>("CED010001W/CED010001WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010001WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROCESS_ID = SearchModel.ProcessID,
                    START_DT = SearchModel.StartDT,
                    END_DT = SearchModel.EndDT,
                    MODULE_ID = SearchModel.ModuleID,
                    PROCESS_STS = SearchModel.ProcessSTS,
                    USER_ID = SearchModel.UserID
                };
                result = db.SingleOrDefault<int>("CED010001W/CED010001WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}