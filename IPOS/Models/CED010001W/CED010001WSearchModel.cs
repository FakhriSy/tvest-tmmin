﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010001W
{
    public class CED010001WSearchModel : SearchModel
    {
        public string ProcessID { get; set; }
        public string StartDT { get; set; }
        public string EndDT { get; set; }
        public string ModuleID { get; set; }
        public string ProcessSTS { get; set; }
        public string UserID { get; set; }
    }
}