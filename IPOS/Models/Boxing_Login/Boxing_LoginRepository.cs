﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.Boxing_Login
{
    public class Boxing_LoginRepository
    {
        #region Singleton
        private static Boxing_LoginRepository instance = null;
        public static Boxing_LoginRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new Boxing_LoginRepository();
                }
                return instance;
            }
        }
        #endregion

        public string ValidateLogin(string line, string noreg, string ipAdd)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Line = line,
                    NoReg = noreg,
                    IPAddress = ipAdd
                };
                result = db.SingleOrDefault<string>("Boxing_Login/ValidateLogin", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}