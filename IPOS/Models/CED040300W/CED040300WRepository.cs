﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED040300W
{
    public class CED040300WRepository
    {
        #region Singleton
        private static CED040300WRepository instance = null;
        public static CED040300WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED040300WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED040300W> SearchData(string barcode, string LineCd, string KanbanType)
        {
            List<CED040300W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Barcode = barcode,
                    LineCd = LineCd,
                    KanbanType = KanbanType
                };
                result = db.Fetch<CED040300W>("CED040300W/CED040300WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040300WGrid1> SearchDataGrid1(CED040300W search_data)
        {
            List<CED040300WGrid1> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DIBLno = search_data.lngDIBLno,
                    LineCd = search_data.LINE_CD,
                    PatternCd = search_data.PATTERN_CD
                };
                result = db.Fetch<CED040300WGrid1>("CED040300W/CED040300WSearchDataGrid1", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED040300WTrx m)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    lngDIBP = m.lngDIBP,
                    strLine = m.strLine,
                    strPatt = m.strPatt,
                    strUnique = m.strUnique,
                    strPartNo = m.strPartNo,
                    lngQtyBox = m.lngQtyBox,
                    lngCT = m.lngCT,
                    lngActCT = m.lngActCT
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ValidateScanBoxing(string pos, string part_no, string part_barcode, int qty_kanban, int qty_box, string pattern_cd)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    POS = pos,
                    PART_NO = part_no,
                    PART_BARCODE = part_barcode,
                    QTY_KANBAN = qty_kanban,
                    QTY_BOX = qty_box,
                    PATTERN_CD = pattern_cd
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WValidate", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ProblemProcess(string KanbanId, string userName, string pos, string StartTime, string PartNo, string PatternCd, string kabanType, int QtyKanban, int QtyBox)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    KanbanId = KanbanId,
                    UserLogin = userName,
                    POS = pos,
                    StartTime = StartTime,
                    PartNo = PartNo,
                    PatternCd = PatternCd,
                    kabanType = kabanType,
                    QtyKanban = QtyKanban,
                    QtyBox = QtyBox
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WProblemProcess", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string UpdateFlagBoxing(string KanbanId, string StartTime, string userName, string kanbanProcess, string pos)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    KanbanId = KanbanId,
                    StartTime = StartTime,
                    UserLogin = userName,
                    KanbanProcess = kanbanProcess,
                    POS = pos
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WUpdateFlagBoxing", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string FinishProcess(string KanbanId, string StartTime, string userName, string POS)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    KanbanId = KanbanId,
                    StartTime = StartTime,
                    UserLogin = userName,
                    POS = POS
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WFinishProcess", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040300WPrint> PrintKanban3(string pos, string userName, string kanban2)
        {
            List<CED040300WPrint> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    POS = pos,
                    UserLogin = userName,
                    Kanban2 = kanban2
                };
                result = db.Fetch<CED040300WPrint>("CED040300W/CED040300WPrintKanban", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string CheckKanban(string barcode)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Barcode = barcode
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WCheckKanban", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string CheckStockKanban3(string barcode)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Barcode = barcode
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WCheckKanban3", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        //FID.Ridwan : 2019-12-21
        public string CheckKanbanCancel(string barcode)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Barcode = barcode
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WCheckKanbanCancel", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040300WCancelGrid> SearchDataCancel(string barcode, string POS, string userName)
        {
            int type = 1;
            List<CED040300WCancelGrid> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Barcode = barcode,
                    POS = POS,
                    UserLogin = userName,
                    Type = type
                };
                result = db.Fetch<CED040300WCancelGrid>("CED040300W/CED040300WSearchCancelData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string FinishProcessCancel(string barcode, string userName, string POS)
        {
            int type = 2;
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    Barcode = barcode,
                    POS = POS,
                    UserLogin = userName,
                    Type = type
                };
                result = db.SingleOrDefault<string>("CED040300W/CED040300WSearchCancelData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

    }
}