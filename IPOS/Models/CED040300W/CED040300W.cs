﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED040300W
{
    public class CED040300W
    {
        public string lngDIBLno { get; set; }
        public string PartNoLnPatt { get; set; }
        public string UniqueLnPatt { get; set; }
        public string lngLnId { get; set; }
        public string lngPattId { get; set; }
        public string strPartNo { get; set; }
        public string strUnique { get; set; }
        public string strLineCd { get; set; }
        public string strPattCd { get; set; }
        public string lngPrtFlg { get; set; }
        public string lngCtm { get; set; }
        public string lngQtyperKbn { get; set; }
        public string lngQtyperBox { get; set; }
        public string strZone { get; set; }
        public string strRack { get; set; }
        public string strAlley { get; set; }
        public string strBKPImage { get; set; }
        public string strPS1Image { get; set; }
        public string strPS2Image { get; set; }
        public string lngRevNo { get; set; }
        public string strRevItem { get; set; }
        public string strAdmin { get; set; }
        public string dtmTCFrom { get; set; }
        public string dtmTCTo { get; set; }
        public string PART_BARCODE { get; set; }
        public string MANIFEST_RECEIVING_FLAG { get; set; }
        public string BOXING_FLAG_TYPE { get; set; }
        public string BOXING_FLAG_ORDER { get; set; }

        public string LINE_CD { get; set; }
        public string PATTERN_CD { get; set; }
    }

    public class CED040300WGrid1
    {
        public string lngDIBLno { get; set; }
        public string strPMCd { get; set; }
        public string strPMName { get; set; }
        public string strPMType { get; set; }
        public string lngPMQty { get; set; }
    }

    public class CED040300WTrx
    {
        public string lngTrxId { get; set; }
        public string dtmDateTime { get; set; }
        public string lngDIBP { get; set; }
        public string strLine { get; set; }
        public string strPatt { get; set; }
        public string strUnique { get; set; }
        public string strPartNo { get; set; }
        public string lngQtyBox { get; set; }
        public string lngCT { get; set; }
        public string lngActCT { get; set; }
    }

    public class CED040300WPrint
    {
        public string Address { get; set; }
        public string Unique { get; set; }
        public string Qty { get; set; }
        public string PartNo { get; set; }
        public string Kanban { get; set; }
        public string PrintOn { get; set; }
        public string PrintBy { get; set; }
        public string PathBarcode { get; set; }
        public string Kanban2 { get; set; }
    }

    public class CED040300WCancelGrid
    {
        public string PART_BARCODE { get; set; }
        public string PART_BARCODE3 { get; set; }
        public string SUPPLY_FLAG { get; set; }
        public string PICKING_FLAG { get; set; }
    }
}