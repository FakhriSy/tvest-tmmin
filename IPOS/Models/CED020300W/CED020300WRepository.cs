﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Data;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;
using System.Data.SqlClient;

namespace IPOS.Models.CED020300W
{
    public class CED020300WRepository
    {
        #region Singleton
        private static CED020300WRepository instance = null;
        public static CED020300WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED020300WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED020300W> SearchData(CED020300WSearchModel m)
        {
            List<CED020300W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    VANNING_PLANT = m.VanningPlant,
                    VANNING_DATE = m.VanningDate,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED020300W>("CED020300W/CED020300WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED020300WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    VANNING_PLANT = m.VanningPlant,
                    VANNING_DATE = m.VanningDate,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.SingleOrDefault<int>("CED020300W/CED020300WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}