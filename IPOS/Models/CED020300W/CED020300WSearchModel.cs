﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED020300W
{
    public class CED020300WSearchModel : SearchModel
    {
        public string VanningDate { get; set; }
        public string VanningPlant { get; set; }
        public string VANNING_DATE { get; set; }
        public string VANNING_PLANT { get; set; }
        public IList<CED020300WSearchModel> LIST_DELETE { get; set; }
    }

    public class CED020300WSearchModelBatch : SearchModel
    {
        public string PlantCd { get; set; }
        public string DateFrom { get; set; }
        public string DateTo { get; set; }
    }
}