﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED020300W
{
    public class CED020300W
    {
        public int ROW_NO { get; set; }
        public string VANNING_DATE { get; set; }
        public string DEST { get; set; }
        public string RENBAN { get; set; }
        public string ORI { get; set; }
        public int? CASE_QTY { get; set; }
        public string SIZE { get; set; }
        public string VANNING_PLACE { get; set; }
        public string STATUS { get; set; }
        public string TIME { get; set; }
        public string ACTUAL { get; set; }
        public string START_TIME { get; set; }
        public string TT { get; set; }
        public int? CUMM { get; set; }
        public string REMARKS { get; set; }
    }

    public class CED020300WDetail
    {
        public string No { get; set; }
        public string SeqNo { get; set; }
        public string VanningDt { get; set; }
        public string DestCd { get; set; }
        public string ContainerSno { get; set; }
        public string OriContSize { get; set; }
        public string CaseQty { get; set; }
        public string Size { get; set; }
        public string StatusLeadTime { get; set; }
        public string VanningPlant { get; set; }
        public string ArrivalContTime { get; set; }
        public string ArrivalContAct { get; set; }
        public string VanningPlanStartTime { get; set; }
        public string VanningPlanTime { get; set; }
        public string VanningPlanCumm { get; set; }
        public string Remarks { get; set; }
        public string Group { get; set; }
        public string Shift { get; set; }
        public string CreatedBy { get; set; }
        public string CreatedDt { get; set; }
        public string ChangedBy { get; set; }
        public string ChangeDt { get; set; }
    }
}