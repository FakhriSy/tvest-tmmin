﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010306W
{
    public class CED010306WRepository
    {
        #region Singleton
        private static CED010306WRepository instance = null;
        public static CED010306WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010306WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010306W> SearchData(CED010306WSearchModel SearchModel)
        {
            List<CED010306W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Delivery_No = SearchModel.Delivery_No,
                    Route = SearchModel.Route,
                    Status = SearchModel.Status,
                    L_P = SearchModel.L_P,
                    Rate = SearchModel.Rate,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010306W>("CED010306W/CED010306WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010306WSearchModel SearchModel)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Delivery_No = SearchModel.Delivery_No,
                    Route = SearchModel.Route,
                    Status = SearchModel.Status,
                    L_P = SearchModel.L_P,
                    Rate = SearchModel.Rate
                };
                result = db.SingleOrDefault<int>("CED010306W/CED010306WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED010306WDetail> SearchDataDetail(CED010306WSearchModel SearchModel)
        {
            List<CED010306WDetail> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Delivery_No = SearchModel.Delivery_No,
                    ROW_START = SearchModel.Start,
                    ROW_END = SearchModel.End
                };
                result = db.Fetch<CED010306WDetail>("CED010306W/CED010306WSearchDataDetail", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}