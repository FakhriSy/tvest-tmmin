﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED010306W
{
    public class CED010306WSearchModel : SearchModel
    {
        public string Delivery_No { get; set; }
        public string Route { get; set; }
        public string Status { get; set; }
        public string L_P { get; set; }
        public string Rate { get; set; }
    }
}