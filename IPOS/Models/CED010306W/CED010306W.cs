﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010306W
{
    public class CED010306W
    {
        public string No { get; set; }
        public string Delivery_No { get; set; }
        public string Pickup_Date { get; set; }
        public string Route_Rate { get; set; }
        public string L_P { get; set; }
        public string Dock { get; set; }
        public string Truck_Station { get; set; }
        public string YardIn_Date { get; set; }
        public string YardIn_Time { get; set; }
        public string PlanArrival_Date { get; set; }
        public string PlanArrival_Time { get; set; }
        public string ActualArrival_Date { get; set; }
        public string ActualArrival_Time { get; set; }
        public string PlanDeparture_Date { get; set; }
        public string PlanDeparture_Time { get; set; }
        public string ActualDeparture_Date { get; set; }
        public string ActualDeparture_Time { get; set; }
    }

    public class CED010306WDetail
    {
        public string No { get; set; }
        public string Manifest_No { get; set; }
        public string Order_No { get; set; }
        public string Supplier { get; set; }
        public string Supplier_Plant { get; set; }
        public string Order_Release_Date { get; set; }
        public string Total_Kanban { get; set; }
    }
}