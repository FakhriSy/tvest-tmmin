﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.Common;

namespace IPOS.Models.CED031501W
{
    public class CED031501WSearchModel : SearchModel
    {
        public string DeliveryDateFrom { get; set; }
        public string DeliveryDateTo { get; set; }
        public string Shift { get; set; }
        public string PhysicalDock { get; set; }
        public string RouteName { get; set; }
        public string RouteCycle { get; set; }
        public string LogicalDock { get; set; }
        public string TS { get; set; }
        public string Supplier { get; set; }
        public string ManifestNo { get; set; }
        public string OrderNo { get; set; }
        public string Status { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string Dock { get; set; }
        public string PartNo { get; set; }
        public string KanbanId { get; set; }
        public string Type { get; set; }



        //public string Supplier { get; set; }
        public string ArrivalDateFrom { get; set; }
        public string ArrivalDateTo { get; set; }
        //public string PhysicalDock { get; set; }
        //public string RouteName { get; set; }
        //public string RouteCycle { get; set; }
        public string DeliveryNo { get; set; }
        //public string ManifestNo { get; set; }
        //public string PartNo { get; set; }
        public string CompletenessStatus { get; set; }
        //public string CurrentPage { get; set; }
        //public string DataPerPage { get; set; }
    }
}