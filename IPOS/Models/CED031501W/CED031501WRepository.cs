﻿using IPOS.Models.Common;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED031501W
{
    public class CED031501WRepository
    {
        #region Singleton
        private static CED031501WRepository instance = null;
        public static CED031501WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED031501WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IEnumerable<comboBox> UserDockMapping(string username)
        {
            IEnumerable<comboBox> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    USERNAME = username
                };
                result = db.Fetch<comboBox>("CED031501W/CED031501WUserDockMapping", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031501W> SearchData(CED031501WSearchModel m, string username)
        {
            List<CED031501W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Supplier = m.Supplier,
                    ArrivalDateFrom = m.ArrivalDateFrom,
                    ArrivalDateTo = m.ArrivalDateTo,
                    PhysicalDock = m.PhysicalDock,
                    RouteName = m.RouteName,
                    RouteCycle = m.RouteCycle,
                    DeliveryNo = m.DeliveryNo,
                    ManifestNo = m.ManifestNo,
                    PartNo = m.PartNo,
                    CompletenessStatus = m.CompletenessStatus,
                    ROW_START = m.Start,
                    ROW_END = m.End,
                    Username = username
                };
                result = db.Fetch<CED031501W>("CED031501W/CED031501WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED031501WSearchModel m, string username)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    Supplier = m.Supplier,
                    ArrivalDateFrom = m.ArrivalDateFrom,
                    ArrivalDateTo = m.ArrivalDateTo,
                    PhysicalDock = m.PhysicalDock,
                    RouteName = m.RouteName,
                    RouteCycle = m.RouteCycle,
                    DeliveryNo = m.DeliveryNo,
                    ManifestNo = m.ManifestNo,
                    PartNo = m.PartNo,
                    CompletenessStatus = m.CompletenessStatus,
                    Username = username
                };
                result = db.SingleOrDefault<int>("CED031501W/CED031501WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED031501W m, int mode, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    Type=m.Type,
                    ManifestNo = m.ManifestNo,
                    KanbanId = m.KanbanId,
                    PlanCatchUp = m.PlanCatchUp,
                    Problem = m.Problem,
                    Changed_Date = DateTime.Now.ToString("dd-MMM-yyyy"),
                    USER_LOGIN = userLogin,
                    ItemParam = m.ItemParam,
                    ChangedDtParam = m.ChangedDtParam

                };
                result = db.SingleOrDefault<string>("CED031501W/CED031501WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ReceiveDataValidate(CED031501W m)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    ManifestNo = m.ManifestNo,
                    //ArrivalTime = m.ArrivalTime,
                    USER_LOGIN = m.Changed_By
                };
                result = db.SingleOrDefault<string>("CED031501W/CED031501WReceiveDataValidate", args);
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ReceiveDataApply(string process_id, string statusCode)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROCESS_ID = process_id,
                    STATUS_CD = statusCode,
                };
                result = db.SingleOrDefault<string>("CED031501W/CED031501WReceiveDataApply", args);
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031501W> ReceiveDataValidate2(CED031501W m)
        {
            List<CED031501W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    MANIFEST_NO = m.ManifestNo,
                    ARRIVAL_ACTUAL_DT = m.ArrivalTime,
                    USER_LOGIN = m.Changed_By
                };
                result = db.Fetch<CED031501W>("CED031501W/CED031501WValidateData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED031501W> ReceiveDataApply2(string process_id, string statusCode)
        {
            List<CED031501W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PROCESS_ID = process_id,
                    STATUS_CD = statusCode,
                };
                result = db.Fetch<CED031501W>("CED031501W/CED031501WReceiveDataApply", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }


    }
}