﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED031501W
{
    public class CED031501W
    {
        public string No { get; set; }
        public string DeliveryDate { get; set; }
        public string Shift { get; set; }
        public string PhysicalDock { get; set; }
        public string LogicalDock { get; set; }
        public string TS { get; set; }
        public string Route { get; set; }
        public string Cycle { get; set; }
        public string LP { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string OrderNo { get; set; }
        public string ManifestNo { get; set; }
        public string KanbanId { get; set; }
        public string PartNo { get; set; }
        public string PlanReceiveOriginal { get; set; }
        public string PlanReceiveUpload { get; set; }
        public string ActualReceive { get; set; }
        public string ModType { get; set; }
        public string Dest { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string KanbanPlan { get; set; }
        public string KanbanAct { get; set; }
        public string Status { get; set; }
        public string PlanCatchUp { get; set; }
        public string Problem { get; set; }
        public string Created_By { get; set; }
        public DateTime Created_Date { get; set; }
        public string Changed_By { get; set; }
        public DateTime Changed_Date { get; set; }
        public string Type { get; set; }
        public string PLaneNo { get; set; }

        public string ItemParam { get; set; }
        public string ChangedDtParam { get; set; }


        //public string ManifestNo { get; set; }
        //public string OrderNo { get; set; }
        //public string SupplierPlantCode { get; set; }
        //public string SupplierName { get; set; }
        public string DockCode { get; set; }
        //public string Route { get; set; }
        public string RouteDate { get; set; }
        public string ArrivalTime { get; set; }
        public string OrderQty { get; set; }
        public string CompletenessStatus { get; set; }
        public string CompletenessStatusText { get; set; }

        public string PROCESS_ID { get; set; }
        public string MSG { get; set; }
    }

    public class CED031501WAPI
    {
        public string MSG { get; set; }
        public string PROCESS_ID { get; set; }
        public string PROCESS_CD { get; set; }

        public int statusCode { get; set; }
        public bool result { get; set; }
        public string message { get; set; }

        //public Newtonsoft.Json.Linq.JObject token { get; set; }

        //public string MANIFEST_NO { get; set; }
        //public string ARRIVAL_ACTUAL_DT { get; set; }
        //public string CHANGED_BY { get; set; }
    }
}