﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED040800W
{
    public class CED040800WRepository
    {
        #region Singleton
        private static CED040800WRepository instance = null;
        public static CED040800WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED040800WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED040800W> SearchData(string kanbanid, string kanbantype)
        {
            List<CED040800W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KanbanId = kanbanid,
                    KanbanType = kanbantype
                };
                result = db.Fetch<CED040800W>("CED040800W/CED040800WGetData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040800WGrid> SearchDataGrid(string kanbanid)
        {
            List<CED040800WGrid> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KanbanId = kanbanid
                };
                result = db.Fetch<CED040800WGrid>("CED040800W/CED040800WGetDataGrid", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<Defect> GetDefect()
        {
            List<Defect> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                };
                result = db.Fetch<Defect>("CED040800W/CED040800WGetDefect", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<SubDefect> GetSubDefect(string defectcd)
        {
            List<SubDefect> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    DefectCd = defectcd
                };
                result = db.Fetch<SubDefect>("CED040800W/CED040800WGetSubDefect", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ProcessReject(string problemId, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    ProblemId = problemId,
                    USER_LOGIN = userLogin

                };
                result = db.SingleOrDefault<string>("CED040800W/CED040800WProcessReject", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ProcessIssue(string problemId, string mainDefect, string subDefect, string kanbanId, string problemImg1, string problemImg2, string thumbImgProblem1, string thumbImgProblem2, string type, string userLogin, string kanbanType, string lastChangeDate, string pos)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    ProblemId = problemId,
                    MainDefect = mainDefect,
                    SubDefect = subDefect,
                    KanbanId = kanbanId,
                    ProblemImage1 = problemImg1,
                    ProblemImage2 = problemImg2,
                    ProblemImageThumb1 = thumbImgProblem1, 
                    ProblemImageThumb2 = thumbImgProblem2,
                    Type = type,
                    USER_LOGIN = userLogin,
                    KanbanType = kanbanType,
                    LastChangeDate = lastChangeDate,
                    POS = pos
                };
                result = db.SingleOrDefault<string>("CED040800W/CED040800WProcessIssue", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string ProcessIssueDetail(CED040800WGrid data, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    ProblemId = data.ProblemId,
                    KanbanId = data.KanbanId,
                    Qty = data.Qty,
                    LastPosition = data.LastPosition,
                    Line = data.Line,
                    USER_LOGIN = userLogin,
                    UserName = data.UserName
                };
                result = db.SingleOrDefault<string>("CED040800W/CED040800WProcessIssueDetail", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040800WGrid> GetListKanban(string partNo, string KanbanId)
        {
            List<CED040800WGrid> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartNo = partNo,
                    KanbanId = KanbanId
                };
                result = db.Fetch<CED040800WGrid>("CED040800W/CED040800WGetListKanban", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED040800WGrid> SearchDataAddKanban(string kanbanid)
        {
            List<CED040800WGrid> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KanbanId = kanbanid
                };
                result = db.Fetch<CED040800WGrid>("CED040800W/CED040800WGetDataAddKanban", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}