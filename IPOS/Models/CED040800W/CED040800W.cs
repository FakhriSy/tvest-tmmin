﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED040800W
{
    public class CED040800W
    {
        public string ProblemId { get; set; }
        public string MainDefect { get; set; }
        public string SubDefect { get; set; }
        public string UniqueNo { get; set; }
        public string PartNo { get; set; }
        public string PartName { get; set; }
        public string Supplier { get; set; }
        public string TotalKanban { get; set; }
        public string TotalPcs { get; set; }
        public string ImgProblem1 { get; set; }
        public string ImgProblem2 { get; set; }
        public string LastChangeDate { get; set; }
    }

    public class CED040800WGrid
    {
        public string No { get; set; }
        public string ProblemId { get; set; }
        public string KanbanId { get; set; }
        public string Qty { get; set; }
        public string LastPosition { get; set; }
        public string Line { get; set; }
        public string UserName { get; set; }
    }

    public class Defect
    {
        public string DEFECT_CD { get; set; }
        public string DEFECT_NM { get; set; }
    }

    public class SubDefect
    {
        public string DEFECT_CD { get; set; }
        public string SUB_DEFECT_CD { get; set; }
        public string SUB_DEFECT_NM { get; set; }
    }
}