﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED010305W
{
    public class CED010305W
    {
        public string Route { get; set; }
        public string Rate { get; set; }
        public string Station { get; set; }
        public string PlanTruck1 { get; set; }
        public string PlanTruck2 { get; set; }
        public string ActTruck1 { get; set; }
        public string ActTruck2 { get; set; }
        public string DelayTruck1 { get; set; }
        public string DelayTruck2 { get; set; }

        public string TruckStation { get; set; }
        public string LP { get; set; }
        public string Status { get; set; }
        public string PlanArrival { get; set; }
        public string PlanDeparture { get; set; }
        public string ActArrivalYard { get; set; }
        public string ActArrival { get; set; }
        public string ActDeparture { get; set; }
        public string DepartureCountdown { get; set; }
        public string PlanCatchUp { get; set; }
        public string Problem { get; set; }
    }
}