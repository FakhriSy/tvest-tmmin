﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010305W
{
    public class CED010305WRepository
    {
        #region Singleton
        private static CED010305WRepository instance = null;
        public static CED010305WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010305WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED010305W> RetrieveData(int cat, int ts)
        {
            List<CED010305W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    CAT = cat,
                    TS = ts
                };
                result = db.Fetch<CED010305W>("CED010305W/CED010305WRetrieveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED010305W> RetrieveStation(int ts)
        {
            List<CED010305W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    TS = ts
                };
                result = db.Fetch<CED010305W>("CED010305W/CED010305WRetrieveStation", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}