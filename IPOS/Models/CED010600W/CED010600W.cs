﻿using System;

namespace IPOS.Models.CED010600W
{
    public class CED010600W
    {
        // CYCLE TIME
        public string MOD_ID { get; set; }
        public string MP {  get; set; }
        public string CYCLE_TIME {  get; set; }
        public string KIT_FLAG { get; set; }
        public string DEDICATED_LINE { get; set; }
        public string SEMIFINISH_FLAG {  get; set; }
        public string PREV_PKG_PLANT {  get; set; }
        public string PACKING_PLANT_FROM { get; set; }
        public string PACKING_PLANT_TO { get; set; }

        // PREPARATION TIME
        public string PREPARATION_TIME { get; set; }
        public string MEASUREMENT { get; set; }
        public string RATIO { get; set; }
        
        // BOTH
        public string MOD_TYPE { get; set; }
        public string CREATED_BY { get; set; }
        public string CREATED_DT { get; set; }
        public string CHANGED_BY { get; set; }
        public string CHANGED_DT { get;set; }
    }
}