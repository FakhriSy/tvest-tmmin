﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOS.Models.CED010600W;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED010600W
{
    public class CED010600WRepository
    {
        #region Singleton
        private static CED010600WRepository instance = null;

        public static CED010600WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED010600WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED010600W> SearchData(CED010600WSearchModel m)
        {
            List<CED010600W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    TABLE = m.TABLE,
                    MOD_ID = m.MOD_ID,
                    MP = m.MP,
                    KIT_FLAG = m.KIT_FLAG,
                    DEDICATED_LINE = m.DEDICATED_LINE,
                    SEMIFINISH_FLAG = m.SEMIFINISH_FLAG,
                    MOD_TYPE = m.MOD_TYPE,
                    ROW_START = m.Start,
                    ROW_END = m.End
                };
                result = db.Fetch<CED010600W>("CED010600W/CED010600WSearchData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public int SearchCountData(CED010600WSearchModel m)
        {
            int result = 0;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    TABLE = m.TABLE,
                    MOD_ID = m.MOD_ID,
                    MP = m.MP,
                    KIT_FLAG = m.KIT_FLAG,
                    DEDICATED_LINE = m.DEDICATED_LINE,
                    SEMIFINISH_FLAG = m.SEMIFINISH_FLAG,
                    MOD_TYPE = m.MOD_TYPE,
                };
                result = db.SingleOrDefault<int>("CED010600W/CED010600WSearchCountData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED010600W m, int mode, int table, string userLogin)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    MODE = mode,
                    TABLE = table,

                    // CYCLE TIME
                    MOD_ID = m.MOD_ID,
                    MP = m.MP,
                    CYCLE_TIME = m.CYCLE_TIME,
                    KIT_FLAG = m.KIT_FLAG,
                    DEDICATED_LINE = m.DEDICATED_LINE,
                    SEMIFINISH_FLAG = m.SEMIFINISH_FLAG,
                    //PREV_PKG_PLANT = m.PREV_PKG_PLANT,
                    PACKING_PLANT_FROM = m.PACKING_PLANT_FROM,
                    PACKING_PLANT_TO = m.PACKING_PLANT_TO,

                    // PREPARATION TIME
                    PREPARATION_TIME = m.PREPARATION_TIME,
                    MEASUREMENT = m.MEASUREMENT,
                    RATIO = m.RATIO,

                    // BOTH
                    MOD_TYPE = m.MOD_TYPE,
                    CHANGED_DT = m.CHANGED_DT,
                    USER_LOGIN = userLogin
                };
                result = db.SingleOrDefault<string>("CED010600W/CED010600WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string DeleteData(CED010600W m, int table)
        {
            string result = null;
            string errorresult = "";
            string[] listModId = m.MOD_ID.Split(';');
            string[] listMP = m.MP.Split(';');
            string[] listModType = m.MOD_TYPE.Split(';');
            string[] listChangedDt = m.CHANGED_DT.Split(';');

            //DELETE TRANSACTION
            for (int i = 0; i < listChangedDt.Length-1; i++)
            {
                IDBContext db = DatabaseManager.Instance.GetContext();
                try
                {
                    db.BeginTransaction();
                    dynamic args = new
                    {
                        TABLE = table,
                        MOD_ID = listModId[i],
                        MP = listMP[i],
                        MOD_TYPE = listModType[i],
                        CHANGED_DT = listChangedDt[i]
                    };
                    result = db.SingleOrDefault<string>("CED010600W/CED010600WDeleteData", args);
                    db.CommitTransaction();
                }
                catch (Exception e)
                {
                    db.AbortTransaction();
                    result = e.Message.ToString();
                }
                finally
                {
                    db.Close();
                    if (result.Split('|')[0] == "E")
                        errorresult = result;
                    else
                        errorresult = errorresult + "";
                }
            }

            if (errorresult == "")
                return result;
            else
                return errorresult;
        }
    }
}