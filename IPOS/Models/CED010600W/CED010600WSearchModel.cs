﻿using System;
using IPOS.Models.Common;

namespace IPOS.Models.CED010600W
{
    public class CED010600WSearchModel : SearchModel
    {
        public string TABLE {  get; set; }

        // CYCLE TIME
        public string MOD_ID { get; set; }
        public string MP { get; set; }
        public string KIT_FLAG { get; set; }
        public string DEDICATED_LINE { get; set; }
        public string SEMIFINISH_FLAG { get; set; }

        // CYCLE TIME & PREPARATION TIME
        public string MOD_TYPE {  get; set; }
    }
}