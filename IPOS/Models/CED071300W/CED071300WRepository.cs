﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED071300W
{
    public class CED071300WRepository
    {
        #region Singleton
        private static CED071300WRepository instance = null;
        public static CED071300WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED071300WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED071300W> SearchData(string KanbanID)
        {
            List<CED071300W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    KanbanID = KanbanID
                };
                result = db.Fetch<CED071300W>("CED071300W/CED071300WSearchDetailRealeseKanban", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED071300WGrid> SearchDataGrid(string PartBarcode, string ProblemID)
        {
            List<CED071300WGrid> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    PartBarcode = PartBarcode,
                    ProblemID = ProblemID
                };
                result = db.Fetch<CED071300WGrid>("CED071300W/CED071300WSearchDataGrid", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string SaveData(CED071300W m, String username)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    ProblemID = m.ProblemID,
                    PartBarcode = m.PartBarcode,
                    USER_LOGIN = username
                };
                result = db.SingleOrDefault<string>("CED071300W/CED071300WSaveData", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public string FinishProcess(string userName, string POS)
        {
            string result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                db.BeginTransaction();
                dynamic args = new
                {
                    UserLogin = userName,
                    POS = POS
                };
                result = db.SingleOrDefault<string>("CED071300W/CED071300WFinishProcess", args);
                db.CommitTransaction();
            }
            catch (Exception e)
            {
                db.AbortTransaction();
                result = e.Message.ToString();
            }
            finally
            {
                db.Close();
            }
            return result;
        }

    }
}