﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED071300W
{
    public class CED071300W
    {
        public string ProblemID { get; set; }
        public string ProblemName { get; set; }
        public string Unique { get; set; }
        public string PartNo { get; set; }
        public string PartName { get; set; }
        public string SupplierCd { get; set; }
        public string SupplierAvvreviation { get; set; }
        public string KanbanProblem { get; set; }
        public string KanbanRelease { get; set; }
        public string ProblemImg1 { get; set; }
        public string ProblemImg2 { get; set; }
        public string PartBarcode { get; set; }
    }

    public class CED071300WGrid
    {
        public string No { get; set; }
        public string Kanban_Id { get; set; }
        public string Qty { get; set; }
        public string Last_Position { get; set; }
        public string Line { get; set; }
    }

}