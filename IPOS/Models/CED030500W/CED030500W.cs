﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOS.Models.CED030500W
{
    public class CED030500W
    {
        public string Route { get; set; }
        public string Rate { get; set; }
        public string Station { get; set; }
        public string PlanTruck1 { get; set; }
        public string PlanTruck2 { get; set; }
        public string ActTruck1 { get; set; }
        public string ActTruck2 { get; set; }
        public string DelayTruck1 { get; set; }
        public string DelayTruck2 { get; set; }

        public string TruckStation { get; set; }
        public string LP { get; set; }
        public string Status { get; set; }
        public string PlanArrival { get; set; }
        public string PlanDeparture { get; set; }
        public string ActArrivalYard { get; set; }
        public string ActArrival { get; set; }
        public string ActDeparture { get; set; }
        public string DepartureCountdown { get; set; }
        public string PlanCatchUp { get; set; }
        public string Problem { get; set; }
        public DateTime? WarningCountdown { get; set; }
        public DateTime? ARRIVAL_ACTUAL_DT { get; set; }
        public DateTime? DEPARTURE_PLAN_DT { get; set; }
        public DateTime? DEPARTURE_ACTUAL_DT { get; set; }
        public int TimeCountdown { get; set; }
    }

    public class CED030500WBindStation
    {
        public string STATION { get; set; }
    }
}