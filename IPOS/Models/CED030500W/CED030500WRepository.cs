﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED030500W
{
    public class CED030500WRepository
    {
        #region Singleton
        private static CED030500WRepository instance = null;
        public static CED030500WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED030500WRepository();
                }
                return instance;
            }
        }
        #endregion

        public IList<CED030500W> RetrieveData(string cat, string ts)
        {
            List<CED030500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    CAT = cat,
                    TS = ts
                };
                result = db.Fetch<CED030500W>("CED030500W/CED030500WRetrieveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030500W> RetrieveStation(string ts)
        {
            List<CED030500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    TS = ts
                };
                result = db.Fetch<CED030500W>("CED030500W/CED030500WRetrieveStation", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }

        public IList<CED030500WBindStation> BindTS()
        {
            List<CED030500WBindStation> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {

                };
                result = db.Fetch<CED030500WBindStation>("CED030500W/CED030500WBindTS", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}