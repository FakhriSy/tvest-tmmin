﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Toyota.Common.Database;
using Toyota.Common.Web.Platform;

namespace IPOS.Models.CED040500W
{
    public class CED040500WRepository
    {
        #region Singleton
        private static CED040500WRepository instance = null;
        public static CED040500WRepository Instance
        {
            get
            {
                if (instance == null)
                {
                    instance = new CED040500WRepository();
                }
                return instance;
            }
        }
        #endregion
        public IList<CED040500W> RetrieveData(int cat, string Type)
        {
            List<CED040500W> result = null;
            IDBContext db = DatabaseManager.Instance.GetContext();
            try
            {
                dynamic args = new
                {
                    CAT = cat,
                    TYPE = Type
                };
                result = db.Fetch<CED040500W>("CED040500W/CED040500WRetrieveData", args);
            }
            finally
            {
                db.Close();
            }
            return result;
        }
    }
}