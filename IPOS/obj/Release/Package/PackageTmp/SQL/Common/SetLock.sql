﻿DECLARE @@p_processID BIGINT
	,@@p_functionID VARCHAR(20)
	,@@p_lockReff VARCHAR(MAX)
	,@@p_remark VARCHAR(MAX)
	,@@p_userID VARCHAR(MAX)
	,@@p_command VARCHAR(50)
	,@@N_ERR INT
	,@@MSG_TXT VARCHAR(MAX)

SELECT @@p_processID = @p_processID
	,@@p_functionID = @p_functionID
	,@@p_lockReff = @p_lockReff
	,@@p_remark = @p_remark
	,@@p_userID = @p_userID
	,@@p_command = @p_command

EXEC SP_SetLock @@p_processID
	,@@p_functionID
	,@@p_lockReff
	,@@p_remark
	,@@p_userID
	,@@p_command
	,@@N_ERR OUTPUT
	,@@MSG_TXT OUTPUT

SELECT @@MSG_TXT