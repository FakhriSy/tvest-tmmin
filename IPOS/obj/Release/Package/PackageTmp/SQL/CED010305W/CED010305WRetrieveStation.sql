﻿SELECT B.STATION Station,
	SUM(CASE WHEN B.ARRIVAL_ACTUAL_DT IS NOT NULL THEN 1 ELSE 0 END) PlanTruck1, COUNT(1) PlanTruck2,
	SUM(CASE WHEN B.ARRIVAL_STATUS = '99' THEN 1 ELSE 0 END) ActTruck1, COUNT(1) ActTruck2,
	SUM(CASE WHEN B.ARRIVAL_STATUS = '1' THEN 1 ELSE 0 END) DelayTruck1, COUNT(1) DelayTruck2
FROM TB_R_DELIVERY_CTL_H A
	LEFT JOIN TB_R_DELIVERY_CTL_D B ON A.DELIVERY_NO=B.DELIVERY_NO
WHERE B.STATION BETWEEN 1 AND @TS
GROUP BY B.STATION
ORDER BY B.STATION