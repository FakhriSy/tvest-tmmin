﻿-- SET NOCOUNT ON added to prevent extra result sets from
-- interfering with SELECT statements.
SET NOCOUNT ON;
BEGIN TRY
	DECLARE
	@@GROUPING_CODE VARCHAR(25)
	,@@GROUPING_NAME VARCHAR(255)
	,@@ZONE_NAME VARCHAR(255)
	,@@ROW_START VARCHAR(25)
	,@@ROW_END VARCHAR(25)
	-- Message
	,@@MSG_TXT VARCHAR(MAX)
	,@@MSG_TYPE CHAR
	
	SET @@GROUPING_CODE = RTRIM(LTRIM(@GROUPING_CODE));
	SET @@GROUPING_NAME = RTRIM(LTRIM(@GROUPING_NAME));
	SET @@ZONE_NAME = RTRIM(LTRIM(@ZONE_NAME));

	SELECT
		COUNT(1)
		FROM (
		SELECT
		    ROW_NUMBER() OVER(ORDER BY H.ZONE_GROUPING_CD) ROW_NO,
			H.ZONE_GROUPING_CD [GROUPING_CODE], ZONE_GROUPING_NM [GROUPING_NAME], 
			COUNT(ZONE_CD) [COUNT],H.CREATED_BY,CONVERT(DATE,H.CREATED_DT) AS CREATED_ON ,H.CHANGED_BY,
			CONVERT(DATE,H.CHANGED_DT) AS CHANGED_ON
				from TB_M_ZONE_GROUPING_H H
			LEFT JOIN TB_M_ZONE_GROUPING_D D ON H.ZONE_GROUPING_CD=D.ZONE_GROUPING_CD
			WHERE 1=1
			AND (ISNULL(@@GROUPING_CODE,'')='' OR H.ZONE_GROUPING_CD LIKE '%'+@@GROUPING_CODE+'%')
			AND (ISNULL(@@GROUPING_NAME,'')='' OR H.ZONE_GROUPING_NM LIKE '%'+@@GROUPING_NAME+'%')
			AND (ISNULL(@@ZONE_NAME,'')='' OR D.ZONE_NM LIKE '%'+@@ZONE_NAME+'%')
			GROUP BY H.ZONE_GROUPING_CD, ZONE_GROUPING_NM,h.CREATED_BY,h.CREATED_DT ,h.CHANGED_BY,h.CHANGED_DT
		)TB
	 
END TRY
	
BEGIN CATCH
	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
	SET @@MSG_TYPE = 'E'

	SELECT @@MSG_TYPE+'|'+@@MSG_TXT 
END CATCH