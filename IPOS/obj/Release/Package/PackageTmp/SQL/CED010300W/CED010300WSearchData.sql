﻿SET NOCOUNT ON;
BEGIN TRY
	DECLARE @@MSG_TXT VARCHAR(MAX), @@MSG_TYPE CHAR
	
	SELECT * FROM (
		SELECT
			ROW_NUMBER() OVER(ORDER BY PART_NO ASC) No,
			SUPPLIER_CD SupplierCode, SUPPLIER_PLANT_CD SupplierPlantCode, PART_NO PartNo, UNIQUE_NO UniqueNo, QKP_IMAGE QkpImage, REV_ITEM RevItem,
			REV_NO RevNo, DT_FROM DateFrom, DT_TO DateTo,
			CREATED_BY, CREATED_DT, CHANGED_BY, CHANGED_DT
		FROM TB_M_PART_UNIQUE
		WHERE 1 = 1
		AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+LTRIM(RTRIM(@PartNo))+'%')
		AND (ISNULL(@SupplierCode,'')='' OR SUPPLIER_CD = (SELECT splitdata
															FROM dbo.fn_SplitString(@SupplierCode, '-')
															WHERE rowno = 0))
		AND (ISNULL(@SupplierCode,'')='' OR SUPPLIER_PLANT_CD = (SELECT splitdata
															FROM dbo.fn_SplitString(@SupplierCode, '-')
															WHERE rowno = 1))
		AND (ISNULL(@LATEST_VALUE,'')='' OR DT_TO = '9999-12-31')
	) TB WHERE TB.No BETWEEN @ROW_START AND @ROW_END
END TRY
	
BEGIN CATCH
	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
	SET @@MSG_TYPE = 'E'

	SELECT @@MSG_TYPE+'|'+@@MSG_TXT
END CATCH