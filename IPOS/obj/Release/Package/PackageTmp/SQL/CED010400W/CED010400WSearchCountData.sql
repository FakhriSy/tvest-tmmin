﻿EXEC CED010400W_SearchCountData
@Table ,
@PartNo ,
@LineId ,
@PatternId ,
@DockCode ,
@PackingLine ,
@Passthru ,
@Picking ,
@Boxing ,
@Stacking ,
@FloorRack ,
@showLatest 

--SET NOCOUNT ON;
--BEGIN TRY
--	DECLARE @@MSG_TXT VARCHAR(MAX), @@MSG_TYPE CHAR

--	IF(@Table=1)
--	BEGIN
--		IF (@showLatest = '1')
--		BEGIN
--			SELECT COUNT(1) FROM (
--				SELECT
--					ROW_NUMBER() OVER(ORDER BY PART_NO, DOCK_CD ASC) No
--					,PART_NO as Part_No
--					,DOCK_CD as Dock_Code
--					,LINE_CD as Line_Id
--					,PATTERN_CD as Pattern_Id
--					,PACKING_LINE_CD as Packing_Line
--					,BOXING_FLAG as Boxing_Flag
--					,PICKING_FLAG as Picking_Flag
--					,STACKING_FLAG as Stacking_Flag
--					,FLOW_RACK_FLAG as FloorRack_Flag
--					,FORMAT(VALID_FROM, 'dd-MMM-yyyy') as ValidFrom
--					,FORMAT(VALID_TO, 'dd-MMM-yyyy') as ValidTo
--					,CREATED_BY as Created_By
--					,FORMAT(CREATED_DT, 'dd-MMM-yyyy') as Created_Date
--					,CHANGED_BY as Changed_By
--					,FORMAT(CHANGED_DT, 'dd-MMM-yyyy') as Changed_Date
--					,VALID_FROM AS ValidFrom_Hide
--				FROM TB_M_PART_TYPE X
--				WHERE CAST(VALID_FROM AS DATE) = (SELECT CAST(MAX(VALID_FROM) AS DATE) FROM TB_M_PART_TYPE WHERE PART_NO=X.PART_NO AND DOCK_CD=X.DOCK_CD)
--				AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+@PartNo+'%')
--				AND (ISNULL(@DockCode,'')='' OR DOCK_CD = @DockCode)
--				AND (ISNULL(@LineId,'')='' OR LINE_CD = @LineId)
--				AND (ISNULL(@PatternId,'')='' OR PATTERN_CD = @PatternId)

--				AND (ISNULL(@Boxing,'')='' OR BOXING_FLAG = @Boxing)
--				AND (ISNULL(@Picking,'')='' OR PICKING_FLAG = @Picking)
--				AND (ISNULL(@Stacking,'')='' OR STACKING_FLAG = @Stacking)
--				AND (ISNULL(@FloorRack,'')='' OR FLOW_RACK_FLAG = @FloorRack)
--			) TB
--		END
--		ELSE
--		BEGIN
--			SELECT COUNT(1) FROM (
--				SELECT
--					ROW_NUMBER() OVER(ORDER BY PART_NO, DOCK_CD ASC) No
--					,PART_NO as Part_No
--					,DOCK_CD as Dock_Code
--					,LINE_CD as Line_Id
--					,PATTERN_CD as Pattern_Id
--					,'PACKING_LINE_CD' as Packing_Line
--					,BOXING_FLAG as Boxing_Flag
--					,PICKING_FLAG as Picking_Flag
--					,STACKING_FLAG as Stacking_Flag
--					,FLOW_RACK_FLAG as FloorRack_Flag
--					,FORMAT(VALID_FROM, 'dd-MMM-yyyy') as ValidFrom
--					,FORMAT(VALID_TO, 'dd-MMM-yyyy') as ValidTo
--					,CREATED_BY as Created_By
--					,FORMAT(CREATED_DT, 'dd-MMM-yyyy') as Created_Date
--					,CHANGED_BY as Changed_By
--					,FORMAT(CHANGED_DT, 'dd-MMM-yyyy') as Changed_Date
--					,VALID_FROM AS ValidFrom_Hide
--				FROM TB_M_PART_TYPE
--				WHERE 1 = 1
--				AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+@PartNo+'%')
--				AND (ISNULL(@DockCode,'')='' OR DOCK_CD = @DockCode)
--				AND (ISNULL(@LineId,'')='' OR LINE_CD = @LineId)
--				AND (ISNULL(@PatternId,'')='' OR PATTERN_CD = @PatternId)

--				AND (ISNULL(@Boxing,'')='' OR BOXING_FLAG = @Boxing)
--				AND (ISNULL(@Picking,'')='' OR PICKING_FLAG = @Picking)
--				AND (ISNULL(@Stacking,'')='' OR STACKING_FLAG = @Stacking)
--				AND (ISNULL(@FloorRack,'')='' OR FLOW_RACK_FLAG = @FloorRack)
--			) TB
--		END
--	END
--	ELSE IF(@Table=2)
--	BEGIN
--		IF (@showLatest = '1')
--		BEGIN
--			SELECT COUNT(1) FROM (
--				SELECT
--					ROW_NUMBER() OVER(ORDER BY PART_NO, DOCK_CD ASC) No
--					,PART_NO as Part_No
--					,DOCK_CD as Dock_Code
--					,LINE_CD as Line_Id
--					,PATTERN_CD as Pattern_Id
--					,'PACKING_LINE_CD' as Packing_Line
--					,CYCLE_TIME as Cycle_Time
--					,FORMAT(VALID_FROM, 'dd-MMM-yyyy') as ValidFrom
--					,FORMAT(VALID_TO, 'dd-MMM-yyyy') as ValidTo
--					,CREATED_BY as Created_By
--					,FORMAT(CREATED_DT, 'dd-MMM-yyyy') as Created_Date
--					,CHANGED_BY as Changed_By
--					,FORMAT(CHANGED_DT, 'dd-MMM-yyyy') as Changed_Date
--					,VALID_FROM AS ValidFrom_Hide
--				FROM TB_M_PART_CYCLE_TIME X
--				WHERE CAST(VALID_FROM AS DATE) = (SELECT CAST(MAX(VALID_FROM) AS DATE) FROM TB_M_PART_CYCLE_TIME WHERE PART_NO=X.PART_NO AND DOCK_CD=X.DOCK_CD AND LINE_CD=X.LINE_CD AND PATTERN_CD=X.PATTERN_CD)
--				AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+@PartNo+'%')
--				AND (ISNULL(@DockCode,'')='' OR DOCK_CD = @DockCode)
--				AND (ISNULL(@LineId,'')='' OR LINE_CD = @LineId)

--			) TB
--		END
--		ELSE
--		BEGIN
--			SELECT COUNT(1) FROM (
--				SELECT
--					ROW_NUMBER() OVER(ORDER BY PART_NO, DOCK_CD ASC) No
--					,PART_NO as Part_No
--					,DOCK_CD as Dock_Code
--					,LINE_CD as Line_Id
--					,PATTERN_CD as Pattern_Id
--					,'PACKING_LINE_CD' as Packing_Line
--					,CYCLE_TIME as Cycle_Time
--					,FORMAT(VALID_FROM, 'dd-MMM-yyyy') as ValidFrom
--					,FORMAT(VALID_TO, 'dd-MMM-yyyy') as ValidTo
--					,CREATED_BY as Created_By
--					,FORMAT(CREATED_DT, 'dd-MMM-yyyy') as Created_Date
--					,CHANGED_BY as Changed_By
--					,FORMAT(CHANGED_DT, 'dd-MMM-yyyy') as Changed_Date
--					,VALID_FROM AS ValidFrom_Hide
--				FROM TB_M_PART_CYCLE_TIME
--				WHERE 1 = 1
--				AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+@PartNo+'%')
--				AND (ISNULL(@DockCode,'')='' OR DOCK_CD = @DockCode)
--				AND (ISNULL(@LineId,'')='' OR LINE_CD = @LineId)

--			) TB
--		END
--	END
--	ELSE IF(@Table=3)
--	BEGIN
--		IF (@showLatest = '1')
--		BEGIN
--			SELECT COUNT(1) FROM (
--				SELECT
--					ROW_NUMBER() OVER(ORDER BY PART_NO, PATTERN_CD ASC) No
--					,PART_NO as Part_No
--					,LINE_CD as Line_Id
--					,'' as Dock_Code
--					,PATTERN_CD as Pattern_Id
--					,RACK_ADDRESS as Rack_Address
--					,MAX_STOCK_RACK as Max_Stock_Rack
--					,QTY_BOX as Qty_Box
--					,QTY_KANBAN as Qty_Kanban
--					,BKP_IMAGE as Bkp_Image
--					,PKP_IMAGE as Pkp_Image
--					,PS1_IMAGE as Ps1_Image
--					,PS2_IMAGE as Ps2_Image
--					,DS_IMAGE as Ds_Image
--					,PASSTHRU_FLAG as Passthru_Flag
--					,FORMAT(VALID_FROM, 'dd-MMM-yyyy') as ValidFrom
--					,FORMAT(VALID_TO, 'dd-MMM-yyyy') as ValidTo
--					,CREATED_BY as Created_By
--					,FORMAT(CREATED_DT, 'dd-MMM-yyyy') as Created_Date
--					,CHANGED_BY as Changed_By
--					,FORMAT(CHANGED_DT, 'dd-MMM-yyyy') as Changed_Date
--					,VALID_FROM AS ValidFrom_Hide

--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='BKP_IMAGE' AND SYS_CD='CED010400W') AS Bkp_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='PKP_IMAGE' AND SYS_CD='CED010400W') AS Pkp_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='PS1_IMAGE' AND SYS_CD='CED010400W') AS Ps1_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='PS2_IMAGE' AND SYS_CD='CED010400W') AS Ps2_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='DS_IMAGE' AND SYS_CD='CED010400W') AS Ds_Path

--				FROM TB_M_PART_LINE X
--				WHERE CAST(VALID_FROM AS DATE) = (SELECT CAST(MAX(VALID_FROM) AS DATE) FROM TB_M_PART_LINE WHERE PART_NO=X.PART_NO AND LINE_CD=X.LINE_CD AND PATTERN_CD=X.PATTERN_CD)
--				AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+@PartNo+'%')
--				AND (ISNULL(@PatternId,'')='' OR PATTERN_CD = @PatternId)

--				AND (ISNULL(@Passthru,'')='' OR PASSTHRU_FLAG = @Passthru)
--			) TB
--		END
--		ELSE
--		BEGIN
--			SELECT COUNT(1) FROM (
--				SELECT
--					ROW_NUMBER() OVER(ORDER BY PART_NO, PATTERN_CD ASC) No
--					,PART_NO as Part_No
--					,LINE_CD as Line_Id
--					,'' as Dock_Code
--					,PATTERN_CD as Pattern_Id
--					,RACK_ADDRESS as Rack_Address
--					,MAX_STOCK_RACK as Max_Stock_Rack
--					,QTY_BOX as Qty_Box
--					,QTY_KANBAN as Qty_Kanban
--					,BKP_IMAGE as Bkp_Image
--					,PKP_IMAGE as Pkp_Image
--					,PS1_IMAGE as Ps1_Image
--					,PS2_IMAGE as Ps2_Image
--					,DS_IMAGE as Ds_Image
--					,PASSTHRU_FLAG as Passthru_Flag
--					,FORMAT(VALID_FROM, 'dd-MMM-yyyy') as ValidFrom
--					,FORMAT(VALID_TO, 'dd-MMM-yyyy') as ValidTo
--					,CREATED_BY as Created_By
--					,FORMAT(CREATED_DT, 'dd-MMM-yyyy') as Created_Date
--					,CHANGED_BY as Changed_By
--					,FORMAT(CHANGED_DT, 'dd-MMM-yyyy') as Changed_Date
--					,VALID_FROM AS ValidFrom_Hide

--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='BKP_IMAGE' AND SYS_CD='CED010400W') AS Bkp_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='PKP_IMAGE' AND SYS_CD='CED010400W') AS Pkp_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='PS1_IMAGE' AND SYS_CD='CED010400W') AS Ps1_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='PS2_IMAGE' AND SYS_CD='CED010400W') AS Ps2_Path
--					,(select TOP 1 SYS_VAL from TB_M_SYSTEM WHERE SYS_CAT='MASTER' AND SYS_SUB_CAT='DS_IMAGE' AND SYS_CD='CED010400W') AS Ds_Path

--				FROM TB_M_PART_LINE
--				WHERE 1 = 1
--				AND (ISNULL(@PartNo,'')='' OR PART_NO LIKE '%'+@PartNo+'%')
--				AND (ISNULL(@PatternId,'')='' OR PATTERN_CD = @PatternId)

--				AND (ISNULL(@Passthru,'')='' OR PASSTHRU_FLAG = @Passthru)
--			) TB
--		END
--	END

--END TRY
	
--BEGIN CATCH
--	SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
--	SET @@MSG_TYPE = 'E'

--	SELECT @@MSG_TYPE+'|'+@@MSG_TXT
--END CATCH