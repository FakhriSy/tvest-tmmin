﻿/*EXEC SP_InsertR_BATCH_QUEUE 
	 @PROJECT_CODE,
     @BATCH_ID,
     @REQUEST_ID,
     @REQUEST_BY,
     @PARAM*/


BEGIN
	-- SET NOCOUNT ON added to prevent extra result sets from
	-- interfering with SELECT statements.
	SET NOCOUNT ON;
	BEGIN TRY
	    DECLARE
		-- Message
		@@MSG_TXT VARCHAR(MAX)
		,@@MSG_TYPE CHAR

		INSERT INTO AG100_ODB.dbo.TB_R_BATCH_QUEUE
		(
			QUEUE_NO
			,	PROJECT_CODE
			,	BATCH_ID
			,	REQUEST_ID
			,	REQUEST_DATE
			,	REQUEST_BY
			,	SUPPORT_ID
			,	PARAMETER
		)
		SELECT 
			@PROCESS_ID
			,	@PROJECT_CODE
			,	@BATCH_ID
			,	@REQUEST_ID
			,	GETDATE()
			,	@REQUEST_BY
			,	NULL
			,	@PARAM
			
		SELECT 'I|MCEDSTD002I:Arrival Departure Report is processing. Please see Log Monitoring with Process ID ' + CONVERT(VARCHAR,@PROCESS_ID) + '.'
	END TRY
	
	BEGIN CATCH
		SET @@MSG_TXT = (SELECT ERROR_MESSAGE())
		SET @@MSG_TYPE = 'E'

		SELECT @@MSG_TYPE+'|'+@@MSG_TXT
	END CATCH
END