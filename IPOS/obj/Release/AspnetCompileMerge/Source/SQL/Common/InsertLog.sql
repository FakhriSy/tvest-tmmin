﻿DECLARE @@p_messages VARCHAR(MAX)
	,@@p_user VARCHAR(50)
	,@@p_location VARCHAR(512)
	,@@p_pid BIGINT
	,@@p_msg_id VARCHAR(12)
	,@@p_msg_type VARCHAR(3)
	,@@p_module_id VARCHAR(10)
	,@@p_function_id VARCHAR(20)
	,@@p_process_status TINYINT

SELECT @@p_messages = @p_messages
	,@@p_user = @p_user
	,@@p_location = @p_location
	,@@p_pid = @p_pid
	,@@p_msg_id = @p_msg_id
	,@@p_msg_type = @p_msg_type
	,@@p_module_id = @p_module_id
	,@@p_function_id = @p_function_id
	,@@p_process_status = @p_process_status

EXEC SP_PutLog @@p_messages
	,@@p_user
	,@@p_location
	,@@p_pid OUTPUT
	,@@p_msg_id
	,@@p_msg_type
	,@@p_module_id
	,@@p_function_id
	,@@p_process_status

SELECT CONVERT(VARCHAR, @@p_pid)