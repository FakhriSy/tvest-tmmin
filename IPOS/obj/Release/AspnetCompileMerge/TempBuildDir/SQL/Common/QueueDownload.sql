﻿INSERT INTO TB_R_BATCH_QUEUE
(
	QUEUE_NO
	,	PROJECT_CODE
	,	BATCH_ID
	,	REQUEST_ID
	,	REQUEST_DATE
	,	REQUEST_BY
	,	SUPPORT_ID
	,	PARAMETER
)
SELECT 
	NEWID()
	,	'IPOS'
	,	@FunctBatch
	,	@FunctScreen
	,	GETDATE()
	,	@Username
	,	NULL
	,	@FunctBatch + '#:#' + @Username + '#:#' + @UserLocation + '#:#' + @ProcessId + '#:#' + @Parameter