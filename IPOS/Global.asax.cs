﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Web.Routing;
using Toyota.Common.Web.Platform;
using Toyota.Common.Web.UI;
using Toyota.Common.Credential;

namespace IPOS
{
    public class MvcApplication : WebApplication
    {
        public MvcApplication()
        {
            ApplicationSettings.Instance.Name = "TVEST"; //Toyota-Indonesia Vanning Export Smart Integrated System
            ApplicationSettings.Instance.Alias = "TVEST";
            ApplicationSettings.Instance.OwnerName = "Toyota Motor Manufacturing Indonesia";
            ApplicationSettings.Instance.OwnerAlias = "TMMIN";
            ApplicationSettings.Instance.OwnerEmail = "tdk@toyota.co.id"; 
            ApplicationSettings.Instance.Menu.Enabled = true; // option setting enable/disable all menu

            ApplicationSettings.Instance.Runtime.HomeController = "Home"; //uncomment this to setting default page
            BypassLogin(false);
        }

        private void BypassLogin(bool isBypass)
        {
            if (isBypass)
            {
                ApplicationSettings.Instance.Security.EnableAuthentication = true; // option setting authentication app
                ApplicationSettings.Instance.Security.IgnoreAuthorization = true;
                ApplicationSettings.Instance.Security.SimulateAuthenticatedSession = true;
                ApplicationSettings.Instance.Security.EnableSingleSignOn = false;

                ApplicationSettings.Instance.Security.SimulatedAuthenticatedUser = new User()
                {
                    Username = "System",
                    Password = "toyota",
                    FirstName = "ISTD",
                    LastName = "User",
                    RegistrationNumber = "09709493"
                };
            }
            else
            {
                ApplicationSettings.Instance.Security.EnableAuthentication = true; // option setting authentication app
                ApplicationSettings.Instance.Security.IgnoreAuthorization = false;
                ApplicationSettings.Instance.Security.SimulateAuthenticatedSession = false;
                ApplicationSettings.Instance.Security.EnableSingleSignOn = false;
            }
        }

        protected override void Startup()
        {
            ProviderRegistry.Instance.Register<IUserProvider>(typeof(UserProvider), DatabaseManager.Instance, "SecurityCenter");
            //ProviderRegistry.Instance.Register<UILayout>(typeof(BootstrapLayout));
            //BootstrapLayout layout = (BootstrapLayout)ApplicationSettings.Instance.UI.GetLayout(); 
        }

        protected void Application_BeginRequest()
        {
            HttpContext.Current.Response.Cache.SetAllowResponseInBrowserHistory(false);
            HttpContext.Current.Response.Cache.SetCacheability(HttpCacheability.NoCache);
            HttpContext.Current.Response.Cache.SetNoStore();
            Response.Cache.SetExpires(DateTime.Now);
            Response.Cache.SetValidUntilExpires(true);
        }
    }
}