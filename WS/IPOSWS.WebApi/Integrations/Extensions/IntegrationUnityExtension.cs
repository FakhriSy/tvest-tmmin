﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;
using IPOSWS.WebApi.Integrations.Utilities;
using IPOSWS.WebApi.Services.Interfaces;

namespace IPOSWS.WebApi.Integrations.Extensions
{
    /// <summary>
    /// Integration unitoy extension
    /// </summary>
    public class IntegrationUnityExtension : UnityContainerExtension
    {
        /// <summary>
        /// Initialize
        /// </summary>
        protected override void Initialize()
        {
            var dmsClient = new IntegrationClient(ConfigurationManager.AppSettings.Get("DMSService"), ConfigurationManager.AppSettings.Get("DMSResource"));
            Container.RegisterType<IDMSService, DMSService>(
                new TransientLifetimeManager(),
                new InjectionConstructor(dmsClient));
        }
    }
}