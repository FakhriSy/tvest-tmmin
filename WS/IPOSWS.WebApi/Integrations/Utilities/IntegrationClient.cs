﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;

namespace IPOSWS.WebApi.Integrations.Utilities
{
    /// <summary>
    /// Integration client
    /// </summary>
    public class IntegrationClient : HttpClient
    {
        #region variables

        private readonly string identity;
        protected readonly string baseUrl;
        protected readonly string serviceResourceId;

        #endregion

        #region construction

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="baseAddress">Base address</param>
        /// <param name="resourceId">Resource id</param>
        public IntegrationClient(string baseAddress, string resourceId)
        {
            serviceResourceId = resourceId;
            baseUrl = baseAddress;
            BaseAddress = new Uri(baseUrl);
            identity = Guid.NewGuid().ToString();

            var timeout = GetClientTimeoutSetting();
            if (timeout.HasValue)
                this.Timeout = TimeSpan.FromSeconds(timeout.Value);
        }

        #endregion

        #region public method

        #region Identity

        /// <summary>
        /// Get client identity (to make sure implement singleton client correctly)
        /// </summary>
        /// <returns></returns>
        public string GetClientIdentity()
        {
            return identity;
        }

        /// <summary>
        /// Get client timeout
        /// </summary>
        /// <returns></returns>
        public int? GetClientTimeout()
        {
            return GetClientTimeoutSetting();
        }

        #endregion

        #region GET method

        /// <summary>
        /// Http GET with auto mapper conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <returns>T_out</returns>
        public async Task<T_out> GetContentAsync<T_in, T_out>(string url)
        {
            return await GetContentAsync<T_in, T_out>(url, null, null);
        }

        /// <summary>
        /// Http GET with action conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint adress</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <returns>T_out</returns>
        public async Task<T_out> GetContentAsync<T_in, T_out>(string url, Func<T_in, T_out> action)
        {
            return await GetContentAsync(url, action, null);
        }

        /// <summary>
        /// Http GET with action conversion from T_in to T_out (with message header injection)
        /// </summary>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint adress</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <param name="headerAction">Request header</param>
        /// <returns>T_out</returns>
        public async Task<T_out> GetContentAsync<T_in, T_out>(string url, Func<T_in, T_out> action, Action<HttpRequestHeaders> headerAction)
        {
            var serviceUrl = baseUrl + url;
            var token = GetServiceToken(serviceResourceId);

            var requestMessage = new HttpRequestMessage(HttpMethod.Get, serviceUrl);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            var response = SendAsync(requestMessage);

            var result = new IntegrationResult<T_in, T_out>(response, action);
            result.RequestMethod = "GET";
            result.RequestUrl = serviceUrl;

            return await result.BuildResult();
        }

        #endregion

        #region DELETE method

        /// <summary>
        /// Http DELETE
        /// </summary>
        /// <param name="url">Endpoint adress</param>
        /// <returns>Task</returns>
        public async Task DeleteContentAsync(string url)
        {
            await DeleteContentAsync(url, null);
        }

        /// <summary>
        /// Http DELETE (with message header injection)
        /// </summary>
        /// <param name="url">Endpoint adress</param>
        /// <param name="headerAction">Header action</param>
        /// <returns>Task</returns>
        public async Task DeleteContentAsync(string url, Action<HttpRequestHeaders> headerAction)
        {
            var serviceUrl = baseUrl + url;
            var token = GetServiceToken(serviceResourceId);

            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, serviceUrl);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            var response = SendAsync(requestMessage);

            var result = new IntegrationResult<string, string>(response);
            result.DisableAutoConversion = true;
            result.RequestMethod = "DELETE";
            result.RequestUrl = serviceUrl;

            await result.BuildResult();
        }

        /// <summary>
        /// Http DELETE with auto mapper conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint adress</param>
        /// <returns>T_out</returns>
        public async Task<T_out> DeleteContentAsync<T_in, T_out>(string url)
        {
            return await DeleteContentAsync<T_in, T_out>(url, null, null);
        }

        /// <summary>
        /// Http DELETE with action conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint adress</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <returns>T_out</returns>
        public async Task<T_out> DeleteContentAsync<T_in, T_out>(string url, Func<T_in, T_out> action)
        {
            return await DeleteContentAsync<T_in, T_out>(url, action, null);
        }

        /// <summary>
        /// Http DELETE with action conversion from T_in to T_out (with message header injection)
        /// </summary>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint adress</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <param name="headerAction">Request header</param>
        /// <returns>T_out</returns>
        public async Task<T_out> DeleteContentAsync<T_in, T_out>(string url, Func<T_in, T_out> action, Action<HttpRequestHeaders> headerAction)
        {
            var serviceUrl = baseUrl + url;
            var token = GetServiceToken(serviceResourceId);

            var requestMessage = new HttpRequestMessage(HttpMethod.Delete, serviceUrl);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            var response = SendAsync(requestMessage);

            var result = new IntegrationResult<T_in, T_out>(response, action);
            result.RequestMethod = "DELETE";
            result.RequestUrl = serviceUrl;

            return await result.BuildResult();
        }

        #endregion

        #region POST method

        /// <summary>
        /// Http POST with auto mapper conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_value">Payload to be post</typeparam>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <param name="value">T_value object</param>
        /// <returns>T_out</returns>
        public async Task<T_out> PostContentAsync<T_value, T_in, T_out>(string url, T_value value)
        {
            return await PostContentAsync<T_value, T_in, T_out>(url, value, null, null);
        }

        /// <summary>
        /// Http POST with action conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_value">Payload to be post</typeparam>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <param name="value">T_value object</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <returns>T_out</returns>
        public async Task<T_out> PostContentAsync<T_value, T_in, T_out>(string url, T_value value, Func<T_in, T_out> action)
        {
            return await PostContentAsync(url, value, action, null);
        }

        /// <summary>
        /// Http POST with action conversion from T_in to T_out (with message header injection)
        /// </summary>
        /// <typeparam name="T_value">Payload to be post</typeparam>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <param name="value">T_value object</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <param name="headerAction">Request header</param>
        /// <returns>T_out</returns>
        public async Task<T_out> PostContentAsync<T_value, T_in, T_out>(string url, T_value value, Func<T_in, T_out> action, Action<HttpRequestHeaders> headerAction)
        {
            var serviceUrl = baseUrl + url;
            var token = GetServiceToken(serviceResourceId);

            var payload = new ObjectContent<T_value>(value, new JsonMediaTypeFormatter());
            var requestMessage = new HttpRequestMessage(HttpMethod.Post, serviceUrl) { Content = payload };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            var response = SendAsync(requestMessage);

            var result = new IntegrationResult<T_in, T_out>(response, action);
            result.RequestMethod = "POST";
            result.RequestUrl = serviceUrl;
            result.RequestPayload = Newtonsoft.Json.JsonConvert.SerializeObject(value);

            return await result.BuildResult();
        }

        #endregion

        #region PATCH method

        /// <summary>
        /// Http PATCH
        /// </summary>
        /// <param name="url">Endpoint adress</param>
        /// <param name="value">Value</param>
        /// <returns>Task</returns>
        public async Task PatchContentAsync<T_value>(string url, T_value value)
        {
            await PatchContentAsync(url, value, null);
        }

        /// <summary>
        /// Http PATCH (with message header injection)
        /// </summary>
        /// <param name="url">Endpoint adress</param>
        /// <param name="value">Value</param>
        /// <param name="headerAction">Header action</param>
        /// <returns>Task</returns>
        public async Task PatchContentAsync<T_value>(string url, T_value value, Action<HttpRequestHeaders> headerAction)
        {
            var serviceUrl = baseUrl + url;
            var token = GetServiceToken(serviceResourceId);

            var requestMessage = new HttpRequestMessage(new HttpMethod("PATCH"), serviceUrl);
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            var response = SendAsync(requestMessage);

            var result = new IntegrationResult<string, string>(response);
            result.DisableAutoConversion = true;
            result.RequestMethod = "PATCH";
            result.RequestUrl = serviceUrl;

            await result.BuildResult();
        }

        /// <summary>
        /// Http PATCH with auto mapper conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_value">Payload to be patch</typeparam>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <param name="value">T_value object</param>
        /// <returns>T_out</returns>
        public async Task<T_out> PatchContentAsync<T_value, T_in, T_out>(string url, T_value value)
        {
            return await PatchContentAsync<T_value, T_in, T_out>(url, value, null, null);
        }

        /// <summary>
        /// Http PATCH with action conversion from T_in to T_out
        /// </summary>
        /// <typeparam name="T_value">Payload to be patch</typeparam>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <param name="value">T_value object</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <returns>T_out</returns>
        public async Task<T_out> PatchContentAsync<T_value, T_in, T_out>(string url, T_value value, Func<T_in, T_out> action)
        {
            return await PatchContentAsync(url, value, action, null);
        }

        /// <summary>
        /// Http PATCH with action conversion from T_in to T_out (with message header injection)
        /// </summary>
        /// <typeparam name="T_value">Payload to be patch</typeparam>
        /// <typeparam name="T_in">Result from service</typeparam>
        /// <typeparam name="T_out">Expected output type</typeparam>
        /// <param name="url">Endpoint address</param>
        /// <param name="value">T_value object</param>
        /// <param name="action">Function to convert T_in to T_out (set null to use default AutoMapper)</param>
        /// <param name="headerAction">Request header</param>
        /// <returns>T_out</returns>
        public async Task<T_out> PatchContentAsync<T_value, T_in, T_out>(string url, T_value value, Func<T_in, T_out> action, Action<HttpRequestHeaders> headerAction)
        {
            var ServiceUrl = baseUrl + url;
            var token = GetServiceToken(serviceResourceId);

            var content = new ObjectContent<T_value>(value, new JsonMediaTypeFormatter());
            var requestMessage = new HttpRequestMessage(new HttpMethod("PATCH"), ServiceUrl) { Content = content };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            var response = SendAsync(requestMessage);

            var result = new IntegrationResult<T_in, T_out>(response, action);
            result.RequestMethod = "PATCH";
            result.RequestUrl = ServiceUrl;
            result.RequestPayload = Newtonsoft.Json.JsonConvert.SerializeObject(value);

            return await result.BuildResult();
        }

        #endregion

        #region SEND method

        /// <summary>
        /// Send HttpContent
        /// </summary>
        /// <param name="method">Http method</param>
        /// <param name="content">Http content</param>
        /// <param name="url">Endpoint adress</param>
        /// <returns>HttpResponseMessage</returns>
        public async Task<HttpResponseMessage> SendContentAsync(HttpMethod method, HttpContent content, string url)
        {
            return await SendContentAsync(method, content, url, null);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="method">Http method</param>
        /// <param name="content">Http content</param>
        /// <param name="url">Endpoint adress</param>
        /// <param name="headerAction">Request header</param>
        /// <returns>HttpResponseMessage</returns>
        public async Task<HttpResponseMessage> SendContentAsync(HttpMethod method, HttpContent content, string url, Action<HttpRequestHeaders> headerAction)
        {
            var token = GetServiceToken(serviceResourceId);

            var requestMessage = new HttpRequestMessage(method, url) { Content = content };
            requestMessage.Headers.Authorization = new AuthenticationHeaderValue("Bearer", token);

            if (headerAction != null)
                headerAction.Invoke(requestMessage.Headers);

            return await SendAsync(requestMessage);
        }

        #endregion

        #endregion

        #region private method

        private string GetServiceToken(string resourceId)
        {
            string authority = "";
            string clientId = "";
            string clientAppKey = "";
            return "token";
        }

        private int? GetClientTimeoutSetting()
        {
            try
            {
                var timeoutSetting = ConfigurationManager.AppSettings["ClientTimeout"];
                int? timeout = null;
                var timeoutInt = 0;
                if (int.TryParse(timeoutSetting, out timeoutInt)) timeout = timeoutInt;
                return timeout;
            }
            catch
            {
                return null;
            }
        }

        #endregion
    }
}