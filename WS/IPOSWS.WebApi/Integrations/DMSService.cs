﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IPOSWS.WebApi.Integrations.Utilities;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Services.Interfaces;

namespace IPOSWS.WebApi.Integrations
{
    /// <summary>
    /// DMS service
    /// </summary>
    public class DMSService : BaseService, IDMSService
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="client">Client</param>
        public DMSService(IntegrationClient client) : base(client)
        {

        }
    }
}