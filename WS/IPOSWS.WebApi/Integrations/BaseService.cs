﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOSWS.WebApi.Integrations.Utilities;
using IPOSWS.WebApi.Services.Interfaces;

namespace IPOSWS.WebApi.Integrations
{
    /// <summary>
    /// Base service
    /// </summary>
    public class BaseService : IBaseService
    {
        /// <summary>
        /// Integration client
        /// </summary>
        protected readonly IntegrationClient client;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="httpClient">Httpclient</param>
        public BaseService(IntegrationClient httpClient)
        {
            client = httpClient;
        }

        /// <summary>
        /// Get client identity
        /// </summary>
        /// <returns></returns>
        public string GetClientIdentity()
        {
            return client.GetClientIdentity();
        }

        /// <summary>
        /// Get client timeout
        /// </summary>
        /// <returns></returns>
        public int? GetClientTimeout()
        {
            return client.GetClientTimeout();
        }
    }
}