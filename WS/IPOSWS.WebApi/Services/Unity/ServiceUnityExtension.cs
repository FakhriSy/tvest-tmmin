﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using IPOSWS.WebApi.Services.Interfaces;
using IPOSWS.WebApi.Data.SqlServer.Unity;
using IPOSWS.WebApi.Integrations;
using IPOSWS.WebApi.Integrations.Extensions;

namespace IPOSWS.WebApi.Services.Unity
{
    /// <summary>
    /// Service unity extension
    /// </summary>
    public class ServiceUnityExtension : UnityContainerExtension
    {
        /// <summary>
        /// Initialize
        /// </summary>
        protected override void Initialize()
        {
            Container.AddNewExtension<EFUnityExtension>();
            Container.AddNewExtension<IntegrationUnityExtension>();

            Container.RegisterType<IDataActionService, DataActionService>(new PerResolveLifetimeManager());
            Container.RegisterType<IDMSActionService, DMSActionService>(new PerResolveLifetimeManager());

            Container.RegisterType<IDataService, DataService>(new PerResolveLifetimeManager());
        }
    }
}