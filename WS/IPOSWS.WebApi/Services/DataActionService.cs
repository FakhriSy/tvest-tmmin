﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Services.Interfaces;

namespace IPOSWS.WebApi.Services
{
    /// <summary>
    /// Data action service
    /// </summary>
    public class DataActionService : IDataActionService
    {
        /// <summary>
        /// Data service
        /// </summary>
        [Dependency]
        public IDataService DataService { get; set; }
    }
}