﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Services.Interfaces;

namespace IPOSWS.WebApi.Services
{
    /// <summary>
    /// DMS action service
    /// </summary>
    public class DMSActionService : IDMSActionService
    {
        /// <summary>
        /// Data action service
        /// </summary>
        [Dependency]
        public IDataActionService DataActionService { get; set; }

        /// <summary>
        /// DMS service
        /// </summary>
        [Dependency]
        public IDMSService DMSService { get; set; }
    }
}