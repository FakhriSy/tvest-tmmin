﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED050600;
using IPOSWS.WebApi.Models.CED050200;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED050600Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetPickingTypeResponse))]
        [HttpPost, Route("GetPickingType")]
        public async Task<IHttpActionResult> GetPickingType()
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetPickingTypeResponse resp = new GetPickingTypeResponse();

                return await Task.Run(() =>
                {
                    var o = _context.Database.SqlQuery<CED050600PickingType>("exec [CED050600A_GetPickingType]").ToList<CED050600PickingType>();

                    resp = common.CED050600GetPickingType(o);

                    return Ok<GetPickingTypeResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }            
        }



        [ResponseType(typeof(GetPickingResponse))]
        [HttpPost, Route("GetPicking")]
        public async Task<IHttpActionResult> GetPicking(GetPickingRequest param)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetPickingResponse resp = new GetPickingResponse();
                
                return await Task.Run(() =>
                {

                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.UserName
                    };


                    var pKanbanHeader = new SqlParameter
                    {
                        ParameterName = "KanbanHeader",
                        Value = param.KanbanHeader
                    };

                    var pLine = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = param.Line
                    };


                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = pUserName;
                    p[1] = pKanbanHeader;
                    p[2] = pLine;

                    var o = _context.Database.SqlQuery<CED050600PickingData>("exec [CED050600A_GetPickingData] @UserName,@KanbanHeader,@Line", p).ToList<CED050600PickingData>();

                    resp = common.CED050600GetPickingData(o);

                    return Ok<GetPickingResponse>(resp);

                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetShooterDataResponse))]
        [HttpPost, Route("GetShooterData")]
        public async Task<IHttpActionResult> GetShooterData(GetShooterDataRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetShooterDataResponse resp = new GetShooterDataResponse();

                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.UserName
                    };

                    var pControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = param.ControlModuleNo
                    };


                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = pUserName;
                    p[1] = pControlModuleNo;


                    var o = _context.Database.SqlQuery<CED050600Shooter>("exec [CED050600A_GetShooterData] @UserName,@ControlModuleNo", p).ToList<CED050600Shooter>();

                    resp = common.CED050600GetShooter(o);

                    return Ok<GetShooterDataResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(PostpickingResponse))]
        [HttpPost, Route("Postpicking")]
        public async Task<IHttpActionResult> Postpicking(PostpickingRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                PostpickingResponse resp = new PostpickingResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var Kanban3 = new SqlParameter
                    {
                        ParameterName = "kanban3",
                        Value = parameter.kanban3
                    };

                    var Kanban4 = new SqlParameter
                    {
                        ParameterName = "kanban4",
                        Value = parameter.kanban4
                    };

                    var TotalTime = new SqlParameter
                    {
                        ParameterName = "totalTime",
                        Value = parameter.totalTime
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    //[wot.farhan] add variabel Partno
                    var PartNo = new SqlParameter
                    {
                        ParameterName = "PartNo",
                        Value = parameter.PartNo
                    };

                    SqlParameter[] p = new SqlParameter[6];
                    p[0] = UserName;
                    p[1] = Kanban3;
                    p[2] = Kanban4;
                    p[3] = TotalTime;
                    p[4] = Line;
                    p[5] = PartNo; //[wot.farhan] add variabel Partno

                    var o = _context.Database.SqlQuery<PostpickingResponse>("exec [CED050600A_PostPicking] @UserName,@Kanban3,@Kanban4,@TotalTime,@Line,@PartNo", p).SingleOrDefault<PostpickingResponse>();

                    resp = common.CED050600PostpickingResponse(o);

                    return Ok<PostpickingResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetShooterUpdateResponseData))]
        [HttpPost, Route("PostSupplyShooter")]
        public async Task<IHttpActionResult> PostShooterSupplyData(GetShooterUpdateDataRequest parameter)
        {

            int paramCounter = 0;
            string kanbanId = string.Empty;

            foreach (var item in parameter.kanbanId)
            {
                paramCounter++;
                kanbanId += item.kanbanID + (paramCounter < parameter.kanbanId.Count() ? "|" : "");
            }


            CommonRepository common = new CommonRepository();

            try
            {
                GetShooterUpdateResponseData resp = new GetShooterUpdateResponseData();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var pKanbanId = new SqlParameter
                    {
                        ParameterName = "KanbanId",
                        Value = kanbanId
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    //var controlModuleNo = new SqlParameter
                    //{
                    //    ParameterName = "ControlModuleNo",
                    //    Value = parameter.controlModuleNo
                    //};

                    //var kanbanHeader = new SqlParameter
                    //{
                    //    ParameterName = "KanbanHeader",
                    //    Value = parameter.kanbanHeader
                    //};



                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = pKanbanId;
                    p[2] = Line;

                    var o = _context.Database.SqlQuery<BaseMessage>("exec [CED050600A_PostShooterSupply] @UserName,@KanbanId,@Line", p).ToList<BaseMessage>();

                    resp.Data = o;

                    return Ok<GetShooterUpdateResponseData>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(PostProblemPickingUpdateResponse))]
        [HttpPost, Route("PostProblemPicking")]
        public async Task<IHttpActionResult> PostProblemPicking(PostProblemPickingUpdateRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                PostProblemPickingUpdateResponse resp = new PostProblemPickingUpdateResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = parameter.kanbanID
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = KanbanID;
                    p[2] = Line;

                    var o = _context.Database.SqlQuery<PostProblemPickingUpdateResponse>("exec [CED050600A_PostProblemPicking] @UserName,@KanbanID,@Line", p).SingleOrDefault<PostProblemPickingUpdateResponse>();

                    resp = common.CED050600GetProblemPickingUpdateResponse(o);

                    return Ok<PostProblemPickingUpdateResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }




        [ResponseType(typeof(GetZoneResponse))]
        [HttpPost, Route("GetZonePicking")]
        public async Task<IHttpActionResult> GetProblemBarcode(GetZoneRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetZoneResponse resp = new GetZoneResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.userName
                    };

                    var ZoneGroupingCd = new SqlParameter
                    {
                        ParameterName = "ZoneGroupingCd",
                        Value = "PC"
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = ZoneGroupingCd;

                    var o = _context.Database.SqlQuery<GetZone>("exec [Common_GetZone] @UserName,@ZoneGroupingCd", p).ToList<GetZone>();

                    resp = common.CED050200GetZone(o);

                    return Ok<GetZoneResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        													

    }
}
