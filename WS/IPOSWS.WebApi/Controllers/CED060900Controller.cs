﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED060900;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED060900Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetModuleKanbanResponse))]
        [HttpGet, Route("GetModuleKanban")]
        public async Task<IHttpActionResult> GetModuleKanban()
        {

            CommonRepository common = new CommonRepository();

            try
            {
                List<GetModuleKanbanResponse> resp = new List<GetModuleKanbanResponse>();

                return await Task.Run(() =>
                {
                    var o = _context.Database.SqlQuery<GetModuleKanbanResponse>("exec [CED060900B_GetModuleKanban]").ToList<GetModuleKanbanResponse>();

                    resp = common.CED060900BGetModuleKanbanResponse(o);

                    return Ok<List<GetModuleKanbanResponse>>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(SubmitCloseModuleResponse))]
        [HttpPost, Route("SubmitCloseModule")]
        public async Task<IHttpActionResult> SubmitCloseModule(SubmitCloseModuleRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                List<GetModuleKanbanResponse> resp = new List<GetModuleKanbanResponse>();

                return await Task.Run(() =>
                {
                    var CtrlModNo = new SqlParameter
                    {
                        ParameterName = "CtrlModNo",
                        Value = parameter.CtrlModNo
                    };

                    var CloseModTime = new SqlParameter
                    {
                        ParameterName = "CloseModTime",
                        Value = parameter.CloseModTime
                    };

                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = CtrlModNo;
                    p[1] = CloseModTime;
                    p[2] = UserName;

                    var o = _context.Database.SqlQuery<GetModuleKanbanResponse>("exec [CED060900B_SubmitCloseModule] @CtrlModNo,@CloseModTime,@UserName", p).ToList<GetModuleKanbanResponse>();

                    resp = common.CED060900BGetModuleKanbanResponse(o);

                    return Ok<GetModuleKanbanResponse>(resp.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(SubmitKanbanCrippleResponse))]
        [HttpPost, Route("SubmitKanbanCripple")]
        public async Task<IHttpActionResult> SubmitKanbanCripple([FromBody] SubmitKanbanCrippleRequest parameter)
        {
            int paramCounter = 0;
            string kanbanId = string.Empty;
            string stackingTime = string.Empty;
            string crippleSts = string.Empty;
            string partNo = string.Empty;
            string boxNo = string.Empty;

            foreach (var item in parameter.KanbanList)
            {
                paramCounter += 1;
                kanbanId += item.KanbanID + (paramCounter < parameter.KanbanList.Count() ? "|" : "");
                stackingTime += item.StackingTime + (paramCounter < parameter.KanbanList.Count() ? "|" : "");
                crippleSts += item.CrippleSts + (paramCounter < parameter.KanbanList.Count() ? "|" : "");
                partNo += item.PartNo + (paramCounter < parameter.KanbanList.Count() ? "|" : "");
                boxNo += item.BoxNo + (paramCounter < parameter.KanbanList.Count() ? "|" : "");
            }


            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var CtrlModNo = new SqlParameter
                    {
                        ParameterName = "CtrlModNo",
                        Value = parameter.CtrlModNo
                    };
                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = kanbanId + "|"
                    };
                    var StackingTime = new SqlParameter
                    {
                        ParameterName = "StackingTime",
                        Value = stackingTime + "|"
                    };
                    var CrippleSts = new SqlParameter
                    {
                        ParameterName = "CrippleSts",
                        Value = crippleSts + "|"
                    };
                    var PartNo = new SqlParameter
                    {
                        ParameterName = "PartNo",
                        Value = partNo + "|"
                    };
                    var BoxNo = new SqlParameter
                    {
                        ParameterName = "BoxNo",
                        Value = boxNo + "|"
                    };
                    var FoundDt = new SqlParameter
                    {
                        ParameterName = "FoundDt",
                        Value = parameter.FoundDt
                    };
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[8];
                    p[0] = KanbanID;
                    p[1] = StackingTime;
                    p[2] = CrippleSts;
                    p[3] = PartNo;
                    p[4] = BoxNo;
                    p[5] = CtrlModNo;
                    p[6] = FoundDt;
                    p[7] = UserName;

                    var o = _context.Database.SqlQuery<SubmitKanbanCrippleResponse>("exec [CED060900B_SubmitKanbanCripple] @KanbanID,@StackingTime,@CrippleSts,@PartNo,@BoxNo,@CtrlModNo,@FoundDt,@UserName", p).ToList<SubmitKanbanCrippleResponse>();

                    return Ok <List<SubmitKanbanCrippleResponse>>(o);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

    }
}
