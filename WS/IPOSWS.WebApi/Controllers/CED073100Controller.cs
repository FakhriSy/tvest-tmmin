﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED073100;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED073100Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetCrippleDataResponse))]
        [HttpPost, Route("GetCrippleData")]
        public async Task<IHttpActionResult> GetCrippleData(GetCrippleDataRequest parameter)
        {
            GetCrippleDataResponse resp = new GetCrippleDataResponse();
            CommonRepository common = new CommonRepository();
            try
            {
                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = UserName;

                    var o = _context.Database.SqlQuery<CrippleDataResponse>("exec [SP_CED073100B_GetCrippleData] @UserName", p).ToList<CrippleDataResponse>();
                    resp = common.CrippleDataOutputResponse(o);
                    return Ok<GetCrippleDataResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(GetCripplePartResponse))]
        [HttpPost, Route("GetCripplePart")]
        public async Task<IHttpActionResult> GetCripplePart(GetCripplePartRequest parameter)
        {
            CommonRepository common = new CommonRepository();
            try
            {
                GetCripplePartResponse resp = new GetCripplePartResponse();

                return await Task.Run(() =>
                {
                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = ControlModuleNo;

                    var o = _context.Database.SqlQuery<GetCripplePartResponse>("exec [SP_CED073100B_GetCripplePart] @ControlModuleNo", p).ToList<GetCripplePartResponse>();

                    resp = common.CED073100BGetCripplePartResponse(o);

                    return Ok<GetCripplePartResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        [ResponseType(typeof(PostCrippleResponse))]
        [HttpPost, Route("PostCripple")]
        public async Task<IHttpActionResult> PostCripple(PostCrippleRequest parameter)
        {
            CommonRepository common = new CommonRepository();
            try
            {
                return await Task.Run(() =>
                {
                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var ChangedBy = new SqlParameter
                    {
                        ParameterName = "ChangedBy",
                        Value = parameter.ChangedBy
                    };
                    var ChangedDt = new SqlParameter
                    {
                        ParameterName = "ChangedDt",
                        Value = parameter.ChangedDt
                    };


                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = ControlModuleNo;
                    p[1] = UserName;
                    p[2] = ChangedBy;
                    p[3] = ChangedDt;

                    var o = _context.Database.SqlQuery<PostCrippleResponse>("exec [SP_CED073100B_PostCripple] @ControlModuleNo, @UserName, @ChangedBy , @ChangedDt", p).ToList<PostCrippleResponse>();
                    
                    return Ok<PostCrippleResponse>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

    }
}
