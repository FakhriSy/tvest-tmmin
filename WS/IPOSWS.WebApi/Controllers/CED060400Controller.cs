﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED060400;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED060400Controller : CorsController //ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetPrepareModuleDataResponse))]
        [HttpPost, Route("GetPrepareModuleData")]
        public async Task<IHttpActionResult> GetPrepareModuleData(GetPrepareModuleDataRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetPrepareModuleDataResponse resp = new GetPrepareModuleDataResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = Shift;

                    var o = _context.Database.SqlQuery<PostPrepareModuleDataResponse>("exec [CED060400B_GetPrepareModuleData] @UserName,@Shift", p).ToList<PostPrepareModuleDataResponse>();

                    resp = common.PostPrepareModuleDataOutputResponse(o);

                    return Ok<GetPrepareModuleDataResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(PostPrepareModuleResponse))]
        [HttpPost, Route("PostPrepareModule")]
        public async Task<IHttpActionResult> PostPrepareModule(PostPrepareModuleRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var ActualStart = new SqlParameter
                    {
                        ParameterName = "ActualStart",
                        Value = parameter.ActualStart
                    };

                    var ActualFinish = new SqlParameter
                    {
                        ParameterName = "ActualFinish",
                        Value = parameter.ActualFinish
                    };

                    var ActualCT = new SqlParameter
                    {
                        ParameterName = "ActualCT",
                        Value = parameter.ActualCT
                    };

                    var ChangedBy = new SqlParameter
                    {
                        ParameterName = "ChangedBy",
                        Value = parameter.ChangedBy
                    };

                    var ChangedDt = new SqlParameter
                    {
                        ParameterName = "ChangedDt",
                        Value = parameter.ChangedDt
                    };

                    SqlParameter[] p = new SqlParameter[7];
                    p[0] = UserName;
                    p[1] = ControlModuleNo;
                    p[2] = ActualStart;
                    p[3] = ActualFinish;
                    p[4] = ActualCT;
                    p[5] = ChangedBy;
                    p[6] = ChangedDt;

                    var o = _context.Database.SqlQuery<PostPrepareModuleResponse>("exec [CED060400B_PostPrepareModule] @UserName,@ControlModuleNo,@ActualStart,@ActualFinish,@ActualCT,@ChangedBy,@ChangedDt", p).ToList<PostPrepareModuleResponse>();
                    return Ok<PostPrepareModuleResponse>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

    }
}
