﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models.CED010100;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IPOSWS.WebApi.Controllers
{
    public class CED010100Controller : CorsController //ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [AllowAnonymous]
        [HttpGet, Route("GetCommonDataShift")]
        public IHttpActionResult GetCommonDataShift()
        {
            string sql = "SELECT SYS_CD ShiftCode, SYS_VAL ShiftName FROM TB_M_SYSTEM WHERE SYS_CAT='00000' AND SYS_SUB_CAT='SHIFT'";
            var o = _context.Database.SqlQuery<CED010100Shift>(sql).ToList<CED010100Shift>();
            return Ok<List<CED010100Shift>>(o);
        }

        [AllowAnonymous]
        [HttpGet, Route("GetCommonDataLine")]
        public IHttpActionResult GetCommonDataLine()
        {
            string sql = "SELECT LINE_CD LineCode, LINE_NM LineName FROM TB_M_LINE";
            var o = _context.Database.SqlQuery<CED010100Line>(sql).ToList<CED010100Line>();
            return Ok<List<CED010100Line>>(o);
        }

        [AllowAnonymous]
        [HttpGet, Route("GetCommonDataZone")]
        public IHttpActionResult GetCommonDataZone()
        {
            string sql = "SELECT ZONE_CD ZoneCode, ZONE_NM ZoneName FROM TB_M_ZONE_GROUPING_D";
            var o = _context.Database.SqlQuery<CED010100Zone>(sql).ToList<CED010100Zone>();
            return Ok<List<CED010100Zone>>(o);
        }
    }
}
