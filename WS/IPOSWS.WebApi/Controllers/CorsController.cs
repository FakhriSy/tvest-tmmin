﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace IPOSWS.WebApi.Controllers
{
    /// <summary>
    /// Handle cors issue
    /// </summary>
    [Authorize]
    public class CorsController : BaseApiController
    {

    }
}