﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED010203;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{
    [RoutePrefix("api/v1")]
    public class CED010203Controller : CorsController // ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(EngineDataResponse))]
        [HttpPost, Route("GetPostEngineData")]
        public async Task<IHttpActionResult> GetPostEngineData(EngineDataRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var CaseMark = new SqlParameter
                    {
                        ParameterName = "CaseMark",
                        Value = parameter.CaseMark
                    };

                    SqlParameter[] param = new SqlParameter[2];
                    param[0] = UserName;
                    param[1] = CaseMark;

                    var res = _context.Database.SqlQuery<EngineDataResponse>("exec [CED010203_GetPostEngineData] @UserName, @CaseMark", param).ToList<EngineDataResponse>();

                    return Ok<EngineDataResponse>(res.FirstOrDefault());
                });
            }
            catch (Exception e)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(e.Message));
            }

        }
    }
}