﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED072100;
using IPOSWS.WebApi.Models.USER;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{
    public class UserController : ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetUserResponse))]
        [HttpPost, Route("GetUserRequest")]
        public async Task<IHttpActionResult> GetUser(GetUserRequest parameter)
        {

            return await Task.Run(() =>
            {
                var reg = new SqlParameter
                {
                    ParameterName = "reg",
                    Value = parameter.Reg
                };

                var line = new SqlParameter
                {
                    ParameterName = "line",
                    Value = parameter.Line
                };

                SqlParameter[] p = new SqlParameter[2];
                p[0] = reg;
                p[1] = line;

                var o = _context.Database.SqlQuery<GetUserResponse>("exec [sp_GetDataUser] @reg,@line", p).ToList<GetUserResponse>();
                return Ok<List<GetUserResponse>>(o);
            });
        }
    }
}
