﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED072100;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED072100Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetInvestigatingProblemResponse))]
        [HttpPost, Route("GetInvestigatingProblemRequest")]
        public async Task<IHttpActionResult> GetInvestigatingProblem(GetInvestigatingProblemRequest parameter)
        {
            GetInvestigatingProblemResponse resp = new GetInvestigatingProblemResponse();
            CommonInvest common = new CommonInvest();
            CommonRepository commonRepository = new CommonRepository();
            try
            {

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var PartNo = new SqlParameter
                    {
                        ParameterName = "PartNo",
                        Value = parameter.PartNo
                    };

                    var Supplier = new SqlParameter
                    {
                        ParameterName = "Supplier",
                        Value = parameter.Supplier
                    };

                    var Problem = new SqlParameter
                    {
                        ParameterName = "Problem",
                        Value = parameter.Problem
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = PartNo;
                    p[2] = Supplier;
                    p[3] = Problem;

                    var o = _context.Database.SqlQuery<PostDataResponseInv>("exec [CED072100W_GetInvestigatingProblem] @UserName,@PartNo,@Supplier,@Problem", p).ToList<PostDataResponseInv>();
                    resp = common.investigatingPResponse(_context, o);
                    return Ok<GetInvestigatingProblemResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(commonRepository.OtputErrorMessage(ae.Message));
            }
                
        }

    }
}
