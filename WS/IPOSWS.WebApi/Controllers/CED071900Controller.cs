﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED071900;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED071900Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetNewProblemRequest))]
        [HttpPost, Route("GetNewProblem")]
        public async Task<IHttpActionResult> GetNewProblem(GetNewProblemRequest parameter)
        {
            CommonRepository common = new CommonRepository();
            try
            {
                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var PartNo = new SqlParameter
                    {
                        ParameterName = "PartNo",
                        Value = parameter.PartNo
                    };

                    var Supplier = new SqlParameter
                    {
                        ParameterName = "Supplier",
                        Value = parameter.Supplier
                    };

                    var Problem = new SqlParameter
                    {
                        ParameterName = "Problem",
                        Value = parameter.Problem
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = PartNo;
                    p[2] = Supplier;
                    p[3] = Problem;

                    var o = _context.Database.SqlQuery<PostDataResponse>("exec [CED071900_GetNewProblem] @UserName,@PartNo,@Supplier,@Problem", p).ToList<PostDataResponse>();

                    IHttpActionResult result = null;
                    if (o.Count > 0)
                    {
                        result = Ok(new
                        {
                            TotalCase = o.Count,
                            TotalPcs = o.Sum(x => x.KanbanQty),
                            data = o,
                            Result = "Success",
                            Message = "MCEDSTD013I : Data retrieved successfully"
                        });
                    }
                    else
                    {
                        result = Ok(new
                        {
                            data = o,
                            TotalCase = o.Count,
                            TotalPcs = o.Sum(x => x.KanbanQty),
                            Result = "Success",
                            Message = "MCEDSTD030I : Data Not Found"
                        });
                    }

                    return result;
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
                
        }
    }
}
