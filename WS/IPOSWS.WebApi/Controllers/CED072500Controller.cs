﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED072500;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED072500Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetCompleteProblemResponse))]
        [HttpPost, Route("GetCompleteProblem")]
        public async Task<IHttpActionResult> GetCompleteProblem(GetCompleteProblemRequest parameter)
        {

            CommonRepository common = new CommonRepository();
            try
            {
                GetCompleteProblemResponse resp = new GetCompleteProblemResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var PartNo = new SqlParameter
                    {
                        ParameterName = "PartNo",
                        Value = parameter.PartNo
                    };
                    var Supplier = new SqlParameter
                    {
                        ParameterName = "Supplier",
                        Value = parameter.Supplier
                    };
                    var Problem = new SqlParameter
                    {
                        ParameterName = "Problem",
                        Value = parameter.Problem
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = PartNo;
                    p[2] = Supplier;
                    p[3] = Problem;

                    var o = _context.Database.SqlQuery<CED072500Problem>("exec [SP_CED072500B_GetCompleteProblem] @UserName,@PartNo,@Supplier,@Problem", p).ToList<CED072500Problem>();

                    resp = common.CED072500BGetCompleteProblemResponse(o);

                    return Ok<GetCompleteProblemResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
            
        }

        [ResponseType(typeof(PostConfirmProblemResponse))]
        [HttpPost, Route("PostConfirmProblem")]
        public async Task<IHttpActionResult> PostConfirmProblem(PostConfirmProblemRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            int paramCounter = 0;
            string uName = string.Empty;
            string probId = string.Empty;

            foreach (var item in parameter.Data)
            {
                paramCounter += 1;
                uName += item.UserName + (paramCounter < parameter.Data.Count() ? "|" : "");
                probId += item.ProblemID + (paramCounter < parameter.Data.Count() ? "|" : "");
            }

            try
            {
                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = uName + "|"
                    };
                    var ProblemID = new SqlParameter
                    {
                        ParameterName = "ProblemID",
                        Value = probId + "|"
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = ProblemID;
                    var o = _context.Database.SqlQuery<PostConfirmProblemResponse>("exec [SP_CED072500B_PostConfirmProblem] @UserName,@ProblemID", p).ToList<PostConfirmProblemResponse>();
                    
                    return Ok<List<PostConfirmProblemResponse>>(o);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

    }
}
