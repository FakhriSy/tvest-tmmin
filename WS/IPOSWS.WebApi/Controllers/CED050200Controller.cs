﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED050200;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED050200Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetShooterResponse))]
        [HttpPost, Route("GetShooter")]
        public async Task<IHttpActionResult> GetShooter(GetShooterRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetShooterResponse resp = new GetShooterResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.UserName
                    };

                    var Shooter = new SqlParameter
                    {
                        ParameterName = "Shooter",
                        Value = param.Shooter
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = param.KanbanID
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = param.Line
                    };
                    
                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = Shooter;
                    p[2] = KanbanID;
                    p[3] = Line;

                    var o = _context.Database.SqlQuery<CED050200Shooter>("exec [CED050200A_GetShooterData] @UserName,@Shooter,@KanbanId,@Line", p).ToList<CED050200Shooter>();

                    resp = common.CED050200GetShooter(o);

                    return Ok<GetShooterResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
            
        }



        [ResponseType(typeof(GetShooterUpdateResponse))]
        [HttpPost, Route("PostShooterSupply")]
        public async Task<IHttpActionResult> PostShooterSupply(GetShooterUpdateRequest parameter)
        {

            int paramCounter = 0;
            string kanbanId = string.Empty;

            foreach (var item in parameter.kanbanId)
            {
                paramCounter++;
                kanbanId += item.kanbanID + (paramCounter < parameter.kanbanId.Count() ? "|" : "");
            }

            CommonRepository common = new CommonRepository();

            try
            {
                GetShooterUpdateResponse resp = new GetShooterUpdateResponse();


                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = kanbanId
                    };

                    var Status = new SqlParameter
                    {
                        ParameterName = "Status",
                        Value = parameter.status
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };
                    

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = KanbanID;
                    p[2] = Status;
                    p[3] = Line;

                    var o = _context.Database.SqlQuery<BaseMessage>("exec [CED050200A_PostShooterSupply] @UserName,@KanbanID,@Status,@Line", p).ToList<BaseMessage>();

                    resp.Data = o;

                    return Ok<GetShooterUpdateResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetProblemPickingUpdateResponse))]
        [HttpPost, Route("PostProblemShooter")]
        public async Task<IHttpActionResult> PostProblemPicking(GetProblemPickingUpdateRequest parameter)
        {

            CommonRepository common = new CommonRepository();


            try
            {
                GetProblemPickingUpdateResponse resp = new GetProblemPickingUpdateResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = parameter.kanbanID
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = KanbanID;
                    p[2] = Line;

                    var o = _context.Database.SqlQuery<GetProblemPickingUpdateResponse>("exec [CED050200A_PostProblemShooter] @UserName,@KanbanID,@Line", p).SingleOrDefault<GetProblemPickingUpdateResponse>();

                    resp = common.CED050200GetProblemPickingUpdateResponse(o);

                    return Ok<GetProblemPickingUpdateResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetProblemBarcodeResponse))]
        [HttpGet, Route("GetProblemBarcode")]
        public async Task<IHttpActionResult> GetProblemBarcode(string UserName = "")
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetProblemBarcodeResponse resp = new GetProblemBarcodeResponse();

                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = UserName
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = pUserName;

                    var o = _context.Database.SqlQuery<GetProblemBarcodeResponse>("exec [CED050200A_GetProblemBarcode] @UserName", p).ToList<GetProblemBarcodeResponse>();

                    resp = common.CED050200GetProblemBarcode(o);

                    return Ok<GetProblemBarcodeResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetZoneResponse))]
        [HttpPost, Route("GetZoneShooter")]
        public async Task<IHttpActionResult> GetProblemBarcode(GetZoneRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetZoneResponse resp = new GetZoneResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.userName
                    };

                    var ZoneGroupingCd = new SqlParameter
                    {
                        ParameterName = "ZoneGroupingCd",
                        Value = "SH"
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = ZoneGroupingCd;

                    var o = _context.Database.SqlQuery<GetZone>("exec [Common_GetZone] @UserName,@ZoneGroupingCd", p).ToList<GetZone>();

                    resp = common.CED050200GetZone(o);

                    return Ok<GetZoneResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        													

    }
}
