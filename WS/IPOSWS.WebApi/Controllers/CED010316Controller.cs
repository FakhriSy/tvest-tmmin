﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models.CED010316;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IPOSWS.WebApi.Controllers
{
    public class CED010316Controller : CorsController//ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        /// <summary>
        /// Get Data
        /// </summary>
        /// <param name="parameter">Prod Date</param>
        /// <returns>List</returns>
        [AllowAnonymous]
        [HttpGet, Route("GetDataPLaneSchedule")]
        public IHttpActionResult GetDataPLaneSchedule(string Prod_Date = null)
        {
            string sql = "sp_GetDataPLaneSchedule '" + Prod_Date + "'";
            var o = _context.Database.SqlQuery<CED010316>(sql).ToList<CED010316>();
            return Ok<List<CED010316>>(o);
        }

        /// <summary>
        /// Get Data
        /// </summary>
        /// <param name="parameter">Prod Date</param>
        /// <returns>List</returns>
        [AllowAnonymous]
        [HttpGet, Route("PostDataPLaneSchedule")]
        public IHttpActionResult PostDataPLaneSchedule(string Prod_Date, string PLane_Code, string Start_Time, string Actual_Time, string Type, string UserName)
        {
            string sql = "sp_PostDataPLaneSchedule '" + Prod_Date + "', '" + PLane_Code + "', '" + Start_Time + "', '" + Actual_Time + "', '" + Type + "', '" + UserName + "'";
            var o = _context.Database.SqlQuery<CED010316Post>(sql).ToList<CED010316Post>();
            return Ok<List<CED010316Post>>(o);
        }
    }
}
