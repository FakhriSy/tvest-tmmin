﻿using Microsoft.Practices.Unity;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.Description;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Services.Interfaces;

namespace IPOSWS.WebApi.Controllers.v1
{
    /// <summary>
    /// Access controller
    /// </summary>
    public class AccessController : CorsController
    {
        /// <summary>
        /// Get access token
        /// </summary>
        /// <param name="userName">username</param>
        /// <param name="password">Password</param>
        /// <returns>Access token (bearer)</returns>
        [AllowAnonymous]
        [ResponseType(typeof(string))]
        [HttpGet, Route("api/v1/Access/Token")]
        public async Task<IHttpActionResult> GetAccessToken(string userName, string password)
        {
            IHttpActionResult result = null;

            try
            {
                using (var client = new HttpClient())
                {
                    var formContent = new FormUrlEncodedContent(new[]
                    {
                        new KeyValuePair<string, string>("username", userName),
                        new KeyValuePair<string, string>("password", password),
                        new KeyValuePair<string, string>("grant_type", "password")
                    });
                    var scheme = Request.RequestUri.Scheme;
                    var host = Request.RequestUri.Host;
                    var port = Request.RequestUri.Port.ToString(CultureInfo.InvariantCulture);
                    var virtualPathRoot = Request.GetOwinContext().Get<string>("owin.RequestPathBase");
                    string baseUrl = string.Format("{0}://{1}:{2}{3}", scheme, host, port, "");

                    client.BaseAddress = new Uri(baseUrl);
                    var response = await client.PostAsync(client.BaseAddress + virtualPathRoot + "/oauth/token", formContent);
                    var token = await response.Content.ReadAsAsync<dynamic>();

                    result = Ok(new
                    {
                        Token = token
                    });
                }
            }
            catch (ArgumentException ae)
            {
                result = BadRequest(ae.Message);
            }

            return result;
        }


    }
}