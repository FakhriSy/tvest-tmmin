﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED060700;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class LoginController : ApiController //CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }




        [ResponseType(typeof(GetLoginStackingResponse))]
        [HttpPost, Route("Login")]
        public async Task<IHttpActionResult> GetLoginData(GetLoginStackingRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetLoginStackingResponse resp = new GetLoginStackingResponse();

                return await Task.Run(() =>
                {

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.Line
                    };
                    var NoReg = new SqlParameter
                    {
                        ParameterName = "NoReg",
                        Value = parameter.NoReg
                    };
                    var DeviceName = new SqlParameter
                    {
                        ParameterName = "DeviceName",
                        Value = parameter.DeviceName
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = Line;
                    p[1] = NoReg;
                    p[2] = DeviceName;

                    var o = _context.Database.SqlQuery<GetLoginStackingResponse>("exec [COMMON_Login] @Line,@NoReg,@DeviceName", p).ToList<GetLoginStackingResponse>();

                    resp = common.CED060700BGetLoginStackingResponse(o);

                    return Ok<GetLoginStackingResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }



        [ResponseType(typeof(GetLoginLeaderResponse))]
        [HttpPost, Route("LoginLeader")]
        public async Task<IHttpActionResult> GetLoginDataLeader(GetLoginLeaderRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetLoginLeaderResponse resp = new GetLoginLeaderResponse();

                return await Task.Run(() =>
                {

                    var Username = new SqlParameter
                    {
                        ParameterName = "Username",
                        Value = parameter.Username
                    };
                    var Password = new SqlParameter
                    {
                        ParameterName = "Password",
                        Value = parameter.Password
                    };
                    var DeviceName = new SqlParameter
                    {
                        ParameterName = "DeviceName",
                        Value = parameter.DeviceName
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = Username;
                    p[1] = Password;
                    p[2] = DeviceName;

                    var o = _context.Database.SqlQuery<GetLoginLeaderResponse>("exec [COMMON_LoginLeader] @Username,@Password,@DeviceName", p).ToList<GetLoginLeaderResponse>();

                    resp = common.CED060700BGetLoginLeaderResponse(o);

                    return Ok<GetLoginLeaderResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }




        [ResponseType(typeof(BaseMessage))]
        [HttpPost, Route("Logout")]
        public async Task<IHttpActionResult> GetLoginDataLeader(LogoutRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {

                return await Task.Run(() =>
                {

                    var NoReg = new SqlParameter
                    {
                        ParameterName = "NoReg",
                        Value = parameter.NoReg
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = NoReg;

                    var o = _context.Database.SqlQuery<BaseMessage>("exec [COMMON_Logout] @NoReg", p).ToList<BaseMessage>();

                    return Ok<BaseMessage>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        //[ResponseType(typeof(GetLoginStackingResponse))]
        //[HttpPost, Route("Login")]
        //public async Task<IHttpActionResult> GetLoginStackingData(GetLoginStackingRequest parameter)
        //{
        //    try
        //    {
        //        GetLoginStackingResponse resp = new GetLoginStackingResponse();
        //        CommonRepository common = new CommonRepository();

        //        return await Task.Run(() =>
        //        {

        //            var Line = new SqlParameter
        //            {
        //                ParameterName = "Line",
        //                Value = parameter.Line
        //            };
        //            var NoReg = new SqlParameter
        //            {
        //                ParameterName = "NoReg",
        //                Value = parameter.NoReg
        //            };
        //            //var DeviceName = new SqlParameter
        //            //{
        //            //    ParameterName = "DeviceName",
        //            //    Value = parameter.DeviceName
        //            //};

        //            SqlParameter[] p = new SqlParameter[2];
        //            p[0] = Line;
        //            p[1] = NoReg;
        //            //p[2] = DeviceName;
        //            //,@DeviceName

        //            var o = _context.Database.SqlQuery<GetLoginStackingResponse>("exec [CED060700B_Login] @Line,@NoReg", p).ToList<GetLoginStackingResponse>();

        //            resp = common.CED060700BGetLoginStackingResponse(o);

        //            return Ok<GetLoginStackingResponse>(resp);
        //        });
        //    }
        //    catch (Exception ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }

        //}


    }
}
