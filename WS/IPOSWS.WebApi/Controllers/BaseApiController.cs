﻿using IPOSWS.WebApi.Models;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using System.Net.Http;
using Microsoft.AspNet.Identity.Owin;

namespace IPOSWS.WebApi.Controllers
{
    /// <summary>
    /// Base api controller
    /// </summary>
    public class BaseApiController : ApiController
    {

    }
}