﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models.CED072300A;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using IPOSWS.WebApi.Models;


namespace IPOSWS.WebApi.Controllers
{
    
    [RoutePrefix("api/v1")]
    public class CED072300AController : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [HttpPost, Route("GetPartProblemData")]
        public IHttpActionResult GetPartProblemData([FromBody]CED072300AParam Param)
        {
            CommonRepository common = new CommonRepository();
            try
            {
                int totalCase = 0;
                string sql = "SP_CED072300A_GetPartProblemDataHeader '" + Param.PartNo + "', '" + Param.ProblemName + "', '" + Param.Mode + "'";
                var o = _context.Database.SqlQuery<CED072300AHeader>(sql).ToList<CED072300AHeader>();
                List<CED072300AHeader> dataHeader = new List<CED072300AHeader>();
                foreach (CED072300AHeader header in o)
                {
                    string sqlDet = "SP_CED072300A_GetPartProblemDataDetail '" + header.PartNo + "'";
                    var oDet = _context.Database.SqlQuery<CED072300ADetail>(sqlDet).ToList<CED072300ADetail>();
                    header.ProblemDetail = oDet;
                    header.TotalCase = oDet.Count();
                    dataHeader.Add(header);            
                }
            
                CED072300AResponse data = new CED072300AResponse();
                data.Data = dataHeader;
                if (o.Count > 0)
                {
                    data.Result = "Success";
                    data.Message = "MCEDSTD013I : Data retrieved successfully";
                }
                else
                {
                    data.Result = "Failed";
                    data.Message = "MCEDSTD030I : No data found";
                }
            
                return Ok<CED072300AResponse>(data);
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [HttpPost, Route("SetHoldKanban")]
        public IHttpActionResult SetHoldKanban(CED072300AParamSetHoldKanban Param)
        {

            CommonRepository common = new CommonRepository();
            try
            {

                int paramCounter = 0;
                string PartNo = string.Empty;

                foreach (var item in Param.dataDetail)
                {
                    paramCounter++;
                    PartNo += item.PartNo + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                }

                string sql = "SP_CED072300A_SetHoldKanban '" + PartNo + "', '" + Param.Area + "', '" + Param.UserName + "'";
                var o = _context.Database.SqlQuery<CED072300AResult>(sql).ToList<CED072300AResult>();
                CED072300AResult rst = new CED072300AResult();
                rst.Message = o[0].Message;
                rst.Result = o[0].Result;
                return Ok<CED072300AResult>(rst);
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [HttpPost, Route("SetReleaseKanban")]
        public IHttpActionResult SetReleaseKanban([FromBody]CED072300AParamSetHoldKanban Param)
        {
            CommonRepository common = new CommonRepository();
            try
            {

                int paramCounter = 0;
                string PartNo = string.Empty;
                string Area = string.Empty;
                string UserName = string.Empty;

                foreach (var item in Param.dataDetail)
                {
                    paramCounter++;
                    PartNo += item.PartNo + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                }

                string sql = "SP_CED072300A_SetReleaseKanban '" + PartNo + "', '" + Param.Area + "', '" + Param.UserName + "'";
                var o = _context.Database.SqlQuery<CED072300AResult>(sql).ToList<CED072300AResult>();
                CED072300AResult rst = new CED072300AResult();
                rst.Message = o[0].Message;
                rst.Result = o[0].Result;
                return Ok<CED072300AResult>(rst);
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
                
        }
    }
}
