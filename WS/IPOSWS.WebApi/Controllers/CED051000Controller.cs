﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED051000;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED051000Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetSequenceDataResponse))]
        [HttpPost, Route("GetSequenceData")]
        public async Task<IHttpActionResult> GetSequenceData(GetSequenceDataRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetSequenceDataResponse resp = new GetSequenceDataResponse();
                
                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };
                    var ProdDate = new SqlParameter
                    {
                        ParameterName = "ProdDate",
                        Value = parameter.ProdDate
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = Shift;
                    p[2] = ProdDate;

                    var o = _context.Database.SqlQuery<GetSequenceDataResponse>("exec [CED051000B_GetSequenceData] @UserName,@Shift,@ProdDate", p).ToList<GetSequenceDataResponse>();

                    return Ok<GetSequenceDataResponse>(o.FirstOrDefault());
                });
            } 
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        [ResponseType(typeof(PostSupplyStartJundateStatusResponse))]
        [HttpPost, Route("PostSupplyStartJundateStatus")]
        public async Task<IHttpActionResult> PostSupplyStartJundateStatus(PostSupplyStartJundateStatusRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var Seq = new SqlParameter
                    {
                        ParameterName = "Seq",
                        Value = parameter.Seq
                    };

                    var Module = new SqlParameter
                    {
                        ParameterName = "Module",
                        Value = parameter.Module
                    };

                    var Case = new SqlParameter
                    {
                        ParameterName = "Case",
                        Value = parameter.Case
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.Line
                    };

                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };

                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var ChangedBy = new SqlParameter
                    {
                        ParameterName = "ChangedBy",
                        Value = parameter.ChangedBy
                    };

                    var ChangedDt = new SqlParameter
                    {
                        ParameterName = "ChangedDt",
                        Value = parameter.ChangedDt
                    };


                    SqlParameter[] p = new SqlParameter[9];
                    p[0] = Seq;
                    p[1] = Module;
                    p[2] = Case;
                    p[3] = Line;
                    p[4] = UserName;
                    p[5] = Shift;
                    p[6] = ControlModuleNo;
                    p[7] = ChangedBy;
                    p[8] = ChangedDt;
                    //var o = _context.Database.SqlQuery<PostSupplyStartJundateStatusResponse>("exec [CED051000B_PostSupplyStartJundateStatus] @Seq,@Module,@Case,@Line,@UserName,@Shift", p).ToList<PostSupplyStartJundateStatusResponse>();
                    var o = _context.Database.SqlQuery<PostSupplyStartJundateStatusResponse>("exec [CED051000B_PostSupplyStartJundateStatus] @Seq,@Module,@Case,@Line,@UserName,@Shift,@ControlModuleNo,@ChangedBy,@ChangedDt", p).ToList<PostSupplyStartJundateStatusResponse>();
                    return Ok<PostSupplyStartJundateStatusResponse>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(PostSupplyEndStatusResponse))]
        [HttpPost, Route("PostSupplyEndStatus")]
        public async Task<IHttpActionResult> PostSupplyEndStatus(PostSupplyEndStatusRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var Seq = new SqlParameter
                    {
                        ParameterName = "Seq",
                        Value = parameter.Seq
                    };

                    var Module = new SqlParameter
                    {
                        ParameterName = "Module",
                        Value = parameter.Module
                    };

                    var Case = new SqlParameter
                    {
                        ParameterName = "Case",
                        Value = parameter.Case
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.Line
                    };

                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };

                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var ChangedBy = new SqlParameter
                    {
                        ParameterName = "ChangedBy",
                        Value = parameter.ChangedBy
                    };

                    var ChangedDt = new SqlParameter
                    {
                        ParameterName = "ChangedDt",
                        Value = parameter.ChangedDt
                    };
                    
                    SqlParameter[] p = new SqlParameter[9];
                    p[0] = Seq;
                    p[1] = Module;
                    p[2] = Case;
                    p[3] = Line;
                    p[4] = UserName;
                    p[5] = Shift;
                    p[6] = ControlModuleNo;
                    p[7] = ChangedBy;
                    p[8] = ChangedDt;

                    //var o = _context.Database.SqlQuery<PostSupplyEndStatusResponse>("exec [CED051000B_PostSupplyEndStatus] @Seq,@Module,@Case,@Line,@UserName,@Shift", p).ToList<PostSupplyEndStatusResponse>();
                    var o = _context.Database.SqlQuery<PostSupplyEndStatusResponse>("exec [CED051000B_PostSupplyEndStatus] @Seq,@Module,@Case,@Line,@UserName,@Shift,@ControlModuleNo,@ChangedBy,@ChangedDt", p).ToList<PostSupplyEndStatusResponse>();
                    return Ok<PostSupplyEndStatusResponse>(o.FirstOrDefault());
                });
            } 
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        //NEW API [WOT]
        [AllowAnonymous]
        [ResponseType(typeof(GetSequenceListApiResponse))]
        [HttpPost, Route("GetSequenceListApi")]
        public async Task<IHttpActionResult> GetSequenceListApi(GetSequenceListApiRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetSequenceListApiResponse respto = new GetSequenceListApiResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };
                    var ProdDate = new SqlParameter
                    {
                        ParameterName = "ProdDate",
                        Value = parameter.ProdDate
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = Shift;
                    p[2] = ProdDate;

                    var o = _context.Database.SqlQuery<GetSequenceListApiData>("exec [CED051000B_GetSequenceListApi] @UserName,@Shift,@ProdDate", p).ToList<GetSequenceListApiData>();

                    respto = common.CED051000BGetSequenceListApi(o);

                    return Ok<GetSequenceListApiResponse>(respto);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }
        // LAST CODE

    }
}
