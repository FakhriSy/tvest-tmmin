﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace IPOSWS.WebApi.Controllers
{
    public class UploadImageController : ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [AllowAnonymous]
        [HttpPost, Route("uploadImageAPI")]
        public async Task<IHttpActionResult> uploadImageAPI([FromBody] ImageModel dataParam)
        {
            return await Task.Run(() =>
            {
                byte[] f = Convert.FromBase64String(dataParam.imgFile);
                string fileName = dataParam.fileName;
                var destinationSource = System.Web.Hosting.HostingEnvironment.MapPath("~/TempImage/");
                //Image Ori
                MemoryStream ms = new MemoryStream(f);
                FileStream fs = new FileStream(destinationSource + fileName, FileMode.Create);
                ms.WriteTo(fs);
                ms.Close();
                fs.Close();
                fs.Dispose();
                //Image Thumb
                Image img = byteArrayToImage(f);
                byte[] dataThumb = MakeThumbnail(f, img.Width / 2, img.Height / 2);
                MemoryStream msThumb = new MemoryStream(dataThumb);
                FileStream fsThumb = new FileStream(destinationSource + "thumb_" + fileName, FileMode.Create);
                msThumb.WriteTo(fsThumb);
                msThumb.Close();
                fsThumb.Close();
                fsThumb.Dispose();

                string ftp_server = "ftp://10.165.8.80/IMAGE";
                string ftp_user = "ipoms";
                string ftp_pass = "1234";
                ftp ftpClient = new ftp(ftp_server, ftp_user, ftp_pass);
                ftpClient.upload(fileName, destinationSource + fileName);
                ftpClient.upload("thumb_" + fileName, destinationSource + "thumb_" + fileName);

                string sql = "UPDATE TB_R_PROBLEM_H SET PROBLEM_IMAGE_1='" + fileName + "', PROBLEM_THUMBNAIL_1='thumb_'" + fileName + "";
                var o = _context.Database.ExecuteSqlCommand(sql);

                return Ok(new
                {
                    Result = "OK"
                });
            });
        }

        [AllowAnonymous]
        [HttpGet, Route("GetProblemImage")]
        public IHttpActionResult GetProblemImage()
        {
            string sql = "SELECT PROBLEM_ID ProblemId, 'http://10.165.8.80:8099/' + PROBLEM_IMAGE_1 ProblemImage1, 'http://10.165.8.80:8099/Thumb_' + PROBLEM_IMAGE_1 Thumb_ProblemImage1 FROM TB_R_PROBLEM_H";
            var o = _context.Database.SqlQuery<ImageModelGet>(sql).ToList<ImageModelGet>();
            return Ok<List<ImageModelGet>>(o);
        }
        
        private static byte[] MakeThumbnail(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            var jpegQuality = 50;
            Image image;
            using (var inputStream = new MemoryStream(myImage))
            {
                image = Image.FromStream(new MemoryStream(myImage)).GetThumbnailImage(thumbWidth, thumbHeight, null, new IntPtr());
                var jpegEncoder = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, jpegQuality);
                Byte[] outputBytes;
                using (var outputStream = new MemoryStream())
                {
                    image.Save(outputStream, jpegEncoder, encoderParameters);
                    return outputBytes = outputStream.ToArray();
                }
            }
        }

        private static Image byteArrayToImage(byte[] bytesArr)
        {
            MemoryStream memstr = new MemoryStream(bytesArr);
            Image img = Image.FromStream(memstr);
            return img;
        }
    }

    public class ImageModelGet
    {
        public string ProblemId { get; set; }
        public string ProblemImage1 { get; set; }
        public string Thumb_ProblemImage1 { get; set; }
    }

    public class ImageModel {
        public string fileName { get; set; }
        public string imgFile { get; set; }
    }

    public class ImageResult
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }
}
