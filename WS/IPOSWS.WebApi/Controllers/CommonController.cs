﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.Common;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;


namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CommonController : ApiController //CorsController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetVersionResponse))]
        [HttpGet, Route("GetVersion")]
        public async Task<IHttpActionResult> GetVersion(String Type)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetVersionResponse resp = new GetVersionResponse();

                return await Task.Run(() =>
                {
                    
                    var pType = new SqlParameter
                    {
                        ParameterName = "Type",
                        Value = Type
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = pType;
                    
                    var o = _context.Database.SqlQuery<GetVersionResponse>("exec [COMMON_GetVersion] @Type",p).ToList<GetVersionResponse>();

                    return Ok<GetVersionResponse>(common.CommonGetVersion(o.FirstOrDefault()));
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


    }
}
