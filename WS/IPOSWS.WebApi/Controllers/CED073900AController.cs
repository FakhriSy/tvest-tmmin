﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED073900A;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Drawing;
using System.Drawing.Imaging;
using System.IO;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;
using System.Data.SqlClient;
using System.Threading.Tasks;

namespace IPOSWS.WebApi.Controllers
{
    
    
    [RoutePrefix("api/v1")]
    public class CED073900AController : CorsController //ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(CED073900AGetRespons))]
        [HttpPost, Route("GetProblemData")]
        public IHttpActionResult GetProblemData([FromBody]CED073900AParam Param)
        {

            CommonRepository common = new CommonRepository();

            try
            {              
                string sql = "CED073900A_GetProblemData '" + Param.UserName + "', '" + Param.PartNo + "', '" + Param.Supplier + "','" + Param.KanbanId + "'   ";
                var o = _context.Database.SqlQuery<CED073900AGetData>(sql).ToList<CED073900AGetData>();

                CED073900AGetRespons data = new CED073900AGetRespons();

                if (o.Count > 0)
                {
                    data.TotalCase = o.Count.ToString();
                    data.TotalPcs = o.Sum(x => x.KanbanQty).ToString();
                    data.Data = o;
                    data.Result = "Success";
                    data.Message = "MCEDSTD013I : Data retrieved successfully";
                }
                else
                {
                    data.TotalCase = "0";
                    data.TotalPcs = "0";
                    data.Result = "Failed";
                    data.Message = "MCEDSTD030I : No Data Found";
                }

                return Ok<CED073900AGetRespons>(data);
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(CED073900AGetDefectResponse))]
        [HttpGet, Route("GetDefectData")]
        public async Task<IHttpActionResult> GetDefect()
        {
            CommonRepository common = new CommonRepository();

            try
            {
                CED073900AGetDefectResponse resp = new CED073900AGetDefectResponse();

                return await Task.Run(() =>
                {

                    var o = _context.Database.SqlQuery<CED073900ADefectResponse>("exec [CED073900A_GetDefectData]").ToList<CED073900ADefectResponse>();

                    resp = common.CED073900GetDefect(o);

                    return Ok<CED073900AGetDefectResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        //[ResponseType(typeof(CED073900AResponse))]
        //[HttpPost, Route("PostNewProblem")]
        //public IHttpActionResult InputProblem([FromBody]CED073900A Param)

        [ResponseType(typeof(CED073900AResponse))]
        [HttpPost, Route("PostNewProblem")]
        public async Task<IHttpActionResult> InputProblem(CED073900A Param)        
        {
            CommonRepository common = new CommonRepository();
            try
            {

                string destinationSource = common.GetSystemMasterValue("CED040800W", "IMG_PATH", "FOLDER");
                string folderYear = DateTime.Now.ToString("yyyy");
                string folderMonth = DateTime.Now.ToString("MM");
                destinationSource = destinationSource + folderYear + "/" + folderMonth + "/";
                string pathTemp = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadTemp/ImgProblem");


                var imgProblem1 = "";
                var imgProblem2 = "";
                var imgProblemThumb1 = "";
                var imgProblemThumb2 = "";

                int paramCounter = 0;
                string kanbanId = string.Empty;
                string KanbanQty = string.Empty;
                string LastPosition = string.Empty;
                string Issuer = string.Empty;
                string ManifestNo = string.Empty;
                if (Param.ProblemImage1 != "")
                {
                    if (!Directory.Exists(destinationSource))
                    {
                        Directory.CreateDirectory(destinationSource);
                    }

                    byte[] f = Convert.FromBase64String(Param.ProblemImage1);
                    string fileName = Param.KanbanId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg";


                    if (System.IO.File.Exists(destinationSource + fileName))
                    {
                        System.IO.File.Delete(destinationSource + fileName);
                    }

                    //Image Ori
                    MemoryStream ms = new MemoryStream(f);
                    FileStream fs = new FileStream(destinationSource + fileName, FileMode.Create);
                    ms.WriteTo(fs);
                    ms.Close();
                    fs.Close();
                    fs.Dispose();
                    //Image Thumb
                    Image img = byteArrayToImage(f);
                    byte[] dataThumb = MakeThumbnail(f, img.Width / 2, img.Height / 2);
                    MemoryStream msThumb = new MemoryStream(dataThumb);
                    FileStream fsThumb = new FileStream(destinationSource + "thumb_" + fileName, FileMode.Create);
                    msThumb.WriteTo(fsThumb);
                    msThumb.Close();
                    fsThumb.Close();
                    fsThumb.Dispose();

                    imgProblem1 = folderYear + "/" + folderMonth + "/" + fileName;
                    byte[] data = System.IO.File.ReadAllBytes(destinationSource + "thumb_" + fileName);
                    imgProblemThumb1 = Convert.ToBase64String(data);
                }
                if (Param.ProblemImage2 != "")
                {
                    if (!Directory.Exists(destinationSource))
                    {
                        Directory.CreateDirectory(destinationSource);
                    }

                    byte[] f = Convert.FromBase64String(Param.ProblemImage2);
                    string fileName = Param.KanbanId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg";


                    if (System.IO.File.Exists(destinationSource + fileName))
                    {
                        System.IO.File.Delete(destinationSource + fileName);
                    }

                    //Image Ori
                    MemoryStream ms = new MemoryStream(f);
                    FileStream fs = new FileStream(destinationSource + fileName, FileMode.Create);
                    ms.WriteTo(fs);
                    ms.Close();
                    fs.Close();
                    fs.Dispose();
                    //Image Thumb
                    Image img = byteArrayToImage(f);
                    byte[] dataThumb = MakeThumbnail(f, img.Width / 2, img.Height / 2);
                    MemoryStream msThumb = new MemoryStream(dataThumb);
                    FileStream fsThumb = new FileStream(destinationSource + "thumb_" + fileName, FileMode.Create);
                    msThumb.WriteTo(fsThumb);
                    msThumb.Close();
                    fsThumb.Close();
                    fsThumb.Dispose();

                    imgProblem2 = folderYear + "/" + folderMonth + "/" + fileName;
                    byte[] data = System.IO.File.ReadAllBytes(destinationSource + "thumb_" + fileName);
                    imgProblemThumb2 = Convert.ToBase64String(data);
                }


                foreach (var item in Param.dataDetail){
                        paramCounter++;
                        kanbanId += item.KanbanId + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                        KanbanQty += item.KanbanQty + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                        LastPosition += item.LastPosition + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                        Issuer += item.Issuer + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                        ManifestNo += item.ManifestNo + (paramCounter < Param.dataDetail.Count() ? "|" : "");
                }


                return await Task.Run(() =>
                {
                    var pProblemId = new SqlParameter {
	                    ParameterName = "ProblemId",
	                    Value = Param.ProblemId
                    };

                    var pKanbanIdHeader = new SqlParameter {
	                    ParameterName = "KanbanIdHeader",
                        Value = Param.KanbanId
                    };
                    
                    var pKanbanId = new SqlParameter {
	                    ParameterName = "KanbanId",
                        Value = kanbanId
                    };
                    
                    var pMainDefect = new SqlParameter {
	                    ParameterName = "MainDefect",
	                    Value = Param.MainDefect
                    };
                    
                    var pSubDefect = new SqlParameter {
	                    ParameterName = "SubDefect",
	                    Value = Param.SubDefect
                    };
                    
                    var pPartNo = new SqlParameter {
	                    ParameterName = "PartNo",
	                    Value = Param.PartNo
                    };
                    
                    var pUniqueNo = new SqlParameter {
	                    ParameterName = "UniqueNo",
	                    Value = Param.UniqueNo
                    };
                    
                    var pSupplierCode = new SqlParameter {
	                    ParameterName = "SupplierCode",
	                    Value = Param.SupplierCode
                    };
                    
                    var pSupplierPlantCode = new SqlParameter {
	                    ParameterName = "SupplierPlantCode",
	                    Value = Param.SupplierPlantCode
                    };
                    
                    var pProblemImage1 = new SqlParameter {
	                    ParameterName = "ProblemImage1",
	                    Value = imgProblem1
                    };
                    
                    var pProblemImage2 = new SqlParameter {
	                    ParameterName = "ProblemImage2",
	                    Value = imgProblem2
                    };
                    
                    var pProblemImageThumb1 = new SqlParameter {
	                    ParameterName = "ProblemImageThumb1",
	                    Value = imgProblemThumb1
                    };
                    
                    var pProblemImageThumb2 = new SqlParameter {
	                    ParameterName = "ProblemImageThumb2",
	                    Value = imgProblemThumb2
                    };
                    
                    var pUsername = new SqlParameter {
	                    ParameterName = "Username",
	                    Value = Param.Username
                    };
                    
                    var ppackingLine = new SqlParameter {
	                    ParameterName = "packingLine",
	                    Value = Param.PackingLine
                    };
                    
                    var pKanbanQty = new SqlParameter {
	                    ParameterName = "KanbanQty",
	                    Value = KanbanQty
                    };
                    
                    var pLastPosition = new SqlParameter {
	                    ParameterName = "LastPosition",
	                    Value = LastPosition
                    };
                    
                    var pIssuer = new SqlParameter {
	                    ParameterName = "Issuer",
	                    Value = Issuer
                    };
                    
                    var pManifestNo = new SqlParameter
                    {
                        ParameterName = "ManifestNo",
                        Value = ManifestNo
                    };

                    SqlParameter[] p = new SqlParameter[19];
                    p[0] = pProblemId;
                    p[1] = pKanbanIdHeader;
                    p[2] = pKanbanId;
                    p[3] = pMainDefect;
                    p[4] = pSubDefect;
                    p[5] = pPartNo;
                    p[6] = pUniqueNo;
                    p[7] = pSupplierCode;
                    p[8] = pSupplierPlantCode;
                    p[9] = pProblemImage1;
                    p[10] = pProblemImage2;
                    p[11] = pProblemImageThumb1;
                    p[12] = pProblemImageThumb2;
                    p[13] = pUsername;
                    p[14] = ppackingLine;
                    p[15] = pKanbanQty;
                    p[16] = pLastPosition;
                    p[17] = pIssuer;
                    p[18] = pManifestNo;

                    var o = _context.Database.SqlQuery<CED073900AResponse>("exec [CED073900A_PostNewProblem] @ProblemId,@KanbanIdHeader,@KanbanId,@MainDefect,@SubDefect,@PartNo,@UniqueNo,@SupplierCode,@SupplierPlantCode,@ProblemImage1,@ProblemImage2,@ProblemImageThumb1,@ProblemImageThumb2,@Username,@packingLine,@KanbanQty,@LastPosition,@Issuer,@ManifestNo", p).ToList<CED073900AResponse>();

                    CED073900AResponse dataResult = new CED073900AResponse();
                    dataResult.ProblemId = o.FirstOrDefault().ProblemId;
                    dataResult.Result = o.FirstOrDefault().Result;
                    dataResult.Message = o.FirstOrDefault().Message;

                    return Ok<CED073900AResponse>(dataResult);
                    
                });

            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }




        //[ResponseType(typeof(CED073900AResponse))]
        //[HttpPost, Route("PostNewProblem")]
        //public IHttpActionResult InputProblem([FromBody]CED073900A Param)
        //{
        //    CommonRepository common = new CommonRepository();
        //    string destinationSource = common.GetSystemMasterValue("CED040800W", "IMG_PATH", "FOLDER");
        //    string folderYear = DateTime.Now.ToString("yyyy");
        //    string folderMonth = DateTime.Now.ToString("MM");
        //    destinationSource = destinationSource + folderYear + "/" + folderMonth + "/";
        //    string pathTemp = System.Web.Hosting.HostingEnvironment.MapPath("~/UploadTemp/ImgProblem");


        //    var imgProblem1 = "";
        //    var imgProblem2 = "";
        //    var imgProblemThumb1 = "";
        //    var imgProblemThumb2 = "";

        //    int paramCounter    = 0;
        //    string kanbanId     = string.Empty;
        //    string KanbanQty    = string.Empty;
        //    string LastPosition = string.Empty;
        //    string Issuer       = string.Empty;
        //    string ManifestNo   = string.Empty;
        //    if (Param.ProblemImage1 != "")
        //    {
        //        if (!Directory.Exists(destinationSource))
        //        {
        //            Directory.CreateDirectory(destinationSource);
        //        }

        //        byte[] f = Convert.FromBase64String(Param.ProblemImage1);
        //        string fileName = Param.KanbanId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg";


        //        if (System.IO.File.Exists(destinationSource + fileName))
        //        {
        //            System.IO.File.Delete(destinationSource + fileName);
        //        }
                
        //        //Image Ori
        //        MemoryStream ms = new MemoryStream(f);
        //        FileStream fs = new FileStream(destinationSource + fileName, FileMode.Create);
        //        ms.WriteTo(fs);
        //        ms.Close();
        //        fs.Close();
        //        fs.Dispose();
        //        //Image Thumb
        //        Image img = byteArrayToImage(f);
        //        byte[] dataThumb = MakeThumbnail(f, img.Width / 2, img.Height / 2);
        //        MemoryStream msThumb = new MemoryStream(dataThumb);
        //        FileStream fsThumb = new FileStream(destinationSource + "thumb_" + fileName, FileMode.Create);
        //        msThumb.WriteTo(fsThumb);
        //        msThumb.Close();
        //        fsThumb.Close();
        //        fsThumb.Dispose();

        //        imgProblem1 = folderYear + "/" + folderMonth + "/"+fileName;
        //        byte[] data = System.IO.File.ReadAllBytes(destinationSource + "thumb_" + fileName);
        //        imgProblemThumb1 = Convert.ToBase64String(data);
        //        //ftpClient.upload(fileName, destinationSource + fileName);
        //    }
        //    if (Param.ProblemImage2 != "")
        //    {
        //        if (!Directory.Exists(destinationSource))
        //        {
        //            Directory.CreateDirectory(destinationSource);
        //        }

        //        byte[] f = Convert.FromBase64String(Param.ProblemImage2);
        //        string fileName = Param.KanbanId + "_" + DateTime.Now.ToString("yyyyMMddHHmmssfff") + ".jpg";


        //        if (System.IO.File.Exists(destinationSource + fileName))
        //        {
        //            System.IO.File.Delete(destinationSource + fileName);
        //        }
                
        //        //Image Ori
        //        MemoryStream ms = new MemoryStream(f);
        //        FileStream fs = new FileStream(destinationSource + fileName, FileMode.Create);
        //        ms.WriteTo(fs);
        //        ms.Close();
        //        fs.Close();
        //        fs.Dispose();
        //        //Image Thumb
        //        Image img = byteArrayToImage(f);
        //        byte[] dataThumb = MakeThumbnail(f, img.Width / 2, img.Height / 2);
        //        MemoryStream msThumb = new MemoryStream(dataThumb);
        //        FileStream fsThumb = new FileStream(destinationSource + "thumb_" + fileName, FileMode.Create);
        //        msThumb.WriteTo(fsThumb);
        //        msThumb.Close();
        //        fsThumb.Close();
        //        fsThumb.Dispose();

        //        imgProblem2 = folderYear + "/" + folderMonth + "/" + fileName;
        //        byte[] data = System.IO.File.ReadAllBytes(destinationSource + "thumb_" + fileName);
        //        imgProblemThumb2 = Convert.ToBase64String(data);
        //        //ftpClient.upload(fileName, destinationSource + fileName);
        //    }

        //    string sql = "CED073900A_PostNewProblemHeader '" + Param.ProblemId + "','" + Param.KanbanId + "', '" + Param.MainDefect + "', '" + Param.SubDefect + "', '" + Param.PartNo + "', '" + Param.UniqueNo + "', '" + Param.SupplierCode + "', '" + Param.SupplierPlantCode + "', '" + imgProblem1 + "', '" + imgProblem2 + "', '" + imgProblemThumb1 + "', '" + imgProblemThumb2 + "', '" + Param.Username + "', '" + Param.PackingLine + "'";
        //    var o = _context.Database.SqlQuery<CED073900AResponse>(sql).ToList<CED073900AResponse>();
        //    var problemId = o.FirstOrDefault().ProblemId;

        //    var result = "";
        //    var msg = "";

        //    if (o[0].ProblemId != null)
        //        foreach (var item in Param.dataDetail)
        //        {
        //            paramCounter++;
        //            kanbanId += item.KanbanId + (paramCounter < Param.dataDetail.Count() ? "|" : "");
        //            KanbanQty += item.KanbanQty + (paramCounter < Param.dataDetail.Count() ? "|" : "");
        //            LastPosition += item.LastPosition + (paramCounter < Param.dataDetail.Count() ? "|" : "");
        //            Issuer += item.Issuer + (paramCounter < Param.dataDetail.Count() ? "|" : "");
        //            ManifestNo += item.ManifestNo + (paramCounter < Param.dataDetail.Count() ? "|" : "");
        //        }


        //    string sqlDet = "CED073900A_PostNewProblemDetail '" + problemId + "', '" + kanbanId + "', '" + KanbanQty + "', '" + LastPosition + "', '" + Issuer + "', '" + ManifestNo + "', '" + Param.Username + "'";
        //    var oDet = _context.Database.SqlQuery<CED073900AResponse>(sqlDet).ToList<CED073900AResponse>();            
            
        //    //foreach (CED073900ADetail paramDetail in Param.dataDetail)
        //        //{
        //        //    string sqlDet = "CED073900A_PostNewProblemDetail '" + problemId + "', '" + paramDetail.KanbanId + "', '" + paramDetail.KanbanQty + "', '" + paramDetail.LastPosition + "', '" + paramDetail.Issuer + "'";
        //        //    var oDet = _context.Database.SqlQuery<CED073900AResponse>(sqlDet).ToList<CED073900AResponse>();

        //        //    result = oDet[0].Result;
        //        //    msg = oDet[0].Message;
        //        //}

        //    CED073900AResponse dataResult = new CED073900AResponse();
        //    dataResult.ProblemId = problemId;
        //    dataResult.Result = o.FirstOrDefault().Result;
        //    dataResult.Message = o.FirstOrDefault().Message;

        //    return Ok<CED073900AResponse>(dataResult);
        //}

        private static byte[] MakeThumbnail(byte[] myImage, int thumbWidth, int thumbHeight)
        {
            var jpegQuality = 50;
            Image image;
            using (var inputStream = new MemoryStream(myImage))
            {
                image = Image.FromStream(new MemoryStream(myImage)).GetThumbnailImage(thumbWidth, thumbHeight, null, new IntPtr());
                var jpegEncoder = ImageCodecInfo.GetImageDecoders().First(c => c.FormatID == ImageFormat.Jpeg.Guid);
                var encoderParameters = new EncoderParameters(1);
                encoderParameters.Param[0] = new EncoderParameter(Encoder.Quality, jpegQuality);
                Byte[] outputBytes;
                using (var outputStream = new MemoryStream())
                {
                    image.Save(outputStream, jpegEncoder, encoderParameters);
                    return outputBytes = outputStream.ToArray();
                }
            }
        }

        private static Image byteArrayToImage(byte[] bytesArr)
        {
            MemoryStream memstr = new MemoryStream(bytesArr);
            Image img = Image.FromStream(memstr);
            return img;
        }
    }
}
