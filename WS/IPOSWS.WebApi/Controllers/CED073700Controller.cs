﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED073700;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED073700Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetCrippleList))]
        [HttpPost, Route("GetCrippleList")]
        public async Task<IHttpActionResult> GetCrippleList(GetCrippleListRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetCrippleList resp = new GetCrippleList();

                return await Task.Run(() =>
                {
                    var ProdDate = new SqlParameter
                    {
                        ParameterName = "ProdDate",
                        Value = parameter.ProdDate == null ? "" : parameter.ProdDate
                    };
                    var VanningDate = new SqlParameter
                    {
                        ParameterName = "VanningDate",
                        Value = parameter.VanningDate == null ? "" :parameter.VanningDate
                    };
                    var PackingLine = new SqlParameter
                    {
                        ParameterName = "PackingLine",
                        Value = parameter.PackingLine == null ? "" : parameter.PackingLine
                    };
                    
                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = ProdDate;
                    p[1] = VanningDate;
                    p[2] = PackingLine;

                    var o = _context.Database.SqlQuery<GetCrippleListResponse>("exec [CED073700B_GetCrippleList] @ProdDate,@VanningDate,@PackingLine", p).ToList<GetCrippleListResponse>();

                    resp = common.CED073700BGetCrippleListResponse(o);

                    return Ok<GetCrippleList>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
                //return BadRequest(ae.Message);
            }
            
        }


        [ResponseType(typeof(GetPackingLine))]
        [HttpPost, Route("GetPackingLine")]
        public async Task<IHttpActionResult> GetPackingLine()
        {
            CommonRepository common = new CommonRepository();
            try
            {
                GetPackingLine resp = new GetPackingLine();
             
                return await Task.Run(() =>
                {
                    var o = _context.Database.SqlQuery<GetPackingLineResponse>("exec [CED073700B_GetPackingLine]").ToList<GetPackingLineResponse>();

                    if(o.Count > 0){
                        
                        resp.Data = o;
                        resp.Result = "Success";
                        resp.Message = "MCEDSTD013I : Data retrieved successfully";
                    
                    }else{

                        resp.Data = o;
                        resp.Result = "Failed";
                        resp.Message = "MCEDSTD030I : No data found";
                    
                    }
                    
                    
                    return Ok<GetPackingLine>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
                //return BadRequest(ae.Message);
            }

        }

        [ResponseType(typeof(GetActionResponse))]
        [HttpGet, Route("GetAction")]
        public async Task<IHttpActionResult> GetAction()
        {
            CommonRepository common = new CommonRepository();
            try
            {
                GetActionResponse resp = new GetActionResponse();


                return await Task.Run(() =>
                {
                    var o = _context.Database.SqlQuery<GetAction>("exec [CED073700B_GetAction]").ToList<GetAction>();

                    resp = common.CED073700BGetActionResponse(o);

                    return Ok<GetActionResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
                //return BadRequest(ae.Message);
            }
        }

        [ResponseType(typeof(PostCompleteResponse))]
        [HttpPost, Route("PostComplete")]
        public async Task<IHttpActionResult> PostComplete(PostCompleteRequest parameter)
        {
            CommonRepository common = new CommonRepository();
            try
            {
                return await Task.Run(() =>
                {
                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = parameter.KanbanID
                    };

                    var TransactionType = new SqlParameter
                    {
                        ParameterName = "TransactionType",
                        Value = parameter.TransactionType
                    };

                    var CrippleProblem = new SqlParameter
                    {
                        ParameterName = "CrippleProblem",
                        Value = parameter.CrippleProblem
                    };

                    var CrippleAction = new SqlParameter
                    {
                        ParameterName = "CrippleAction",
                        Value = parameter.CrippleAction
                    };

                    var CrippleEONo = new SqlParameter
                    {
                        ParameterName = "CrippleEONo",
                        Value = parameter.CrippleEONo
                    };

                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var ChangedBy = new SqlParameter
                    {
                        ParameterName = "ChangedBy",
                        Value = parameter.ChangedBy
                    };

                    var ChangedDt = new SqlParameter
                    {
                        ParameterName = "ChangedDt",
                        Value = parameter.ChangedDt
                    };
 

                    SqlParameter[] p = new SqlParameter[9];
                    p[0] = ControlModuleNo;
                    p[1] = KanbanID;
                    p[2] = TransactionType;
                    p[3] = CrippleProblem;
                    p[4] = CrippleAction;
                    p[5] = CrippleEONo;
                    p[6] = UserName;
                    p[7] = ChangedBy;
                    p[8] = ChangedDt;

                    var o = _context.Database.SqlQuery<PostCompleteResponse>("exec [CED073700B_PostComplete] @ControlModuleNo,@KanbanID,@TransactionType,@CrippleProblem,@CrippleAction,@CrippleEONo,@UserName,@ChangedBy,@ChangedDt", p).ToList<PostCompleteResponse>();

                    return Ok<PostCompleteResponse>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
                //return BadRequest(ae.Message);
            }
        }

    }
}
