﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED098;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED098Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetChartProblemStatusResponse))]
        [HttpPost, Route("GetChartProblemStatusResponse")]
        public async Task<IHttpActionResult> GetChartProblemStatusResponse(GetChartProblemStatusRequest parameter)
        {
            GetChartProblemStatusResponse resp = new GetChartProblemStatusResponse();
            CommonRepository commonRepository = new CommonRepository();
            try
            {

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.Username
                    };
                    var CodeReport = new SqlParameter
                    {
                        ParameterName = "CodeReport",
                        Value = parameter.CodeReport
                    };



                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = CodeReport;

                    var o = _context.Database.SqlQuery<CED098Data>("exec [CED098_GetDataChart] @UserName,@CodeReport", p).ToList<CED098Data>();
                    resp = commonRepository.getChartProblemStatus(o);
                    return Ok<GetChartProblemStatusResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(commonRepository.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetChartProblemCategoryReponse))]
        [HttpPost, Route("GetChartProblemCategory")]
        public async Task<IHttpActionResult> GetChartProblemCategory(GetChartProblemStatusRequest parameter)
        {
            GetChartProblemCategoryReponse resp = new GetChartProblemCategoryReponse();
            CommonRepository commonRepository = new CommonRepository();
            try
            {

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.Username
                    };
                    var CodeReport = new SqlParameter
                    {
                        ParameterName = "CodeReport",
                        Value = parameter.CodeReport
                    };



                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = CodeReport;

                    var o = _context.Database.SqlQuery<CED098DataCat>("exec [CED098_GetDataChart] @UserName,@CodeReport", p).ToList<CED098DataCat>();
                    resp = commonRepository.getChartProblemCategory(o);
                    return Ok<GetChartProblemCategoryReponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(commonRepository.OtputErrorMessage(ae.Message));
            }

        }


        //problemFlag
        [ResponseType(typeof(GetChartProblemFlagResponse))]
        [HttpPost, Route("GetChartProblemFlag")]
        public async Task<IHttpActionResult> GetChartProblemFlag(GetChartProblemStatusRequest parameter)
        {
            GetChartProblemFlagResponse resp = new GetChartProblemFlagResponse();
            CommonRepository commonRepository = new CommonRepository();
            try
            {

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.Username
                    };
                    var CodeReport = new SqlParameter
                    {
                        ParameterName = "CodeReport",
                        Value = parameter.CodeReport
                    };



                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = CodeReport;

                    var o = _context.Database.SqlQuery<CED098DataFlag>("exec [CED098_GetDataChart] @UserName,@CodeReport", p).ToList<CED098DataFlag>();
                    resp = commonRepository.getChartProblemFlag(o);
                    return Ok<GetChartProblemFlagResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(commonRepository.OtputErrorMessage(ae.Message));
            }

        }


        //problemPartLine


        [ResponseType(typeof(GetChartLineProblemResponse))]
        [HttpPost, Route("GetChartProblemPartLine")]
        public async Task<IHttpActionResult> GetChartProblemPartLine(GetChartProblemStatusRequest parameter)
        {
            GetChartLineProblemResponse resp = new GetChartLineProblemResponse();
            CommonRepository commonRepository = new CommonRepository();
            try
            {

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.Username
                    };
                    var CodeReport = new SqlParameter
                    {
                        ParameterName = "CodeReport",
                        Value = parameter.CodeReport
                    };



                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = CodeReport;

                    var o = _context.Database.SqlQuery<CED098DataChartPartLine>("exec [CED098_GetDataChart] @UserName,@CodeReport", p).ToList<CED098DataChartPartLine>();
                    resp = commonRepository.getChartProblemLine(o);
                    return Ok<GetChartLineProblemResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(commonRepository.OtputErrorMessage(ae.Message));
            }

        }

        //problemBar

        [ResponseType(typeof(GetBarLineProblemResponse))]
        [HttpPost, Route("GetBarLineProblem")]
        public async Task<IHttpActionResult> GetBarLineProblem(GetChartProblemStatusRequest parameter)
        {
            GetBarLineProblemResponse resp = new GetBarLineProblemResponse();
            CommonRepository commonRepository = new CommonRepository();
            try
            {

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.Username
                    };
                    var CodeReport = new SqlParameter
                    {
                        ParameterName = "CodeReport",
                        Value = parameter.CodeReport
                    };



                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = CodeReport;

                    var o = _context.Database.SqlQuery<CED098DataBarLine>("exec [CED098_GetDataChart] @UserName,@CodeReport", p).ToList<CED098DataBarLine>();
                    resp = commonRepository.getBarProblemLine(o);
                    return Ok<GetBarLineProblemResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(commonRepository.OtputErrorMessage(ae.Message));
            }

        }



    }
}
