﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED061100;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IPOSWS.WebApi.Models.CED050200;


namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED061100Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetModuleKanbanCrippleResponse))]
        [HttpPost, Route("GetModuleKanbanCripple")]
        public async Task<IHttpActionResult> GetModuleKanbanCripple(SubmitOpenModuleCrippleRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                List<GetModuleKanbanCrippleResponse> resp = new List<GetModuleKanbanCrippleResponse>();

                return await Task.Run(() =>
                {
                    var CaseMark = new SqlParameter
                    {
                        ParameterName = "CaseMark",
                        Value = parameter.CaseMark
                    };

                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = CaseMark;
                    p[1] = UserName;

                    var o = _context.Database.SqlQuery<GetModuleKanbanCrippleResponse>("exec [CED061100B_GetModuleKanbanCripple] @CaseMark,@UserName", p).ToList<GetModuleKanbanCrippleResponse>();

                    resp = common.CED061100BGetModuleKanbanCrippleResponse(o);

                    return Ok<GetModuleKanbanCrippleResponse>(resp.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(SubmitKanbanCrippleStackingResponse))]
        [HttpPost, Route("SubmitKanbanCrippleStacking")]
        public async Task<IHttpActionResult> SubmitKanbanCrippleStacking(SubmitKanbanCrippleStackingRequest parameter)
        {

            //int paramCounter = 0;
            //string KanbanId = string.Empty;
            //string StackingDate = string.Empty;
            //string KanbanProblem = string.Empty;
            //string StackingFlag = string.Empty;
            //string Kanban2 = string.Empty;
            //string Kanban3 = string.Empty;
            //foreach (var item in parameter.listKanban)
            //{
            //    paramCounter++;
            //    KanbanId += item.KanbanID + (paramCounter < parameter.listKanban.Count() ? "|" : "");
            //    StackingDate += item.StackingDate + (paramCounter < parameter.listKanban.Count() ? "|" : "");
            //    KanbanProblem += item.kanbanProblem + (paramCounter < parameter.listKanban.Count() ? "|" : "");
            //    StackingFlag += item.StackingFlag + (paramCounter < parameter.listKanban.Count() ? "|" : "");
            //    //Kanban2 += item.Kanban2 + (paramCounter < parameter.listKanban.Count() ? "|" : "");
            //    //Kanban3 += item.Kanban3 + (paramCounter < parameter.listKanban.Count() ? "|" : "");
            //}

            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var pCtrlModNo = new SqlParameter
                    {
                        ParameterName = "CtrlModNo",
                        Value = parameter.CtrlModNo
                    };

                    //var pKanbanID = new SqlParameter
                    //{
                    //    ParameterName = "KanbanID",
                    //    Value = KanbanId
                    //};

                    //var pStackingDate = new SqlParameter
                    //{
                    //    ParameterName = "StackingDate",
                    //    Value = StackingDate
                    //};

                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var pGunId = new SqlParameter
                    {
                        ParameterName = "GunId",
                        Value = parameter.GunId
                    };

                    //var pKanbanProblem = new SqlParameter
                    //{
                    //    ParameterName = "KanbanProblem",
                    //    Value = KanbanProblem
                    //};

                    var pLine = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.Line
                    };

                    //var pStackingFlag = new SqlParameter
                    //{
                    //    ParameterName = "StackingFlag",
                    //    Value = StackingFlag
                    //};


                    
                    //var pKanban2 = new SqlParameter
                    //{
                    //    ParameterName = "Kanban2",
                    //    Value = Kanban2
                    //};

                    //var pKanban3 = new SqlParameter
                    //{
                    //    ParameterName = "Kanban3",
                    //    Value = Kanban3
                    //};


                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = pCtrlModNo;
                    //p[1] = pKanbanID;
                    //p[2] = pStackingDate;
                    p[1] = pUserName;
                    p[2] = pGunId;
                    //p[5] = pKanbanProblem;
                    p[3] = pLine;
                    //p[7] = pStackingFlag;
                    //p[6] = pKanban2;
                    //p[7] = pKanban3;

                    var o = _context.Database.SqlQuery<SubmitKanbanCrippleStackingResponse>("exec [CED061100B_SubmitKanbanCrippleStacking] @CtrlModNo,@UserName,@GunId,@Line", p).ToList<SubmitKanbanCrippleStackingResponse>();
                    return Ok<SubmitKanbanCrippleStackingResponse>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }



        [ResponseType(typeof(GetZoneResponse))]
        [HttpPost, Route("GetZoneStackingCripple")]
        public async Task<IHttpActionResult> GetZoneFlowrack(GetZoneRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetZoneResponse resp = new GetZoneResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.userName
                    };

                    var ZoneGroupingCd = new SqlParameter
                    {
                        ParameterName = "ZoneGroupingCd",
                        Value = "ST"
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = ZoneGroupingCd;

                    var o = _context.Database.SqlQuery<GetZone>("exec [Common_GetZone] @UserName,@ZoneGroupingCd", p).ToList<GetZone>();

                    resp = common.CED050200GetZone(o);

                    return Ok<GetZoneResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }



        [ResponseType(typeof(BaseMessage))]
        [HttpPost, Route("PostKanbanCripple")]
        public async Task<IHttpActionResult> SubmitKanbanCripple(PostKanbanCrippleRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var pCtrlModNo = new SqlParameter
                    {
                        ParameterName = "CtrlModNo",
                        Value = parameter.CtrlModNo
                    };

                    var pKanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = parameter.KanbanID
                    };

                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var pLine = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.Line
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = pCtrlModNo;
                    p[1] = pKanbanID;
                    p[2] = pUserName;
                    p[3] = pLine;

                    var o = _context.Database.SqlQuery<BaseMessage>("exec [CED061100B_SubmitKanbanCripple] @CtrlModNo,@KanbanID,@UserName,@Line", p).ToList<BaseMessage>();
                    return Ok<BaseMessage>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }



        [ResponseType(typeof(BaseMessage))]
        [HttpPost, Route("PostProblemStackingCripple")]
        public async Task<IHttpActionResult> PostProblemStacking(PostProblemStackingCrippleUpdateRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                BaseMessage resp = new BaseMessage();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = parameter.kanbanID
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = KanbanID;
                    p[2] = Line;

                    var o = _context.Database.SqlQuery<BaseMessage>("exec [CED061100B_PostProblemStackingCripple] @UserName,@KanbanID,@Line", p).SingleOrDefault<BaseMessage>();

                    //resp = common.CED050600GetProblemPickingUpdateResponse(o);

                    return Ok<BaseMessage>(o);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


    }
}
