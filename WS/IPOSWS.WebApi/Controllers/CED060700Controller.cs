﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED060700;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IPOSWS.WebApi.Models.CED050200;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED060700Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }


        [ResponseType(typeof(GetNextStackingDataResponse))]
        [HttpPost, Route("GetNextStackingData")]
        public async Task<IHttpActionResult> GetNextStackingData(GetNextStackingDataRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                List<GetNextStackingDataResponse> resp = new List<GetNextStackingDataResponse>();

                return await Task.Run(() =>
                {

                    var LineStacking = new SqlParameter
                    {
                        ParameterName = "LineStacking",
                        Value = parameter.LineStacking == null ? "" : parameter.LineStacking
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = LineStacking;


                    var o = _context.Database.SqlQuery<GetNextStackingData>("exec [CED060700B_GetNextStackingData] @LineStacking", p).ToList<GetNextStackingData>();

                    resp = common.CED060700BGetNextStackingDataResponse(o);

                    return Ok<GetNextStackingDataResponse>(resp.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(GetKanbanStackingResponse))]
        [HttpPost, Route("GetKanbanStacking")]
        public async Task<IHttpActionResult> GetNextStackingData(GetKanbanStackingRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetKanbanStackingResponse resp = new GetKanbanStackingResponse();

                return await Task.Run(() =>
                {
                    var Username = new SqlParameter
                    {
                        ParameterName = "Username",
                        Value = parameter.Username
                    };
                    var KanbanId = new SqlParameter
                    {
                        ParameterName = "KanbanId",
                        Value = parameter.KanbanId
                    };
                    var KanbanType = new SqlParameter
                    {
                        ParameterName = "KanbanType",
                        Value = parameter.KanbanType
                    };
                    var Kanban4 = new SqlParameter
                    {
                        ParameterName = "Kanban4",
                        Value = parameter.Kanban4
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = Username;
                    p[1] = KanbanId;
                    p[2] = KanbanType;
                    p[3] = Kanban4;

                    var o = _context.Database.SqlQuery<GetKanbanStackingResponse>("exec [CED060700B_GetKanbanStacking] @Username,@KanbanId,@KanbanType,@Kanban4", p).ToList<GetKanbanStackingResponse>();

                    resp = common.CED060700BGetKanbanStackingResponse(o);

                    return Ok<GetKanbanStackingResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }


        [ResponseType(typeof(PostModuleOpenResponse))]
        [HttpPost, Route("PostModuleOpen")]
        public async Task<IHttpActionResult> PostModuleOpen(PostModuleOpenRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {
                    var LineStacking = new SqlParameter
                    {
                        ParameterName = "LineStacking",
                        Value = parameter.LineStacking
                    };

                    var ProdDate = new SqlParameter
                    {
                        ParameterName = "ProdDate",
                        Value = parameter.ProdDate
                    };

                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var StackingDate = new SqlParameter
                    {
                        ParameterName = "StackingDate",
                        Value = parameter.StackingDate
                    };

                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[5];
                    p[0] = LineStacking;
                    p[1] = ProdDate;
                    p[2] = ControlModuleNo;
                    p[3] = StackingDate;
                    p[4] = UserName;

                    var o = _context.Database.SqlQuery<PostModuleOpenResponse>("exec [CED060700B_PostModuleOpen] @LineStacking,@ProdDate,@ControlModuleNo,@StackingDate,@UserName", p).ToList<PostModuleOpenResponse>();

                    return Ok<PostModuleOpenResponse>(o.FirstOrDefault());
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }



        [ResponseType(typeof(GetZoneResponse))]
        [HttpPost, Route("GetZoneStacking")]
        public async Task<IHttpActionResult> GetZoneFlowrack(GetZoneRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetZoneResponse resp = new GetZoneResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.userName
                    };

                    var ZoneGroupingCd = new SqlParameter
                    {
                        ParameterName = "ZoneGroupingCd",
                        Value = "ST"
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = ZoneGroupingCd;

                    var o = _context.Database.SqlQuery<GetZone>("exec [Common_GetZone] @UserName,@ZoneGroupingCd", p).ToList<GetZone>();

                    resp = common.CED050200GetZone(o);

                    return Ok<GetZoneResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }



        //[ResponseType(typeof(PostKanbanStackingResponse))]
        //[HttpPost, Route("PostKanbanStacking")]
        //public async Task<IHttpActionResult> PostKanbanStacking(PostKanbanStackingRequest parameter)
        //{

        //    int paramCounter = 0;
        //    string KanbanID = string.Empty;
        //    string LineStacking = string.Empty;
        //    string ProdDate = string.Empty;
        //    string ControlModuleNo = string.Empty;
        //    string StackingDate = string.Empty;
        //    string UserName = string.Empty;
        //    var res = new PostKanbanStackingResponse();


        //    foreach (var item in parameter.stackingData)
        //    {
        //        paramCounter++;
        //        KanbanID += item.KanbanID + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        LineStacking += item.LineStacking + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        ProdDate += item.ProdDate + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        ControlModuleNo += item.ControlModuleNo + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        StackingDate += item.StackingDate + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //    }

        //    try
        //    {
        //        return await Task.Run(() =>
        //        {
        //            var pLineStacking = new SqlParameter
        //            {
        //                ParameterName = "LineStacking",
        //                Value = LineStacking
        //            };

        //            var pProdDate = new SqlParameter
        //            {
        //                ParameterName = "ProdDate",
        //                Value = ProdDate
        //            };

        //            var pControlModuleNo = new SqlParameter
        //            {
        //                ParameterName = "ControlModuleNo",
        //                Value = ControlModuleNo
        //            };

        //            var pKanbanID = new SqlParameter
        //            {
        //                ParameterName = "KanbanID",
        //                Value = KanbanID
        //            };

        //            var pStackingDate = new SqlParameter
        //            {
        //                ParameterName = "StackingDate",
        //                Value = StackingDate
        //            };

        //            var pUserName = new SqlParameter
        //            {
        //                ParameterName = "UserName",
        //                Value = parameter.UserName
        //            };

        //            SqlParameter[] p = new SqlParameter[6];
        //            p[0] = pLineStacking;
        //            p[1] = pProdDate;
        //            p[2] = pControlModuleNo;
        //            p[3] = pKanbanID;
        //            p[4] = pStackingDate;
        //            p[5] = pUserName;

        //            var o = _context.Database.SqlQuery<BaseMessage>("exec [CED060700B_PostKanbanStacking] @LineStacking,@ProdDate,@ControlModuleNo,@KanbanID,@StackingDate,@UserName", p).ToList<BaseMessage>();
                    
        //            res.messageDetail = o;

        //            return Ok<PostKanbanStackingResponse>(res);
        //        });
        //    }
        //    catch (Exception ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //}



        //[ResponseType(typeof(PostKanbanStackingResponse))]
        //[HttpPost, Route("PostKanban2Stacking")]
        //public async Task<IHttpActionResult> PostKanbanStacking(PostKanban2StackingRequest parameter)
        //{

        //    int paramCounter = 0;
        //    string Kanban2 = string.Empty;
        //    string Kanban3 = string.Empty;
        //    string Kanban4 = string.Empty;
        //    string StackingDate = string.Empty;
        //    string UserName = string.Empty;
        //    var res = new PostKanbanStackingResponse();


        //    foreach (var item in parameter.stackingData)
        //    {
        //        paramCounter++;
        //        Kanban2 += item.Kanban2 + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        Kanban3 += item.Kanban3 + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        Kanban4 += item.Kanban4 + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //        StackingDate += item.StackingDate + (paramCounter < parameter.stackingData.Count() ? "|" : "");
        //    }

        //    try
        //    {
        //        return await Task.Run(() =>
        //        {
        //            var pKanban2 = new SqlParameter
        //            {
        //                ParameterName = "Kanban2",
        //                Value = Kanban2
        //            };

        //            var pKanban3 = new SqlParameter
        //            {
        //                ParameterName = "Kanban3",
        //                Value = Kanban3
        //            };

        //            var pKanban4 = new SqlParameter
        //            {
        //                ParameterName = "Kanban4",
        //                Value = Kanban4
        //            };

        //            var pStackingDate = new SqlParameter
        //            {
        //                ParameterName = "StackingDate",
        //                Value = StackingDate
        //            };

        //            var pUserName = new SqlParameter
        //            {
        //                ParameterName = "UserName",
        //                Value = parameter.UserName
        //            };

        //            SqlParameter[] p = new SqlParameter[5];
        //            p[0] = pKanban2;
        //            p[1] = pKanban3;
        //            p[2] = pKanban4;
        //            p[3] = pStackingDate;
        //            p[4] = pUserName;

        //            var o = _context.Database.SqlQuery<BaseMessage>("exec [CED060700B_PostKanban2stacking] @Kanban2,@Kanban3,@Kanban4,@StackingDate,@UserName", p).ToList<BaseMessage>();

        //            res.messageDetail = o;

        //            return Ok<PostKanbanStackingResponse>(res);
        //        });
        //    }
        //    catch (Exception ae)
        //    {
        //        return BadRequest(ae.Message);
        //    }
        //}

        [ResponseType(typeof(PostModuleCloseResponse))]
        [HttpPost, Route("PostModuleClose")]
        public async Task<IHttpActionResult> PostModuleClose(PostModuleCloseRequest parameter)
        {
            
            CommonRepository common = new CommonRepository();

            int paramCounter = 0;
            string KanbanId     = string.Empty;
            string KanbanType   = string.Empty;
            string BoxNo        = string.Empty;
            string PartNo       = string.Empty;
            string StackingFlag = string.Empty;
            string StackingDate = string.Empty;
            string ProblemSts   = string.Empty;
            string PartProblem  = string.Empty;
            
            string problemType  = string.Empty;
            string start        = string.Empty;
            string finish       = string.Empty;
            string ct           = string.Empty;
            
            foreach (var item in parameter.listKanbanId)
            {
                paramCounter++;
                KanbanId    += item.KanbanId + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                KanbanType  += item.KanbanType + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                BoxNo       += item.BoxNo + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                PartNo      += item.PartNo + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                StackingFlag+= item.StackingFlag + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                StackingDate+= item.StackingDate + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                ProblemSts  += item.ProblemSts + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
                PartProblem += item.PartProblem + (paramCounter < parameter.listKanbanId.Count() ? "|" : "");
            }


            paramCounter = 0;
            foreach (var item in parameter.listProblem)
            {
                paramCounter++;
                problemType += item.problemType + (paramCounter < parameter.listProblem.Count() ? "|" : "");
                start += item.start + (paramCounter < parameter.listProblem.Count() ? "|" : "");
                finish += item.finish + (paramCounter < parameter.listProblem.Count() ? "|" : "");
                ct += item.ct + (paramCounter < parameter.listProblem.Count() ? "|" : "");
            }


            try
            {
                return await Task.Run(() =>
                {

                    var pKanbanId = new SqlParameter
                    {
                        ParameterName = "KanbanId",
                        Value = KanbanId
                    };
                    var pKanbanType = new SqlParameter
                    {
                        ParameterName = "KanbanType",
                        Value = KanbanType
                    };
                    var pBoxNo = new SqlParameter
                    {
                        ParameterName = "BoxNo",
                        Value = BoxNo
                    };
                    var pPartNo = new SqlParameter
                    {
                        ParameterName = "PartNo",
                        Value = PartNo
                    };

                    var pStackingFlag = new SqlParameter
                    {
                        ParameterName = "StackingFlag",
                        Value = StackingFlag
                    };
                    var pStackingDateKanban = new SqlParameter
                    {
                        ParameterName = "StackingDateKanban",
                        Value = StackingDate
                    };
                    var pProblemSts = new SqlParameter
                    {
                        ParameterName = "ProblemSts",
                        Value = ProblemSts
                    };

                    var pLineStacking = new SqlParameter
                    {
                        ParameterName = "LineStacking",
                        Value = parameter.LineStacking
                    };

                    var pProdDate = new SqlParameter
                    {
                        ParameterName = "ProdDate",
                        Value = parameter.ProdDate
                    };

                    var pControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    var pProblemPart = new SqlParameter
                    {
                        ParameterName = "PartProblem",
                        Value = PartProblem
                    };

                    var pGunId = new SqlParameter
                    {
                        ParameterName = "GunId",
                        Value = parameter.GunId
                    };



                    var pProblemType = new SqlParameter
                    {
                        ParameterName = "ProblemType",
                        Value = problemType
                    };

                    var pStart = new SqlParameter
                    {
                        ParameterName = "Start",
                        Value = start
                    };

                    var pFinish = new SqlParameter
                    {
                        ParameterName = "Finish",
                        Value = finish
                    };

                    var pCt = new SqlParameter
                    {
                        ParameterName = "Ct",
                        Value = ct
                    };


                    //SqlParameter[] q = new SqlParameter[13];
                    SqlParameter[] q = new SqlParameter[17];
                    q[0] = pKanbanId;
                    q[1] = pKanbanType;
                    q[2] = pBoxNo;
                    q[3] = pPartNo;
                    q[4] = pStackingFlag;
                    q[5] = pStackingDateKanban;
                    q[6] = pUserName;
                    q[7] = pLineStacking;
                    q[8]= pProdDate;
                    q[9]= pControlModuleNo;
                    q[10]= pProblemSts;
                    q[11]= pProblemPart;
                    q[12]= pGunId;

                    q[13] = pProblemType;
                    q[14] = pStart;
                    q[15] = pFinish;
                    q[16] = pCt;

                    var r = _context.Database.SqlQuery<PostModuleCloseResponse>("exec [CED060700B_PostModuleClose] @KanbanId,@KanbanType,@BoxNo,@PartNo,@StackingFlag,@StackingDateKanban,@UserName,@LineStacking,@ProdDate,@ControlModuleNo,@ProblemSts,@PartProblem,@GunId,@ProblemType,@Start,@Finish,@Ct", q).ToList<PostModuleCloseResponse>();
                    return Ok<PostModuleCloseResponse>(common.PostModuleCloseResponse(r));
                });
            }
            catch (Exception ae)
            {
                //[WOT Farhan] - 2022-02-07, Add Message For Timeout
                var message = ae.Message;
                message = "Timeout, Please Re-scan ContentList";
                if (ae.Message.Contains("Timeout expired") || ae.Message.Contains("Execution Timeout Expired"))
                {
                    return Json<ErrorMessage>(common.OtputErrorMessage(message));
                }
                else
                {
                    return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
                }
                //return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }


        [ResponseType(typeof(GetBarcodeProcessResponse))]
        [HttpPost, Route("GetBarcodeProcess")]
        public async Task<IHttpActionResult> GetBarcodeProcess()
        {
            CommonRepository common = new CommonRepository();

            try
            {
                var res = new GetBarcodeProcessResponse();

                return await Task.Run(() =>
                {
                    var o = _context.Database.SqlQuery<GetBarcodeProcess>("exec [CED060700B_GetBarcodeProcess]").ToList<GetBarcodeProcess>();

                    res.data = o;

                    if(o.Count > 0){
                        res.Result = "Success";
                        res.Message = "MCEDSTD013I : Data retrieved successfully";
                    }else{
                        res.Result = "Success";
                        res.Message = "MCEDSTD030I : No data found";
                    }

                    return Ok<GetBarcodeProcessResponse>(res);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

    }
}
