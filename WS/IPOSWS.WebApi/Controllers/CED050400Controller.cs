﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED050400;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;
using IPOSWS.WebApi.Models.CED050200;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED050400Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetFlowRackResponse))]
        [HttpPost, Route("GetFlowRack")]
        public async Task<IHttpActionResult> GetFlowRack(GetFlowRackRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetFlowRackResponse resp = new GetFlowRackResponse();

                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.UserName
                    };


                    var pParam = new SqlParameter
                    {
                        ParameterName = "Param",
                        Value = param.Param
                    };

                    //var pLine = new SqlParameter
                    //{
                    //    ParameterName = "Line",
                    //    Value = param.Line
                    //};


                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = pUserName;
                    p[1] = pParam;
                    //p[2] = pLine;
                    var o = _context.Database.SqlQuery<CED050400FlowRack>("exec [CED050400A_GetFlowRackData] @UserName,@Param", p).ToList<CED050400FlowRack>();
                    //var o = _context.Database.SqlQuery<CED050400FlowRack>("exec [CED050400A_GetFlowRackData] @UserName,@Param,@Line", p).ToList<CED050400FlowRack>();

                    resp = common.CED050400FlowRack(o);

                    return Ok<GetFlowRackResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
            
        }



        [ResponseType(typeof(PostSupplyFlowRackResponse))]
        [HttpPost, Route("PostSupplyFlowRack")]
        public async Task<IHttpActionResult> PostSupplyFlowRack(PostSupplyFlowRackRequest parameter)
        {

            int paramCounter = 0;
            string kanbanId = string.Empty;

            foreach (var item in parameter.kanbanId)
            {
                paramCounter++;
                kanbanId += item.kanbanID + (paramCounter < parameter.kanbanId.Count() ? "|" : "");
            }


            CommonRepository common = new CommonRepository();

            try
            {
                PostSupplyFlowRackResponse resp = new PostSupplyFlowRackResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var pKanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = kanbanId
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = pKanbanID;
                    p[2] = Line;

                    var o = _context.Database.SqlQuery<PostSupplyFlowRackResponse>("exec [CED050400A_PostSupplyFlowRack] @UserName,@KanbanID,@Line", p).SingleOrDefault<PostSupplyFlowRackResponse>();

                    resp = common.CED050400PostSupplyFlowRackResponse(o);

                    return Ok<PostSupplyFlowRackResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


        [ResponseType(typeof(PostProblemFlowRackResponse))]
        [HttpPost, Route("PostProblemFlowRack")]
        public async Task<IHttpActionResult> PostProblemFlowRack(PostProblemFlowRackRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                PostProblemFlowRackResponse resp = new PostProblemFlowRackResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.userName
                    };

                    var KanbanID = new SqlParameter
                    {
                        ParameterName = "KanbanID",
                        Value = parameter.kanbanID
                    };

                    var Line = new SqlParameter
                    {
                        ParameterName = "Line",
                        Value = parameter.line
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = UserName;
                    p[1] = KanbanID;
                    p[2] = Line;


                    var o = _context.Database.SqlQuery<PostProblemFlowRackResponse>("exec [CED050400A_PostProblemFlowRack] @UserName,@KanbanID,@Line", p).SingleOrDefault<PostProblemFlowRackResponse>();

                    resp = common.CED050400PostProblemFlowRackResponse(o);

                    return Ok<PostProblemFlowRackResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        [ResponseType(typeof(GetZoneResponse))]
        [HttpPost, Route("GetZoneFlowrack")]
        public async Task<IHttpActionResult> GetZoneFlowrack(GetZoneRequest param)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetZoneResponse resp = new GetZoneResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = param.userName
                    };

                    var ZoneGroupingCd = new SqlParameter
                    {
                        ParameterName = "ZoneGroupingCd",
                        Value = "FR"
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = UserName;
                    p[1] = ZoneGroupingCd;

                    var o = _context.Database.SqlQuery<GetZone>("exec [Common_GetZone] @UserName,@ZoneGroupingCd", p).ToList<GetZone>();

                    resp = common.CED050200GetZone(o);

                    return Ok<GetZoneResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        													

    }
}
