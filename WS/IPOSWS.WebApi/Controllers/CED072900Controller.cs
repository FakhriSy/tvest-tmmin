﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED072900;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED072900Controller : CorsController //ApiController//
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetPOSSummaryResponse))]
        [HttpPost, Route("GetPOSSummary")]
        public async Task<IHttpActionResult> GetPOSSummary(GetPOSSummaryRequest parameter)
        {

            CommonRepository common = new CommonRepository();
            try
            {

                GetPOSSummaryResponse resp = new GetPOSSummaryResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };

                    var LineCd = new SqlParameter
                    {
                        ParameterName = "LineCd",
                        Value = parameter.LineCd
                    };

                    var ProdDt = new SqlParameter
                    {
                        ParameterName = "ProdDt",
                        Value = parameter.ProdDt
                    };

                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = Shift;
                    p[2] = LineCd;
                    p[3] = ProdDt;

                    var o = _context.Database.SqlQuery<POSSumary>("exec [CED072900B_GetPOSSummary] @UserName,@Shift,@LineCd,@ProdDt", p).ToList<POSSumary>();
                    resp = common.PostSummaryOutputResponse(o);
                    return Ok<GetPOSSummaryResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(GetPOSDataResponse))]
        [HttpPost, Route("GetPOSData")]
        public async Task<IHttpActionResult> GetPOSData(GetPOSDataRequest parameter)
        {
            CommonRepository common = new CommonRepository();
            try
            {
                GetPOSDataResponse resp = new GetPOSDataResponse();

                return await Task.Run(() =>
                {
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };
                    var ZoneCd = new SqlParameter
                    {
                        ParameterName = "ZoneCd",
                        Value = parameter.ZoneCd
                    };
                    var Shift = new SqlParameter
                    {
                        ParameterName = "Shift",
                        Value = parameter.Shift
                    };
                    var ProdDt = new SqlParameter
                    {
                        ParameterName = "ProdDt",
                        Value = parameter.ProdDt
                    };


                    SqlParameter[] p = new SqlParameter[4];
                    p[0] = UserName;
                    p[1] = ZoneCd;
                    p[2] = Shift;
                    p[3] = ProdDt;

                    var o = _context.Database.SqlQuery<PostDataResponse>("exec [CED072900B_GetPOSData] @UserName,@ZoneCd,@Shift,@ProdDt", p).ToList<PostDataResponse>();

                    resp = common.PostDataOutputResponse(o);

                    return Ok<GetPOSDataResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(GetPOSUpdateResponse))]
        [HttpPost, Route("GetPOSUpdate")]
        public async Task<IHttpActionResult> GetPOSUpdate(GetPOSUpdateRequest parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                return await Task.Run(() =>
                {

                    var Username = new SqlParameter
                    {
                        ParameterName = "Username",
                        Value = parameter.Username
                    };

                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var SkipControlModuleNo = new SqlParameter
                    {
                        ParameterName = "SkipControlModuleNo",
                        Value = parameter.SkipControlModuleNo
                    };

                    var Shift = new SqlParameter
                    {
                        ParameterName = "Action",
                        Value = parameter.Action
                    };

                    var ChangedBy = new SqlParameter
                    {
                        ParameterName = "ChangedBy",
                        Value = parameter.ChangedBy
                    };

                    var ChangedDt = new SqlParameter
                    {
                        ParameterName = "ChangedDt",
                        Value = parameter.ChangedDt
                    };

                    var SkipChangedBy = new SqlParameter
                    {
                        ParameterName = "SkipChangedBy",
                        Value = parameter.SkipChangedBy
                    };

                    var SkipChangedDt = new SqlParameter
                    {
                        ParameterName = "SkipChangedDt",
                        Value = parameter.SkipChangedDt
                    };


                    SqlParameter[] p = new SqlParameter[8];
                    p[0] = Username;
                    p[1] = ControlModuleNo;
                    p[2] = SkipControlModuleNo;
                    p[3] = Shift;
                    p[4] = ChangedBy;
                    p[5] = ChangedDt;
                    p[6] = SkipChangedBy;
                    p[7] = SkipChangedDt;

                    var o = _context.Database.SqlQuery<GetPOSUpdateResponse>("exec [CED072900B_GetPOSUpdate] @Username,@ControlModuleNo,@SkipControlModuleNo,@Action,@ChangedBy,@ChangedDt,@SkipChangedBy,@SkipChangedDt", p).ToList<GetPOSUpdateResponse>();
                    return Ok<List<GetPOSUpdateResponse>>(o);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

        [ResponseType(typeof(GetPOSLineResponse))]
        [HttpPost, Route("GetPOSLine")]
        public async Task<IHttpActionResult> GetPOSLine()
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetPOSLineResponse resp = new GetPOSLineResponse();

                return await Task.Run(() =>
                {

                    var o = _context.Database.SqlQuery<POSLineResponse>("exec [CED072900B_GetPOSLine]").ToList<POSLineResponse>();
                    resp = common.POSLineOutputResponse(o);
                    return Ok<GetPOSLineResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }
        }

    }
}
