﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models.CED010214;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IPOSWS.WebApi.Controllers
{
    public class CED010214Controller : CorsController//ApiController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        /// <summary>
        /// Get Data
        /// </summary>
        /// <param name="parameter">Packing Date</param>
        /// <param name="parameter">Destination Code</param>
        /// <param name="parameter">Plant</param>
        /// <param name="parameter">Lot No</param>
        /// <returns>List</returns>
        [AllowAnonymous]
        [HttpGet, Route("GetDetailCaseMarkContentList")]
        public IHttpActionResult GetDetailCaseMarkContentList(string Packing_Date = null, string Destination_Code = null, string Plant = null, string LotNo = null)
        {
            string sql = "sp_PrintCaseMarkContentList '" + Packing_Date + "','" + Destination_Code + "','" + Plant + "','" + LotNo + "'";
            var o = _context.Database.SqlQuery<CED010214>(sql).ToList<CED010214>();
            return Ok<List<CED010214>>(o);
        }

        /// <summary>
        /// Print Data
        /// </summary>
        /// <param name="parameter">Packing Date</param>
        /// <param name="parameter">Destination Code</param>
        /// <param name="parameter">Plant</param>
        /// <param name="parameter">Lot No</param>
        /// <returns>List</returns>
        //[AllowAnonymous]
        //[HttpGet, Route("SendCaseMarkContentListJobPrint")]
        //public IHttpActionResult SendCaseMarkContentListJobPrint(string packingdate, string destinationcode, string plant, string lotno)
        //{
        //    string sql = "sp_PrintCaseMarkContentList '" + packingdate + "','" + destinationcode + "','" + plant + "','" + lotno + "'";
        //    var o = _context.Database.SqlQuery<CED010214>(sql).ToList<CED010214>();
        //    return Ok<List<CED010214>>(o);
        //}
    }
}
