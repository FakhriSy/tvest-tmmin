﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models.CED021300A;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{
    public class CED021300AController : CorsController
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(CED021300APrinterRespons))]
        [HttpPost, Route("GetActivePrinter")]
        public IHttpActionResult GetActivePrinter()
        {
            string sql = "CED021300A_GetActivePrinter";
            var o = _context.Database.SqlQuery<CED021300APrinter>(sql).ToList<CED021300APrinter>();

            CED021300APrinterRespons data = new CED021300APrinterRespons();
            if (o.Count > 0)
            {
                data.data = o;
                data.Result = "Success";
                data.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                data.Result = "Failed";
                data.Message = "MCEDSTD030I : No data found";
            }

            return Ok<CED021300APrinterRespons>(data);
        }

        [ResponseType(typeof(CED021300APrintingRespons))]
        [HttpPost, Route("GetPrintingData")]
        public IHttpActionResult GetPrintingData([FromBody]CED021300APrintingParam Param)
        {
            CED021300APrintingRespons data = new CED021300APrintingRespons();
            if (CheckFormatDate(Param.PackingDt) == false)
            {
                data.Result = "Failed";
                data.Message = "MCEDSTD062E : Invalid format for Packing Date. The format must be Date";
            }
            else
            {
                string sql = "CED021300A_GetPrintingData '" + Param.PackingDt + "', '" + Param.DestinationCd + "', '" + Param.LotModuleNo + "', '" + Param.PrintFlag + "'";
                var o = _context.Database.SqlQuery<CED021300APrintingData>(sql).ToList<CED021300APrintingData>();

                if (o.Count > 0)
                {
                    data.data = o;
                    data.Result = "Success";
                    data.Message = "MCEDSTD013I : Data retrieved successfully";
                }
                else
                {
                    data.Result = "Failed";
                    data.Message = "MCEDSTD030I : No data found";
                }
            }

            return Ok<CED021300APrintingRespons>(data);
        }

        [ResponseType(typeof(CED021300APOSRespons))]
        [HttpPost, Route("PostPrinterProcess")]
        public IHttpActionResult PostPrinterProcess([FromBody]CED021300APOSParam Param)
        {
            string sql = "CED021300A_PostPrinterProcess '" + Param.UserName + "', '" + Param.ControlModuleNo + "', '" + Param.PrinterCd + "'";
            var o = _context.Database.SqlQuery<CED021300APOSRespons>(sql).ToList<CED021300APOSRespons>();

            CED021300APOSRespons data = new CED021300APOSRespons();
            data.Result = o[0].Result;
            data.Message = o[0].Message;

            return Ok<CED021300APOSRespons>(data);
        }

        private bool CheckFormatDate(string someString)
        {
            DateTime myDate;
            bool isDate = DateTime.TryParse(someString, out myDate);

            return isDate;
        }
    }
}
