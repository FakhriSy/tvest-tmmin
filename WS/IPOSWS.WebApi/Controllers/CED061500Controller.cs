﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED061500;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED061500Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        [ResponseType(typeof(GetPullingResponse))]
        [HttpPost, Route("GetPulling")]
        public async Task<IHttpActionResult> GetPulling(GetPullingRequest Parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetPullingResponse resp = new GetPullingResponse();

                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = Parameter.UserName
                    };

                    var pGroup = new SqlParameter
                    {
                        ParameterName = "Group",
                        Value = Parameter.Group
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = pUserName;
                    p[1] = pGroup;

                    var o = _context.Database.SqlQuery<CED061500Pulling>("exec [SP_CED061500A_GetPulling] @UserName,@Group", p).ToList<CED061500Pulling>();

                    resp = common.CED061500GetPullingResponse(o);

                    return Ok<GetPullingResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }



        [ResponseType(typeof(GetPullingUpdateResponse))]
        [HttpPost, Route("GetPullingUpdate")]
        public async Task<IHttpActionResult> GetPullingUpdate(GetPullingUpdateRequest parameter)
        {

            CommonRepository common = new CommonRepository();
            try
            {
                GetPullingUpdateResponse resp = new GetPullingUpdateResponse();

                return await Task.Run(() =>
                {
                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };

                    var Mode = new SqlParameter
                    {
                        ParameterName = "Mode",
                        Value = parameter.Mode
                    };
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = ControlModuleNo;
                    p[1] = Mode;
                    p[2] = UserName;


                    var o = _context.Database.SqlQuery<GetPullingUpdateResponse>("exec [SP_CED061500A_GetPullingUpdate] @ControlModuleNo,@Mode,@UserName", p).SingleOrDefault<GetPullingUpdateResponse>();

                    resp = common.CED061500GetPullingUpdateResponse(o);

                    return Ok<GetPullingUpdateResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


    }
}
