﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED061300;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.SqlClient;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Description;

namespace IPOSWS.WebApi.Controllers
{

    [RoutePrefix("api/v1")]
    public class CED061300Controller : CorsController //ApiController //
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }


        [ResponseType(typeof(GetModuleResponse))]
        [HttpPost, Route("GetItemModule")]
        public async Task<IHttpActionResult> GetItemModule(GetItemModuleRequest Parameter)
        {
            CommonRepository common = new CommonRepository();

            try
            {
                GetItemModuleResponse resp = new GetItemModuleResponse();

                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = Parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[1];
                    p[0] = pUserName;

                    var o = _context.Database.SqlQuery<CED061300ItemModule>("exec [CED061300A_GetItemModule] @UserName", p).ToList<CED061300ItemModule>();

                    resp = common.CED061300GetItemModuleResponse(o);

                    return Ok<GetItemModuleResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        [ResponseType(typeof(GetModuleResponse))]
        [HttpPost, Route("GetModule")]
        public async Task<IHttpActionResult> GetModule(GetModuleRequest Parameter)
        {

            CommonRepository common = new CommonRepository();
            try
            {
                GetModuleResponse resp = new GetModuleResponse();


                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = Parameter.UserName
                    };

                    var pGroup = new SqlParameter
                    {
                        ParameterName = "Group",
                        Value = Parameter.Group
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = pUserName;
                    p[1] = pGroup;

                    var o = _context.Database.SqlQuery<CED061300Module>("exec [CED061300A_GetModule] @UserName,@Group", p).ToList<CED061300Module>();

                    resp = common.CED061300GetModuleResponse(o);

                    return Ok<GetModuleResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        [ResponseType(typeof(GetSupplyResponse))]
        [HttpPost, Route("GetSupply")]
        public async Task<IHttpActionResult> GetSupply(GetSupplyRequest Parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetSupplyResponse resp = new GetSupplyResponse();

                return await Task.Run(() =>
                {
                    var pUserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = Parameter.UserName
                    };

                    var pGroup = new SqlParameter
                    {
                        ParameterName = "Group",
                        Value = Parameter.Group
                    };

                    SqlParameter[] p = new SqlParameter[2];
                    p[0] = pUserName;
                    p[1] = pGroup;


                    var o = _context.Database.SqlQuery<CED061300Supply>("exec [CED061300A_GetSupply] @UserName,@Group", p).ToList<CED061300Supply>();

                    resp = common.CED061300GetSupplyResponse(o);

                    return Ok<GetSupplyResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }

        [ResponseType(typeof(GetSupplyUpdateResponse))]
        [HttpPost, Route("GetSupplyUpdate")]
        public async Task<IHttpActionResult> GetSupplyUpdate(GetSupplyUpdateRequest parameter)
        {

            CommonRepository common = new CommonRepository();

            try
            {
                GetSupplyUpdateResponse resp = new GetSupplyUpdateResponse();

                return await Task.Run(() =>
                {
                    var ControlModuleNo = new SqlParameter
                    {
                        ParameterName = "ControlModuleNo",
                        Value = parameter.ControlModuleNo
                    };
                    var Mode = new SqlParameter
                    {
                        ParameterName = "Mode",
                        Value = parameter.Mode
                    };
                    var UserName = new SqlParameter
                    {
                        ParameterName = "UserName",
                        Value = parameter.UserName
                    };

                    SqlParameter[] p = new SqlParameter[3];
                    p[0] = ControlModuleNo;
                    p[1] = Mode;
                    p[2] = UserName;

                    var o = _context.Database.SqlQuery<GetSupplyUpdateResponse>("exec [CED061300A_GetSupplyUpdate] @ControlModuleNo,@Mode,@UserName", p).SingleOrDefault<GetSupplyUpdateResponse>();

                    resp = common.CED061300GetSupplyUpdateResponse(o);

                    return Ok<GetSupplyUpdateResponse>(resp);
                });
            }
            catch (Exception ae)
            {
                return Json<ErrorMessage>(common.OtputErrorMessage(ae.Message));
            }

        }


    }
}
