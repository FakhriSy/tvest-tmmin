﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Web;
using IPOSWS.WebApi.Models;

namespace IPOSWS.WebApi.Data.SqlServer.Configurations
{
    /// <summary>
    /// base configuration
    /// </summary>
    /// <typeparam name="T">Dynamic type</typeparam>
    public abstract class BaseConfiguration<T> : EntityTypeConfiguration<T> where T : BaseModel
    {
        /// <summary>
        /// base configuration
        /// </summary>
        public BaseConfiguration()
        {
            HasKey(e => e.SID);

            Property(e => e.SID).HasColumnName("SID");

            Property(e => e.CreatedBy).HasColumnName("CREATED_BY").HasMaxLength(10).IsUnicode(false);
            Property(e => e.UtcCreatedDate).HasColumnName("UTC_CREATED_DATE");
            Property(e => e.UpdatedBy).HasColumnName("UPDATED_BY").HasMaxLength(10).IsUnicode(false);
            Property(e => e.UtcUpdatedDate).HasColumnName("UTC_UPDATED_DATE");
        }
    }
}