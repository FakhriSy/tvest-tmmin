﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Data.SqlServer.Configurations
{
    /// <summary>
    /// Schema
    /// </summary>
    public static class Schema
    {
        /// <summary>
        /// Default schema
        /// </summary>
        public const string DefaultSchema = "dbo";
    }
}