﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using System.Web;
using IPOSWS.WebApi.Data.Interfaces;

namespace IPOSWS.WebApi.Data.SqlServer
{
    /// <summary>
    /// Transaction manager
    /// </summary>
    public class TransactionManager : ITransactionManager
    {
        /// <summary>
        /// Isolation level
        /// </summary>
        private static IsolationLevel ISOLATION_LEVEL = IsolationLevel.ReadCommitted;

        /// <summary>
        /// Db context
        /// </summary>
        private DbContext context { get; set; }

        /// <summary>
        /// Db context transaction
        /// </summary>
        private DbContextTransaction transaction { get; set; }

        /// <summary>
        /// Session
        /// </summary>
        public object Session
        {
            get
            {
                return context;
            }
        }

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context"></param>
        public TransactionManager(DbContext context)
        {
            this.context = context;
        }

        /// <summary>
        /// Start transaction
        /// </summary>
        public void StartTransaction()
        {
            if (transaction != null)
                transaction.Dispose();

            transaction = context.Database.BeginTransaction(ISOLATION_LEVEL);
        }

        /// <summary>
        /// Commiit transaction
        /// </summary>
        /// <returns></returns>
        public int CommitTransaction()
        {
            try
            {
                var result = context.SaveChanges();
                transaction.Commit();
                return result;
            }
            catch (DbEntityValidationException e)
            {
                foreach (var eve in e.EntityValidationErrors)
                {
                    //Log.ErrorFormat("Entity of type \"{0}\" in state \"{1}\" has the following validation errors:",
                    //    eve.Entry.Entity.GetType().Name, eve.Entry.State);
                    //foreach (var ve in eve.ValidationErrors)
                    //{
                    //    Log.ErrorFormat("- Property: \"{0}\", Value: \"{1}\", Error: \"{2}\"",
                    //        ve.PropertyName,
                    //        eve.Entry.CurrentValues.GetValue<object>(ve.PropertyName),
                    //        ve.ErrorMessage);
                    //}
                }
                throw;
            }
            catch
            {
                transaction.Rollback();
                throw;
            }
        }

        public void RollbackTransaction()
        {
            transaction.Rollback();
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        //The bulk of the clean-up code is implemented in Dispose(bool) 
        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (transaction != null)
                {
                    transaction.Dispose();
                    transaction = null;
                }

                if (context != null)
                {
                    context.Database.Connection.Close();
                    context.Dispose();
                    context = null;
                }
            }

            //// free native resources if there are any.
            //if (nativeResource != IntPtr.Zero)
            //{
            //    Marshal.FreeHGlobal(nativeResource);
            //    nativeResource = IntPtr.Zero;
            //}
        }

        // NOTE: Leave out the finalizer altogether if this class doesn't 
        // own unmanaged resources itself, but leave the other methods
        // exactly as they are. 
        ~TransactionManager()
        {
            // Finalizer calls Dispose(false)
            Dispose(false);
        }
    }
}