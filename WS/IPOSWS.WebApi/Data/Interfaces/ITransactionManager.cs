﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Data.Interfaces
{
    /// <summary>
    /// Transaction manager interface
    /// </summary>
    public interface ITransactionManager : IDisposable
    {
        /// <summary>
        /// Sesssion
        /// </summary>
        object Session { get; }

        /// <summary>
        /// Start transaction
        /// </summary>
        void StartTransaction();

        /// <summary>
        /// Commit transaction
        /// </summary>
        /// <returns></returns>
        int CommitTransaction();

        /// <summary>
        /// Rollback transaction
        /// </summary>
        void RollbackTransaction();
    }

}