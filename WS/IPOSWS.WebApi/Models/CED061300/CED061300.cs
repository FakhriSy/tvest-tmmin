﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED061300
{
    public class CED061300
    {
    }

    public class GetItemModuleRequest
    {
        public string UserName { get; set; }
        //public string Shift { get; set; }
    }

    public class GetModuleRequest
    {
        public string UserName { get; set; }
        public string Group { get; set; }
        //public string Shift { get; set; }
    }

    public class GetItemModuleResponse
    {
        public List<CED061300ItemModule> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetModuleResponse
    {
        public string PlanSummary { get; set; }
        public string FinishSummary { get; set; }
        public string RemainSummary { get; set; }
        public string ParamRange { get; set; }
        public List<CED061300Module> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetSupplyRequest
    {
        public string UserName { get; set; }
        public string Group { get; set; }
        //public string Shift { get; set; }
    }

    public class GetSupplyResponse
    {
        public List<CED061300Supply> Data { get; set; }
        public string ParamRange { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetSupplyUpdateRequest
    {
        public string ControlModuleNo { get; set; }
        public string UserName { get; set; }
        public string Mode { get; set; }
    }

    public class GetSupplyUpdateResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }
   
}