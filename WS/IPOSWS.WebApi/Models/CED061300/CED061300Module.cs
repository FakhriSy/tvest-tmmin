﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED061300
{


    public class CED061300ItemModule
    {
        public string Name { get; set; }
        public string Code { get; set; }
    }


    public class CED061300Module
    {
        public string ModuleType { get; set; }
        public string ModuleNo { get; set; }
        public string Flag { get; set; }
        public string ActFinish { get; set; }
        public string ControlModuleNo { get; set; }
        public string PlanSummary { get; set; }
        public string FinishSummary { get; set; }
        public string RemainSummary { get; set; }
        public string ParamRange { get; set; }
        public string ReceivedDt { get; set; }
        public string ReceivedSts { get; set; }
        public string ActPulingBy { get; set; }
    }


    public class CED061300Supply
    {
        public string ModuleType { get; set; }
        public string LineCode { get; set; }
        public string Flag { get; set; }
        public string ActFinish { get; set; }
        public string ControlModuleNo { get; set; }
        public string ParamRange { get; set; }
        public string ReceivedDt { get; set; }
        public string ReceivedSts { get; set; }
        public string ActPulingBy { get; set; }
    }



}