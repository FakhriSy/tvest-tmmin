﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED050600
{
    public class CED050600
    {
    }


    public class GetPickingTypeResponse
    {
        public List<CED050600PickingType> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetPickingRequest
    {
        public string UserName { get; set; }
        public string KanbanHeader { get; set; }
        public string Line { get; set; }
    }

    public class GetPickingResponse
    {
        public string KanbanHeaderCd { get; set; }
        public string Lot { get; set; }
        public string CaseNo { get; set; }
        public string ShooterNo { get; set; }
        public string ShooterType { get; set; }
        public string Picking { get; set; }
        public string Remaining { get; set; }
        public string SupplySts { get; set; }
        public string CrippleSts { get; set; }
        public string ControlModuleNo { get; set; }
        public string Oya { get; set; }
        public string OyaPicking { get; set; }
        public List<ListPickingData> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class ListPickingData
    {
        public string KanbanId { get; set; }
        public string KanbanSts { get; set; }
        public string PartNo { get; set; }
        public string RackAddressCd { get; set; }
    }


    public class GetShooterDataRequest
    {
        public string UserName { get; set; }
        public string ControlModuleNo { get; set; }
        public string Line { get; set; }
    }

    public class GetShooterDataResponse
    {
        //public string ModuleInProcess { get; set; }
        //public string ShooterNo { get; set; }
        public string ShooterSts { get; set; }
        public string ShooterReady { get; set; }
        //public string Lot { get; set; }
        //public string CaseNo { get; set; }
        //public string Picking { get; set; }
        //public string Remaining { get; set; }
        //public List<ListShooterData> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class ListShooterData
    {
        public string KanbanID { get; set; }
    }


    public class PostpickingRequest
    {
        public string userName { get; set; }
        public string kanban3 { get; set; }
        public string kanban4 { get; set; }
        public string totalTime { get; set; }
        public string line { get; set; }
        public string PartNo { get; set; } //[wot.farhan] add variabel Partno
    }

    public class PostpickingResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class GetShooterUpdateResponseData
    {
        public List<BaseMessage> Data { get; set; }
    }

    public class GetShooterUpdateDataRequest
    {
        public string userName { get; set; }
        public string line { get; set; }
        public List<ShooterKanbanData> kanbanId { get; set; }
    }

    public class ShooterKanbanData{
        public string kanbanID { get; set; }
    }

    public class PostProblemPickingUpdateRequest
    {
        public string userName { get; set; }
        public string kanbanID { get; set; }
        public string line { get; set; }
    }

    public class PostProblemPickingUpdateResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }


}