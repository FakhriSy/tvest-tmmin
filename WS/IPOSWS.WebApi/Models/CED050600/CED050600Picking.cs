﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED050600
{
    public class CED050600PickingType
    {
        public string LineCd { get; set; }
        public string LineNm { get; set; }
    }

    public class CED050600PickingData
    {
        public string KanbanHeaderCd { get; set; }
        public string RackAddressCd { get; set; }
        public string SupplySts { get; set; }
        public string Lot { get; set; }
        public string CaseNo { get; set; }
        public string ShooterNo { get; set; }
        public string ShooterType { get; set; }
        public string Picking { get; set; }
        public string Remaining { get; set; }
        public string KanbanId { get; set; }
        public string PartNo { get; set; }
        public string KanbanSts { get; set; }
        public string ControlModuleNo { get; set; }
        public string CrippleSts { get; set; }
        public string Oya { get; set; }
        public string OyaPicking { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class CED050600Shooter
    {
        public string ShooterReady { get; set; }
        public string ShooterSts { get; set; }
    }
}