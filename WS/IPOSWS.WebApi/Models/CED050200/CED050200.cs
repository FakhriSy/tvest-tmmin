﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED050200
{
    public class CED050200
    {
    }


    public class GetShooterRequest
    {
        public string UserName { get; set; }
        public string Shooter { get; set; }
        public string KanbanID { get; set; }
        public string Line { get; set; }
    }
    
    public class GetShooterResponse
    {
        public string ModuleInProcess { get; set; }
        public string ShooterNo { get; set; }
        public string ModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string MaxSupply { get; set; }
        public string TotalPlan  { get; set; }
		public string TotalRemain  { get; set; }
		public string Actual  { get; set; }
        public string ShooterSts { get; set; }
        public string ShooterMsg { get; set; }
        public List<ListKanbanShooter> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class ListKanbanShooter
    {
        public string KanbanID { get; set; }
        public string KanbanSts { get; set; }

        //[wot.daniar] add variabel PickingSts
        public string PickingSts { get; set; }
    }




    public class GetShooterUpdateRequest
    {
        public string userName { get; set; }
        public string status { get; set; }
        public string line { get; set; }
        public List<ShooterKanban> kanbanId { get; set; }
    }

    public class ShooterKanban
    {
        public string kanbanID { get; set; }
    }

    public class GetShooterUpdateResponse
    {
        public List<BaseMessage> Data { get; set; }
    }


    public class GetProblemPickingUpdateRequest
    {
        public string userName { get; set; }
        public string kanbanID { get; set; }
        public string line { get; set; }
    }

    public class GetProblemPickingUpdateResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetProblemBarcodeResponse
    {
        public string ProblemBarcode { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }



    public class GetZone{
        public string ZoneGroupingCd { get; set; }
        public string ZoneGroupingNm { get; set; }
        public string ZoneCd { get; set; }
        public string ZoneNm { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }
    
    public class GetZoneRequest
    {
        public string userName { get; set; }
    }

    public class GetZoneResponse
    {
        public List<ListZone> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class ListZone{
        public string ZoneGroupingCd { get; set; }
        public string ZoneGroupingNm { get; set; }
        public string ZoneCd { get; set; }
        public string ZoneNm { get; set; }
    }

}