﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED050200
{
    public class CED050200Shooter
    {
        public string ModuleInProcess { get; set; }
        public string ShooterNo { get; set; }
        public string ModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string KanbanID { get; set; }
        public string MaxSupply { get; set; }
        public string TotalPlan { get; set; }
        public string TotalRemain { get; set; }
        public string Actual { get; set; }
        public string ShooterSts { get; set; }
        public string ShooterMsg { get; set; }
        public string KanbanSts { get; set; }
        public string ControlModuleNo { get; set; }

        //[wot.daniar] add variabel PickingSts
        public string PickingSts { get; set; }
    }


}