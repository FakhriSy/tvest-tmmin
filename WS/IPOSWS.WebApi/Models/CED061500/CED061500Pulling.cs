﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED061500
{
    public class CED061500Pulling
    {
        public string LineCode      { get; set; }
        public string LotModuleNo   { get; set; }
        public string ActFinish     { get; set; }
        public string Flag          { get; set; }
        public string ControlModuleNo { get; set; }
        public string PlanSummary   { get; set; }
        public string FinishSummary { get; set; }
        public string RemainSummary { get; set; }
        public string ParamRange    { get; set; }
        public string ActPullingBy  { get; set; }
    }


}