﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED061500
{
    public class CED061500
    {
    }


    public class GetPullingRequest
    {
        public string UserName { get; set; }
        public string Group { get; set; }
        //public string Shift { get; set; }
    }

    public class GetPullingResponse
    {
        public string PlanSummary { get; set; }
        public string FinishSummary { get; set; }
        public string RemainSummary { get; set; }
        public string ParamRange { get; set; }
        public List<CED061500Pulling> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetPullingUpdateResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetPullingUpdateRequest
    {
        public string ControlModuleNo { get; set; }
        public string UserName { get; set; }
        public string Mode { get; set; }
    }

}