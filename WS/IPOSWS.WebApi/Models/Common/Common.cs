﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.Common
{
    public class Common
    {
    }


    public class GetVersionResponse
    {
        public string Version { get; set; }
        public string URL { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        
    }

}