﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED010100
{
    public class CED010100
    {
    }

    public class CED010100Shift
    {
        public string ShiftCode { get; set; }
        public string ShiftName { get; set; }
    }

    public class CED010100Line
    {
        public string LineCode { get; set; }
        public string LineName { get; set; }
    }

    public class CED010100Zone
    {
        public string ZoneCode { get; set; }
        public string ZoneName { get; set; }
    }
}