﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models
{
    public class SystemMaster
    {
        public string SystemValueText { get; set; }
        public int SystemValueNum { get; set; }
        public string SystemValueDate { get; set; }
    }
}