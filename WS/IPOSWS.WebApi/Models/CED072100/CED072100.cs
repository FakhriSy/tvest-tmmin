﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IPOSWS.WebApi.Models.CED072100
{
    public class CED072100 {}
    
    public class GetInvestigatingProblemRequest
    {
        public string UserName { get; set; }
        public string PartNo { get; set; }
        public string Supplier { get; set; }
        public string Problem { get; set; }
    }

    public class GetDetailInvestigatingProblemRequest
    {
        public string ProblemId { get; set; }
    }

    public class GetInvestigatingProblemResponse
    {
        public List<PostDataInv> data { get; set; }
        public string TotalCase { get; set; }
        public string TotalPcs { get; set; }
        public string Result {   get; set; }
        public string Message {   get; set; }
    }

    public class CommonInvest
    {
        public GetInvestigatingProblemResponse investigatingPResponse(ApplicationDbContext context,List<PostDataResponseInv> data)
        {
            GetInvestigatingProblemResponse getI = new GetInvestigatingProblemResponse();

            List<PostDataInv> dataList = new List<PostDataInv>();

            getI.Result = "Success";
            if(data.Count == 0)
            {
                getI.Message = "MCEDSTD030I : Data Not Found";

            }
            else
            {
                var totalPcs = 0;
                var _rowsGroup = data.GroupBy(x => x.problem_id).Select(x => x.FirstOrDefault()).ToList();


                foreach (var item in _rowsGroup)
                {
                    totalPcs = 0;
                    PostDataInv outResp = new PostDataInv();
                    outResp.seq = item.seq_problem;
                    outResp.problem_id = item.problem_id;
                    outResp.problem_dt = item.problem_dt;
                    outResp.problem_tm = item.problem_tm;
                    outResp.issuer = item.issuer;
                    outResp.part_no = item.part_no;
                    outResp.unique_no = item.unique_no;
                    outResp.part_name = item.part_name;
                    outResp.supplier = item.supplier;
                    outResp.problem_nm = item.problem_nm;
                    outResp.problem_flag_id = item.problem_flag_id;
                    outResp.problem_thumbnail_1 = item.problem_thumbnail_1;
                    outResp.problem_thumbnail_2 = item.problem_thumbnail_2;
                    outResp.problem_image_1 = item.problem_image_1;
                    outResp.problem_image_2 = item.problem_image_2;
                    outResp.date_sts = item.date_sts;
                    outResp.Kanban = data.Where(y => y.problem_id == item.problem_id).Select(y => new KanbanDetail
                    {
                        seq             = y.seq,
                        kanban_id       = y.part_barcode,
                        kanban_qty      = y.kanban_qty,
                        last_position   = y.kanban_last_position,
                        
                    }).ToList();
                    outResp.Total_Pcs = outResp.Kanban.Sum(x => x.kanban_qty);
                    //outResp.kanban_qty = null;
                    dataList.Add(outResp);
                }

                

                //foreach (PostDataResponseInv model in data)
                //{

                //    var param = new GetDetailInvestigatingProblemRequest();
                //    param.ProblemId = model.problem_id;

                //    var Problemid = new SqlParameter
                //    {
                //        ParameterName = "problemid",
                //        Value = param.ProblemId
                //    };

                //    SqlParameter[] p = new SqlParameter[1];
                //    p[0] = Problemid;

                //    var oDet = context.Database.SqlQuery<KanbanDetail>
                //        ("exec [CED072100W_GetDetailInvestigatingProblem] @problemid", p)
                //        .ToList<KanbanDetail>();

                //    model.Kanban = oDet;
                //    dataList.Add(model);
                //    totalPcs = model.Total_Pcs;
                //}

                getI.TotalCase  = dataList.Count.ToString();
                getI.TotalPcs   = dataList.Sum(x => x.Total_Pcs).ToString();
                //getI.data = dataList;
                
                getI.Message = "MCEDSTD013I : Data retrieved successfully";
           
            }

            getI.data = dataList;

            return getI;
        }
        
    }

}