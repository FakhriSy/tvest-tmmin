﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072100
{

    public class PostDataInv
    {



        public string problem_id { get; set; }
        public string part_barcode { get; set; }
        public string kanban_last_position { get; set; }
        public string issuer { get; set; }
        public string part_name { get; set; }
        public string problem_nm { get; set; }
        public string unique_no { get; set; }
        public string problem_flag_id { get; set; }
        public string supplier_cd { get; set; }
        public string supplier { get; set; }
        public string problem_dt { get; set; }
        public string problem_image_2 { get; set; }
        public string problem_image_1 { get; set; }
        public string problem_thumbnail_1 { get; set; }
        public string problem_thumbnail_2 { get; set; }
        public string part_no { get; set; }
        public string problem_tm { get; set; }
        public string seq { get; set; }
        public int Total_Pcs { get; set; }
        public Int32 date_sts { get; set; }

        public List<KanbanDetail> Kanban { get; set; }
    }

    public class PostDataResponseInv
    {
     


        public string problem_id { get ; set; }
        public string seq_problem { get ; set; }        
        public string part_barcode { get ; set; }
        public Int32 kanban_qty { get ; set; }
        public string kanban_last_position { get ; set; }
        public string issuer { get ; set; }
        public string part_name { get ; set; }
        public string problem_nm { get ; set; }
        public string unique_no { get ; set; }
        public string problem_flag_id { get ; set; }
        public string supplier_cd { get ; set; }
        public string supplier { get ; set; }
        public string problem_dt { get ; set; }
        public string problem_image_2 { get ; set; }
        public string problem_image_1 { get ; set; }
        public string problem_thumbnail_1 { get ; set; }
        public string problem_thumbnail_2 { get ; set; }
        public string part_no { get ; set; }
        public string problem_tm { get ; set; }
        public string seq { get ; set; }
        public string Total_Pcs { get ; set; }
        public Int32 date_sts { get ; set; }
        
        public List<KanbanDetail> Kanban { get ; set ; }
    }

    public class KanbanDetail
    {
        public string seq { get; set; }
        public string problem_id { get ; set ; }
        public Int32 kanban_qty { get ; set ; }
        public string kanban_id { get ; set ; }
        public string last_position { get ; set ; }
       
    }


}