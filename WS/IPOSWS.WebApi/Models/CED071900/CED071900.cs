﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED071900
{
    public class CED071900 {}

    public class GetNewProblemRequest
    {
        public string UserName { get; set; }
        public string PartNo { get; set; }
        public string Supplier { get; set; }
        public string Problem { get; set; }
    }

    public class GetNewProblemResponse
    {
        public List<PostDataResponse> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }


}