﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED071900
{
    public class PostDataResponse
    {
        /**/
        public string ProblemId { get; set; }
        public string ProblemDate { get; set; }
        public string ProblemTime { get; set; }
        public Int32 date_sts { get; set; }
        public string Seq { get; set; }
        public string Issuer { get; set; }
        public string PartNo { get; set; }
        public string UniqueNo { get; set; }
        public string PartName { get; set; }
        public string Supplier { get; set; }
        public string ProblemName { get; set; }
        public string ProblemFlagId { get; set; }
        public string InitialPartBarCode { get; set; }
        public int KanbanQty { get; set; }
        public string ProblemThumbnail1 { get; set; }
        public string ProblemThumbnail2 { get; set; }
        public string ProblemImage1 { get; set; }
        public string ProblemImage2 { get; set; }
        public string TotalCase { get; set; }
        public string TotalPcs { get; set; }
    }
}