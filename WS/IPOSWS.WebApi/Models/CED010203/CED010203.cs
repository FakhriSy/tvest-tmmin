﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED010203
{
    public class CED010203
    {
    }

    public class EngineDataRequest
    {
        public string UserName { get; set; }
        public string CaseMark { get; set; }
    }

    public class EngineDataResponse
    {
        public string ControlModuleNo { get; set; }
        public string LotModuleNo { get; set; }
        public string CaseNo {  get; set; }
        public string RenbanNo { get; set; }
        public string PlanQty { get; set; }
        public string ActualQty { get; set; }
        public string RemainQty { get; set; }
        public string DestinationCd { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }
}