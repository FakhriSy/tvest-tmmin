﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.USER
{
    public class USER
    {
       

        public string RegNo { get ; set ; }
        public string Pin { get ; set ; }
    }

    public class GetUserResponse
    {
        

        public string Status { get ; set ; }
        public string LinkName { get ; set ; }
    }

  

    public class GetUserRequest
  
    {
        

        public string Reg { get ; set ; }
        public string Line { get ; set ; }
    }
}