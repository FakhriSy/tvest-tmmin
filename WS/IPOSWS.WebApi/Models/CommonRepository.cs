﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.Script.Serialization;
using IPOSWS.WebApi.Data.SqlServer.Context;
using IPOSWS.WebApi.Models;
using IPOSWS.WebApi.Models.CED072900;
using IPOSWS.WebApi.Models.CED060400;
using IPOSWS.WebApi.Models.CED072500;
using IPOSWS.WebApi.Models.CED061100;
using IPOSWS.WebApi.Models.CED073100;
using IPOSWS.WebApi.Models.CED061300;
using IPOSWS.WebApi.Models.CED061500;
using IPOSWS.WebApi.Models.CED050200;
using IPOSWS.WebApi.Models.CED050400;
using IPOSWS.WebApi.Models.CED050600;
using IPOSWS.WebApi.Models.CED073700;
using IPOSWS.WebApi.Models.CED060700;
using IPOSWS.WebApi.Models.CED073900A;
using IPOSWS.WebApi.Models.CED060900;
using IPOSWS.WebApi.Models.CED051000;
using IPOSWS.WebApi.Models.CED098;
using IPOSWS.WebApi.Models.Common;

namespace IPOSWS.WebApi.Models
{
    public class CommonRepository
    {
        public ApplicationDbContext _context
        {
            get
            {
                return new ApplicationDbContext(ConfigurationManager.ConnectionStrings["IPOSWSConnection"].ConnectionString);
            }
        }

        private const String QUICK_REPLY_SYNTAX = "{replies:title=";
        private const String QUICK_REPLY_SYNTAX_SUFFIX = "}";
        private const String BUTTON_SYNTAX = "{button:";
        private const String BUTTON_SYNTAX_SUFFIX = "}";
        private const String IMAGE_SYNTAX = "{image:";
        private const String IMAGE_SYNTAX_SUFFIX = "}";
        private const String FORM_SYNTAX = "{form:";
        private const String FORM_SYNTAX_SUFFIX = "}";




        private const String BUBLE_SEPARATOR = "&split&";
        private const String QUICK_REPLY_SEPARATOR = "@===@";
        private const String COMMA = ",";
        private const String EQUAL = "=";

        private const String IMAGE_STICKER_SYSTEM_ID = "IMAGE_STICKER";

        private const String HALLO = "HALLO";
        private const String GESTURING_OK = "GESTURING_OK";
        private const String IDEA_ENTHUSIAS_STAND = "IDEA_ENTHUSIAS_STAND";
        private const String IDEA_ENTHUSIAS = "IDEA_ENTHUSIAS";
        private const String TIPPING_HAND = "TIPPING_HAND";
        private const String CONFUSED_STAND = "CONFUSED_STAND";
        private const String CONFUSED = "CONFUSED";
        private const String CONFUSED_OPEN_HAND = "CONFUSED_OPEN_HAND";
        private const String THANK_YOU_STAND = "THANK_YOU_STAND";
        private const String THANK_YOU = "THANK_YOU";
        private const String SAD_SORRY = "SAD_SORRY";
        private const String HAIR_REVISED = "HAIR_REVISED";
        private const String SPORT_OUTFIT = "SPORT_OUTFIT";

        public String Hallo { get { return HALLO; } }
        public String GesturingOK { get { return GESTURING_OK; } }
        public String IdeaEnthusiasStand { get { return IDEA_ENTHUSIAS_STAND; } }
        public String IdeaEnthusias { get { return IDEA_ENTHUSIAS; } }
        public String TippingHand { get { return TIPPING_HAND; } }
        public String Confused { get { return CONFUSED; } }
        public String ConfusedStand { get { return CONFUSED_STAND; } }
        public String ConfusedOpenHand { get { return CONFUSED_OPEN_HAND; } }
        public String ThankYouHand { get { return THANK_YOU_STAND; } }
        public String ThankYou { get { return THANK_YOU; } }
        public String SadSorry { get { return SAD_SORRY; } }
        public String HairRevised { get { return HAIR_REVISED; } }
        public String SportOutfit { get { return SPORT_OUTFIT; } }
        public String BubleSeparator { get { return BUBLE_SEPARATOR; } }

        StringBuilder quickReply;
        StringBuilder button;


        public ErrorMessage OtputErrorMessage(string message)
        {
            ErrorMessage res = new ErrorMessage();
            res.result = "Failed";
            res.message = message;
            return res;
        }


        public OutputResponse OutputResponse(string output, bool next, bool success, bool repeat, bool agent)
        {
            OutputResponse outResp = new OutputResponse();
            OutputModel outModel = new OutputModel();
            outModel.output = output;
            outResp.value = outModel;
            outResp.next = next;
            outResp.success = success;
            outResp.repeat = repeat;
            outResp.agent = agent;
            return outResp;
        }
        public OutputResponse OutputResponseOther(string output, string model, bool next, bool success, bool repeat, bool agent, Dictionary<string, string> entities)
        {
            OutputResponse outResp = new OutputResponse();
            OutputModel outModel = new OutputModel();
            outModel.output = output;
            outResp.value = outModel;
            outResp.next = next;
            outResp.success = success;
            outResp.repeat = repeat;
            outResp.agent = agent;
            outResp.entities = entities;
            return outResp;
        }

        public string QuickReplyResponse(string title, List<string> value)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                foreach (string item in value)
                {
                    quickReply.Append(item).Append(QUICK_REPLY_SEPARATOR).Append(item).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        public string QuickReplyResponse(string title, List<Tuple<string, string>> tuples)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                foreach (Tuple<string, string> item in tuples)
                {
                    quickReply.Append(item.Item1).Append(QUICK_REPLY_SEPARATOR).Append(item.Item2).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        /*untuk pengkondisian dengan parameter lain. penambahan parameter bawaan untuk kondisi2 berikutnya.*/
        public string QuickReplayResponseOther(string title, List<string> value, string other)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                foreach (string item in value)
                {
                    quickReply.Append(item).Append(QUICK_REPLY_SEPARATOR).Append(item).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        public string FailedResponse()
        {
            string outputTemp = "";
            outputTemp = "Maaf saya tidak mengerti.�";
            return outputTemp;
        }
        public string QuickReplayResponseTest(string title, List<string> value, List<string> valueid)
        {
            quickReply = new StringBuilder();
            quickReply.Append(QUICK_REPLY_SYNTAX);
            quickReply.Append(title).Append(COMMA);
            if (quickReply != null)
            {
                string[] values = value.ToArray();
                string[] valuesid = valueid.ToArray();
                for (int i = 0; i < values.Length; i++)
                {
                    quickReply.Append(values[i]).Append(QUICK_REPLY_SEPARATOR).Append(valuesid[i]).Append(COMMA);
                }
            }

            quickReply.Append(QUICK_REPLY_SYNTAX_SUFFIX);
            string outputTemp = quickReply.ToString();
            int index = outputTemp.LastIndexOf(',');
            outputTemp = outputTemp.Remove(index, 1);
            return outputTemp;
        }
        public string GenerateForm(string formId, List<Tuple<string, string>> listSetValue)
        {
            StringBuilder temp = new StringBuilder();
            temp.Append(FORM_SYNTAX);
            temp.Append(formId);
            string outputTemp = "";
            if (listSetValue.Count > 0)
            {
                temp.Append(COMMA);
                foreach (Tuple<string, string> item in listSetValue)
                {
                    temp.Append(item.Item1);
                    temp.Append(EQUAL);
                    temp.Append(item.Item2);
                    temp.Append(COMMA);
                }
                temp.Append(FORM_SYNTAX_SUFFIX);
                outputTemp = temp.ToString();
                int index = outputTemp.LastIndexOf(',');
                outputTemp = outputTemp.Remove(index, 1);
            }
            return outputTemp;
        }
        public string GetValueTextByIDType(string systemId, string systemType)
        {
            string outputTemp = "";
            string sql = @" SELECT SYSTEM_VALUE_TEXT as SystemValueText" +
                          " FROM TB_M_SYSTEM " +
                          " WHERE SYSTEM_ID = '" + systemId + "'" +
                          " AND SYSTEM_TYPE = '" + systemType + "'"
                          ;
            var o = _context.Database.SqlQuery<SystemMaster>(sql).ToList<SystemMaster>();
            return o.FirstOrDefault().SystemValueText.ToString();
        }
        public string GetSystemMasterValue(string SYS_CAT, string SYS_SUB_CAT, string SYS_CD)
        {
            string outputTemp = "";
            string sql = @" SELECT SYS_VAL as SystemValueText" +
                          " FROM TB_M_SYSTEM " +
                          " WHERE SYS_CAT = '" + SYS_CAT + "'" +
                          " AND SYS_CD = '" + SYS_CD + "'" +
                          " AND SYS_SUB_CAT = '" + SYS_SUB_CAT + "'"
                          ;
            var o = _context.Database.SqlQuery<SystemMaster>(sql).ToList<SystemMaster>();
            return o.FirstOrDefault().SystemValueText.ToString();
        }        
        public string ToRupiah(int angka)
        {
            return String.Format(CultureInfo.CreateSpecificCulture("id-id"), "Rp. {0:N}", angka);
        }
        public string Shorten(string longUrl, string login, string apikey, out string message)
        {
            StringBuilder msg = new StringBuilder();
            var url = string.Format("http://api.bit.ly/v3/shorten?login={1}&apiKey={2}&longUrl={0}", HttpUtility.UrlEncode(longUrl), login, apikey);
            msg.Append("poin = 0 " + url);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(url);

            msg.Append("poin = 1 ");
            try
            {
                WebResponse response = request.GetResponse();
                msg.Append("poin = 2 ");
                using (Stream responseStream = response.GetResponseStream())
                {
                    msg.Append("poin = 3 ");
                    StreamReader reader = new StreamReader(responseStream, Encoding.UTF8);
                    msg.Append("poin = 4 ");
                    JavaScriptSerializer js = new JavaScriptSerializer();
                    msg.Append("poin = 5 ");
                    dynamic jsonResponse = js.Deserialize<dynamic>(reader.ReadToEnd());
                    msg.Append("poin = 6 ");
                    string s = jsonResponse["data"]["url"];
                    msg.Append("success =  " + s);
                    message = msg.ToString();
                    s = s.Replace("http", "https");
                    return s;
                }
            }
            catch (WebException ex)
            {
                msg.Append("ex =  " + ex.Message);
                message = msg.ToString();
                WebResponse errorResponse = ex.Response;
                using (Stream responseStream = errorResponse.GetResponseStream())
                {
                    StreamReader reader = new StreamReader(responseStream, Encoding.GetEncoding("utf-8"));
                    String errorText = reader.ReadToEnd();
                    // log errorText
                }
                throw;
            }
        }

        public GetPOSDataResponse PostDataOutputResponse(List<PostDataResponse> data)
        {
            GetPOSDataResponse outResp = new GetPOSDataResponse();
            outResp.data = data;

            if (data.Count > 0)
            {
                outResp.RefreshParam = data.FirstOrDefault().RefreshParam;
                outResp.RowActive = data.FirstOrDefault().RowActive;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetPOSSummaryResponse PostSummaryOutputResponse(List<POSSumary> data)
        {
            GetPOSSummaryResponse outResp = new GetPOSSummaryResponse();
            outResp.data = data;

            if (data.Count > 0)
            {
                outResp.data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetPOSLineResponse POSLineOutputResponse(List<POSLineResponse> data)
        {
            GetPOSLineResponse outResp = new GetPOSLineResponse();

            if (data.Count > 0)
            {
                outResp.data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetPrepareModuleDataResponse PostPrepareModuleDataOutputResponse(List<PostPrepareModuleDataResponse> data)
        {
            GetPrepareModuleDataResponse outResp = new GetPrepareModuleDataResponse();
            outResp.data = data;

            if (data.Count > 0)
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetCompleteProblemResponse CED072500BGetCompleteProblemResponse(List<CED072500Problem> data)
        {


            GetCompleteProblemResponse outResp = new GetCompleteProblemResponse();
            List<CED072500Problem> result = new List<CED072500Problem>();
            CED072500Problem problem = new CED072500Problem();

            var _rowsGroup = data.GroupBy(x => x.ProblemID).Select(x => x.FirstOrDefault()).ToList();

            if (data.Count == 0)
            {
                outResp.TotalCase = "0";
                outResp.TotalPcs = "0";
                outResp.Problem = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }
            else
            {

                outResp.TotalCase = _rowsGroup.FirstOrDefault().TotalCase;
                outResp.TotalPcs = _rowsGroup.FirstOrDefault().TotalPcs;
                foreach (var item in _rowsGroup)
                {
                    var itemData = new CED072500Problem();
                    itemData.Seq = item.Seq;
                    itemData.ProblemID = item.ProblemID;
                    itemData.ProblemDt = item.ProblemDt;
                    itemData.ProblemTm = item.ProblemTm;
                    itemData.Issuer = item.Issuer;
                    itemData.PartNo = item.PartNo;
                    itemData.UniqueNo = item.UniqueNo;
                    itemData.PartNm = item.PartNm;
                    itemData.Supplier = item.Supplier;
                    itemData.Problem = item.Problem;
                    itemData.ProblemFlagID = item.ProblemFlagID;
                    itemData.TotalPcs = item.TotalPcsKanban;
                    itemData.ProblemThumbnail1 = item.ProblemThumbnail1;
                    itemData.ProblemThumbnail2 = item.ProblemThumbnail2;
                    itemData.ProblemImage1 = item.ProblemImage1;
                    itemData.ProblemImage2 = item.ProblemImage2;
                    itemData.Kanban = data.Where(z => z.ProblemID == item.ProblemID).Select(z => new CED072500Kanban {
                            Seq = z.Seq,
                            KanbanID = z.KanbanID,
                            KanbanQty = z.KanbanQty,
                            Position = z.Position
                        }).ToList();

                    result.Add(itemData);
                }
                outResp.Problem = result;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }












            
            //if (data.Count > 0)
            //{
            //    outResp.TotalCase = data.FirstOrDefault().TotalCase;
            //    outResp.TotalPcs = data.FirstOrDefault().TotalPcs;
            //    outResp.Problem = data.Select(x => new CED072500Problem
            //    {
            //        Seq = x.Seq,
            //        ProblemID = x.ProblemID,
            //        ProblemDt = x.ProblemDt,
            //        ProblemTm = x.ProblemTm,
            //        Issuer = x.Issuer,
            //        PartNo = x.PartNo,
            //        UniqueNo = x.UniqueNo,
            //        PartNm = x.PartNm,
            //        Supplier = x.Supplier,
            //        Problem = x.Problem,
            //        ProblemFlagID = x.ProblemFlagID,
            //        TotalPcs = x.TotalPcsKanban,
            //        ProblemThumbnail1 = x.ProblemThumbnail1,
            //        ProblemThumbnail2 = x.ProblemThumbnail2,
            //        ProblemImage1 = x.ProblemImage1,
            //        ProblemImage2 = x.ProblemImage2,
            //        Kanban = data.Where(y => y.ProblemID == x.ProblemID).Select(y => new CED072500Kanban
            //        {
            //            Seq = y.Seq,
            //            KanbanID = y.KanbanID,
            //            KanbanQty = y.KanbanQty,
            //            Position = y.Position
            //        }).ToList()
            //    }).ToList();

            //    outResp.Result = "Success";
            //    outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            //}
            //else
            //{
            //    outResp.TotalCase = "0";
            //    outResp.TotalPcs = "0";
            //    outResp.Problem = data;
            //    outResp.Result = "Success";
            //    outResp.Message = "MCEDSTD030I : No data found";
            //}

            return outResp;
        }

        public List<GetModuleKanbanCrippleResponse> CED061100BGetModuleKanbanCrippleResponse(List<GetModuleKanbanCrippleResponse> data)
        {



            List<GetModuleKanbanCrippleResponse> result = new List<GetModuleKanbanCrippleResponse>();
            GetModuleKanbanCrippleResponse outResp = new GetModuleKanbanCrippleResponse();

            if (data.Count > 0)
            {
                var _rowsGroup = data.GroupBy(x => x.ControlModuleNo).Select(x => x.FirstOrDefault()).ToList();

                foreach (var item in _rowsGroup)
                {


                    //if (item.CtrlModNo == null)
                    //{
                    //    outResp.CtrlModNo = "";
                    //    outResp.LotModNo = "";
                    //    outResp.CaseNo = "";
                    //    outResp.ModuleSts = "";
                    //    outResp.ModCrippleOpenDt = "";
                    //    outResp.ModCrippleCloseDt = "";
                    //    outResp.Shift = "";
                    //    outResp.KanbanList = data.Select(x => new CED061100Kanban
                    //    {
                    //        KanbanID = x.KanbanID,
                    //        StackingTime = x.StackingTime,
                    //        CrippleFlag = x.CrippleFlag
                    //    }).ToList();
                    //    outResp.Result = item.Result;
                    //    outResp.Message = item.Message;

                    //    result.Add(outResp);
                    //    break;
                    //}

                    outResp.ProdDate = item.ProdDate;
                    outResp.LineStacking = item.LineStacking;
                    outResp.SeqNo = item.SeqNo;
                    outResp.LotModuleNo = item.LotModuleNo;
                    outResp.CaseNo = item.CaseNo;
                    outResp.ShutterNo = item.ShutterNo;
                    outResp.ControlModuleNo = item.ControlModuleNo;
                    outResp.ModuleStatus = item.ModuleStatus;
                    outResp.CycleTime = item.CycleTime;
                    outResp.ContainerSNo = item.ContainerSNo;
                    outResp.ModuleDestCd = item.ModuleDestCd;
                    outResp.Plan = item.Plan;
                    outResp.Actual = item.Actual;
                    outResp.Remain = item.Remain;
                    outResp.Cripple = item.Cripple;
                    outResp.LotPttrn = item.LotPttrn;
                    outResp.ListKanbanID = data.Where(y => y.KanbanID != null).Select(x => new CED061100Kanban
                    {
                        KanbanID = x.KanbanID,
                        BoxNo = x.BoxNo,
                        PartNo = x.PartNo,
                        KanbanType = x.KanbanType,
                        SupplyFlag = x.SupplyFlag,
                        KanbanPartType = x.KanbanPartType,
                        KanbanSts = x.KanbanSts,
                        readyStatus = x.readyStatus,
                        uniqueNo = x.uniqueNo,
                        qty = x.qty
                    }).ToList();
                    outResp.Result = item.Result;
                    outResp.Message = item.Message;

                    result.Add(outResp);
                }
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }


           

            return result;
        }

        public GetCrippleDataResponse CrippleDataOutputResponse(List<CrippleDataResponse> data)
        {
            GetCrippleDataResponse outResp = new GetCrippleDataResponse();

            if (data.Count > 0)
            {
                outResp.data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetCripplePartResponse CED073100BGetCripplePartResponse(List<GetCripplePartResponse> data)
        {
            GetCripplePartResponse outResp = new GetCripplePartResponse();

            var _rowsGroup = data.GroupBy(x => x.ControlModuleNo).Select(x => x.FirstOrDefault()).ToList();

            if (data.Count == 0)
            {               
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }
            else {

                outResp.DateProd = _rowsGroup.FirstOrDefault().DateProd;
                outResp.VanningTime = _rowsGroup.FirstOrDefault().VanningTime;
                outResp.Destination = _rowsGroup.FirstOrDefault().Destination;
                outResp.Renban = _rowsGroup.FirstOrDefault().Renban;
                outResp.ModLot = _rowsGroup.FirstOrDefault().ModLot;
                outResp.CaseNo = _rowsGroup.FirstOrDefault().CaseNo;
                outResp.Cripple = _rowsGroup.FirstOrDefault().Cripple;
                outResp.Readiness = _rowsGroup.FirstOrDefault().Readiness;
                outResp.Status = _rowsGroup.FirstOrDefault().Status;
                outResp.ControlModuleNo = _rowsGroup.FirstOrDefault().ControlModuleNo;
                outResp.ChangedBy = _rowsGroup.FirstOrDefault().ChangedBy;
                outResp.ChangedDt = _rowsGroup.FirstOrDefault().ChangedDt;
                outResp.CripplePart = data.Where(y => y.ControlModuleNo == _rowsGroup.FirstOrDefault().ControlModuleNo).Select(y => new CripplePart
                {
                    PartNoCripple = y.PartNoCripple,
                    BoxNoCripple = y.BoxNoCripple,
                    PartNoReady = y.PartNoReady,
                    BoxNoReady = y.BoxNoReady,

                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            return outResp;
        }
        
        public GetModuleResponse CED061300GetModuleResponse(List<CED061300Module> data)
        {
            GetModuleResponse outResp = new GetModuleResponse();
            CED061300Module module = new CED061300Module();

            if (data.Count > 0)
            {
                outResp.PlanSummary     = data.FirstOrDefault().PlanSummary;
                outResp.FinishSummary   = data.FirstOrDefault().FinishSummary;
                outResp.RemainSummary   = data.FirstOrDefault().RemainSummary;
                outResp.ParamRange      = data.FirstOrDefault().ParamRange;
                outResp.Data = data.Select(x => new CED061300Module
                {
                    ModuleType = x.ModuleType,
                    ModuleNo = x.ModuleNo,
                    Flag = x.Flag,
                    ActFinish = x.ActFinish,
                    ControlModuleNo = x.ControlModuleNo,
                    ReceivedDt = x.ReceivedDt,
                    ReceivedSts = x.ReceivedSts,
                    ActPulingBy = x.ActPulingBy
                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.PlanSummary = "0";
                outResp.FinishSummary = "0";
                outResp.RemainSummary = "0";
                outResp.ParamRange = "0";
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetItemModuleResponse CED061300GetItemModuleResponse(List<CED061300ItemModule> data)
        {
            GetItemModuleResponse outResp = new GetItemModuleResponse();
            CED061300ItemModule module = new CED061300ItemModule();

            if (data.Count > 0)
            {
                outResp.Data = data.Select(x => new CED061300ItemModule
                {
                    Name = x.Name,
                    Code = x.Code,
                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetSupplyResponse CED061300GetSupplyResponse(List<CED061300Supply> data)
        {
            GetSupplyResponse outResp = new GetSupplyResponse();
            CED061300Supply supply = new CED061300Supply();

            if (data.Count > 0)
            {
                outResp.Data = data.Select(x => new CED061300Supply
                {
                    ModuleType = x.ModuleType,
                    LineCode = x.LineCode,
                    Flag = x.Flag,
                    ActFinish = x.ActFinish,
                    ControlModuleNo = x.ControlModuleNo,
                    ReceivedDt = x.ReceivedDt,
                    ReceivedSts = x.ReceivedSts,
                    ActPulingBy = x.ActPulingBy
                }).ToList();
                outResp.ParamRange = data.FirstOrDefault().ParamRange;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetSupplyUpdateResponse CED061300GetSupplyUpdateResponse(GetSupplyUpdateResponse data)
        {
            GetSupplyUpdateResponse outResp = new GetSupplyUpdateResponse();
            CED061300Supply supply = new CED061300Supply();

            if (data!=null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }

        public GetPullingResponse CED061500GetPullingResponse(List<CED061500Pulling> data)
        {
            GetPullingResponse outResp = new GetPullingResponse();
            CED061500Pulling module = new CED061500Pulling();

            if (data.Count > 0)
            {
                outResp.PlanSummary = data.FirstOrDefault().PlanSummary;
                outResp.FinishSummary = data.FirstOrDefault().FinishSummary;
                outResp.RemainSummary = data.FirstOrDefault().RemainSummary;
                outResp.ParamRange = data.FirstOrDefault().ParamRange;
                outResp.Data = data.Select(x => new CED061500Pulling
                {
                    LineCode = x.LineCode,
                    LotModuleNo = x.LotModuleNo,
                    ActFinish = x.ActFinish,
                    Flag = x.Flag,
                    ControlModuleNo = x.ControlModuleNo,
                    ActPullingBy = x.ActPullingBy
                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.PlanSummary = "0";
                outResp.FinishSummary = "0";
                outResp.RemainSummary = "0";
                outResp.ParamRange = "0";
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetPullingUpdateResponse CED061500GetPullingUpdateResponse(GetPullingUpdateResponse data)
        {
            GetPullingUpdateResponse outResp = new GetPullingUpdateResponse();
            CED061500Pulling supply = new CED061500Pulling();

            if (data != null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }

        public GetShooterResponse CED050200GetShooter(List<CED050200Shooter> data)
        {
            GetShooterResponse outResp = new GetShooterResponse();
            CED050200Shooter module = new CED050200Shooter();

            if (data.Count > 0)
            {
                outResp.ModuleInProcess = data.FirstOrDefault().ModuleInProcess;
                outResp.ShooterNo = data.FirstOrDefault().ShooterNo;
                outResp.ModuleNo = data.FirstOrDefault().ModuleNo;
                outResp.CaseNo = data.FirstOrDefault().CaseNo;
                outResp.MaxSupply = data.FirstOrDefault().MaxSupply;
                outResp.TotalPlan = data.FirstOrDefault().TotalPlan;
                outResp.TotalRemain = data.FirstOrDefault().TotalRemain;
                outResp.Actual = data.FirstOrDefault().Actual;
                outResp.ShooterSts = data.FirstOrDefault().ShooterSts;
                outResp.ShooterMsg = data.FirstOrDefault().ShooterMsg;
                outResp.Data = data.Select(x => new ListKanbanShooter
                {
                    KanbanID = x.KanbanID,
                    KanbanSts = x.KanbanSts,
                    PickingSts = x.PickingSts //[wot.daniar] add variabel PickingSts
                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.ModuleInProcess = "";
                outResp.ShooterNo = "";
                outResp.ModuleNo = "";
                outResp.CaseNo = "";
                outResp.MaxSupply = "";
                outResp.TotalPlan = "";
                outResp.TotalRemain = "";
                outResp.Actual = "";
                outResp.ShooterSts = "";
                outResp.Data = new List<ListKanbanShooter>();
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetChartProblemStatusResponse getChartProblemStatus(List<CED098Data> data)
        {
            GetChartProblemStatusResponse outResp = new GetChartProblemStatusResponse();
           // CED050200Shooter module = new CED050200Shooter();

            if (data.Count > 0)
            {
                outResp.Data = data;                
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetChartProblemCategoryReponse getChartProblemCategory(List<CED098DataCat> data)
        {
            GetChartProblemCategoryReponse outResp = new GetChartProblemCategoryReponse();

            if (data.Count > 0)
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";

            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }
            return outResp;
        }


        public GetChartProblemFlagResponse getChartProblemFlag(List<CED098DataFlag> data)
        {
            GetChartProblemFlagResponse outResp = new GetChartProblemFlagResponse();

            if (data.Count > 0)
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";

            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }
            return outResp;
        }


        public GetChartLineProblemResponse getChartProblemLine(List<CED098DataChartPartLine> data)
        {
            GetChartLineProblemResponse outResp = new GetChartLineProblemResponse();

            if (data.Count > 0)
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";

            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }
            return outResp;
        }

        //problemBar
        public GetBarLineProblemResponse getBarProblemLine(List<CED098DataBarLine> data)
        {
            GetBarLineProblemResponse outResp = new GetBarLineProblemResponse();

            if (data.Count > 0)
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";

            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }
            return outResp;
        }



        public GetProblemPickingUpdateResponse CED050200GetProblemPickingUpdateResponse(GetProblemPickingUpdateResponse data)
        {
            GetProblemPickingUpdateResponse outResp = new GetProblemPickingUpdateResponse();

            if (data != null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }


        public GetFlowRackResponse CED050400FlowRack(List<CED050400FlowRack> data)
        {
            GetFlowRackResponse outResp = new GetFlowRackResponse();
            CED050400FlowRack module = new CED050400FlowRack();

            if (data.Count > 0)
            {
                outResp.RackAddressCd = data.FirstOrDefault().RackAddressCd;
                outResp.CurrentStock = data.FirstOrDefault().CurrentStock;
                outResp.MaxStock = data.FirstOrDefault().MaxStock;
                outResp.MaxSupplyBox = data.FirstOrDefault().MaxSupplyBox;
                outResp.Data = data.Select(x => new CED050400FlowRack
                {
                    KanbanID = x.KanbanID,
                    KanbanSts = x.KanbanSts
                }).ToList();


                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;
            }
            else
            {
                outResp.RackAddressCd = "";
                outResp.CurrentStock = "";
                outResp.MaxStock = "";
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public PostSupplyFlowRackResponse CED050400PostSupplyFlowRackResponse(PostSupplyFlowRackResponse data)
        {
            PostSupplyFlowRackResponse outResp = new PostSupplyFlowRackResponse();

            if (data != null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }

        public PostProblemFlowRackResponse CED050400PostProblemFlowRackResponse(PostProblemFlowRackResponse data)
        {
            PostProblemFlowRackResponse outResp = new PostProblemFlowRackResponse();

            if (data != null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }


        public GetPickingTypeResponse CED050600GetPickingType(List<CED050600PickingType> data)
        {
            GetPickingTypeResponse outResp = new GetPickingTypeResponse();
            CED050600PickingType module = new CED050600PickingType();

            if (data.Count > 0)
            {
                outResp.Data = data.Select(x => new CED050600PickingType
                {
                    LineCd = x.LineCd,
                    LineNm = x.LineNm
                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetPickingResponse CED050600GetPickingData(List<CED050600PickingData> data)
        {
            GetPickingResponse outResp = new GetPickingResponse();
            CED050600PickingData module = new CED050600PickingData();

            if (data.Count > 0)
            {
                outResp.KanbanHeaderCd = data.FirstOrDefault().KanbanHeaderCd;
                outResp.Lot         = data.FirstOrDefault().Lot;
                outResp.CaseNo      = data.FirstOrDefault().CaseNo;
                outResp.Picking     = data.FirstOrDefault().Picking;
                outResp.Remaining   = data.FirstOrDefault().Remaining;
                outResp.ShooterNo   = data.FirstOrDefault().ShooterNo;
                outResp.ShooterType = data.FirstOrDefault().ShooterType;
                outResp.SupplySts   = data.FirstOrDefault().SupplySts;
                outResp.ControlModuleNo = data.FirstOrDefault().ControlModuleNo;
                outResp.CrippleSts  = data.FirstOrDefault().CrippleSts;
                outResp.Oya         = data.FirstOrDefault().Oya;
                outResp.OyaPicking  = data.FirstOrDefault().OyaPicking;
                outResp.Data = data.Select(x => new ListPickingData
                {
                    KanbanId = x.KanbanId,
                    KanbanSts = x.KanbanSts,
                    PartNo = x.PartNo,
                    RackAddressCd = x.RackAddressCd
                }).ToList();

                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;
            }
            else
            {

                outResp.Lot = "";
                outResp.CaseNo = "";
                outResp.Picking = "";
                outResp.Remaining = "";
                outResp.Data = new List<ListPickingData>();
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetShooterDataResponse CED050600GetShooter(List<CED050600Shooter> data)
        {
            GetShooterDataResponse outResp = new GetShooterDataResponse();
            CED050600Shooter module = new CED050600Shooter();

            if (data.Count > 0)
            {
                outResp.ShooterSts = data.FirstOrDefault().ShooterSts;
                outResp.ShooterReady = data.FirstOrDefault().ShooterReady;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                //outResp.ModuleInProcess = "";
                //outResp.ShooterNo = "";
                //outResp.Lot = "";
                //outResp.CaseNo = "";
                //outResp.Data = new List<ListShooterData>();
                outResp.ShooterSts = "0";
                outResp.ShooterReady = "0";
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public PostpickingResponse CED050600PostpickingResponse(PostpickingResponse data)
        {
            PostpickingResponse outResp = new PostpickingResponse();

            if (data != null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }

        public GetProblemBarcodeResponse CED050200GetProblemBarcode(List<GetProblemBarcodeResponse> data)
        {
            GetProblemBarcodeResponse outResp = new GetProblemBarcodeResponse();

            if (data.Count > 0)
            {
                outResp.ProblemBarcode = data.FirstOrDefault().ProblemBarcode;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.ProblemBarcode = "";
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetZoneResponse CED050200GetZone(List<GetZone> data)
        {
            GetZoneResponse outResp = new GetZoneResponse();


            if (data.Count > 0)
            {
                outResp.Data = data.Select(x => new ListZone
                {
                    ZoneGroupingCd = x.ZoneGroupingCd,
                    ZoneGroupingNm = x.ZoneGroupingNm,
                    ZoneCd = x.ZoneCd,
                    ZoneNm = x.ZoneNm
                }).ToList();
                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public GetCrippleList CED073700BGetCrippleListResponse(List<GetCrippleListResponse> data)
        {
            GetCrippleList response = new GetCrippleList();

            List<GetCrippleListResponse> Result = new List<GetCrippleListResponse>();

            if (data.Count > 0)
            {
                var _rowsGroup = data.GroupBy(x => x.ControlModuleNo).Select(x => x.FirstOrDefault()).ToList();

                foreach (var item in _rowsGroup)
                {
                    GetCrippleListResponse outResp = new GetCrippleListResponse();

                    outResp.ControlModuleNo = item.ControlModuleNo;
                    outResp.ProdDate = item.ProdDate;
                    outResp.VanningDate = item.VanningDate;
                    outResp.PackingLine = item.PackingLine;
                    outResp.Dest = item.Dest;
                    outResp.Renban = item.Renban;
                    outResp.Lot = item.Lot;
                    outResp.Case = item.Case;
                    outResp.StackingSts = item.StackingSts;
                    outResp.Remaining = item.Remaining;
                    outResp.Detail = data.Where(y => y.ControlModuleNo == item.ControlModuleNo).Select(y => new CED073700Detail
                    {
                        SuppCode = y.SuppCode,
                        SuppName = y.SuppName,
                        PartNo = y.PartNo,
                        KanbanID = y.KanbanID,
                        Address = y.Address,
                        LastLocation = y.LastLocation,
                        Problem = y.Problem,
                        Action = y.Action,
                        EONo = y.EONo,
                        ReceiveFlag = y.ReceiveFlag,
                        ManifestReceiveSts = y.ManifestReceiveSts,
                        ChangedBy = y.ChangedBy,
                        ChangedDt = y.ChangedDt,
                        Status = y.Status
                    }).ToList();


                    Result.Add(outResp);
                }

                response.Result = "Success";
                response.Message = "MCEDSTD013I : Data retrieved successfully";
                response.Data = Result;
            }
            else
            {
                //GetCrippleListResponse outResp = new GetCrippleListResponse();

                //outResp.Result = "Failed";
                //outResp.Message = "MCEDSTD030I : No data found";

                //Result.Add(outResp);

                response.Data = data;
                response.Result = "Success";
                response.Message = "MCEDSTD030I : No data found";

            }

            return response;
        }

        public GetActionResponse CED073700BGetActionResponse(List<GetAction> data)
        {
            GetActionResponse outResp = new GetActionResponse();
            GetAction action = new GetAction();

            if (data.Count > 0)
            {
                outResp.Data = data.Select(x => new GetAction
                {
                    ListAction = x.ListAction
                }).ToList();

                outResp.Result = "Success";
                outResp.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                outResp.Data = data;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }

        public List<GetNextStackingDataResponse> CED060700BGetNextStackingDataResponse(List<GetNextStackingData> data)
        {
            List<GetNextStackingDataResponse> result = new List<GetNextStackingDataResponse>();

            var _rowsGroup = data.GroupBy(x => x.ControlModuleNo).Select(x => x.FirstOrDefault()).ToList();

            foreach (var item in _rowsGroup)
            {
                GetNextStackingDataResponse outResp = new GetNextStackingDataResponse();

                if (item.ControlModuleNo == null)
                {
                    outResp.Result = item.Result;
                    outResp.Message = item.Message;

                    result.Add(outResp);
                    break;
                }

                outResp.ProdDate = item.ProdDate;
                outResp.LineStacking = item.LineStacking;
                outResp.SeqNo = item.SeqNo;
                outResp.LotModuleNo = item.LotModuleNo;
                outResp.CaseNo = item.CaseNo;
                outResp.ShutterNo = item.ShutterNo;
                outResp.ControlModuleNo = item.ControlModuleNo;
                outResp.ModuleStatus = item.ModuleStatus;
                outResp.CycleTime = item.CycleTime;
                outResp.ContainerSNo = item.ContainerSNo;
                outResp.ModuleDestCd = item.ModuleDestCd;
                outResp.Plan = item.Plan;
                outResp.Actual = item.Actual;
                outResp.Remain = item.Remain;
                outResp.Cripple = item.Cripple;
                outResp.LotPttrn = item.LotPttrn;
                outResp.caseMark = item.caseMark;
                outResp.contentList = item.contentList;
                outResp.ModType = item.ModType;
                outResp.ListKanbanID = data.Where(x => x.ControlModuleNo.ToString() == item.ControlModuleNo.ToString()).Select(x => new CED060700Kanban
                {
                    KanbanID = x.KanbanID,
                    BoxNo    = x.BoxNo,
                    PartNo   = x.PartNo,
                    KanbanType = x.KanbanType,
                    SupplyFlag = x.SupplyFlag,
                    KanbanPartType = x.KanbanPartType,
                    kanbanSts = x.kanbanSts,
                    UniqueNo = x.UniqueNo,
                    Qty = x.Qty
                }).ToList();
                outResp.Result = item.Result;
                outResp.Message = item.Message;

                result.Add(outResp);
            }

            return result;
        }

        public CED073900AGetDefectResponse CED073900GetDefect(List<CED073900ADefectResponse> data)
        {
           
            CED073900AGetDefectResponse res = new CED073900AGetDefectResponse();
            List<CED073900AGetDefect> result = new List<CED073900AGetDefect>();

            var _rowsGroup = data.GroupBy(x => x.DefectCd).Select(x => x.FirstOrDefault()).ToList();

            foreach (var item in _rowsGroup)
            {
                CED073900AGetDefect outResp = new CED073900AGetDefect();

                outResp.DefectCd = item.DefectCd;
                outResp.DefectNm = item.DefectNm;
                outResp.SubData = data.Where(x => x.DefectCd.ToString() == item.DefectCd.ToString()).Select(x => new CED073900AGetSubDefect
                {
                    SubDefectCd = x.SubDefectCd,
                    SubDefectNm = x.SubDefectNm,
                }).ToList();

                result.Add(outResp);
            }

            if (result.Count > 0)
            {
                res.Data = result;
                res.Result = "Success";
                res.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                res.Data = result;
                res.Result = "Success";
                res.Message = "MCEDSTD030I : No data found";
            }

            return res;
        }


        //public CED073900AGetSubDefectResponse CED073900GetSubDefect(List<CED073900AGetSubDefect> data)
        //{
        //    CED073900AGetSubDefectResponse outResp = new CED073900AGetSubDefectResponse();
        //    CED073900AGetSubDefect module = new CED073900AGetSubDefect();

        //    if (data.Count > 0)
        //    {
        //        outResp.Data = data.Select(x => new CED073900AGetSubDefect
        //        {
        //            DefectCd = x.DefectCd,
        //            SubDefectCd = x.SubDefectCd,
        //            SubDefectNm = x.SubDefectNm
        //        }).ToList();

        //        outResp.Result = "Success";
        //        outResp.Message = "MCEDSTD013I : Data retrieved successfully";
        //    }
        //    else
        //    {
        //        outResp.Data = data;
        //        outResp.Result = "Success";
        //        outResp.Message = "MCEDSTD030I : No data found";
        //    }

        //    return outResp;
        //}

        public PostProblemPickingUpdateResponse CED050600GetProblemPickingUpdateResponse(PostProblemPickingUpdateResponse data)
        {
            PostProblemPickingUpdateResponse outResp = new PostProblemPickingUpdateResponse();

            if (data != null)
            {
                outResp.Result = data.Result;
                outResp.Message = data.Message;
            }

            return outResp;
        }

        public List<GetModuleKanbanResponse> CED060900BGetModuleKanbanResponse(List<GetModuleKanbanResponse> data)
        {
            List<GetModuleKanbanResponse> result = new List<GetModuleKanbanResponse>();

            var _rowsGroup = data.GroupBy(x => x.CtrlModNo).Select(x => x.FirstOrDefault()).ToList();

            foreach (var item in _rowsGroup)
            {
                GetModuleKanbanResponse outResp = new GetModuleKanbanResponse();

                if (item.CtrlModNo == null)
                {
                    outResp.CtrlModNo = "";
                    outResp.LotModNo = "";
                    outResp.CaseNo = "";
                    outResp.ModuleSts = "";
                    outResp.ModOpenDt = "";
                    outResp.ModCloseDt = "";
                    outResp.Shift = "";
                    outResp.KanbanList = data.Select(x => new CED060900Kanban
                    {
                        KanbanID = x.KanbanID,
                        StackingTime = x.StackingTime,
                        CrippleSts = x.CrippleSts,
                        PartNo = x.PartNo,
                        BoxNo = x.BoxNo
                    }).ToList();
                    outResp.Result = item.Result;
                    outResp.Message = item.Message;

                    result.Add(outResp);
                    break;
                }

                outResp.CtrlModNo = item.CtrlModNo;
                outResp.LotModNo = item.LotModNo;
                outResp.CaseNo = item.CaseNo;
                outResp.ModuleSts = item.ModuleSts;
                outResp.ModOpenDt = item.ModOpenDt;
                outResp.ModCloseDt = item.ModCloseDt;
                outResp.Shift = item.Shift;
                outResp.KanbanList = data.Where(x => x.CtrlModNo.ToString() == item.CtrlModNo.ToString()).Select(x => new CED060900Kanban
                {
                    KanbanID = x.KanbanID,
                    StackingTime = x.StackingTime,
                    CrippleSts = x.CrippleSts,
                    PartNo = x.PartNo,
                    BoxNo = x.BoxNo
                }).ToList();
                outResp.Result = item.Result;
                outResp.Message = item.Message;

                result.Add(outResp);
            }

            return result;
        }


        public GetKanbanStackingResponse CED060700BGetKanbanStackingResponse(List<GetKanbanStackingResponse> data)
        {
            
            GetKanbanStackingResponse outResp = new GetKanbanStackingResponse();

            if (data.Count > 0)
            {
                outResp.Kanban3     = data.FirstOrDefault().Kanban3;
                outResp.UniqueNo    = data.FirstOrDefault().UniqueNo;
                outResp.BoxNo       = data.FirstOrDefault().BoxNo;
                outResp.PartNo      = data.FirstOrDefault().PartNo;
                outResp.Kanban2     = data.FirstOrDefault().Kanban2;
                outResp.Result      = data.FirstOrDefault().Result;
                outResp.Message     = data.FirstOrDefault().Message;
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message     = "MCEDSTD030I : No data found";
            }

            return outResp;

        }



        public GetLoginLeaderResponse CED060700BGetLoginLeaderResponse(List<GetLoginLeaderResponse> data)
        {

            GetLoginLeaderResponse outResp = new GetLoginLeaderResponse();

            if (data.Count > 0)
            {
                outResp.RegNo = data.FirstOrDefault().RegNo;
                outResp.Pin = data.FirstOrDefault().Pin;
                outResp.FirstName = data.FirstOrDefault().FirstName;
                outResp.LastName = data.FirstOrDefault().LastName;
                outResp.Username = data.FirstOrDefault().Username;
                outResp.Menu = data.FirstOrDefault().Menu;
                outResp.Function = data.Where(x => x.RegNo.ToString() == data.FirstOrDefault().RegNo).Select(x => new GetFunctionData
                {
                    ModuleId = x.ModuleId,
                    FunctionId = x.FunctionId,
                    FunctionNm = x.FunctionNm
                }).ToList();


                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;

            }
            else
            {
                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;
            }

            return outResp;

        }

        public GetLoginStackingResponse CED060700BGetLoginStackingResponse(List<GetLoginStackingResponse> data)
        {

            GetLoginStackingResponse outResp = new GetLoginStackingResponse();

            if (data.Count > 0)
            {
                outResp.ZoneGroupingCd = data.FirstOrDefault().ZoneGroupingCd;
                outResp.ZoneGroupingNm = data.FirstOrDefault().ZoneGroupingNm;
                outResp.ZoneCd = data.FirstOrDefault().ZoneCd;
                outResp.ZoneNm = data.FirstOrDefault().ZoneNm;
                outResp.RegNo = data.FirstOrDefault().RegNo;
                outResp.Pin = data.FirstOrDefault().Pin;
                outResp.FirstName = data.FirstOrDefault().FirstName;
                outResp.LastName = data.FirstOrDefault().LastName;
                outResp.Username = data.FirstOrDefault().Username;
                outResp.Menu = data.FirstOrDefault().Menu;
                outResp.CrippleCd = data.FirstOrDefault().CrippleCd;
                outResp.Function = data.Where(x => x.RegNo.ToString() == data.FirstOrDefault().RegNo).Select(x => new GetFunctionData
                {
                    ModuleId = x.ModuleId,
                    FunctionId = x.FunctionId,
                    FunctionNm = x.FunctionNm
                }).ToList();


                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;

            }
            else
            {
                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;
            }

            return outResp;

        }


        public PostModuleCloseResponse PostModuleCloseResponse(List<PostModuleCloseResponse> message)
        {
            PostModuleCloseResponse outResp = new PostModuleCloseResponse();


            outResp.listMessage = message.Select(x => new BaseMessage
            {
                Result = x.Result,
                Message = x.Message
            }).ToList();

            if (message.FirstOrDefault().Result == "Failed")
            {
                outResp.Result = "Failed";
                outResp.Message = "MCEDSTD030E : Failed to update data, Please Re-Scan Content List";
            }
            else
            {
                outResp.PlanCt = message.FirstOrDefault().PlanCt;
                outResp.ActCt = message.FirstOrDefault().ActCt;
                outResp.StatusStacking = message.FirstOrDefault().StatusStacking;
                outResp.TimeStacking = message.FirstOrDefault().TimeStacking;
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD029I : The data are successfully updated";
            }

            return outResp;
        }


        public GetVersionResponse CommonGetVersion(GetVersionResponse data)
        {

            GetVersionResponse res = new GetVersionResponse();
            if (data!=null)
            {
                res.URL = data.URL;
                res.Version = data.Version;
                res.Result = "Success";
                res.Message = "MCEDSTD013I : Data retrieved successfully";
            }
            else
            {
                res.Result = "Success";
                res.Message = "MCEDSTD030I : No data found";
            }
            return res;
        }

        //ADD [WOT]
        public GetSequenceListApiResponse CED051000BGetSequenceListApi(List<GetSequenceListApiData> data)
        {
            GetSequenceListApiResponse outResp = new GetSequenceListApiResponse();
            if (data.Count > 0)
            {
                outResp.ListSequenceData = data.Select(x => new GetSequenceListApiData
                {
                    TIMING = x.TIMING,
                    DOLLY_1 = x.DOLLY_1,
                    DOLLY_2 = x.DOLLY_2,
                    DOLLY_3 = x.DOLLY_3,
                    DOLLY_4 = x.DOLLY_4,
                    DOLLY_5 = x.DOLLY_5,
                    DOLLY_6 = x.DOLLY_6,
                    DOLLY_7 = x.DOLLY_7,
                    DOLLY_8 = x.DOLLY_8,
                    prodDt = x.prodDt,
                    Plan = x.Plan,
                    Finish = x.Finish,
                    Remaining = x.Remaining,
                    ModuleSts = x.ModuleSts,
                    PlanCt = x.PlanCt,
                    Seq = x.Seq,
                    Module = x.Module,
                    Case = x.Case,
                    Line = x.Line,
                    SeqRemark = x.SeqRemark,
                    ChangedBy = x.ChangedBy,
                    ChangedDt = x.ChangedDt,
                    readySts = x.readySts,
                    ControlModuleNo = x.ControlModuleNo
                }).ToList();
                outResp.Result = data.FirstOrDefault().Result;
                outResp.Message = data.FirstOrDefault().Message;
            }
            else
            {
                outResp.Result = "Success";
                outResp.Message = "MCEDSTD030I : No data found";
            }

            return outResp;
        }
        //Last Code

    }    
}