﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072900
{
    public class PostDataResponse
    {
        public string No { get; set; }
        public string SeqNo { get; set; }
        public string PlanSeqNo { get; set; }
        public string ProdDt { get; set; }
        public string StackingTime { get; set; }
        public string PlanCT { get; set; }
        public string ActStart { get; set; }
        public string ActFinish { get; set; }
        public string ActCT { get; set; }
        public string Destination { get; set; }
        public string Renban { get; set; }
        public string ModLot { get; set; }
        public string CaseNo { get; set; }
        public string ModType { get; set; }
        public string POS { get; set; }
        public string ControlModuleNo { get; set; }
        public string PlanKbn { get; set; }
        public string ShooterNo { get; set; }
        public string Readiness { get; set; }
        public string Progress { get; set; }
        public string PartStatus { get; set; }
        public string ModuleStatus { get; set; }
        public string SkipFlag { get; set; }
        public string Status { get; set; }        
        public string PlanSf { get; set; }
        public string PlanJdt { get; set; }
        public string PlanSht { get; set; }        
        public string ReadinesSf { get; set; }
        public string ReadinesJdt { get; set; }
        public string ReadinesSht { get; set; }
        public string Background { get; set; }
        public string RefreshParam { get; set; }
        public string RowActive { get; set; }        
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        
    }
}