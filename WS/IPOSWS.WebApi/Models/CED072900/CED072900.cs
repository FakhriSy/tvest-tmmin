﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072900
{
    public class CED072900
    {
    }

    public class GetPOSSummaryRequest
    {
        public string UserName { get; set; }
        public string Shift { get; set; }
        public string LineCd { get; set; }
        public string ProdDt { get; set; }
    }

    public class GetPOSSummaryResponse
    {
        public List<POSSumary> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class POSSumary {
        public string ZoneName { get; set; }
        public string Color { get; set; }
        public string ZoneCd { get; set; }
    }

    public class GetPOSDataRequest
    {
        public string UserName { get; set; }
        public string ZoneCd { get; set; }
        public string Shift { get; set; }
        public string ProdDt { get; set; }
    }

    public class GetPOSDataResponse
    {
        public List<PostDataResponse> data { get; set; }
        public string RefreshParam { get; set; }
        public string RowActive { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetPOSUpdateRequest
    {
        public string Username { get; set; }
        public string ControlModuleNo { get; set; }
        public string SkipControlModuleNo { get; set; }
        public string Action { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string SkipChangedBy { get; set; }
        public string SkipChangedDt { get; set; }
    }

    public class GetPOSUpdateResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetPOSLineResponse
    {
        public List<POSLineResponse> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class POSLineResponse
    {
        public string LineName { get; set; }
        public string LineCd { get; set; }
    }
}