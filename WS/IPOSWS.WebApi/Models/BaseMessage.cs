﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models
{
    public class BaseMessage
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

}