﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models
{
    public class ErrorMessage
    {
        public string result { get; set; }
        public string message { get; set; }
    }

}