﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED061100
{
    public class CED061100
    {
    }

    public class GetModuleKanbanCrippleRequest
    {
        public string UserName { get; set; }
        public string Shift { get; set; }
    }

    public class GetModuleKanbanCrippleResponse
    {
        public string ProdDate { get; set; }
        public string LineStacking { get; set; }
        public string SeqNo { get; set; }
        public string LotModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string ShutterNo { get; set; }
        public string ControlModuleNo { get; set; }
        public string ModuleStatus { get; set; }
        public int? CycleTime { get; set; }
        public int? Plan { get; set; }
        public int? Actual { get; set; }
        public int? Remain { get; set; }
        public int? Cripple { get; set; }
        public string ContainerSNo { get; set; }
        public string ModuleDestCd { get; set; }
        public string KanbanID { get; set; }
        public string KanbanType { get; set; }
        public string PartNo { get; set; }
        public string BoxNo { get; set; }
        public string SupplyFlag { get; set; }
        public string LotPttrn { get; set; }
        public string KanbanPartType { get; set; }
        public string KanbanSts { get; set; }
        public string readyStatus { get; set; }
        public string uniqueNo { get; set; }
        public string qty { get; set; }        

        public List<CED061100Kanban> ListKanbanID { get; set; }
        //public string CtrlModNo { get; set; }
        //public string LotModNo { get; set; }
        //public string CaseNo { get; set; }
        //public string ModuleSts { get; set; }
        //public string ModCrippleOpenDt { get; set; }
        //public string ModCrippleCloseDt { get; set; }
        //public string Shift { get; set; }
        //public string KanbanID { get; set; }
        //public string StackingTime { get; set; }
        //public string CrippleFlag { get; set; }
        //public List<CED061100Kanban> KanbanList { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetModuleKanbanCripple
    {
        public string CtrlModNo { get; set; }
        public string LotModNo { get; set; }
        public string CaseNo { get; set; }
        public string ModuleSts { get; set; }
        public string ModCrippleOpenDt { get; set; }
        public string ModCrippleCloseDt { get; set; }
        public string Shift { get; set; }
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleFlag { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class PostKanbanCrippleRequest
    {
        public string CtrlModNo { get; set; }
        public string KanbanID { get; set; }
        public string UserName { get; set; }
        public string Line { get; set; }
    }

    public class SubmitKanbanCrippleStackingRequest
    {
        public string CtrlModNo { get; set; }
        public string UserName { get; set; }
        public string GunId { get; set; }
        public string Line { get; set; }
        //public List<listKanban> listKanban { get; set; } 
    }

    public class listKanban
    {
        public string KanbanID { get; set; }
        //public string Kanban2 { get; set; }
        //public string Kanban3 { get; set; }
        public string StackingDate { get; set; }
        public string kanbanProblem { get; set; }
        public string StackingFlag { get; set; }
    }

    public class SubmitKanbanCrippleStackingResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class SubmitOpenModuleCrippleRequest
    {
        public string CaseMark { get; set; }
        public string UserName { get; set; }
    }

    public class SubmitOpenModuleCrippleResponse
    {       
        public string Result { get; set; }
        public string Message { get; set; }
    }

    //public class CED061100Kanban
    //{
    //    public string KanbanID { get; set; }
    //    public string KanbanType { get; set; }
    //    public string SupplyFlag { get; set; }
    //    public string BoxNo { get; set; }
    //    public string PartNo { get; set; }
    //}

    public class SubmitCloseModuleCrippleRequest
    {
        public string CtrlModNo { get; set; }
        public DateTime? CloseModuleTime { get; set; }
        public string UserName { get; set; }
    }

    public class SubmitCloseModuleCrippleResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class PostProblemStackingCrippleUpdateRequest
    {
        public string userName { get; set; }
        public string kanbanID { get; set; }
        public string line { get; set; }
    }
}