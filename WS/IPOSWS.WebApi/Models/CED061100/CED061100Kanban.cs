﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED061100
{
    public class CED061100Kanban
    {        
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleFlag { get; set; }
        public string KanbanType { get; set; }
        public string SupplyFlag { get; set; }
        public string BoxNo { get; set; }
        public string PartNo { get; set; }
        public string KanbanPartType { get; set; }
        public string KanbanSts { get; set; }
        public string readyStatus { get; set; }
        public string uniqueNo { get; set; }
        public string qty { get; set; }        

    }
}