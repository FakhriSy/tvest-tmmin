﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073700
{
    public class CED073700Header
    {
        public string ControlModuleNo { get; set; }
        public string ProdDate { get; set; }
        public string VanningDate { get; set; }
        public string PackingLine { get; set; }
        public string Dest { get; set; }
        public string Renban { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string StackingSts { get; set; }
        public string Remaining { get; set; }
        public List<CED073700Detail> Detail { get; set; }
        //DETAIL
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string PartNo { get; set; }
        public string KanbanID { get; set; }
        public string Address { get; set; }
        public string LastLocation { get; set; }
        public string Problem { get; set; }
        public string Action { get; set; }
        public string EONo { get; set; }
        public string ManifestReceiveSts { get; set; }
    }
}