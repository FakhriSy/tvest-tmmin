﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073700
{
    public class CED073700Detail
    {
        public string ControlModuleNo { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string PartNo { get; set; }
        public string KanbanID { get; set; }
        public string Address { get; set; }
        public string LastLocation { get; set; }
        public string Problem { get; set; }
        public string Action { get; set; }
        public string EONo { get; set; }
        public string ReceiveFlag { get; set; }
        public string ManifestReceiveSts { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string Status { get; set; }
    }
}