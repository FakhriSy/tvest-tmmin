﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073700
{
    public class CED073700
    {
    }

    public class GetCrippleListRequest
    {
        public string ProdDate { get; set; }
        public string VanningDate { get; set; }
        public string PackingLine { get; set; }
    }

    public class GetCrippleList{
        public string Result { get; set; }
        public string Message { get; set; }
        public List<GetCrippleListResponse> Data { get; set; }
    }

    public class GetPackingLine
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public List<GetPackingLineResponse> Data { get; set; }
    }

    public class GetPackingLineResponse
    {
        public string ZoneCd { get; set; }
        public string ZoneNm { get; set; }
    }



    public class GetCrippleListResponse
    {
        //public List<CED073700Header> Header { get; set; }
        public string ControlModuleNo { get; set; }
        public string ProdDate { get; set; }
        public string VanningDate { get; set; }
        public string PackingLine { get; set; }
        public string Dest { get; set; }
        public string Renban { get; set; }
        public string Lot { get; set; }
        public string Case { get; set; }
        public string StackingSts { get; set; }
        public string Remaining { get; set; }
        public string ReceiveFlag { get; set; }        
        public List<CED073700Detail> Detail { get; set; }
        //DETAIL
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string PartNo { get; set; }
        public string KanbanID { get; set; }
        public string Address { get; set; }
        public string LastLocation { get; set; }
        public string Problem { get; set; }
        public string Action { get; set; }
        public string Status { get; set; }
        public string EONo { get; set; }
        public string ManifestReceiveSts { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetActionRequest
    {
        //public List<GetAction> Data { get; set; }
    }

    public class GetActionResponse
    {
        public List<GetAction> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetAction
    {
        public string ListAction { get; set; }
    }

    public class PostCompleteRequest
    {
        public string ControlModuleNo { get; set; }
        public string KanbanID { get; set; }
        public string TransactionType { get; set; }
        public string CrippleProblem { get; set; }
        public string CrippleAction { get; set; }
        public string CrippleEONo { get; set; }
        public string UserName { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
    }

    public class PostCompleteResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string ManifestReceiveSts { get; set; }
        public string Status { get; set; }
    }

}