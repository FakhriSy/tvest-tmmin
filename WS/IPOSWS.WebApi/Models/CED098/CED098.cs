﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace IPOSWS.WebApi.Models.CED098
{
    public class CED098Data
    {
        public string ProblemSts { get; set; }
        public int TotalProbStatus { get; set; }
    }

    public class GetChartProblemStatusRequest
    {
        public string Username { get; set; }
        public string CodeReport { get; set; }
    }

    public class GetChartProblemStatusResponse
    {
        public List<CED098Data> Data { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }

    public class CED098DataCat
    {
        public string defectCd { get; set; }
        public string problem_cateory { get; set; }
        public int totalDefect { get; set; }
        public int totalProblemCategory {get; set;}
    }

    public class GetChartProblemCategoryReponse
    {
        public List<CED098DataCat> Data { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }

    public class CED098DataFlag
    {
        public string problem_flag_id  { get; set; }
        public int total_flag  { get; set; }
    }

    public class GetChartProblemFlagResponse
    {
        public List<CED098DataFlag> Data { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }

    public class CED098DataChartPartLine
    {
        public string partline_cd {get; set;}
        public int total_partline_category {get; set;}
    }

    public class GetChartLineProblemResponse
    {
        public List<CED098DataChartPartLine> Data { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }

    public class CED098DataBarLine
    {
        public string nama_supplier { get; set; }
        public string defect_cd { get; set; }
        public int total_defect_per_sup { get; set; }
    }


    public class GetBarLineProblemResponse
    {
        public List<CED098DataBarLine> Data { get; set; }
        public string Message { get; set; }
        public string Result { get; set; }
    }

}