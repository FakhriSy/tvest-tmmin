﻿using IPOSWS.WebApi.Data.SqlServer.Context;
using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Linq;
using System.Runtime.Remoting.Contexts;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace IPOSWS.WebApi.Models.CED072100
{
    public class CED098 {}
    
    public class GetChartProblemStatusRequest
    {
        public string UserName { get; set; }
        public string code_report { get; set; }
    }

    public class GetChartProblemCategoryRequest
    {
        public string UserName { get; set; }
        public string code_report { get; set; }
    }

    public class GetChartProblemFlagRequest
    {
        public string UserName { get; set; }
        public string code_report { get; set; }
    }

    public class GetChartProblemPartLineRequest
    {
        public string UserName { get; set; }
        public string code_report { get; set; }
    }

    public class GetChartBarRequest
    {
        public string UserName { get; set; }
        public string code_report { get; set; }
    }
    public class GetChartProblemStatusResponse
    {
        public string Problem_Sts { get; set; }
        public string Total_Prob_Status { get; set; }
        
    }

    public class GetChartProblemCategoryResponse
    {
        public string defect_cd { get; set; }
        public string problem_category { get; set; }
        public int total_defect { get; set; }
    }

    public class GetChartProblemFlagResponse
    {
        public string problem_flag_id { get; set; }
        public int total_flag { get; set; }
    }

    public class GetChartProblemPartLine
    {
        public string partline_cd { get; set; }
        public int total_partline_category { get; set; }
    }

    public class GetBarChartResponse
    {
        public string nama_supplier { get; set; }
        public string defect_cd { get; set; }
        public int total_defect_per_sup { get; set; }
    }


}