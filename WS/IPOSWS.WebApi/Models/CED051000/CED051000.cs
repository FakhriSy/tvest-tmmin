﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED051000
{
    public class CED051000
    {
    }

    public class GetSequenceDataRequest
    {
        public string UserName { get; set; }
        public int Shift { get; set; }
        public string ProdDate { get; set; }
    }

    public class GetSequenceDataResponse
    {
        //[WOT] Add Parameter Next
        public string NextSeq { get; set; }
        public string NextModule { get; set; }
        public string NextCase { get; set; }
        public string NextLine { get; set; }
        public string NextControlModuleNo { get; set; }
        public string NextChangedDt { get; set; }
        public string NextChangedBy { get; set; }
        public string prodDt { get; set; }
        public int? Plan { get; set; }
        public int? Finish { get; set; }
        public int? Remaining { get; set; }
        public string ModuleSts { get; set; }
        public string PlanCt { get; set; }
        public string Seq { get; set; }
        public string Module { get; set; }
        public string Case { get; set; }
        public string Line { get; set; }
        public string SeqRemark { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string readySts { get; set; }
        //public List<GetSequenceData> data { get; set; }
        public string ControlModuleNo { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }

    
    }

    //public class GetSequenceData
    //{
    //    public int Plan { get; set; }
    //    public int Finish { get; set; }
    //    public int Remaining { get; set; }
    //    public string Sequence { get; set; }
    //    public string Module { get; set; }
    //    public string CaseNo { get; set; }
    //    public string Line { get; set; }
    //    public string SeqRemark { get; set; }
    //}

    public class PostSupplyStartJundateStatusRequest
    {
        public string Seq { get; set; }
        public string Module { get; set; }
        public string Case { get; set; }
        public string Line { get; set; }
        public string UserName { get; set; }
        public int Shift { get; set; }
        public string ControlModuleNo { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }       
    }

    public class PostSupplyStartJundateStatusResponse
    {
        public string Module  { get; set; }
        public string CaseNo  { get; set; }
        public string Line    { get; set; }
        public string Dolly1  { get; set; }
        public string Dolly2  { get; set; }
        public string Dolly3  { get; set; }
        public string Dolly4  { get; set; }
        public string Dolly5 { get; set; }
        public string Dolly6 { get; set; }
        public string Dolly7 { get; set; }
        public string Dolly8 { get; set; }
        public string Dolly9 { get; set; }
        public string ShooterNo { get; set; }
        public string Dolly1Ready { get; set; }
        public string Dolly2Ready { get; set; }
        public string Dolly3Ready { get; set; }
        public string Dolly4Ready { get; set; }
        public string Dolly5Ready { get; set; }
        public string Dolly6Ready { get; set; }
        public string Dolly7Ready { get; set; }
        public string Dolly8Ready { get; set; }
        public string Dolly9Ready { get; set; }
        public string ShooterNoReady { get; set; }
        public string ActStart{ get; set; }
        public int? CycleTime { get; set; }
        public string Result  { get; set; }
        public string Message { get; set; }
    }

    public class PostSupplyEndStatusRequest
    {
        public string Seq { get; set; }
        public string Module { get; set; }
        public string Case { get; set; }
        public string Line { get; set; }
        public string UserName { get; set; }
        public int Shift { get; set; }
        public string ControlModuleNo { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
    }

    public class PostSupplyEndStatusResponse
    {
        public string ActualCycleStatus { get; set; }
        public int? Timming { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    //NEW API [WOT]
    public class GetSequenceListApiData
    {
        //Item Dolly
        public string DOLLY_1 { get; set; }
        public string DOLLY_2 { get; set; }
        public string DOLLY_3 { get; set; }
        public string DOLLY_4 { get; set; }
        public string DOLLY_5 { get; set; }
        public string DOLLY_6 { get; set; }
        public string DOLLY_7 { get; set; }
        public string DOLLY_8 { get; set; }
        public string DOLLY_9 { get; set; }
        public string TIMING { get; set; }

        public string prodDt { get; set; }
        public int? Plan { get; set; }
        public int? Finish { get; set; }
        public int? Remaining { get; set; }
        public string ModuleSts { get; set; }
        public string PlanCt { get; set; }
        public string Seq { get; set; }
        public string Module { get; set; }
        public string Case { get; set; }
        public string Line { get; set; }
        public string SeqRemark { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string readySts { get; set; }
        public string ControlModuleNo { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetSequenceListApiResponse
    {
        public List<GetSequenceListApiData> ListSequenceData { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }
    //GetSequenceListApiData
    public class GetSequenceListApiRequest
    {
        public string UserName { get; set; }
        public int Shift { get; set; }
        public string ProdDate { get; set; }
    }

    // LAST CODE

}