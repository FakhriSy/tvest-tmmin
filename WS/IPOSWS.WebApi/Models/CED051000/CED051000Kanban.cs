﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED051000
{
    public class CED051000Kanban
    {
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleFlag { get; set; }
    }
}