﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED060700
{
    public class CED060700
    {
    }

    public class GetNextStackingDataRequest
    {
        public string LineStacking { get; set; }
        //public string ProdDate { get; set; }
    }

    public class GetLoginLeaderRequest
    {
        public string Username { get; set; }
        public string Password { get; set; }
        public string DeviceName { get; set; }
    }


    public class LogoutRequest
    {
        public string NoReg { get; set; }
    }


    public class GetLoginLeaderResponse 
    {
        public string RegNo { get; set; }
        public string Pin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        public string ModuleId { get; set; }
        public string FunctionId { get; set; }
        public string FunctionNm { get; set; }
        public string Menu { get; set; }
        public List<GetFunctionData> Function { get; set; }
    }

    public class GetLoginStackingRequest
    {
        public string Line { get; set; }
        public string NoReg { get; set; }
        public string DeviceName { get; set; }
    }

    public class GetLoginStackingResponse 
    {
        public string ZoneGroupingCd { get; set; }
        public string ZoneGroupingNm { get; set; }
        public string ZoneCd { get; set; }
        public string ZoneNm { get; set; }
        public string RegNo { get; set; }
        public string Pin { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Username { get; set; }
        public string Role { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        public string ModuleId { get; set; }
        public string FunctionId { get; set; }
        public string FunctionNm { get; set; }
        public string Menu { get; set; }
        public string CrippleCd { get; set; }
        public List<GetFunctionData> Function { get; set; }
    }

    public class GetFunctionData
    {
        public string ModuleId { get; set; }
        public string FunctionId { get; set; }
        public string FunctionNm { get; set; }
    }


    public class GetNextStackingDataResponse
    {
        public string ProdDate { get; set; }
        public string LineStacking { get; set; }
        public string SeqNo { get; set; }
        public string LotModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string ShutterNo { get; set; }
        public string ControlModuleNo { get; set; }
        public string ModuleStatus { get; set; }
        public int? CycleTime { get; set; }
        public int? Plan { get; set; }
        public int? Actual { get; set; }
        public int? Remain { get; set; }
        public int? Cripple { get; set; }
        public string ContainerSNo { get; set; }
        public string ModuleDestCd { get; set; }
        public string LotPttrn { get; set; }
        public string caseMark { get; set; }
        public string contentList { get; set; }
        public string ModType { get; set; }
        public List<CED060700Kanban> ListKanbanID { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetNextStackingData
    {
        public string ProdDate { get; set; }
        public string LineStacking { get; set; }
        public string SeqNo { get; set; }
        public string LotModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string ShutterNo { get; set; }
        public string ControlModuleNo { get; set; }
        public string ModuleStatus { get; set; }
        public int? CycleTime { get; set; }
        public int? Plan { get; set; }
        public int? Actual { get; set; }
        public int? Remain { get; set; }
        public int? Cripple { get; set; }
        public string ContainerSNo { get; set; }
        public string ModuleDestCd { get; set; }
        public string KanbanID { get; set; }
        public string KanbanType { get; set; }
        public string PartNo { get; set; }
        public string BoxNo { get; set; }
        public string SupplyFlag { get; set; }
        public string LotPttrn { get; set; }
        public string caseMark { get; set; }
        public string contentList { get; set; }
        //public string BoxingFlag { get; set; }
        //public string FlowRackFlag { get; set; }
        //public string PickingFlag { get; set; }
        public string KanbanPartType { get; set; }
        public string kanbanSts { get; set; }
        public string UniqueNo { get; set; }
        public string Qty { get; set; }
        public string ModType { get; set; }
        public List<CED060700Kanban> ListKanbanID { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    
    }

    public class CED060700Kanban
    {
        public string KanbanID { get; set; }
        public string KanbanType { get; set; }
        public string SupplyFlag { get; set; }        
        public string BoxNo { get; set; }
        public string PartNo { get; set; }
        public string KanbanPartType { get; set; }
        public string kanbanSts { get; set; }
        public string UniqueNo { get; set; }
        public string Qty { get; set; }
    }

    public class PostModuleOpenRequest
    {
        public string LineStacking { get; set; }
        public string ProdDate { get; set; }
        public string ControlModuleNo { get; set; }
        public string StackingDate { get; set; }
        public string UserName { get; set; }
    }

    public class PostModuleOpenResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
        public string ActStart { get; set; }
    }

    public class PostKanbanStackingRequest
    {
        public List<stackingData> stackingData { get; set; }
        public string UserName { get; set; }
    }

    public class PostKanban2StackingRequest
    {
        public List<stackingDataKanban2> stackingData { get; set; }
        public string UserName { get; set; }
    }


    public class stackingDataKanban2
    {
        public string Kanban2 { get; set; }
        public string Kanban3 { get; set; }
        public string Kanban4 { get; set; }
        public string StackingDate { get; set; }
    }


    public class stackingData
    {
        public string LineStacking { get; set; }
        public string ProdDate { get; set; }
        public string ControlModuleNo { get; set; }
        public string KanbanID { get; set; }
        public string StackingDate { get; set; }
    }

    public class PostKanbanStackingResponse
    {
        public List<BaseMessage> messageDetail { get; set; }
    }

    public class PostModuleCloseRequest
    {
        public string LineStacking { get; set; }
        public string ProdDate { get; set; }
        public string ControlModuleNo { get; set; }
        public string StackingDate { get; set; }
        public string UserName { get; set; }
        public string GunId { get; set; }
        public List<listKanbanId> listKanbanId { get; set; }
        public List<listProblem> listProblem { get; set; }   
    }

    public class listProblem
    {
        public string problemType { get; set; }
        public string start { get; set; }
        public string finish { get; set; }
        public string ct { get; set; }
    }

    public class listKanbanId
    {
        public string KanbanId { get; set; }
        public string KanbanType { get; set; }
        public string BoxNo { get; set; }
        public string PartNo { get; set; }
        public string StackingFlag { get; set; }
        public string StackingDate { get; set; }
        public string ProblemSts { get; set; }
        public string PartProblem { get; set; }
    }

    public class PostModuleCloseResponse
    {
        public string PlanCt  { get; set; }
        public string ActCt  { get; set; }
		public string StatusStacking  { get; set; }
        public string TimeStacking { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
        public List<BaseMessage> listMessage { get; set; }
    }

    public class GetKanbanStackingRequest
    {
        public string Username { get; set; }
        public string KanbanId { get; set; }
        public string KanbanType { get; set; }
        public string Kanban4 { get; set; }        
    }

    public class GetKanbanStackingResponse
    {
        public string Kanban3 { get; set; }
        public string PartNo { get; set; }
        public string Kanban2 { get; set; }
        public string UniqueNo { get; set; }
        public string BoxNo { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetBarcodeProcessResponse
    {
        public List<GetBarcodeProcess> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetBarcodeProcess
    {
        public string cdBarcode { get; set; }
        public string barcodeVal { get; set; }
    }




}