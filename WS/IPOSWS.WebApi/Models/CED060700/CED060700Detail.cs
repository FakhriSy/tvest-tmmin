﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED060700
{
    public class CED060700Detail
    {
        public string ControlModuleNo { get; set; }
        public string SuppCode { get; set; }
        public string SuppName { get; set; }
        public string PartNo { get; set; }
        public string KanbanID { get; set; }
        public string Address { get; set; }
        public string LastLocation { get; set; }
        public string Problem { get; set; }
        public string Action { get; set; }
        public string EONo { get; set; }
        public string ManifestReceiveSts { get; set; }
    }
}