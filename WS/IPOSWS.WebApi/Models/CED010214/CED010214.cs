﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED010214
{
    public class CED010214
    {
        public string PackingDate { get; set; }
        public string DestinationCode { get; set; }
        public string Plant { get; set; }
        public string LotNo { get; set; }
        public string CaseNo { get; set; }
        public string RenbanNo { get; set; }
        public string PlanTime { get; set; }
    }
}