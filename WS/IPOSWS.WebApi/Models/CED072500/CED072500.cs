﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072500
{
    public class CED072500
    {
    }

    public class GetCompleteProblemRequest
    {
        public string UserName { get; set; }
        public string PartNo { get; set; }
        public string Supplier { get; set; }
        public string Problem { get; set; }
    }

    public class GetCompleteProblemResponse
    {
        public string TotalCase { get; set; }
        public string TotalPcs { get; set; }
        public List<CED072500Problem> Problem { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class PostConfirmProblemRequest
    {
        public List<ConfirmProblem> Data { get; set; }
    }

    public class ConfirmProblem
    {
        public string UserName { get; set; }
        public string ProblemID { get; set; }
    }

    public class PostConfirmProblemResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

}