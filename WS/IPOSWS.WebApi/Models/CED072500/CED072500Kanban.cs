﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072500
{
    public class CED072500Kanban
    {
        public string Seq { get; set; }
        public string KanbanID { get; set; }
        public string KanbanQty { get; set; }
        public string Position { get; set; }
    }
}