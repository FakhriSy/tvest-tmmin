﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072500
{
    public class CED072500Problem
    {
        public string TotalCase { get; set; }
        public string KanbanID { get; set; }
        public string KanbanQty { get; set; }
        public string Position { get; set; }
        public string Seq { get; set; }
        public string ProblemID { get; set; }
        public string ProblemDt { get; set; }
        public string ProblemTm { get; set; }
        public string Issuer { get; set; }
        public string PartNo { get; set; }
        //public string Unique { get; set; }
        public string UniqueNo { get; set; }
        public string PartNm { get; set; }
        public string Supplier { get; set; }
        public string Problem { get; set; }
        public string ProblemFlagID { get; set; }
        public string TotalPcs { get; set; }
        public string TotalPcsKanban { get; set; }
        //public byte[] ProblemThumbnail1 { get; set; }
        //public byte[] ProblemThumbnail2 { get; set; }
        public string ProblemThumbnail1 { get; set; }
        public string ProblemThumbnail2 { get; set; }
        public string ProblemImage1 { get; set; }
        public string ProblemImage2 { get; set; }
        public List<CED072500Kanban> Kanban { get; set; }
    }
}