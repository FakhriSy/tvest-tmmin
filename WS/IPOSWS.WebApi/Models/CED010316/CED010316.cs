﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED010316
{
    public class CED010316
    {
        public string ProdDate { get; set; }
        public string PLaneCode { get; set; }
        public string PlanStartTime { get; set; }
        public string CycleTime { get; set; }
    }

    public class CED010316Post
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }
}