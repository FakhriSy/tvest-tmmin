﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073900A
{
    public class CED073900A
    {
        public string ProblemId { get; set; }
        public string KanbanId { get; set; }
        public string MainDefect { get; set; }
        public string SubDefect { get; set; }
        public string PartNo { get; set; }
        public string UniqueNo { get; set; }
        public string SupplierCode { get; set; }
        public string SupplierPlantCode { get; set; }
        public string ProblemImage1 { get; set; }
        public string ProblemImage2 { get; set; }
        public string Username { get; set; }
        public string PackingLine { get; set; }
        
        public List<CED073900ADetail> dataDetail { get; set; }
    }

    public class CED073900ADetail
    {
        public string KanbanId { get; set; }
        public string KanbanQty { get; set; }
        public string LastPosition { get; set; }
        public string Issuer { get; set; }
        public string ManifestNo { get; set; }
    }

    public class CED073900AResponse
    {
        public string ProblemId { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED073900AParam
    {
        public string UserName { get; set; }
        public string PartNo { get; set; }
        public string Supplier { get; set; }
        public string KanbanId { get; set; }
        //public string UniqueNo { get; set; }
        //public string PartNo { get; set; }
        //public string Supplier { get; set; }
        //public string Problem { get; set; }
    }

    public class CED073900AGetData
    {

        //public int TotalCase { get; set; }
        //public int TotalPcs { get; set; }
        public string Seq { get; set; }
        public string KanbanID { get; set; }
        public string PartNo { get; set; }
        public string PartNm { get; set; }
        public int KanbanQty { get; set; }
        public string LastPosition { get; set; }
        public string ZoneNm { get; set; }
        public string ProblemFlag { get; set; }
        public string PackingLine { get; set; }
        public string PIC { get; set; }
        public string SuppCd { get; set; }
        public string SuppNm { get; set; }
        public string UniqueNo { get; set; }
        public string SuppPln { get; set; }
        public string ProblemDt { get; set; }
        public string ProblemTime { get; set; }
        public string Issuer { get; set; }
        public string ProblemFlagBy { get; set; }
        public string ManifestNo { get; set; }
    }

    public class CED073900AGetRespons
    {
        public string TotalCase { get; set; }
        public string TotalPcs { get; set; }
        public List<CED073900AGetData> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class CED073900AGetDefectResponse
    {
        public List<CED073900AGetDefect> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class CED073900ADefectResponse{
        public string DefectCd { get; set; }
        public string DefectNm { get; set; }
        public string SubDefectCd { get; set; }
        public string SubDefectNm { get; set; }
    }

    
    public class CED073900AGetDefect{
        public string DefectCd { get; set; }
        public string DefectNm { get; set; }
        public List<CED073900AGetSubDefect> SubData { get; set; }
    }


    public class CED073900AGetSubDefectResponse
    {
        public List<CED073900AGetSubDefect> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }
    
    public class CED073900AGetSubDefect {
        public string SubDefectCd { get; set; }
        public string SubDefectNm { get; set; }
    }



}