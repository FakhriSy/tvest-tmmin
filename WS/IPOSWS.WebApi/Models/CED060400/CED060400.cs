﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED060400
{
    public class CED060400
    {
    }

    public class GetPrepareModuleDataRequest
    {
        public string UserName { get; set; }
        public string Shift { get; set; }
    }

    public class GetPrepareModuleDataResponse
    {
        public List<PostPrepareModuleDataResponse> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class PostPrepareModuleRequest
    {
        public string UserName { get; set; }
        public string ControlModuleNo { get; set; }
        public string ActualStart { get; set; }
        public string ActualFinish { get; set; }
        public string ActualCT { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
    }

    public class PostPrepareModuleResponse
    {
        public string ControlModuleNo { get; set; }
        public string printSts { get; set; }
        public string changedBy { get; set; }
        public string changedDt { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

}