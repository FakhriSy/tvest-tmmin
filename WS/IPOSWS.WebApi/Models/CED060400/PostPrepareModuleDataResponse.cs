﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED060400
{
    public class PostPrepareModuleDataResponse
    {
        public string PrepareModuleTime { get; set; }
        public string Line { get; set; }
        public string LotModuleNo { get; set; }
        public string ModuleType { get; set; }
        public string Jumping { get; set; }
        public string ControlModuleNo { get; set; }
        public string PrepareModuleSts { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public string PrintFlag { get; set; }
        public string disableAction { get; set; }
        //public string Result { get; set; }
        //public string Message { get; set; }
    }
}