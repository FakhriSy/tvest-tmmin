﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models
{
    /// <summary>
    /// Base model
    /// </summary>
    public class BaseModel
    {
        /// <summary>
        /// System id
        /// </summary>
        public Guid? SID { get; set; }

        /// <summary>
        /// Utc created date
        /// </summary>
        public DateTime? UtcCreatedDate { get; set; }

        /// <summary>
        /// Created by
        /// </summary>
        public string CreatedBy { get; set; }

        /// <summary>
        /// Utc updated date
        /// </summary>
        public DateTime? UtcUpdatedDate { get; set; }

        /// <summary>
        /// Updated by
        /// </summary>
        public string UpdatedBy { get; set; }
    }
}