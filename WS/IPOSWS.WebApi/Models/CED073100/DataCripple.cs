﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073100
{
    public class DataCripple
    {
        public string No { get; set; }
        public string DateProd { get; set; }
        public string VanningTime { get; set; }
        public string Destination { get; set; }
        public string Renban { get; set; }
        public string ModLot { get; set; }
        public string CaseNo { get; set; }
        public string Cripple { get; set; }
        public string Readiness { get; set; }
        public string Status { get; set; }
        public string ControlModuleNo { get; set; }
        public string PartNoCripple { get; set; }
        public string BoxNoCripple { get; set; }
        public string PartNoReady { get; set; }
        public string BoxNoReady { get; set; }
        public List<CripplePart> CripplePart { get; set; }
    }
}