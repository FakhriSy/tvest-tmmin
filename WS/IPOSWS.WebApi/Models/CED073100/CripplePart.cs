﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073100
{
    public class CripplePart
    {
        public string PartNoCripple { get; set; }
        public string BoxNoCripple { get; set; }
        public string PartNoReady { get; set; }
        public string BoxNoReady { get; set; }
    }
}