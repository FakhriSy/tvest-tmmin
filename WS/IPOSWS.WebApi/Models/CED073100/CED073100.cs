﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED073100
{
    public class CED073100
    {
    }

    public class GetCrippleDataRequest
    {
        public string UserName { get; set; }
    }

    public class GetCrippleDataResponse
    {
        public List<CrippleDataResponse> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class GetCripplePartRequest
    {
        public string ControlModuleNo { get; set; }
    }

    public class GetCripplePartResponse
    {
        //public List<DataCripple> Data { get; set; }
        public string No { get; set; }
        public string DateProd { get; set; }
        public string VanningTime { get; set; }
        public string Destination { get; set; }
        public string Renban { get; set; }
        public string ModLot { get; set; }
        public string CaseNo { get; set; }
        public string Cripple { get; set; }
        public string Readiness { get; set; }
        public string Status { get; set; }
        public string ControlModuleNo { get; set; }
        public string PartNoCripple { get; set; }
        public string BoxNoCripple { get; set; }
        public string PartNoReady { get; set; }
        public string BoxNoReady { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
        public List<CripplePart> CripplePart { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class PostCrippleRequest
    {
        public string ControlModuleNo { get; set; }
        public string UserName { get; set; }
        public string ChangedBy { get; set; }
        public string ChangedDt { get; set; }
    }

    public class PostCrippleResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

}