﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED021300A
{
    public class CED021300A
    {
    }

    public class CED021300APrinter
    {
        public string PrinterCd { get; set; }
        public string PrinterNm { get; set; }
    }

    public class CED021300APrinterRespons
    {
        public List<CED021300APrinter> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED021300APrintingParam
    {
        public string PackingDt { get; set; }
        public string DestinationCd { get; set; }
        public string LotModuleNo { get; set; }
        public string PrintFlag { get; set; }
    }

    public class CED021300APrintingData
    {
        public string PackingDt { get; set; }
        public string PlanTm { get; set; }
        public string Line { get; set; }
        public string Destination { get; set; }
        public string LotModuleNo { get; set; }
        public string CaseNo { get; set; }
        public string RenbanNo { get; set; }
        public string ControlModuleNo { get; set; }
        public string PrintFlag { get; set; }
    }

    public class CED021300APrintingRespons
    {
        public List<CED021300APrintingData> data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED021300APOSParam
    {
        public string UserName { get; set; }
        public string ControlModuleNo { get; set; }
        public string PrinterCd { get; set; }
    }

    public class CED021300APOSRespons
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }
}