﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED050400
{
    public class CED050400
    {
    }


    public class GetFlowRackRequest
    {
        public string UserName { get; set; }
        public string Param { get; set; }
        public string Line { get; set; }
    }

    public class GetFlowRackResponse
    {
        public string RackAddressCd { get; set; }
        public string CurrentStock { get; set; }
        public string MaxStock { get; set; }
        public string MaxSupplyBox { get; set; }
        public List<CED050400FlowRack> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class PostSupplyFlowRackRequest
    {
        public string userName { get; set; }
        public string line { get; set; }
        public List<PostSupplyKanban> kanbanId { get; set; }
    }

    public class PostSupplyKanban
    {
        public string kanbanID { get; set; }
    }

    public class PostSupplyFlowRackResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }


    public class PostProblemFlowRackRequest
    {
        public string userName { get; set; }
        public string kanbanID { get; set; }
        public string line { get; set; }
    }

    public class PostProblemFlowRackResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    

  

}