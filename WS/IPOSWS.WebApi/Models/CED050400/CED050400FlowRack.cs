﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED050400
{
    public class CED050400FlowRack
    {
        public string RackAddressCd { get; set; }
        public string CurrentStock { get; set; }
        public string MaxStock { get; set; }
        public string MaxSupplyBox { get; set; }
        public string KanbanID { get; set; }
        public string KanbanSts { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }


}