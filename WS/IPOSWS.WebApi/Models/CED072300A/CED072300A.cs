﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED072300A
{
    public class CED072300AHeader
    {
        public int id { get; set; }
        public string PartNo { get; set; }
        public string Unique { get; set; }
        public int KanbanQty { get; set; }
        public int CountKanban { get; set; }
        public String ProblemDate { get; set; }
        public String HoldStatus { get; set; }
        public int TotalCase { get; set; }
        public List<CED072300ADetail> ProblemDetail { get; set; }
    }

    public class CED072300ADetail
    {
        public string ProblemID { get; set; }
        public string ProblemName { get; set; }
        public string Issuer { get; set; }
        public string ProblemDT { get; set; }
    }

    public class CED072300AResponse
    {
        public List<CED072300AHeader> Data { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED072300AResult
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED072300AParam
    {
        public string PartNo { get; set; }
        public string ProblemName { get; set; }
        public string Mode { get; set; }
    }

    public class CED072300AParamSetHoldKanban
    {
        public string Area { get; set; }
        public string UserName { get; set; }
        public List<CED072300ADetailHoldRelease> dataDetail { get; set; }
    }

    public class CED072300ADetailHoldRelease
    {
        public string PartNo { get; set; }

    }

}