﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace IPOSWS.WebApi.Models.CED060900
{
    public class CED060900
    {
    }

    public class GetModuleKanbanRequest
    {
    }

    public class GetModuleKanbanResponse
    {
        public string CtrlModNo { get; set; }
        public string LotModNo { get; set; }
        public string CaseNo { get; set; }
        public string ModuleSts { get; set; }
        public string ModOpenDt { get; set; }
        public string ModCloseDt { get; set; }
        public string Shift { get; set; }
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleSts { get; set; }
        public string PartNo { get; set; }
        public string BoxNo { get; set; }
        public List<CED060900Kanban> KanbanList { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class CED060900Kanban
    {
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleSts { get; set; }
        public string PartNo { get; set; }
        public string BoxNo { get; set; }
    }

    public class SubmitCloseModuleRequest
    {
        public string CtrlModNo { get; set; }
        public string CloseModTime { get; set; }
        public string UserName { get; set; }
    }

    public class SubmitCloseModuleResponse
    {
        public string CtrlModNo { get; set; }
        public string LotModNo { get; set; }
        public string CaseNo { get; set; }
        public string ModuleSts { get; set; }
        public string ModOpenDt { get; set; }
        public string ModCloseDt { get; set; }
        public string Shift { get; set; }
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleSts { get; set; }
        public string PartNo { get; set; }
        public string BoxNo { get; set; }
        public List<CED060900Kanban> KanbanList { get; set; }
        public string Result { get; set; }
        public string Message { get; set; }
    }

    public class SubmitKanbanCrippleRequest
    {
        public string CtrlModNo { get; set; }
        public List<KanbanCripple> KanbanList { get; set; }
        public string FoundDt { get; set; }
        public string UserName { get; set; }
    }

    public class KanbanCripple
    {
        public string KanbanID { get; set; }
        public string StackingTime { get; set; }
        public string CrippleSts { get; set; }
        public string PartNo { get; set; }
        public string BoxNo { get; set; }
    }

    public class SubmitKanbanCrippleResponse
    {
        public string Result { get; set; }
        public string Message { get; set; }
    }

}